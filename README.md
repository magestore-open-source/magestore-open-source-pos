# Magestore POS Open Edition

## How to build POS Open package
1. Copy folder `client/pos` to other place
2. Enter to this folder
    `cd <path to folder>/pos`
3. Run `npm install`
4. Run `npm run-script build`
5. Copy all content in folder `build` to
    `<path to pos open folder>/server/app/code/Magestore/Webpos/build/apps/pos`
6. Compress `server` folder to filename `pos-open-v1.x.x.zip`. Finish this step, you will have a complete package of POS Enterprise package to *test* or *release* in **step 7**
7. In step `Release` in gitlab, drag and drop that compressed file to attachment

