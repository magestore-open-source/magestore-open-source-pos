<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magestore\PosSampleData\Api\Data\ProductInterface;

/**
 * Model - Product Repository
 */
class ProductRepository implements \Magestore\PosSampleData\Api\ProductRepositoryInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * ProductRepository constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    /**
     * @inheritdoc
     */
    public function enableVisibleOnPos(ProductInterface $product)
    {
        $productModel = $this->productRepository->get($product->getSku());
        if (!$productModel->getId() || $productModel->getWebposVisible()) {
            return false;
        }
        $productModel->setWebposVisible(1);
        $this->productRepository->save($productModel);
        return true;
    }
}
