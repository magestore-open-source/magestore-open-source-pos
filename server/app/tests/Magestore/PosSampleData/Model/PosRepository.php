<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Model;

use Magento\Framework\Exception\LocalizedException;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;
use Magestore\Appadmin\Model\Staff\StaffFactory;
use Magestore\PosSampleData\Api\Data\PosInterface;
use Magestore\Webpos\Api\Data\Staff\SessionInterfaceFactory;
use Magestore\Webpos\Api\Data\Staff\SessionSearchResultsInterface;
use Magestore\Webpos\Api\Data\Staff\SessionSearchResultsInterfaceFactory;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;
use Magestore\Webpos\Model\Pos\PosFactory;
use Magestore\Webpos\Model\ResourceModel\Staff\Session;
use Magestore\Webpos\Model\ResourceModel\Staff\Session\CollectionFactory;

/**
 * Model - Pos Repository
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PosRepository implements \Magestore\PosSampleData\Api\PosRepositoryInterface
{
    /**
     * @var StaffFactory
     */
    protected $sessionFactory;
    /**
     * @var Session
     */
    protected $sessionResource;
    /**
     * @var SessionSearchResultsInterface
     */
    protected $sessionSearchResults;
    /**
     * @var CollectionFactory
     */
    protected $sessionCollectionFactory;

    /**
     * @var StaffRepositoryInterface
     */
    protected $staffRepository;

    /**
     * @var PosFactory
     */
    protected $posFactory;

    /**
     * @var StaffFactory
     */
    protected $staffFactory;

    /**
     * @var \Magestore\Webpos\Model\Pos\PosRepository
     */
    protected $posRepository;
    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;
    /**
     * @var \Magestore\Webpos\Api\Event\DispatchServiceInterface
     */
    protected $dispatchService;

    /**
     * @param SessionInterfaceFactory $sessionFactory
     * @param Session $sessionResource
     * @param CollectionFactory $sessionCollectionFactory
     * @param SessionSearchResultsInterfaceFactory $sessionSearchResultsInterfaceFactory
     * @param StaffRepositoryInterface $staffRepository
     * @param SessionRepositoryInterface $sessionRepository
     * @param PosRepositoryInterface $posRepository
     * @param PosFactory $posFactory
     * @param DispatchServiceInterface $dispatchService
     * @param StaffFactory $staffFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        SessionInterfaceFactory $sessionFactory,
        Session $sessionResource,
        CollectionFactory $sessionCollectionFactory,
        SessionSearchResultsInterfaceFactory $sessionSearchResultsInterfaceFactory,
        StaffRepositoryInterface $staffRepository,
        SessionRepositoryInterface $sessionRepository,
        PosRepositoryInterface $posRepository,
        PosFactory $posFactory,
        DispatchServiceInterface $dispatchService,
        StaffFactory $staffFactory
    ) {
        $this->sessionFactory = $sessionFactory;
        $this->sessionResource = $sessionResource;
        $this->sessionSearchResults = $sessionSearchResultsInterfaceFactory;
        $this->sessionCollectionFactory = $sessionCollectionFactory;
        $this->staffRepository = $staffRepository;
        $this->posFactory = $posFactory;
        $this->staffFactory = $staffFactory;
        $this->posRepository = $posRepository;
        $this->sessionRepository = $sessionRepository;
        $this->dispatchService = $dispatchService;
    }
    /**
     * Save Role.
     *
     * @param PosInterface $pos
     * @return boolean
     * @throws LocalizedException
     */
    public function forceSignOut(PosInterface $pos)
    {
        $posModel = $this->posFactory->create()->load($pos->getPosName(), 'pos_name');
        $this->signOut($posModel);
        return true;
    }

    /**
     * Sign Out
     *
     * @param \Magestore\Webpos\Model\Pos\Pos $posModel
     * @throws LocalizedException
     */
    public function signOut($posModel)
    {
        if ($posModel->getId()) {
            // dispatch event to logout POS
            $this->dispatchService->dispatchEventForceSignOut($posModel->getStaffId(), $posModel->getPosId());
            $posModel->setStaffId(null);
            $this->posRepository->save($posModel);
            $this->sessionRepository->signOutPos($posModel->getId());
        }
    }
}
