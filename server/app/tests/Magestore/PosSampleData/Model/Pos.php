<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Model;

use Magento\Framework\DataObject;
use Magestore\PosSampleData\Api\Data\PosInterface;

/**
 * Model Pos
 */
class Pos extends DataObject implements PosInterface
{
    /**
     * Get pos name
     *
     * @api
     * @return string
     */
    public function getPosName()
    {
        return $this->getData(self::POS_NAME);
    }

    /**
     * Set pos name
     *
     * @api
     * @param string $posName
     * @return PosInterface
     */
    public function setPosName($posName)
    {
        return $this->setData(self::POS_NAME, $posName);
    }

    /**
     * Get username
     *
     * @api
     * @return string
     */
    public function getUsername()
    {
        return $this->getData(self::USERNAME);
    }

    /**
     * Set username
     *
     * @api
     * @param string $username
     * @return PosInterface
     */
    public function setUsername($username)
    {
        return $this->setData(self::USERNAME, $username);
    }
}
