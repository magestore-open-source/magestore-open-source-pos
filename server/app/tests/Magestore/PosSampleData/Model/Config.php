<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magestore\PosSampleData\Api\Data\ConfigInterface;

/**
 * PosSampleData - Config model
 */
class Config extends DataObject implements ConfigInterface
{

    /**
     * @inheritdoc
     */
    public function getPath()
    {
        return $this->getData(self::PATH);
    }
    /**
     * @inheritdoc
     */
    public function setPath($path)
    {
        return $this->setData(self::PATH, $path);
    }

    /**
     * @inheritdoc
     */
    public function getValue()
    {
        return $this->getData(self::VALUE);
    }
    /**
     * @inheritdoc
     */
    public function setValue($value)
    {
        return $this->setData(self::VALUE, $value);
    }

    /**
     * @inheritdoc
     */
    public function getScope()
    {
        return $this->getData(self::SCOPE) ? $this->getData(self::SCOPE) : ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
    }
    /**
     * @inheritdoc
     */
    public function setScope($scope)
    {
        return $this->setData(self::SCOPE, $scope);
    }

    /**
     * @inheritdoc
     */
    public function getScopeId()
    {
        return $this->getData(self::SCOPE_ID) ? $this->getData(self::SCOPE_ID) : 0;
    }
    /**
     * @inheritdoc
     */
    public function setScopeId($scopeId)
    {
        return $this->setData(self::SCOPE_ID, $scopeId);
    }
}
