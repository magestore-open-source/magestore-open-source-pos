<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Model;

use Magento\Framework\DataObject;
use Magestore\PosSampleData\Api\Data\CurrencyRateInterface;

/**
 * Model - Currency Rate
 */
class CurrencyRate extends DataObject implements CurrencyRateInterface
{
    /**
     * @inheritdoc
     */
    public function getCurrencyCode()
    {
        return $this->getData(self::CURRENCY_CODE);
    }
    /**
     * @inheritdoc
     */
    public function setCurrencyCode($currencyCode)
    {
        return $this->setData(self::CURRENCY_CODE, $currencyCode);
    }
    /**
     * @inheritdoc
     */
    public function getCurrencyTo()
    {
        return $this->getData(self::CURRENCY_TO);
    }
    /**
     * @inheritdoc
     */
    public function setCurrencyTo($currencyTo)
    {
        return $this->setData(self::CURRENCY_TO, $currencyTo);
    }
    /**
     * @inheritdoc
     */
    public function getRate()
    {
        return $this->getData(self::RATE);
    }
    /**
     * @inheritdoc
     */
    public function setRate($rate)
    {
        return $this->setData(self::RATE, $rate);
    }
}
