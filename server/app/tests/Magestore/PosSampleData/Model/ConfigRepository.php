<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Model;

use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Cache\FrontendInterface;
use Magestore\PosSampleData\Api\ConfigRepositoryInterface;
use Magestore\PosSampleData\Api\Data\ConfigInterface;

/**
 * Model - Config Repository
 */
class ConfigRepository implements ConfigRepositoryInterface
{

    /**
     * Application config
     *
     * @var ScopeConfigInterface
     */
    protected $_appConfig;
    /**
     * @var WriterInterface
     */
    protected $configWriter;

    /**
     * Construct
     *
     * @param ReinitableConfigInterface $config
     * @param WriterInterface $configWriter
     */
    public function __construct(
        ReinitableConfigInterface $config,
        WriterInterface $configWriter
    ) {
        $this->_appConfig = $config;
        $this->configWriter = $configWriter;
    }

    /**
     * @inheritDoc
     */
    public function changeConfig(ConfigInterface $config)
    {
        $this->configWriter->save($config->getPath(), $config->getValue(), $config->getScope(), $config->getScopeId());

        // re-init configuration
        $this->_appConfig->reinit();
        return true;
    }
}
