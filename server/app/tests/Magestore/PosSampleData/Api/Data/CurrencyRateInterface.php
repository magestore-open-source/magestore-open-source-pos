<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Api\Data;

interface CurrencyRateInterface
{
    const CURRENCY_CODE = 'currency_code';
    const CURRENCY_TO = 'currency_to';
    const RATE = 'rate';

    /**
     * Get currency code
     *
     * @return string|null
     */
    public function getCurrencyCode();
    /**
     * Set currency code
     *
     * @param string|null $currencyCode
     * @return $this
     */
    public function setCurrencyCode($currencyCode);
    /**
     * Get currency to
     *
     * @return string|null
     */
    public function getCurrencyTo();
    /**
     * Set currency to
     *
     * @param string|null $currencyTo
     * @return $this
     */
    public function setCurrencyTo($currencyTo);
    /**
     * Get rate
     *
     * @return float
     */
    public function getRate();
    /**
     * Set rate
     *
     * @param float $rate
     * @return $this
     */
    public function setRate($rate);
}
