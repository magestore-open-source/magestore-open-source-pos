<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Api\Data;

interface ConfigInterface
{
    const PATH = 'path';
    const VALUE = 'value';
    const SCOPE = 'scope';
    const SCOPE_ID = 'scope_id';

    /**
     * Get Path
     *
     * @return string|null
     */
    public function getPath();
    /**
     * Set Path
     *
     * @param string|null $path
     * @return $this
     */
    public function setPath($path);

    /**
     * Get Value
     *
     * @return string|null
     */
    public function getValue();
    /**
     * Set Value
     *
     * @param string|null $value
     * @return $this
     */
    public function setValue($value);

    /**
     * Get Scope
     *
     * @return string|null
     */
    public function getScope();
    /**
     * Set Scope
     *
     * @param string|null $scope
     * @return $this
     */
    public function setScope($scope);

    /**
     * Get Scope Id
     *
     * @return int|null
     */
    public function getScopeId();
    /**
     * Set Scope Id
     *
     * @param int|null $scopeId
     * @return $this
     */
    public function setScopeId($scopeId);
}
