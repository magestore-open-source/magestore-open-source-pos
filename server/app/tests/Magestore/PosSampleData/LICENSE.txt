Magestore POS Open Source, is the world's #1 POS for Magento.
Manage real-time retail operations across channels.

Copyright (c) 2021-2022 Magestore POS Open Source.

Magestore POS Open Source is free software: you can redistribute
it and/or modify it under the terms of the GNU Lesser General Public
License version 3 as published by the Free Software Foundation.

Magestore POS Open Source is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License version 3 for more details.

You should have received a copy of the GNU Lesser General Public License
version 3along with PHPWord. If not, see <http://www.gnu.org/licenses/>.
