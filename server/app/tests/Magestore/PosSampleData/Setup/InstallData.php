<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\PosSampleData\Setup;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\State;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Module\Manager;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\InventoryCatalogApi\Api\DefaultStockProviderInterface;
use Magestore\Appadmin\Model\Staff\StaffFactory;
use Magestore\Webpos\Api\WebposManagementInterface;
use Magestore\Webpos\Model\Location\LocationFactory;
use Magestore\Webpos\Model\Pos\PosFactory;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory;

/**
 * Pos Sample Data - Install Data
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InstallData implements InstallDataInterface
{
    const NOT_ENCODE_PASSWORD = 0;

    /**
     * @var LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var PosFactory
     */
    protected $_posFactory;
    /**
     * @var CollectionFactory
     */
    protected $_locationCollectionFactory;
    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * @var StaffFactory
     */
    protected $_staffFactory;

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;
    /**
     * @var State
     */
    protected $_appState;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var ProductFactory
     */
    protected $productFactory;
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param LocationFactory $locationFactory
     * @param PosFactory $posFactory
     * @param CollectionFactory $locationCollectionFactory
     * @param Manager $moduleManager
     * @param StaffFactory $staffFactory
     * @param ProductMetadataInterface $productMetadata
     * @param State $appState
     * @param ProductRepositoryInterface $productRepository
     * @param ProductFactory $productFactory
     * @param ObjectManagerInterface $objectManager
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        LocationFactory $locationFactory,
        PosFactory $posFactory,
        CollectionFactory $locationCollectionFactory,
        Manager $moduleManager,
        StaffFactory $staffFactory,
        ProductMetadataInterface $productMetadata,
        State $appState,
        ProductRepositoryInterface $productRepository,
        ProductFactory $productFactory,
        ObjectManagerInterface $objectManager
    ) {
        $this->_locationFactory = $locationFactory;
        $this->_posFactory = $posFactory;
        $this->_locationCollectionFactory = $locationCollectionFactory;
        $this->moduleManager = $moduleManager;
        $this->_staffFactory = $staffFactory;
        $this->productMetadata = $productMetadata;
        $this->_appState = $appState;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritDoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = ObjectManager::getInstance();
        /** @var WebposManagementInterface $webposManagement */
        $webposManagement = $objectManager->get('Magestore\Webpos\Api\WebposManagementInterface');

        /* SAMPLE MULTI POS*/
        $locationDataSample2 = [
            'name' => 'Location_MultiPOS',
            'street' => '6146 Honey Bluff Parkway',
            'city' => 'Calder',
            'region' => 'Michigan',
            'region_id' => 33,
            'country_id' => 'US',
            'country' => 'United State',
            'postcode' => '49628-7978',
            'description' => 'To distribute products for brick-and-mortar store'
        ];

        if ($webposManagement->isMSIEnable() && $webposManagement->isWebposStandard()) {
            /** @var DefaultStockProviderInterface $defaultStockProvider */
            $defaultStockProvider = $objectManager->create(
                DefaultStockProviderInterface::class
            );
            $locationDataSample2['stock_id'] = $defaultStockProvider->getId();
        }

        $locationSample2 = $this->_locationFactory->create()->load($locationDataSample2['name'], 'name');
        if (!$locationSample2->getId()) {
            $locationSample2 = $this->_locationFactory->create()->setData($locationDataSample2)->save();
        }

        $posMultiPOSData1 = [
            'pos_name' => 'POS_MultiPOS1',
            'location_id' => $locationSample2->getId(),
            'status' => '1'
        ];

        $pos1 = $this->_posFactory->create()->load($posMultiPOSData1['pos_name'], 'pos_name');
        if (!$pos1->getId()) {
            $pos1 = $this->_posFactory->create()->setData($posMultiPOSData1)->save();
        }

        $posMultiPOSData2 = [
            'pos_name' => 'POS_MultiPOS2',
            'location_id' => $locationSample2->getId(),
            'status' => '1'
        ];
        $pos2 = $this->_posFactory->create()->load($posMultiPOSData2['pos_name'], 'pos_name');
        if (!$pos2->getId()) {
            $pos2 = $this->_posFactory->create()->setData($posMultiPOSData2)->save();
        }

        /* SAMPLE ONE POS ONE LOCATION*/

        $locationDataSample1 = [
            'name' => 'Location_OnePOSOneLocation',
            'street' => '6146 Honey Bluff Parkway',
            'city' => 'Calder',
            'region' => 'Michigan',
            'region_id' => 33,
            'country_id' => 'US',
            'country' => 'United State',
            'postcode' => '49628-7978',
            'description' => 'To distribute products for brick-and-mortar store'
        ];

        if (isset($locationDataSample2['stock_id'])) {
            $locationDataSample1['stock_id'] = $locationDataSample2['stock_id'];
        }

        $locationSample1 = $this->_locationFactory->create()->load($locationDataSample1['name'], 'name');
        if (!$locationSample1->getId()) {
            $locationSample1 = $this->_locationFactory->create()->setData($locationDataSample1)->save();
        }

        $posData = [
            'pos_name' => 'POS_OnePOSOneLocation',
            'location_id' => $locationSample1->getId(),
            'status' => '1'
        ];

        $pos = $this->_posFactory->create()->load($posData['pos_name'], 'pos_name');
        if (!$pos->getId()) {
            $pos = $this->_posFactory->create()->setData($posData)->save();
        }

        $staffOnePosData = [
            'username' => 'staffonepos',
            'password' => 'admin123',
            'name' => 'Staff One POS One Location',
            'email' => 'staffonepos@example.com',
            'location_ids' => $locationSample1->getId(),
            'not_encode' => 0,
            'status' => 1
        ];

        $staff1 = $this->_staffFactory->create()->load($staffOnePosData['username'], 'username');
        if (!$staff1->getId()) {
            $staff1 = $this->_staffFactory->create()->setData($staffOnePosData)->save();
        }

        $staffMultiPosData = [
            'username' => 'staffmultipos',
            'password' => 'admin123',
            'name' => 'Staff Multi POS',
            'email' => 'staffmultipos@example.com',
            'location_ids' => $locationSample2->getId(),
            'not_encode' => 0,
            'status' => 1
        ];

        $staff2 = $this->_staffFactory->create()->load($staffMultiPosData['username'], 'username');
        if (!$staff2->getId()) {
            $staff2 = $this->_staffFactory->create()->setData($staffMultiPosData)->save();
        }

        $version = $this->productMetadata->getVersion();
        try {
            if (version_compare($version, '2.2.0', '>=')) {
                $this->_appState->setAreaCode(Area::AREA_ADMINHTML);
            } else {
                $this->_appState->setAreaCode('admin');
            }
        } catch (Exception $e) {
            $this->_appState->getAreaCode();
        }
        $productId = null;
        try {
            $product = $this->productRepository->get('simple-option');
            $productId = $product->getId();
        } catch (Exception $e) {

        }
        if (!$productId) {
            $this->createNewProduct();
        }

        //virtual product
        $virtualProductId = null;
        try {
            $product = $this->productRepository->get('virtual-test');
            $virtualProductId = $product->getId();
        } catch (Exception $e) {

        }
        if (!$virtualProductId) {
            $this->createVirtualProduct();
        }

        //virtual product with custom option
        $virtualProductId = null;
        try {
            $product = $this->productRepository->get('virtual-test-co');
            $virtualProductId = $product->getId();
        } catch (Exception $e) {

        }
        if (!$virtualProductId) {
            $this->createVirtualProductWithCustomOption();
        }
    }

    /**
     * Create New Product
     *
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws LocalizedException
     * @throws StateException
     */
    public function createNewProduct()
    {
        $product = $this->productFactory->create();
        $product->setSku('simple-option');
        $product->setName('Simple Product With Custom Option');
        $product->setAttributeSetId(4);
        $product->setStatus(1);
        $product->setWeight(10);
        $product->setVisibility(4);
        $product->setTaxClassId(0);
        $product->setTypeId('simple');
        $product->setPrice(100);
        $product->setStockData(
            [
                'use_config_manage_stock' => 0,
                'manage_stock' => 1,
                'is_in_stock' => 1,
                'qty' => 9999
            ]
        );
        $product = $this->productRepository->save($product);

        // add custom option
        $option = [
            "sort_order"    => 1,
            "title"         => "Test custom option",
            "price_type"    => "fixed",
            "price"         => "5",
            "type"          => "field",
            "is_require"    => 1
        ];
        $product->setHasOptions(1);
        $product->setCanSaveCustomOptions(true);
        $option = $this->objectManager->create('\Magento\Catalog\Model\Product\Option')
            ->setProductId($product->getId())
            ->setStoreId($product->getStoreId())
            ->addData($option);
        $option->save();
        $product->addOption($option);
        $this->productRepository->save($product);
    }

    /**
     * Create Virtual Product
     *
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws LocalizedException
     * @throws StateException
     */
    public function createVirtualProduct()
    {
        $product = $this->productFactory->create();
        $product->setSku('virtual-test');
        $product->setName('Virtual test');
        $product->setAttributeSetId(4);
        $product->setStatus(1);
        $product->setWeight(10);
        $product->setVisibility(4);
        $product->setTaxClassId(0);
        $product->setTypeId('virtual');
        $product->setPrice(100);
        $product->setStockData(
            [
                'use_config_manage_stock' => 0,
                'manage_stock' => 1,
                'is_in_stock' => 1,
                'qty' => 9999
            ]
        );
        $this->productRepository->save($product);
    }

    /**
     * Create Virtual Product With Custom Option
     *
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws LocalizedException
     * @throws StateException
     */
    public function createVirtualProductWithCustomOption()
    {
        $product = $this->productFactory->create();
        $product->setSku('virtual-test-co');
        $product->setName('Virtual test CO');
        $product->setAttributeSetId(4);
        $product->setStatus(1);
        $product->setWeight(10);
        $product->setVisibility(4);
        $product->setTaxClassId(0);
        $product->setTypeId('virtual');
        $product->setPrice(100);
        $product->setStockData(
            [
                'use_config_manage_stock' => 0,
                'manage_stock' => 1,
                'is_in_stock' => 1,
                'qty' => 9999
            ]
        );
        $product = $this->productRepository->save($product);

        // add custom option
        $option = [
            "sort_order"    => 1,
            "title"         => "Test custom option",
            "price_type"    => "fixed",
            "price"         => "5",
            "type"          => "field",
            "is_require"    => 1
        ];
        $product->setHasOptions(1);
        $product->setCanSaveCustomOptions(true);
        $option = $this->objectManager->create('\Magento\Catalog\Model\Product\Option')
            ->setProductId($product->getId())
            ->setStoreId($product->getStoreId())
            ->addData($option);
        $option->save();
        $product->addOption($option);
        $this->productRepository->save($product);
    }
}
