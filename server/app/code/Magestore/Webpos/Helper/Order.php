<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Helper;

use Exception;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Phrase;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Api\Data\Checkout\OrderInterface;
use Magestore\Webpos\Api\Data\Checkout\OrderInterfaceFactory;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;

/**
 * Order helper
 */
class Order extends Data
{
    const PERMISSION_ALL = 'Magestore_Appadmin::all';

    /**
     * @var PosRepositoryInterface
     */
    protected $posRepository;

    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepository;

    /**
     * @var OrderInterfaceFactory
     */
    protected $posOrderInterfaceFactory;

    /**
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param PosRepositoryInterface $posRepository
     * @param LocationRepositoryInterface $locationRepository
     * @param OrderInterfaceFactory $posOrderInterfaceFactory
     */
    public function __construct(
        Context                     $context,
        ObjectManagerInterface      $objectManager,
        StoreManagerInterface       $storeManager,
        PosRepositoryInterface      $posRepository,
        LocationRepositoryInterface $locationRepository,
        OrderInterfaceFactory       $posOrderInterfaceFactory
    ) {
        parent::__construct($context, $objectManager, $storeManager);
        $this->posRepository = $posRepository;
        $this->locationRepository = $locationRepository;
        $this->posOrderInterfaceFactory = $posOrderInterfaceFactory;
    }

    /**
     * Get Pos By Order
     *
     * @param \Magento\Sales\Model\Order|OrderInterface $order
     * @return array
     */
    public function getPosByOrder($order)
    {
        try {
            $info = [];
            $posId = $order->getPosId();
            $posModel = $this->posRepository->getById($posId);
            if ($posModel->getPosId()) {
                $info['pos_name'] = $posModel->getPosName();
                $locationId = $posModel->getLocationId();
                $locationModel = $this->locationRepository->getById($locationId);
                if ($locationModel->getLocationId()) {
                    $info['location_name'] = $locationModel->getName();
                }
            }
            return $info;
        } catch (Exception $e) {
            return [
                'pos_name' => '',
                'location_name' => ''
            ];
        }
    }

    /**
     * Get Payment Status
     *
     * @param \Magento\Sales\Model\Order|OrderInterface $order
     * @return Phrase
     */
    public function getPaymentStatus($order)
    {
        if ($order->getTotalPaid() < $order->getGrandTotal() && $order->getTotalPaid() > 0) {
            return __('Partial');
        } else {
            return __('Full');
        }
    }

    /**
     * Get correct status from order
     *
     * @param \Magento\Sales\Model\Order|OrderInterface $order
     * @return OrderInterface
     */
    public function verifyOrderReturn($order)
    {
        $posOrder = $this->posOrderInterfaceFactory->create();
        $posOrder->setData($order->getData());
        $posOrder->setAddresses($order->getAddresses());
        $posOrder->setPayments($order->getPayments());
        $posOrder->setItems($order->getItems());
        $posOrder->setStatusHistories($order->getStatusHistories());
        $this->_eventManager->dispatch('pos_verify_order_after', ['order' => $posOrder]);
        return $posOrder;
    }
}
