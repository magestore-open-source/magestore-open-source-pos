<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Helper\Product;

use Exception;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Api\Catalog\Attribute\AttributesRepositoryInterface;
use Magestore\Webpos\Helper\Data;
use Psr\Log\LoggerInterface;

/**
 * Product helper CustomSale
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CustomSale extends Data
{
    const SKU = 'pos_custom_sale';
    const TYPE = 'customsale';
    /**
     * @var State
     */
    protected $_appState;
    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;
    /**
     * @var Product
     */
    protected $_product;
    /**
     * @var AttributeSetFactory
     */
    protected $attributeSetFactory;
    /**
     * @var CategorySetupFactory
     */
    protected $categorySetupFactory;
    /**
     * @var CollectionFactory
     */
    protected $_websiteCollectionFactory;
    /**
     * @var AttributesRepositoryInterface
     */
    protected $attributesRepository;
    /**
     * @var AttributeValue
     */
    protected $attributeValueHelper;

    /**
     * CustomSale constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param State $appState
     * @param ProductMetadataInterface $productMetadata
     * @param Product $product
     * @param AttributeSetFactory $attributeSetFactory
     * @param CollectionFactory $websiteCollectionFactory
     * @param CategorySetupFactory $categorySetupFactory
     * @param AttributesRepositoryInterface $attributesRepository
     * @param AttributeValue $attributeValueHelper
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        State $appState,
        ProductMetadataInterface $productMetadata,
        Product $product,
        AttributeSetFactory $attributeSetFactory,
        CollectionFactory $websiteCollectionFactory,
        CategorySetupFactory $categorySetupFactory,
        AttributesRepositoryInterface $attributesRepository,
        AttributeValue $attributeValueHelper
    ) {
        parent::__construct($context, $objectManager, $storeManager);
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->_websiteCollectionFactory = $websiteCollectionFactory;
        $this->_product = $product;
        $this->_appState = $appState;
        $this->productMetadata = $productMetadata;
        $this->attributesRepository = $attributesRepository;
        $this->attributeValueHelper = $attributeValueHelper;
    }

    /**
     * Get Product Model
     *
     * @return Product
     */
    protected function getProductModel()
    {
        return $this->_product;
    }

    /**
     * Get Product Id
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->getProductModel()->getIdBySku(self::SKU);
    }

    /**
     * Delete Product
     */
    public function deleteProduct()
    {
        try {
            $id = $this->getProductModel()->getIdBySku(self::SKU);
            $this->getProductModel()->load($id)->delete();
        } catch (Exception $e) {
            $logger = ObjectManager::getInstance()
                ->get(LoggerInterface::class);
            $logger->info($e->getMessage());
            $logger->info($e->getTraceAsString());
        }
    }

    /**
     * Create Product
     *
     * @param ModuleDataSetupInterface $setup
     * @return $this
     * @throws Exception
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function createProduct($setup)
    {
        try {
            $version = $this->productMetadata->getVersion();

            if (version_compare($version, '2.2.0', '>=') || $version === 'No version set (parsed as 1.0.0)') {
                $this->_appState->setAreaCode(Area::AREA_ADMINHTML);
            } else {
                $this->_appState->setAreaCode('admin');
            }
        } catch (Exception $e) {
            $this->_appState->getAreaCode();
        }

        $product = $this->getProductModel();
        if ($product->getIdBySku(self::SKU)) {
            return $this;
        } else {
            $product = $product->getCollection()->addAttributeToFilter('type_id', 'simple')->getFirstItem();
            $product->setId(null);
        }

        $websiteIds = $this->_websiteCollectionFactory->create()
            ->addFieldToFilter('website_id', ['neq' => 0])
            ->getAllIds();

        $attributeSet = $this->attributeSetFactory->create();
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $entityTypeId = $categorySetup->getEntityTypeId(Product::ENTITY);
        $attributeSetId = $this->attributeSetFactory->create()
            ->getCollection()
            ->setEntityTypeFilter($entityTypeId)
            ->addFieldToFilter('attribute_set_name', 'Custom_Sale_Attribute_Set')
            ->getFirstItem()
            ->getAttributeSetId();
        if (!$attributeSetId) {
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
            $data = [
                'attribute_set_name' => 'Custom_Sale_Attribute_Set', // define custom attribute set name here
                'entity_type_id' => $entityTypeId,
                'sort_order' => 200,
            ];
            $attributeSet->setData($data);
            $attributeSet->validate();
            $attributeSet->save();
            $attributeSet->initFromSkeleton($attributeSetId);
            $attributeSet->save();
            $attributeSetId = $attributeSet->getId();
        }

        $product->setAttributeSetId($attributeSetId)
            ->setTypeId(self::TYPE)
            ->setStoreId(0)
            ->setSku(self::SKU)
            ->setWebsiteIds($websiteIds)
            ->setStockData(
                [
                    'manage_stock' => 0,
                    'use_config_manage_stock' => 0,
                ]
            );
        
        $product->addData(
            [
                'name' => 'Custom Sale',
                'weight' => 1,
                'status' => 1,
                'visibility' => 1,
                'price' => 0,
                'description' => 'Custom Sale for POS system',
                'short_description' => 'Custom Sale for POS system',
                'quantity_and_stock_status' => [
                    'is_in_stock' => 1,
                    'qty' => 0
                ]
            ]
        );

        $product = $this->addRequireAttributes($product, $attributeSetId);

        if (!is_array($product->validate())) {
            try {
                $product->save();
                if (!$product->getId()) {
                    $lastProduct = $this->getProductModel()->getCollection()
                        ->setOrder('entity_id', 'DESC')
                        ->getFirstItem();
                    $lastProductId = $lastProduct->getId();
                    $product->setName('Custom Sale')->setId($lastProductId + 1)->save();
                    $this->getProductModel()->load(0)->delete();
                }
            } catch (Exception $e) {
                return $this;
            }
        }
        return $this;
    }

    /**
     * Add data for requiring attributes of custom sale product
     *
     * @param Product $product
     * @param int $attributeSetId
     * @return Product
     * @throws NoSuchEntityException If $attributeSetId is not found
     */
    protected function addRequireAttributes($product, $attributeSetId)
    {
        $listAttributes = $this->attributesRepository->getCustomSaleRequireAttributes($attributeSetId);
        /** @var ProductAttributeInterface $attribute */
        foreach ($listAttributes as $attribute) {
            if ($product->getData($attribute->getAttributeCode())) {
                continue;
            } elseif ($attribute['default_value'] != null) {
                $value = $attribute['default_value'];
            } else {
                $value = $this->attributeValueHelper->getDefaultValueAttribute($attribute);
            }

            if ($value) {
                $product->setData($attribute->getAttributeCode(), $value);
            }
        }

        return $product;
    }
}
