<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Helper;

/**
 * Simple profiler for API request
 */
class Profiler
{
    /**
     * Enable/disable profiler
     *
     * @var bool
     */
    public static $enable = false;
    
    /**
     * Profiler result
     *
     * @var array
     */
    public static $profiles = [];
    
    /**
     * Current profile path
     *
     * @var array
     */
    private static $paths = [];
    
    /**
     * Start timer
     *
     * @param string $name
     */
    public static function start($name) // phpcs:ignore
    {
        if (self::$enable) {
            $profiles = &self::$profiles;
            foreach (self::$paths as $path) {
                $profiles = &$profiles[$path];
            }
            self::$paths[] = $name;
            $profiles[$name]['run_time'] = microtime(true);
        }
    }
    
    /**
     * Stop timer
     *
     * @param string $name
     */
    public static function stop($name) // phpcs:ignore
    {
        if (self::$enable) {
            $profiles = &self::$profiles;
            
            array_pop(self::$paths);
            foreach (self::$paths as $path) {
                $profiles = &$profiles[$path];
            }
            $profiles[$name]['run_time'] = microtime(true) - $profiles[$name]['run_time'];
        }
    }
}
