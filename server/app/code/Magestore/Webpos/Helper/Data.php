<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Helper;

use Exception;
use Magento\Catalog\Model\Product\Type;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Filesystem;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;
use Magestore\Webpos\Model\Checkout\PosOrder;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;

/**
 * Class Data
 *
 * Used for helper data
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Data extends AbstractHelper
{
    const URL_TYPE_PWA = 'apps/pos';
    /**
     *
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Data constructor.
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->objectManager = $objectManager;
        $this->_storeManager = $storeManager;
    }

    /**
     * Get model
     *
     * @param string $modelName
     * @param array $arguments
     * @return mixed
     * @throws ValidatorException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getModel($modelName, array $arguments = [])
    {
        $model = $this->objectManager->create('\Magestore\Webpos\\' . $modelName);
        if (!$model) {
            throw new ValidatorException(
                __('%1 doesn\'t extends \Magento\Framework\Model\AbstractModel', $modelName)
            );
        }
        return $model;
    }

    /**
     * Get config
     *
     * @param string $path
     * @return string
     */
    public function getStoreConfig($path)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get current location id
     *
     * @return mixed
     */
    public function getCurrentLocationId()
    {
        $objectManager = ObjectManager::getInstance();
        /** @var GetSessionByIdInterface $getSessionById */
        $getSessionById = $objectManager->get(GetSessionByIdInterface::class);
        $request = $objectManager->get(RequestInterface::class);
        try {
            $locationId = $request->getParam(PosOrder::PARAM_ORDER_LOCATION_ID);
            if (!$locationId) {
                $session = $getSessionById->execute(
                    $request->getParam(RequestProcessor::SESSION_PARAM_KEY)
                );
                $locationId = $session->getLocationId();
            }
        } catch (Exception $e) {
            $locationId = $request->getParam(PosOrder::PARAM_ORDER_LOCATION_ID);
        }
        return $locationId;
    }

    /**
     * Get webpos images
     *
     * @return string
     */
    public function getWebPosImages()
    {
        return $this->_storeManager->getStore()->getBaseUrl(
            UrlInterface::URL_TYPE_MEDIA
        ) . 'webpos/logo/' . $this->getWebPosLogoSetting();
    }

    /**
     * Get pos logo
     *
     * @return mixed
     */
    public function getWebPosLogoSetting()
    {
        return $this->scopeConfig->getValue(
            'webpos/general/webpos_logo',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get current store view
     *
     * @todo: will change in the future to current store view, now it's default store view
     *
     * @return StoreInterface|null
     */
    public function getCurrentStoreView()
    {
        try {
            /*return $this->_storeManager->getStore();*/
            return $this->_storeManager->getDefaultStoreView();
        } catch (Exception $e) {
            return $this->_storeManager->getStore();
        }
    }

    /**
     * Get object manager
     *
     * @return ObjectManagerInterface
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * Get pos url
     *
     * @return bool|string
     */
    public function getPosUrl()
    {
        $fileSystem = $this->objectManager->create(Filesystem::class);
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB)
            . $fileSystem->getUri(DirectoryList::MEDIA);
        if (substr($mediaUrl, -strlen(UrlInterface::URL_TYPE_MEDIA . '/'))
            == UrlInterface::URL_TYPE_MEDIA . '/') {
            $parentUrl = substr(
                $mediaUrl,
                0,
                -strlen(UrlInterface::URL_TYPE_MEDIA . '/')
            );
        }
        if (substr($mediaUrl, -strlen(UrlInterface::URL_TYPE_MEDIA))
            == UrlInterface::URL_TYPE_MEDIA) {
            $parentUrl = substr($mediaUrl, 0, -strlen(UrlInterface::URL_TYPE_MEDIA));
        }
        $url = $parentUrl . self::URL_TYPE_PWA;
        return $url;
    }

    /**
     * Get pó relative url
     *
     * @return bool|mixed|string
     */
    public function getPosRelativeUrl()
    {
        $posUrl = $this->getPosUrl();
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        $relativeUrl = str_replace($baseUrl, '', $posUrl);
        return $relativeUrl;
    }

    /**
     * Is Enabled Elastic Search Engine
     *
     * @return boolean
     */
    public function isEnableElasticSearch()
    {
        if (strpos($this->scopeConfig->getValue('catalog/search/engine'), 'elasticsearch') !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get product type ids to support
     *
     * @return array
     */
    public function getProductTypeIds()
    {
        $types = [
            Type::TYPE_VIRTUAL,
            Type::TYPE_SIMPLE,
            Grouped::TYPE_CODE,
            Type::TYPE_BUNDLE,
            Configurable::TYPE_CODE
        ];
        return $types;
    }
}
