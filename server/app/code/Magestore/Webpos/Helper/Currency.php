<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Helper;

use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Currency Helper
 */
class Currency extends AbstractHelper
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var CurrencyFactory
     */
    protected $_currencyFactory;

    /**
     * Currency cache
     *
     * @var array
     */
    protected $_currencyCache = [];

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param CurrencyFactory $currencyFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        CurrencyFactory $currencyFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->_objectManager = $objectManager;
        $this->_currencyFactory = $currencyFactory;
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * Currency Convert
     *
     * @param float $amount
     * @param string $from
     * @param string $to
     */
    public function currencyConvert($amount, $from, $to)
    {
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrency()->getCode();

        if (($to == $baseCurrencyCode) || ($from == $baseCurrencyCode)) {
            if ($to == $baseCurrencyCode) {
                //convert $amount to base currency
                $converted = $this->convertToBase($amount, $from);
            } else {
                //convert $amount from base currency to $to
                $converted = $this->convertFromBase($amount, $to);
            }
        } else {
            //convert $amount to base currency
            $converted = $this->convertToBase($amount, $from);
            //conver $converted to $to
            $converted = $this->convertFromBase($converted, $to);
        }
        return $converted;
    }

    /**
     * Convert $amount from baseCurrency
     *
     * @param float $amount
     * @param string $to
     * @return float
     */
    public function convertFromBase($amount, $to)
    {
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrency()->getCode();

        //init object for $to currency code. If there is no currency with $to code, return $amount
        if (empty($this->_currencyCache[$to])) {
            $this->_currencyCache[$to] = $this->_currencyFactory->create()->load($to);
            if (!$this->_currencyCache[$to]->getCode()) {
                return $amount;
            }
        }

        // init object for $baseCurrencyCode currency code.
        // If there is no currency with $baseCurrencyCode code, return $amount
        if (empty($this->_currencyCache[$baseCurrencyCode])) {
            $this->_currencyCache[$baseCurrencyCode] = $this->_currencyFactory->create()->load($baseCurrencyCode);
            if (!$this->_currencyCache[$baseCurrencyCode]->getCode()) {
                return $amount;
            }
        }

        $converted = $this->_currencyCache[$baseCurrencyCode]->convert($amount, $this->_currencyCache[$to]);
        return $converted;
    }

    /**
     * Convert $amount from baseCurrency
     *
     * @param float $amount
     * @param string $from
     * @return float
     */
    public function convertToBase($amount, $from)
    {
        if ($amount == 0) {
            return 0;
        }
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrency()->getCode();

        //init object for $from currency code. If there is no currency with $from code, return $amount
        if (empty($this->_currencyCache[$from])) {
            $this->_currencyCache[$from] = $this->_currencyFactory->create()->load($from);
            if (!$this->_currencyCache[$from]->getCode()) {
                return $amount;
            }
        }

        // init object for $baseCurrencyCode currency code.
        // If there is no currency with $baseCurrencyCode code, return $amount
        if (empty($this->_currencyCache[$baseCurrencyCode])) {
            $this->_currencyCache[$baseCurrencyCode] = $this->_currencyFactory->create()->load($baseCurrencyCode);
            if (!$this->_currencyCache[$baseCurrencyCode]->getCode()) {
                return $amount;
            }
        }

        $converted = $this->_currencyCache[$baseCurrencyCode]->convert(1/$amount, $this->_currencyCache[$from]);
        $converted = 1 / $converted;

        return $converted;
    }

    /**
     * Get base currency code
     *
     * @return string
     */
    public function getBaseCurrencyCode()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCode();
    }

    /**
     * Get current currency code
     *
     * @return string
     */
    public function getCurrentCurrencyCode()
    {
        return  $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }
}
