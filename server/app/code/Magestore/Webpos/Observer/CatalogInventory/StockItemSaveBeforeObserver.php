<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\CatalogInventory;

use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Helper\Data;

/**
 * Webpos - StockItemSaveBeforeObserver
 */
class StockItemSaveBeforeObserver implements ObserverInterface
{

    /**
     * @var Data
     */
    protected $helper;

    /**
     * StockItemSaveBeforeObserver constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @inheritDoc
     */
    public function execute(EventObserver $observer)
    {
        $object = $observer->getEvent()->getObject();
        if (($object instanceof StockItemInterface)) {
            $object->setData('updated_time', null);
        }
    }
}
