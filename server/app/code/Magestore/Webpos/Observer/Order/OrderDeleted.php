<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Order;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Api\Data\Log\OrderDeletedInterfaceFactory;
use Magestore\Webpos\Api\Log\OrderDeletedRepositoryInterface;

/**
 * Observer - OrderDeleted
 */
class OrderDeleted implements ObserverInterface
{
    /**
     * @var OrderDeletedRepositoryInterface
     */
    protected $orderDeletedRepository;
    /**
     * @var OrderDeletedInterfaceFactory
     */
    protected $orderDeletedInterfaceFactory;

    /**
     * Construct
     *
     * @param OrderDeletedRepositoryInterface $orderDeletedRepository
     * @param OrderDeletedInterfaceFactory $orderDeletedInterfaceFactory
     */
    public function __construct(
        OrderDeletedRepositoryInterface $orderDeletedRepository,
        OrderDeletedInterfaceFactory $orderDeletedInterfaceFactory
    ) {
        $this->orderDeletedRepository = $orderDeletedRepository;
        $this->orderDeletedInterfaceFactory = $orderDeletedInterfaceFactory;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        $orderIncrementId = $observer->getOrder()->getIncrementId();
        $orderDeleted = $this->orderDeletedInterfaceFactory->create()
                        ->setOrderIncrementId($orderIncrementId);
        $this->orderDeletedRepository->save($orderDeleted);
    }
}
