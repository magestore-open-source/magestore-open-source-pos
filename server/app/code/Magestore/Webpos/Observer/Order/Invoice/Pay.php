<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Order\Invoice;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\Payment\CollectionFactory;

/**
 * Observer - Invoice - Pay
 */
class Pay implements ObserverInterface
{

    /**
     * @var CollectionFactory
     */
    protected $orderPaymentCollectionFactory;

    /**
     * Pay constructor.
     *
     * @param CollectionFactory $orderPaymentCollectionFactory
     */
    public function __construct(
        CollectionFactory $orderPaymentCollectionFactory
    ) {
        $this->orderPaymentCollectionFactory = $orderPaymentCollectionFactory;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @return $this|void
     */
    public function execute(EventObserver $observer)
    {
        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();
        $grandTotal = $order->getGrandTotal();
        $baseGrandTotal = $order->getBaseGrandTotal();
        $posPayments = $this->orderPaymentCollectionFactory->create()->addFieldToFilter('order_id', $order->getId());
        $posTotalPaid = 0;
        $posBaseTotalPaid = 0;
        if ($posPayments->getSize()) {
            foreach ($posPayments as $posPayment) {
                $posTotalPaid += $posPayment->getAmountPaid();
                $posBaseTotalPaid += $posPayment->getBaseAmountPaid();
            }
        }

        $totalInvoiceAmount = $invoice->getGrandTotal();
        $baseTotalInvoiceAmount = $invoice->getBaseGrandTotal();

        $posPreTotalPaid = $order->getPosPreTotalPaid();
        $posBasePreTotalPaid = $order->getPosBasePreTotalPaid();

        if ($posPreTotalPaid > 0 && $posBasePreTotalPaid > 0) {
            $totalInvoiceAmount += $posPreTotalPaid;
            $baseTotalInvoiceAmount += $posBasePreTotalPaid;
            $posTotalPaid = $order->getTotalPaid() - $invoice->getGrandTotal();
            $posBaseTotalPaid = $order->getBaseTotalPaid() - $invoice->getBaseGrandTotal();
        }

        $order->setPosPreTotalPaid($totalInvoiceAmount);
        $order->setPosBasePreTotalPaid($baseTotalInvoiceAmount);

        $totalPaid = max($posTotalPaid, $totalInvoiceAmount);
        $baseTotalPaid = max($posBaseTotalPaid, $baseTotalInvoiceAmount);

        $totalPaid = min($totalPaid, $grandTotal);
        $baseTotalPaid = min($baseTotalPaid, $baseGrandTotal);

        $order->setTotalPaid($totalPaid);
        $order->setBaseTotalPaid($baseTotalPaid);

        return $this;
    }
}
