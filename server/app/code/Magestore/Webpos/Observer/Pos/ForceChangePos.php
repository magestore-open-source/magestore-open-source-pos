<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Pos;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;

/**
 * Observer - ForceChangePos
 */
class ForceChangePos implements ObserverInterface
{
    /**
     * @var PosRepositoryInterface
     */
    protected $posRepository;
    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;

    /**
     * ForceSignOut constructor.
     *
     * @param PosRepositoryInterface $posRepository
     * @param SessionRepositoryInterface $sessionRepository
     */
    public function __construct(
        PosRepositoryInterface $posRepository,
        SessionRepositoryInterface $sessionRepository
    ) {
        $this->posRepository = $posRepository;
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @return $this|void
     * @throws LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        $staffId = $observer->getStaffId();
        $posId = $observer->getPosId();

        $sessions = $this->sessionRepository->getListByStaffId($staffId);
        $sessions->addFieldToFilter('pos_id', $posId);

        foreach ($sessions as $session) {
            if ($session->getHasException() != DispatchServiceInterface::EXCEPTION_CODE_FORCE_SIGN_OUT
                && $session->getHasException() != DispatchServiceInterface::EXCEPTION_CODE_FORCE_CHANGE_POS) {
                $session->setHasException(DispatchServiceInterface::EXCEPTION_CODE_FORCE_CHANGE_POS);
                $session->setLocationId(null);
                $session->setPosId(null);
                $this->sessionRepository->save($session);
            }
        }

        $pos = $this->posRepository->getById($posId);
        $pos->setStaffId(null);
        $this->posRepository->save($pos);

        return $this;
    }
}
