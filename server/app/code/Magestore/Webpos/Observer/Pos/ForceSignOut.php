<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Pos;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;

/**
 * Observer - ForceSignOut
 */
class ForceSignOut implements ObserverInterface
{
    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;

    /**
     * ForceSignOut constructor.
     * @param SessionRepositoryInterface $sessionRepository
     */
    public function __construct(
        SessionRepositoryInterface $sessionRepository
    ) {
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @return $this|void
     * @throws LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        $staffId = $observer->getStaffId();
        $posId = $observer->getPosId();

        $sessions = $this->sessionRepository->getListByStaffId($staffId);
        if ($posId) {
            $sessions->addFieldToFilter('pos_id', $posId);
        }

        foreach ($sessions as $session) {
            if ($session->getHasException() != DispatchServiceInterface::EXCEPTION_CODE_FORCE_SIGN_OUT) {
                $session->setHasException(DispatchServiceInterface::EXCEPTION_CODE_FORCE_SIGN_OUT);
                $this->sessionRepository->save($session);
            }
        }

        return $this;
    }
}
