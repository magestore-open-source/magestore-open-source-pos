<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Customer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Api\Data\Log\CustomerDeletedInterfaceFactory;
use Magestore\Webpos\Api\Log\CustomerDeletedRepositoryInterface;

/**
 * Observer - CustomerDeleted
 */
class CustomerDeleted implements ObserverInterface
{

    protected $customerDeletedRepository;
    protected $customerDeletedInterfaceFactory;

    /**
     * Construct
     *
     * @param CustomerDeletedRepositoryInterface $customerDeletedRepository
     * @param CustomerDeletedInterfaceFactory $customerDeletedInterfaceFactory
     */
    public function __construct(
        CustomerDeletedRepositoryInterface $customerDeletedRepository,
        CustomerDeletedInterfaceFactory $customerDeletedInterfaceFactory
    ) {
        $this->customerDeletedRepository = $customerDeletedRepository;
        $this->customerDeletedInterfaceFactory = $customerDeletedInterfaceFactory;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        $customerId = $observer->getCustomer()->getId();
        $customerDeleted = $this->customerDeletedInterfaceFactory->create()->setCustomerId($customerId);
        $this->customerDeletedRepository->save($customerDeleted);
    }
}
