<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Category;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Api\Data\Log\CategoryDeletedInterfaceFactory;
use Magestore\Webpos\Api\Log\CategoryDeletedRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted\CollectionFactory;

/**
 * Observer - CategoryMoveAfter
 */
class CategoryMoveAfter implements ObserverInterface
{
    /**
     * @var CategoryDeletedRepositoryInterface
     */
    protected $categoryDeletedRepository;
    /**
     * @var CategoryDeletedInterfaceFactory
     */
    protected $categoryDeletedInterfaceFactory;
    /**
     * @var CollectionFactory
     */
    protected $categoryDeletedCollection;
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * CategoryMoveAfter constructor.
     *
     * @param CategoryDeletedRepositoryInterface $categoryDeletedRepository
     * @param CategoryDeletedInterfaceFactory $categoryDeletedInterfaceFactory
     * @param CollectionFactory $categoryDeletedCollection
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        CategoryDeletedRepositoryInterface $categoryDeletedRepository,
        CategoryDeletedInterfaceFactory $categoryDeletedInterfaceFactory,
        CollectionFactory $categoryDeletedCollection,
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->categoryDeletedRepository = $categoryDeletedRepository;
        $this->categoryDeletedInterfaceFactory = $categoryDeletedInterfaceFactory;
        $this->categoryDeletedCollection = $categoryDeletedCollection;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(EventObserver $observer)
    {
        $categoryId = $observer->getData('category_id');
        $prevParentId = $observer->getData('prev_parent_id');
        $parentId = $observer->getData('parent_id');

        $prevParent = $this->categoryRepository->get($prevParentId);
        $parent = $this->categoryRepository->get($parentId);

        $prevCatPath = explode('/', $prevParent->getPath());
        $rootPrevCategory = isset($prevCatPath[1]) ? $prevCatPath[1] : null;

        $catPath = explode('/', $parent->getPath());
        $rootCategory = isset($catPath[1]) ? $catPath[1] : null;

        if ($rootPrevCategory != $rootCategory) {
            // add new category deleted on old root category
            if ($rootPrevCategory) {
                $categoryDeleted = $this->categoryDeletedInterfaceFactory->create()
                    ->setCategoryId($categoryId)
                    ->setRootCategoryId($rootPrevCategory);
                $this->categoryDeletedRepository->save($categoryDeleted);
            }

            // delete all log category deleted on new root category
            $categoryDeletedCollection = $this->categoryDeletedCollection->create();
            $categoryDeletedCollection->addFieldToFilter('root_category_id', $rootCategory);
            $categoryDeletedCollection->addFieldToFilter('category_id', $categoryId);
            foreach ($categoryDeletedCollection as $catModel) {
                $this->categoryDeletedRepository->deleteById($catModel->getId());
            }
        }
    }
}
