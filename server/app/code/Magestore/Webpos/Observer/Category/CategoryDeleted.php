<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Category;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Api\Data\Log\CategoryDeletedInterfaceFactory;
use Magestore\Webpos\Api\Log\CategoryDeletedRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted\CollectionFactory;

/**
 * Observer - CategoryDeleted
 */
class CategoryDeleted implements ObserverInterface
{
    /**
     * @var CategoryDeletedRepositoryInterface
     */
    protected $categoryDeletedRepository;
    /**
     * @var CategoryDeletedInterfaceFactory
     */
    protected $categoryDeletedInterfaceFactory;
    /**
     * @var CollectionFactory
     */
    protected $categoryDeletedCollection;

    /**
     * CategoryDeleted constructor.
     *
     * @param CategoryDeletedRepositoryInterface $categoryDeletedRepository
     * @param CategoryDeletedInterfaceFactory $categoryDeletedInterfaceFactory
     * @param CollectionFactory $categoryDeletedCollection
     */
    public function __construct(
        CategoryDeletedRepositoryInterface $categoryDeletedRepository,
        CategoryDeletedInterfaceFactory $categoryDeletedInterfaceFactory,
        CollectionFactory $categoryDeletedCollection
    ) {
        $this->categoryDeletedRepository = $categoryDeletedRepository;
        $this->categoryDeletedInterfaceFactory = $categoryDeletedInterfaceFactory;
        $this->categoryDeletedCollection = $categoryDeletedCollection;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        $category = $observer->getCategory();
        $categoryId = $category->getId();
        $catPath = explode('/', $category->getPath());
        if (count($catPath) <= 1) {
            return;
        }
        $rootCategoryId = $catPath[1];
        if ($rootCategoryId == $categoryId) {
            // category which was deleted is root category
            $categoryDeletedCollection = $this->categoryDeletedCollection->create();
            $categoryDeletedCollection->addFieldToFilter('root_category_id', $rootCategoryId);
            foreach ($categoryDeletedCollection as $catModel) {
                $this->categoryDeletedRepository->deleteById($catModel->getId());
            }
        } else {
            $categoryDeleted = $this->categoryDeletedInterfaceFactory->create()
                ->setCategoryId($categoryId)
                ->setRootCategoryId($rootCategoryId);
            $this->categoryDeletedRepository->save($categoryDeleted);

        }
    }
}
