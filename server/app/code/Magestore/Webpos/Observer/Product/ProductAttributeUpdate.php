<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Product;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Api\Log\ProductDeletedRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\Collection;

/**
 * Observer - ProductAttributeUpdate
 */
class ProductAttributeUpdate implements ObserverInterface
{
    /**
     * @var ProductDeletedRepositoryInterface
     */
    protected $productDeletedRepository;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $productResouce;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * Construct
     *
     * @param ProductDeletedRepositoryInterface $productDeletedRepository
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResouce
     * @param ProductFactory $productFactory
     */
    public function __construct(
        ProductDeletedRepositoryInterface $productDeletedRepository,
        ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\ResourceModel\Product $productResouce,
        ProductFactory $productFactory
    ) {
        $this->productDeletedRepository = $productDeletedRepository;
        $this->resourceConnection = $resourceConnection;
        $this->productResouce = $productResouce;
        $this->productFactory = $productFactory;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(EventObserver $observer)
    {
        $productIds = $observer->getProductIds();
        $attributesData = $observer->getAttributesData();
        if ((isset($attributesData['status']) && $attributesData['status'] == Status::STATUS_DISABLED)
            || (isset($attributesData['webpos_visible'])
                && $attributesData['webpos_visible'] != Collection::VISIBLE_ON_WEBPOS
            )
        ) {
            foreach ($productIds as $productId) {
                $this->productDeletedRepository->insertByProductId($productId);
            }
        } elseif ((isset($attributesData['status']) && $attributesData['status'] == Status::STATUS_ENABLED)
            || (isset($attributesData['webpos_visible'])
                && $attributesData['webpos_visible'] == Collection::VISIBLE_ON_WEBPOS
            )
        ) {
            $select = $this->productResouce->getConnection()->select()->from(
                $this->productResouce->getTable('catalog_product_entity'),
                ['sku', 'entity_id', 'type_id']
            )->where(
                'entity_id IN (?)',
                $productIds
            );
            $productSkus = $this->productResouce->getConnection()->fetchAll($select);
            foreach ($productSkus as $productSku) {
                if (in_array($productSku['entity_id'], $productIds)) {
                    /** @var Product $product */
                    $product = $this->productFactory->create();
                    $product->setId($productSku['entity_id']);
                    $product->setEntityId($productSku['entity_id']);
                    $product->setSku($productSku['sku']);
                    $this->productDeletedRepository->deleteByProduct($product);
                }
            }
        }
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('catalog_product_entity'); //gives table name with prefix
        try {
            $connection->update(
                $tableName,
                ['updated_at' => date('Y-m-d H:i:s')],
                $connection->quoteInto("entity_id IN (?)", $productIds)
            );
        } catch (Exception $e) {
            $productIds = $observer->getProductIds();
        }
    }
}
