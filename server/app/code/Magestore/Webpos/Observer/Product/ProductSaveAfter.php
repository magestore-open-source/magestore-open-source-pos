<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Observer\Product;

use Exception;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magestore\Webpos\Api\DataProvider\IsSkipReindexPluginInterface;
use Magestore\Webpos\Api\Log\ProductDeletedRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\Collection;

/**
 * Class ProductSaveAfter
 *
 * Observer product save
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductSaveAfter implements ObserverInterface
{
    /**
     * @var ProductDeletedRepositoryInterface
     */
    protected $productDeletedRepository;
    /**
     * @var Type
     */
    protected $productType;
    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * Product Type Instances cache
     *
     * @var array
     */
    protected $productTypes = [];

    /**
     * @var IsSkipReindexPluginInterface
     */
    protected $isSkipReindexPlugin;

    /**
     * Constructor
     *
     * @param ProductDeletedRepositoryInterface $productDeletedRepository
     * @param Type $productType
     * @param ProductMetadataInterface $productMetadata
     * @param ResourceConnection $resourceConnection
     * @param IsSkipReindexPluginInterface|null $isSkipReindexPlugin
     */
    public function __construct(
        ProductDeletedRepositoryInterface $productDeletedRepository,
        Type $productType,
        ProductMetadataInterface $productMetadata,
        ResourceConnection $resourceConnection,
        $isSkipReindexPlugin = null
    ) {
        $this->productDeletedRepository = $productDeletedRepository;
        $this->productType = $productType;
        $this->productMetadata = $productMetadata;
        $this->resourceConnection = $resourceConnection;
        $this->isSkipReindexPlugin = $isSkipReindexPlugin ?: ObjectManager::getInstance()
            ->get(IsSkipReindexPluginInterface::class);
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(EventObserver $observer)
    {
        if ($this->isSkipReindexPlugin->getIsSkipReindexPlugin()) {
            return;
        }

        $product = $observer->getProduct();
        $productId = $product->getId();

        if (version_compare($this->productMetadata->getVersion(), '2.1.13', '<=')) {
            try {
                $connection = $this->resourceConnection->getConnection();
                //gives table name with prefix
                $tableName = $this->resourceConnection->getTableName('catalog_product_entity');
                $connection->update(
                    $tableName,
                    ['updated_at' => date('Y-m-d H:i:s')],
                    $connection->quoteInto("entity_id = ?", $productId)
                );
            } catch (Exception $e) {
                $productId = $product->getId();
            }
        }
        if (!$product->isComposite()) {
            $parentIds = [];
            foreach ($this->getProductTypeInstances() as $typeInstance) {
                /* @var $typeInstance AbstractType */
                $parentIds = array_merge($parentIds, $typeInstance->getParentIdsByChild($productId)); // phpcs:ignore
            }
            if (!empty($parentIds)) {
                $connection = $this->resourceConnection->getConnection();
                $tableName = $this->resourceConnection->getTableName('catalog_product_entity');
                $connection->update(
                    $tableName,
                    ['updated_at' => date("Y-m-d H:i:s")],
                    $connection->quoteInto("entity_id IN (?)", $parentIds)
                );
            }
        }

        if ($product->getStatus() == Status::STATUS_DISABLED
            || $product->getData('webpos_visible') != Collection::VISIBLE_ON_WEBPOS
        ) {
            if ($productId) {
                $this->productDeletedRepository->insertByProductId($productId);
            }
        } elseif ($product->getStatus() == Status::STATUS_ENABLED
            && $product->getData('webpos_visible') == Collection::VISIBLE_ON_WEBPOS
        ) {
            $this->productDeletedRepository->deleteByProduct($product);
        }
    }

    /**
     * Retrieve Product Type Instances as key - type code, value - instance model
     *
     * @return array
     */
    protected function getProductTypeInstances()
    {
        if (empty($this->productTypes)) {
            $productEmulator = new DataObject();
            foreach (array_keys($this->productType->getTypes()) as $typeId) {
                $productEmulator->setTypeId($typeId);
                $this->productTypes[$typeId] = $this->productType->factory($productEmulator);
            }
        }
        return $this->productTypes;
    }
}
