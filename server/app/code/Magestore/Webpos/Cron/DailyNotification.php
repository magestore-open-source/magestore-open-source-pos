<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Cron;

use Magento\Framework\FlagManager;
use Magento\Framework\Notification\NotifierInterface;

/**
 * Pos - Daily Notification
 */
class DailyNotification
{
    /**
     * Code of the flag to retrieve used day of POS.
     */
    const USED_DAYS_FLAG_CODE = 'magestore_pos_used_days';

    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const URL = 'url';

    /**
     * @var FlagManager
     */
    protected $flagManager;

    /**
     * @var NotifierInterface
     */
    protected $notifier;

    /**
     * DailyNotification construct
     *
     * @param FlagManager $flagManager
     * @param NotifierInterface $notifier
     */
    public function __construct(
        FlagManager $flagManager,
        NotifierInterface $notifier
    ) {
        $this->flagManager = $flagManager;
        $this->notifier = $notifier;
    }

    /**
     * Get List Notifications By Number Of Used Days
     *
     * Format: [
     *      '{number_of_used_days}' => (List of notifications in the day) [
     *          [
     *              'title' => '{Notification's title}',
     *              'description' => '{Notification's description}',
     *              'url' => '{Notification's url}'
     *          ],
     *          ...
     *      ],
     *      ...
     * ]
     *
     * @return array
     */
    public function getListNotifications(): array
    {
        return [
            1 => [
                [
                    self::TITLE => __("Manage your multi-store chain with Magestore POS Commerce"),
                    self::DESCRIPTION => __(
                        "When your business expands from one store to multiple stores,"
                        . " using a more centralized system to access real-time info"
                        . " and eliminate manual update is a perfect choice."
                        . " Streamline order, customer, and inventory data"
                        . " between your website and multiple offline stores immediately with our premium version."
                    ),
                    self::URL => "https://www.magestore.com/?"
                        . "utm_source=pos-open-backend&utm_medium=product&utm_campaign=upsell_commerce_2021"
                        . "&utm_content=notification-day1"
                ]
            ]
        ];
    }

    /**
     * Show notifications of day
     */
    public function execute()
    {
        $flagData = (int)$this->flagManager->getFlagData(self::USED_DAYS_FLAG_CODE);
        $usedDay = $flagData ? $flagData + 1 : 1;
        $this->flagManager->saveFlag(self::USED_DAYS_FLAG_CODE, $usedDay);
        $listNotifications = $this->getListNotifications();
        if (isset($listNotifications[$usedDay])) {
            foreach ($listNotifications[$usedDay] as $notification) {
                $this->notifier->addNotice(
                    $notification[self::TITLE],
                    $notification[self::DESCRIPTION],
                    $notification[self::URL]
                );
            }
        }
    }
}
