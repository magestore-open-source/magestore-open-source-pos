<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Cron;

use Exception;
use Magestore\Webpos\Api\Checkout\PosOrderRepositoryInterface;
use Magestore\Webpos\Model\Checkout\PosOrder;
use Magestore\Webpos\Model\Request\ActionLog;
use Magestore\Webpos\Model\ResourceModel\Request\ActionLog\Collection;
use Magestore\Webpos\Model\ResourceModel\Request\ActionLog\CollectionFactory;

/**
 * Cron - WebposOrderConverter
 */
class WebposOrderConverter
{
    /**
     * @var PosOrderRepositoryInterface
     */
    protected $posOrderRepository;
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder\Collection
     */
    protected $posOrderCollection;
    /**
     * @var CollectionFactory
     */
    protected $actionLogCollectionFactory;

    /**
     * WebposOrderConverter constructor.
     *
     * @param PosOrderRepositoryInterface $posOrderRepository
     * @param \Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder\Collection $posOrderCollection
     * @param CollectionFactory $actionLogCollectionFactory
     */
    public function __construct(
        PosOrderRepositoryInterface $posOrderRepository,
        \Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder\Collection $posOrderCollection,
        CollectionFactory $actionLogCollectionFactory
    ) {
        $this->posOrderRepository = $posOrderRepository;
        $this->posOrderCollection = $posOrderCollection;
        $this->actionLogCollectionFactory = $actionLogCollectionFactory;
    }

    /**
     * Process in-completed orders
     *
     * @throws Exception
     */
    public function execute()
    {
        $posOrderCollection = $this->posOrderCollection
            ->addFieldToFilter('status', ['neq' => PosOrder::STATUS_COMPLETED]);

        foreach ($posOrderCollection->getItems() as $posOrder) {
            $this->posOrderRepository->processConvertOrder($posOrder->getIncrementId());
        }

        /** @var Collection $actionLogCollection */
        $actionLogCollection = $this->actionLogCollectionFactory->create();
        $actionLogCollection->addFieldToFilter(
            'status',
            ['neq' => ActionLog::STATUS_COMPLETED]
        );

        $actionLogCollection->processActionLog();
    }
}
