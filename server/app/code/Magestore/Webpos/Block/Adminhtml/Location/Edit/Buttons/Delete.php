<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Block\Adminhtml\Location\Edit\Buttons;

/**
 * Button Delete
 */
class Delete extends Generic
{
    /**
     * @inheritDoc
     */
    public function getButtonData()
    {
        if (!$this->getLocation() || !$this->getLocation()->getId()) {
            return [];
        }

        if (!$this->authorization->isAllowed('Magestore_Webpos::location_delete')) {
            return [];
        }

        $url = $this->getUrl('*/*/delete', ['id' => $this->getLocation()->getId()]);

        return [
            'label' => __('Delete'),
            'class' => 'delete',
            'on_click' => sprintf("deleteConfirm(
                    'Are you sure you want to do this?', 
                    '%s',
                    {data: {}}
                )", $url),
            'sort_order' => 20
        ];
    }
}
