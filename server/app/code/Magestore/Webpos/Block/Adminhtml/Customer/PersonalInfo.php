<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Block\Adminhtml\Customer;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magestore\Webpos\Api\Customer\CustomerRepositoryInterface;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;

/**
 * Additional Class PersonalInfo
 */
class PersonalInfo extends Template
{
    /**
     * @var LocationRepositoryInterface
     */
    protected $_locationRepositoryInterface;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * Constructor for location attribute source
     *
     * @param Context $context
     * @param LocationRepositoryInterface $locationRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        LocationRepositoryInterface $locationRepository,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        $this->_locationRepositoryInterface = $locationRepository;
        $this->_customerRepository = $customerRepository;

        parent::__construct($context, $data);
    }

    /**
     * Get Customer's created location name
     *
     * @return string|null
     */
    public function getCreatedLocationName()
    {
        $customerId = $this->getParentBlock()->getCustomer()->getId();
        $customer = $this->_customerRepository->getById($customerId);
        $locationId = $customer->getCreatedLocationId();
        if ($locationId === null) {
            return null;
        }

        try {
            $dataLocation = $this->_locationRepositoryInterface->getById($locationId);
        } catch (\Exception $error) {
            $dataLocation = null;
        }

        if ($dataLocation) {
            return $dataLocation->getName();
        }
        return null;
    }
}
