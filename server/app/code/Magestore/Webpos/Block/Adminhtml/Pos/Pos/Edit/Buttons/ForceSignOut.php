<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Block\Adminhtml\Pos\Pos\Edit\Buttons;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\UiComponent\Context;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;
use Magestore\Webpos\Helper\Data;

/**
 * Button Force sign out
 */
class ForceSignOut extends Generic
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var StaffRepositoryInterface
     */
    protected $staffRepository;

    /**
     * ForceSignOut constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param AuthorizationInterface $authorization
     * @param Data $helper
     * @param StaffRepositoryInterface $staffRepository
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AuthorizationInterface $authorization,
        Data $helper,
        StaffRepositoryInterface $staffRepository
    ) {
        parent::__construct($context, $registry, $authorization);
        $this->helper = $helper;
        $this->staffRepository = $staffRepository;
    }
    
    /**
     * Get Button Data
     *
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getButtonData()
    {
        if (!$this->getPos() || !$this->getPos()->getId()) {
            return [];
        }

        if ($this->getPos() && !$this->getPos()->getStaffId()) {
            return [];
        }

        if (!$this->authorization->isAllowed('Magestore_Webpos::pos')) {
            return [];
        }

        $url = $this->getUrl('*/*/forceSignOut', ['id' => $this->getPos()->getId()]);
        $onClick = sprintf("location.href = '%s';", $url);

        return [
            'label' => __('Force Sign-out'),
            'class' => 'force_sign_out',
            'on_click' => $onClick,
            'sort_order' => 16
        ];
    }
}
