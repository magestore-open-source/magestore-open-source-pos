<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Block\Adminhtml\Pos\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magestore\Webpos\Model\ResourceModel\Pos\Pos\CollectionFactory;

/**
 * Tab - Linked Pos
 */
class LinkedPos extends Extended
{
    /**
     * @var CollectionFactory
     */
    protected $posCollectionFactory;
    /**
     * @param Context $context
     * @param Data $backendHelper
     * @param CollectionFactory $posCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $posCollectionFactory,
        array $data = []
    ) {
        $this->posCollectionFactory = $posCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('linked-pos');
        $this->setDefaultSort('pos_id');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
        $this->setEmptyText(__('No POS available in this location.'));
        $this->setFilterVisibility(false);
    }

    /**
     * @inheritDoc
     */
    protected function _prepareCollection()
    {
        $locationId = (int)$this->getRequest()->getParam('id');
        $collection = $this->posCollectionFactory->create()->addFieldToFilter('location_id', $locationId);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @inheritDoc
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'pos_id',
            [
                'header' => __('ID'),
                'index' => 'pos_id',
                'type' => 'text'
            ]
        );

        $this->addColumn(
            'pos_name',
            [
                'header' => __('POS Name'),
                'index' => 'pos_name',
                'type' => 'text'
            ]
        );

        $this->addColumn(
            'status',
            [
                'header' => __('Operating Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => [
                    '1' => 'Enabled',
                    '2' => 'Disabled'
                ]
            ]
        );

        $this->addColumn(
            'edit',
            [
                'header' => __('Action'),
                'type' => 'action',
                'getter' => 'getPosId',
                'actions' => [
                    [
                        'caption' => __('Details'),
                        'url' => [
                            'base' => 'webposadmin/pos/edit'
                        ],
                        'field' => 'id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @inheritDoc
     */
    public function getGridUrl()
    {
        return $this->getUrl('webposadmin/location_location/grid', ['_current' => true]);
    }
}
