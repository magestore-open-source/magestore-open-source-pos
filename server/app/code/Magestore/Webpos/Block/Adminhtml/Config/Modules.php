<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Block\Adminhtml\Config;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Module\FullModuleList;

/**
 * Block - Modules
 */
class Modules extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Magestore_Webpos::config/modules.phtml';

    /**
     * @var FullModuleList
     */
    protected $moduleList;

    /**
     * @param Context $context
     * @param FullModuleList $moduleList
     * @param array $data
     */
    public function __construct(
        Context $context,
        FullModuleList $moduleList,
        array $data = []
    ) {
        $this->moduleList = $moduleList;
        parent::__construct($context, $data);
    }

    /**
     * Get list of modules and versions
     *
     * @return string[]
     */
    public function getModules()
    {
        $result = [];
        foreach ($this->moduleList->getAll() as $name => $module) {
            if (0 === strpos($name, 'Magestore_')) {
                $result[substr($name, 10)] = $module['setup_version'];
            }
        }
        return $result;
    }
}
