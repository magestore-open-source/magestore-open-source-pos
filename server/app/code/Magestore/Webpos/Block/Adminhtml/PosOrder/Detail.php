<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Block\Adminhtml\PosOrder;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Directory\Model\Currency;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magestore\Webpos\Model\Checkout\PosOrderFactory;
use Magestore\Webpos\Model\Source\Adminhtml\Location;
use Magestore\Webpos\Model\Source\Adminhtml\Pos;
use Magento\Sales\Model\Order\StatusFactory;

/**
 * Class \Magestore\Webpos\Block\Adminhtml\PosOrder\Detail
 */
class Detail extends Template
{
    /**
     * @var PosOrderFactory
     */
    protected $posOrderFactory;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var CurrencyFactory
     */
    protected $currencyFactory;

    /**
     * @var Location
     */
    protected $location;

    /**
     * @var Pos
     */
    protected $pos;

    /**
     * @var StatusFactory
     */
    protected $statusFactory;

    /**
     * Detail constructor.
     * @param Context $context
     * @param PosOrderFactory $posOrderFactory
     * @param Json $json
     * @param CurrencyFactory $currencyFactory
     * @param Location $location
     * @param Pos $pos
     * @param StatusFactory $statusFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        PosOrderFactory $posOrderFactory,
        Json $json,
        CurrencyFactory $currencyFactory,
        Location $location,
        Pos $pos,
        StatusFactory $statusFactory,
        array $data = []
    ) {
        $this->posOrderFactory = $posOrderFactory;
        $this->json = $json;
        $this->currencyFactory = $currencyFactory;
        $this->location = $location;
        $this->pos = $pos;
        $this->statusFactory = $statusFactory;
        parent::__construct($context, $data);
    }

    /**
     * Get Pos order data
     *
     * @return void
     */
    public function _construct()
    {
        $this->getPosOrderData();
        parent::_construct();
    }

    /**
     * Format price
     *
     * @param float $price
     * @return string
     */
    public function formatPrice($price)
    {
        $orderCurrencyCode = $this->getData('order_currency_code');
        $currencyModel = $this->getCurrency($orderCurrencyCode);
        if ($currencyModel->getId()) {
            return $currencyModel->format($price);
        } else {
            return $price;
        }
    }

    /**
     * Get location name
     *
     * @param int $locationId
     * @return string
     */
    public function getLocationName($locationId)
    {
        $locationOption = $this->location->getOptionArray();
        if (isset($locationOption[$locationId])) {
            return $locationOption[$locationId];
        } else {
            return '';
        }
    }

    /**
     * Get pos name
     *
     * @param int $posId
     * @return string
     */
    public function getPosName($posId)
    {
        $posOption = $this->pos->getOptionArray();
        if (isset($posOption[$posId])) {
            return $posOption[$posId];
        } else {
            return '';
        }
    }

    /**
     * Get label by code
     *
     * @param string $statusCode
     * @return string
     */
    public function getStatus($statusCode)
    {
        $status = $this->statusFactory->create()->load($statusCode, 'status');
        return $status->getLabel();
    }

    /**
     * Retrieve order currency
     *
     * @param string $code
     * @return Currency
     */
    public function getCurrency($code)
    {
        return $this->currencyFactory->create()->load($code);
    }

    /**
     * Get Pos order data
     *
     * @return void
     */
    public function getPosOrderData()
    {
        $posOrder = $this->getOrder();
        if ($posOrder->getId()) {
            $params = $posOrder->getParams();
            $decodeParams = $this->json->unserialize($params);
            if (isset($decodeParams['order'])) {
                $this->setData($decodeParams['order']);
            }
        }
    }

    /**
     * Get address description by type
     *
     * @param string $type
     * @return string
     */
    public function getAddressDescription($type)
    {
        $addresses = $this->getData('addresses');
        foreach ($addresses as $address) {
            if ($address['address_type'] == $type) {
                $addressArray = [];
                if (isset($address['street'][0])) {
                    $addressArray[] = $address['street'][0];
                }
                $addressArray[] = $address['city'];
                $addressArray[] = $address['region'];
                $addressArray[] = $address['postcode'];
                $addressArray[] = $address['country_id'];
                return implode(', ', $addressArray);
            }
        }
        return '';
    }

    /**
     * Get Order
     *
     * @return mixed
     */
    public function getOrder()
    {
        return $this->posOrderFactory->create()->load($this->getRequest()->getParam('id'));
    }
}
