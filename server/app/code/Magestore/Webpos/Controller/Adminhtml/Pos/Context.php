<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Controller\Adminhtml\Pos;

use Magento\Backend\Helper\Data;
use Magento\Backend\Model\Auth;
use Magento\Backend\Model\Session;
use Magento\Backend\Model\UrlInterface;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\CatalogInventory\Model\Configuration;
use Magento\Framework\App\ActionFlag;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\ViewInterface;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Controller\ResultFactory as ResultFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Logger\Monolog;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;

/**
 * Pos Controller Context
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class Context extends \Magento\Backend\App\Action\Context
{
    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var ForwardFactory
     */
    protected $_resultForwardFactory;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var LayoutFactory
     */
    protected $_resultLayoutFactory;

    /**
     * @var Configuration
     */
    protected $_catalogInventoryConfiguration;

    /**
     * @var Monolog
     */
    protected $_logger;

    /**
     * Context constructor.
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param ObjectManagerInterface $objectManager
     * @param ManagerInterface $eventManager
     * @param \Magento\Framework\UrlInterface $url
     * @param RedirectInterface $redirect
     * @param ActionFlag $actionFlag
     * @param ViewInterface $view
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param RedirectFactory $resultRedirectFactory
     * @param ResultFactory $resultFactory
     * @param Session $session
     * @param AuthorizationInterface $authorization
     * @param Auth $auth
     * @param Data $helper
     * @param UrlInterface $backendUrl
     * @param Validator $formKeyValidator
     * @param ResolverInterface $localeResolver
     * @param Registry $registry
     * @param ForwardFactory $resultForwardFactory
     * @param PageFactory $resultPageFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param Monolog $logger
     * @param bool $canUseBaseUrl
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        RequestInterface $request,
        ResponseInterface $response,
        ObjectManagerInterface $objectManager,
        ManagerInterface $eventManager,
        \Magento\Framework\UrlInterface $url,
        RedirectInterface $redirect,
        ActionFlag $actionFlag,
        ViewInterface $view,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        RedirectFactory $resultRedirectFactory,
        ResultFactory $resultFactory,
        Session $session,
        AuthorizationInterface $authorization,
        Auth $auth,
        Data $helper,
        UrlInterface $backendUrl,
        Validator $formKeyValidator,
        ResolverInterface $localeResolver,
        Registry $registry,
        ForwardFactory $resultForwardFactory,
        PageFactory $resultPageFactory,
        LayoutFactory $resultLayoutFactory,
        Monolog $logger,
        $canUseBaseUrl = false
    ) {
        parent::__construct(
            $request,
            $response,
            $objectManager,
            $eventManager,
            $url,
            $redirect,
            $actionFlag,
            $view,
            $messageManager,
            $resultRedirectFactory,
            $resultFactory,
            $session,
            $authorization,
            $auth,
            $helper,
            $backendUrl,
            $formKeyValidator,
            $localeResolver,
            $canUseBaseUrl
        );

        $this->_eventManager = $eventManager;
        $this->_coreRegistry = $registry;
        $this->_resultForwardFactory = $resultForwardFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultLayoutFactory = $resultLayoutFactory;
        $this->_logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getEventManager()
    {
        return $this->_eventManager;
    }

    /**
     * Get Core Registry
     *
     * @return Registry
     */
    public function getCoreRegistry()
    {
        return $this->_coreRegistry;
    }

    /**
     * Get Result Forward Factory
     *
     * @return ForwardFactory
     */
    public function getResultForwardFactory()
    {
        return $this->_resultForwardFactory;
    }

    /**
     * Get Result Page Factory
     *
     * @return PageFactory
     */
    public function getResultPageFactory()
    {
        return $this->_resultPageFactory;
    }

    /**
     * Get Result Layout Factory
     *
     * @return LayoutFactory
     */
    public function getResultLayoutFactory()
    {
        return $this->_resultLayoutFactory;
    }

    /**
     * Get Logger
     *
     * @return Monolog
     */
    public function getLogger()
    {
        return $this->_logger;
    }
}
