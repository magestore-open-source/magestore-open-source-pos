<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Controller\Adminhtml\Pos;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Logger\Monolog;
use Magento\Framework\Registry;
use Magento\Framework\View\Page\Config;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;
use Magestore\Webpos\Api\Data\Pos\PosInterfaceFactory;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Model\Pos\PosRepository;

/**
 * Pos - AbstractAction
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class AbstractAction extends Action
{
    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var Monolog
     */
    protected $logger;

    /**
     * @var Config
     */
    protected $pageConfig;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var PosInterfaceFactory
     */
    protected $posInterfaceFactory;

    /**
     * @var PosRepository
     */
    protected $posRepository;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;
    /**
     * @var DispatchServiceInterface
     */
    protected $dispatchService;

    /**
     * @var StaffRepositoryInterface
     */
    protected $staffRepository;

    /**
     * AbstractAction constructor.
     * @param Context $context
     * @param Config $pageConfig
     * @param Data $helper
     * @param PosInterfaceFactory $posInterfaceFactory
     * @param PosRepositoryInterface $posRepository
     * @param JsonFactory $jsonFactory
     * @param SessionRepositoryInterface $sessionRepository
     * @param DispatchServiceInterface $dispatchService
     * @param StaffRepositoryInterface $staffRepository
     */
    public function __construct(
        Context $context,
        Config $pageConfig,
        Data $helper,
        PosInterfaceFactory $posInterfaceFactory,
        PosRepositoryInterface $posRepository,
        JsonFactory $jsonFactory,
        SessionRepositoryInterface $sessionRepository,
        DispatchServiceInterface $dispatchService,
        StaffRepositoryInterface $staffRepository
    ) {
        parent::__construct($context);
        $this->eventManager = $context->getEventManager();
        $this->coreRegistry = $context->getCoreRegistry();
        $this->resultForwardFactory = $context->getResultForwardFactory();
        $this->resultPageFactory = $context->getResultPageFactory();
        $this->resultLayoutFactory = $context->getResultLayoutFactory();
        $this->logger = $context->getLogger();
        $this->pageConfig = $pageConfig;
        $this->helper = $helper;
        $this->posInterfaceFactory = $posInterfaceFactory;
        $this->posRepository = $posRepository;
        $this->jsonFactory = $jsonFactory;
        $this->sessionRepository = $sessionRepository;
        $this->dispatchService = $dispatchService;
        $this->staffRepository = $staffRepository;
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magestore_Webpos::pos');
    }
}
