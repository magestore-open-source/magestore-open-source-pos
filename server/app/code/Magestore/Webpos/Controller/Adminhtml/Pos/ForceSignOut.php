<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Controller\Adminhtml\Pos;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class \Magestore\Webpos\Controller\Adminhtml\Pos\ForceSignOut
 *
 * Force Sign-out Pos
 * Methods:
 *  execute
 */
class ForceSignOut extends AbstractAction implements HttpGetActionInterface
{
    /**
     * Execute
     *
     * @return $this|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $modelId = $this->getRequest()->getParam('id');
        if ($modelId > 0) {
            $model = $this->posInterfaceFactory->create()->load($modelId);

            // dispatch event to logout POS
            $this->dispatchService->dispatchEventForceSignOut($model->getStaffId(), $model->getPosId());

            $model->setStaffId(null);
            $this->posRepository->save($model);
            $this->sessionRepository->signOutPos($modelId);
        }
        return $resultRedirect->setPath('*/*/edit', ['id' =>$modelId]);
    }
}
