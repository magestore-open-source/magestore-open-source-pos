<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Controller\Adminhtml\Location\Location;

use Exception;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\InventoryCatalogApi\Api\DefaultStockProviderInterface;
use Magestore\Webpos\Api\WebposManagementInterface;
use Magestore\Webpos\Controller\Adminhtml\Location\Location;

/**
 * Save location
 */
class Save extends Location implements HttpPostActionInterface
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('location_id');
        $data = $this->getRequest()->getPostValue();
        $arr1 = $data['general_information']['sub_general_information'];
        $arr2 = $data['general_information']['stock_selection'] ?? [];
        $arr3 = $data['general_information']['address_information'];
        $arr = array_merge($arr1, $arr2, $arr3);

        if (!$arr) {
            return $this->redirectResult('*/*/');
        }

        /** @var WebposManagementInterface $webposManagement */
        $webposManagement = $this->_objectManager->get(WebposManagementInterface::class);
        if ($webposManagement->isMSIEnable() && $webposManagement->isWebposStandard()) {
            /** @var DefaultStockProviderInterface $defaultStockProvider */
            $defaultStockProvider = $this->_objectManager->create(DefaultStockProviderInterface::class);
            $arr['stock_id'] = $defaultStockProvider->getId();
        }

        if ($id) {
            $model = $this->locationRepository->getById($id);
        } else {
            $model = $this->locationInterfaceFactory->create();
        }

        $model->setData($arr);

        try {
            $this->locationRepository->save($model);
            $this->messageManager->addSuccessMessage(__('You saved the location.'));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        if ($this->getRequest()->getParam('back') == 'edit') {
            return $this->redirectResult('*/*/edit', ['id' => $model->getLocationId()]);
        }

        return $this->redirectResult('*/*/');
    }
}
