<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Controller\Adminhtml\Location\Location;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\View\Result\Page;
use Magestore\Webpos\Controller\Adminhtml\Location\Location;

/**
 * Inline edit location
 */
class InlineEdit extends Location implements HttpPostActionInterface
{
    protected $resource = 'Magestore_Webpos::location_edit';

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('items', []);

        if (!($this->getRequest()->getParam('isAjax') && count($data))) {
            $resultData = [
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ];
        } else {
            foreach ($data as $id => $item) {
                $location = $this->locationRepository->getById($id);
                $location->addData($item);
                $this->locationRepository->save($location);
            }
            $resultData = [
                'messages' => $this->getErrorMessages(),
                'error' => $this->isErrorExists(),

            ];
        }

        return $this->jsonResult($resultData);
    }

    /**
     * Get Error Messages
     *
     * @return array
     */
    protected function getErrorMessages()
    {
        $messages = [];
        foreach ($this->getMessageManager()->getMessages()->getItems() as $error) {
            $messages[] = $error->getText();
        }
        return $messages;
    }

    /**
     * Is Error Exists
     *
     * @return bool
     */
    protected function isErrorExists()
    {
        return (bool)$this->getMessageManager()->getMessages(true)->getCount();
    }
}
