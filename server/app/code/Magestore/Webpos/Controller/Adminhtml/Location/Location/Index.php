<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Controller\Adminhtml\Location\Location;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magestore\Webpos\Api\Data\Location\LocationInterfaceFactory;
use Magestore\Webpos\Controller\Adminhtml\Location\Location;

/**
 * Location Index Controller
 */
class Index extends Location implements HttpGetActionInterface
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magestore_Webpos::locations');
        $resultPage->addBreadcrumb(__('Location'), __('Location'));
        $resultPage->getConfig()->getTitle()->prepend(__('Location'));
        return $resultPage;
    }
}
