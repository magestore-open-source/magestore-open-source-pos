<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Controller\Adminhtml\Location\Location;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magestore\Webpos\Model\ResourceModel\Location\Location\Collection;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;

/**
 * Mass delete location
 */
class MassDelete extends Action implements HttpPostActionInterface
{
    /**
     * @var string
     */
    protected $redirectUrl = '*/*/index';
    /**
     * @var Filter
     */
    protected $filter;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepositoryInterface;

    /**
     * @var $posRepositoryInterface
     */
    protected $posRepositoryInterface;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param LocationRepositoryInterface $locationRepositoryInterface
     * @param PosRepositoryInterface $posRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        LocationRepositoryInterface $locationRepositoryInterface,
        PosRepositoryInterface $posRepository
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->locationRepositoryInterface = $locationRepositoryInterface;
        $this->posRepositoryInterface = $posRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            return $this->massAction($collection);
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath($this->redirectUrl);
        }
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magestore_Webpos::location_delete');
    }

    /**
     * Mass Action
     *
     * @param Collection $collection
     * @return Redirect
     */
    protected function massAction(Collection $collection)
    {
        $checkDelete = true;
        foreach ($collection as $location) {
            if (count($this->posRepositoryInterface->getPosByLocationId($location->getLocationId())) > 0) {
                $checkDelete = false;
                break;
            }
        }
        if ($checkDelete) {
            $locationDeleted = 0;
            foreach ($collection as $location) {
                $this->locationRepositoryInterface->delete($location);
                $locationDeleted++;
            }
            if ($locationDeleted) {
                $this->messageManager->addSuccessMessage(__('A total of %1 record(s) were deleted.', $locationDeleted));
            }
        } else {
            $this->messageManager->addErrorMessage(
                __('The operations failed. Some locations are still working. You can\'t delete them!')
            );
        }
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->redirectUrl);

        return $resultRedirect;
    }
}
