<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Controller\Adminhtml\Location\Location;

use Exception;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magestore\Webpos\Controller\Adminhtml\Location\Location;

/**
 * Delete location
 */
class Delete extends Location implements HttpPostActionInterface
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        try {
            $this->locationRepository->deleteById($id);
            $this->messageManager->addSuccessMessage('Items deleted successfully!');
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $this->redirectResult('*/*/index');
    }
}
