<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Controller\Adminhtml\Location;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magestore\Webpos\Api\Data\Location\LocationInterfaceFactory;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;

/**
 * Location abstract controller
 */
abstract class Location extends \Magento\Backend\App\Action
{

    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepository;

    /**
     * @var LocationInterfaceFactory
     */
    protected $locationInterfaceFactory;

    /**
     * @var Registry
     */
    protected $registry;

    protected $resource = 'Magestore_Webpos::locations';

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param ForwardFactory $resultForwardFactory
     * @param Registry $registry
     * @param LocationRepositoryInterface $locationRepository
     * @param LocationInterfaceFactory $locationInterfaceFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        LayoutFactory $resultLayoutFactory,
        ForwardFactory $resultForwardFactory,
        Registry $registry,
        LocationRepositoryInterface $locationRepository,
        LocationInterfaceFactory $locationInterfaceFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->locationRepository = $locationRepository;
        $this->locationInterfaceFactory = $locationInterfaceFactory;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * Redirect Result
     *
     * @param string $url
     * @param array $params
     * @return Redirect
     */
    public function redirectResult($url, $params = [])
    {
        /* @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($url, $params);

        return $resultRedirect;
    }

    /**
     * Json Result
     *
     * @param array $data
     * @return Json
     */
    public function jsonResult($data)
    {
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($data);
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed($this->resource);
    }
}
