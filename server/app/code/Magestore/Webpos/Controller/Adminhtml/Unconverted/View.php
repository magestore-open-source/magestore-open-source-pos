<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Controller\Adminhtml\Unconverted;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magestore\Webpos\Model\Checkout\PosOrder;

/**
 * Class \Magestore\Webpos\Controller\Adminhtml\Unconverted\View
 */
class View extends AbstractAction implements HttpGetActionInterface
{
    /**
     * View order
     *
     * @return Page|void
     */
    public function execute()
    {
        $posOrder = $this->_objectManager->get(PosOrder::class)
            ->load($this->getRequest()->getParam('id'));
        if ($posOrder->getId()) {
            $resultPage = $this->resultPageFactory->create();
            $resultPage->setActiveMenu('Magestore_Webpos::unconvertedOrder');
            $resultPage->addBreadcrumb(__('Unconverted Order'), __('Unconverted Order'));
            $resultPage->getConfig()->getTitle()->prepend(__('Unconverted Order #%1', $posOrder->getIncrementId()));
            return $resultPage;
        }
    }
}
