<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Controller\Adminhtml\Unconverted;

use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magestore\Webpos\Api\Checkout\PosOrderRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder\Collection;

/**
 * Class \Magestore\Webpos\Controller\Adminhtml\Unconverted\MassConvert
 */
class MassConvert extends AbstractMassAction implements HttpPostActionInterface
{
    /**
     * @var PosOrderRepositoryInterface
     */
    protected $posOrderRepository;

    /**
     * MassConvert constructor.
     *
     * @param PosOrderRepositoryInterface $posOrderRepository
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        PosOrderRepositoryInterface $posOrderRepository,
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        $this->posOrderRepository = $posOrderRepository;
        parent::__construct($context, $filter, $collectionFactory);
    }

    /**
     * Mass action
     *
     * @param Collection $collection
     * @return Redirect|ResultInterface|mixed
     */
    protected function massAction(Collection $collection)
    {
        $orderConverted = 0;
        foreach ($collection as $model) {
            try {
                $this->posOrderRepository->processConvertOrder($model->getIncrementId());
                $orderConverted++;
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath($this->getComponentRefererUrl());
                return $resultRedirect;
            }
        }
        if ($orderConverted) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) were converted.', $orderConverted));
        }
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getComponentRefererUrl());

        return $resultRedirect;
    }
}
