<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magestore\Webpos\Helper\Data;

/**
 * Redirect to webpos checkout
 */
class Index extends Action implements HttpGetActionInterface
{
    /**
     * Execute
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $objectManager = ObjectManager::getInstance();
        $helperData = $objectManager->get(Data::class);
        $url = $helperData->getPosUrl() . '/';
        if ($this->isNginxServer()) {
            $url = $url . 'index.html';
        }
        return $this->_redirect($url);
    }

    /**
     * Is nginx server
     *
     * @return bool
     */
    protected function isNginxServer()
    {
        $serverSoftware = $this->getRequest()->getServer('SERVER_SOFTWARE');
        $serverSoftware = strtolower($serverSoftware);
        return strpos($serverSoftware, 'nginx') !== false;
    }

    /**
     * Is allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magestore_Webpos::checkout');
    }
}
