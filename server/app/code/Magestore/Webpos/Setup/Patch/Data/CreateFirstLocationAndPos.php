<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Setup\Patch\Data;

use Magento\Directory\Model\Country;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\InventoryApi\Api\SourceRepositoryInterface;
use Magento\InventoryCatalogApi\Api\DefaultSourceProviderInterface;
use Magento\InventoryCatalogApi\Api\DefaultStockProviderInterface;
use Magestore\Webpos\Api\WebposManagementInterface;
use Magestore\Webpos\Model\Location\Location;
use Magestore\Webpos\Model\Location\LocationFactory;
use Magestore\Webpos\Model\Pos\PosFactory;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory;
use Magestore\Webpos\Model\Source\Adminhtml\Status as PosStatus;

/**
 * Create first location and pos after installing pos
 */
class CreateFirstLocationAndPos implements DataPatchInterface
{
    /**
     * @var LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var PosFactory
     */
    protected $_posFactory;

    /**
     * @var CollectionFactory
     */
    protected $_locationCollectionFactory;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * @var WebposManagementInterface
     */
    protected $webposManagement;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * InstallData constructor.
     *
     * @param LocationFactory $locationFactory
     * @param PosFactory $posFactory
     * @param CollectionFactory $locationCollectionFactory
     * @param CountryFactory $countryFactory
     * @param WebposManagementInterface $webposManagement
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        LocationFactory $locationFactory,
        PosFactory $posFactory,
        CollectionFactory $locationCollectionFactory,
        CountryFactory $countryFactory,
        WebposManagementInterface $webposManagement,
        ObjectManagerInterface $objectManager
    ) {
        $this->_locationFactory = $locationFactory;
        $this->_posFactory = $posFactory;
        $this->_locationCollectionFactory = $locationCollectionFactory;
        $this->countryFactory = $countryFactory;
        $this->webposManagement = $webposManagement;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $isMSIEnable = $this->webposManagement->isMSIEnable();

        $locationData = [
            'name' => 'Primary Location',
            'street' => '6146 Honey Bluff Parkway',
            'city' => 'Calder',
            'region' => 'Michigan',
            'region_id' => 33,
            'country_id' => 'US',
            'country' => 'United State',
            'postcode' => '49628-7978',
            'description' => 'To distribute products for brick-and-mortar store'
        ];
        if ($isMSIEnable) {
            /** @var SourceRepositoryInterface $sourceRepository */
            $sourceRepository = $this->objectManager->create(
                SourceRepositoryInterface::class
            );
            /** @var DefaultSourceProviderInterface $defaultSourceProvider */
            $defaultSourceProvider = $this->objectManager->create(
                DefaultSourceProviderInterface::class
            );
            $defaultSource = $sourceRepository->get($defaultSourceProvider->getCode());
            /** @var DefaultStockProviderInterface $defaultStockProvider */
            $defaultStockProvider = $this->objectManager->create(
                DefaultStockProviderInterface::class
            );
            if ($defaultSource->getSourceCode()) {
                /** @var Country $country */
                $country = $this->countryFactory->create();
                $country->loadByCode($defaultSource->getCountryId());
                $locationData = [
                    'name' => 'Primary Location',
                    'street' => $defaultSource->getStreet(),
                    'city' => $defaultSource->getCity(),
                    'region' => $defaultSource->getRegion(),
                    'region_id' => $defaultSource->getRegionId(),
                    'country_id' => $defaultSource->getCountryId(),
                    'country' => $country->getName(),
                    'postcode' => $defaultSource->getPostcode(),
                    'stock_id' => $defaultStockProvider->getId(),
                    'description' => 'To distribute products for brick-and-mortar store'
                ];
            }
        }

        if ($locationData) {
            $this->_locationFactory->create()->setData($locationData)->save();
        }

        /** @var Location $locationModel */
        $locationModel = $this->_locationCollectionFactory->create()->getFirstItem();
        $posData = [
            'pos_name' => 'Primary POS',
            'location_id' => $locationModel->getId(),
            'status' => PosStatus::STATUS_ENABLED
        ];
        $this->_posFactory->create()->setData($posData)->save();
        return $this;
    }
}
