<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magestore\Webpos\Model\Customer\Attribute\Source\Location;

/**
 * Pos - Create customer telephone attribute
 */
class CreateCustomerAttributes implements DataPatchInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create();
        // add customer_attribute to customer
        $eavSetup->addAttribute(
            Customer::ENTITY,
            'customer_telephone',
            [
                'type' => 'varchar',
                'label' => 'Customer Telephone',
                'input' => 'text',
                'required' => false,
                'visible' => false,
                'system' => 0,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'sort_order' => '200'
            ]
        );

        $eavSetup->addAttribute(
            Customer::ENTITY,
            'tmp_customer_id',
            [
                'type' => 'text',
                'label' => 'Temp Customer ID',
                'input' => 'text',
                'required' => false,
                'visible' => false,
                'system' => 1,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'sort_order' => '210'
            ]
        );

        $eavSetup->addAttribute(
            Customer::ENTITY,
            'created_location_id',
            [
                'type' => 'int',
                'label' => 'Created at Location',
                'input' => 'select',
                'required' => 0,
                'visible' => 0,
                'system' => 1,
                'source' => Location::class,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'sort_order' => '220',
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'is_searchable_in_grid' => true,
            ]
        );

        return $this;
    }
}
