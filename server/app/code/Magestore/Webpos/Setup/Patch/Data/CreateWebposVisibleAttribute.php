<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Setup\Patch\Data;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\Action;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\Collection;
use Zend_Validate_Exception;

/**
 * Create webpos_visible attribute and init data
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreateWebposVisibleAttribute implements DataPatchInterface
{

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var Attribute
     */
    protected $_eavAttribute;

    /**
     * @param ProductMetadataInterface $productMetadata
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param ObjectManagerInterface $objectManager
     * @param Attribute $eavAttribute
     */
    public function __construct(
        ProductMetadataInterface $productMetadata,
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        ObjectManagerInterface $objectManager,
        Attribute $eavAttribute
    ) {
        $this->productMetadata = $productMetadata;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->objectManager = $objectManager;
        $this->_eavAttribute = $eavAttribute;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->createWebposVisibleAttribute();
        $this->addDefaultDataForAttribute();
        return $this;
    }

    /**
     * Create Webpos Visible Attribute
     *
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function createWebposVisibleAttribute()
    {
        // create attribute
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create();
        /**
         * Remove attribute webpos_visible
         */
        //Find these in the eav_entity_type table
        $action = $this->objectManager->get(Action::class);
        $attribute = $action->getAttribute('webpos_visible');
        if ($attribute) {
            $entityTypeId = $this->objectManager
                ->create(Config::class)
                ->getEntityType(ProductAttributeInterface::ENTITY_TYPE_CODE)
                ->getEntityTypeId();
            $eavSetup->removeAttribute($entityTypeId, 'webpos_visible');
        }

        $eavSetup->removeAttribute(
            Product::ENTITY,
            'webpos_visible'
        );

        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            Product::ENTITY,
            'webpos_visible',
            [
                'group' => 'General',
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Visible on POS',
                'input' => 'boolean',
                'class' => '',
                'source' => Boolean::class,
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => Boolean::VALUE_NO,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => ''
            ]
        );
    }

    /**
     * Add Default Data For Attribute
     */
    public function addDefaultDataForAttribute()
    {
        // add default data for attribute
        $attributeId = $this->_eavAttribute->getIdByCode('catalog_product', 'webpos_visible');
        $action = $this->objectManager->create(Action::class);
        $connection = $action->getConnection();
        $table = $this->moduleDataSetup->getTable('catalog_product_entity_int');
        //set invisible for default
        $productCollection = $this->objectManager->create(
            Collection::class
        );
        $visibleInSite = $this->objectManager->create(Visibility::class)
            ->getVisibleInSiteIds();

        $productCollection->addAttributeToFilter('visibility', ['in' => $visibleInSite]);

        $edition = $this->productMetadata->getEdition();
        foreach ($productCollection->getAllIds() as $productId) {
            if ($edition == 'Enterprise' || $edition == 'B2B') {
                $data = [
                    'attribute_id' => $attributeId,
                    'store_id' => 0,
                    'row_id' => $productId,
                    'value' => Boolean::VALUE_YES
                ];
            } else {
                $data = [
                    'attribute_id' => $attributeId,
                    'store_id' => 0,
                    'entity_id' => $productId,
                    'value' => Boolean::VALUE_YES
                ];
            }
            $connection->insertOnDuplicate($table, $data, ['value']);
        }
    }
}
