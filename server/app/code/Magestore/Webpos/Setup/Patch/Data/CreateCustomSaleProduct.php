<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Setup\Patch\Data;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\InventoryCatalogApi\Api\DefaultStockProviderInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Helper\Product\CustomSale;

/**
 * Pos - Create custom sale product
 */
class CreateCustomSaleProduct implements DataPatchInterface
{
    /**
     * @var State
     */
    protected $_appState;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var CustomSale
     */
    protected $customSaleHelper;

    /**
     * @var DefaultStockProviderInterface
     */
    protected $defaultStockProvider;

    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * @param State $appState
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomSale $customSaleHelper
     * @param DefaultStockProviderInterface $defaultStockProvider
     * @param StockManagementInterface $stockManagement
     */
    public function __construct(
        State $appState,
        ModuleDataSetupInterface $moduleDataSetup,
        CustomSale $customSaleHelper,
        DefaultStockProviderInterface $defaultStockProvider,
        StockManagementInterface $stockManagement
    ) {
        $this->_appState = $appState;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customSaleHelper = $customSaleHelper;
        $this->defaultStockProvider = $defaultStockProvider;
        $this->stockManagement = $stockManagement;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [
            CreateWebposVisibleAttribute::class
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->_appState->emulateAreaCode(
            Area::AREA_ADMINHTML,
            function () {
                $this->customSaleHelper->createProduct($this->moduleDataSetup);
                $this->stockManagement->addCustomSaleToStock($this->defaultStockProvider->getId());
            }
        );

        return $this;
    }
}
