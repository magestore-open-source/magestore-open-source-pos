<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Setup\Patch\Data;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Sales\Model\ResourceModel\Order;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\Collection;

/**
 * Pos - Update pos staff id data for sales order grid
 */
class UpdatePosStaffIdDataForSalesOrderGrid implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ObjectManagerInterface $objectManager
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        /* @var Collection $orderCollection */
        $orderCollection = $this->objectManager->create(
            Collection::class
        );
        $orderCollection->addFieldToSelect(['entity_id', 'pos_staff_id']);
        $orderCollection->addFieldToFilter('pos_staff_id', ['neq' => 'NULL']);
        $connection = $this->objectManager
            ->create(Order::class)
            ->getConnection();
        foreach ($orderCollection->getData() as $order) {
            $connection->insertOnDuplicate(
                $this->moduleDataSetup->getTable('sales_order_grid'),
                $order,
                ['pos_staff_id']
            );
        }

        return $this;
    }
}
