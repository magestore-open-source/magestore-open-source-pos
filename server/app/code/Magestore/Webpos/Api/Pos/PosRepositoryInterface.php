<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Pos;

interface PosRepositoryInterface
{
    /**
     * Save pos.
     *
     * @param \Magestore\Webpos\Api\Data\Pos\PosInterface $pos
     * @return \Magestore\Webpos\Api\Data\Pos\PosInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magestore\Webpos\Api\Data\Pos\PosInterface $pos);

    /**
     * Retrieve pos.
     *
     * @param int $posId
     * @return \Magestore\Webpos\Api\Data\Pos\PosInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($posId);

    /**
     * Delete pos.
     *
     * @param \Magestore\Webpos\Api\Data\Pos\PosInterface $pos
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magestore\Webpos\Api\Data\Pos\PosInterface $pos);

    /**
     * Delete pos by ID.
     *
     * @param int $posId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($posId);

    /**
     * Retrieve pos.
     *
     * @param \Magestore\Webpos\Api\Data\Pos\EnterPosInterface $pos
     * @return \Magestore\Webpos\Api\Data\MessageInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function assignPos($pos);

    /**
     * Retrieve pos collection.
     *
     * @param int $locationId
     * @return \Magestore\Webpos\Api\Data\Pos\PosInterface[]
     */
    public function getPosByLocationId($locationId);

    /**
     * Get all pos
     *
     * @return \Magestore\Webpos\Api\Data\Pos\PosInterface[]
     */
    public function getAllPos();

    /**
     * Retrieve pos matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Pos\PosSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve pos
     *
     * @param int $posId
     * @return \Magestore\Webpos\Api\Data\Pos\PosInterface
     */
    public function freePosById($posId);
}
