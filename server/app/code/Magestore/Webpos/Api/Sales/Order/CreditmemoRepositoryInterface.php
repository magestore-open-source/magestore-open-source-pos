<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Sales\Order;

/**
 * Interface CreditmemoRepositoryInterface
 */
interface CreditmemoRepositoryInterface
{
    /**
     * Create credit memo bu order id
     *
     * @param \Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface $creditmemo
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createCreditmemoByOrderId($creditmemo);

    /**
     * Process creating creditmemo request on action log
     *
     * @param string $requestIncrementId
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface|bool
     */
    public function processCreditmemoRequest($requestIncrementId);

    /**
     * Send email
     *
     * @param string $creditmemoIncrementId
     * @param string $email
     * @param string|null $incrementId
     * @return boolean
     * @throws \Exception
     */
    public function sendEmail($creditmemoIncrementId, $email, $incrementId = '');

    /**
     * Create customer
     *
     * @param \Magestore\Webpos\Api\Data\Customer\CustomerInterface $customer
     * @param string $incrementId
     * @return \Magestore\Webpos\Api\Data\Customer\CustomerInterface
     * @throws \Exception
     */
    public function createCustomer($customer, $incrementId);
}
