<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\MultiSourceInventory;

/**
 * Interface StockManagementInterface
 */
interface StockManagementInterface
{
    /**
     * Get Stock
     *
     * @return \Magento\InventoryApi\Api\Data\StockInterface|null
     */
    public function getStock();

    /**
     * Get Stock Id
     *
     * @return int|null
     */
    public function getStockId();

    /**
     * Get Linked Source Codes By Stock Id
     *
     * @param int $stockId
     * @return string[]
     */
    public function getLinkedSourceCodesByStockId($stockId);

    /**
     * Add Custom Sale To Stock
     *
     * @param int $stockId
     * @return void
     */
    public function addCustomSaleToStock($stockId);

    /**
     * Get Stock Id From Order
     *
     * @param \Magento\Sales\Model\Order|\Magento\Sales\Api\Data\OrderInterface $order
     * @return int|null
     */
    public function getStockIdFromOrder($order);
}
