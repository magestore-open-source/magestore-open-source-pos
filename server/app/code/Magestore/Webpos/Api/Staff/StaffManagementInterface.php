<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Staff;

use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Appadmin\Api\Data\Staff\StaffInterface;
use Magestore\Webpos\Api\Data\Staff\Login\LoginResultInterface;
use Magestore\Webpos\Api\Data\Staff\Logout\LogoutResultInterface;

interface StaffManagementInterface
{

    /**
     * Function login api
     *
     * @param \Magestore\Appadmin\Api\Data\Staff\StaffInterface $staff
     * @return \Magestore\Webpos\Api\Data\Staff\Login\LoginResultInterface
     * @throws \Magento\Framework\Exception\AuthorizationException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function login($staff);

    /**
     * Function continueLogin api
     *
     * @return \Magestore\Webpos\Api\Data\Staff\Login\LoginResultInterface
     */
    public function continueLogin();

    /**
     * Function Logout
     *
     * @return \Magestore\Webpos\Api\Data\Staff\Logout\LogoutResultInterface
     */

    public function logout();

    /**
     * Function authorizeSession
     *
     * @param string $phpSession
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function authorizeSession($phpSession);

    /**
     * Change password api
     *
     * @param string $username
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $confirmationPassword
     * @return boolean|string
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function changePassword($username, $oldPassword, $newPassword, $confirmationPassword);
}
