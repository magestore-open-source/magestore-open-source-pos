<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Staff;

interface SessionRepositoryInterface
{
    /**
     * Save Session.
     *
     * @param \Magestore\Webpos\Api\Data\Staff\SessionInterface $Session
     * @return \Magestore\Webpos\Api\Data\Staff\SessionInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magestore\Webpos\Api\Data\Staff\SessionInterface $Session);

    /**
     * Retrieve Session.
     *
     * @param int $id
     * @return \Magestore\Webpos\Api\Data\Staff\SessionInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Session.
     *
     * @param int $sessionId
     * @return \Magestore\Webpos\Api\Data\Staff\SessionInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBySessionId($sessionId);

    /**
     * Retrieve Session matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Staff\SessionSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve Session matching the specified criteria.
     *
     * @param int $staffId
     * @return \Magestore\Webpos\Model\ResourceModel\Staff\Session\Collection
     */
    public function getListByStaffId($staffId);

    /**
     * Delete Session.
     *
     * @param \Magestore\Webpos\Api\Data\Staff\SessionInterface $Session
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magestore\Webpos\Api\Data\Staff\SessionInterface $Session);

    /**
     * Delete Session by ID.
     *
     * @param int $sessionId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($sessionId);

    /**
     * Sign Out Pos
     *
     * @param int $posId
     * @return mixed
     */
    public function signOutPos($posId);
}
