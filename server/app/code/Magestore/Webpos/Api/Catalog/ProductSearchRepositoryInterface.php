<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Catalog;

/**
 * @api
 */
interface ProductSearchRepositoryInterface extends ProductRepositoryInterface
{
    /**
     * Get webpos search attributes
     *
     * @return mixed[]
     */
    public function getSearchAttributes();

    /**
     * Search product
     *
     * @param \Magestore\Webpos\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Catalog\SimpleProductSearchResultsInterface
     */
    public function search(\Magestore\Webpos\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Search product barcode
     *
     * @param \Magestore\Webpos\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Catalog\ProductSearchResultsInterface
     */
    public function barcode(\Magestore\Webpos\Api\SearchCriteriaInterface $searchCriteria);
}
