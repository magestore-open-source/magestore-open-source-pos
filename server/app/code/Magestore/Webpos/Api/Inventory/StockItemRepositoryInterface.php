<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Inventory;

/**
 * Interface StockItemRepository
 * @api
 */
interface StockItemRepositoryInterface
{
    /**
     * Update Stock Item data
     *
     * @param string $itemId
     * @param \Magestore\Webpos\Api\Data\Inventory\StockItemInterface $stockItem
     * @return int
     */
    public function updateStockItem($itemId, \Magestore\Webpos\Api\Data\Inventory\StockItemInterface $stockItem);

    /**
     * Update Stock Item data
     *
     * @param \Magestore\Webpos\Api\Data\Inventory\StockItemInterface[] $stockItems
     * @return bool
     */
    public function massUpdateStockItems($stockItems);

    /**
     * Load Stock Item data collection by given search criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Inventory\StockSearchResultsInterface
     */
    public function getStockItems(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Sync Stock Item data collection by given search criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Inventory\StockSearchResultsInterface
     */
    public function sync(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Get Available Qty
     *
     * @param int $product_id
     * @return \Magestore\Webpos\Api\Data\Inventory\AvailableQtyInterface
     */
    public function getAvailableQty($product_id);
}
