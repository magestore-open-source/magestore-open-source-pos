<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Checkout;

interface CouponManagementInterface
{
    /**
     * Check Coupon
     *
     * @param \Magestore\Webpos\Api\Data\Checkout\QuoteInterface $quote
     * @param string $coupon_code
     * @return \Magestore\Webpos\Api\Data\Checkout\RuleInterface[]
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function checkCoupon(\Magestore\Webpos\Api\Data\Checkout\QuoteInterface $quote, $coupon_code = null);
}
