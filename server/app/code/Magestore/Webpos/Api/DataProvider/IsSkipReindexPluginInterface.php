<?php
/**
 * Copyright © 2018 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magestore\Webpos\Api\DataProvider;

interface IsSkipReindexPluginInterface
{
    const IS_SKIP_REINDEX_PLUGIN = 'is_skip_reindex_plugin';

    /**
     * Set is skip reindex plugin for save multiple
     *
     * @param bool $isSkipReindexPlugin
     * @return void
     */
    public function setIsSkipReindexPlugin(bool $isSkipReindexPlugin);

    /**
     * Get is skip reindex plugin for save multiple
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsSkipReindexPlugin(): bool;
}
