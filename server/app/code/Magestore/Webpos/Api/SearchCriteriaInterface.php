<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api;

/**
 * @api
 */
interface SearchCriteriaInterface extends \Magento\Framework\Api\SearchCriteriaInterface
{
    /**
     * Get query string
     *
     * @return string
     */
    public function getQueryString();

    /**
     * Set query string
     *
     * @param string $queryString
     * @return SearchCriteriaInterface
     */
    public function setQueryString($queryString);

    /**
     * Get is limit
     *
     * @return int|null
     */
    public function getIsLimit();

    /**
     * Set is limit
     *
     * @param int|null $isLimit
     * @return SearchCriteriaInterface
     */
    public function setIsLimit($isLimit);

    /**
     * Get is hold
     *
     * @return int|null
     */
    public function getIsHold();

    /**
     * Set is hold
     *
     * @param int|null $isHold
     * @return SearchCriteriaInterface
     */
    public function setIsHold($isHold);
}
