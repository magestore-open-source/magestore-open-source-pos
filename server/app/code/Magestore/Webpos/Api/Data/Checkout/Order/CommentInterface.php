<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Checkout\Order;

interface CommentInterface
{
    const ENTITY_ID = 'entity_id';
    const IS_VISIBLE_ON_FRONT = 'is_visible_on_front';
    const COMMENT = 'comment';
    const CREATED_AT = 'created_at';

    /**
     * Gets the created-at timestamp for the order status history.
     *
     * @return string|null Created-at timestamp.
     */
    public function getCreatedAt();

    /**
     * Sets the created-at timestamp for the order status history.
     *
     * @param string|null $createdAt timestamp
     * @return CommentInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Gets the ID for the order status history.
     *
     * @return int|null Order status history ID.
     */
    public function getEntityId();

    /**
     * Sets entity ID.
     *
     * @param int|null $entityId
     * @return CommentInterface
     */
    public function setEntityId($entityId);

    /**
     * Gets the is-visible-on-storefront flag value for the order status history.
     *
     * @return int|null Is-visible-on-storefront flag value.
     */
    public function getIsVisibleOnFront();

    /**
     * Sets the is-visible-on-storefront flag value for the order status history.
     *
     * @param int|null $isVisibleOnFront
     * @return CommentInterface
     */
    public function setIsVisibleOnFront($isVisibleOnFront);

    /**
     * Gets the comment for the order status history.
     *
     * @return string|null Comment.
     */
    public function getComment();

    /**
     * Sets the comment for the order status history.
     *
     * @param string|null $comment
     * @return CommentInterface
     */
    public function setComment($comment);
}
