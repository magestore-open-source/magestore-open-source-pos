<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Pos;

/**
 * Interface PosInterface
 */
interface PosInterface
{
    const POS_ID = "pos_id";
    const POS_NAME = 'pos_name';
    const LOCATION_ID = 'location_id';
    const STAFF_ID = 'staff_id';
    const STATUS = 'status';

    /**
     * Get pos id
     *
     * @return string|null
     * @api
     */
    public function getPosId();

    /**
     * Set pos id
     *
     * @param string $posId
     * @return PosInterface
     * @api
     */
    public function setPosId($posId);

    /**
     * Get pos name
     *
     * @return string|null
     * @api
     */
    public function getPosName();

    /**
     * Set pos name
     *
     * @param string $posName
     * @return PosInterface
     * @api
     */
    public function setPosName($posName);

    /**
     * Get location id
     *
     * @return string|null
     * @api
     */
    public function getLocationId();

    /**
     * Set location id
     *
     * @param string $locationId
     * @return PosInterface
     * @api
     */
    public function setLocationId($locationId);

    /**
     * Get staff id
     *
     * @return string|null
     * @api
     */
    public function getStaffId();

    /**
     * Set staff id
     *
     * @param string $staffId
     * @return PosInterface
     * @api
     */
    public function setStaffId($staffId);

    /**
     * Get status
     *
     * @return string|null
     * @api
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param string $status
     * @return PosInterface
     * @api
     */
    public function setStatus($status);
}
