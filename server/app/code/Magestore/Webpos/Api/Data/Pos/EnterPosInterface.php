<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Pos;

/**
 * Interface EnterPosInterface
 */
interface EnterPosInterface
{
    const POS_ID = "pos_id";
    const LOCATION_ID = 'location_id';

    /**
     * Get pos id
     *
     * @return string|null
     * @api
     */
    public function getPosId();

    /**
     * Set pos id
     *
     * @param string $posId
     * @return EnterPosInterface
     * @api
     */
    public function setPosId($posId);

    /**
     * Get location id
     *
     * @return string|null
     * @api
     */
    public function getLocationId();

    /**
     * Set location id
     *
     * @param string $locationId
     * @return EnterPosInterface
     * @api
     */
    public function setLocationId($locationId);
}
