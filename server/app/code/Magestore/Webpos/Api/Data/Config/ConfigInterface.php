<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Config;

/**
 * @api
 *
 * Pos - Interface ConfigInterface
 */
interface ConfigInterface
{
    const SETTINGS = 'settings';
    const CURRENCIES = 'currencies';
    const BASE_CURRENCY_CODE = 'base_currency_code';
    const CURRENT_CURRENCY_CODE = 'current_currency_code';
    const PRICE_FORMATS = 'price_formats';
    const GUEST_CUSTOMER = 'guest_customer';
    const STORE_NAME = 'store_name';
    const CUSTOMER_GROUPS = 'customer_groups';
    const SHIPPING = 'shipping';
    const PAYMENT = 'payment';
    const TAX_RATES = 'tax_rates';
    const TAX_RULES = 'tax_rules';
    const PRODUCT_TAX_CLASSES = 'product_tax_classes';
    const IS_PRIMARY_LOCATION = 'is_primary_location';
    const CUSTOM_SALE_PRODUCT_ID = 'custom_sale_product_id';
    const ENABLE_MODULES = 'enable_modules';
    const MAX_DISCOUNT_PERCENT = 'max_discount_percent';
    const ROOT_CATEGORY_ID = 'ROOT_CATEGORY_ID';
    const CUSTOMER_FORM = 'customer_form';
    const CUSTOMER_ADDRESS_FORM = 'customer_address_form';
    const COUNTRIES_WITH_OPTIONAL_ZIP = 'countries_with_optional_zip';
    const MAGENTO_VERSION = 'magento_version';
    const SERVER_TIMEZONE_OFFSET = 'server_timezone_offset';
    const CUSTOMER_CUSTOM_ATTRIBUTES = 'customer_custom_attributes';
    const CUSTOMER_ADDRESS_CUSTOM_ATTRIBUTES = 'customer_address_custom_attributes';

    /**
     * Get settings
     *
     * @return \Magestore\Webpos\Api\Data\Config\SystemConfigInterface[]
     * @api
     */
    public function getSettings();

    /**
     * Set settings
     *
     * @param \Magestore\Webpos\Api\Data\Config\SystemConfigInterface[] $settings
     * @return ConfigInterface
     * @api
     */
    public function setSettings($settings);

    /**
     * Get currencies
     *
     * @return \Magestore\Webpos\Api\Data\Config\CurrencyInterface[]
     * @api
     */
    public function getCurrencies();

    /**
     * Set currencies
     *
     * @param \Magestore\Webpos\Api\Data\Config\CurrencyInterface[] $currencies
     * @return ConfigInterface
     * @api
     */
    public function setCurrencies($currencies);

    /**
     * Get base currency code
     *
     * @return string
     * @api
     */
    public function getBaseCurrencyCode();

    /**
     * Set base currency code
     *
     * @param string $baseCurrencyCode
     * @return ConfigInterface
     * @api
     */
    public function setBaseCurrencyCode($baseCurrencyCode);

    /**
     * Get current currency code
     *
     * @return string
     * @api
     */
    public function getCurrentCurrencyCode();

    /**
     * Set current currency code
     *
     * @param string $currentCurrencyCode
     * @return ConfigInterface
     * @api
     */
    public function setCurrentCurrencyCode($currentCurrencyCode);

    /**
     * Get price format
     *
     * @return \Magestore\Webpos\Api\Data\Config\PriceFormatInterface[]
     * @api
     */
    public function getPriceFormats();

    /**
     * Set price format
     *
     * @param \Magestore\Webpos\Api\Data\Config\PriceFormatInterface[] $priceFormats
     * @return ConfigInterface
     * @api
     */
    public function setPriceFormats($priceFormats);

    /**
     * Get price format
     *
     * @return \Magestore\Webpos\Api\Data\Config\GuestCustomerInterface
     * @api
     */
    public function getGuestCustomer();

    /**
     * Set price format
     *
     * @param \Magestore\Webpos\Api\Data\Config\GuestCustomerInterface $guestInfo
     * @return ConfigInterface
     * @api
     */
    public function setGuestCustomer($guestInfo);

    /**
     * Get Store Name
     *
     * @return string
     */
    public function getStoreName();

    /**
     * Set Store Name
     *
     * @param string $storeName
     * @return ConfigInterface
     */
    public function setStoreName($storeName);

    /**
     * Get Customer Groups
     *
     * @return \Magestore\Webpos\Api\Data\Config\CustomerGroupInterface[]
     */
    public function getCustomerGroups();

    /**
     * Set Customer Groups
     *
     * @param \Magestore\Webpos\Api\Data\Config\CustomerGroupInterface[] $customerGroups
     * @return ConfigInterface
     */
    public function setCustomerGroups($customerGroups);

    /**
     * Get Shipping
     *
     * @return \Magestore\Webpos\Api\Data\Config\ShippingInterface
     */
    public function getShipping();

    /**
     * Set Shipping
     *
     * @param \Magestore\Webpos\Api\Data\Config\ShippingInterface $shipping
     * @return ConfigInterface
     */
    public function setShipping($shipping);

    /**
     * Get Payment
     *
     * @return \Magestore\Webpos\Api\Data\Config\PaymentInterface
     */
    public function getPayment();

    /**
     * Set Payment
     *
     * @param \Magestore\Webpos\Api\Data\Config\PaymentInterface $payment
     * @return ConfigInterface
     */
    public function setPayment($payment);

    /**
     * Get Tax Rates
     *
     * @return \Magestore\Webpos\Api\Data\Tax\TaxRateInterface[]
     */
    public function getTaxRates();

    /**
     * Set Tax Rates
     *
     * @param \Magestore\Webpos\Api\Data\Tax\TaxRateInterface[] $taxRates
     * @return ConfigInterface
     */
    public function setTaxRates($taxRates);

    /**
     * Get Tax Rules
     *
     * @return \Magestore\Webpos\Api\Data\Tax\TaxRuleInterface[]
     */
    public function getTaxRules();

    /**
     * Set Tax Rules
     *
     * @param \Magestore\Webpos\Api\Data\Tax\TaxRuleInterface[] $taxRules
     * @return ConfigInterface
     */
    public function setTaxRules($taxRules);

    /**
     * Get Product Tax Classes
     *
     * @return \Magestore\Webpos\Api\Data\Config\ProductTaxClassesInterface[]
     */
    public function getProductTaxClasses();

    /**
     * Set Product Tax Classes
     *
     * @param \Magestore\Webpos\Api\Data\Config\ProductTaxClassesInterface[] $taxClasses
     * @return ConfigInterface
     */
    public function setProductTaxClasses($taxClasses);

    /**
     * Get Is Primary Location
     *
     * @return boolean
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsPrimaryLocation();

    /**
     * Set Is Primary Location
     *
     * @param boolean $isPrimaryLocation
     * @return ConfigInterface
     */
    public function setIsPrimaryLocation($isPrimaryLocation);

    /**
     * Get custom sale product id
     *
     * @return int|null
     */
    public function getCustomSaleProductId();

    /**
     * Set custom sale product id
     *
     * @param int|null $customSaleProductId
     * @return ConfigInterface
     * @api
     */
    public function setCustomSaleProductId($customSaleProductId);

    /**
     * Get enable modules
     *
     * @return string[]
     * @api
     */
    public function getEnableModules();

    /**
     * Set enable modules
     *
     * @param string[] $enableModules
     * @return ConfigInterface
     * @api
     */
    public function setEnableModules($enableModules);

    /**
     * Get max discount percent
     *
     * @return float
     */
    public function getMaxDiscountPercent();

    /**
     * Set Max Discount Percent
     *
     * @param float $maxDiscountPercent
     * @return ConfigInterface
     */
    public function setMaxDiscountPercent($maxDiscountPercent);

    /**
     * Get root category
     *
     * @return int|null
     * @api
     */
    public function getRootCategoryId();

    /**
     * Set root category
     *
     * @param int $rootCategoryId
     * @return ConfigInterface
     * @api
     */
    public function setRootCategoryId($rootCategoryId);

    /**
     * Get Customer Form
     *
     * @return \Magento\Customer\Api\Data\AttributeMetadataInterface[]
     */
    public function getCustomerForm();

    /**
     * Set Customer Form
     *
     * @param \Magento\Customer\Api\Data\AttributeMetadataInterface[] $customerForm
     * @return ConfigInterface
     */
    public function setCustomerForm($customerForm);

    /**
     * Get Customer Address Form
     *
     * @return \Magento\Customer\Api\Data\AttributeMetadataInterface[]
     */
    public function getCustomerAddressForm();

    /**
     * Set Customer Address Form
     *
     * @param \Magento\Customer\Api\Data\AttributeMetadataInterface[] $customerAddressForm
     * @return ConfigInterface
     */
    public function setCustomerAddressForm($customerAddressForm);

    /**
     * Get Customer Custom Attributes
     *
     * @return \Magento\Customer\Api\Data\AttributeMetadataInterface[]
     */
    public function getCustomerCustomAttributes();

    /**
     * Set Customer Custom Attributes
     *
     * @param \Magento\Customer\Api\Data\AttributeMetadataInterface[] $customerCustomAttributes
     * @return ConfigInterface
     */
    public function setCustomerCustomAttributes($customerCustomAttributes);

    /**
     * Get Customer Address Custom Attributes
     *
     * @return \Magento\Customer\Api\Data\AttributeMetadataInterface[]
     */
    public function getCustomerAddressCustomAttributes();

    /**
     * Set Customer Address Custom Attributes
     *
     * @param \Magento\Customer\Api\Data\AttributeMetadataInterface[] $customerAddressCustomAttributes
     * @return ConfigInterface
     */
    public function setCustomerAddressCustomAttributes($customerAddressCustomAttributes);

    /**
     * Get Countries With Optional Zip
     *
     * @return string
     */
    public function getCountriesWithOptionalZip();

    /**
     * Set Countries With Optional Zip
     *
     * @param string $countriesWithOptionalZip
     * @return ConfigInterface
     */
    public function setCountriesWithOptionalZip($countriesWithOptionalZip);

    /**
     * Get Magento Version
     *
     * @return string
     */
    public function getMagentoVersion();

    /**
     * Set Magento Version
     *
     * @param string $magentoVersion
     * @return ConfigInterface
     */
    public function setMagentoVersion($magentoVersion);

    /**
     * Get Server Timezone Offset
     *
     * @return int
     */
    public function getServerTimezoneOffset();

    /**
     * Set Server Timezone Offset
     *
     * @param int $serverTimezoneOffset
     * @return ConfigInterface
     */
    public function setServerTimezoneOffset($serverTimezoneOffset);
}
