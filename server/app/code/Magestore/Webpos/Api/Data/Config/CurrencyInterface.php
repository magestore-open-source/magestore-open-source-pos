<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Config;

/**
 * Interface CurrencyInterface
 * @api
 */
interface CurrencyInterface
{
    const CODE = 'code';
    const CURRENCY_NAME = 'currency_name';
    const CURRENCY_RATE = 'currency_rate';
    const CURRENCY_SYMBOL = 'currency_symbol';
    const IS_DEFAULT = 'is_default';

    /**
     * Get code
     *
     * @return string
     * @api
     */
    public function getCode();

    /**
     * Set code
     *
     * @param string $code
     * @return CurrencyInterface
     * @api
     */
    public function setCode($code);

    /**
     * Get currency name
     *
     * @return string
     * @api
     */
    public function getCurrencyName();

    /**
     * Set currency name
     *
     * @param string $currencyName
     * @return CurrencyInterface
     * @api
     */
    public function setCurrencyName($currencyName);

    /**
     * Get currency rate
     *
     * @return float
     * @api
     */
    public function getCurrencyRate();

    /**
     * Set currency rate
     *
     * @param float $currencyRate
     * @return CurrencyInterface
     * @api
     */
    public function setCurrencyRate($currencyRate);

    /**
     * Get currency symbol
     *
     * @return string
     * @api
     */
    public function getCurrencySymbol();

    /**
     * Set currency symbol
     *
     * @param string $currencySymbol
     * @return CurrencyInterface
     * @api
     */
    public function setCurrencySymbol($currencySymbol);

    /**
     * Get is default
     *
     * @return int
     * @api
     */
    public function getIsDefault();

    /**
     * Set is default
     *
     * @param int $isDefault
     * @return CurrencyInterface
     * @api
     */
    public function setIsDefault($isDefault);
}
