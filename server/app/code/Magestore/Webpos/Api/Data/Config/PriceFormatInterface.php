<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Config;

/**
 * @api
 */
interface PriceFormatInterface
{
    const CURRENCY_CODE = 'currency_code';
    const DECIMAL_SYMBOL = 'decimal_symbol';
    const GROUP_LENGTH = 'group_length';
    const GROUP_SYMBOL = 'group_symbol';
    const INTEGER_REQUIRED = 'integer_required';
    const PATTERN = 'pattern';
    const PRECISION = 'precision';
    const REQUIRED_PRECISION = 'required_precision';

    /**
     * Get code
     *
     * @return string
     * @api
     */
    public function getCurrencyCode();

    /**
     * Set code
     *
     * @param string $currencyCode
     * @return PriceFormatInterface
     * @api
     */
    public function setCurrencyCode($currencyCode);

    /**
     * Get decimal symbol
     *
     * @return string
     * @api
     */
    public function getDecimalSymbol();

    /**
     * Set decimal symbol
     *
     * @param string $decimalSymbol
     * @return PriceFormatInterface
     * @api
     */
    public function setDecimalSymbol($decimalSymbol);

    /**
     * Get group symbol
     *
     * @return string
     * @api
     */
    public function getGroupSymbol();

    /**
     * Set group symbol
     *
     * @param string $groupSymbol
     * @return PriceFormatInterface
     * @api
     */
    public function setGroupSymbol($groupSymbol);

    /**
     * Get group length
     *
     * @return int
     * @api
     */
    public function getGroupLength();

    /**
     * Set group length
     *
     * @param int $groupLength
     * @return PriceFormatInterface
     * @api
     */
    public function setGroupLength($groupLength);

    /**
     * Get integer required
     *
     * @return int
     * @api
     */
    public function getIntegerRequired();

    /**
     * Set integer required
     *
     * @param int $integerRequired
     * @return PriceFormatInterface
     * @api
     */
    public function setIntegerRequired($integerRequired);

    /**
     * Get pattern
     *
     * @return string
     * @api
     */
    public function getPattern();

    /**
     * Set pattern
     *
     * @param string $pattern
     * @return PriceFormatInterface
     * @api
     */
    public function setPattern($pattern);

    /**
     * Get precision
     *
     * @return int
     * @api
     */
    public function getPrecision();

    /**
     * Set precision
     *
     * @param int $precision
     * @return PriceFormatInterface
     * @api
     */
    public function setPrecision($precision);

    /**
     * Get required precision
     *
     * @return int
     * @api
     */
    public function getRequiredPrecision();

    /**
     * Set required precision
     *
     * @param int $requiredPrecision
     * @return PriceFormatInterface
     * @api
     */
    public function setRequiredPrecision($requiredPrecision);
}
