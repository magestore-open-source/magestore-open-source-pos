<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Config;

/**
 * @api
 */
interface ShippingInterface
{
    const SHIPPING_METHODS = 'shipping_methods';
    const DELIVERY_DATE = 'delivery_date';
    const DEFAULT_SHIPPING_TITLE = 'default_shipping_title';

    /**
     * Get Shipping Methods
     *
     * @return string
     */
    public function getShippingMethods();

    /**
     * Set Shipping Methods
     *
     * @param string $shippingMethod
     * @return ShippingInterface
     */
    public function setShippingMethods($shippingMethod);

    /**
     * Get Delivery Date
     *
     * @return int
     */
    public function getDeliveryDate();

    /**
     * Set Delivery Date
     *
     * @param int $deliveryDate
     * @return ShippingInterface
     */
    public function setDeliveryDate($deliveryDate);

    /**
     * Get Default Shipping Title
     *
     * @return string
     */
    public function getDefaultShippingTitle();

    /**
     * Set Default Shipping Title
     *
     * @param string $defaultShippingTitle
     * @return ShippingInterface
     */
    public function setDefaultShippingTitle($defaultShippingTitle);
}
