<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Config;

/**
 * @api
 */
interface GuestCustomerInterface
{
    const STATUS = 'status';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const EMAIL = 'email';

    /**
     * Get status
     *
     * @return int
     * @api
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param int $status
     * @return GuestCustomerInterface
     * @api
     */
    public function setStatus($status);

    /**
     * Get first name
     *
     * @return string
     * @api
     */
    public function getFirstName();

    /**
     * Set first name
     *
     * @param string $firstName
     * @return GuestCustomerInterface
     * @api
     */
    public function setFirstName($firstName);

    /**
     * Get last name
     *
     * @return string
     * @api
     */
    public function getLastName();

    /**
     * Set last name
     *
     * @param string $lastName
     * @return GuestCustomerInterface
     * @api
     */
    public function setLastName($lastName);

    /**
     * Get email
     *
     * @return string
     * @api
     */
    public function getEmail();

    /**
     * Set email
     *
     * @param string $email
     * @return GuestCustomerInterface
     * @api
     */
    public function setEmail($email);
}
