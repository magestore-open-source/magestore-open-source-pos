<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Log;

/**
 * Interface OrderDeletedInterface
 */
interface OrderDeletedInterface
{
    const ID = 'id';
    const ORDER_ID = 'order_id';
    const DELETED_AT = 'deleted_at';

    /**
     * Get Id
     *
     * @return int
     */
    public function getId();

    /**
     * Set Id
     *
     * @param int $id
     * @return OrderDeletedInterface
     */
    public function setId($id);

    /**
     * Get order id
     *
     * @return int
     */
    public function getOrderId();

    /**
     * Set order id
     *
     * @param int $productId
     * @return OrderDeletedInterface
     */
    public function setOrderId($productId);

    /**
     * Get deleted at
     *
     * @return string
     */
    public function getDeletedAt();

    /**
     * Set deleted at
     *
     * @param string $deletedAt
     * @return OrderDeletedInterface
     */
    public function setDeletedAt($deletedAt);
}
