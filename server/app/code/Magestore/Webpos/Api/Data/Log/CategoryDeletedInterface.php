<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Log;

/**
 * Interface CategoryDeletedInterface
 */
interface CategoryDeletedInterface
{
    const ID = 'id';
    const CATEGORY_ID = 'category_id';
    const ROOT_CATEGORY_ID = 'root_category_id';
    const DELETED_AT = 'deleted_at';

    /**
     * Get Id
     *
     * @return int
     */
    public function getId();

    /**
     * Set Id
     *
     * @param int $id
     * @return CategoryDeletedInterface
     */
    public function setId($id);

    /**
     * Get category id
     *
     * @return int
     */
    public function getCategoryId();

    /**
     * Set category id
     *
     * @param int $categoryId
     * @return CategoryDeletedInterface
     */
    public function setCategoryId($categoryId);

    /**
     * Get category id
     *
     * @return int
     */
    public function getRootCategoryId();

    /**
     * Set category id
     *
     * @param int $rootCategoryId
     * @return CategoryDeletedInterface
     */
    public function setRootCategoryId($rootCategoryId);

    /**
     * Get deleted at
     *
     * @return string
     */
    public function getDeletedAt();

    /**
     * Set deleted at
     *
     * @param string $deletedAt
     * @return CategoryDeletedInterface
     */
    public function setDeletedAt($deletedAt);
}
