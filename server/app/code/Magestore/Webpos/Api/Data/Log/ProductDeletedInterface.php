<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Log;

/**
 * Interface ProductDeletedInterface
 */
interface ProductDeletedInterface
{
    const ID = 'id';
    const PRODUCT_ID = 'product_id';
    const DELETED_AT = 'deleted_at';

    const STOCK_ID = 'stock_id';

    /**
     * Get Id
     *
     * @return int
     */
    public function getId();

    /**
     * Set Id
     *
     * @param int $id
     * @return ProductDeletedInterface
     */
    public function setId($id);

    /**
     * Get product id
     *
     * @return int
     */
    public function getProductId();

    /**
     * Set product id
     *
     * @param int $productId
     * @return ProductDeletedInterface
     */
    public function setProductId($productId);

    /**
     * Get deleted at
     *
     * @return string
     */
    public function getDeletedAt();

    /**
     * Set deleted at
     *
     * @param string $deletedAt
     * @return ProductDeletedInterface
     */
    public function setDeletedAt($deletedAt);

    /**
     * Get stock id
     *
     * @return int|null
     */
    public function getStockId();

    /**
     * Set stock id
     *
     * @param int|null $stockId
     * @return ProductDeletedInterface
     */
    public function setStockId($stockId);
}
