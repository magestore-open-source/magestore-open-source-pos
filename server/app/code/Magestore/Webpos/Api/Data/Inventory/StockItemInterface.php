<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Inventory;

/**
 * @api
 */
interface StockItemInterface
{
    /**
     * Get Product Id
     *
     * @return int
     */
    public function getProductId();

    /**
     * Set Product Id
     *
     * @param int $productId
     * @return StockItemInterface
     */
    public function setProductId($productId);

    /**
     * Get Item Id
     *
     * @return int
     */
    public function getItemId();

    /**
     * Set Item Id
     *
     * @param int $itemId
     * @return StockItemInterface
     */
    public function setItemId($itemId);

    /**
     * Get Stock Id
     *
     * @return int
     */
    public function getStockId();

    /**
     * Set Stock Id
     *
     * @param int $stockId
     * @return StockItemInterface
     */
    public function setStockId($stockId);

    /**
     * Product SKU
     *
     * @return string|null
     */
    public function getSku();

    /**
     * Set Product SKU
     *
     * @param string $sku
     * @return StockItemInterface
     */
    public function setSku($sku);

    /**
     * Product Name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set Product Name
     *
     * @param string $name
     * @return StockItemInterface
     */
    public function setName($name);

    /**
     * Get Qty
     *
     * @return float
     */
    public function getQty();

    /**
     * Set Qty
     *
     * @param float $qty
     * @return StockItemInterface
     */
    public function setQty($qty);

    /**
     * Get Quantity
     *
     * @return float
     */
    public function getQuantity();

    /**
     * Set Quantity
     *
     * @param float $quantity
     * @return StockItemInterface
     */
    public function setQuantity($quantity);

    /**
     * Retrieve Stock Availability
     *
     * @return bool|int
     */
    public function getIsInStock();

    /**
     * Set Stock Availability
     *
     * @param bool|int $isInStock
     * @return StockItemInterface
     */
    public function setIsInStock($isInStock);

    /**
     * Get Use Config Manage Stock
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseConfigManageStock();

    /**
     * Set Use Config Manage Stock
     *
     * @param bool $useConfigManageStock
     * @return StockItemInterface
     */
    public function setUseConfigManageStock($useConfigManageStock);

    /**
     * Retrieve can Manage Stock
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getManageStock();

    /**
     * Set can Manage Stock
     *
     * @param bool $manageStock
     * @return StockItemInterface
     */
    public function setManageStock($manageStock);

    /**
     * Retrieve can backorder
     *
     * @return int
     */
    public function getBackorders();

    /**
     * Set can backorder
     *
     * @param int $backorders
     * @return StockItemInterface
     */
    public function setBackorders($backorders);

    /**
     * Get Use Config Backorders
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseConfigBackorders();

    /**
     * Set Use Config Backorders
     *
     * @param bool $useConfigBackorders
     * @return StockItemInterface
     */
    public function setUseConfigBackorders($useConfigBackorders);

    /**
     * Get Min Qty
     *
     * @return int|float
     */
    public function getMinQty();

    /**
     * Set Min Qty
     *
     * @param int|float $minQty
     * @return StockItemInterface
     */
    public function setMinQty($minQty);

    /**
     * Get Use Config Min Qty
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseConfigMinQty();

    /**
     * Set Use Config Min Qty
     *
     * @param bool $useConfigMinQty
     * @return StockItemInterface
     */
    public function setUseConfigMinQty($useConfigMinQty);

    /**
     * Get Min Sale Qty
     *
     * @return float
     */
    public function getMinSaleQty();

    /**
     * Set Min Sale Qty
     *
     * @param float $minSaleQty
     * @return StockItemInterface
     */
    public function setMinSaleQty($minSaleQty);

    /**
     * Get Use Config Min Sale Qty
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseConfigMinSaleQty();

    /**
     * Set Use Config Min Sale Qty
     *
     * @param bool $useConfigMinSaleQty
     * @return StockItemInterface
     */
    public function setUseConfigMinSaleQty($useConfigMinSaleQty);

    /**
     * Get Max Sale Qty
     *
     * @return float
     */
    public function getMaxSaleQty();

    /**
     * Set Max Sale Qty
     *
     * @param float $maxSaleQty
     * @return StockItemInterface
     */
    public function setMaxSaleQty($maxSaleQty);

    /**
     * Get Use Config Max Sale Qty
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseConfigMaxSaleQty();

    /**
     * Set Use Config Max Sale Qty
     *
     * @param bool $useConfigMaxSaleQty
     * @return StockItemInterface
     */
    public function setUseConfigMaxSaleQty($useConfigMaxSaleQty);

    /**
     * Get Updated Time
     *
     * @return string
     */
    public function getUpdatedTime();

    /**
     * Set Updated Time
     *
     * @param string $updatedTime
     * @return StockItemInterface
     */
    public function setUpdatedTime($updatedTime);

    /**
     * Get Use Config Qty Increments
     *
     * @return int|null
     */
    public function getUseConfigQtyIncrements();

    /**
     * Set Use Config Qty Increments
     *
     * @param int $useConfigQtyIncrements
     * @return StockItemInterface
     */
    public function setUseConfigQtyIncrements($useConfigQtyIncrements);

    /**
     * Get Qty Increments
     *
     * @return string
     */
    public function getQtyIncrements();

    /**
     * Set Qty Increments
     *
     * @param string $qtyIncrements
     * @return StockItemInterface
     */
    public function setQtyIncrements($qtyIncrements);

    /**
     * Get Is Qty Decimal
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsQtyDecimal();

    /**
     * Set Is Qty Decimal
     *
     * @param bool $isQtyDecimal
     * @return StockItemInterface
     */
    public function setIsQtyDecimal($isQtyDecimal);

    /**
     * Get Use Config Enable Qty Inc
     *
     * @return int|null
     */
    public function getUseConfigEnableQtyInc();

    /**
     * Set Use Config Enable Qty Inc
     *
     * @param int $useConfigEnableQtyInc
     * @return StockItemInterface
     */
    public function setUseConfigEnableQtyInc($useConfigEnableQtyInc);

    /**
     * Get Enable Qty Increments
     *
     * @return int|null
     */
    public function getEnableQtyIncrements();

    /**
     * Set Enable Qty Increments
     *
     * @param int $enableQtyIncrements
     * @return StockItemInterface
     */
    public function setEnableQtyIncrements($enableQtyIncrements);
}
