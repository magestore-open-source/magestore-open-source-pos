<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Location;

interface LocationInterface
{
    const LOCATION_ID = 'location_id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const TELEPHONE = 'telephone';
    const EMAIL = 'email';
    const STREET = 'street';
    const CITY = 'city';
    const REGION = 'region';
    const REGION_ID = 'region_id';
    const COUNTRY = 'country';
    const COUNTRY_ID = 'country_id';
    const POSTCODE = 'postcode';

    const STOCK_ID = 'stock_id';

    /**
     * Get Location Id
     *
     * @return int|null
     */
    public function getLocationId();

    /**
     * Set Location Id
     *
     * @param int $locationId
     * @return LocationInterface
     */
    public function setLocationId($locationId);

    /**
     * Get Name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set Name
     *
     * @param string $name
     * @return LocationInterface
     */
    public function setName($name);

    /**
     * Get Description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Set Description
     *
     * @param string $description
     * @return LocationInterface
     */
    public function setDescription($description);

    /**
     * Get Telephone
     *
     * @return string
     */
    public function getTelephone();

    /**
     * Set Telephone
     *
     * @param string $telephone
     * @return LocationInterface
     */
    public function setTelephone($telephone);

    /**
     * Get Email
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Set Email
     *
     * @param string $email
     * @return LocationInterface
     */
    public function setEmail($email);

    /**
     * Get Street
     *
     * @return string|null
     */
    public function getStreet();

    /**
     * Set Street
     *
     * @param string $street
     * @return LocationInterface
     */
    public function setStreet($street);

    /**
     * Get City
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Set City
     *
     * @param string $city
     * @return LocationInterface
     */
    public function setCity($city);

    /**
     * Get Region
     *
     * @return string|null
     */
    public function getRegion();

    /**
     * Set Region
     *
     * @param string $region
     * @return LocationInterface
     */
    public function setRegion($region);

    /**
     * Get Region Id
     *
     * @return int|null
     */
    public function getRegionId();

    /**
     * Set Region Id
     *
     * @param int $regionId
     * @return LocationInterface
     */
    public function setRegionId($regionId);

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Set country
     *
     * @param string $country
     * @return LocationInterface
     */
    public function setCountry($country);

    /**
     * Get Country Id
     *
     * @return string|null
     */
    public function getCountryId();

    /**
     * Set Country Id
     *
     * @param string $countryId
     * @return LocationInterface
     */
    public function setCountryId($countryId);

    /**
     * Get Postcode
     *
     * @return string|null
     */
    public function getPostcode();

    /**
     * Set Postcode
     *
     * @param string $postcode
     * @return LocationInterface
     */
    public function setPostcode($postcode);

    /**
     * Get Address
     *
     * @return \Magestore\Webpos\Api\Data\Staff\Login\Location\AddressInterface
     */
    public function getAddress();

    /**
     * Set Address
     *
     * @param \Magestore\Webpos\Api\Data\Staff\Login\Location\AddressInterface $address
     * @return LocationInterface
     */
    public function setAddress($address);

    /**
     * Get Stock Id
     *
     * @return int|null
     */
    public function getStockId();

    /**
     * Set Stock Id
     *
     * @param int|null $stockId
     * @return LocationInterface
     */
    public function setStockId($stockId);
}
