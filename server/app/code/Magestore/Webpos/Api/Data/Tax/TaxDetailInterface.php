<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Tax;

/**
 * Tax class interface.
 * @api
 * @since 100.0.2
 */
interface TaxDetailInterface
{
    const ID = 'id';
    const AMOUNT = 'amount';
    const BASE_AMOUNT = 'base_amount';
    const PERCENT = 'percent';
    const PROCESS = 'process';
    const RATES = 'rates';
    const CODE = 'code';
    const TITLE = 'title';
    const BASE_REAL_AMOUNT = 'base_real_amount';

    /**
     * Get id
     *
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get code
     *
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     *
     * @param string $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount();

    /**
     * Set amount
     *
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount);

    /**
     * Get base amount
     *
     * @return float
     */
    public function getBaseAmount();

    /**
     * Set base amount
     *
     * @param float $baseAmount
     * @return $this
     */
    public function setBaseAmount($baseAmount);

    /**
     * Get base real amount
     *
     * @return float
     */
    public function getBaseRealAmount();

    /**
     * Set base real amount
     *
     * @param float $baseRealAmount
     * @return $this
     */
    public function setBaseRealAmount($baseRealAmount);

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent();

    /**
     * Set percent
     *
     * @param float $percent
     * @return $this
     */
    public function setPercent($percent);

    /**
     * Get process
     *
     * @return int
     */
    public function getProcess();

    /**
     * Set process
     *
     * @param int $process
     * @return $this
     */
    public function setProcess($process);
}
