<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Tax;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * List of all weee attributes, their amounts, etc.., that product has
 * @api
 * @since 100.2.0
 */
interface WeeeAttributeInterface extends ExtensibleDataInterface
{
    const WEBSITE_ID = 'website_id';
    const COUNTRY = 'country';
    const STATE = 'state';
    const VALUE = 'value';
    const WEBSITE_VALUE = 'website_value';

    /**
     * Get Website Id
     *
     * @return int
     */
    public function getWebsiteId();

    /**
     * Get Country
     *
     * @return string
     */
    public function getCountry();

    /**
     * Get State
     *
     * @return int
     */
    public function getState();

    /**
     * Get Value
     *
     * @return float
     */
    public function getValue();

    /**
     * Get Website Value
     *
     * @return int
     */
    public function getWebsiteValue();
}
