<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Payment;

interface PaymentInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const CODE = 'code';
    const TITLE = 'title';
    const TYPE = 'type';
    const IS_DEFAULT = 'is_default';
    const IS_PAY_LATER = 'is_pay_later';
    const IS_REFERENCE_NUMBER = 'is_reference_number';
    const IS_SUGGEST_MONEY = 'is_suggest_money';
    const IS_ALLOW_PAY_VIA_EMAIL = 'is_allow_pay_via_email';
    const CAN_DUE = 'can_due';
    const YES = 1;
    const NO = 0;

    const REFUND_TYPE = 1;
    const ORDER_TYPE = 0;
    /**#@-*/

    /**
     * Get code
     *
     * @return string
     * @api
     */
    public function getCode();

    /**
     * Set code
     *
     * @param string $code
     * @return PaymentInterface
     * @api
     */
    public function setCode($code);

    /**
     * Get title
     *
     * @return string|null
     * @api
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     * @return PaymentInterface
     * @api
     */
    public function setTitle($title);

    /**
     * Get type
     *
     * @return int
     * @api
     */
    public function getType();

    /**
     * Set type
     *
     * @param int $type
     * @return PaymentInterface
     * @api
     */
    public function setType($type);

    /**
     * Get is default
     *
     * @return int
     * @api
     */
    public function getIsDefault();

    /**
     * Set is default
     *
     * @param int $isDefault
     * @return PaymentInterface
     * @api
     */
    public function setIsDefault($isDefault);

    /**
     * Get is pay later
     *
     * @return int
     * @api
     */
    public function getIsPayLater();

    /**
     * Set is pay later
     *
     * @param int $isPayLater
     * @return PaymentInterface
     * @api
     */
    public function setIsPayLater($isPayLater);

    /**
     * Get is reference number
     *
     * @return int
     * @api
     */
    public function getIsReferenceNumber();

    /**
     * Set is reference number
     *
     * @param int $isReferenceNumber
     * @return PaymentInterface
     * @api
     */
    public function setIsReferenceNumber($isReferenceNumber);

    /**
     * Get is suggest money
     *
     * @return int
     * @api
     */
    public function getIsSuggestMoney();

    /**
     * Set is suggest money
     *
     * @param int $isSuggestMoney
     * @return PaymentInterface
     * @api
     */
    public function setIsSuggestMoney($isSuggestMoney);

    /**
     * Get int can due
     *
     * @return int
     * @api
     */
    public function getCanDue();

    /**
     * Set is can due
     *
     * @param int $canDue
     * @return PaymentInterface
     * @api
     */
    public function setCanDue($canDue);

    /**
     * Get int isAllowPayViaEmail
     *
     * @return int
     * @api
     */
    public function getIsAllowPayViaEmail();

    /**
     * Set is $isAllowPayViaEmail
     *
     * @param int $isAllowPayViaEmail
     * @return PaymentInterface
     * @api
     */
    public function setIsAllowPayViaEmail($isAllowPayViaEmail);
}
