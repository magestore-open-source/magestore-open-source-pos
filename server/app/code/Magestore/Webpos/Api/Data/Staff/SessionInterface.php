<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Staff;

/**
 * Interface SessionInterface
 */
interface SessionInterface
{
    const ID = "id";
    const STAFF_ID = 'staff_id';
    const LOGGED_DATE = 'logged_date';
    const SESSION_ID = 'session_id';
    const LOCATION_ID = 'location_id';
    const POS_ID = 'pos_id';
    const HAS_EXCEPTION = 'has_exception';

    /**
     * Get id
     *
     * @return string|null
     * @api
     */
    public function getId();

    /**
     * Set id
     *
     * @param int $id
     * @return SessionInterface
     * @api
     */
    public function setId($id);

    /**
     * Get staff id
     *
     * @return int
     * @api
     */
    public function getStaffId();

    /**
     * Set staff id
     *
     * @param int $staffId
     * @return SessionInterface
     * @api
     */
    public function setStaffId($staffId);

    /**
     * Get logged date
     *
     * @return string
     * @api
     */
    public function getLoggedDate();

    /**
     * Set logged date
     *
     * @param string $loggedDate
     * @return SessionInterface
     * @api
     */
    public function setLoggedDate($loggedDate);

    /**
     * Get session
     *
     * @return string
     * @api
     */
    public function getSessionId();

    /**
     * Set session
     *
     * @param string $sessionId
     * @return SessionInterface
     * @api
     */
    public function setSessionId($sessionId);

    /**
     * Get location id
     *
     * @return int
     * @api
     */
    public function getLocationId();

    /**
     * Set location id
     *
     * @param string $locationId
     * @return SessionInterface
     * @api
     */
    public function setLocationId($locationId);

    /**
     * Get session
     *
     * @return int
     * @api
     */
    public function getPosId();

    /**
     * Set pos Id
     *
     * @param string $posId
     * @return SessionInterface
     * @api
     */
    public function setPosId($posId);

    /**
     * Get has exception
     *
     * @return int
     * @api
     */
    public function getHasException();

    /**
     * Set has exception
     *
     * @param int $hasException
     * @return SessionInterface
     * @api
     */
    public function setHasException($hasException);
}
