<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Staff\Login\Location;

/**
 * @api
 */
interface AddressInterface
{
    const STREET = 'street';
    const CITY = 'city';
    const REGION = 'region';
    const REGION_ID = 'region_id';
    const COUNTRY_ID = 'country_id';
    const COUNTRY = 'country';
    const POSTCODE = 'postcode';

    /**
     * Get Street
     *
     * @return string|null
     */
    public function getStreet();

    /**
     * Set Street
     *
     * @param string|null $street
     * @return AddressInterface
     */
    public function setStreet($street);

    /**
     * Get City
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Set City
     *
     * @param string|null $city
     * @return AddressInterface
     */
    public function setCity($city);

    /**
     * Get Region
     *
     * @return \Magento\Customer\Api\Data\RegionInterface
     */
    public function getRegion();

    /**
     * Set Region
     *
     * @param \Magento\Customer\Api\Data\RegionInterface $region
     * @return AddressInterface
     */
    public function setRegion($region);

    /**
     * Get Region Id
     *
     * @return int|null
     */
    public function getRegionId();

    /**
     * Set Region Id
     *
     * @param int|null $regionId
     * @return AddressInterface
     */
    public function setRegionId($regionId);

    /**
     * Get Country
     *
     * @return string|null
     */
    public function getCountryId();

    /**
     * Set Country
     *
     * @param string|null $country
     * @return AddressInterface
     */
    public function setCountryId($country);

    /**
     * Get Country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Set Country
     *
     * @param string|null $country
     * @return AddressInterface
     */
    public function setCountry($country);

    /**
     * Get Postcode
     *
     * @return string|null
     */
    public function getPostcode();

    /**
     * Set Postcode
     *
     * @param string|null $postcode
     * @return AddressInterface
     */
    public function setPostcode($postcode);
}
