<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Staff\Login;

/**
 * Interface LocationResultInterface
 *
 * @api
 */
interface LocationResultInterface
{
    const LOCATION_ID = 'location_id';
    const LOCATION_NAME = 'location_name';
    const STORE_ID = 'store_id';
    const TELEPHONE = 'telephone';
    const ADDRESS = 'address';
    const POS = 'pos';

    /**
     * Get location id
     *
     * @return int
     * @api
     */
    public function getLocationId();

    /**
     * Set location id
     *
     * @param int $locationId
     * @return LocationResultInterface
     * @api
     */
    public function setLocationId($locationId);

    /**
     * Get location name
     *
     * @return string
     * @api
     */
    public function getName();

    /**
     * Set location name
     *
     * @param string $locationName
     * @return LocationResultInterface
     * @api
     */
    public function setName($locationName);

    /**
     * Get address
     *
     * @return \Magestore\Webpos\Api\Data\Staff\Login\Location\AddressInterface
     */
    public function getAddress();

    /**
     * Set address
     *
     * @param \Magestore\Webpos\Api\Data\Staff\Login\Location\AddressInterface $address
     * @return LocationResultInterface
     */
    public function setAddress($address);

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone();

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return LocationResultInterface
     */
    public function setTelephone($telephone);

    /**
     * Get store id
     *
     * @return int
     * @api
     */
    public function getStoreId();

    /**
     * Set store id
     *
     * @param int $storeId
     * @return LocationResultInterface
     * @api
     */
    public function setStoreId($storeId);

    /**
     * Get pos
     *
     * @return \Magestore\Webpos\Api\Data\Staff\Login\PosResultInterface[]
     * @api
     */
    public function getPos();

    /**
     * Set pos
     *
     * @param \Magestore\Webpos\Api\Data\Staff\Login\PosResultInterface $pos
     * @return LocationResultInterface
     * @api
     */
    public function setPos($pos);
}
