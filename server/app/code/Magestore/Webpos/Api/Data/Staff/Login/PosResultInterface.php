<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Staff\Login;

/**
 * Interface PosResultInterface
 * @api
 */
interface PosResultInterface
{
    const POS_ID = 'pos_id';
    const POS_NAME = 'pos_name';
    const STAFF_ID = 'staff_id';
    const STATUS = 'status';
    const STAFF_NAME = 'staff_name';

    /**
     * Get pos id
     *
     * @return int
     * @api
     */
    public function getPosId();

    /**
     * Set pos id
     *
     * @param int $posId
     * @return PosResultInterface
     * @api
     */
    public function setPosId($posId);

    /**
     * Get pos name
     *
     * @return string
     * @api
     */
    public function getPosName();

    /**
     * Set pos name
     *
     * @param string $posName
     * @return PosResultInterface
     * @api
     */
    public function setPosName($posName);

    /**
     * Get staff id
     *
     * @return int
     * @api
     */
    public function getStaffId();

    /**
     * Set staff id
     *
     * @param int $staffId
     * @return PosResultInterface
     * @api
     */
    public function setStaffId($staffId);

    /**
     * Get status
     *
     * @return int
     * @api
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param int $status
     * @return PosResultInterface
     * @api
     */
    public function setStatus($status);

    /**
     * Get staff name
     *
     * @return string
     * @api
     */
    public function getStaffName();

    /**
     * Set staff name
     *
     * @param int $staffName
     * @return PosResultInterface
     * @api
     */
    public function setStaffName($staffName);
}
