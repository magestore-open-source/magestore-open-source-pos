<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Staff\Login;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 */

/**
 * Interface LoginResultInterface
 */
interface LoginResultInterface extends ExtensibleDataInterface
{
    /**
     * Staff Id
     *
     * Property STAFF_ID of staff
     */
    const STAFF_ID = 'staff_id';

    /**
     * Time out
     *
     * Property TIME_OUT of staff
     */
    const TIME_OUT = 'timeout';

    /**
     * Session Id
     *
     * Property SESSION_ID of staff
     */
    const SESSION_ID = 'session_id';

    /**
     * Massage
     *
     * Property MESSAGE of staff
     */
    const MESSAGE = 'message';

    /**
     * Staff name
     *
     * Property STAFF_NAME of staff
     */
    const STAFF_NAME = 'staff_name';

    /**
     * Locations
     *
     * Property LOCATIONS of staff
     */
    const LOCATIONS = 'locations';

    /**
     * Website Id
     *
     * Property WEBSITE_ID of staff
     */
    const WEBSITE_ID = 'website_id';

    /**
     * Staff email
     *
     * Property STAFF_EMAIL of staff
     */
    const STAFF_EMAIL = 'staff_email';

    /**
     * Sharing Account
     *
     * Property SHARING_ACCOUNT of staff
     */
    const SHARING_ACCOUNT = 'sharing_account';

    /**
     * Token of login
     *
     * Property TOKEN of staff
     */
    const TOKEN = 'token';

    /**
     * Get staff id
     *
     * @return int
     * @api
     */
    public function getStaffId();

    /**
     * Set staff id
     *
     * @param int $staffId
     * @return LoginResultInterface
     * @api
     */
    public function setStaffId($staffId);

    /**
     * Get session time out
     *
     * @return int
     * @api
     */
    public function getTimeout();

    /**
     * Set session time out
     *
     * @param int $timeout
     * @return LoginResultInterface
     * @api
     */
    public function setTimeout($timeout);

    /**
     * Get session id
     *
     * @return string
     * @api
     */
    public function getSessionId();

    /**
     * Set session id
     *
     * @param string $sessionId
     * @return LoginResultInterface
     * @api
     */
    public function setSessionId($sessionId);

    /**
     * Get message
     *
     * @return string
     * @api
     */
    public function getMessage();

    /**
     * Set message
     *
     * @param string $message
     * @return LoginResultInterface
     * @api
     */
    public function setMessage($message);

    /**
     * Get staff name
     *
     * @return string
     * @api
     */
    public function getStaffName();

    /**
     * Set staff name
     *
     * @param string $staffName
     * @return LoginResultInterface
     * @api
     */
    public function setStaffName($staffName);

    /**
     * Get staff name
     *
     * @return string
     * @api
     */
    public function getStaffEmail();

    /**
     * Set staff name
     *
     * @param string $staffEmail
     * @return LoginResultInterface
     * @api
     */
    public function setStaffEmail($staffEmail);

    /**
     * Get locations
     *
     * @return \Magestore\Webpos\Api\Data\Staff\Login\LocationResultInterface[]
     * @api
     */
    public function getLocations();

    /**
     * Set locations
     *
     * @param \Magestore\Webpos\Api\Data\Staff\Login\LocationResultInterface[] $locations
     * @return LoginResultInterface
     * @api
     */
    public function setLocations($locations);

    /**
     * Get website id
     *
     * @return int|null
     * @api
     */
    public function getWebsiteId();

    /**
     * Set website id
     *
     * @param int|null $websiteId
     * @return LoginResultInterface
     * @api
     */
    public function setWebsiteId($websiteId);

    /**
     * Set SharingAcountInformation
     *
     * @param int $sharingAccount
     * @return LoginResultInterface
     * @api
     */
    public function setSharingAccount($sharingAccount);

    /**
     * Get SharingAcountInformation
     *
     * @return int
     * @api
     */
    public function getSharingAccount();

    /**
     * Set token
     *
     * @param string $token
     * @return LoginResultInterface
     * @api
     */
    public function setToken($token);

    /**
     * Get token
     *
     * @return string
     * @api
     */
    public function getToken();

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Magestore\Webpos\Api\Data\Staff\Login\LoginResultExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Magestore\Webpos\Api\Data\Staff\Login\LoginResultExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Magestore\Webpos\Api\Data\Staff\Login\LoginResultExtensionInterface $extensionAttributes
    );
}
