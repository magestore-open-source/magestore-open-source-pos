<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Country;

/**
 * Data - Country interface.
 */
interface CountryInterface
{
    const ID = 'id';
    const CODE = 'code';
    const NAME = 'name';
    const REGION_REQUIRE = 'region_require';
    const REGIONS = 'regions';

    /**
     * Get ID
     *
     * @return string
     * @api
     */
    public function getId();

    /**
     * Set ID
     *
     * @param string $id
     * @return CountryInterface
     * @api
     */
    public function setId($id);

    /**
     * Get name
     *
     * @return string
     * @api
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     * @return CountryInterface
     * @api
     */
    public function setName($name);

    /**
     * Get region require
     *
     * @return int
     * @api
     */
    public function getRegionRequire();

    /**
     * Set region require
     *
     * @param int $regionRequire
     * @return CountryInterface
     * @api
     */
    public function setRegionRequire($regionRequire);

    /**
     * Get regions
     *
     * @return \Magestore\Webpos\Api\Data\Country\RegionInterface[]
     * @api
     */
    public function getRegions();

    /**
     * Set regions
     *
     * @param \Magestore\Webpos\Api\Data\Country\RegionInterface[] $regions
     * @return CountryInterface
     * @api
     */
    public function setRegions($regions);
}
