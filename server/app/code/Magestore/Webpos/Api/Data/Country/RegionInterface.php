<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Country;

/**
 * Country - Region interface.
 */
interface RegionInterface
{
    const ID = 'id';
    const CODE = 'code';
    const NAME = 'name';

    /**
     * Get ID
     *
     * @return int
     * @api
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $id
     * @return RegionInterface
     * @api
     */
    public function setId($id);

    /**
     * Get code
     *
     * @return string
     * @api
     */
    public function getCode();

    /**
     * Set code
     *
     * @param string $code
     * @return RegionInterface
     * @api
     */
    public function setCode($code);

    /**
     * Get name
     *
     * @return string
     * @api
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     * @return RegionInterface
     * @api
     */
    public function setName($name);
}
