<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Catalog\Option\Config\Attribute\Option;

/**
 * Interface ConfigOptionsInterface
 */
interface ProductInterface
{
    const ID = 'id';
    const PRICE = 'price';
    const BASE_PRICE = 'base_price';

    /**
     * Get Id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Id
     *
     * @param int $id
     * @return ProductInterface
     */
    public function setId($id);

    /**
     * Get Price
     *
     * @return float|null
     */
    public function getPrice();

    /**
     * Set Price
     *
     * @param float $price
     * @return ProductInterface
     */
    public function setPrice($price);

    /**
     * Get Base Price
     *
     * @return float|null
     */
    public function getBasePrice();

    /**
     * Set Base Price
     *
     * @param float $basePrice
     * @return ProductInterface
     */
    public function setBasePrice($basePrice);
}
