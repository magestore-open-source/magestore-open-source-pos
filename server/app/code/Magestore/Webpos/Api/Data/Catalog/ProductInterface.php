<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Catalog;

/**
 * Product data interface
 */
interface ProductInterface extends ProductOriginalInterface
{
    /**
     * Get list of product options
     *
     * @return \Magento\Catalog\Api\Data\ProductCustomOptionInterface[]|null
     */
    public function getCustomOptions();

    /**
     * Get list of product config options
     *
     * @return \Magestore\Webpos\Api\Data\Catalog\Option\ConfigOptionsInterface[]|void
     */
    public function getConfigOption();

    /**
     * Get is options
     *
     * @return int
     */
    public function getIsOptions();

    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function getJsonConfig();

    /**
     * Get JSON encoded configuration array which can be used for JS dynamic price
     *
     * Calculation depending on product options
     *
     * @return string
     */
    public function getPriceConfig();

    /**
     * Get stocks data by product
     *
     * @return \Magestore\Webpos\Api\Data\Inventory\StockItemInterface[]|null
     */
    public function getStocks();

    /**
     * Get data of children product
     *
     * @return \Magestore\Webpos\Api\Data\Catalog\ProductOriginalInterface[]|null
     */
    public function getChildrenProducts();

    /**
     * Get pos barcode of config webpos/product_search/barcod
     *
     * @return string|null
     */
    public function getPosBarcode();
}
