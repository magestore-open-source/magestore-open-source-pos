<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Catalog\Option\Config\Attribute;

/**
 * Interface ConfigOptionsInterface
 */
interface OptionInterface
{
    const ID = 'id';
    const LABEL = 'label';
    const PRODUCTS = 'products';

    /**
     * Get Id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Id
     *
     * @param int $id
     * @return OptionInterface
     */
    public function setId($id);

    /**
     * Get Label
     *
     * @return string|null
     */
    public function getLabel();

    /**
     * Set Label
     *
     * @param string $label
     * @return OptionInterface
     */
    public function setLabel($label);

    /**
     * Get Products
     *
     * @return \Magestore\Webpos\Api\Data\Catalog\Option\Config\Attribute\Option\ProductInterface[]
     */
    public function getProducts();

    /**
     * Set Products
     *
     * @param \Magestore\Webpos\Api\Data\Catalog\Option\Config\Attribute\Option\ProductInterface[] $products
     * @return OptionInterface
     */
    public function setProducts($products);
}
