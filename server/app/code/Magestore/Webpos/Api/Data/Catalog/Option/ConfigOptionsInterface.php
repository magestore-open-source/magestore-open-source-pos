<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Catalog\Option;

/**
 * Interface ConfigOptionsInterface
 */
interface ConfigOptionsInterface
{
    const ID = 'id';
    const CODE = 'code';
    const LABEL = 'label';
    const OPTIONS = 'options';
    const POSITION = 'position';

    /**
     * Get Id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Id
     *
     * @param int $id
     * @return ConfigOptionsInterface
     */
    public function setId($id);

    /**
     * Get Code
     *
     * @return string|null
     */
    public function getCode();

    /**
     * Set Code
     *
     * @param string $code
     * @return ConfigOptionsInterface
     */
    public function setCode($code);

    /**
     * Get Label
     *
     * @return string|null
     */
    public function getLabel();

    /**
     * Set Label
     *
     * @param string $label
     * @return ConfigOptionsInterface
     */
    public function setLabel($label);

    /**
     * Get Options
     *
     * @return \Magestore\Webpos\Api\Data\Catalog\Option\Config\Attribute\OptionInterface[]
     */
    public function getOptions();

    /**
     * Set Options
     *
     * @param \Magestore\Webpos\Api\Data\Catalog\Option\Config\Attribute\OptionInterface[] $options
     * @return ConfigOptionsInterface
     */
    public function setOptions($options);

    /**
     * Get Position
     *
     * @return int|null
     */
    public function getPosition();

    /**
     * Set Position
     *
     * @param int $position
     * @return ConfigOptionsInterface
     */
    public function setPosition($position);
}
