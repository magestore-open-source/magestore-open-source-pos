<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Catalog;

/**
 * @api
 */
interface CategoryInterface
{
    /**
     * Category id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Retrieve Name data wrapper
     *
     * @return string
     */
    public function getName();

    /**
     * Retrieve children ids
     *
     * @return string[]
     */
    public function getChildrenIds();

    /**
     * Retrieve children ids
     *
     * @return \Magestore\Webpos\Api\Data\Catalog\CategoryInterface[]
     */
    public function getChildren();

    /**
     * Retrieve children ids
     *
     * @param \Magestore\Webpos\Api\Data\Catalog\CategoryInterface[] $children
     * @return \Magestore\Webpos\Api\Data\Catalog\CategoryInterface
     */
    public function setChildren($children);

    /**
     * Get category image
     *
     * @return string/null
     */
    public function getImage();

    /**
     * Get category position
     *
     * @return int/null
     */
    public function getPosition();

    /**
     * Retrieve level
     *
     * @return int
     */
    public function getLevel();

    /**
     * Is first category
     *
     * @return int
     */
    public function isFirstCategory();

    /**
     * Get parent category identifier
     *
     * @return int
     */
    public function getParentId();

    /**
     * Get Path category
     *
     * @codeCoverageIgnoreStart
     * @return string|null
     */
    public function getPath();
}
