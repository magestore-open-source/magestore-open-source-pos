<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Catalog\Option;

/**
 * Interface ConfigOptionsInterface
 */
interface ProductOptionsInterface
{
    const CONFIG_OPTION = 'config_option';
    const CUSTOM_OPTION = 'custom_option';
    const BUNDLE_OPTION = 'bundle_option';
    const IS_OPTIONS = 'is_options';

    /**
     * Get Config Option
     *
     * @return \Magestore\Webpos\Api\Data\Catalog\Option\ConfigOptionsInterface[]
     */
    public function getConfigOption();

    /**
     * Set Config Option
     *
     * @param \Magestore\Webpos\Api\Data\Catalog\Option\ConfigOptionsInterface[] $configOption
     * @return ProductOptionsInterface
     */
    public function setConfigOption($configOption);

    /**
     * Get Custom Option
     *
     * @return string|null
     */
    public function getCustomOption();

    /**
     * Set Custom Option
     *
     * @param string $customOption
     * @return ProductOptionsInterface
     */
    public function setCustomOption($customOption);

    /**
     * Get Bundle Option
     *
     * @return string|null
     */
    public function getBundleOption();

    /**
     * Set Bundle Option
     *
     * @param string $bundleOption
     * @return ProductOptionsInterface
     */
    public function setBundleOption($bundleOption);

    /**
     * Get is Options
     *
     * @return int|null
     */
    public function getIsOptions();

    /**
     * Set is Options
     *
     * @param int $isOptions
     * @return ProductOptionsInterface
     */
    public function setIsOptions($isOptions);
}
