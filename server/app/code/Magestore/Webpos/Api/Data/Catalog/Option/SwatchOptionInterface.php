<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Catalog\Option;

interface SwatchOptionInterface
{
    const ATTRIBUTE_ID = 'attribute_id';
    const ATTRIBUTE_CODE = 'attribute_code';
    const ATTRIBUTE_LABEL = 'attribute_label';
    const SWATCHES = 'swatches';

    /**
     * Get Attribute Id
     *
     * @return int|null
     */
    public function getAttributeId();

    /**
     * Set Attribute Id
     *
     * @param int $attributeId
     * @return SwatchOptionInterface
     */
    public function setAttributeId($attributeId);

    /**
     * Get Attribute Code
     *
     * @return string|null
     */
    public function getAttributeCode();

    /**
     * Set Attribute Code
     *
     * @param string $attributeCode
     * @return SwatchOptionInterface
     */
    public function setAttributeCode($attributeCode);

    /**
     * Get Attribute Label
     *
     * @return string|null
     */
    public function getAttributeLabel();

    /**
     * Set Attribute Label
     *
     * @param string $attributeLabel
     * @return SwatchOptionInterface
     */
    public function setAttributeLabel($attributeLabel);

    /**
     * Get Swatches
     *
     * @return \Magestore\Webpos\Api\Data\Catalog\Option\Swatch\SwatchInterface[]
     */
    public function getSwatches();

    /**
     * Set Swatches
     *
     * @param \Magestore\Webpos\Api\Data\Catalog\Option\Swatch\SwatchInterface[] $swatches
     * @return SwatchOptionInterface
     */
    public function setSwatches($swatches);
}
