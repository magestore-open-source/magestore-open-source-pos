<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Customer;

/**
 * Magestore's Customer interface.
 */
interface CustomerInterface extends \Magento\Customer\Api\Data\CustomerInterface
{
    const TELEPHONE = 'telephone';
    const SUBSCRIBER_STATUS = 'subscriber_status';
    const FULL_NAME = 'full_name';
    const SEARCH_STRING = 'search_string';
    const IS_CREATING = 'is_creating';
    const TMP_CUSTOMER_ID = 'tmp_customer_id';
    const CREATED_LOCATION_ID = "created_location_id";

    /**
     * Get customer telephone
     *
     * @return string|null
     */
    public function getTelephone();

    /**
     * Set customer telephone
     *
     * @param string $telephone
     * @return CustomerInterface
     */
    public function setTelephone($telephone);

    /**
     * Get subscriber
     *
     * @return int|null
     */
    public function getSubscriberStatus();

    /**
     * Set subscriber
     *
     * @param int $subscriberStatus
     * @return CustomerInterface
     * @api
     */
    public function setSubscriberStatus($subscriberStatus);

    /**
     * Get full name
     *
     * @return string|null
     */
    public function getFullName();

    /**
     * Set full name
     *
     * @param string $fullName
     * @return CustomerInterface
     */
    public function setFullName($fullName);

    /**
     * Get search string
     *
     * @return string|null
     */
    public function getSearchString();

    /**
     * Set customer is creating
     *
     * @param string $isCreating
     * @return CustomerInterface
     */
    public function setIsCreating($isCreating);

    /**
     * Get customer is creating
     *
     * @return boolean
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsCreating();

    /**
     * Get gender
     *
     * @return string|null
     */
    public function getGender();

    /**
     * Set gender
     *
     * @param string|null $gender
     * @return $this
     */
    public function setGender($gender);

    /**
     * Get temp customer id
     *
     * @return string|null
     */
    public function getTmpCustomerId();

    /**
     * Set temp customer id
     *
     * @param string|null $tmpCustomerId
     * @return $this
     */
    public function setTmpCustomerId($tmpCustomerId);

    /**
     * Get location Id from which customer is created
     *
     * @return int|null
     */
    public function getCreatedLocationId();

    /**
     * Set location Id from which customer is created
     *
     * @param int $createdLocationId
     * @return $this
     */
    public function setCreatedLocationId($createdLocationId);
}
