<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Shipping;

/**
 * Interface ShippingMethodInterface
 */
interface ShippingMethodInterface
{
    const CODE = 'code';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const ERROR_MESSAGE = 'error_message';
    const SHIPMENT_REQUEST_TYPE = 'shipment_request_type';
    const TYPE = 'type';
    const PRICE = 'price';
    const CONDITION_NAME = 'condition_name';
    const IS_DEFAULT = 'is_default';
    const SPECIFIC_COUNTRIES_ALLOW = 'specific_countries_allow';
    const SPECIFIC_COUNTRY = 'specific_country';
    const HANDLING_FEE = 'handling_fee';
    const HANDLING_TYPE = 'handling_type';
    const HANDLING_ACTION = 'handling_action';
    const MAX_PACKAGE_WEIGHT = 'max_package_weight';
    const INCLUDE_VIRTUAL_PRICE = 'include_virtual_price';
    const FREE_SHIPPING_SUBTOTAL = 'free_shipping_subtotal';
    const RATES = 'rates';

    /**
     * Get Code
     *
     * @return string
     */
    public function getCode();

    /**
     * Set Code
     *
     * @param string $code
     * @return ShippingMethodInterface
     */
    public function setCode($code);

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set Title
     *
     * @param string $title
     * @return ShippingMethodInterface
     */
    public function setTitle($title);

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set Description
     *
     * @param string $description
     * @return ShippingMethodInterface
     */
    public function setDescription($description);

    /**
     * Get Error Message
     *
     * @return string
     */
    public function getErrorMessage();

    /**
     * Set Error Message
     *
     * @param string $errorMessage
     * @return ShippingMethodInterface
     */
    public function setErrorMessage($errorMessage);

    /**
     * Get Type
     *
     * @return string
     */
    public function getType();

    /**
     * Set Type
     *
     * @param string $type
     * @return ShippingMethodInterface
     */
    public function setType($type);

    /**
     * Get Shipment Request Type
     *
     * @return int
     */
    public function getShipmentRequestType();

    /**
     * Set Shipment Request Type
     *
     * @param int $shipmentRequestType
     * @return ShippingMethodInterface
     */
    public function setShipmentRequestType($shipmentRequestType);

    /**
     * Get Condition Name
     *
     * @return string
     */
    public function getConditionName();

    /**
     * Set Condition Name
     *
     * @param string $conditionName
     * @return ShippingMethodInterface
     */
    public function setConditionName($conditionName);

    /**
     * Get Price
     *
     * @return float
     */
    public function getPrice();

    /**
     * Set Price
     *
     * @param float $price
     * @return ShippingMethodInterface
     */
    public function setPrice($price);

    /**
     * Get Is Default
     *
     * @return int
     */
    public function getIsDefault();

    /**
     * Set Is Default
     *
     * @param int $isDefault
     * @return ShippingMethodInterface
     */
    public function setIsDefault($isDefault);

    /**
     * Get Specific Countries Allow
     *
     * @return int
     */
    public function getSpecificCountriesAllow();

    /**
     * Set Specific Countries Allow
     *
     * @param int $specificCountriesAllow
     * @return ShippingMethodInterface
     */
    public function setSpecificCountriesAllow($specificCountriesAllow);

    /**
     * Get Specific Country
     *
     * @return string
     */
    public function getSpecificCountry();

    /**
     * Set Specific Country
     *
     * @param string $specificCountry
     * @return ShippingMethodInterface
     */
    public function setSpecificCountry($specificCountry);

    /**
     * Get Handling Fee
     *
     * @return float
     */
    public function getHandlingFee();

    /**
     * Set Handling Fee
     *
     * @param float $handlingFee
     * @return ShippingMethodInterface
     */
    public function setHandlingFee($handlingFee);

    /**
     * Get Handling Type
     *
     * @return string
     */
    public function getHandlingType();

    /**
     * Set Handling Type
     *
     * @param string $handlingType
     * @return ShippingMethodInterface
     */
    public function setHandlingType($handlingType);

    /**
     * Get Handling Action
     *
     * @return string
     */
    public function getHandlingAction();

    /**
     * Set Handling Action
     *
     * @param string $handlingAction
     * @return ShippingMethodInterface
     */
    public function setHandlingAction($handlingAction);

    /**
     * Get Max Package Weight
     *
     * @return float
     */
    public function getMaxPackageWeight();

    /**
     * Set Max Package Weight
     *
     * @param float $maxPackageWeight
     * @return ShippingMethodInterface
     */
    public function setMaxPackageWeight($maxPackageWeight);

    /**
     * Get Include Virtual Price
     *
     * @return int
     */
    public function getIncludeVirtualPrice();

    /**
     * Set Include Virtual Price
     *
     * @param int $includeVirtualPrice
     * @return ShippingMethodInterface
     */
    public function setIncludeVirtualPrice($includeVirtualPrice);

    /**
     * Get Free Shipping Subtotal
     *
     * @return float
     */
    public function getFreeShippingSubtotal();

    /**
     * Set Free Shipping Subtotal
     *
     * @param float $freeShippingSubtotal
     * @return ShippingMethodInterface
     */
    public function setFreeShippingSubtotal($freeShippingSubtotal);

    /**
     * Get Rates
     *
     * @return mixed[]
     */
    public function getRates();

    /**
     * Set Rates
     *
     * @param mixed[] $rates
     * @return ShippingMethodInterface
     */
    public function setRates($rates);
}
