<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Data\Sales\Order;

/**
 * Pos - Creditmemo interface
 */
interface CreditmemoInterface extends \Magento\Sales\Api\Data\CreditmemoInterface
{
    const ORDER_INCREMENT_ID = 'order_increment_id';
    const POS_LOCATION_ID = 'pos_location_id';
    const PAYMENTS = 'payments';

    /**
     * Get Payment
     *
     * @return \Magestore\Webpos\Api\Data\Checkout\Order\PaymentInterface[]|null
     */
    public function getPayments();

    /**
     * Set Payment
     *
     * @param \Magestore\Webpos\Api\Data\Checkout\Order\PaymentInterface[]|null $payment
     * @return CreditmemoInterface
     */
    public function setPayments($payment);

    /**
     * Gets credit memo items.
     *
     * @return \Magestore\Webpos\Api\Data\Sales\Order\Creditmemo\ItemInterface[] Array of credit memo items.
     */
    public function getItems();

    /**
     * Sets credit memo items.
     *
     * @param \Magestore\Webpos\Api\Data\Sales\Order\Creditmemo\ItemInterface[] $items
     * @return CreditmemoInterface
     */
    public function setItems($items);

    /**
     * Get order increment id
     *
     * @return string|null
     */
    public function getOrderIncrementId();

    /**
     * Set order increment id
     *
     * @param string|null $orderIncrementId
     * @return CreditmemoInterface
     */
    public function setOrderIncrementId($orderIncrementId);

    /**
     * Get pos location id
     *
     * @return int|null
     */
    public function getPosLocationId();

    /**
     * Set pos location id
     *
     * @param int|null $posLocationId
     * @return CreditmemoInterface
     */
    public function setPosLocationId($posLocationId);
}
