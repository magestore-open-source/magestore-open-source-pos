<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Log;

/**
 * Repository deleted product
 */
interface ProductDeletedRepositoryInterface
{
    /**
     * Insert By Product Id
     *
     * @param int $productId
     * @return void
     */
    public function insertByProductId($productId);

    /**
     * Delete By Product Id
     *
     * @param int $productId
     * @return void
     */
    public function deleteByProductId($productId);

    /**
     * Delete By Product
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteByProduct($product);

    /**
     * Insert By Product Id
     *
     * @param int $productId
     * @param array $stockIds
     * @return void
     */
    public function insertByProductIdAndStock($productId, $stockIds = []);

    /**
     * Delete By Product Id
     *
     * @param int $productId
     * @param array $stockIds
     * @return void
     */
    public function deleteByProductIdAndStock($productId, $stockIds = []);
}
