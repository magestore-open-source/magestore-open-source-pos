<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Log;

use Magento\Framework\Exception\NoSuchEntityException;
use Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface;

/**
 * Interface CustomerDeletedRepositoryInterface
 */
interface CustomerDeletedRepositoryInterface
{

    /**
     * Save
     *
     * @param \Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface $customerDeleted
     * @return \Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface $customerDeleted);

    /**
     * Delete
     *
     * @param \Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface $customerDeleted
     */
    public function delete(\Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface $customerDeleted);

    /**
     * Delete By ID
     *
     * @param int $customerId
     */
    public function deleteById($customerId);

    /**
     * Get By Id
     *
     * @param int $customerId
     * @return \Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($customerId);
}
