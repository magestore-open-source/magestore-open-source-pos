<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Log;

/**
 * Interface DataLogRepositoryInterface
 */
interface DataLogRepositoryInterface
{
    /**
     * Retrieve data matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Log\DataLogResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListCustomer(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve data matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Log\DataLogResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListCategory(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve data matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Log\DataLogResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListProduct(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Retrieve data matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Log\DataLogStringResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getListOrder(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
