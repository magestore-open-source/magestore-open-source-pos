<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Log;

/**
 * Interface CategoryDeletedRepositoryInterface
 */
interface CategoryDeletedRepositoryInterface
{

    /**
     * Save
     *
     * @param \Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface $categoryDeleted
     * @return \Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface $categoryDeleted);

    /**
     * Delete
     *
     * @param \Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface $categoryDeleted
     * @return boolean
     */
    public function delete(\Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface $categoryDeleted);

    /**
     * Delete by id
     *
     * @param int $categoryId
     * @return boolean
     */
    public function deleteById($categoryId);

    /**
     * Get By Id
     *
     * @param int $categoryId
     * @return \Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($categoryId);
}
