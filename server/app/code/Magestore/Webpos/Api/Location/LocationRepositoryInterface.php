<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Api\Location;

interface LocationRepositoryInterface
{

    /**
     * Get By Id
     *
     * @param int $id
     * @return \Magestore\Webpos\Api\Data\Location\LocationInterface
     */
    public function getById($id);

    /**
     * Get List
     *
     * @param \Magento\Framework\Api\SearchCriteria $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Location\LocationSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteria $searchCriteria);

    /**
     * Get list all location
     *
     * @return \Magestore\Webpos\Api\Data\Location\LocationInterface[]
     */
    public function getAllLocation();

    /**
     * Save
     *
     * @param \Magestore\Webpos\Api\Data\Location\LocationInterface $location
     * @return \Magestore\Webpos\Api\Data\Location\LocationInterface
     */
    public function save(\Magestore\Webpos\Api\Data\Location\LocationInterface $location);

    /**
     * Delete
     *
     * @param \Magestore\Webpos\Api\Data\Location\LocationInterface $location
     * @return boolean
     */
    public function delete(\Magestore\Webpos\Api\Data\Location\LocationInterface $location);

    /**
     * Delete By Id
     *
     * @param int $locationId
     * @return boolean
     */
    public function deleteById($locationId);

    /**
     * Get List Available
     *
     * @param int $staffId
     * @return boolean|\Magestore\Webpos\Api\Data\Location\LocationInterface[]
     */
    public function getListAvailable($staffId);

    /**
     * Get List Location With Staff
     *
     * @param int $staffId
     * @return boolean|\Magestore\Webpos\Api\Data\Location\LocationInterface[]
     */
    public function getListLocationWithStaff($staffId);

    /**
     * Get Location Ids By Stock Id
     *
     * @param int $stockId
     * @return int[]|null
     */
    public function getLocationIdsByStockId($stockId);
}
