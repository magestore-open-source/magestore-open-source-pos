<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Ui\DataProvider\Form\Modifier;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\HtmlContent;
use Magento\Ui\Component\Form;

/**
 * Modifier - AbstractModifier
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class AbstractModifier implements ModifierInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var string
     */
    protected $scopeName = '';

    /**
     * @var bool
     */
    protected $visible = true;

    /**
     * @var bool
     */
    protected $opened = false;

    /**
     * @var int
     */
    protected $sortOrder = 10;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var string
     */
    protected $groupContainer = '';

    /**
     * @var string
     */
    protected $groupLabel = '';

    protected $components = [
        'html' => 'Magento_Ui/js/form/components/html',
        'button' => 'Magestore_Webpos/js/form/components/button'
    ];

    /**
     * @param ObjectManagerInterface $objectManager
     * @param Registry $registry
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Registry $registry,
        RequestInterface $request,
        UrlInterface $urlBuilder
    ) {
        $this->objectManager = $objectManager;
        $this->registry = $registry;
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Set fieldset is visble
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set fieldset is opened
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getOpened()
    {
        return $this->opened;
    }

    /**
     * Set fieldset sort order
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set Default Value
     *
     * @param array $field
     * @param mixed $value
     * @return array
     */
    public function setDefaultValue($field, $value)
    {
        $field['arguments']['data']['config']['default'] = $value;
        return $field;
    }

    /**
     * Add Form Field
     *
     * @param string $label
     * @param string $dataType
     * @param string $formElement
     * @param int $sortOrder
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @param bool $disable
     * @return array
     */
    public function addFormField(
        $label,
        $dataType,
        $formElement,
        $sortOrder,
        $validation = [],
        $defaultValue = null,
        $notice = '',
        $disable = false
    ) {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'label' => __($label),
                        'dataType' => $dataType,
                        'formElement' => $formElement,
                        'sortOrder' => $sortOrder,
                        'default' => $defaultValue,
                        'notice' => __($notice),
                        'validation' => $validation,
                        'disabled' => $disable
                    ],
                ],
            ],
        ];
        if ($defaultValue) {
            $field = $this->setDefaultValue($field, $defaultValue);
        }
        return $field;
    }

    /**
     * Add Form Field Text
     *
     * @param string $label
     * @param string $formElement
     * @param int $sortOrder
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @param bool $disabled
     * @return array
     */
    public function addFormFieldText(
        $label,
        $formElement,
        $sortOrder,
        $validation = [],
        $defaultValue = null,
        $notice = '',
        $disabled = false
    ) {
        return $this->addFormField(
            $label,
            'text',
            $formElement,
            $sortOrder,
            $validation,
            $defaultValue,
            $notice,
            $disabled
        );
    }

    /**
     * Add Form Field Password
     *
     * @param string $label
     * @param string $formElement
     * @param int $sortOrder
     * @param array $validation
     * @param string $classes
     * @param mixed|null $defaultValue
     * @param string $notice
     * @param bool $disabled
     * @return array
     */
    public function addFormFieldPassword(
        $label,
        $formElement,
        $sortOrder,
        $validation,
        $classes = '',
        $defaultValue = null,
        $notice = '',
        $disabled = false
    ) {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'elementTmpl' => 'Magestore_Webpos/form/password',
                        'label' => $label,
                        'dataType' => 'text',
                        'formElement' => $formElement,
                        'sortOrder' => $sortOrder,
                        'validation' => $validation,
                        'default' => $defaultValue,
                        'notice' => __($notice),
                        'disabled' => $disabled,
                        'classes' => $classes
                    ],
                ],
            ]
        ];
        return $field;
    }

    /**
     * Add Form Field Date
     *
     * @param string $label
     * @param int $sortOrder
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @return array
     */
    public function addFormFieldDate($label, $sortOrder, $validation = [], $defaultValue = null, $notice = '')
    {
        return $this->addFormField($label, 'date', 'date', $sortOrder, $validation, $defaultValue, $notice);
    }

    /**
     * Add Form Field Text Area
     *
     * @param string $label
     * @param int $sortOrder
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @return array
     */
    public function addFormFieldTextArea($label, $sortOrder, $validation = [], $defaultValue = null, $notice = '')
    {
        return $this->addFormField($label, 'text', 'textarea', $sortOrder, $validation, $defaultValue, $notice);
    }

    /**
     * Add Form Field Wysiwyg Area
     *
     * @param string $label
     * @param int $sortOrder
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @return array
     */
    public function addFormFieldWysiwygArea(
        $label,
        $sortOrder,
        $validation = [],
        $defaultValue = null,
        $notice = ''
    ) {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'label' => __($label),
                        'dataType' => 'text',
                        'formElement' => 'wysiwyg',
                        'sortOrder' => $sortOrder,
                        'default' => $defaultValue,
                        'validation' => $validation,
                        'notice' => __($notice),
                        'wysiwyg' => true
                    ],
                ],
            ],
        ];
        if ($defaultValue) {
            $field = $this->setDefaultValue($field, $defaultValue);
        }
        return $field;
    }

    /**
     * Add Switcher Config
     *
     * @param array $field
     * @param array $switcherConfig
     * @return array
     */
    public function addSwitcherConfig($field, $switcherConfig)
    {
        $field['arguments']['data']['config']['switcherConfig'] = [
            'enabled' => true,
            'rules' => $switcherConfig
        ];
        return $field;
    }

    /**
     * Add Form Field Checkbox
     *
     * @param string $label
     * @param int $sortOrder
     * @param string $default
     * @param string $notice
     * @param array|null $switcherConfig
     * @return array
     */
    public function addFormFieldCheckbox($label, $sortOrder, $default = '', $notice = '', $switcherConfig = null)
    {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __($label),
                        'componentType' => Field::NAME,
                        'formElement' => 'checkbox',
                        'dataType' => 'number',
                        'sortOrder' => $sortOrder,
                        'valueMap' => [
                            'false' => '0',
                            'true' => '1'
                        ],
                        'notice' => __($notice),
                    ],
                ],
            ],
        ];
        if ($switcherConfig) {
            $field = $this->addSwitcherConfig($field, $switcherConfig);
        }
        if ($default) {
            $field = $this->setDefaultValue($field, $default);
        }
        return $field;
    }

    /**
     * Add Form Field Select
     *
     * @param string $label
     * @param int $sortOrder
     * @param array $options
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @param array|null $switcherConfig
     * @param bool $disabled
     * @return array
     */
    public function addFormFieldSelect(
        $label,
        $sortOrder,
        $options = [],
        $validation = [],
        $defaultValue = null,
        $notice = '',
        $switcherConfig = null,
        $disabled = false
    ) {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'dataType' => 'text',
                        'formElement' => 'select',
                        'options' => $options,
                        'componentType' => Field::NAME,
                        'label' => __($label),
                        'sortOrder' => $sortOrder,
                        'validation' => $validation,
                        'disabled' => $disabled,
                        'notice' => __($notice),
                    ],
                ],
            ],
        ];
        if ($defaultValue) {
            $field = $this->setDefaultValue($field, $defaultValue);
        }
        if ($switcherConfig) {
            $field = $this->addSwitcherConfig($field, $switcherConfig);
        }
        return $field;
    }

    /**
     * Add Form Field Multi Select
     *
     * @param string $label
     * @param int $sortOrder
     * @param array $options
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @param bool $disabled
     * @return array
     */
    public function addFormFieldMultiSelect(
        $label,
        $sortOrder,
        $options = [],
        $validation = [],
        $defaultValue = null,
        $notice = '',
        $disabled = false
    ) {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'dataType' => 'text',
                        'formElement' => 'multiselect',
                        'options' => $options,
                        'componentType' => Field::NAME,
                        'label' => __($label),
                        'sortOrder' => $sortOrder,
                        'validation' => $validation,
                        'disabled' => $disabled,
                        'notice' => __($notice),
                    ],
                ],
            ],
        ];
        if ($defaultValue) {
            $field = $this->setDefaultValue($field, $defaultValue);
        }
        return $field;
    }

    /**
     * Add html_content element.
     *
     * @param string $containerName
     * @param string $block
     * @return array
     */
    public function addHtmlContentContainer($containerName = '', $block = '')
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                    ],
                ],
            ],
            'children' => [
                $containerName => [
                    'arguments' => [
                        'data' => [
                            'type' => HtmlContent::NAME,
                            'name' => HtmlContent::NAME,
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => $this->components['html'],
                                'content' => $this->objectManager->create($block)->toHtml()
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Add Button
     *
     * @param string $buttonTitle
     * @param array $actions
     * @param string|null $redirectUrl
     * @return array
     */
    public function addButton($buttonTitle = '', $actions = [], $redirectUrl = null)
    {
        $button = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                        'component' => $this->components['button'],
                        'title' => __($buttonTitle),
                        'actions' => $actions
                    ],
                ],
            ],
        ];
        if ($redirectUrl) {
            $button['arguments']['data']['config']['redirectUrl'] = $redirectUrl;
        }
        return $button;
    }

    /**
     * Returns text column configuration for the dynamic grid
     *
     * @param string $dataScope
     * @param bool $fit
     * @param string $label
     * @param int $sortOrder
     * @return array
     */
    public function getTextColumn($dataScope, $fit, $label, $sortOrder)
    {
        $column = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'formElement' => Form\Element\Input::NAME,
                        'elementTmpl' => 'ui/dynamic-rows/cells/text',
                        'dataType' => Form\Element\DataType\Text::NAME,
                        'dataScope' => $dataScope,
                        'fit' => $fit,
                        'label' => __($label),
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
        return $column;
    }

    /**
     * Returns input number column configuration for the dynamic grid
     *
     * @param string $dataScope
     * @param boolean $fit
     * @param string $label
     * @param int $sortOrder
     * @param array $validation
     * @return array
     */
    public function getInputNumberColumn($dataScope, $fit, $label, $sortOrder, $validation = [])
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'dataType' => Form\Element\DataType\Number::NAME,
                        'formElement' => Form\Element\Input::NAME,
                        'componentType' => Field::NAME,
                        'dataScope' => $dataScope,
                        'label' => __($label),
                        'fit' => $fit,
                        'additionalClasses' => 'admin__field-small',
                        'sortOrder' => $sortOrder,
                        'validation' => $validation
                    ],
                ],
            ],
        ];
    }

    /**
     * Add Form Field Label
     *
     * @param string $label
     * @param string $formElement
     * @param int $sortOrder
     * @param array $validation
     * @param string $classes
     * @param mixed|null $defaultValue
     * @param string $notice
     * @param bool $disabled
     * @return array
     */
    public function addFormFieldLabel(
        $label,
        $formElement,
        $sortOrder,
        $validation,
        $classes = '',
        $defaultValue = null,
        $notice = '',
        $disabled = false
    ) {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'elementTmpl' => 'Magestore_Webpos/form/label',
                        'label' => $label,
                        'dataType' => 'text',
                        'formElement' => $formElement,
                        'sortOrder' => $sortOrder,
                        'validation' => $validation,
                        'default' => $defaultValue,
                        'notice' => __($notice),
                        'disabled' => $disabled,
                        'classes' => $classes
                    ],
                ],
            ]
        ];
        return $field;
    }

    /**
     * Add Form Field Select Text
     *
     * @param string $component
     * @param string $label
     * @param int $sortOrder
     * @param array $options
     * @param array $validation
     * @param mixed|null $defaultValue
     * @param string $notice
     * @param array|null $switcherConfig
     * @param bool $disabled
     * @return array|\array[][][]
     */
    public function addFormFieldSelectText(
        $component,
        $label,
        $sortOrder,
        $options = [],
        $validation = [],
        $defaultValue = null,
        $notice = '',
        $switcherConfig = null,
        $disabled = false
    ) {
        $field = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'component' => $component,
                        'dataType' => 'text',
                        'formElement' => 'select',
                        'options' => $options,
                        'componentType' => Field::NAME,
                        'label' => __($label),
                        'sortOrder' => $sortOrder,
                        'validation' => $validation,
                        'disabled' => $disabled,
                        'notice' => __($notice),
                    ],
                ],
            ],
        ];
        if ($defaultValue) {
            $field = $this->setDefaultValue($field, $defaultValue);
        }
        if ($switcherConfig) {
            $field = $this->addSwitcherConfig($field, $switcherConfig);
        }
        return $field;
    }
}
