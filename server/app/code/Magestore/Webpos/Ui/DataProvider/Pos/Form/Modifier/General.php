<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\DataProvider\Pos\Form\Modifier;

use Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Store\Ui\Component\Listing\Column\Store\Options;
use Magento\Ui\Component\Form;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;
use Magestore\Webpos\Model\Source\Adminhtml\CustomerGroup;
use Magestore\Webpos\Model\Source\Adminhtml\Location;
use Magestore\Webpos\Model\Source\Adminhtml\Pos;
use Magestore\Webpos\Model\Source\Adminhtml\Staff;
use Magestore\Webpos\Model\Source\Adminhtml\Status;

/**
 * Form Modifier - General
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class General extends AbstractModifier
{
    /**
     * @var Options
     */
    protected $optionStoreView;

    /**
     * @var string
     */
    protected $groupContainer = 'general_information';

    /**
     * @var string
     */
    protected $groupLabel = 'General Information';

    /**
     * @var int
     */
    protected $sortOrder = 80;

    /**
     * @var bool
     */
    protected $opened = true;
    /**
     * @var CustomerGroup
     */
    protected $customerGroup;
    /**
     * @var Status
     */
    protected $status;
    /**
     * @var Location
     */
    protected $location;
    /**
     * @var Pos
     */
    protected $pos;

    /**
     * @var Staff
     */
    protected $staff;
    /**
     * @var StaffRepositoryInterface
     */
    protected $staffRepository;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param Registry $registry
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     * @param Options $optionStoreView
     * @param CustomerGroup $customerGroup
     * @param Status $status
     * @param Location $location
     * @param Pos $pos
     * @param Staff $staff
     * @param StaffRepositoryInterface $staffRepository
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Registry $registry,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        Options $optionStoreView,
        CustomerGroup $customerGroup,
        Status $status,
        Location $location,
        Pos $pos,
        Staff $staff,
        StaffRepositoryInterface $staffRepository
    ) {
        parent::__construct($objectManager, $registry, $request, $urlBuilder);
        $this->optionStoreView = $optionStoreView;
        $this->customerGroup = $customerGroup;
        $this->status = $status;
        $this->location = $location;
        $this->pos = $pos;
        $this->staff = $staff;
        $this->staffRepository = $staffRepository;
    }

    /**
     * @inheritDoc
     *
     * @throws LocalizedException
     */
    public function modifyData(array $data)
    {
        $posId = $this->request->getParam('id');
        if (isset($data[$posId]['staff_id']) && $data[$posId]['staff_id']) {
            $staffId = $data[$posId]['staff_id'];

            try {
                $staffModel = $this->staffRepository->getById($staffId);
                if ($staffModel->getStaffId()) {
                    $data[$posId]['staff_name'] = $staffModel->getName();
                }
            } catch (Exception $e) {
                $data[$posId]['staff_name'] = __('N/A');
            }

        }
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                'pos_information' => [
                    'children' => $this->getGeneralChildren(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('POS'),
                                'collapsible' => true,
                                'dataScope' => 'data',
                                'visible' => $this->getVisible(),
                                'opened' => $this->getOpened(),
                                'componentType' => Form\Fieldset::NAME,
                                'sortOrder' => 1
                            ],
                        ],
                    ],
                ],

            ]
        );
        return $meta;
    }

    /**
     * Get General Children
     *
     * @return array
     */
    protected function getGeneralChildren()
    {
        if ($this->request->getParam('default_location')) {
            $defaultLocation = $this->request->getParam('default_location');
        } else {
            $defaultLocation = 0;
        }

        $pos = $this->registry->registry('current_pos');

        $staffId = null;
        if ($pos && $pos->getId()) {
            $staffId = $pos->getStaffId();
        }

        $children = [
            'pos_name' => $this->addFormFieldText(__('POS Name'), 'input', 10, ['required-entry' => true]),
            'location_id' => $this->addFormFieldSelectText(
                'Magestore_Webpos/js/form/element/location-form',
                __('Location'),
                20,
                $this->location->toOptionArray(),
                ['required-entry' => true],
                $defaultLocation,
                '',
                null,
                $staffId ? true : false
            ),
            'status' => $this->addFormFieldSelect(
                'Status',
                30,
                $this->status->toOptionArray(),
                ['required-entry' => false]
            ),
        ];
        if ($this->request->getParam('id')) {
            $children['staff_name'] = $this->addFormFieldLabel(
                __('Current Staff'),
                'input',
                25,
                ['required-entry' => false],
                '',
                'N/A'
            );
        }
        return $children;
    }
}
