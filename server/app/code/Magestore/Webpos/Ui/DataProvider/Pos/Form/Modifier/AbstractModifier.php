<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\DataProvider\Pos\Form\Modifier;

use Magestore\Webpos\Model\Pos\Pos;

/**
 * Modifier - AbstractModifier
 */
class AbstractModifier extends \Magestore\Webpos\Ui\DataProvider\Form\Modifier\AbstractModifier
{
    /**
     * @var Pos
     */
    protected $currentPos;

    /**
     * @var string
     */
    protected $scopeName = 'webpos_pos_form.webpos_pos_form';

    /**
     * Get CurrentPos
     *
     * @return Pos|mixed
     */
    public function getCurrentPos()
    {
        if (!$this->currentPos) {
            $this->currentPos = $this->registry->registry('current_pos');
        }
        return $this->currentPos;
    }
}
