<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\DataProvider\Location\Form;

use Magento\Framework\Registry;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory;
use Magestore\Webpos\Model\ResourceModel\Location\Location\Collection;

/**
 * Form - Location
 */
class Location extends AbstractDataProvider
{

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var PoolInterface
     */
    protected $pool;

    /**
     * @var array
     */
    protected $loadedData = [];

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param Registry $registry
     * @param CollectionFactory $collectionFactory
     * @param PoolInterface $pool
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        Registry $registry,
        CollectionFactory $collectionFactory,
        PoolInterface $pool,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->_coreRegistry = $registry;
        $this->collection = $collectionFactory->create();
        $this->pool = $pool;
    }

    /**
     * @inheritDoc
     */
    public function getData()
    {
        $location = $this->_coreRegistry->registry('current_location');
        if ($location && $location->getLocationId()) {
            $result = [
                'general_information' => [
                    'sub_general_information' => [
                        'location_id' => $location->getLocationId(),
                        'name' => $location->getName(),
                        'description' => $location->getDescription(),
                        'telephone' => $location->getTelephone(),
                        'email' => $location->getEmail()
                    ],
                    'stock_selection' => [
                        'stock_id' => $location->getStockId()
                    ],
                    'address_information' => [
                        'street' => $location->getStreet(),
                        'city' => $location->getCity(),
                        'country_id' => $location->getCountryId(),
                        'region' => $location->getRegion(),
                        'region_id' => $location->getRegionId(),
                        'postcode' => $location->getPostcode()
                    ]
                ]
            ];
            $this->loadedData[$location->getLocationId()] = $result;
        }

        foreach ($this->pool->getModifiersInstances() as $modifier) {
            $this->loadedData = $modifier->modifyData($this->loadedData);
        }

        return $this->loadedData;
    }

    /**
     * @inheritDoc
     */
    public function getMeta()
    {
        $meta = parent::getMeta();

        $meta = $meta = array_replace_recursive($meta, [
            'general_information' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'sortOrder' => 10
                        ]
                    ]
                ], 'children' => [
                    'sub_general_information' => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'sortOrder' => 10
                                ]
                            ]
                        ],
                    ], 'address_information' => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'sortOrder' => 30
                                ]
                            ]
                        ],
                    ]
                ]
            ]
        ]);

        foreach ($this->pool->getModifiersInstances() as $modifier) {
            $meta = $modifier->modifyMeta($meta);
        }

        return $meta;
    }
}
