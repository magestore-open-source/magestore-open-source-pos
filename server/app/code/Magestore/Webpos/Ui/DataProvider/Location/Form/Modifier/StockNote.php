<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\DataProvider\Location\Form\Modifier;

use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magestore\Webpos\Api\WebposManagementInterface;

/**
 * Location form - StockNote
 */
class StockNote implements ModifierInterface
{
    /**
     * @var WebposManagementInterface
     */
    protected $webposManagement;

    /**
     * StockNote constructor.
     * @param WebposManagementInterface $webposManagement
     */
    public function __construct(
        WebposManagementInterface $webposManagement
    ) {
        $this->webposManagement = $webposManagement;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        $multiStockPageLink = "https://www.magestore.com/multi-stock/?"
            . "utm_source=pos-open-backend&utm_medium=product"
            . "&utm_campaign=upsell_commerce_2021&utm_content=add-new-location";
        $note = __(
            "This open version only supports Default Stock. Upgrade to our Commerce plan to use "
            . "<a href='%1' target='_blank'>Multiple Stocks</a>.",
            $multiStockPageLink
        );
        $meta = array_replace_recursive($meta, [
            'general_information' => [
                'children' => [
                    'stock_selection' => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'sortOrder' => 20
                                ]
                            ]
                        ],
                        'children' => [
                            'stock_id' => [
                                'arguments' => [
                                    'data' => [
                                        'config' => [
                                            'additionalInfo' => $note
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ], $meta);
        return $meta;
    }
}
