<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Ui\DataProvider\Location\Inventory;

use Exception;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Framework\Api\Filter;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Select;
use Magento\Framework\ObjectManagerInterface;
use Magento\InventoryApi\Api\StockRepositoryInterface;
use Magento\InventoryIndexer\Model\StockIndexTableNameResolverInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Ui\DataProvider\AddFieldToCollectionInterface;
use Magento\Ui\DataProvider\AddFilterToCollectionInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Model\ResourceModel\Location\Inventory\Grid\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

/**
 * DataProvider - LocationInventoryDataProvider
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class LocationInventoryDataProvider extends AbstractDataProvider
{
    /**
     * Product collection
     *
     * @var Collection
     */
    protected $collection;

    /**
     * @var RequestInterface
     */
    protected $requestInterface;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var StockManagementInterface
     */
    protected $stockManagementInterface;

    /**
     * @var AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * @var PoolInterface
     */
    private $modifiersPool;

    protected $mappingFields = [
        'quantity' => [
            'table_alias' => 'stock_table',
            'field' => 'quantity',
            'condition' => 'e.sku = stock_table.sku'
        ]
    ];

    /**
     * LocationInventoryDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $requestInterface
     * @param ObjectManagerInterface $objectManager
     * @param StockManagementInterface $stockManagementInterface
     * @param array $addFieldStrategies
     * @param array $addFilterStrategies
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $modifiersPool
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $requestInterface,
        ObjectManagerInterface $objectManager,
        StockManagementInterface $stockManagementInterface,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = [],
        PoolInterface $modifiersPool = null
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->requestInterface = $requestInterface;
        $this->objectManager = $objectManager;
        $this->stockManagementInterface = $stockManagementInterface;
        $this->addFieldStrategies = $addFieldStrategies;
        $this->addFilterStrategies = $addFilterStrategies;
        $this->modifiersPool = $modifiersPool ?: ObjectManager::getInstance()->get(PoolInterface::class);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        $stockId = $this->requestInterface->getParam('stock_id');
        $stock = null;
        $stockTable = "";
        $this->objectManager = ObjectManager::getInstance();
        /** @var ResourceConnection $resource */
        $resource = $this->objectManager->create(ResourceConnection::class);
        if ($stockId) {
            /** @var StockRepositoryInterface $stock */
            $stockRepository = $this->objectManager->create(StockRepositoryInterface::class);
            try {
                $stock = $stockRepository->get($stockId);
            } catch (Exception $e) {
                $stock = null;
            }
            $stockTable = $this->objectManager
                ->get(StockIndexTableNameResolverInterface::class)
                ->execute($stockId);
        }
        if ($stock && $stock->getStockId() && $stockTable && $resource->getConnection()->isTableExists($stockTable)) {
            $sourceItemTable = $resource->getTableName('inventory_source_item');
            $linkedSources = $this->stockManagementInterface->getLinkedSourceCodesByStockId($stock->getStockId());
            $quantityFieldCondition = $this->getQuantityFieldCondition();
            $this->getCollection()->getSelect()
                ->joinLeft(
                    ['stock_table' => $stockTable],
                    'e.sku = stock_table.sku',
                    [
                        'quantity' => $quantityFieldCondition,
                        'is_salable' => 'is_salable'
                    ]
                )->joinLeft(
                    ['inventory_source_item' => $sourceItemTable],
                    "e.sku = inventory_source_item.sku 
                    AND inventory_source_item.source_code IN ('" . implode("', '", $linkedSources) . "')",
                    ['source_code']
                )->group('e.entity_id')
                ->having('inventory_source_item.source_code IN (?)', $linkedSources)
                ->orHaving('stock_table.is_salable = ?', 1);
        } else {
            $this->getCollection()->addFieldToFilter('entity_id', 0);
        }
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();
        $data = [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => array_values($items),
        ];

        /** @var ModifierInterface $modifier */
        foreach ($this->modifiersPool->getModifiersInstances() as $modifier) {
            $data = $modifier->modifyData($data);
        }
        return $data;
    }

    /**
     * Add field to select
     *
     * @param string|array $field
     * @param string|null $alias
     * @return void
     */
    public function addField($field, $alias = null)
    {
        if (isset($this->addFieldStrategies[$field])) {
            $this->addFieldStrategies[$field]->addField($this->getCollection(), $field, $alias);
        } else {
            parent::addField($field, $alias);
        }
    }

    /**
     * @inheritDoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function addFilter(Filter $filter)
    {
        if ($filter->getField() === 'quantity') {
            $condition = [$filter->getConditionType() => $filter->getValue()];
            $quantityFieldCondition = $this->getQuantityFieldCondition();
            if (isset($condition['gteq'])) {
                $this->getCollection()->getSelect()->where(
                    "$quantityFieldCondition >= ?",
                    (float)$condition['gteq']
                );
            }
            if (isset($condition['lteq'])) {
                $whereQuantityCondition = "($quantityFieldCondition <= ? OR $quantityFieldCondition IS NULL)";
                $whereConditionArray = $this->getCollection()->getSelect()->getPart(Select::WHERE);
                if (isset($whereConditionArray) && is_array($whereConditionArray)) {
                    foreach ($whereConditionArray as $whereCondition) {
                        $hasGTEQQuantity = strpos($whereCondition, "$quantityFieldCondition >=");
                        if ($hasGTEQQuantity !== false && $hasGTEQQuantity >= 0) {
                            $whereQuantityCondition = "$quantityFieldCondition <= ?";
                        }
                    }
                }
                $this->getCollection()->getSelect()->where($whereQuantityCondition, (float)$condition['lteq']);
            }
        } else {
            if (isset($this->addFilterStrategies[$filter->getField()])) {
                $this->addFilterStrategies[$filter->getField()]
                    ->addFilter(
                        $this->getCollection(),
                        $filter->getField(),
                        [$filter->getConditionType() => $filter->getValue()]
                    );
            } else {
                parent::addFilter($filter);
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getMeta()
    {
        $meta = parent::getMeta();

        /** @var ModifierInterface $modifier */
        foreach ($this->modifiersPool->getModifiersInstances() as $modifier) {
            $meta = $modifier->modifyMeta($meta);
        }

        return $meta;
    }

    /**
     * Add Order
     *
     * @param string $field
     * @param string $direction
     * @return void
     */
    public function addOrder($field, $direction)
    {
        if ($field === 'quantity') {
            $stockId = $this->requestInterface->getParam('stock_id');
            if ($stockId) {
                $field = $this->getQuantityFieldCondition();
                $this->getCollection()->getSelect()->order("$field $direction");
            }
        } else {
            $this->getCollection()->addOrder($field, $direction);
        }
    }

    /**
     * Get Quantity Field Condition
     *
     * @return string
     */
    public function getQuantityFieldCondition()
    {
        $this->objectManager = ObjectManager::getInstance();
        $stockConfiguration = $this->objectManager->get(StockConfigurationInterface::class);
        $manageItemProductType = array_keys(array_filter($stockConfiguration->getIsQtyTypeIds()));
        $manageItemProductTypeSql = is_array($manageItemProductType) ?
            "('" . implode("', '", $manageItemProductType) . "')" :
            "('')";
        return "IF(e.type_id IN $manageItemProductTypeSql, TRIM(stock_table.quantity)+0, NULL)";
    }
}
