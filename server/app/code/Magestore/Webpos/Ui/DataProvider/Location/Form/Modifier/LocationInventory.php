<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types = 1);

namespace Magestore\Webpos\Ui\DataProvider\Location\Form\Modifier;

use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Form;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magestore\Webpos\Model\Location\Location;

/**
 * POS location inventory
 */
class LocationInventory implements ModifierInterface
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Location
     */
    protected $currentLocation;

    protected $_groupLabel = 'Location Inventory';
    protected $_sortOrder = 20;
    protected $_groupContainer = 'location_inventory';
    protected $loadedData;

    /**
     * LocationInventory constructor.
     *
     * @param Registry $registry
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        Registry $registry,
        UrlInterface $urlBuilder
    ) {
        $this->registry = $registry;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $this->loadedData = [];
        $location = $this->getCurrentLocation();
        if ($location) {
            $locationData = $location->getData();
            $this->loadedData[$location->getId()] = $locationData;
        }
        return $this->loadedData;
    }

    /**
     * Get opened
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getOpened()
    {
        $locationId = $this->getCurrentLocation()->getId();
        if ($locationId) {
            return true;
        }
        return false;
    }

    /**
     * Get visible
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getVisible()
    {
        $locationId = $this->getCurrentLocation()->getId();
        if (!$locationId) {
            return false;
        }
        return true;
    }

    /**
     * Modify data
     *
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Get current location
     *
     * @return Location
     */
    public function getCurrentLocation()
    {
        if (!$this->currentLocation) {
            $this->currentLocation = $this->registry->registry('current_location');
        }

        return $this->currentLocation;
    }

    /**
     * @inheritdoc
     */
    public function modifyMeta(array $meta)
    {
        $location = $this->getCurrentLocation();
        if (!$location || !$location->getId()) {
            return $meta;
        }
        $meta = array_replace_recursive(
            $meta,
            [
                $this->_groupContainer => [
                    'children' => $this->getChildren(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __($this->_groupLabel),
                                'autoRender' => true,
                                'collapsible' => true,
                                'visible' => $this->getVisible(),
                                'opened' => $this->getOpened(),
                                'componentType' => Form\Fieldset::NAME,
                                'sortOrder' => $this->_sortOrder
                            ],
                        ],
                    ],
                ],
            ]
        );
        return $meta;
    }

    /**
     * Retrieve child meta configuration
     *
     * @return array
     */
    protected function getChildren()
    {
        $children = [
            'inventory_stock' => $this->getDashboardContainer(),
        ];
        return $children;
    }

    /**
     * Get dashboard container
     *
     * @return array
     */
    public function getDashboardContainer()
    {
        $listingTarget = 'webpos_inventory_stock_listing';
        $container = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'autoRender' => true,
                        'componentType' => 'insertListing',
                        'dataScope' => $listingTarget,
                        'externalProvider' => $listingTarget . '.' . $listingTarget . '_data_source',
                        'ns' => $listingTarget,
                        'render_url' => $this->urlBuilder->getUrl('mui/index/render'),
                        'realTimeLink' => true,
                        'dataLinks' => [
                            'imports' => false,
                            'exports' => true
                        ],
                        'behaviourType' => 'simple',
                        'externalFilterMode' => true,
                        'imports' => [
                            'stockId' => '${ $.provider }:data.general_information.stock_selection.stock_id'
                        ],
                        'exports' => [
                            'stockId' => '${ $.externalProvider }:params.stock_id'
                        ]
                    ],
                ],
            ],
        ];

        if ($this->isExistDisableTmpl()) {
            $container['arguments']['data']['config']['imports']['__disableTmpl'] = ['stockId' => false];
            $container['arguments']['data']['config']['exports']['__disableTmpl'] = ['stockId' => false];
        }

        return $container;
    }

    /**
     * Check Class Sanitizer exist method extractConfig
     *
     * @return boolean
     */
    private function isExistDisableTmpl()
    {
        // @codingStandardsIgnoreStart
        $className = 'Magento\Framework\View\Element\UiComponent\DataProvider\Sanitizer';
        // @codingStandardsIgnoreEnd
        $methodName = 'extractConfig';
        return class_exists($className) && !!method_exists($className, $methodName);
    }
}
