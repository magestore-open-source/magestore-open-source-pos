<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\DataProvider\Location\Form\Modifier;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form;
use Magestore\Webpos\Model\Location\Location;

/**
 * Form modifier - Linked Pos
 */
class LinkedPos extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier
{
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var Location
     */
    protected $currentLocation;

    protected $_groupLabel = 'Linked POS';
    protected $_sortOrder = 45;
    protected $_groupContainer = 'linked_pos';

    protected $loadedData;

    /**
     * @param Registry $registry
     */
    public function __construct(
        Registry $registry
    ) {
        $this->registry = $registry;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $this->loadedData = [];
        $location = $this->getCurrentLocation();
        if ($location) {
            $locationData = $location->getData();
            $this->loadedData[$location->getId()] = $locationData;
        }
        return $this->loadedData;
    }

    /**
     * Get Opened
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getOpened()
    {
        $locationId = $this->getCurrentLocation()->getId();
        if ($locationId) {
            return true;
        }
        return false;
    }

    /**
     * Get visible
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getVisible()
    {
        $locationId = $this->getCurrentLocation()->getId();
        if (!$locationId) {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Get current location
     *
     * @return Location
     */
    public function getCurrentLocation()
    {
        if (!$this->currentLocation) {
            $this->currentLocation = $this->registry->registry('current_location');
        }
        return $this->currentLocation;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        $location = $this->getCurrentLocation();
        if (!$location || !$location->getId()) {
            return $meta;
        }
        $meta = array_replace_recursive(
            $meta,
            [
                $this->_groupContainer => [
                    'children' => $this->getChildren(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __($this->_groupLabel),
                                'autoRender' => true,
                                'collapsible' => true,
                                'visible' => $this->getVisible(),
                                'opened' => $this->getOpened(),
                                'componentType' => Form\Fieldset::NAME,
                                'sortOrder' => $this->_sortOrder
                            ],
                        ],
                    ],
                ],
            ]
        );
        return $meta;
    }

    /**
     * Retrieve child meta configuration
     *
     * @return array
     */
    protected function getChildren()
    {
        $children = [
            'linked_container' => $this->getDashboardContainer(),
        ];
        return $children;
    }

    /**
     * Get Dashboard Container
     *
     * @return array
     */
    public function getDashboardContainer()
    {
        $container = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                        'sortOrder' => 10,

                    ],
                ],
            ],
            'children' => [
                'html_content' => [
                    'arguments' => [
                        'data' => [
                            'type' => 'html_content',
                            'name' => 'html_content',
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/html',
                                'content' => ObjectManager::getInstance()
                                    ->create(\Magestore\Webpos\Block\Adminhtml\LinkedPos::class)
                                    ->toHtml()
                            ]
                        ]
                    ]
                ]
            ]
        ];
        return $container;
    }
}
