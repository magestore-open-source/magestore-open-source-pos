<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\DataProvider\Location\Form\Modifier;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Store\Ui\Component\Listing\Column\Store\Options;
use Magento\Ui\Component\Form;

/**
 * Modifier - General
 */
class General extends AbstractModifier
{
    /**
     * @var Options
     */
    protected $optionStoreView;

    /**
     * @var string
     */
    protected $groupContainer = 'general_information';

    /**
     * @var string
     */
    protected $groupLabel = 'General Information';

    /**
     * @var string
     */
    protected $addressContainer = 'address_information';

    /**
     * @var string
     */
    protected $addressLabel = 'Address';

    /**
     * @var int
     */
    protected $sortOrder = 80;

    /**
     * @var bool
     */
    protected $opened = true;

    /**
     * General constructor.
     * @param ObjectManagerInterface $objectManager
     * @param Registry $registry
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     * @param Options $optionStoreView
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Registry $registry,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        Options $optionStoreView
    ) {
        parent::__construct($objectManager, $registry, $request, $urlBuilder);
        $this->optionStoreView = $optionStoreView;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                $this->groupContainer => [
                    'children' => $this->getGeneralChildren(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __($this->groupLabel),
                                'collapsible' => true,
                                'dataScope' => 'data',
                                'visible' => $this->getVisible(),
                                'opened' => $this->getOpened(),
                                'componentType' => Form\Fieldset::NAME,
                                'sortOrder' => $this->getSortOrder()
                            ],
                        ],
                    ],
                ],
                $this->addressContainer => [
                    'children' => $this->getAddressChildren(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __($this->addressLabel),
                                'collapsible' => true,
                                'dataScope' => 'data',
                                'visible' => $this->getVisible(),
                                'opened' => $this->getOpened(),
                                'componentType' => Form\Fieldset::NAME,
                                'sortOrder' => '20'
                            ],
                        ],
                    ],
                ],
            ]
        );
        return $meta;
    }

    /**
     * Get General Children
     *
     * @return array
     */
    protected function getGeneralChildren()
    {
        $children = [
            'name' => $this->addFormFieldText('Location Name', 'input', 20, ['required-entry' => true]),
            'email' => $this->addFormFieldText('Contact Email', 'input', 30),
            'telephone' => $this->addFormFieldText('Telephone', 'input', 40),
            'description' => $this->addFormFieldTextArea('Description', 50)
        ];

        return $children;
    }

    /**
     * Get Address Children
     *
     * @return array
     */
    protected function getAddressChildren()
    {
        $children = [
            'street' => $this->addFormFieldText('Street', 'input', 20, ['required-entry' => true]),
            'city' => $this->addFormFieldText('City', 'input', 30, ['required-entry' => true]),
            'region' => $this->addFormFieldText('State or Province', 'input', 40, ['required-entry' => true]),
            'country_id' => $this->addFormFieldTextArea('Country', 50, ['required-entry' => true]),
            'postcode' => $this->addFormFieldTextArea('Zip/Postal Code', 60, ['required-entry' => true])
        ];

        return $children;
    }

    /**
     * Retrieve countries
     *
     * @return array|null
     */
    protected function getStoreViews()
    {
        return $this->optionStoreView->toOptionArray();
    }
}
