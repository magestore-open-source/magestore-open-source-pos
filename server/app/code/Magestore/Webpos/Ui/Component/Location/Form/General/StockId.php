<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Ui\Component\Location\Form\General;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\InventoryCatalogApi\Api\DefaultStockProviderInterface;
use Magento\Ui\Component\Form\Field;
use Magestore\Webpos\Api\WebposManagementInterface;

/**
 * Form - StockId
 */
class StockId extends Field
{
    /**
     * @var WebposManagementInterface
     */
    private $webposManagement;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * StockId constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param WebposManagementInterface $webposManagement
     * @param ObjectManagerInterface $objectManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        WebposManagementInterface $webposManagement,
        ObjectManagerInterface $objectManager,
        array $components = [],
        array $data = []
    ) {
        $this->webposManagement = $webposManagement;
        $this->objectManager = $objectManager;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Add js listener to reset button
     *
     * @return void
     * @throws LocalizedException
     * @since 100.2.0
     */
    public function prepare()
    {
        parent::prepare();

        $isMSIEnabled = $this->webposManagement->isMSIEnable();
        $config = $this->getData('config');

        if (!$isMSIEnabled) {
            $config['componentDisabled'] = true;
        } else {
            if ($this->webposManagement->isWebposStandard()) {
                $config['disabled'] = true;
                /** @var DefaultStockProviderInterface $defaultStockProvider */
                $defaultStockProvider = $this->objectManager->create(DefaultStockProviderInterface::class);
                $config['default'] = $defaultStockProvider->getId();
            }
        }
        $this->setData('config', $config);
    }
}
