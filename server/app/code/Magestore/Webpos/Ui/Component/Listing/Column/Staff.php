<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;
use Magestore\Appadmin\Api\Data\Staff\StaffInterface;
use Magestore\Appadmin\Model\ResourceModel\Staff\Staff\Collection;

/**
 * Column - Staff
 */
class Staff implements OptionSourceInterface
{
    /**
     * @var Collection
     */
    protected $staffCollection;
    /**
     * Construct
     *
     * @param Collection $staffCollection
     */
    public function __construct(
        Collection $staffCollection
    ) {
        $this->staffCollection = $staffCollection;
    }
    /**
     * Get product type labels array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $options = [];
        $staffs = $this->staffCollection
            ->setOrder(StaffInterface::NAME, 'ASC')
            ->loadData();
        if (count($staffs) > 0) {
            foreach ($staffs as $staff) {
                $options[$staff->getId()] = (string) $staff->getUsername();
            }
        }
        return $options;
    }
    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
    /**
     * Get product type labels array for option element
     *
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }
}
