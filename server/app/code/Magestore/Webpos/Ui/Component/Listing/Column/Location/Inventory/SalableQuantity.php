<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types = 1);

namespace Magestore\Webpos\Ui\Component\Listing\Column\Location\Inventory;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface;
use Magento\InventoryConfigurationApi\Model\IsSourceItemManagementAllowedForProductTypeInterface;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Add grid column with salable quantity data
 */
class SalableQuantity extends Column
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var RequestInterface
     */
    private $requestInterface;

    /**
     * SalableQuantity constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ObjectManagerInterface $objectManager
     * @param RequestInterface $requestInterface
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ObjectManagerInterface $objectManager,
        RequestInterface $requestInterface,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->objectManager = $objectManager;
        $this->requestInterface = $requestInterface;
    }

    /**
     * @inheritdoc
     */
    public function prepareDataSource(array $dataSource)
    {
        $stockId = $this->requestInterface->getParam('stock_id');
        if ($stockId && $dataSource['data']['totalRecords'] > 0) {
            $isSourceItemManagementAllowedForProductType = $this->objectManager
                ->get(IsSourceItemManagementAllowedForProductTypeInterface::class);
            foreach ($dataSource['data']['items'] as &$row) {
                $row['salable_quantity'] =
                    $isSourceItemManagementAllowedForProductType->execute($row['type_id']) === true
                        ? $this->getSalableQuantityDataBySku($row['sku'], (int) $stockId)
                        : null;
            }
        }
        unset($row);

        return $dataSource;
    }

    /**
     * Get Salable Quantity Data By Sku
     *
     * @param string $sku
     * @param int $stockId
     * @return array
     */
    private function getSalableQuantityDataBySku($sku, $stockId)
    {
        /** @var GetStockItemConfigurationInterface $getStockItemConfiguration */
        $getStockItemConfiguration = $this->objectManager
            ->get(GetStockItemConfigurationInterface::class);
        $stockItemConfiguration = $getStockItemConfiguration->execute($sku, $stockId);
        $isManageStock = $stockItemConfiguration->isManageStock();
        return [
            'qty' => $isManageStock ?
                $this->objectManager
                    ->get(GetProductSalableQtyInterface::class)
                    ->execute($sku, $stockId) :
                null,
            'manage_stock' => $isManageStock,
        ];
    }
}
