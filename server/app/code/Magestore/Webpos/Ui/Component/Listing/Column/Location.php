<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;
use Magestore\Webpos\Model\ResourceModel\Location\Location\Collection;

/**
 * Web POS Location
 */
class Location implements OptionSourceInterface
{
    /**
     * @var Collection
     */
    protected $_webposLocation;
    /**
     * Construct
     *
     * @param Collection $webposLocation
     */
    public function __construct(
        Collection $webposLocation
    ) {
        $this->_webposLocation = $webposLocation;
    }
    /**
     * Get locations labels array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $options = [];
        $locations = $this->_webposLocation->loadData();
        if (count($locations) > 0) {
            foreach ($locations as $location) {
                $options[$location->getId()] = (string) $location->getName();
            }
        }
        return $options;
    }
    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
    /**
     * Get location labels array for option element
     *
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }
}
