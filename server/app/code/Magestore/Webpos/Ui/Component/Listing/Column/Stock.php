<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magestore\Webpos\Api\WebposManagementInterface;

/**
 * Column - Stock
 */
class Stock extends Column
{
    /**
     * @var WebposManagementInterface
     */
    protected $webposManagement;

    /**
     * Stock constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param WebposManagementInterface $webposManagement
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        WebposManagementInterface $webposManagement,
        array $components = [],
        array $data = []
    ) {
        $this->webposManagement = $webposManagement;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Produce and return block's html output
     *
     * @return string
     */
    public function toHtml()
    {
        return $this->render();
    }

    /**
     * @inheritDoc
     */
    public function prepare()
    {
        parent::prepare();

        $isMSIEnable = $this->webposManagement->isMSIEnable();

        if (!$isMSIEnable) {
            $config = $this->getData('config');
            $config['componentDisabled'] = true;
            $this->setData('config', $config);
        }
    }
}
