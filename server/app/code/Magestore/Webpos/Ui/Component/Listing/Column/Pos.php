<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;
use Magestore\Webpos\Model\ResourceModel\Pos\Pos\Collection;
use Magestore\Webpos\Model\ResourceModel\Pos\Pos\CollectionFactory;

/**
 * Class \Magestore\Webpos\Ui\Component\Listing\Column\Pos
 */
class Pos implements OptionSourceInterface
{
    /**
     * @var Collection
     */
    protected $collectionFactory;

    /**
     * Pos constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get pos labels array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $options = [];
        $webposPosCollection = $this->collectionFactory->create();
        foreach ($webposPosCollection as $pos) {
            $options[$pos->getId()] = (string)$pos->getPosName();
        }
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }

    /**
     * Get pos labels array for option element
     *
     * @return array
     */
    public function getOptions()
    {
        $results = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $results[] = ['value' => $index, 'label' => $value];
        }
        return $results;
    }
}
