<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Country;

use Exception;
use Magento\Directory\Api\CountryInformationAcquirerInterfaceFactory;
use Magento\Directory\Helper\Data;
use Magento\Directory\Model\AllowedCountries;
use Magento\Directory\Model\CountryFactory;
use Magestore\Webpos\Api\Data\Country\CountryInterface;
use Magestore\Webpos\Api\Data\Country\CountryInterfaceFactory;
use Magestore\Webpos\Api\Data\Country\RegionInterfaceFactory;

/**
 * Model - CountryRepository
 */
class CountryRepository
{

    /**
     * @var AllowedCountries
     */
    protected $allowedCountries;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * @var CountryInterfaceFactory
     */
    protected $countryInterfaceFactory;

    /**
     * @var
     */
    protected $countryInformationAcquirer;

    /**
     * @var CountryInformationAcquirerInterfaceFactory
     */
    protected $countryInformationAcquirerInterfaceFactory;

    /**
     * @var RegionInterfaceFactory
     */
    protected $regionInterfaceFactory;

    /**
     * @var Data
     */
    protected $directoryHelper;

    /**
     * CountryRepository constructor.
     * @param AllowedCountries $allowedCountries
     * @param CountryFactory $countryFactory
     * @param CountryInterfaceFactory $countryInterfaceFactory
     * @param RegionInterfaceFactory $regionInterfaceFactory
     * @param CountryInformationAcquirerInterfaceFactory $countryInformationAcquirerInterfaceFactory
     * @param Data $directoryHelper
     */
    public function __construct(
        AllowedCountries $allowedCountries,
        CountryFactory $countryFactory,
        CountryInterfaceFactory $countryInterfaceFactory,
        RegionInterfaceFactory $regionInterfaceFactory,
        CountryInformationAcquirerInterfaceFactory $countryInformationAcquirerInterfaceFactory,
        Data $directoryHelper
    ) {
        $this->allowedCountries = $allowedCountries;
        $this->countryFactory = $countryFactory;
        $this->countryInterfaceFactory = $countryInterfaceFactory;
        $this->regionInterfaceFactory = $regionInterfaceFactory;
        $this->countryInformationAcquirerInterfaceFactory = $countryInformationAcquirerInterfaceFactory;
        $this->directoryHelper = $directoryHelper;
    }

    /**
     * Get list country
     *
     * @return CountryInterface[]
     */
    public function getList()
    {
        $countryArray = [];

        $allowedCountries = $this->allowedCountries->getAllowedCountries();
        $requiredStateCountry = $this->directoryHelper->getCountriesWithStatesRequired();

        foreach ($allowedCountries as $code) {

            $countryModel = $this->countryFactory->create()->loadByCode($code);
            $countryInstance = $this->countryInterfaceFactory->create();
            try {
                $info = $this->countryInformationAcquirerInterfaceFactory->create()->getCountryInfo($code);
                $name = $info->getFullNameLocale();
            } catch (Exception $e) {
                $name = '';
            }
            $regionCollectionArray = [];
            $regionCollection = $countryModel->getRegions();
            if ($regionCollection->getSize()) {
                foreach ($regionCollection as $region) {
                    $regionModel = $this->regionInterfaceFactory->create();
                    $regionModel->setId($region->getId());
                    $regionModel->setName($region->getName());
                    $regionModel->setCode($region->getCode());
                    $regionCollectionArray[] = $regionModel;
                }
                $countryInstance->setRegions($regionCollectionArray);
            }
            if (in_array($code, $requiredStateCountry)) {
                $countryInstance->setRegionRequire(1);
            } else {
                $countryInstance->setRegionRequire(0);
            }

            $countryInstance->setId($countryModel->getId());
            $countryInstance->setName($name);
            $countryArray[] = $countryInstance;
        }

        return $countryArray;
    }
}
