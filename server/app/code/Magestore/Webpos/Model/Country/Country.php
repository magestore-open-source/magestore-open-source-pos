<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Country;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Country\CountryInterface;
use Magestore\Webpos\Api\Data\Country\RegionInterface;

/**
 * Model - Country
 */
class Country extends DataObject implements CountryInterface
{
    /**
     * Get ID
     *
     * @api
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set ID
     *
     * @api
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
    /**
     * Get name
     *
     * @api
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set name
     *
     * @api
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
    /**
     * Get region require
     *
     * @api
     * @return int
     */
    public function getRegionRequire()
    {
        return $this->getData(self::REGION_REQUIRE);
    }

    /**
     * Set region require
     *
     * @api
     * @param int $regionRequire
     * @return $this
     */
    public function setRegionRequire($regionRequire)
    {
        return $this->setData(self::REGION_REQUIRE, $regionRequire);
    }
    /**
     * Get regions
     *
     * @api
     * @return RegionInterface[]
     */
    public function getRegions()
    {
        return $this->getData(self::REGIONS);
    }

    /**
     * Set regions
     *
     * @api
     * @param RegionInterface[] $regions
     * @return $this
     */
    public function setRegions($regions)
    {
        return $this->setData(self::REGIONS, $regions);
    }
}
