<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\MultiSourceInventory;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\SourceItemRepositoryInterface;
use Magestore\Webpos\Api\MultiSourceInventory\SourceManagementInterface;

/**
 * Model - SourceManagement
 */
class SourceManagement implements SourceManagementInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;

    /**
     * SourceManagement constructor.
     * @param ObjectManagerInterface $objectManager
     * @param SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
    ) {
        $this->objectManager = $objectManager;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
    }

    /**
     * @inheritDoc
     */
    public function getSourceItemsMap($sku, $sourceCodes)
    {
        $searchCriteria = $this->searchCriteriaBuilderFactory
            ->create()
            ->addFilter(ProductInterface::SKU, $sku)
            ->addFilter('source_code', $sourceCodes, 'in')
            ->create();
        /** @var SourceItemRepositoryInterface $sourceItemRepository */
        $sourceItemRepository = $this->objectManager->get(SourceItemRepositoryInterface::class);
        $sourceItems = $sourceItemRepository->getList($searchCriteria)->getItems();
        $sourceItemsMap = [];
        if ($sourceItems) {
            foreach ($sourceItems as $sourceItem) {
                $sourceItemsMap[$sourceItem->getSourceCode()] = $sourceItem;
            }
        }
        return $sourceItemsMap;
    }
}
