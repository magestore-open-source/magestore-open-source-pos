<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\MultiSourceInventory;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\Data\StockInterface;
use Magento\InventoryApi\Api\GetStockSourceLinksInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventoryApi\Api\StockRepositoryInterface;
use Magento\InventoryCatalogApi\Api\DefaultStockProviderInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Webpos\Api\MultiSourceInventory\SourceManagementInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Api\WebposManagementInterface;
use Magestore\Webpos\Helper\Product\CustomSale;
use Magestore\Webpos\Model\Checkout\PosOrder;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;

/**
 * Class StockManagement
 *
 * Used for api management stock
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class StockManagement implements StockManagementInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var WebposManagementInterface
     */
    protected $webposManagement;

    /**
     * @var SourceManagementInterface
     */
    protected $sourceManagement;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepository;

    /**
     * @var SearchCriteriaBuilderFactory
     */
    protected $searchCriteriaBuilderFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var GetSessionByIdInterface
     */
    protected $getSessionById;

    /**
     * StockManagement constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param RequestInterface $request
     * @param Registry $coreRegistry
     * @param WebposManagementInterface $webposManagement
     * @param SourceManagementInterface $sourceManagement
     * @param ProductRepositoryInterface $productRepository
     * @param LocationRepositoryInterface $locationRepository
     * @param SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param GetSessionByIdInterface $getSessionById
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        RequestInterface $request,
        Registry $coreRegistry,
        WebposManagementInterface $webposManagement,
        SourceManagementInterface $sourceManagement,
        ProductRepositoryInterface $productRepository,
        LocationRepositoryInterface $locationRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        DataObjectHelper $dataObjectHelper,
        GetSessionByIdInterface $getSessionById
    ) {
        $this->objectManager = $objectManager;
        $this->request = $request;
        $this->coreRegistry = $coreRegistry;
        $this->webposManagement = $webposManagement;
        $this->sourceManagement = $sourceManagement;
        $this->productRepository = $productRepository;
        $this->locationRepository = $locationRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->getSessionById = $getSessionById;
    }

    /**
     * Get stock
     *
     * @return StockInterface|null
     */
    public function getStock()
    {
        $stockId = $this->getStockId();
        if ($stockId) {
            /** @var StockRepositoryInterface $stockRepository */
            $stockRepository = $this->objectManager->create(
                StockRepositoryInterface::class
            );
            $stock = $stockRepository->get($stockId);
            return $stock;
        }
        return null;
    }

    /**
     * Get stock id
     *
     * @return int|null
     */
    public function getStockId()
    {
        if ($this->webposManagement->isMSIEnable()) {
            try {
                $locationId = $this->request->getParam(
                    PosOrder::PARAM_ORDER_LOCATION_ID
                );
                if (!$locationId) {
                    $sessionId = $this->request->getParam(
                        RequestProcessor::SESSION_PARAM_KEY
                    );
                    $sessionLogin = $this->getSessionById->execute($sessionId);
                    $locationId = $sessionLogin->getLocationId();
                }
            } catch (Exception $e) {
                $locationId = $this->request->getParam(
                    PosOrder::PARAM_ORDER_LOCATION_ID
                );
            }

            if (!$locationId) {
                return null;
            }
            if ($this->webposManagement->isWebposStandard()) {
                /** @var DefaultStockProviderInterface $defaultStockProvider */
                $defaultStockProvider = $this->objectManager->create(
                    DefaultStockProviderInterface::class
                );
                return $defaultStockProvider->getId();
            } else {
                $location = $this->locationRepository->getById($locationId);
                return $location->getStockId();
            }
        }
        return null;
    }

    /**
     * Get linked source code
     *
     * @param int $stockId
     * @return array|string[]
     */
    public function getLinkedSourceCodesByStockId($stockId)
    {
        $searchCriteria = $this->searchCriteriaBuilderFactory
            ->create()
            ->addFilter('stock_id', $stockId)
            ->create();
        /** @var GetStockSourceLinksInterface $getStockSourceLink */
        $getStockSourceLink = $this->objectManager->get(
            GetStockSourceLinksInterface::class
        );
        $stockSourcesLink = $getStockSourceLink->execute($searchCriteria);
        $linkedSources = [];
        foreach ($stockSourcesLink->getItems() as $item) {
            $linkedSources[] = $item->getSourceCode();
        }
        return $linkedSources;
    }

    /**
     * Add custom sale
     *
     * @param int $stockId
     * @return StockManagementInterface
     */
    public function addCustomSaleToStock($stockId)
    {
        if (!$this->webposManagement->isMSIEnable()) {
            return $this;
        }

        try {
            $product = $this->productRepository->get(CustomSale::SKU);
            if ($product->getId()) {
                $sourceCodes = $this->getLinkedSourceCodesByStockId($stockId);
                if (empty($sourceCodes)) {
                    return $this;
                }
                $sourceItemsMap = $this->sourceManagement->getSourceItemsMap(
                    CustomSale::SKU,
                    $sourceCodes
                );
                foreach ($sourceCodes as $sourceCode) {
                    if (isset($sourceItemsMap[$sourceCode])) {
                        return $this;
                    }
                    $this->createSourceItem(
                        CustomSale::SKU,
                        $sourceCode,
                        0,
                        1
                    );
                }
            }
        } catch (Exception $e) {
            return $this;
        }

        return $this;
    }

    /**
     * Get stock id
     *
     * @param OrderInterface $order
     * @return int|null
     */
    public function getStockIdFromOrder($order)
    {
        $locationId = $order->getPosLocationId();
        if ($locationId) {
            $location = $this->locationRepository->getById($locationId);
            if ($location->getLocationId()) {
                return $location->getStockId();
            }
            return null;
        }
        return null;
    }

    /**
     * Get source item
     *
     * @param string $sku
     * @param string $sourceCode
     * @param int $quantity
     * @param int $status
     * @return void
     */
    public function createSourceItem($sku, $sourceCode, $quantity = 0, $status = 1)
    {
        /** @var SourceItemInterface $sourceItem */
        $sourceItem = $this->objectManager
            ->create(SourceItemInterface::class);
        $sourceItemData = [
            'sku' => $sku,
            'source_code' => $sourceCode,
            'quantity' => $quantity,
            'status' => $status
        ];
        $this->dataObjectHelper->populateWithArray(
            $sourceItem,
            $sourceItemData,
            SourceItemInterface::class
        );
        $sourceItemsForSave = [$sourceItem];
        /** @var SourceItemsSaveInterface $sourceItemSave */
        $sourceItemSave = $this->objectManager->create(SourceItemsSaveInterface::class);
        $sourceItemSave->execute($sourceItemsForSave);
    }
}
