<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Location;

use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\ValidatorException;
use Magestore\Webpos\Api\Data\Location\LocationInterface;
use Magento\Framework\Api\SortOrder;
use Magestore\Appadmin\Api\DataProvider\GetStaffByIdInterface;
use Magestore\Webpos\Api\Data\Location\LocationSearchResultsInterfaceFactory;
use Magestore\Webpos\Api\Data\Pos\PosInterface;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Location\Location\Collection;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory;
use Magestore\Webpos\Model\Source\Adminhtml\Status;

/**
 * Location LocationRepository
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class LocationRepository implements LocationRepositoryInterface
{
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Location\Location
     */
    protected $resourceLocation;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var LocationFactory
     */
    protected $locationFactory;

    /**
     * @var LocationSearchResultsInterfaceFactory
     */
    protected $locationSearchResultsFactory;

    /**
     * @var PosRepositoryInterface
     */
    protected $posRepositoryInterface;
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;

    /**
     * @var SearchCriteriaBuilderFactory
     */
    protected $searchCriteriaBuilderFactory;

    protected $getStaffById;

    /**
     * LocationRepository constructor.
     *
     * @param \Magestore\Webpos\Model\ResourceModel\Location\Location $resourceLocation
     * @param CollectionFactory $collectionFactory
     * @param LocationFactory $locationFactory
     * @param LocationSearchResultsInterfaceFactory $locationSearchResultsFactory
     * @param PosRepositoryInterface $posRepository
     * @param RequestInterface $request
     * @param SessionRepositoryInterface $sessionRepository
     * @param SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     * @param GetStaffByIdInterface $getStaffById
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magestore\Webpos\Model\ResourceModel\Location\Location $resourceLocation,
        CollectionFactory $collectionFactory,
        LocationFactory $locationFactory,
        LocationSearchResultsInterfaceFactory $locationSearchResultsFactory,
        PosRepositoryInterface $posRepository,
        RequestInterface $request,
        SessionRepositoryInterface $sessionRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        GetStaffByIdInterface $getStaffById
    ) {
        $this->resourceLocation = $resourceLocation;
        $this->collectionFactory = $collectionFactory;
        $this->locationFactory = $locationFactory;
        $this->locationSearchResultsFactory = $locationSearchResultsFactory;
        $this->posRepositoryInterface = $posRepository;
        $this->request = $request;
        $this->sessionRepository = $sessionRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->getStaffById = $getStaffById;
    }

    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        $location = $this->locationFactory->create();
        $this->resourceLocation->load($location, $id);
        if ($location->getId()) {
            return $location;
        } else {
            throw new NoSuchEntityException(
                __('Location with id "%1" does not exist.', $id)
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteria $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders === null) {
            $sortOrders = [];
        }
        /** @var SortOrder $sortOrder */
        foreach ($sortOrders as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
            );
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collectionSize = $collection->getSize();
        $collection->load();
        $searchResults = $this->locationSearchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collectionSize);
        return $searchResults;
    }

    /**
     * @inheritdoc
     */
    public function getAllLocation()
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        return $collection;
    }

    /**
     * @inheritdoc
     */
    public function save(LocationInterface $location)
    {
        return $this->resourceLocation->save($location);
    }

    /**
     * @inheritdoc
     */
    public function delete(LocationInterface $location)
    {
        return $this->deleteById($location->getLocationId());
    }

    /**
     * @inheritdoc
     */
    public function deleteById($locationId)
    {
        /* @var Location $location */
        $location = $this->getById($locationId);

        if ($location->getId()) {
            // check location
            $checkDelete = true;
            if (count($this->posRepositoryInterface->getPosByLocationId($location->getLocationId())) > 0) {
                $checkDelete = false;
            }

            if ($checkDelete) {
                $this->resourceLocation->delete($location);
                return true;
            } else {
                throw new ValidatorException(
                    __('The operations failed. Some locations are still working. You can\'t delete them!')
                );
            }
        } else {
            throw new NoSuchEntityException(
                __('Location with id "%1" does not exist.', $locationId)
            );
        }
    }

    /**
     * Get Location Ids From Staff Id
     *
     * @param int $staffId
     * @return array
     * @throws NoSuchEntityException
     */
    private function getLocationIdsFromStaffId($staffId)
    {
        $staff = $this->getStaffById->execute($staffId);
        if (!$staff->getStaffId()) {
            throw new NoSuchEntityException(__('Staff does not exist.'));
        }

        $locationIds = $staff->getLocationIds();
        return explode(',', $locationIds);
    }

    /**
     * @inheritdoc
     */
    public function getListAvailable($staffId)
    {
        $locationIdsArray = $this->getLocationIdsFromStaffId($staffId);
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter($collection->getResource()->getIdFieldName(), ['in' => $locationIdsArray]);

        $locations = [];
        $numberPosAssign = 0;

        /** @var LocationInterface $item */
        foreach ($collection as $item) {
            $posCollection = $this->posRepositoryInterface->getPosByLocationId($item->getLocationId());
            $posArray = [];
            /** @var PosInterface $pos */
            foreach ($posCollection as $pos) {
                if ($pos->getStatus() == Status::STATUS_ENABLED) {
                    $posArray[] = $pos;
                    $numberPosAssign++;
                }
            }
            $item->setPos($posArray);
            $locations[] = $item;
        }

        if (!count($locations) || !$numberPosAssign) {
            return false;
        }

        return $locations;
    }

    /**
     * Get List Location With Staff
     *
     * @param int $staffId
     * @return array|bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getListLocationWithStaff($staffId)
    {
        $locationIdsArray = $this->getLocationIdsFromStaffId($staffId);
        $collection = $this->collectionFactory->create();

        $locations = [];
        $numberPosAssign = 0;

        /** @var LocationInterface $item */
        foreach ($collection as $item) {
            $locationId = $item->getLocationId();
            if (!in_array($locationId, $locationIdsArray)) {
                $locations[] = $item;
                continue;
            }

            $posCollection = $this->posRepositoryInterface->getPosByLocationId($locationId);
            $posArray = [];
            /** @var PosInterface $pos */
            foreach ($posCollection as $pos) {
                if ($pos->getStatus() == Status::STATUS_ENABLED) {
                    $posArray[] = $pos;
                    $numberPosAssign++;
                }
            }

            $item->setPos($posArray);
            $locations[] = $item;
        }

        if (!count($locations) || !$numberPosAssign) {
            return false;
        }

        return $locations;
    }

    /**
     * Get Location Ids By Stock Id
     *
     * @param int $stockId
     * @return int[]|null
     */
    public function getLocationIdsByStockId($stockId)
    {
        $objectManager = ObjectManager::getInstance();
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $objectManager->create(SearchCriteriaBuilder::class);
        $searchCriteria = $searchCriteriaBuilder
            ->addFilter(LocationInterface::STOCK_ID, $stockId)
            ->create();
        $locations = $this->getList($searchCriteria)->getItems();
        $locationIds = [];
        foreach ($locations as $location) {
            $locationIds[] = $location->getLocationId();
        }
        return $locationIds;
    }
}
