<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Location;

use Magento\Customer\Api\Data\RegionInterface;
use Magento\Directory\Model\Country;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\Registry;
use Magestore\Webpos\Api\Data\Location\LocationInterface;
use Magestore\Webpos\Api\Data\Staff\Login\Location\AddressInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Model\ResourceModel\Location\Location\Collection;

/**
 * Model Location
 *
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Location extends AbstractModel implements LocationInterface
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'webpos_location';
    /**
     * @var Country
     */
    protected $country;
    /**
     * @var RegionFactory
     */
    protected $regionFactory;
    /**
     * @var AddressInterface
     */
    protected $addressInterface;
    /**
     * @var RegionInterface
     */
    protected $region;

    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * Location constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param \Magestore\Webpos\Model\ResourceModel\Location\Location $resource
     * @param Collection $resourceCollection
     * @param Country $country
     * @param RegionFactory $regionFactory
     * @param AddressInterface $addressInterface
     * @param RegionInterface $region
     * @param Manager $moduleManager
     * @param StockManagementInterface $stockManagement
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Registry $registry,
        \Magestore\Webpos\Model\ResourceModel\Location\Location $resource,
        Collection $resourceCollection,
        Country $country,
        RegionFactory $regionFactory,
        AddressInterface $addressInterface,
        RegionInterface $region,
        Manager $moduleManager,
        StockManagementInterface $stockManagement,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->country = $country;
        $this->regionFactory = $regionFactory;
        $this->addressInterface = $addressInterface;
        $this->region = $region;
        $this->moduleManager = $moduleManager;
        $this->stockManagement = $stockManagement;
    }

    /**
     * @inheritdoc
     */
    public function getLocationId()
    {
        return $this->getData(self::LOCATION_ID);
    }

    /**
     * @inheritdoc
     */
    public function setLocationId($locationId)
    {
        return $this->setData(self::LOCATION_ID, $locationId);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getData(self::LOCATION_ID);
    }

    /**
     * @inheritdoc
     */
    public function setId($locationId)
    {
        return $this->setData(self::LOCATION_ID, $locationId);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritdoc
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @inheritdoc
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @inheritdoc
     */
    public function getTelephone()
    {
        if ($telephone = $this->getData(self::TELEPHONE)) {
            return $telephone;
        } else {
            return "";
        }
    }

    /**
     * @inheritdoc
     */
    public function setTelephone($telephone)
    {
        return $this->setData(self::TELEPHONE, $telephone);
    }

    /**
     * @inheritdoc
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @inheritdoc
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * @inheritdoc
     */
    public function getStreet()
    {
        return $this->getData(self::STREET);
    }

    /**
     * @inheritdoc
     */
    public function setStreet($street)
    {
        return $this->setData(self::STREET, $street);
    }

    /**
     * @inheritdoc
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * @inheritdoc
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * @inheritdoc
     */
    public function getRegion()
    {
        return $this->getData(self::REGION);
    }

    /**
     * @inheritdoc
     */
    public function setRegion($region)
    {
        return $this->setData(self::REGION, $region);
    }

    /**
     * @inheritdoc
     */
    public function getRegionId()
    {
        return $this->getData(self::REGION_ID);
    }

    /**
     * @inheritdoc
     */
    public function setRegionId($regionId)
    {
        return $this->setData(self::REGION_ID, $regionId);
    }

    /**
     * @inheritdoc
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * @inheritdoc
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * @inheritdoc
     */
    public function getCountryId()
    {
        return $this->getData(self::COUNTRY_ID);
    }

    /**
     * @inheritdoc
     */
    public function setCountryId($countryId)
    {
        return $this->setData(self::COUNTRY_ID, $countryId);
    }

    /**
     * @inheritdoc
     */
    public function getPostcode()
    {
        return $this->getData(self::POSTCODE);
    }

    /**
     * @inheritdoc
     */
    public function setPostcode($postcode)
    {
        return $this->setData(self::POSTCODE, $postcode);
    }

    /**
     * @inheritdoc
     */
    public function getAddress()
    {
        $street = $this->getStreet();
        $city = $this->getCity();
        $region = $this->getRegion();
        $regionId = $this->getRegionId();
        $country = $this->getCountry();
        $countryId = $this->getCountryId();
        $postcode = $this->getPostcode();

        $regionCode = $region;
        if ($regionId) {
            $regionCode = $this->regionFactory->create()->load($regionId)->getCode();
        }

        $regionObject = $this->region;
        $regionObject->setRegion($region);
        $regionObject->setRegionCode($regionCode);
        $regionObject->setRegionId($regionId);

        $address = [
            'street' => $street,
            'city' => $city,
            'region' => $regionObject,
            'region_id' => $regionId,
            'country_id' => $countryId,
            'country' => $country,
            'postcode' => $postcode,
        ];
        return $this->addressInterface->setData($address);
    }

    /**
     * @inheritdoc
     */
    public function setAddress($address)
    {
        return $this->getData('address', $address);
    }

    /**
     * Get Stock Id
     *
     * @return int|null
     */
    public function getStockId()
    {
        return $this->getData(self::STOCK_ID);
    }

    /**
     * Set Stock Id
     *
     * @param int|null $stockId
     * @return LocationInterface
     */
    public function setStockId($stockId)
    {
        return $this->setData(self::STOCK_ID, $stockId);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave()
    {
        if ($regionId = $this->getRegionId()) {
            $rgModel = $this->regionFactory->create()->load($regionId);
            if ($rgModel->getId()) {
                $this->setRegion($rgModel->getName());
            }
        }

        $countryId = $this->getCountryId();
        $countryModel = $this->country->loadByCode($countryId);
        $this->setCountry($countryModel->getName());

        return parent::beforeSave();
    }

    /**
     * Processing object after save data
     *
     * @return $this
     */
    public function afterSave()
    {
        $stockId = $this->getStockId();
        if ($stockId) {
            $this->stockManagement->addCustomSaleToStock($stockId);
        }
        return parent::afterSave();
    }
}
