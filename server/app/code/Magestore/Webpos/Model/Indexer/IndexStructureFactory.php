<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Indexer;

use InvalidArgumentException;
use LogicException;
use Magento\Framework\Indexer\IndexStructureInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Search\EngineResolverInterface;

/**
 * Generator of Index structure
 *
 * Follow Magento\CatalogSearch\Model\Indexer\IndexStructureFactory
 *
 * @api
 * @since 100.1.0
 */
class IndexStructureFactory
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     * @since 100.1.0
     */
    protected $objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     * @since 100.1.0
     */
    protected $structures = null;
    /**
     * @var EngineResolverInterface
     */
    private $engineResolver;

    /**
     * Factory constructor
     *
     * @param ObjectManagerInterface $objectManager
     * @param EngineResolverInterface $engineResolver
     * @param string[] $structures
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        EngineResolverInterface $engineResolver,
        array $structures = []
    ) {
        $this->objectManager = $objectManager;
        $this->structures = $structures;
        $this->engineResolver = $engineResolver;
    }

    /**
     * Create index structure
     *
     * @param array $data
     * @return IndexStructureInterface
     * @since 100.1.0
     */
    public function create(array $data = [])
    {
        $currentStructure = $this->engineResolver->getCurrentSearchEngine();
        if (!isset($this->structures[$currentStructure])) {
            throw new LogicException(
                'There is no such index structure: ' . $currentStructure
            );
        }
        $indexStructure = $this->objectManager->create($this->structures[$currentStructure], $data);

        if (!$indexStructure instanceof IndexStructureInterface) {
            throw new InvalidArgumentException(
                $currentStructure . ' index structure doesn\'t implement '. IndexStructureInterface::class
            );
        }

        return $indexStructure;
    }
}
