<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Indexer;

use Exception;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Mview\ActionInterface;
use Magento\Framework\Search\EngineResolverInterface;
use Magestore\Webpos\Model\Indexer\Product\Action\Full;
use Magestore\Webpos\Model\Indexer\Product\Action\Row;
use Magestore\Webpos\Model\Indexer\Product\Action\Rows;
use Psr\Log\LoggerInterface;

/**
 * Class \Magestore\Webpos\Model\Indexer\Product
 */
class Product implements \Magento\Framework\Indexer\ActionInterface, ActionInterface
{
    /**
     * @var Row
     */
    protected $productFlatIndexerRow;

    /**
     * @var Rows
     */
    protected $productFlatIndexerRows;

    /**
     * @var Full
     */
    protected $productFlatIndexerFull;

    /**
     * @var EngineResolverInterface
     */
    protected $engineResolver;

    protected $elasticSearchIndexer;

    /**
     * Product constructor.
     *
     * @param Product\Action\Row $productFlatIndexerRow
     * @param Product\Action\Rows $productFlatIndexerRows
     * @param Product\Action\Full $productFlatIndexerFull
     * @param EngineResolverInterface $engineResolver
     */
    public function __construct(
        Row $productFlatIndexerRow,
        Rows $productFlatIndexerRows,
        Full $productFlatIndexerFull,
        EngineResolverInterface $engineResolver
    ) {
        $this->productFlatIndexerRow = $productFlatIndexerRow;
        $this->productFlatIndexerRows = $productFlatIndexerRows;
        $this->productFlatIndexerFull = $productFlatIndexerFull;
        $this->engineResolver = $engineResolver;
    }

    /**
     * @inheritDoc
     */
    public function executeFull()
    {
        try {
            if ($this->isElasticSearchEnable()) {
                $this->getElasticSearchIndexer()->executeFull();
            } else {
                $this->productFlatIndexerFull->execute();
            }
        } catch (Exception $e) {
            /** @var LoggerInterface $logger */
            $logger = ObjectManager::getInstance()
                ->create(LoggerInterface::class);
            $logger->info($e->getTraceAsString());
        }
    }

    /**
     * @inheritDoc
     */
    public function executeRow($id)
    {
        if ($this->isElasticSearchEnable()) {
            $this->getElasticSearchIndexer()->executeRow($id);
        } else {
            $this->productFlatIndexerRow->execute($id);
        }
    }

    /**
     * @inheritDoc
     */
    public function executeList(array $ids)
    {
        if ($this->isElasticSearchEnable()) {
            $this->getElasticSearchIndexer()->executeList($ids);
        } else {
            $this->productFlatIndexerRows->execute($ids);
        }
    }

    /**
     * @inheritDoc
     */
    public function execute($ids)
    {
        if ($this->isElasticSearchEnable()) {
            $this->getElasticSearchIndexer()->executeList($ids);
        } else {
            $this->executeList($ids);
        }
    }

    /**
     * Is Elastic Search Enable
     *
     * @return bool
     */
    public function isElasticSearchEnable()
    {
        $currentHandler = $this->engineResolver->getCurrentSearchEngine();
        if (in_array($currentHandler, ['elasticsearch', 'elasticsearch5', 'elasticsearch6', 'elasticsearch7'])) {
            return true;
        }
        return false;
    }

    /**
     * Get Elastic Search Indexer
     *
     * @return Fulltext
     */
    public function getElasticSearchIndexer()
    {
        if (!$this->elasticSearchIndexer) {
            $data = [
                "indexer_id" => "webpos_productsearch_fulltext",
                "action_class" => Fulltext::class,
                "title" => "POS Product Search",
                "description" => "Rebuild Catalog product fulltext search index",
                "structure" => IndexStructure::class
            ];
            $this->elasticSearchIndexer = ObjectManager::getInstance()
                ->create(
                    Fulltext::class,
                    [
                        "data" => $data
                    ]
                );
        }
        return $this->elasticSearchIndexer;
    }
}
