<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Indexer\Preference\InventoryElasticsearch\Plugin;

use Magento\CatalogSearch\Model\Indexer\Fulltext\Action\DataProvider;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\InventoryElasticsearch\Plugin\CatalogSearch\Model\Indexer\Fulltext\Action\DataProvider\StockedProductFilterByInventoryStock as CoreStockedProductFilterByInventoryStock; // phpcs:ignore

/**
 * Disable plugin before when reindexing POS data
 */
class StockedProductFilterByInventoryStock
{
    /**
     * Filter out stock options for configurable product.
     *
     * @param CoreStockedProductFilterByInventoryStock $stockedProductFilterByInventoryStock
     * @param mixed $result
     * @param DataProvider $dataProvider
     * @param array $indexData
     * @param array $productData
     * @param int $storeId
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterBeforePrepareProductIndex(
        CoreStockedProductFilterByInventoryStock $stockedProductFilterByInventoryStock,
        $result,
        DataProvider $dataProvider,
        $indexData,
        $productData,
        $storeId
    ) {
        $om = ObjectManager::getInstance();
        /** @var Registry $registry */
        $registry = $om->get(Registry::class);
        if ($registry->registry('webpos_productsearch_fulltext')) {
            return [
                $indexData,
                $productData,
                $storeId
            ];
        }
        return $result;
    }
}
