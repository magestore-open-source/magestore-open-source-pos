<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Indexer\Product;

use Magento\Catalog\Model\Indexer\Product\Flat\Table\BuilderInterfaceFactory;
use Magento\Framework\App\ResourceConnection;
use Magestore\Webpos\Helper\Product\Indexer;

/**
 * Product - TableBuilder
 */
class TableBuilder extends \Magento\Catalog\Model\Indexer\Product\Flat\TableBuilder
{
    /**
     * @param Indexer $productIndexerHelper
     * @param ResourceConnection $resource
     * @param BuilderInterfaceFactory|null $tableBuilderFactory
     */
    public function __construct( // phpcs:ignore
        Indexer $productIndexerHelper,
        ResourceConnection $resource,
        BuilderInterfaceFactory $tableBuilderFactory = null
    ) {
        parent::__construct($productIndexerHelper, $resource, $tableBuilderFactory);
    }
}
