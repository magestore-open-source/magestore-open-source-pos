<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Indexer\Product\Action;

use Magento\Catalog\Model\Product\Type;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Model\Indexer\Product\FlatTableBuilder;
use Magestore\Webpos\Model\Indexer\Product\TableBuilder;

/**
 * Indexer Product Action - Rows
 */
class Rows extends \Magento\Catalog\Model\Indexer\Product\Flat\Action\Rows
{
    /**
     * @param ResourceConnection $resource
     * @param StoreManagerInterface $storeManager
     * @param \Magestore\Webpos\Helper\Product\Indexer $productHelper
     * @param Type $productType
     * @param TableBuilder $tableBuilder
     * @param FlatTableBuilder $flatTableBuilder
     * @param Eraser $flatItemEraser
     */
    public function __construct( // phpcs:ignore
        ResourceConnection $resource,
        StoreManagerInterface $storeManager,
        \Magestore\Webpos\Helper\Product\Indexer $productHelper,
        Type $productType,
        TableBuilder $tableBuilder,
        FlatTableBuilder $flatTableBuilder,
        Eraser $flatItemEraser
    ) {
        parent::__construct(
            $resource,
            $storeManager,
            $productHelper,
            $productType,
            $tableBuilder,
            $flatTableBuilder,
            $flatItemEraser
        );
    }
}
