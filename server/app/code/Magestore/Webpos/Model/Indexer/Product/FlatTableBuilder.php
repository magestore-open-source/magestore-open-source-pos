<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Indexer\Product;

use Magento\Catalog\Model\Indexer\Product\Flat\TableDataInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Helper\Product\Indexer;

/**
 * Indexer - Product - FlatTableBuilder
 */
class FlatTableBuilder extends \Magento\Catalog\Model\Indexer\Product\Flat\FlatTableBuilder
{
    /**
     * @param Indexer $productIndexerHelper
     * @param ResourceConnection $resource
     * @param ScopeConfigInterface $config
     * @param StoreManagerInterface $storeManager
     * @param TableDataInterface $tableData
     */
    public function __construct( // phpcs:ignore
        Indexer $productIndexerHelper,
        ResourceConnection $resource,
        ScopeConfigInterface $config,
        StoreManagerInterface $storeManager,
        TableDataInterface $tableData
    ) {
        parent::__construct($productIndexerHelper, $resource, $config, $storeManager, $tableData);
    }
}
