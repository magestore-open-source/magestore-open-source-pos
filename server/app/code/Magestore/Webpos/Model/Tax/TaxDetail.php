<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Tax;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Tax\TaxDetailInterface;

/**
 * Model - TaxDetail
 */
class TaxDetail extends DataObject implements TaxDetailInterface
{
    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }
    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }
    /**
     * @inheritDoc
     */
    public function getCode()
    {
        return $this->getData(self::CODE);
    }
    /**
     * @inheritDoc
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }
    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }
    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }
    /**
     * @inheritDoc
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }
    /**
     * @inheritDoc
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }
    /**
     * @inheritDoc
     */
    public function getBaseRealAmount()
    {
        return $this->getData(self::BASE_REAL_AMOUNT);
    }
    /**
     * @inheritDoc
     */
    public function setBaseRealAmount($baseRealAmount)
    {
        return $this->setData(self::BASE_REAL_AMOUNT, $baseRealAmount);
    }
    /**
     * @inheritDoc
     */
    public function getBaseAmount()
    {
        return $this->getData(self::BASE_AMOUNT);
    }
    /**
     * @inheritDoc
     */
    public function setBaseAmount($baseAmount)
    {
        return $this->setData(self::BASE_AMOUNT, $baseAmount);
    }
    /**
     * @inheritDoc
     */
    public function getPercent()
    {
        return $this->getData(self::PERCENT);
    }
    /**
     * @inheritDoc
     */
    public function setPercent($percent)
    {
        return $this->setData(self::PERCENT, $percent);
    }
    /**
     * @inheritDoc
     */
    public function getProcess()
    {
        return $this->getData(self::PROCESS);
    }
    /**
     * @inheritDoc
     */
    public function setProcess($process)
    {
        return $this->setData(self::PROCESS, $process);
    }
}
