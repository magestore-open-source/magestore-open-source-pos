<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Tax;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Api\Data\TaxRateTitleInterface;
use Magento\Tax\Model\Calculation\RateRepository;
use Magento\Tax\Model\ResourceModel\Calculation\Rate\Collection;
use Magestore\Webpos\Api\Tax\TaxRateRepositoryInterface;

/**
 * Class TaxRateRepository
 *
 * Get tax rates
 */
class TaxRateRepository extends RateRepository implements TaxRateRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getAllTaxRates()
    {
        $objectManager = ObjectManager::getInstance();

        /* @var Collection $collection */
        $collection = $objectManager->create(Collection::class);
        $collection->setModel(TaxRate::class);
        /* @var StoreManagerInterface $StoreManager */
        $StoreManager = $objectManager->create(StoreManagerInterface::class);
        $collection->joinRegionTable();
        $collection->joinTitle();

        $taxRates = [];

        /* @var TaxRate $taxRateModel */
        foreach ($collection as $taxRateModel) {
            if ($taxRateModel->getTitle()) {
                /* @var TaxRateTitleInterface $taxTitle */
                $taxTitle = $objectManager->create(TaxRateTitleInterface::class);
                $taxTitle->setValue($taxRateModel->getTitle());
                $taxTitle->setStoreId($StoreManager->getStore()->getId());
                $taxRateModel->setTitles([$taxTitle]);
            } else {
                $taxRateModel->setTitles([]);
            }
            if (!$taxRateModel->getTaxPostcode()) {
                $taxRateModel->setTaxPostcode('*');
            }
            $taxRates[] = $taxRateModel;
        }

        return $taxRates;
    }
}
