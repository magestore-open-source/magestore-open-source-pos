<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Checkout\Data;

use Magestore\Webpos\Api\Data\Checkout\RuleInterface;

/**
 * Rule Data Model
 */
class Rule extends \Magento\SalesRule\Model\Data\Rule implements RuleInterface
{
    /**
     * @inheritdoc
     */
    public function getValidItemIds()
    {
        return $this->_get(self::VALID_ITEM_IDS);
    }
    /**
     * @inheritdoc
     */
    public function setValidItemIds($validItemIds)
    {
        return $this->setData(self::VALID_ITEM_IDS, $validItemIds);
    }
}
