<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Checkout;

use Exception;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Webapi\Rest\Request;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Log\Logger;
use Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder\Collection;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;

/**
 * Class PosOrder
 *
 * Used for model pos order
 */
class PosOrder extends AbstractModel
{
    const STATUS_PENDING = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_FAILED = 4;

    const PARAM_ORDER_POS_ID = 'new_order_pos_id';
    const PARAM_ORDER_LOCATION_ID = 'new_order_location_id';

    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * PosOrder constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param \Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder $resource
     * @param Collection $resourceCollection
     * @param Logger $logger
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        \Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder $resource,
        Collection $resourceCollection,
        Logger $logger,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->logger = $logger;
        $this->helper = $helper;
    }

    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init(\Magestore\Webpos\Model\ResourceModel\Checkout\PosOrder::class);
    }

    /**
     * Save request place order
     *
     * @param Request $request
     */
    public function saveRequest(Request $request)
    {
        $params = $request->getBodyParams();

        $existedRequest = clone $this;
        $existedRequest->load($params['order']['increment_id'], 'increment_id');
        if ($existedRequest->getId()) {
            return;
        }

        $data = [
            'status' => self::STATUS_PENDING,
            'increment_id' => $params['order']['increment_id'],
            'session_id' => $request->getParam(
                RequestProcessor::SESSION_PARAM_KEY
            ),
            'store_id' => $this->helper->getCurrentStoreView()->getId(),
            'location_id' => $params['order']['pos_location_id'],
            'pos_staff_id' => $params['order']['pos_staff_id'],
            'pos_id' => $params['order']['pos_id'],
            'order_created_time' => $params['order']['created_at'],
            'params' => json_encode($params),
        ];

        $this->setData($data);

        try {
            $this->save();
        } catch (Exception $e) {
            $this->logger->info($e->getTraceAsString());
        }
    }

    /**
     * @inheritdoc
     */
    public function afterCommitCallback()
    {
        parent::afterCommitCallback();

        if ($this->getId() && $this->getStatus() == self::STATUS_COMPLETED) {
            $this->_resource->processBackupOfWebposOrder($this->getIncrementId());
        }

        return $this;
    }
}
