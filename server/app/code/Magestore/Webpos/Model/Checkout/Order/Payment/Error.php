<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Checkout\Order\Payment;

use Exception;
use Magento\Framework\Model\AbstractModel;
use Magestore\Webpos\Model\Checkout\Order\Payment;

/**
 * Payment Error Model
 */
class Error extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'webpos_order_payment_error';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magestore\Webpos\Model\ResourceModel\Sales\Order\Payment\Error::class);
    }

    /**
     * Save order payment error
     *
     * @param Payment $paymentModel
     * @throws Exception
     */
    public function saveErrorLog(Payment $paymentModel)
    {
        $data = [
            'order_id' => $paymentModel->getOrderId(),
            'params' => json_encode($paymentModel->getData())
        ];

        $this->unsetData('id');
        $this->setData($data);
        $this->save();
    }
}
