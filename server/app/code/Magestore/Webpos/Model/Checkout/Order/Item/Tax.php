<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Checkout\Order\Item;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Checkout\Order\Item\TaxInterface;

/**
 * Order item tax model
 */
class Tax extends DataObject implements TaxInterface
{

    /**
     * @inheritdoc
     */
    public function getTaxItemId()
    {
        return $this->getData(self::TAX_ITEM_ID);
    }
    /**
     * @inheritdoc
     */
    public function setTaxItemId($taxItemId)
    {
        return $this->setData(self::TAX_ITEM_ID, $taxItemId);
    }

    /**
     * @inheritdoc
     */
    public function getTaxId()
    {
        return $this->getData(self::TAX_ID);
    }
    /**
     * @inheritdoc
     */
    public function setTaxId($taxId)
    {
        return $this->setData(self::TAX_ID, $taxId);
    }

    /**
     * @inheritdoc
     */
    public function getTaxCode()
    {
        return $this->getData(self::TAX_CODE);
    }
    /**
     * @inheritdoc
     */
    public function setTaxCode($taxCode)
    {
        return $this->setData(self::TAX_CODE, $taxCode);
    }

    /**
     * @inheritdoc
     */
    public function getItemId()
    {
        return $this->getData(self::ITEM_ID);
    }
    /**
     * @inheritdoc
     */
    public function setItemId($itemId)
    {
        return $this->setData(self::ITEM_ID, $itemId);
    }

    /**
     * @inheritdoc
     */
    public function getTaxPercent()
    {
        return $this->getData(self::TAX_PERCENT);
    }
    /**
     * @inheritdoc
     */
    public function setTaxPercent($taxPercent)
    {
        return $this->setData(self::TAX_PERCENT, $taxPercent);
    }

    /**
     * @inheritdoc
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }
    /**
     * @inheritdoc
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }

    /**
     * @inheritdoc
     */
    public function getBaseAmount()
    {
        return $this->getData(self::BASE_AMOUNT);
    }
    /**
     * @inheritdoc
     */
    public function setBaseAmount($baseAmount)
    {
        return $this->setData(self::BASE_AMOUNT, $baseAmount);
    }

    /**
     * @inheritdoc
     */
    public function getRealAmount()
    {
        return $this->getData(self::REAL_AMOUNT);
    }
    /**
     * @inheritdoc
     */
    public function setRealAmount($realAmount)
    {
        return $this->setData(self::REAL_AMOUNT, $realAmount);
    }

    /**
     * @inheritdoc
     */
    public function getRealBaseAmount()
    {
        return $this->getData(self::REAL_BASE_AMOUNT);
    }
    /**
     * @inheritdoc
     */
    public function setRealBaseAmount($realBaseAmount)
    {
        return $this->setData(self::REAL_BASE_AMOUNT, $realBaseAmount);
    }

    /**
     * @inheritdoc
     */
    public function getAssociatedItemId()
    {
        return $this->getData(self::ASSOCIATED_ITEM_ID);
    }
    /**
     * @inheritdoc
     */
    public function setAssociatedItemId($associatedItemId)
    {
        return $this->setData(self::ASSOCIATED_ITEM_ID, $associatedItemId);
    }

    /**
     * @inheritdoc
     */
    public function getTaxableItemType()
    {
        return $this->getData(self::TAXABLE_ITEM_TYPE);
    }
    /**
     * @inheritdoc
     */
    public function setTaxableItemType($taxableItemType)
    {
        return $this->setData(self::TAXABLE_ITEM_TYPE, $taxableItemType);
    }
}
