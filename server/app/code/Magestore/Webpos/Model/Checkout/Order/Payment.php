<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Checkout\Order;

use Magento\Framework\Model\AbstractModel;
use Magestore\Webpos\Api\Data\Checkout\Order\PaymentExtensionInterface;
use Magestore\Webpos\Api\Data\Checkout\Order\PaymentInterface;

/**
 * Pos Order Payment Model
 */
class Payment extends AbstractModel implements PaymentInterface
{

    /**
     * @var string
     */
    protected $_eventPrefix = 'webpos_order_payment';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magestore\Webpos\Model\ResourceModel\Sales\Order\Payment::class);
    }

    /**
     * @inheritdoc
     */
    public function getPaymentId()
    {
        return $this->getData(self::PAYMENT_ID);
    }
    /**
     * @inheritdoc
     */
    public function setPaymentId($paymentId)
    {
        return $this->setData(self::PAYMENT_ID, $paymentId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }
    /**
     * @inheritdoc
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @inheritdoc
     */
    public function getAmountPaid()
    {
        return $this->getData(self::AMOUNT_PAID);
    }
    /**
     * @inheritdoc
     */
    public function setAmountPaid($amountPaid)
    {
        return $this->setData(self::AMOUNT_PAID, round($amountPaid, 4));
    }

    /**
     * @inheritdoc
     */
    public function getBaseAmountPaid()
    {
        return $this->getData(self::BASE_AMOUNT_PAID);
    }
    /**
     * @inheritdoc
     */
    public function setBaseAmountPaid($baseAmountPaid)
    {
        return $this->setData(self::BASE_AMOUNT_PAID, round($baseAmountPaid, 4));
    }

    /**
     * @inheritdoc
     */
    public function getAmountChange()
    {
        return $this->getData(self::AMOUNT_CHANGE);
    }

    /**
     * @inheritdoc
     */
    public function setAmountChange($amountChange)
    {
        return $this->setData(self::AMOUNT_CHANGE, round($amountChange, 4));
    }

    /**
     * @inheritdoc
     */
    public function getBaseAmountChange()
    {
        return $this->getData(self::BASE_AMOUNT_CHANGE);
    }

    /**
     * @inheritdoc
     */
    public function setBaseAmountChange($baseAmountChange)
    {
        return $this->setData(self::BASE_AMOUNT_CHANGE, round($baseAmountChange, 4));
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return $this->getData(self::METHOD);
    }
    /**
     * @inheritdoc
     */
    public function setMethod($method)
    {
        return $this->setData(self::METHOD, $method);
    }

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }
    /**
     * @inheritdoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritdoc
     */
    public function getTransactionId()
    {
        return $this->getData(self::TRANSACTION_ID);
    }
    /**
     * @inheritdoc
     */
    public function setTransactionId($transactionId)
    {
        return $this->setData(self::TRANSACTION_ID, $transactionId);
    }

    /**
     * @inheritdoc
     */
    public function getInvoiceId()
    {
        return $this->getData(self::INVOICE_ID);
    }
    /**
     * @inheritdoc
     */
    public function setInvoiceId($invoiceId)
    {
        return $this->setData(self::INVOICE_ID, $invoiceId);
    }

    /**
     * @inheritdoc
     */
    public function getReferenceNumber()
    {
        return $this->getData(self::REFERENCE_NUMBER);
    }

    /**
     * @inheritdoc
     */
    public function setReferenceNumber($referenceNumber)
    {
        return $this->setData(self::REFERENCE_NUMBER, $referenceNumber);
    }

    /**
     * @inheritdoc
     */
    public function getIsPaid()
    {
        return $this->getData(self::IS_PAID);
    }
    /**
     * @inheritdoc
     */
    public function setIsPaid($isPaid)
    {
        return $this->setData(self::IS_PAID, $isPaid);
    }
    /**
     * @inheritdoc
     */
    public function getCardType()
    {
        return $this->getData(self::CARD_TYPE);
    }
    /**
     * @inheritdoc
     */
    public function setCardType($cardType)
    {
        return $this->setData(self::CARD_TYPE, $cardType);
    }

    /**
     * @inheritdoc
     */
    public function getPaymentDate()
    {
        return $this->getData(self::PAYMENT_DATE);
    }
    /**
     * @inheritdoc
     */
    public function setPaymentDate($paymentDate)
    {
        return $this->setData(self::PAYMENT_DATE, $paymentDate);
    }

    /**
     * @inheritdoc
     */
    public function getIsPayLater()
    {
        return $this->getData(self::IS_PAY_LATER);
    }
    /**
     * @inheritdoc
     */
    public function setIsPayLater($isPayLater)
    {
        return $this->setData(self::IS_PAY_LATER, $isPayLater);
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }
    /**
     * @inheritdoc
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * @inheritDoc
     */
    public function setReceipt($receipt)
    {
        return $this->setData(self::RECEIPT, $receipt);
    }

    /**
     * @inheritDoc
     */
    public function getReceipt()
    {
        return $this->getData(self::RECEIPT);
    }

    /**
     * @inheritDoc
     */
    public function setIncrementId($increment_id)
    {
        return $this->setData(self::INCREMENT_ID, $increment_id);
    }

    /**
     * @inheritDoc
     */
    public function getIncrementId()
    {
        return $this->getData(self::INCREMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setParentIncrementId($parent_increment_id)
    {
        return $this->setData(self::PARENT_INCREMENT_ID, $parent_increment_id);
    }

    /**
     * @inheritDoc
     */
    public function getParentIncrementId()
    {
        return $this->getData(self::PARENT_INCREMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes()
    {
        return $this->getData(self::EXTENSION_ATTRIBUTES_KEY);
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(PaymentExtensionInterface $extensionAttributes)
    {
        return $this->setData(self::EXTENSION_ATTRIBUTES_KEY, $extensionAttributes);
    }
}
