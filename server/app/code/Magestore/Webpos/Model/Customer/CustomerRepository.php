<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Customer;

use Exception;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Model\Config\Share;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Model\Data\CustomerSecureFactory;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Customer\Model\Customer\NotificationStorage;
use Magento\Customer\Model\ResourceModel\AddressRepository;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Customer\Model\ResourceModel\Grid\CollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ImageProcessorInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\ServiceOutputProcessor;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Api\Customer\CustomerRepositoryInterface;
use Magestore\Webpos\Api\Data\Customer\CustomerInterface;
use Magestore\Webpos\Api\Data\Customer\CustomerSearchResultsInterfaceFactory;
use Magestore\Webpos\Api\SyncInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Model\Cache\Type;
use Psr\Log\LoggerInterface;
use UnexpectedValueException;

/**
 * Class CustomerRepository
 *
 * Customer repository to get information of customers
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class CustomerRepository implements CustomerRepositoryInterface
{
    /**
     * @var EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerSecureFactory
     */
    protected $customerSecureFactory;

    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var AddressRepository
     */
    protected $addressRepository;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $customerResourceModel;

    /**
     * @var CustomerMetadataInterface
     */
    protected $customerMetadata;

    /**
     * @var \Magento\Customer\Api\Data\CustomerSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var ImageProcessorInterface
     */
    protected $imageProcessor;

    /**
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var NotificationStorage
     */
    private $notificationStorage;

    /**
     * @var CollectionFactory
     */
    protected $gridCollection;

    /**
     * @var SubscriberFactory
     */
    protected $subscriberFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var GridFactory
     */
    protected $customerGridFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var Share
     */
    protected $customerShareConfig;

    /**
     * CustomerRepository constructor.
     *
     * @param CustomerFactory $customerFactory
     * @param CustomerSecureFactory $customerSecureFactory
     * @param CustomerRegistry $customerRegistry
     * @param AddressRepository $addressRepository
     * @param \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel
     * @param CustomerMetadataInterface $customerMetadata
     * @param CustomerSearchResultsInterfaceFactory $searchResultsFactory
     * @param ManagerInterface $eventManager
     * @param StoreManagerInterface $storeManager
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param DataObjectHelper $dataObjectHelper
     * @param ImageProcessorInterface $imageProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param NotificationStorage $notificationStorage
     * @param CollectionFactory $gridCollection
     * @param SubscriberFactory $subscriberFactory
     * @param GridFactory $customerGridFactory
     * @param LoggerInterface $logger
     * @param Data $helper
     * @param ObjectManagerInterface $objectManager
     * @param Share $customerShareConfig
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(
        CustomerFactory $customerFactory,
        CustomerSecureFactory $customerSecureFactory,
        CustomerRegistry $customerRegistry,
        AddressRepository $addressRepository,
        \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel,
        CustomerMetadataInterface $customerMetadata,
        CustomerSearchResultsInterfaceFactory $searchResultsFactory,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        DataObjectHelper $dataObjectHelper,
        ImageProcessorInterface $imageProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        NotificationStorage $notificationStorage,
        CollectionFactory $gridCollection,
        SubscriberFactory $subscriberFactory,
        GridFactory $customerGridFactory,
        LoggerInterface $logger,
        Data $helper,
        ObjectManagerInterface $objectManager,
        Share $customerShareConfig
    ) {
        $this->customerFactory = $customerFactory;
        $this->customerSecureFactory = $customerSecureFactory;
        $this->customerRegistry = $customerRegistry;
        $this->addressRepository = $addressRepository;
        $this->customerResourceModel = $customerResourceModel;
        $this->customerMetadata = $customerMetadata;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->eventManager = $eventManager;
        $this->storeManager = $storeManager;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->imageProcessor = $imageProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->notificationStorage = $notificationStorage;
        $this->gridCollection = $gridCollection;
        $this->subscriberFactory = $subscriberFactory;
        $this->logger = $logger;
        $this->customerGridFactory = $customerGridFactory;
        $this->helper = $helper;
        $this->objectManager = $objectManager;
        $this->customerShareConfig = $customerShareConfig;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function save(CustomerInterface $customer, $passwordHash = null)
    {
        // phpcs:disable
        $delegatedClassName = "Magento\Customer\Model\Delegation\Storage";
        if (class_exists($delegatedClassName)) {
            $delegatedStorage = $this->objectManager->create($delegatedClassName);
        } else {
            $delegatedStorage = false;
        }
        if ($delegatedStorage) {
            $delegatedNewOperation = !$customer->getId()
                ? $delegatedStorage->consumeNewOperation() : null;
        }

        $prevCustomerData = null;
        $prevCustomerDataArr = null;
        $isExistingCustomer = false;

        if ($customer->getId()) {
            $isExistingCustomer = true;
            $prevCustomerData = $this->getById($customer->getId());
            $prevCustomerDataArr = $prevCustomerData->__toArray();
        }
        /** @var $customer \Magestore\Webpos\Model\Customer\Data\Customer */
        $customerArr = $customer->__toArray();

        $customer = $this->imageProcessor->save(
            $customer,
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            $prevCustomerData
        );

        $origAddresses = $customer->getAddresses();
        $customer->setAddresses([]);
        $customerData = $this->extensibleDataObjectConverter->toNestedArray(
            $customer,
            [],
            CustomerInterface::class
        );
        $customer->setAddresses($origAddresses);
        $customerModel = $this->customerFactory->create(['data' => $customerData]);
        $customerModel->setData('customer_telephone', $customer->getTelephone());
        $storeId = $customerModel->getStoreId();
        if ($storeId === null) {
            $customerModel->setStoreId($this->storeManager->getStore()->getId());
        }
        $customerModel->setId($customer->getId());

        // Need to use attribute set or future updates can cause data loss
        if (!$customerModel->getAttributeSetId()) {
            $customerModel->setAttributeSetId(
                CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER
            );
        }
        $this->populateCustomerWithSecureData($customerModel, $passwordHash);

        // If customer email was changed, reset RpToken info
        if ($prevCustomerData
            && $prevCustomerData->getEmail() !== $customerModel->getEmail()
        ) {
            $customerModel->setRpToken(null);
            $customerModel->setRpTokenCreatedAt(null);
        }
        if (!array_key_exists('default_billing', $customerArr) &&
            null !== $prevCustomerDataArr &&
            array_key_exists('default_billing', $prevCustomerDataArr)
        ) {
            $customerModel->setDefaultBilling($prevCustomerDataArr['default_billing']);
        }

        if (!array_key_exists('default_shipping', $customerArr) &&
            null !== $prevCustomerDataArr &&
            array_key_exists('default_shipping', $prevCustomerDataArr)
        ) {
            $customerModel->setDefaultShipping($prevCustomerDataArr['default_shipping']);
        }

        try {
            if ($isExistingCustomer) {
                $this->saveAddress($customer->getId(), $customer);
                $customerModel->setDefaultShipping($customer->getDefaultShipping());
                $customerModel->setDefaultBilling($customer->getDefaultBilling());
            }
            $customerModel->save();
            $customerModel->getResource()->addCommitCallback([$customerModel, 'reindex']);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        $this->customerRegistry->push($customerModel);
        $customerId = $customerModel->getId();

        if (!$isExistingCustomer) {
            $this->saveAddress($customerId, $customer);
        }

        $this->customerRegistry->remove($customerId);

        $isSubscriber = $customer->getSubscriberStatus();
        $subscriberModel = $this->subscriberFactory->create()->loadByCustomerId($customerId);
        if ($isSubscriber) {
            $this->addSubscriber($customerId);
        } else {
            if ($subscriberModel->getId() && $subscriberModel->getData('subscriber_status')) {
                try {
                    $subscriberModel->unsubscribe();
                } catch (Exception $e) {
                    $this->logger->critical($e->getMessage());
                }
            }
        }

        /* After save customer and before return the result of API */
        $this->eventManager->dispatch(
            'pos_customer_after_save',
            [
                'customer_saved' => $customerModel,
                'customer_request' => $customer
            ]
        );

        $savedCustomer = $this->get($customer->getEmail(), $customer->getWebsiteId());

        $customerGridModel = $this->customerGridFactory->create()->load($savedCustomer->getId());
        // Update billing_telephone in customer grid if customer doesn't has billing address
        if (!$customerModel->getDefaultBilling()
            && $customerModel->getTelephone() !== $customerGridModel->getData('billing_telephone')
        ) {
            $customerGridModel->setData('billing_telephone', $customerModel->getTelephone());
            $customerGridModel->save();
        }
        $savedCustomer->setData('telephone', $customerGridModel->getData('billing_telephone'));
        $savedCustomer->setData('full_name', $customerGridModel->getData('name'));
        $eventData = ['customer_data_object' => $savedCustomer, 'orig_customer_data_object' => $prevCustomerData];
        if ($delegatedStorage) {
            $eventData['delegate_data'] = $delegatedNewOperation ? $delegatedNewOperation->getAdditionalData() : [];
        }
        $this->eventManager->dispatch('customer_save_after_data_object', $eventData);
        if(!$isExistingCustomer) $this->sendEmailConfirmation($savedCustomer);

        return $savedCustomer;
    }

    /**
     * Send either confirmation or welcome email after an account creation
     *
     * @param CustomerInterface $customer
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function sendEmailConfirmation(CustomerInterface $customer)
    {
        try {
            $templateType = EmailNotificationInterface::NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD;
            $this->getEmailNotification()->newAccount($customer, $templateType, null, $customer->getStoreId());
            $customer->setConfirmation(null);
        } catch (MailException $e) {
            // If we are not able to send a new account email, this should be ignored
            $this->logger->critical($e);
        } catch (UnexpectedValueException $e) {
            $this->logger->error($e);
        }
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated 100.1.0
     */
    private function getEmailNotification()
    {
        if (!($this->emailNotification instanceof EmailNotificationInterface)) {
            return ObjectManager::getInstance()->get(
                EmailNotificationInterface::class
            );
        } else {
            return $this->emailNotification;
        }
    }


    /**
     * Save address
     *
     * @param $customerId
     * @param CustomerInterface $customer
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function saveAddress($customerId, CustomerInterface $customer)
    {
        if ($customer->getAddresses() !== null) {
            if ($customer->getId()) {
                $existingAddresses = $this->getById($customer->getId())->getAddresses();
                $getIdFunc = function ($address) {
                    return $address->getId();
                };
                $existingAddressIds = array_map($getIdFunc, $existingAddresses);
            } else {
                $existingAddressIds = [];
            }

            $savedAddressIds = [];
            foreach ($customer->getAddresses() as $address) {
                $address->setCustomerId($customerId)
                    ->setRegion($address->getRegion());
                $this->addressRepository->save($address);
                if ($address->getId()) {
                    $savedAddressIds[] = $address->getId();
                    if ($address->isDefaultBilling()) {
                        $customer->setDefaultBilling($address->getId());
                    }
                    if ($address->isDefaultShipping()) {
                        $customer->setDefaultShipping($address->getId());
                    }
                }
            }

            $addressIdsToDelete = array_diff($existingAddressIds, $savedAddressIds);
            foreach ($addressIdsToDelete as $addressId) {
                $this->addressRepository->deleteById($addressId);
            }
        }
    }

    /**
     * Add customer subscriber
     *
     * @param int $customerId
     */
    public function addSubscriber($customerId)
    {
        if ($customerId) {
            $subscriberModel = $this->subscriberFactory->create()->loadByCustomerId($customerId);
            if ($subscriberModel->getId() === NULL) {
                try {
                    $this->subscriberFactory->create()->subscribeCustomerById($customerId);
                } catch (LocalizedException $e) {
                    $this->logger->critical($e->getMessage());
                } catch (Exception $e) {
                    $this->logger->critical($e->getMessage());
                }

            } elseif ($subscriberModel->getData('subscriber_status') != 1) {
                $subscriberModel->setData('subscriber_status', 1);
                try {
                    $subscriberModel->save();
                } catch (Exception $e) {
                    $this->logger->critical($e);
                }
            }
        }
    }


    /**
     * Set secure data to customer model
     *
     * @param Customer $customerModel
     * @param string|null $passwordHash
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @return void
     */
    private function populateCustomerWithSecureData($customerModel, $passwordHash = null)
    {
        if ($customerModel->getId()) {
            $customerSecure = $this->customerRegistry->retrieveSecureData($customerModel->getId());

            $customerModel->setRpToken($passwordHash ? null : $customerSecure->getRpToken());
            $customerModel->setRpTokenCreatedAt($passwordHash ? null : $customerSecure->getRpTokenCreatedAt());
            $customerModel->setPasswordHash($passwordHash ?: $customerSecure->getPasswordHash());

            $customerModel->setFailuresNum($customerSecure->getFailuresNum());
            $customerModel->setFirstFailure($customerSecure->getFirstFailure());
            $customerModel->setLockExpires($customerSecure->getLockExpires());
        } elseif ($passwordHash) {
            $customerModel->setPasswordHash($passwordHash);
        }

        if ($passwordHash && $customerModel->getId()) {
            $this->customerRegistry->remove($customerModel->getId());
        }
    }

    /**
     * @inheritdoc
     */
    public function get($email, $websiteId = null)
    {
        $customerModel = $this->customerRegistry->retrieveByEmail($email, $websiteId);
        $customerData = $customerModel->getDataModel();
        $checkSubscriber = $this->subscriberFactory->create()->loadByCustomerId($customerModel->getId());

        if ($checkSubscriber->isSubscribed()) {
            $customerData->setSubscriberStatus(1);
        } else {
            $customerData->setSubscriberStatus(0);
        }
        return $customerData;
    }

    /**
     * @inheritdoc
     */
    public function getById($customerId)
    {
        $customerModel = $this->customerRegistry->retrieve($customerId);
        return $customerModel->getDataModel();
    }

    /**
     * Prepare and get customer collection
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @param bool $customerFilter
     * @return Collection|mixed
     * @throws InputException
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getCustomerCollection($searchCriteria, $customerFilter = false)
    {
        /** @var Collection $collection */
        $collection = $this->customerFactory->create()->getCollection();

        if ($this->customerShareConfig->isWebsiteScope()) {
            $collection->addFieldToFilter('website_id', $this->helper->getCurrentStoreView()->getWebsiteId());
        }

        //Add filters from root filter group to the collection
        if (!$customerFilter) {
            foreach ($searchCriteria->getFilterGroups() as $group) {
                $this->addFilterGroupToCollection($group, $collection);
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            CustomerInterface::class
        );
        // This is needed to make sure all the attributes are properly loaded
        foreach ($this->customerMetadata->getAllAttributesMetadata() as $metadata) {
            $collection->addAttributeToSelect($metadata->getAttributeCode());
        }
        // Needed to enable filtering on name as a whole
        $collection->addNameToSelect();
        // Needed to enable filtering based on billing address attributes
        $collection->getSelect()
            ->joinLeft(
                ['customer_grid' => $collection->getTable('customer_grid_flat')],
                'e.entity_id = customer_grid.entity_id',
                ['customer_grid.name', 'customer_grid.billing_telephone']
            )
            ->joinLeft(
                ['ns' => $collection->getTable('newsletter_subscriber')],
                'e.entity_id = ns.customer_id',
                ['ns.subscriber_status']
            )
            ->columns('IFNULL(customer_grid.billing_telephone,"") AS telephone')
            ->columns('customer_grid.name AS full_name')
            ->columns('IF(ns.subscriber_status = 1, "1", "0") AS subscriber_status');

        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                if ($sortOrder->getField() == 'full_name') {
                    $collection->getSelect()->order('customer_grid.name');
                }
                if ($sortOrder->getField() == 'telephone') {
                    $collection->getSelect()->order('customer_grid.billing_telephone');
                }
                if ($sortOrder->getField() == 'subscriber_status') {
                    $collection->getSelect()->order('ns.subscriber_status');
                }
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        return $collection;
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = $this->objectManager;

        /** @var StateInterface $cacheState */
        $cacheState = $objectManager->get(StateInterface::class);
        if (!$cacheState->isEnabled(Type::TYPE_IDENTIFIER)
            || count($searchCriteria->getFilterGroups())
            || $searchCriteria->getCurrentPage() < 2 // Do not cache for first page
        ) {
            return $this->processGetList($searchCriteria);
        }

        /** @var CacheInterface $cache */
        $cache = $objectManager->get(CacheInterface::class);

        $key = 'syncCustomers-'
            . $searchCriteria->getPageSize()
            . '-' . $searchCriteria->getCurrentPage()
            . '-' . $this->helper->getCurrentStoreView()->getWebsiteId();

        if ($response = $cache->load($key)) {
            // Reponse from cache
            return json_decode($response, true);
        }

        $flag = SyncInterface::QUEUE_NAME;
        if ($cachedAt = $cache->load($flag)) {
            return [
                'async_stage' => 2, // processing
                'cached_at' => $cachedAt, // process started time
            ];
        }

        // Block metaphor
        $cachedAt = date("Y-m-d H:i:s");
        $cache->save($cachedAt, $flag, [Type::CACHE_TAG], 300);

        /** @var ServiceOutputProcessor $processor */
        $processor = $objectManager->get(ServiceOutputProcessor::class);
        $outputData = $processor->process(
            $this->processGetList($searchCriteria),
            CustomerRepositoryInterface::class,
            'getList'
        );
        $outputData['cached_at'] = $cachedAt;
        $cache->save(
            json_encode($outputData),
            $key,
            [
                Type::CACHE_TAG,
            ],
            86400   // Cache lifetime: 1 day
        );
        // Release metaphor
        $cache->remove($flag);
        return $outputData;
    }

    /**
     * Process collection
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     * @throws InputException
     * @throws LocalizedException
     */
    public function processGetList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        /** @var Collection $collection */
        $collection = $this->getCustomerCollection($searchCriteria);
        $searchResults->setTotalCount($collection->getSize());

        $customers = [];
        /** @var Customer $customerModel */
        foreach ($collection as $customerModel) {
            $customers[] = $customerModel->getDataModel();
        }
        $searchResults->setItems($customers);
        return $searchResults;
    }

    /**
     * @inheritdoc
     */
    public function search(\Magestore\Webpos\Api\SearchCriteriaInterface $searchCriteria)
    {
        $queryString = $searchCriteria->getQueryString();
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        $collection = $this->gridCollection->create()->addFieldToFilter(
            ['main_table.name', 'main_table.email', 'main_table.billing_telephone'],
            [
                ['like' => '%' . $queryString . '%'],
                ['like' => '%' . $queryString . '%'],
                ['like' => '%' . $queryString . '%']
            ]
        );

        if ($this->customerShareConfig->isWebsiteScope()) {
            $collection->addFieldToFilter('website_id', $this->helper->getCurrentStoreView()->getWebsiteId());
        }

        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                if ($sortOrder->getField() == 'full_name') {
                    $sortOrder->setField('name');
                }
                if ($sortOrder->getField() == 'telephone') {
                    $sortOrder->setField('billing_telephone');
                }
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $searchResults->setTotalCount($collection->getSize());

        $customers = [];
        foreach ($collection as $customerModel) {
            $customerInfoModel = $this->get($customerModel->getEmail(), $customerModel->getWebsiteId());
            $customerInfoModel->setData('telephone', $customerModel->getData('billing_telephone'));
            $customerInfoModel->setData('full_name', $customerModel->getData('name'));
            $customers[] = $customerInfoModel;
        }
        $searchResults->setItems($customers);
        return $searchResults;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return void
     * @throws InputException
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        Collection $collection
    ) {
        $fields = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = ['attribute' => $filter->getField(), $condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields);
        }
    }
}
