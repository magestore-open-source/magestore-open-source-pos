<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Customer\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory;

/**
 * Class location attribute source
 */
class Location extends AbstractSource
{
    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var CollectionFactory
     */
    protected $_webposLocation;

    /**
     * Constructor for location attribute source
     *
     * @param CollectionFactory $webposLocation
     */
    public function __construct(
        CollectionFactory $webposLocation
    ) {
        $this->_webposLocation = $webposLocation;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    /**
     * @inheritDoc
     */
    public function getAllOptions()
    {
        $collection = $this->_webposLocation->create();

        if ($collection->getSize()) {
            foreach ($collection->getItems() as $location) {
                $this->options[] = ['value' => $location->getId(), 'label' => $location->getName()];
            }
        }

        return $this->options;
    }
}
