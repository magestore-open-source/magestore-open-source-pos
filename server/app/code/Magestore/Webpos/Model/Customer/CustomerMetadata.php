<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Customer;

use Magento\Customer\Model\Attribute;
use Magento\Customer\Model\AttributeMetadataDataProvider;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Model - CustomerMetadata
 */
class CustomerMetadata extends \Magento\Customer\Model\Metadata\CustomerMetadata
{
    const IGNORE_ATTRIBUTE = [
        'created_in',
        'created_at',
        'website_id'
    ];
    /**
     * @inheritDoc
     */
    public function getAttributes($formCode)
    {
        $object = ObjectManager::getInstance();
        $attributes = [];
        $attributesFormCollection = $object->get(AttributeMetadataDataProvider::class)->loadAttributesCollection(
            self::ENTITY_TYPE_CUSTOMER,
            $formCode
        );
        foreach ($attributesFormCollection as $attribute) {
            if (!in_array($attribute->getAttributeCode(), self::IGNORE_ATTRIBUTE)) {
                /** @var $attribute Attribute */
                $attributes[$attribute->getAttributeCode()] = $object->get(AttributeMetadataConverter::class)
                    ->createMetadataAttribute($attribute);
            }
        }
        if (empty($attributes)) {
            throw NoSuchEntityException::singleField('formCode', $formCode);
        }
        return $attributes;
    }
}
