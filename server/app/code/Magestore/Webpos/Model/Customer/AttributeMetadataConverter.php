<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Customer;

use Magento\Customer\Api\Data\AttributeMetadataInterface;
use Magento\Customer\Api\Data\OptionInterface;
use Magento\Customer\Api\Data\ValidationRuleInterface;
use Magento\Customer\Model\Attribute;
use Magento\Eav\Api\Data\AttributeDefaultValueInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;

/**
 * Model - AttributeMetadataConverter
 */
class AttributeMetadataConverter extends \Magento\Customer\Model\AttributeMetadataConverter
{
    const IGNORE_OPTION_FROM_ATTRIBUTE = [
        'country_id',
        'region'
    ];

    /**
     * Create Metadata Attribute
     *
     * @param Attribute $attribute
     * @return AttributeMetadataInterface
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function createMetadataAttribute($attribute)
    {
        $object = ObjectManager::getInstance();
        $options = [];
        if ($attribute->usesSource() && !in_array($attribute->getAttributeCode(), self::IGNORE_OPTION_FROM_ATTRIBUTE)) {
            foreach ($attribute->getSource()->getAllOptions() as $option) {
                $optionDataObject = $object->create(OptionInterface::class);
                if (!is_array($option['value'])) {
                    $optionDataObject->setValue($option['value']);
                } else {
                    $optionArray = [];
                    foreach ($option['value'] as $optionArrayValues) {
                        $optionObject = $object->create(OptionInterface::class);
                        $this->dataObjectHelper->populateWithArray(
                            $optionObject,
                            $optionArrayValues,
                            OptionInterface::class
                        );
                        $optionArray[] = $optionObject;
                    }
                    $optionDataObject->setOptions($optionArray);
                }
                $optionDataObject->setLabel($option['label']);
                $options[] = $optionDataObject;
            }
        }
        $validationRules = [];
        foreach ((array)$attribute->getValidateRules() as $name => $value) {
            $validationRule = $object->create(ValidationRuleInterface::class)
                ->setName($name)
                ->setValue($value);
            $validationRules[] = $validationRule;
        }

        $attributeMetaData = $object->create(AttributeMetadataInterface::class);

        if ($attributeMetaData instanceof AttributeDefaultValueInterface) {
            $attributeMetaData->setDefaultValue($attribute->getDefaultValue());
        }

        $attributeData = $attributeMetaData->setAttributeCode($attribute->getAttributeCode())
            ->setFrontendInput($attribute->getFrontendInput())
            ->setInputFilter((string)$attribute->getInputFilter())
            ->setStoreLabel($attribute->getStoreLabel())
            ->setValidationRules($validationRules)
            ->setIsVisible((boolean)$attribute->getIsVisible())
            ->setIsRequired((boolean)$attribute->getIsRequired())
            ->setMultilineCount((int)$attribute->getMultilineCount())
            ->setDataModel((string)$attribute->getDataModel())
            ->setOptions($options)
            ->setFrontendClass($attribute->getFrontend()->getClass())
            ->setFrontendLabel($attribute->getFrontendLabel())
            ->setNote((string)$attribute->getNote())
            ->setIsSystem((boolean)$attribute->getIsSystem())
            ->setIsUserDefined((boolean)$attribute->getIsUserDefined())
            ->setBackendType($attribute->getBackendType())
            ->setSortOrder((int)$attribute->getSortOrder())
            ->setIsUsedInGrid($attribute->getIsUsedInGrid())
            ->setIsVisibleInGrid($attribute->getIsVisibleInGrid())
            ->setIsFilterableInGrid($attribute->getIsFilterableInGrid())
            ->setIsSearchableInGrid($attribute->getIsSearchableInGrid());

        if ($attribute->getAttributeCode() == 'prefix') {
            $optionPrefix = $object->get(ScopeConfigInterface::class)
                ->getValue('customer/address/prefix_options');
            if ($optionPrefix) {
                $optionArray = explode(';', $optionPrefix);
                foreach ($optionArray as $option) {
                    $options[] = [
                        'value' => $option,
                        'label' => $option
                    ];
                }
                $attributeData->setFrontendInput('select');
            }
            $attributeData->setOptions($options);
        }
        if ($attribute->getAttributeCode() == 'suffix') {
            $optionSuffix = $object->get(ScopeConfigInterface::class)
                ->getValue('customer/address/suffix_options');

            if ($optionSuffix) {
                $optionArray = explode(';', $optionSuffix);
                foreach ($optionArray as $option) {
                    $options[] = [
                        'value' => $option,
                        'label' => $option
                    ];
                }
                $attributeData->setFrontendInput('select');
            }
            $attributeData->setOptions($options);
        }

        return $attributeData;
    }
}
