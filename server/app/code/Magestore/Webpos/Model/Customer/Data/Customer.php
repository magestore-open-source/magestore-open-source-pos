<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Customer\Data;

use Magestore\Webpos\Api\Data\Customer\CustomerInterface;

/**
 * Magestore webpos customer class
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Customer extends \Magento\Customer\Model\Data\Customer implements
    CustomerInterface
{
    /**
     * Get customer billing telephone
     *
     * @return string|null
     */
    public function getTelephone()
    {
        return $this->_get(self::TELEPHONE);
    }

    /**
     * Set customer billing telephone
     *
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        return $this->setData(self::TELEPHONE, $telephone);
    }

    /**
     * Get subscriber
     *
     * @return int|null
     */
    public function getSubscriberStatus()
    {
        return $this->_get(self::SUBSCRIBER_STATUS);
    }

    /**
     * Set subscriber
     *
     * @param int $subscriberStatus
     * @return $this
     */
    public function setSubscriberStatus($subscriberStatus)
    {
        return $this->setData(self::SUBSCRIBER_STATUS, $subscriberStatus);
    }

    /**
     * Get full name
     *
     * @return string|null
     */
    public function getFullName()
    {
        return $this->_get(self::FULL_NAME);
    }

    /**
     * Set full name
     *
     * @param string $fullName
     * @return $this
     */
    public function setFullName($fullName)
    {
        return $this->setData(self::FULL_NAME, $fullName);
    }

    /**
     * Get search string
     *
     * @api
     * @return string|null
     */
    public function getSearchString()
    {
        $searchString = $this->_get(self::EMAIL)
            . ' ' . $this->getFirstname()
            . ' ' . $this->getLastname()
            . ' ' . $this->_get(self::TELEPHONE);
        return $searchString;
    }

    /**
     * Set customer is creating
     *
     * @param string $isCreating
     * @return CustomerInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setIsCreating($isCreating)
    {
        return $this;
    }

    /**
     * Get customer is creating
     *
     * @return boolean
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsCreating()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getTmpCustomerId()
    {
        return $this->_get(self::TMP_CUSTOMER_ID);
    }

    /**
     * @inheritdoc
     */
    public function setTmpCustomerId($tmpCustomerId)
    {
        return $this->setData(self::TMP_CUSTOMER_ID, $tmpCustomerId);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedLocationId()
    {
        return $this->_get(self::CREATED_LOCATION_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedLocationId($createdLocationId)
    {
        return $this->setData(self::CREATED_LOCATION_ID, $createdLocationId);
    }
}
