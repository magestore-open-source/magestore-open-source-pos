<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Request\ActionLog;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magestore\Webpos\Api\Checkout\CheckoutRepositoryInterface;
use Magestore\Webpos\Api\Sales\Order\CreditmemoRepositoryInterface;
use Magestore\Webpos\Api\Sales\OrderRepositoryInterface;
use Magestore\Webpos\Model\Request\Actions\CancelAction;
use Magestore\Webpos\Model\Request\Actions\RefundAction;
use Magestore\Webpos\Model\Request\Actions\TakePaymentAction;
use Magestore\Webpos\Model\ResourceModel\Request\ActionLog;
use Psr\Log\LoggerInterface;

/**
 * Action Log - Collection
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    /**
     * @var CreditmemoRepositoryInterface
     */
    protected $creditmemoRepository;

    /**
     * @var CheckoutRepositoryInterface
     */
    protected $checkoutRepository;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * Collection constructor.
     *
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param CreditmemoRepositoryInterface $creditmemoRepository
     * @param CheckoutRepositoryInterface $checkoutRepository
     * @param OrderRepositoryInterface $orderRepository
     * @param AdapterInterface|null $connection
     * @param AbstractDb|null $resource
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        CreditmemoRepositoryInterface $creditmemoRepository,
        CheckoutRepositoryInterface $checkoutRepository,
        OrderRepositoryInterface $orderRepository,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->creditmemoRepository = $creditmemoRepository;
        $this->checkoutRepository = $checkoutRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Initialize collection resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Magestore\Webpos\Model\Request\ActionLog::class,
            ActionLog::class
        );
    }

    /**
     * Process action log
     */
    public function processActionLog()
    {
        foreach ($this->getItems() as $actionLog) {
            switch ($actionLog->getActionType()) {
                case TakePaymentAction::ACTION_TYPE:
                    $this->checkoutRepository->processTakePaymentActionLog($actionLog->getRequestIncrementId());
                    break;
                case RefundAction::ACTION_TYPE:
                    $this->creditmemoRepository->processCreditmemoRequest($actionLog->getRequestIncrementId());
                    break;
                case CancelAction::ACTION_TYPE:
                    $this->orderRepository->processCancelOrderActionLog($actionLog->getRequestIncrementId());
                    break;
                default:
                    break;
            }
        }
    }
}
