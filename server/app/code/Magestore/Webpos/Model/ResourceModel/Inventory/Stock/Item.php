<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Inventory\Stock;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type as TypeBundle;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Model\Config;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Select;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\GroupedProduct\Model\ResourceModel\Product\Link;
use Magento\InventoryIndexer\Indexer\InventoryIndexer;
use Magento\InventoryIndexer\Indexer\Stock\GetAllStockIds;
use Magento\InventoryIndexer\Model\StockIndexTableNameResolverInterface;
use Magento\InventoryMultiDimensionalIndexerApi\Model\Alias;
use Magento\InventoryMultiDimensionalIndexerApi\Model\IndexNameBuilder;
use Magento\InventoryMultiDimensionalIndexerApi\Model\IndexNameResolverInterface;
use Magento\InventorySalesAdminUi\Model\ResourceModel\GetAssignedStockIdsBySku;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magento\Framework\Stdlib\DateTime\DateTime as DateTimeLib;
use Zend_Db_Expr;

/**
 * Stock item
 *
 * Class \Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Item extends \Magento\CatalogInventory\Model\ResourceModel\Stock\Item
{
    /**
     * @var StockConfigurationInterface
     */
    private $stockConfiguration;

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var Config
     */
    protected $eavConfig;

    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * @var DateTimeLib
     */
    private $dateTime;

    /**
     * @var GetAssignedStockIdsBySku
     */
    private $getAssignedStockIdsBySku;

    /**
     * @var GetAllStockIds
     */
    private $getAllStockIds;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * Construct to add new di
     */
    public function _construct()
    {
        $this->objectManager = ObjectManager::getInstance();
        $this->stockConfiguration = $this->objectManager->get(
            StockConfigurationInterface::class
        );
        $this->productMetadata = $this->objectManager->get(
            ProductMetadataInterface::class
        );
        $this->eavConfig = $this->objectManager->get(
            Config::class
        );
        $this->stockManagement = $this->objectManager->get(
            StockManagementInterface::class
        );
        $this->date = $this->objectManager->get(
            DateTime::class
        );
        $this->registry = $this->objectManager->get(
            Registry::class
        );
        $this->dateTime = $this->objectManager->get(DateTimeLib::class);
        $this->getAssignedStockIdsBySku = $this->objectManager->get(GetAssignedStockIdsBySku::class);
        $this->getAllStockIds = $this->objectManager->get(GetAllStockIds::class);
        parent::_construct();
    }

    /**
     * Add stock data to collection
     *
     * @param Collection $collection
     * @return Collection $collection
     */
    public function addStockDataToCollection($collection)
    {
        $websiteId = $this->stockConfiguration->getDefaultScopeId();

        $joinCondition = $this->getConnection()->quoteInto(
            'e.entity_id = stock_item_index.product_id',
            $websiteId
        );

        $collection->getSelect()->join(
            ['stock_item_index' => $this->getMainTable()],
            $joinCondition,
            [
                'item_id' => 'item_id',
                'stock_id' => 'stock_id',
                'product_id' => 'product_id',
                'qty' => 'qty',
                'is_in_stock' => 'is_in_stock',
                'manage_stock' => 'manage_stock',
                'use_config_manage_stock' => 'use_config_manage_stock',
                'backorders' => 'backorders',
                'use_config_backorders' => 'use_config_backorders',
                'min_qty' => 'min_qty',
                'use_config_min_qty' => 'use_config_min_qty',
                'min_sale_qty' => 'min_sale_qty',
                'use_config_min_sale_qty' => 'use_config_min_sale_qty',
                'max_sale_qty' => 'max_sale_qty',
                'use_config_max_sale_qty' => 'use_config_max_sale_qty',
                'is_qty_decimal' => 'is_qty_decimal',
                'use_config_qty_increments' => 'use_config_qty_increments',
                'qty_increments' => 'qty_increments',
                'use_config_enable_qty_inc' => 'use_config_enable_qty_inc',
                'enable_qty_increments' => 'enable_qty_increments'
            ]
        );
        $productEntityId = $this->eavConfig->getEntityType(Product::ENTITY)->getId();
        $collection->getSelect()->join(
            ['ea' => $this->getTable('eav_attribute')],
            "ea.entity_type_id = $productEntityId AND ea.attribute_code = 'name'",
            [
                'name_attribute_id' => 'attribute_id'
            ]
        );

        if (!$this->isMagentoEnterprise()) {
            $collection->getSelect()->join(
                ['cpev' => $this->getTable('catalog_product_entity_varchar')],
                "cpev.entity_id = e.entity_id AND cpev.attribute_id = ea.attribute_id",
                [
                    'name' => 'value'
                ]
            );
        } else {
            $collection->getSelect()->join(
                ['cpev' => $this->getTable('catalog_product_entity_varchar')],
                "cpev.row_id = e.row_id AND cpev.attribute_id = ea.attribute_id",
                [
                    'name' => 'value'
                ]
            );
        }

        $this->filterByStockAndSource($collection);

        return $collection;
    }

    /**
     * Filter by stock and source
     *
     * @param Collection $collection
     * @return Collection
     */
    public function filterByStockAndSource($collection)
    {
        $stockId = $this->stockManagement->getStockId();
        if (!$stockId) {
            return $collection;
        }
        $objectManager = ObjectManager::getInstance();
        /** @var ResourceConnection $resource */
        $resource = $objectManager->create(ResourceConnection::class);
        $stockTable = $objectManager
            ->get(StockIndexTableNameResolverInterface::class)
            ->execute($stockId);
        if (!$resource->getConnection()->isTableExists($stockTable)) {
            return $collection;
        }
        $sourceItemTable = $resource->getTableName('inventory_source_item');
        $linkedSources = $this->stockManagement->getLinkedSourceCodesByStockId($stockId);
        $reservationTable = $resource->getTableName('inventory_reservation');
        $select = $resource->getConnection()->select()
            ->from(['main_table' => $reservationTable], ['sku'])
            ->where('stock_id = ?', $stockId)
            ->columns(['quantity' => 'SUM(IF(main_table.quantity, main_table.quantity, 0))'])
            ->group('sku');
        $collection->getSelect()
            ->joinLeft(
                ['inventory_source_item' => $sourceItemTable],
                "e.sku = inventory_source_item.sku
                    AND inventory_source_item.source_code IN ('" . implode("', '", $linkedSources) . "')",
                ['source_code', 'quantity']
            )->joinLeft(
                ['stock_table' => $stockTable],
                'e.sku = stock_table.sku',
                ['is_salable']
            )->joinLeft(
                ['reservation' => $select],
                "e.sku = reservation.sku",
                [
                    'qty' => '(IF(stock_table.quantity, stock_table.quantity, 0)'
                        .' + IF(reservation.quantity, reservation.quantity, 0))'
                ]
            )->having('inventory_source_item.source_code IN (?)', $linkedSources)
            ->orHaving('stock_table.is_salable = ?', 1);
        $collection->getSelect()->columns(
            ['is_in_stock' => 'IF(stock_table.is_salable, stock_table.is_salable, 0)']
        );
        return $collection;
    }

    /**
     * Is Magento EE
     *
     * @return bool
     */
    public function isMagentoEnterprise()
    {
        $edition = $this->productMetadata->getEdition();
        return $edition == 'Enterprise' || $edition == 'B2B';
    }

    /**
     * Get available qty
     *
     * @param int $product_id
     * @param int $website_id
     * @return array
     */
    public function getAvailableQty($product_id, $website_id = 0)
    {
        $connection = $this->getConnection();

        $select = $connection->select();
        $select->from(['e' => $this->getTable('cataloginventory_stock_item')]);
        $select->where('product_id = ' . $product_id);
        $select->where('website_id = ' . $website_id);
        $select->reset(Select::COLUMNS);
        $select->columns("qty");

        $qtys = $connection->fetchAll($select);

        return $qtys;
    }

    /**
     * Update time by sku
     *
     * @param array $skus
     * @throws LocalizedException
     */
    public function updateUpdatedTimeBySku($skus)
    {
        if (is_array($skus) && !empty($skus)) {
            $skus = array_unique($skus);
            $processedSkus = $this->registry->registry('webpos_save_stock_item_sku_updated_time');
            if (!empty($processedSkus) && is_array($processedSkus)) {
                $skus = array_diff($skus, $processedSkus);
                $this->registry->unregister('webpos_save_stock_item_sku_updated_time');
                $this->registry->register(
                    'webpos_save_stock_item_sku_updated_time',
                    array_merge($skus, $processedSkus)
                );
            }
            $productTable = $this->getTable('catalog_product_entity');
            $stockItemTable = $this->getMainTable();
            $connection = $this->getConnection();
            $statement = $connection->select()->from(
                $productTable,
                ['entity_id']
            )->where(
                'sku IN (?)',
                $skus
            );
            $productIds = $connection->fetchCol($statement);
            $connection->update(
                $stockItemTable,
                ['updated_time' => $this->date->formatDate($this->dateTime->gmtTimestamp())],
                ['product_id IN (?)' => $productIds]
            );
        }
    }

    /**
     * Get Assigned Stock Id By ids
     *
     * @param array $ids
     */
    public function getAssignedStockIdByIds($ids)
    {
        $catalogSelect = $this->getConnection()->select()
            ->from(
                $this->getTable('catalog_product_entity'),
                ['entity_id', 'sku']
            )->where("entity_id IN (?)", $ids);

        $stockItemSelect = $this->getConnection()->select()
            ->from(
                ['source_item' => $this->getTable('inventory_source_item')],
                []
            );
        $stockItemSelect->joinInner(
            ['catalog_product' => $catalogSelect],
            "source_item.sku = catalog_product.sku"
        );
        $stockItemSelect->joinInner(
            ['source_stock_link' => $this->getTable('inventory_source_stock_link')],
            "source_item.source_code = source_stock_link.source_code",
            ['stock_id']
        );

        // Remove duplicate stock_id by sku
        $select = $this->getConnection()->select()
            ->from(
                $stockItemSelect,
                new Zend_Db_Expr('DISTINCT entity_id, stock_id')
            );

        // Join with composite products
        $resultSelect = $this->getStockForCompositeProducts($select, $ids);

        $result = [];
        foreach ($this->getConnection()->fetchAll($resultSelect) as $item) {
            $result[$item['entity_id']]['stock_id'][] = $item['stock_id'];
        }

        return $result;
    }

    /**
     * Get composite products stock
     *
     * @param Select $resultSelect
     * @param array $ids
     * @return Select
     */
    public function getStockForCompositeProducts($resultSelect, $ids)
    {
        $parentSelect = $this->getConnection()->select()
            ->from(
                ['e' => $this->getTable('catalog_product_entity')],
                ['entity_id', 'sku']
            )
            ->where("entity_id IN (?)", $ids)
            ->where('type_id IN (?)', [Grouped::TYPE_CODE, TypeBundle::TYPE_BUNDLE, Configurable::TYPE_CODE]);

        $compositeProduct = [];
        /** @var IndexNameBuilder $indexNameBuilder */
        $indexNameBuilder = ObjectManager::getInstance()->create(IndexNameBuilder::class);
        /** @var IndexNameResolverInterface $indexNameResolver */
        $indexNameResolver = ObjectManager::getInstance()->create(IndexNameResolverInterface::class);
        foreach ($this->getAllStockIds->execute() as $stockId) {
            $indexName = $indexNameBuilder
                ->setIndexId(InventoryIndexer::INDEXER_ID)
                ->addDimension('stock_', (string)$stockId)
                ->setAlias(Alias::ALIAS_MAIN)
                ->build();
            $indexTableName = $indexNameResolver->resolveName($indexName);

            $compositeProduct[] = $this->getConnection()->select()
                ->from(
                    ['e' => $parentSelect],
                    ['entity_id']
                )->joinInner(
                    ['stock' => $indexTableName],
                    'e.sku = stock.sku',
                    ['stock_id' => new Zend_Db_Expr($stockId)]
                );
        }

        if (count($compositeProduct)) {
            return $this->getConnection()->select()->union(
                array_merge([$resultSelect], $compositeProduct),
                Select::SQL_UNION_ALL
            );
        } else {
            return $resultSelect;
        }
    }

    /**
     * Reindex by source items
     *
     * @param array $sourceItems
     * @return array
     */
    public function reindexBySourceItem($sourceItems)
    {
        $data = [];
        foreach ($sourceItems as $item) {
            $data[] = [
                'source_code' => $item['source_code'],
                'sku' => $item['sku']
            ];
        }

        $tmpTableName = 'tmp_source_item';
        $tmpTable = $this->getConnection()->newTable($this->getTable($tmpTableName));
        $tmpTable->addColumn(
            'source_code',
            Table::TYPE_TEXT,
            64,
            ['nullable' => false],
            'Source Code'
        );
        $tmpTable->addColumn(
            'sku',
            Table::TYPE_TEXT,
            64,
            ['nullable' => false],
            'Sku'
        );
        $this->getConnection()->createTemporaryTable($tmpTable);
        $this->getConnection()->insertOnDuplicate($this->getTable($tmpTableName), $data);

        $newSourceItemSelect = $this->getConnection()->select()
            ->from(
                ['e' => $this->getTable($tmpTableName)],
                []
            );
        $newSourceItemSelect->joinLeft(
            ['source_item' => $this->getTable('inventory_source_item')],
            'e.sku = source_item.sku AND e.source_code = source_item.source_code',
            []
        )->where('source_item.source_item_id IS NULL');
        $newSourceItemSelect->joinInner(
            ['catalog_entity' => $this->getTable('catalog_product_entity')],
            'e.sku = catalog_entity.sku',
            ['entity_id']
        );

        ///// Get parent product /////
        /* Bundle */
        $bundleSelect = $this->bundleProductIndexRowUpdate($newSourceItemSelect);

        /* Grouped */
        $groupedSelect = $this->groupedProductIndexRowUpdate($newSourceItemSelect);

        /* Configurable */
        $configurableSelect = $this->configurableProductIndexRowUpdate($newSourceItemSelect);
        ///// End: Get parent product /////

        $ids = [];
        foreach ([$newSourceItemSelect, $groupedSelect, $configurableSelect, $bundleSelect] as $select) {
            $ids = array_merge($ids, $this->getConnection()->fetchCol($select)); // phpcs:ignore
        }

        $this->getConnection()->dropTemporaryTable($this->getTable($tmpTableName));
        return $ids;
    }

    /**
     * Bundle Product Index Row Update
     *
     * @param Select $childrenSelect
     * @return Select
     */
    public function bundleProductIndexRowUpdate($childrenSelect)
    {
        $bundleSelect = $this->getConnection()->select()
            ->from(
                ['children' => $childrenSelect],
                []
            )->joinInner(
                ['parent_link' => $this->getTable('catalog_product_bundle_selection')],
                'parent_link.product_id = children.entity_id',
                []
            )->joinInner(
                ['parent_product_entity' => $this->getTable('catalog_product_entity')],
                'parent_product_entity.' . $this->getProductLinkedField() . ' = parent_link.parent_product_id',
                ['entity_id']
            );

        return $this->getConnection()->select()
            ->from(
                $bundleSelect,
                new Zend_Db_Expr('DISTINCT entity_id')
            );
    }

    /**
     * Grouped Product Index Row Update
     *
     * @param Select $childrenSelect
     * @return Select
     */
    public function groupedProductIndexRowUpdate($childrenSelect)
    {
        $groupedSelect = $this->getConnection()->select()
            ->from(
                ['children' => $childrenSelect],
                []
            )->joinInner(
                ['parent_link' => $this->getTable('catalog_product_link')],
                'parent_link.linked_product_id = children.entity_id 
                AND parent_link.link_type_id = ' . Link::LINK_TYPE_GROUPED,
                []
            )->joinInner(
                ['parent_product_entity' => $this->getTable('catalog_product_entity')],
                'parent_product_entity.' . $this->getProductLinkedField() . ' = parent_link.product_id',
                ['entity_id']
            );

        return $this->getConnection()->select()
            ->from(
                $groupedSelect,
                new Zend_Db_Expr('DISTINCT entity_id')
            );
    }

    /**
     * Configurable Product Index Row Update
     *
     * @param Select $childrenSelect
     * @return Select
     */
    public function configurableProductIndexRowUpdate($childrenSelect)
    {
        $configurableSelect = $this->getConnection()->select()
            ->from(
                ['children' => $childrenSelect],
                []
            )->joinInner(
                ['parent_link' => $this->getTable('catalog_product_super_link')],
                'parent_link.product_id = children.entity_id',
                []
            )->joinInner(
                ['parent_product_entity' => $this->getTable('catalog_product_entity')],
                'parent_product_entity.' . $this->getProductLinkedField() . ' = parent_link.parent_id',
                ['entity_id']
            );

        return $this->getConnection()->select()
            ->from(
                $configurableSelect,
                new Zend_Db_Expr('DISTINCT entity_id')
            );
    }

    /**
     * Get linked field of product
     *
     * @return string
     */
    public function getProductLinkedField()
    {
        /** @var MetadataPool $metapool */
        $metaPool = ObjectManager::getInstance()->create(MetadataPool::class);
        $metadata = $metaPool->getMetadata(ProductInterface::class);
        return $metadata->getLinkField();
    }
}
