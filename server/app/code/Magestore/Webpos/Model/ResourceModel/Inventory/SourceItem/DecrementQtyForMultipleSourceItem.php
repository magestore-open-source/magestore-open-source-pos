<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Model\ResourceModel\Inventory\SourceItem;

use Magento\Framework\App\ResourceConnection;
use Magento\Inventory\Model\ResourceModel\SourceItem as SourceItemResourceModel;
use Zend_Db_Expr;

/**
 * Implementation of SourceItem decrement qty operation for specific db layer
 */
class DecrementQtyForMultipleSourceItem
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Decrement qty for source item
     *
     * @param array $decrementItems
     * @return void
     */
    public function execute(array $decrementItems): void
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName(SourceItemResourceModel::TABLE_NAME_SOURCE_ITEM);
        if (!count($decrementItems)) {
            return;
        }
        foreach ($decrementItems as $decrementItem) {
            $sourceItem = $decrementItem['source_item'];
            $where = [
                'source_code = ?' => $sourceItem->getSourceCode(),
                'sku = ?' => $sourceItem->getSku()
            ];
            $connection->update(
                [$tableName],
                ['quantity' => new Zend_Db_Expr('quantity - ' . $decrementItem['qty_to_decrement'])],
                $where
            );
        }
    }
}
