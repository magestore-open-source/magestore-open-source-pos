<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\ResourceModel\Customer\Indexer;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;

/**
 * Customers collection for customer_grid indexer
 */
class Collection extends \Magento\Customer\Model\ResourceModel\Customer\Collection
{
    /**
     * @inheritdoc
     */
    protected function beforeAddLoadedItem(DataObject $item)
    {
        if (!$item->getBillingTelephone()) {
            $objectManager = ObjectManager::getInstance();
            $eavConfig = $objectManager->get(Config::class);
            $customerId = $item->getId();

            $customerSelect = $this->getConnection()->select()
                ->from(
                    ['e' => $this->getMainTable()],
                    []
                );

            /** Join with attribute name */
            $customerEntityId = $eavConfig->getEntityType(Customer::ENTITY)->getId();
            $customerSelect->join(
                ['ea' => $this->getTable('eav_attribute')],
                "ea.entity_type_id = $customerEntityId AND ea.attribute_code = 'customer_telephone'",
                []
            );
            $customerSelect->join(
                ['cev' => $this->getTable('customer_entity_varchar')],
                "cev.entity_id = e.entity_id AND cev.attribute_id = ea.attribute_id",
                [
                    'billingTelephone' => 'value'
                ]
            );
            $customerSelect->where("e.entity_id = ?", $customerId);
            $data = $this->getConnection()->fetchAll($customerSelect);
            if (count($data)) {
                $item->setBillingTelephone($data[0]['billingTelephone']);
            }
        }
        return $item;
    }
}
