<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Catalog;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Catalog - Search
 */
class Search extends AbstractDb
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_init('webpos_search_product', 'entity_id');
    }
    
    /**
     * Set store ID for resource model
     *
     * @param int $storeId
     */
    public function setStoreId($storeId)
    {
        $this->_init(sprintf('webpos_search_product_%s', $storeId), 'entity_id');
    }
}
