<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Catalog\Search;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Api\Search\SearchResult;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Search\SearchResponseBuilder;
use Magento\Search\Model\SearchEngine;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Api\SearchCriteriaInterface;
use Magestore\Webpos\Helper\Data as WebposHelper;
use Magestore\Webpos\Model\ResourceModel\Catalog\Search;
use Magestore\Webpos\Model\Search\Request\Builder;
use Zend_Db_Expr;

/**
 * Class Collection
 *
 * Used for search product collection
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Collection extends AbstractCollection
{
    /**
     * @var Builder $builder
     */
    protected $builder;

    /**
     * @var SearchEngine $searchEngine
     */
    protected $searchEngine;

    /**
     * @var SearchResponseBuilder
     */
    protected $searchResponseBuilder;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var WebposHelper
     */
    protected $webposHelper;
    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init(
            \Magestore\Webpos\Model\Catalog\Search::class,
            Search::class
        );

        $objectManager = ObjectManager::getInstance();
        $this->storeManager = $objectManager->get(StoreManagerInterface::class);
        $this->builder = $objectManager->get(Builder::class);
        $this->searchResponseBuilder = $objectManager->get(SearchResponseBuilder::class);
        $this->searchEngine = $objectManager->get(SearchEngine::class);
        $this->webposHelper = $objectManager->get(WebposHelper::class);
        $this->stockManagement = $objectManager->get(StockManagementInterface::class);
    }
    
    /**
     * Set store ID for collection
     *
     * @param int $storeId
     */
    public function setStoreId($storeId)
    {
        $this->getResource()->setStoreId($storeId);
        $this->setMainTable($this->getResource()->getMainTable());
        $this->_reset();
    }
    
    /**
     * @inheritDoc
     */
    protected function _initSelect()
    {
        $this->getSelect()->from(['e' => $this->getMainTable()]);
        return $this;
    }

    /**
     * Get SQL for get record count without left JOINs
     *
     * @return Select
     */
    public function getSelectCountSql()
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Select::ORDER);
        $countSelect->reset(Select::LIMIT_COUNT);
        $countSelect->reset(Select::LIMIT_OFFSET);

        if (!count($this->getSelect()->getPart(Select::GROUP))) {
            $countSelect->reset(Select::COLUMNS);
            $countSelect->columns(new Zend_Db_Expr('COUNT(*)'));
            return $countSelect;
        }
        $group = $this->getSelect()->getPart(Select::GROUP);
        $countSelect->columns(new Zend_Db_Expr(("COUNT(DISTINCT ".implode(", ", $group).")")));
        $select = clone $countSelect;
        $countSelect->reset()->from($select, ['COUNT(*)']);
        return $countSelect;
    }

    /**
     * Search Webpos Product In search Engine
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function searchWebposProductInSearchEngine(
        SearchCriteriaInterface $searchCriteria
    ) {
        $this->builder->bind('search_term', $searchCriteria->getQueryString());
        $this->builder->bind('allow_stock_id', $this->stockManagement->getStockId());
        $this->builder->bind('allow_product_type', $this->webposHelper->getProductTypeIds());
        $this->builder->setRequestName('webpos_productsearch_fulltext');
        $this->builder->bindDimension('scope', $this->storeManager->getStore()->getId());
        if ($searchCriteria->getCurrentPage()) {
            $this->builder->setFrom(($searchCriteria->getCurrentPage() - 1) * $searchCriteria->getPageSize());
        }
        if ($searchCriteria->getPageSize()) {
            $this->builder->setSize($searchCriteria->getPageSize());
        }

        $queryRequest = $this->builder->create();

        $queryResponse = $this->searchEngine->search($queryRequest);
        return $this->searchResponseBuilder->build($queryResponse)
            ->setSearchCriteria($searchCriteria);
    }

    /**
     * Get product Id by barcode
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResult|SearchResultInterface
     * @throws NoSuchEntityException
     */
    public function getProductIdByBarcodeInElasticSearch(
        SearchCriteriaInterface $searchCriteria
    ) {
        $this->builder->bind('allow_product_type', $this->webposHelper->getProductTypeIds());
        $this->builder->bind('allow_stock_id', $this->stockManagement->getStockId());
        $this->builder->bind('search_term', $searchCriteria->getQueryString());
        $this->builder->setRequestName('webpos_scan_barcode');
        $this->builder->bindDimension('scope', $this->storeManager->getStore()->getId());
        $queryRequest = $this->builder->create();

        $queryResponse = $this->searchEngine->search($queryRequest);
        return $this->searchResponseBuilder->build($queryResponse)
            ->setSearchCriteria($searchCriteria);
    }

    /**
     * Function getQueryCategoryId used for get category id
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return string|null
     */
    private function getQueryCategoryId($searchCriteria)
    {
        foreach ($searchCriteria->getFilterGroups() as $group) {
            foreach ($group->getFilters() as $filter) {
                if ($filter->getField() === 'category_id') {
                    return $filter->getValue();
                }
            }
        }
        return null;
    }

    /**
     * Search Webpos Product In search Engine
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     * @throws LocalizedException
     */
    public function getProductListInSearchEngine(
        SearchCriteriaInterface $searchCriteria
    ) {
        $this->builder->bind('allow_product_type', $this->webposHelper->getProductTypeIds());
        $this->builder->bind('allow_stock_id', $this->stockManagement->getStockId());
        $categoryId = $this->getQueryCategoryId($searchCriteria);
        if ($categoryId) {
            $this->builder->bind('category_ids', $categoryId);
            $this->builder->setRequestName('webpos_product_category_list');
        } else {
            $this->builder->setRequestName('webpos_product_list');
        }

        $this->builder->bindDimension('scope', $this->storeManager->getStore()->getId());
        if ($searchCriteria->getCurrentPage()) {
            $this->builder->setFrom(($searchCriteria->getCurrentPage() - 1) * $searchCriteria->getPageSize());
        }
        if ($searchCriteria->getPageSize()) {
            $this->builder->setSize($searchCriteria->getPageSize());
        }

        $objectManager = ObjectManager::getInstance();
        // Set sort order
        /** @var SortOrder $sortOrder */
        $sortOrder = $objectManager->create(SortOrder::class);
        $sortOrder->setField($searchCriteria->getSortOrders()[0]->getField());
        $sortOrder->setDirection(
            $searchCriteria->getSortOrders()[0]->getDirection() ?: SortOrder::SORT_ASC
        );
        $this->builder->setSort([$sortOrder]);

        $queryRequest = $this->builder->create();

        $queryResponse = $this->searchEngine->search($queryRequest);
        return $this->searchResponseBuilder->build($queryResponse)
            ->setSearchCriteria($searchCriteria);
    }
}
