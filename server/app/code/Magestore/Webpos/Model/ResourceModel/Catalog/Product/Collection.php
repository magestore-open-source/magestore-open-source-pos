<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

// @codingStandardsIgnoreFile

namespace Magestore\Webpos\Model\ResourceModel\Catalog\Product;


use Magento\Eav\Model\ResourceModel\Attribute\DefaultEntityAttributes\ProviderInterface;
use Magento\Framework\DB\Select;
use Zend_Db_Expr;

/**
 * Product collection
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @method ProviderInterface getResource()
 */
class Collection extends \Magento\Catalog\Model\ResourceModel\Product\Collection
{
    const VISIBLE_ON_WEBPOS = 1;

    /**
     * Initialize resources
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Magestore\Webpos\Model\Catalog\Product::class,
            \Magento\Catalog\Model\ResourceModel\Product::class
        );
        $this->_initTables();
    }

    /**
     * Filter product collection that visible on webpos
     *
     * @return Collection
     */
    public function addVisibleFilter()
    {
        $this->addAttributeToFilter([
            ['attribute' => 'webpos_visible', 'eq' => self::VISIBLE_ON_WEBPOS, 'left'],
        ], '', 'left');
        return $this;
    }

    /**
     * Get collection size
     *
     * @return int
     */
    public function getSize()
    {
        if (is_null($this->_totalRecords)) {
            $sql = $this->getSelect();
            $this->_totalRecords = count($this->getConnection()->fetchAll($sql, $this->_bindParams));
        }
        return intval($this->_totalRecords);
    }
    
    /**
     * Get SQL for get record count without left JOINs
     *
     * @return Select
     */
    public function getSelectCountSql()
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Select::ORDER);
        $countSelect->reset(Select::LIMIT_COUNT);
        $countSelect->reset(Select::LIMIT_OFFSET);

        if (!count($this->getSelect()->getPart(Select::GROUP))) {
            $countSelect->reset(Select::COLUMNS);
            $countSelect->columns(new Zend_Db_Expr('COUNT(*)'));
            return $countSelect;
        }
        $group = $this->getSelect()->getPart(Select::GROUP);
        $countSelect->columns(new Zend_Db_Expr(("COUNT(DISTINCT ".implode(", ", $group).")")));
        $select = clone $countSelect;
        $countSelect->reset()->from($select, ['COUNT(*)']);
        return $countSelect;
    }
}
