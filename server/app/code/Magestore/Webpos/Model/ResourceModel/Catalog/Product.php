<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Catalog;

/**
 * ResourceModel Product
 */
class Product extends \Magento\Catalog\Model\ResourceModel\Product
{
    /**
     * Get Product Static Fields Information
     *
     * @param string[] $fields
     * @param array $productIds
     * @return array
     */
    public function getProductStaticFieldsInformation($fields = [], $productIds = [])
    {
        $select = $this->getConnection()->select()
            ->from([
                $this->getTable('catalog_product_entity'),
                []
            ]);
        if (count($fields)) {
            $select->columns($fields);
        } else {
            $select->columns('*');
        }
        if (count($productIds)) {
            $select->where('entity_id IN (?)', $productIds);
        }
        return $this->getConnection()->fetchAll($select);
    }

    /**
     * Update updated time of products
     *
     * @param string $tableName
     * @param string $updatedAt
     * @param array $productIds
     */
    public function updateUpdatedTimeOfProducts($tableName, $updatedAt, $productIds)
    {
        $connection = $this->getConnection();
        if ($connection->isTableExists($tableName)) {
            $this->getConnection()->update(
                $tableName,
                ['updated_at' => $updatedAt],
                $connection->quoteInto("entity_id IN (?)", $productIds)
            );
        }
    }
}
