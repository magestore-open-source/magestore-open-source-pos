<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Log;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magestore\Webpos\Api\Data\Log\ProductDeletedInterface;

/**
 * Log - ProductDeleted
 */
class ProductDeleted extends AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('webpos_product_deleted', 'id');
    }

    /**
     * Insert Multiple
     *
     * @param int $productId
     * @param int[]|null $stockIds
     */
    public function insertMultiple($productId = null, $stockIds = [])
    {
        $connection = $this->getConnection();
        $insertData = [];
        if (empty($stockIds)) {
            $connection->insertMultiple($this->getMainTable(), [ProductDeletedInterface::PRODUCT_ID => $productId]);
        } else {
            foreach ($stockIds as $stockId) {
                $insertData[] = [
                    ProductDeletedInterface::PRODUCT_ID => $productId,
                    ProductDeletedInterface::STOCK_ID => $stockId
                ];
            }
            $connection->insertMultiple($this->getMainTable(), $insertData);
        }
    }

    /**
     * Delete By Product Id
     *
     * @param int|null $productId
     * @param int[]|null $stockIds
     */
    public function deleteByProductId($productId = null, $stockIds = [])
    {
        if ($productId) {
            $whereCondition = [ProductDeletedInterface::PRODUCT_ID . ' = ?' => $productId];
            if (!empty($stockIds)) {
                $whereCondition[ProductDeletedInterface::STOCK_ID . ' IN (?)'] = $stockIds;
            }
            $this->getConnection()->delete($this->getMainTable(), $whereCondition);
        }
    }
}
