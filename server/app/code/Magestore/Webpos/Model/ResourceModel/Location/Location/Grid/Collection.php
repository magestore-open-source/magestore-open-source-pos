<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Location\Location\Grid;

use Magento\Framework\DB\Select;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Zend_Db_Expr;

/**
 * Class \Magestore\Webpos\Model\ResourceModel\Location\Location\Grid\Collection
 */
class Collection extends SearchResult
{
    /**
     * @inheritDoc
     */
    protected function _initSelect()
    {

        $this->getSelect()->from(['main_table' => $this->getMainTable()])
            ->columns(
                [
                    'address' => new Zend_Db_Expr("CONCAT_WS(', ',street, city, region, country, postcode)")
                ]
            );

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addFieldToFilter($field, $condition = null)
    {
        if (is_array($field)) {
            $conditions = [];
            foreach ($field as $key => $value) {
                $conditions[] = $this->_translateCondition($value, isset($condition[$key]) ? $condition[$key] : null);
            }

            $resultCondition = '(' . implode(') ' . Select::SQL_OR . ' (', $conditions) . ')';
        } else {
            $resultCondition = $this->_translateCondition($field, $condition);
        }

        if ($field != 'address') {
            $this->_select->where($resultCondition, null, Select::TYPE_CONDITION);
        } else {
            $this->_select->having($resultCondition, null, Select::TYPE_CONDITION);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getSelectCountSql()
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Select::ORDER);
        $countSelect->reset(Select::LIMIT_COUNT);
        $countSelect->reset(Select::LIMIT_OFFSET);
        $countSelect->reset(Select::COLUMNS);

        if (!count($this->getSelect()->getPart(Select::GROUP))) {
            $countSelect->columns(new Zend_Db_Expr('COUNT(*)'));
            $countSelect->columns(
                [
                    'address' => new Zend_Db_Expr("CONCAT_WS(', ',street, city, region, country, postcode)")
                ]
            );
            return $countSelect;
        }

        $countSelect->reset(Select::GROUP);
        $group = $this->getSelect()->getPart(Select::GROUP);
        $countSelect->columns(new Zend_Db_Expr(("COUNT(DISTINCT ".implode(", ", $group).")")));
        return $countSelect;
    }
}
