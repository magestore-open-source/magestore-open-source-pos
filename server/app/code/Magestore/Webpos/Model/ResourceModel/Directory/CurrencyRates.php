<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\ResourceModel\Directory;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Class CurrencyRates
 *
 * Get currency rates
 */
class CurrencyRates
{
    /**
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * @var Resource
     */
    protected $resource;

    /**
     * @param ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->resource = $resource;
        $this->connection = $resource->getConnection();
    }

    /**
     * Get currency rates
     *
     * @param string $fromCode
     * @param array $toCodes
     * @return array
     */
    public function getCurrencyRates(string $fromCode, array $toCodes)
    {
        $select = $this->connection->select()->from(
            [
                'currency_rate' => $this->resource->getTableName('directory_currency_rate'),
            ]
        )->where('currency_from = ?', $fromCode)
        ->where('currency_to IN (?)', $toCodes);
        $data = $this->connection->fetchAll($select);
        $rates = [];
        if (!empty($data)) {
            foreach ($data as $rate) {
                $rates[$rate['currency_to']] = $rate['rate'];
            }
        }
        return $rates;
    }
}
