<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\ResourceModel\Checkout;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend_Db_Exception;

/**
 * Class PosOrder
 *
 * Used for resource model table webpos_order
 */
class PosOrder extends AbstractDb
{
    /**
     * @var TimezoneInterface
     */
    protected $localeDate;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('webpos_order', 'id');
        $objectManager = ObjectManager::getInstance();
        $this->localeDate = $objectManager->get(TimezoneInterface::class);
    }

    /**
     * Function delete Webpos Backup Table
     */
    public function deleteWebposBackupTable()
    {
        $currentDate = $this->localeDate->date()->format("Y-m-d h:i:s");
        $dateTwoMonthAgo = date('Ymd', strtotime('last day of -3 month', strtotime($currentDate)));
        $connection = $this->getConnection();
        $tableNameToCompare = $this->getTable("webpos_order_backup_".$dateTwoMonthAgo);
        foreach ($connection->getTables("%webpos_order_backup_%") as $table) {
            // Drop all table has been pass 2 months from current month
            if (strncmp($tableNameToCompare, $table, strlen($tableNameToCompare)) > 0) {
                if ($connection->isTableExists($table)) {
                    $connection->dropTable($table);
                }
            }
        }
    }

    /**
     * Process backup order of webpos order
     *
     * @param string $incrementId
     */
    public function processBackupOfWebposOrder($incrementId)
    {
        $currentDate = $this->localeDate->date()->format("Y-m-d h:i:s");
        $monday = date('Ymd', strtotime('monday this week', strtotime($currentDate)));
        $sunday = date('Ymd', strtotime('sunday this week', strtotime($currentDate)));

        $connection = $this->getConnection();
        $backupTable = $this->getTable("webpos_order_backup_".$monday."_".$sunday);
        $mainTable = $this->getMainTable();
        if (!$connection->isTableExists($backupTable)) {
            $this->createTableBackupWebposOrder($backupTable, $mainTable);
        }

        $columns = [
            'status' => 'o.status',
            'created_at' => 'o.created_at',
            'additional_data' => 'o.additional_data',
            'increment_id' => 'o.increment_id',
            'session_id' => 'o.session_id',
            'store_id' => 'o.store_id',
            'location_id' => 'o.location_id',
            'pos_id' => 'o.pos_id',
            'pos_staff_id' => 'o.pos_staff_id',
            'order_created_time' => 'o.order_created_time',
            'params' => 'o.params'
        ];
        $select = $connection->select();
        $select->from(
            ['o' => $mainTable],
            $columns
        )->where("increment_id = ?", $incrementId);
        $connection->query($connection->insertFromSelect($select, $backupTable, array_keys($columns)));
        $connection->delete($mainTable, ["increment_id = ?" => $incrementId]);
    }

    /**
     * Function  createTableBackupWebposOrder used for create table backup data from exist table for webpos order
     *
     * @param string $newTableName
     * @param string $originTable
     * @throws Zend_Db_Exception
     */
    public function createTableBackupWebposOrder($newTableName, $originTable)
    {
        $connection = $this->getConnection();
        $tmpTable = $connection->createTableByDdl($originTable, $newTableName);
        $connection->createTable($tmpTable);
    }
}
