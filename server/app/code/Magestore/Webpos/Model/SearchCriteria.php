<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model;

use Magestore\Webpos\Api\SearchCriteriaInterface;

/**
 * Search Criteria Model
 */
class SearchCriteria extends \Magento\Framework\Api\SearchCriteria implements SearchCriteriaInterface
{
    const QUERY_STRING = 'query_string';
    const IS_LIMIT = 'is_limit';
    const IS_HOLD = 'is_hold';

    /**
     * @inheritDoc
     */
    public function setQueryString($queryString)
    {
        $this->setData(self::QUERY_STRING, $queryString);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getQueryString()
    {
        return urldecode($this->_get(self::QUERY_STRING));
    }

    /**
     * @inheritDoc
     */
    public function setIsLimit($isLimit)
    {
        $this->setData(self::IS_LIMIT, $isLimit);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getIsLimit()
    {
        return $this->_get(self::IS_LIMIT);
    }

    /**
     * @inheritDoc
     */
    public function setIsHold($isHold)
    {
        $this->setData(self::IS_HOLD, $isHold);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getIsHold()
    {
        return $this->_get(self::IS_HOLD);
    }
}
