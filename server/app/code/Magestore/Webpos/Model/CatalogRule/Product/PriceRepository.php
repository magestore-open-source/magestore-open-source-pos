<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\CatalogRule\Product;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Webapi\ServiceOutputProcessor;
use Magestore\Webpos\Api\CatalogRule\Product\PriceRepositoryInterface;
use Magestore\Webpos\Api\Data\CatalogRule\Product\PriceIdsSearchResultsInterface;
use Magestore\Webpos\Api\Data\CatalogRule\Product\PriceIdsSearchResultsInterfaceFactory;
use Magestore\Webpos\Api\Data\CatalogRule\Product\PriceInterface;
use Magestore\Webpos\Api\Data\CatalogRule\Product\PriceSearchResultsInterface;
use Magestore\Webpos\Api\Data\CatalogRule\Product\PriceSearchResultsInterfaceFactory;
use Magestore\Webpos\Api\SyncInterface;
use Magestore\Webpos\Model\Cache\Type;
use Magestore\Webpos\Model\ResourceModel\CatalogRule\Product\Price\Collection;
use Magestore\Webpos\Model\ResourceModel\CatalogRule\Product\Price\CollectionFactory;
use Zend_Db_Expr;

/**
 * Catalog rule price repository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PriceRepository implements PriceRepositoryInterface
{
    /**
     * @var PriceSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var PriceIdsSearchResultsInterfaceFactory
     */
    protected $searchIdsResultsFactory;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * PriceRepository constructor.
     *
     * @param PriceSearchResultsInterfaceFactory $searchResultsFactory
     * @param PriceIdsSearchResultsInterfaceFactory $searchIdsResultsFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        PriceSearchResultsInterfaceFactory $searchResultsFactory,
        PriceIdsSearchResultsInterfaceFactory $searchIdsResultsFactory,
        CollectionFactory $collectionFactory
    ) {
        $this->searchResultsFactory = $searchResultsFactory;
        $this->searchIdsResultsFactory = $searchIdsResultsFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Retrieve Catalog rule  product price which match a specified criteria.
     *
     * @api
     * @param SearchCriteriaInterface $searchCriteria
     * @return PriceSearchResultsInterface|array
     * @throws LocalizedException
     */
    public function sync(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();
        
        /** @var StateInterface $cacheState */
        $cacheState = $objectManager->get(StateInterface::class);
        if (!$cacheState->isEnabled(Type::TYPE_IDENTIFIER)
            || count($searchCriteria->getFilterGroups())
        ) {
            return $this->processSync($searchCriteria);
        }
        
        /** @var CacheInterface $cache */
        $cache = $objectManager->get(CacheInterface::class);
        
        $key = 'syncCatalogRules-'
            . $searchCriteria->getPageSize()
            . '-' . $searchCriteria->getCurrentPage();
        
        if ($response = $cache->load($key)) {
            // Reponse from cache
            return json_decode($response, true);
        }
        
        $flag = SyncInterface::QUEUE_NAME;
        if ($cachedAt = $cache->load($flag)) {
            return [
                'async_stage'   => 2, // processing
                'cached_at'     => $cachedAt, // process started time
            ];
        }
        
        // Block metaphor
        $cachedAt = date("Y-m-d H:i:s");
        $cache->save($cachedAt, $flag, [Type::CACHE_TAG], 300);
        
        /** @var ServiceOutputProcessor $processor */
        $processor = $objectManager->get(ServiceOutputProcessor::class);
        $outputData = $processor->process(
            $this->processSync($searchCriteria),
            PriceRepositoryInterface::class,
            'sync'
        );
        $outputData['cached_at'] = $cachedAt;
        $cache->save(
            json_encode($outputData),
            $key,
            [
                Type::CACHE_TAG,
            ],
            43200   // Cache lifetime: 0.5 day
        );
        // Release metaphor
        $cache->remove($flag);
        return $outputData;
    }

    /**
     * Process Sync
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return PriceSearchResultsInterface
     * @see \Magestore\Webpos\Api\CatalogRule\Product\PriceRepositoryInterface::sync()
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function processSync(SearchCriteriaInterface $searchCriteria)
    {
        /** @var PriceSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        $isSync = true;

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === PriceInterface::UPDATED_TIME) {
                    $isSync = false;
                }
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $searchResults->addFieldToFilter($fields, $conditions);
            }
        }

        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /* @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $searchResults->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } elseif (!$isSync) {
            $searchResults->addOrder(
                PriceInterface::UPDATED_TIME,
                'ASC'
            );
        }

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setCurPage($searchCriteria->getCurrentPage());
        $searchResults->setPageSize($searchCriteria->getPageSize());
        return $searchResults;
    }

    /**
     * Get Rule Product Price Ids
     *
     * @return array
     */
    protected function getRuleProductPriceIds()
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $select = $collection->getSelect()
            ->reset(Select::COLUMNS)
            ->columns(['rule_product_price_id']);
        return $collection->getResource()->getConnection()->fetchCol($select);
    }
    /**
     * Retrieve Catalog rule product price ids which match a specified criteria.
     *
     * @api
     * @param SearchCriteriaInterface $searchCriteria
     * @return PriceIdsSearchResultsInterface
     * @throws LocalizedException
     */
    public function getAllIds(SearchCriteriaInterface $searchCriteria)
    {
        /** @var PriceIdsSearchResultsInterface $searchResults */
        $searchResults = $this->searchIdsResultsFactory->create();

        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $select = $collection->getSelect()
            ->reset(Select::COLUMNS)
            ->order('rule_product_price_id ASC')
            ->columns(['rule_product_price_id']);

        if ($searchCriteria->getPageSize() && $searchCriteria->getCurrentPage()) {
            $select->limit(
                $searchCriteria->getPageSize(),
                $searchCriteria->getPageSize() * ($searchCriteria->getCurrentPage() - 1)
            );
        }

        $resouce = $collection->getResource()->getConnection();
        $items = $resouce->fetchCol($select);

        $select->reset(Select::ORDER);
        $select->reset(Select::LIMIT_COUNT);
        $select->reset(Select::LIMIT_OFFSET);
        $select->reset(Select::COLUMNS);
        $select->columns(new Zend_Db_Expr(("COUNT(DISTINCT rule_product_price_id)")));
        
        $totalCount = $resouce->fetchOne($select);
        
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($items);
        $searchResults->setTotalCount($totalCount);
        return $searchResults;
    }
}
