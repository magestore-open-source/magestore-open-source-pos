<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Payment;

use Magento\Framework\Api\SearchResults;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Webpos\Api\Payment\PaymentRepositoryInterface;

/**
 * Class \Magestore\Webpos\Model\Payment\PaymnetRepository
 */
class PaymentRepository implements PaymentRepositoryInterface
{
    const EVENT_WEBPOS_GET_PAYMENT_AFTER = 'webpos_get_payment_after';
    /**
     * webpos payment source model
     *
     * @var \Magestore\Webpos\Model\Source\Adminhtml\Payment
     */
    protected $paymentModelSource;

    /**
     * webpos payment result interface
     *
     * @var SearchResults
     */
    protected $paymentResultInterface;

    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * PaymentRepository constructor.
     * @param \Magestore\Webpos\Model\Source\Adminhtml\Payment $paymentModelSource
     * @param SearchResults $paymentResultInterface
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        \Magestore\Webpos\Model\Source\Adminhtml\Payment $paymentModelSource,
        SearchResults $paymentResultInterface,
        ManagerInterface $eventManager
    ) {
        $this->paymentModelSource = $paymentModelSource;
        $this->paymentResultInterface = $paymentResultInterface;
        $this->eventManager = $eventManager;
    }

    /**
     * @inheritDoc
     */
    public function getList()
    {
        $paymentList = $this->paymentModelSource->getPosPaymentMethods();
        $data = ['payments' => new DataObject(['list' => $paymentList])];
        $this->eventManager->dispatch(
            self::EVENT_WEBPOS_GET_PAYMENT_AFTER,
            $data
        );
        $paymentList = $data['payments']->getList();
        $payments = $this->paymentResultInterface;
        $payments->setItems($paymentList);
        $payments->setTotalCount(count($paymentList));
        return $payments;
    }
}
