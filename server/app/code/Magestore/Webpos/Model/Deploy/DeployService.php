<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Deploy;

use Composer\Util\Filesystem;
use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Module\Dir\Reader;
use Magestore\Webpos\Api\Console\WebposDeployInterface;
use Psr\Log\LoggerInterface;

/**
 * Deploy webpos Service
 */
class DeployService implements WebposDeployInterface
{
    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $_dir;
    /**
     * @var Reader
     */
    protected $reader;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;
    /**
     * @var DriverInterface
     */
    protected $driverInterface;
    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $symfonyFileSystem;
    /**
     * @var Filesystem
     */
    protected $composerFileSystem;

    /**
     * DeployService constructor.
     *
     * @param \Magento\Framework\Filesystem\DirectoryList $dir
     * @param Reader $reader
     * @param \Magento\Framework\Filesystem $filesystem
     */
    public function __construct(
        \Magento\Framework\Filesystem\DirectoryList $dir,
        Reader $reader,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->_dir = $dir;
        $this->reader = $reader;
        $this->filesystem = $filesystem;
    }

    /**
     * Execute
     *
     * @return bool
     * @throws Exception
     */
    public function execute()
    {
        /** Deploy webapp */
        try {
            $sourceDir = $this->reader->getModuleDir('', 'Magestore_Webpos') . '/build/apps';
            if ($this->getDriverInterface()->isDirectory($sourceDir)) {
                //we will update/save source of this lib at the folder which includes media folder
                $mediaDir = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath();
                $toolDir = $this->getDriverInterface()->getParentDirectory($mediaDir) . '/apps/';
                //delete old source of tool
                $this->rrmdir($toolDir);
                //copy new source of this tool
                $this->xcopy($sourceDir, $toolDir, 0775);
            }
        } catch (Exception $e) {
            ObjectManager::getInstance()
                ->get(LoggerInterface::class)
                ->info($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Webpos Deploy
     *
     * @param string $deployName
     * @param string $output
     * @return null|string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function webposDeploy($deployName, $output)
    {
        try {
            $startTime = microtime(true);
            $result = $this->execute();
            $resultTime = microtime(true) - $startTime;
            $resultTime = round($resultTime, 2) . 's';
            $messageSuccess = $result ? "Webpos Deploy Successfully Completed!!" : "";
            $output->writeln($messageSuccess);
            $output->writeln('Execution time : ' . $resultTime);
            return null;
        } catch (LocalizedException $e) {
            return ($e->getMessage());
        } catch (Exception $e) {
            return ($e->getMessage());
        }
    }

    /**
     * Xcopy
     *
     * @param string $source
     * @param string $dest
     * @param int $permissions
     * @return bool
     */
    public function xcopy($source, $dest, $permissions = 0755)
    {
        // Check for symlinks
        if (is_link($source)) { // phpcs:ignore
            return $this->getDriverInterface()->symlink(
                $this->getSymfonyFileSystem()->readlink($source),
                $dest
            );
        }
        // Simple copy for a file
        if ($this->getDriverInterface()->isFile($source)) {
            return $this->getDriverInterface()->copy($source, $dest);
        }
        // Make destination directory
        if (!$this->getDriverInterface()->isDirectory($dest)) {
            $this->getDriverInterface()->createDirectory($dest, $permissions);
        }
        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            // Deep copy directories
            $this->xcopy("$source/$entry", "$dest/$entry", $permissions);
        }
        // Clean up
        $dir->close();
        return true;
    }

    /**
     * Rrm dir
     *
     * @param string $dir
     */
    public function rrmdir($dir)
    {
        if ($this->getDriverInterface()->isDirectory($dir)) {
            $objects = scandir($dir); // phpcs:ignore
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if ($this->getDriverInterface()->isDirectory($dir . "/" . $object)) {
                        $this->rrmdir($dir . "/" . $object);
                    } else {
                        $this->getComposerFileSystem()->unlink($dir . "/" . $object);
                    }
                }
            }
            $this->getDriverInterface()->deleteDirectory($dir);
        }
    }

    /**
     * Get driver interface
     *
     * @return DriverInterface
     */
    public function getDriverInterface()
    {
        if (!$this->driverInterface) {
            $objectManager = ObjectManager::getInstance();
            $this->driverInterface = $objectManager->get(DriverInterface::class);
        }
        return $this->driverInterface;
    }

    /**
     * Get symfony file system
     *
     * @return \Symfony\Component\Filesystem\Filesystem
     */
    public function getSymfonyFileSystem()
    {
        if (!$this->symfonyFileSystem) {
            $this->symfonyFileSystem = ObjectManager::getInstance()
                ->get(\Symfony\Component\Filesystem\Filesystem::class);
        }
        return $this->symfonyFileSystem;
    }

    /**
     * Get composer file system
     *
     * @return Filesystem
     */
    public function getComposerFileSystem()
    {
        if (!$this->composerFileSystem) {
            $this->composerFileSystem = ObjectManager::getInstance()
                ->get(Filesystem::class);
        }
        return $this->composerFileSystem;
    }
}
