<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Log;

use Exception;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magestore\Webpos\Api\Data\Log\OrderDeletedInterface;
use Magestore\Webpos\Api\Log\OrderDeletedRepositoryInterface;

/**
 * Log - OrderDeletedRepository
 */
class OrderDeletedRepository implements OrderDeletedRepositoryInterface
{
    /**
     * @var OrderDeletedInterface
     */
    protected $orderDeleted;
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Log\OrderDeleted
     */
    protected $orderDeletedResource;

    /**
     * OrderDeletedRepository constructor.
     * @param OrderDeletedInterface $orderDeleted
     * @param \Magestore\Webpos\Model\ResourceModel\Log\OrderDeleted $orderDeletedResource
     */
    public function __construct(
        OrderDeletedInterface $orderDeleted,
        \Magestore\Webpos\Model\ResourceModel\Log\OrderDeleted $orderDeletedResource
    ) {
        $this->orderDeleted = $orderDeleted;
        $this->orderDeletedResource = $orderDeletedResource;
    }
    /**
     * @inheritDoc
     */
    public function save(OrderDeletedInterface $orderDeleted)
    {
        try {
            $this->orderDeletedResource->save($orderDeleted);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Unable to save order deleted'));
        }
        return $orderDeleted;
    }

    /**
     * @inheritDoc
     */
    public function delete(OrderDeletedInterface $orderDeleted)
    {
        $this->deleteById($orderDeleted->getId());
    }

    /**
     * @inheritDoc
     */
    public function deleteById($orderId)
    {
        try {
            $orderDeleted = $this->getById($orderId);
            $this->orderDeletedResource->delete($orderDeleted);
        } catch (Exception $e) {
            throw new CouldNotDeleteException(__('Unable to delete order deleted'));
        }
    }
    /**
     * @inheritDoc
     */
    public function getById($orderId)
    {
        $orderDeleted = $this->orderDeleted;
        $this->orderDeletedResource->load($orderDeleted, $orderId);
        if (!$orderDeleted->getId()) {
            throw new NoSuchEntityException(__('Order with id "%1" does not exist.', $orderId));
        } else {
            return $orderDeleted;
        }
    }
}
