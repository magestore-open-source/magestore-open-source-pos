<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Log;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface;
use Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted\Collection;

/**
 * Log - CustomerDeleted
 */
class CustomerDeleted extends AbstractModel implements CustomerDeletedInterface
{
    /**
     * OrderDeleted constructor.
     * @param Context $context
     * @param Registry $registry
     * @param \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct( // phpcs:ignore
        Context $context,
        Registry $registry,
        \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }
    /**
     * Set Id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get customer id
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }
    /**
     * Set customer id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }
    /**
     * Get deleted at
     *
     * @return string
     */
    public function getDeletedAt()
    {
        return $this->getData(self::DELETED_AT);
    }
    /**
     * Set deleted at
     *
     * @param string $deletedAt
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        return $this->setData(self::DELETED_AT, $deletedAt);
    }
}
