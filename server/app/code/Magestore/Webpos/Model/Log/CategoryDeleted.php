<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Log;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface;
use Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted\Collection;

/**
 * Log - CategoryDeleted
 */
class CategoryDeleted extends AbstractModel implements CategoryDeletedInterface
{
    /**
     * OrderDeleted constructor.
     * @param Context $context
     * @param Registry $registry
     * @param \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct( // phpcs:ignore
        Context $context,
        Registry $registry,
        \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }
    /**
     * @inheritdoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritdoc
     */
    public function getCategoryId()
    {
        return $this->getData(self::CATEGORY_ID);
    }
    /**
     * @inheritdoc
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }

    /**
     * @inheritdoc
     */
    public function getRootCategoryId()
    {
        return $this->getData(self::ROOT_CATEGORY_ID);
    }
    /**
     * @inheritdoc
     */
    public function setRootCategoryId($rootCategoryId)
    {
        return $this->setData(self::ROOT_CATEGORY_ID, $rootCategoryId);
    }
    /**
     * @inheritdoc
     */
    public function getDeletedAt()
    {
        return $this->getData(self::DELETED_AT);
    }
    /**
     * @inheritdoc
     */
    public function setDeletedAt($deletedAt)
    {
        return $this->setData(self::DELETED_AT, $deletedAt);
    }
}
