<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Log;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magestore\Webpos\Api\Data\Log\DataLogResultsInterface;
use Magestore\Webpos\Api\Log\DataLogRepositoryInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Model\ResourceModel\Log\OrderDeleted\CollectionFactory;

/**
 * Model log DataLogRepository
 */
class DataLogRepository implements DataLogRepositoryInterface
{
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Log\ProductDeleted\CollectionFactory
     */
    protected $productDeletedCollection;

    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted\CollectionFactory
     */
    protected $customerDeletedCollection;
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted\CollectionFactory
     */
    protected $categoryDeletedCollection;

    /**
     * @var CollectionFactory
     */
    protected $orderDeletedCollection;

    /**
     * @var DataLogResultsInterface
     */
    protected $dataLogResults;

    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * DataLogRepository constructor.
     *
     * @param \Magestore\Webpos\Model\ResourceModel\Log\ProductDeleted\CollectionFactory $productDeletedCollection
     * @param \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted\CollectionFactory $customerDeletedCollection
     * @param \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted\CollectionFactory $categoryDeletedCollection
     * @param CollectionFactory $orderDeletedCollection
     * @param DataLogResultsInterface $dataLogResults
     * @param StockManagementInterface $stockManagement
     */
    public function __construct(
        \Magestore\Webpos\Model\ResourceModel\Log\ProductDeleted\CollectionFactory $productDeletedCollection,
        \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted\CollectionFactory $customerDeletedCollection,
        \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted\CollectionFactory $categoryDeletedCollection,
        CollectionFactory $orderDeletedCollection,
        DataLogResultsInterface $dataLogResults,
        StockManagementInterface $stockManagement
    ) {
        $this->customerDeletedCollection = $customerDeletedCollection;
        $this->categoryDeletedCollection = $categoryDeletedCollection;
        $this->productDeletedCollection = $productDeletedCollection;
        $this->orderDeletedCollection = $orderDeletedCollection;
        $this->dataLogResults = $dataLogResults;
        $this->stockManagement = $stockManagement;
    }

    /**
     * Retrieve data matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return DataLogResultsInterface
     */
    public function getListCustomer(SearchCriteriaInterface $searchCriteria)
    {
        $customerDeletedCollection = $this->customerDeletedCollection->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                if ($filter->getField() == 'updated_at') {
                    $customerDeletedCollection->addFieldToFilter('deleted_at', [$condition => $filter->getValue()]);
                }
            }
        }

        $customerDeletedIds = [];
        foreach ($customerDeletedCollection as $customer) {
            $customerDeletedIds[] = (int)$customer->getCustomerId();
        }
        $this->dataLogResults->setIds($customerDeletedIds);
        return $this->dataLogResults;
    }

    /**
     * @inheritdoc
     */
    public function getListCategory(SearchCriteriaInterface $searchCriteria)
    {
        $categoryDeletedCollection = $this->categoryDeletedCollection->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                if ($filter->getField() == 'updated_at') {
                    $categoryDeletedCollection->addFieldToFilter('deleted_at', [$condition => $filter->getValue()]);
                } elseif ($filter->getField() == 'root_category_id') {
                    $categoryDeletedCollection->addFieldToFilter(
                        'root_category_id',
                        [$condition => $filter->getValue()]
                    );
                }
            }
        }

        $categoryDeletedIds = [];
        foreach ($categoryDeletedCollection as $category) {
            $categoryDeletedIds[] = (int)$category->getCategoryId();
        }
        $this->dataLogResults->setIds($categoryDeletedIds);
        return $this->dataLogResults;
    }

    /**
     * Retrieve data matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return DataLogResultsInterface
     */
    public function getListProduct(SearchCriteriaInterface $searchCriteria)
    {
        $productDeletedCollection = $this->productDeletedCollection->create();
        $this->filterByStockId($productDeletedCollection);
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                if ($filter->getField() == 'updated_at') {
                    $productDeletedCollection->addFieldToFilter('deleted_at', [$condition => $filter->getValue()]);
                }
            }
        }

        $productDeletedIds = [];
        foreach ($productDeletedCollection as $product) {
            $productDeletedIds[] = (int)$product->getProductId();
        }
        $this->dataLogResults->setIds($productDeletedIds);
        return $this->dataLogResults;
    }

    /**
     * Retrieve data matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return DataLogResultsInterface
     */
    public function getListOrder(SearchCriteriaInterface $searchCriteria)
    {
        $orderDeletedCollection = $this->orderDeletedCollection->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                if ($filter->getField() == 'updated_at') {
                    $orderDeletedCollection->addFieldToFilter('deleted_at', [$condition => $filter->getValue()]);
                }
            }
        }

        $orderDeletedIds = [];
        foreach ($orderDeletedCollection as $order) {
            $orderDeletedIds[] = $order->getOrderIncrementId();
        }
        $this->dataLogResults->setIds($orderDeletedIds);
        return $this->dataLogResults;
    }

    /**
     * Filter by stock
     *
     * @param AbstractCollection $collection
     * @return AbstractCollection
     */
    public function filterByStockId(AbstractCollection $collection)
    {
        if ($stockId = $this->stockManagement->getStockId()) {
            $collection->addFieldToFilter('stock_id', $stockId);
        }
        return $collection;
    }
}
