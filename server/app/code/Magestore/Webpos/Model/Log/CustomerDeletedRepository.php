<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Log;

use Exception;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magestore\Webpos\Api\Data\Log\CustomerDeletedInterface;
use Magestore\Webpos\Api\Log\CustomerDeletedRepositoryInterface;

/**
 * Log - CustomerDeletedRepository
 */
class CustomerDeletedRepository implements CustomerDeletedRepositoryInterface
{
    /**
     * @var CustomerDeletedInterface
     */
    protected $customerDeleted;
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted
     */
    protected $customerDeletedResource;

    /**
     * CustomerDeletedRepository constructor.
     * @param CustomerDeletedInterface $customerDeleted
     * @param \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted $customerDeletedResource
     */
    public function __construct(
        CustomerDeletedInterface $customerDeleted,
        \Magestore\Webpos\Model\ResourceModel\Log\CustomerDeleted $customerDeletedResource
    ) {
        $this->customerDeleted = $customerDeleted;
        $this->customerDeletedResource = $customerDeletedResource;
    }

    /**
     * @inheritDoc
     */
    public function save(CustomerDeletedInterface $customerDeleted)
    {
        try {
            $this->customerDeletedResource->save($customerDeleted);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Unable to save customer deleted'));
        }
        return $customerDeleted;
    }

    /**
     * @inheritDoc
     */
    public function delete(CustomerDeletedInterface $customerDeleted)
    {
        $this->deleteById($customerDeleted->getId());
    }

    /**
     * @inheritDoc
     */
    public function deleteById($customerId)
    {
        try {
            $customerDeleted = $this->getById($customerId);
            $this->customerDeletedResource->delete($customerDeleted);
        } catch (Exception $e) {
            throw new CouldNotDeleteException(__('Unable to delete customer deleted'));
        }
    }
    /**
     * @inheritDoc
     */
    public function getById($customerId)
    {
        $customerDeleted = $this->customerDeleted;
        $this->customerDeletedResource->load($customerDeleted, $customerId);
        if (!$customerDeleted->getId()) {
            throw new NoSuchEntityException(__('Customer with id "%1" does not exist.', $customerId));
        } else {
            return $customerDeleted;
        }
    }
}
