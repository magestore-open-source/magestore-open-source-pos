<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Log;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magestore\Webpos\Api\Data\Location\LocationInterface;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Bundle\Model\Product\Type as Bundle;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\InventorySalesAdminUi\Model\ResourceModel\GetAssignedStockIdsBySku;
use Magestore\Webpos\Api\Data\Log\ProductDeletedInterface;
use Magestore\Webpos\Api\Log\ProductDeletedRepositoryInterface;
use Magestore\Webpos\Api\WebposManagementInterface;
use Magestore\Webpos\Model\ResourceModel\Location\Location\Collection;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory;

/**
 * Repository delete products
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductDeletedRepository implements ProductDeletedRepositoryInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * @var ProductDeletedInterface
     */
    protected $productDeleted;
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Log\ProductDeleted
     */
    protected $productDeletedResource;

    /**
     * @var WebposManagementInterface
     */
    protected $webposManagement;

    /**
     * @var CollectionFactory
     */
    protected $locationCollectionFactory;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * ProductDeletedRepository constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param ProductDeletedInterface $productDeleted
     * @param \Magestore\Webpos\Model\ResourceModel\Log\ProductDeleted $productDeletedResource
     * @param WebposManagementInterface $webposManagement
     * @param CollectionFactory $locationCollectionFactory
     * @param ResourceConnection $resource
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ProductDeletedInterface $productDeleted,
        \Magestore\Webpos\Model\ResourceModel\Log\ProductDeleted $productDeletedResource,
        WebposManagementInterface $webposManagement,
        CollectionFactory $locationCollectionFactory,
        ResourceConnection $resource
    ) {
        $this->objectManager = $objectManager;
        $this->productDeleted = $productDeleted;
        $this->productDeletedResource = $productDeletedResource;
        $this->webposManagement = $webposManagement;
        $this->locationCollectionFactory = $locationCollectionFactory;
        $this->resource = $resource;
    }

    /**
     * Insert new product to webpos_product_delete table
     *
     * @param int $productId
     * @return void
     */
    public function insertByProductId($productId)
    {
        if ($this->webposManagement->isMSIEnable()) {
            /** @var Collection $collection */
            $collection = $this->locationCollectionFactory->create();
            $collection->addFieldToFilter(LocationInterface::STOCK_ID, ['gt' => 0]);
            $collection->getSelect()->group(LocationInterface::STOCK_ID);
            $stockIds = $collection->getColumnValues(LocationInterface::STOCK_ID);
            if (!empty($stockIds)) {
                $this->productDeletedResource->deleteByProductId($productId);
                $this->productDeletedResource->insertMultiple($productId, $stockIds);
            }
        } else {
            $this->productDeletedResource->deleteByProductId($productId);
            $this->productDeletedResource->insertMultiple($productId);
        }
    }

    /**
     * Delete product from webpos_product_delete table by id
     *
     * @param int $productId
     * @return void
     * @throws LocalizedException
     */
    public function deleteByProductId($productId)
    {
        try {
            $this->productDeletedResource->deleteByProductId($productId);
        } catch (Exception $e) {
            throw new CouldNotDeleteException(__('Unable to delete product deleted'));
        }
    }

    /**
     * Delete product from webpos_product_delete table by product model
     *
     * @param ProductInterface $product
     * @return void
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * phpcs:disable Generic.Metrics.NestingLevel
     */
    public function deleteByProduct($product)
    {
        try {
            if ($product->getTypeId() != Grouped::TYPE_CODE
                && $product->getTypeId() != Bundle::TYPE_CODE
                && $product->getTypeId() != Configurable::TYPE_CODE) {
                if ($this->webposManagement->isMSIEnable()) {
                    /** @var Collection $collection */
                    $collection = $this->locationCollectionFactory->create();
                    $collection->addFieldToFilter(LocationInterface::STOCK_ID, ['gt' => 0]);
                    $collection->getSelect()->group(LocationInterface::STOCK_ID);
                    $stockIds = $collection->getColumnValues(LocationInterface::STOCK_ID);
                    if (!empty($stockIds)) {
                        /** @var GetAssignedStockIdsBySku $getAssignedStockIdsBySku */
                        $getAssignedStockIdsBySku = $this->objectManager
                            ->get(GetAssignedStockIdsBySku::class);
                        $assignedStockIds = $getAssignedStockIdsBySku->execute($product->getSku());
                        $inStockIds = $notInStockIds = [];
                        foreach ($stockIds as $stockId) {
                            if (in_array($stockId, $assignedStockIds)) {
                                $inStockIds[] = $stockId;
                            } else {
                                $notInStockIds[] = $stockId;
                            }
                        }
                        if (!empty($inStockIds) || !empty($notInStockIds)) {
                            $this->productDeletedResource->deleteByProductId(
                                $product->getId(),
                                array_merge($inStockIds, $notInStockIds)
                            );
                        }
                        if (!empty($notInStockIds)) {
                            $this->productDeletedResource->insertMultiple($product->getId(), $notInStockIds);
                        }
                    }
                } else {
                    $this->productDeletedResource->deleteByProductId($product->getId());
                }
            } else {
                $this->productDeletedResource->deleteByProductId($product->getId());
            }
        } catch (Exception $e) {
            throw new CouldNotDeleteException(__('Unable to delete product deleted'));
        }
    }

    /**
     * @inheritDoc
     */
    public function insertByProductIdAndStock($productId, $stockIds = [])
    {
        if (!count($stockIds)) {
            $this->deleteByProductId($productId);
        } else {
            /** @var Collection $collection */
            $collection = $this->locationCollectionFactory->create();
            $collection->addFieldToFilter(LocationInterface::STOCK_ID, ['in' => $stockIds]);
            $collection->getSelect()->group(LocationInterface::STOCK_ID);
            $stockIds = $collection->getColumnValues(LocationInterface::STOCK_ID);
            if (!empty($stockIds)) {
                $this->productDeletedResource->deleteByProductId($productId, $stockIds);
                $this->productDeletedResource->insertMultiple($productId, $stockIds);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function deleteByProductIdAndStock($productId, $stockIds = [])
    {
        if (!count($stockIds)) {
            $this->deleteByProductId($productId);
        } else {
            /** @var Collection $collection */
            $collection = $this->locationCollectionFactory->create();
            $collection->addFieldToFilter(LocationInterface::STOCK_ID, ['in' => $stockIds]);
            $collection->getSelect()->group(LocationInterface::STOCK_ID);
            $stockIds = $collection->getColumnValues(LocationInterface::STOCK_ID);
            if (!empty($stockIds)) {
                $this->productDeletedResource->deleteByProductId($productId, $stockIds);
            }
        }
    }
}
