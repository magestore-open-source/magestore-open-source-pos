<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Log;

use Exception;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magestore\Webpos\Api\Data\Log\CategoryDeletedInterface;
use Magestore\Webpos\Api\Log\CategoryDeletedRepositoryInterface;

/**
 * Log - CategoryDeletedRepository
 */
class CategoryDeletedRepository implements CategoryDeletedRepositoryInterface
{
    /**
     * @var CategoryDeletedInterface
     */
    protected $categoryDeleted;
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted
     */
    protected $categoryDeletedResource;

    /**
     * CategoryDeletedRepository constructor.
     * @param CategoryDeletedInterface $categoryDeleted
     * @param \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted $categoryDeletedResource
     */
    public function __construct(
        CategoryDeletedInterface $categoryDeleted,
        \Magestore\Webpos\Model\ResourceModel\Log\CategoryDeleted $categoryDeletedResource
    ) {
        $this->categoryDeleted = $categoryDeleted;
        $this->categoryDeletedResource = $categoryDeletedResource;
    }

    /**
     * @inheritDoc
     */
    public function save(CategoryDeletedInterface $categoryDeleted)
    {
        try {
            $this->categoryDeletedResource->save($categoryDeleted);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Unable to save category deleted'));
        }
        return $categoryDeleted;
    }

    /**
     * @inheritDoc
     */
    public function delete(CategoryDeletedInterface $categoryDeleted)
    {
        return $this->deleteById($categoryDeleted->getId());
    }

    /**
     * @inheritDoc
     */
    public function deleteById($categoryId)
    {
        try {
            $categoryDeleted = $this->getById($categoryId);
            $this->categoryDeletedResource->delete($categoryDeleted);
            return true;
        } catch (Exception $e) {
            throw new CouldNotDeleteException(__('Unable to delete category deleted'));
        }
    }
    /**
     * @inheritDoc
     */
    public function getById($categoryId)
    {
        $categoryDeleted = $this->categoryDeleted;
        $this->categoryDeletedResource->load($categoryDeleted, $categoryId);
        if (!$categoryDeleted->getId()) {
            throw new NoSuchEntityException(__('Category with id "%1" does not exist.', $categoryId));
        } else {
            return $categoryDeleted;
        }
    }
}
