<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\QuoteRepository\Plugin;

use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;
use Magento\Framework\Exception\StateException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Framework\Webapi\Rest\Request as RestRequest;
use Magestore\Webpos\Api\DataProvider\GetCurrentStaffIdBySessionInterface;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;

/**
 * The plugin checks if the user has ability to change the quote.
 *
 * Class AccessChangeQuoteControl
 */
class AccessChangeQuoteControl
{
    /**
     * @var UserContextInterface
     */
    private $userContext;

    /**
     * @var RestRequest
     */
    protected $apiRequest;

    /**
     * @var GetCurrentStaffIdBySessionInterface
     */
    protected $getCurrentStaffIdBySession;

    /**
     * AccessChangeQuoteControl constructor.
     *
     * @param UserContextInterface $userContext
     * @param RestRequest $apiRequest
     * @param GetCurrentStaffIdBySessionInterface $getCurrentStaffIdBySession
     */
    public function __construct(
        UserContextInterface $userContext,
        RestRequest $apiRequest,
        GetCurrentStaffIdBySessionInterface $getCurrentStaffIdBySession
    ) {
        $this->userContext = $userContext;
        $this->apiRequest = $apiRequest;
        $this->getCurrentStaffIdBySession = $getCurrentStaffIdBySession;
    }

    /**
     * Rewrite function
     *
     * @param CartRepositoryInterface $subject
     * @param CartInterface $quote
     * @return array
     * @throws StateException
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSave(CartRepositoryInterface $subject, CartInterface $quote)
    {
        $sessionId = $this->apiRequest->getParam(
            RequestProcessor::SESSION_PARAM_KEY
        );
        if ($sessionId) {
            $currentStaff = $this->getCurrentStaffIdBySession->execute($sessionId);
            if ($currentStaff) {
                return [$quote];
            }
        }

        if (!$this->isAllowed($quote)) {
            throw new StateException(__("Invalid state change requested"));
        }
        return [$quote];
    }

    /**
     * Checks if user is allowed to change the quote.
     *
     * @param Quote $quote
     * @return bool
     */
    private function isAllowed(Quote $quote)
    {
        switch ($this->userContext->getUserType()) {
            case UserContextInterface::USER_TYPE_CUSTOMER:
                $isAllowed = ($quote->getCustomerId() == $this->userContext->getUserId());
                break;
            case UserContextInterface::USER_TYPE_GUEST:
                $isAllowed = ($quote->getCustomerId() === null);
                break;
            case UserContextInterface::USER_TYPE_ADMIN:
            case UserContextInterface::USER_TYPE_INTEGRATION:
                $isAllowed = true;
                break;
            default:
                $isAllowed = false;
        }

        return $isAllowed;
    }
}
