<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Framework\Option\ArrayInterface;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;

/**
 * Source option Staff
 */
class Staff implements ArrayInterface
{
    /**
     * @var StaffRepositoryInterface
     */
    public $staffRepository;

    /**
     * Pos constructor.
     *
     * @param PosRepositoryInterface $staffRepository
     */
    public function __construct(
        StaffRepositoryInterface $staffRepository
    ) {
        $this->staffRepository = $staffRepository;
    }

    /**
     * To option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $allStaff = $this->staffRepository->getAllStaff();
        $allStaffArray = [];
        foreach ($allStaff as $staff) {
            $allStaffArray[] = ['label' => $staff->getUsername(), 'value' => $staff->getId()];
        }
        return $allStaffArray;
    }

    /**
     * Get options array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $allStaff = $this->staffRepository->getAllStaff();
        $allStaffArray = [];
        foreach ($allStaff as $staff) {
            $allStaffArray[$staff->getId()] = $staff->getUsername();
        }
        return $allStaffArray;
    }

    /**
     * OptionArrayToShow
     *
     * @return array
     */
    public function optionArrayToShow()
    {
        $options = [];
        $options[] = ['value' => null, 'label' => ' '];
        $optionsArr = $this->toOptionArray();
        $options = array_merge($options, $optionsArr);
        return $options;
    }
}
