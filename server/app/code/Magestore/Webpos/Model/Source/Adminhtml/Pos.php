<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Framework\Option\ArrayInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;

/**
 * Source - Pos
 */
class Pos implements ArrayInterface
{
    /**
     * @var PosRepositoryInterface
     */
    public $posRepository;

    /**
     * Pos constructor.
     * @param PosRepositoryInterface $posRepository
     */
    public function __construct(
        PosRepositoryInterface $posRepository
    ) {
        $this->posRepository = $posRepository;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        $allPos = $this->posRepository->getAllPos();
        $allPosArray = [];
        foreach ($allPos as $pos) {
            $allPosArray[] = ['label' => $pos->getPosName(), 'value' => $pos->getPosId()];
        }
        return $allPosArray;
    }

    /**
     * Get Option Array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $allPos = $this->posRepository->getAllPos();
        $allPosArray = [];
        foreach ($allPos as $pos) {
            $allPosArray[$pos->getPosId()] = $pos->getPosName();
        }
        return $allPosArray;
    }
}
