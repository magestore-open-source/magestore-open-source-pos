<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Source\Adminhtml\Performance;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Source - Performance Mode
 */
class Mode implements OptionSourceInterface
{

    const ONLINE_MODE = 0;
    const OFFLINE_MODE = 1;

    /**
     * @var array
     */
    protected $_options;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->_options = [
            ['value' => self::ONLINE_MODE, 'label' => __('Online')],
            ['value' => self::OFFLINE_MODE, 'label' => __('Offline')],
        ];
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return $this->_options;
    }
}
