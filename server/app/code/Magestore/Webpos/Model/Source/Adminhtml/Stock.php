<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Option\ArrayInterface;
use Magento\Inventory\Model\ResourceModel\Stock\Collection;
use Magento\InventoryCatalogApi\Api\DefaultStockProviderInterface;
use Magestore\Webpos\Api\WebposManagementInterface;

/**
 * Source - Stock
 */
class Stock implements ArrayInterface
{
    protected $options;

    /**
     * @var WebposManagementInterface
     */
    protected $webposManagement;

    /**
     * Stock constructor.
     * @param WebposManagementInterface $webposManagement
     */
    public function __construct(
        WebposManagementInterface $webposManagement
    ) {
        $this->webposManagement = $webposManagement;
    }

    /**
     * Return options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $isMSIEnable = $this->webposManagement->isMSIEnable();
        if (!$isMSIEnable) {
            return [];
        }
        if (!$this->options) {
            $this->options = [['value' => '', 'label' => __('--Please Select--')]];
            $objectManager = ObjectManager::getInstance();
            /** @var AbstractCollection $stockCollection */
            $stockCollection = $objectManager->create(Collection::class);
            $stockCollection->getSelect()->joinInner(
                ['inventory_source_stock_link' => $stockCollection->getTable('inventory_source_stock_link')],
                'main_table.stock_id = inventory_source_stock_link.stock_id',
                []
            )->columns([
                'stock_id' => 'main_table.stock_id',
                'name' => 'main_table.name'
            ])->group(
                'main_table.stock_id'
            )->having('COUNT(inventory_source_stock_link.source_code) = 1');
            if ($this->webposManagement->isWebposStandard()) {
                /** @var DefaultStockProviderInterface $defaultStockProvider */
                $defaultStockProvider = $objectManager
                    ->create(DefaultStockProviderInterface::class);
                $defaultStockId = $defaultStockProvider->getId();
                $stockCollection->getSelect()->having('main_table.stock_id = ?', $defaultStockId);
            }
            foreach ($stockCollection as $item) {
                $this->options[] = ['value' => $item->getStockId(), 'label' => $item->getName()];
            }
        }

        return $this->options;
    }
}
