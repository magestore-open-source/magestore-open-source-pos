<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Model\Source\Adminhtml\Payment;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Method
 *
 * Used to get payment method options source
 */
class Method implements OptionSourceInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var array
     */
    protected $mappingPaymentConfigurationAndPaymentCode;

    /**
     * @var array
     */
    protected $methodWithoutConfig;

    /**
     * Method constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param array $mappingPaymentConfigurationAndPaymentCode
     * @param array $methodWithoutConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        array $mappingPaymentConfigurationAndPaymentCode = [],
        array $methodWithoutConfig = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->mappingPaymentConfigurationAndPaymentCode = $mappingPaymentConfigurationAndPaymentCode;
        $this->methodWithoutConfig = $methodWithoutConfig;
    }

    /**
     * To option array label
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        $options = $this->getOptionHash();
        foreach ($options as $code => $title) {
            $result[] = [
                'value' => $code,
                'label' => $title
            ];
        }
        return $result;
    }

    /**
     * Get all options hash
     *
     * @return array
     */
    public function getOptionHash()
    {
        $options = [];
        $mapping = $this->mappingPaymentConfigurationAndPaymentCode;
        $paymentMethods = $this->scopeConfig->getValue('webpos/payment');
        foreach ($paymentMethods as $code => $paymentMethod) {
            if (isset($mapping[$code])) {
                $code = $mapping[$code];
            }
            if (is_array($paymentMethod) && isset($paymentMethod['title'])) {
                $options[$code] = $paymentMethod['title'];
            }
        }
        foreach ($this->methodWithoutConfig as $code => $title) {
            $options[$code] = __($title);
        }
        return $options;
    }
}
