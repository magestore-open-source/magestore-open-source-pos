<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Option\ArrayInterface;
use Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate;
use Magento\Shipping\Model\Config;
use Magento\Store\Model\ScopeInterface;
use Magestore\Webpos\Api\Data\Shipping\ShippingMethodInterfaceFactory;

/**
 * Source Shipping
 */
class Shipping implements ArrayInterface
{
    const SPECIFIC_SHIPPING = 'webpos/shipping/method';

    /**
     * @var array
     */
    protected $allowShippings = [];

    /**
     * @var ShippingMethodInterfaceFactory
     */
    protected $shippingMethodFactory;

    /**
     * @var Config
     */
    protected $shippingConfigModel;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Shipping constructor.
     *
     * @param ShippingMethodInterfaceFactory $shippingModel
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $shippingConfigModel
     */
    public function __construct(
        ShippingMethodInterfaceFactory $shippingModel,
        ScopeConfigInterface $scopeConfig,
        Config $shippingConfigModel
    ) {
        $this->shippingMethodFactory = $shippingModel;
        $this->shippingConfigModel = $shippingConfigModel;
        $this->scopeConfig = $scopeConfig;
        $this->allowShippings = ['webpos_shipping', 'flatrate', 'freeshipping', 'tablerate'];
    }

    /**
     * To Option Array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collection = $this->getListCoreShippingMethods();
        $options = [];
        if (count($collection) > 0) {
            foreach ($collection as $code => $carrier) {
                if ($code == 'webpos_shipping') {
                    continue;
                }
                $title = $carrier->getConfigData('title') . ' - ' . $carrier->getConfigData('name');
                $options[] = ['value' => $code, 'label' => $title];
            }
        }
        return $options;
    }

    /**
     * Get shipping methods for pos
     *
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getPosShippingMethods()
    {
        $collection = $this->getListCoreShippingMethods();
        $shippingList = [];
        if (count($collection) > 0) {
            foreach ($collection as $code => $carrier) {
                if (!$this->isAllowOnWebPOS($code)) {
                    continue;
                }
                $shippingModel = $this->shippingMethodFactory->create();
                $isDefault = 0;
                if ($code == $this->getDefaultShippingMethod()) {
                    $isDefault = 1;
                }
                $method_code = $code;
                if ($code == 'webpos_shipping') {
                    $method_code = 'storepickup';
                }
                $methodCode = $code . '_' . $method_code;
                $methodTitle = $carrier->getConfigData('title') . ' - ' . $carrier->getConfigData('name');
                $methodPrice = ($carrier->getConfigData('price') != null) ? (float)$carrier->getConfigData('price') : 0;
                $methodType = ($carrier->getConfigData('type') != null) ? $carrier->getConfigData('type') : '';
                $shipmentRequestType = $carrier->getConfigData('shipment_requesttype')
                    ? $carrier->getConfigData('shipment_requesttype')
                    : 0;
                $conditionName = $carrier->getConfigData('condition_name') != null
                    ? $carrier->getConfigData('condition_name')
                    : '';
                $methodDescription = $carrier->getConfigData('description') != null
                    ? $carrier->getConfigData('description')
                    : '';
                $speCountriesAllow = $carrier->getConfigData('sallowspecific')
                    ? $carrier->getConfigData('sallowspecific')
                    : 0;
                $methodSpecificerrmsg = $carrier->getConfigData('specificerrmsg') != null
                    ? $carrier->getConfigData('specificerrmsg')
                    : '';
                $specificCountry = $carrier->getConfigData('specificcountry')
                    ? $carrier->getConfigData('specificcountry')
                    : '';
                $handlingFee = $carrier->getConfigData('handling_fee')
                    ? (float)$carrier->getConfigData('handling_fee')
                    : 0;
                $handlingType = $carrier->getConfigData('handling_type')
                    ? $carrier->getConfigData('handling_type')
                    : 'F';
                $handlingAction = $carrier->getConfigData('handling_action')
                    ? $carrier->getConfigData('handling_action')
                    : 'O';
                $maxPackageWeight = $carrier->getConfigData('max_package_weight')
                    ? $carrier->getConfigData('max_package_weight')
                    : 0;
                $includeVirtualPrice = $carrier->getConfigData('include_virtual_price')
                    ? $carrier->getConfigData('include_virtual_price')
                    : 0;
                $freeShippingSubtotal = $carrier->getConfigData('free_shipping_subtotal')
                    ? $carrier->getConfigData('free_shipping_subtotal')
                    : 0;
                $rates = [];
                if ($code == 'tablerate') {
                    /** @var Tablerate $resourceModel */
                    $resourceModel = ObjectManager::getInstance()
                        ->create(Tablerate::class);
                    $connection = $resourceModel->getConnection();
                    $select = $connection->select()->from($resourceModel->getMainTable());
                    $result = $connection->fetchAll($select);
                    if (!empty($result)) {
                        $rates = $result;
                    }
                }
                $shippingModel->setCode($methodCode);
                $shippingModel->setTitle($methodTitle);
                $shippingModel->setPrice($methodPrice);
                $shippingModel->setType($methodType);
                $shippingModel->setShipmentRequestType($shipmentRequestType);
                $shippingModel->setConditionName($conditionName);
                $shippingModel->setDescription($methodDescription);
                $shippingModel->setErrorMessage($methodSpecificerrmsg);
                $shippingModel->setIsDefault($isDefault);
                $shippingModel->setSpecificCountriesAllow($speCountriesAllow);
                $shippingModel->setSpecificCountry($specificCountry);
                $shippingModel->setHandlingFee($handlingFee);
                $shippingModel->setHandlingType($handlingType);
                $shippingModel->setHandlingAction($handlingAction);
                $shippingModel->setMaxPackageWeight($maxPackageWeight);
                $shippingModel->setIncludeVirtualPrice($includeVirtualPrice);
                $shippingModel->setFreeShippingSubtotal($freeShippingSubtotal);
                $shippingModel->setRates($rates);
                $shippingList[] = $shippingModel->getData();
            }
        }
        return $shippingList;
    }

    /**
     * Get List Core Shipping Methods
     *
     * @return array
     */
    protected function getListCoreShippingMethods()
    {
        $collection = $this->shippingConfigModel->getAllCarriers();
        $listMethod = [];
        foreach ($collection as $code => $carrier) {
            if (in_array($code, $this->allowShippings)) {
                $listMethod[$code] = $carrier;
            }
        }
        return $listMethod;
    }

    /**
     * Get array of allow shipping methods
     *
     * @return array
     */
    public function getAllowShippingMethods()
    {
        return $this->allowShippings;
    }

    /**
     * Check shipping method for pos
     *
     * @param string $code
     * @return boolean
     */
    public function isAllowOnWebPOS($code)
    {
        $specificshipping = $this->scopeConfig->getValue(
            self::SPECIFIC_SHIPPING,
            ScopeInterface::SCOPE_STORE
        );
        $specificshipping = explode(',', $specificshipping);
        $specificshipping[] = 'webpos_shipping';
        if (in_array($code, $specificshipping)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get default shipping method for pos
     *
     * @return string
     */
    public function getDefaultShippingMethod()
    {
        return 'webpos_shipping';
    }
}
