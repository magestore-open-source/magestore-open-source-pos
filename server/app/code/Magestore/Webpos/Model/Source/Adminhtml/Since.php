<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Framework\Option\ArrayInterface;

/**
 * Source - Since
 */
class Since implements ArrayInterface
{

    const SINCE_24H = '24h';
    const SINCE_7DAYS = '7days';
    const SINCE_MONTH = 'month';
    const SINCE_YTD = 'YTD';
    const SINCE_2YTD = '2YTD';

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('Last 24 hours'), 'value' => self::SINCE_24H],
            ['label' => __('Last 7 days'), 'value' => self::SINCE_7DAYS],
            ['label' => __('Current month'), 'value' => self::SINCE_MONTH],
            ['label' => __('YTD'), 'value' => self::SINCE_YTD],
            ['label' => __('2YTD'), 'value' => self::SINCE_2YTD]
        ];
    }
}
