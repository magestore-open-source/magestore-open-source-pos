<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Framework\Option\ArrayInterface;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;

/**
 * Source - Location
 */
class Location implements ArrayInterface
{
    /**
     * @var LocationRepositoryInterface
     */
    public $locationRepository;

    /**
     * Location constructor.
     * @param LocationRepositoryInterface $locationRepository
     */
    public function __construct(
        LocationRepositoryInterface $locationRepository
    ) {
        $this->locationRepository = $locationRepository;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        $allLocation = $this->locationRepository->getAllLocation();
        $allLocationArray = [];
        foreach ($allLocation as $location) {
            $allLocationArray[] = ['label' => $location->getName(), 'value' => $location->getLocationId()];
        }
        return $allLocationArray;
    }

    /**
     * Get Option Array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $allLocation = $this->locationRepository->getAllLocation();
        $allLocationArray = [];
        foreach ($allLocation as $location) {
            $allLocationArray[$location->getLocationId()] = $location->getName();
        }
        return $allLocationArray;
    }
}
