<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Framework\Option\ArrayInterface;

/**
 * Sync source model
 */
class Sync implements ArrayInterface
{
   /**
    * @inheritDoc
    */
    public function toOptionArray()
    {
        return [
            ['label' => __('1 min'), 'value' => 1],
            ['label' => __('5 mins'), 'value' => 5],
            ['label' => __('10 mins'), 'value' => 10],
            ['label' => __('30 mins'), 'value' => 30],
            ['label' => __('60 mins'), 'value' => 60],
        ];
    }
}
