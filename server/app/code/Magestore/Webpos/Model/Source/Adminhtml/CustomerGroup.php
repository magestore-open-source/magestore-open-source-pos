<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Source\Adminhtml;

use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;

/**
 * Source option CustomerGroup
 */
class CustomerGroup implements ArrayInterface
{

    const ALL = 'all';
    /**
     *
     * @var CollectionFactory
     */
    protected $_customerGroupCollectionFactory;

    /**
     * CustomerGroup constructor.
     *
     * @param CollectionFactory $customerGroupCollectionFactory
     */
    public function __construct(
        CollectionFactory $customerGroupCollectionFactory
    ) {
        $this->_customerGroupCollectionFactory = $customerGroupCollectionFactory;
    }

    /**
     * ToOptionArray
     *
     * @return array
     */
    public function toOptionArray()
    {
        $groups = $this->_customerGroupCollectionFactory->create();
        $options = [];
        $options[] = [
            'value' => self::ALL,
            'label' => __('All groups')
        ];
        foreach ($groups as $group) {
            if ($group->getId() == 0) {
                continue;
            }
            $options[] = [
                'value' => $group->getId(),
                'label' => $group->getData('customer_group_code')
            ];
        }
        return $options;
    }

    /**
     * Get Option Array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $array = [self::ALL => __('All groups')];
        $groups = $this->_customerGroupCollectionFactory->create();
        foreach ($groups as $group) {
            if ($group->getId() == 0) {
                continue;
            }
            $array[$group->getId()] = $group->getData('customer_group_code');
        }
        return $array;
    }
}
