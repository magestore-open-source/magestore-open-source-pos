<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Website;

use Magento\Config\Model\Config\Backend\Image\Logo;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Api\Data\Website\WebsiteInformationInterface;
use Magestore\Webpos\Api\Website\WebsiteInformationRepositoryInterface;

/**
 * Website - WebsiteInformationRepository
 */
class WebsiteInformationRepository implements WebsiteInformationRepositoryInterface
{
    /**
     * @var WebsiteInformationInterface
     */
    protected $websiteInformation;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    protected $urlBuilder;
    /**
     * @var Database
     */
    protected $webposFileStorageHelper;

    protected $fileSystem;

    /**
     * Media Directory
     *
     * @var ReadInterface
     */
    protected $mediaDirectory;

    protected $template;

    /**
     * @param WebsiteInformationInterface $websiteInformation
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $urlBuilder
     * @param Database $webposFileStorageHelper
     * @param Filesystem $fileSystem
     * @param Template $template
     */
    public function __construct(
        WebsiteInformationInterface $websiteInformation,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder,
        Database $webposFileStorageHelper,
        Filesystem $fileSystem,
        Template $template
    ) {
        $this->websiteInformation = $websiteInformation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
        $this->webposFileStorageHelper = $webposFileStorageHelper;
        $this->fileSystem = $fileSystem;
        $this->template = $template;
    }

    /**
     * @inheritDoc
     */
    public function getInformation()
    {
        $image = $this->scopeConfig->getValue('webpos/general/webpos_logo');
        if ($image) {
            $imageUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                . 'webpos/logo/' . $image;
        } else {
            $imageUrl = $this->getStoreLogoUrl();
        }
        $this->websiteInformation->setLogoUrl($imageUrl);
        return $this->websiteInformation;
    }

    /**
     * Get Store Logo Url
     *
     * @return string
     */
    protected function getStoreLogoUrl()
    {
        $uploadFolderName = Logo::UPLOAD_DIR;
        $webposLogoPath = $this->scopeConfig->getValue(
            'design/header/logo_src',
            ScopeInterface::SCOPE_STORE
        );
        $path = $uploadFolderName . '/' . $webposLogoPath;
        $logoUrl = $this->urlBuilder
                ->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $path;
        if ($webposLogoPath !== null && $this->isFile($path)) {
            $url = $logoUrl;
        } else {
            $url = '';
        }
        return $url;
    }

    /**
     * Is File
     *
     * @param string $filename
     * @return bool
     */
    protected function isFile($filename)
    {
        if ($this->webposFileStorageHelper->checkDbUsage() && !$this->getMediaDirectory()->isFile($filename)) {
            $this->webposFileStorageHelper->saveFileToFilesystem($filename);
        }

        return $this->getMediaDirectory()->isFile($filename);
    }

    /**
     * Get media directory
     *
     * @return ReadInterface
     */
    protected function getMediaDirectory()
    {
        if (!$this->mediaDirectory) {
            $this->mediaDirectory = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
        }
        return $this->mediaDirectory;
    }
}
