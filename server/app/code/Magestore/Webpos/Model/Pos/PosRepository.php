<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Pos;

use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magestore\Webpos\Api\Data\Pos\PosInterface;
use Magestore\Webpos\Api\Data\Pos\PosInterfaceFactory;
use Magestore\Webpos\Api\Data\Pos\PosSearchResultsInterfaceFactory;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Pos\Pos\Collection;
use Magestore\Webpos\Model\ResourceModel\Staff\Session;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;

/**
 * Class PosRepository
 *
 * Used for pos repository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PosRepository implements PosRepositoryInterface
{

    /**
     * @var  \Magestore\Webpos\Model\ResourceModel\Pos\Pos
     */
    protected $posResourceModel;
    /**
     * @var \Magestore\Webpos\Model\ResourceModel\Pos\Pos\CollectionFactory
     */
    protected $posResourceCollectionFactory;

    /**
     * @var PosInterfaceFactory
     */
    protected $posFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var Session
     */
    protected $sessionResource;

    /**
     * @var PosSearchResultsInterfaceFactory
     */
    protected $posSearchResults;
    /**
     * @var CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var GetSessionByIdInterface
     */
    protected $getSessionById;

    /**
     * PosRepository constructor.
     *
     * @param \Magestore\Webpos\Model\ResourceModel\Pos\Pos $posResourceModel
     * @param PosInterfaceFactory $posFactory
     * @param \Magestore\Webpos\Model\ResourceModel\Pos\Pos\CollectionFactory $posResourceCollection
     * @param RequestInterface $request
     * @param Session $sessionResource
     * @param PosSearchResultsInterfaceFactory $posSearchResultsInterfaceFactory
     * @param CollectionFactory $orderCollectionFactory
     * @param GetSessionByIdInterface $getSessionById
     */
    public function __construct(
        \Magestore\Webpos\Model\ResourceModel\Pos\Pos $posResourceModel,
        PosInterfaceFactory $posFactory,
        \Magestore\Webpos\Model\ResourceModel\Pos\Pos\CollectionFactory $posResourceCollection,
        RequestInterface $request,
        Session $sessionResource,
        PosSearchResultsInterfaceFactory $posSearchResultsInterfaceFactory,
        CollectionFactory $orderCollectionFactory,
        GetSessionByIdInterface $getSessionById
    ) {
        $this->posResourceModel = $posResourceModel;
        $this->posFactory = $posFactory;
        $this->posResourceCollectionFactory = $posResourceCollection;
        $this->request = $request;
        $this->sessionResource = $sessionResource;
        $this->posSearchResults = $posSearchResultsInterfaceFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->getSessionById = $getSessionById;
    }

    /**
     * Save staff.
     *
     * @param PosInterface $pos
     * @return PosInterface
     * @throws LocalizedException
     */
    public function save(PosInterface $pos)
    {
        try {
            $this->posResourceModel->save($pos);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Unable to save pos'));
        }
        return $pos;
    }

    /**
     * @inheritdoc
     */
    public function getAllPos()
    {
        /** @var Collection $collection */
        $collection = $this->posResourceCollectionFactory->create();
        return $collection;
    }

    /**
     * Retrieve pos collection.
     *
     * @param int $locationId
     * @return PosInterface[]
     */
    public function getPosByLocationId($locationId)
    {
        $posCollection =  $this->posResourceCollectionFactory->create()
            ->joinToStaffTable()
            ->addFieldToFilter(PosInterface::LOCATION_ID, $locationId)
            ;
        return $posCollection;
    }

    /**
     * @inheritdoc
     */
    public function delete(PosInterface $pos)
    {
        return $this->deleteById($pos->getId());
    }

    /**
     * @inheritdoc
     */
    public function deleteById($posId)
    {
        $pos = $this->getById($posId);
        $salesCollection = $this->orderCollectionFactory->create()->addFieldToFilter('pos_id', $posId);
        if (!$salesCollection->getSize()) {
            $this->posResourceModel->delete($pos);
        } else {
            throw new CouldNotDeleteException(
                __('The operations failed. Some POS are still working. You can\'t delete them!')
            );
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function getById($posId)
    {
        $pos = $this->posFactory->create();
        $this->posResourceModel->load($pos, $posId);
        if (!$pos->getId()) {
            throw new NoSuchEntityException(
                __('Pos with id "%1" does not exist.', $posId)
            );
        } else {
            return $pos;
        }
    }

    /**
     * @inheritdoc
     */
    public function assignPos($pos)
    {
        $posModel = $this->posFactory->create();
        $this->posResourceModel->load($posModel, $pos->getPosId());
        if (!$posModel->getId()) {
            throw new NoSuchEntityException(
                __('Pos with id '.$pos->getPosId().' does not exist.')
            );
        } else {
            try {
                $sessionId = $this->request->getParam(
                    RequestProcessor::SESSION_PARAM_KEY
                );
                try {
                    $sessionLogin = $this->getSessionById->execute($sessionId);
                    if ($sessionLogin) {
                        $sessionLogin->setLocationId($posModel->getLocationId())
                            ->setPosId($posModel->getPosId())
                            ->setHasException(0);
                        $this->sessionResource->save($sessionLogin);
                    } else {
                        throw new NoSuchEntityException(__('The session does not exist.'));
                    }
                } catch (LocalizedException $e) {
                    throw new LocalizedException(
                        __('There are some problem when save session!')
                    );
                }
                $posModel->setData('staff_id', $sessionLogin->getStaffId());
                $this->posResourceModel->save($posModel);
            } catch (Exception $e) {
                throw new CouldNotSaveException(__('Unable to save pos'));
            }
        }
        return $posModel->setMessage('Enter to pos successfully');
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->posResourceCollectionFactory->create();
        //Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders === null) {
            $sortOrders = [];
        }
        /** @var SortOrder $sortOrder */
        foreach ($sortOrders as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == SortOrder::SORT_ASC)
                    ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
            );
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collectionSize = $collection->getSize();
        $collection->load();
        $searchResults = $this->posSearchResults->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collectionSize);
        return $searchResults;
    }

    /**
     * Free pos by id
     *
     * @param int $posId
     * @return PosInterface
     * @throws Exception
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    public function freePosById($posId)
    {
        $posModel = $this->posFactory->create();
        $this->posResourceModel->load($posModel, $posId);
        if (!$posModel->getId()) {
            throw new NoSuchEntityException(
                __('Pos with id ' . $posId . ' does not exist.')
            );
        }

        $posModel->setData(PosInterface::STAFF_ID, null);
        $this->posResourceModel->save($posModel);
        return $posModel;
    }
}
