<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Pos;

use Exception;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Webpos\Api\Data\Pos\PosInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Pos\Pos\Collection;
use Magestore\Webpos\Model\Source\Adminhtml\Status;
use Magestore\Webpos\Model\ResourceModel\Pos\Pos as PosResourceModel;

/**
 * Pos model
 */
class Pos extends AbstractModel implements PosInterface
{
    /**
     * @var DispatchServiceInterface
     */
    protected $dispatchService;
    /**
     * @var PosRepositoryInterface
     */
    protected $posRepository;

    protected $currentData = [];

    /**
     * Pos constructor.
     * @param Context $context
     * @param Registry $registry
     * @param PosResourceModel $resource
     * @param Collection $resourceCollection
     * @param DispatchServiceInterface $dispatchService
     * @param PosRepositoryInterface $posRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PosResourceModel $resource,
        Collection $resourceCollection,
        DispatchServiceInterface $dispatchService,
        PosRepositoryInterface $posRepository,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->dispatchService = $dispatchService;
        $this->posRepository = $posRepository;
    }

    /**
     * @inheritDoc
     */
    public function getPosId()
    {
        return $this->getData(self::POS_ID);
    }

    /**
     * @inheritDoc
     */
    public function setPosId($posId)
    {
        return $this->setData(self::POS_ID, $posId);
    }

    /**
     * @inheritDoc
     */
    public function getPosName()
    {
        return $this->getData(self::POS_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setPosName($posName)
    {
        return $this->setData(self::POS_NAME, $posName);
    }
    /**
     * @inheritDoc
     */
    public function getLocationId()
    {
        return $this->getData(self::LOCATION_ID);
    }

    /**
     * @inheritDoc
     */
    public function setLocationId($locationId)
    {
        return $this->setData(self::LOCATION_ID, $locationId);
    }

    /**
     * @inheritDoc
     */
    public function getStaffId()
    {
        return $this->getData(self::STAFF_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStaffId($staff_id)
    {
        return $this->setData(self::STAFF_ID, $staff_id);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     *
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        if (!$this->isObjectNew()) {
            $currentObject = $this->posRepository->getById($this->getPosId());
            $this->currentData = $currentObject->getData();
        }
        return parent::beforeSave();
    }

    /**
     * @inheritDoc
     *
     * @throws Exception
     * @throws AlreadyExistsException
     */
    public function afterSave()
    {
        if (!$this->isObjectNew()) {
            // when disable pos or change location
            if (($this->getStatus() == Status::STATUS_DISABLED && $this->hasDataChangeForField('status'))
                || $this->hasDataChangeForField('location_id')) {
                $this->dispatchService->dispatchEventForceChangePos($this->currentData['staff_id'], $this->getPosId());
                $this->setStaffId(null);
                $this->_resource->save($this);
            }
        }
        return parent::afterSave();
    }

    /**
     * Has Data Change For Field
     *
     * @param string $fieldName
     * @return bool
     */
    protected function hasDataChangeForField($fieldName)
    {
        return !($this->currentData[$fieldName] == $this->getData($fieldName));
    }

    /**
     * @inheritDoc
     */
    public function beforeDelete()
    {
        $this->dispatchService->dispatchEventForceChangePos($this->getStaffId(), $this->getPosId());
        return parent::beforeDelete();
    }
}
