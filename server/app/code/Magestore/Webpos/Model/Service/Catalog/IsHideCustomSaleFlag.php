<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Service\Catalog;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;

/**
 * Class IsHideCustomSaleFlag
 *
 * Hide Custom sale flag
 */
class IsHideCustomSaleFlag extends DataObject
{
    const HIDE_CUSTOM_SALE_FLAG = 'hide_custom_sale_flag';

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * IsHideCustomSaleFlag constructor.
     *
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        RequestInterface  $request,
        array $data = []
    ) {
        $this->request = $request;
        parent::__construct($data);
    }

    /**
     * Set hide custom sale flag
     *
     * @param int $flag
     * @return $this;
     */
    public function setHideCustomSaleFlag(int $flag)
    {
        return $this->setData(self::HIDE_CUSTOM_SALE_FLAG, $flag);
    }

    /**
     * Get hide custom sale flag
     *
     * @return int
     */
    public function getHideCustomSaleFlag()
    {
        if ($this->request->getFullActionName() === 'catalog_product_index') {
            return 1;
        }
        return $this->_getData(self::HIDE_CUSTOM_SALE_FLAG);
    }
}
