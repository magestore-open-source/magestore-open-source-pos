<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Service\Request;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Webapi\Controller\Rest\Router\Route;
use Magestore\Webpos\Model\Request\Actions\CancelAction;
use Magestore\Webpos\Model\Request\Actions\RefundAction;
use Magestore\Webpos\Model\Request\Actions\TakePaymentAction;

/**
 * Request - SaveActionLog
 */
class SaveActionLog
{
    /**
     * @var array
     */
    protected $actionLogType;
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * SaveActionLog constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param array $actionLogType
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        $actionLogType = []
    ) {
        $this->objectManager = $objectManager;
        $this->actionLogType = $actionLogType;
    }

    /**
     * Save action log
     *
     * @param Request $request
     * @param Route $route
     * @return void
     */
    public function execute(
        Request $request,
        Route $route
    ) {
        switch ($route->getRoutePath()) {
            case 'V1/webpos/checkout/placeOrder':
                $actionType = "order";
                break;
            case 'V1/webpos/order/takePayment':
                $actionType = TakePaymentAction::ACTION_TYPE;
                break;
            case 'V1/webpos/creditmemos/create':
                $actionType = RefundAction::ACTION_TYPE;
                break;
            case 'V1/webpos/order/cancel':
                $actionType = CancelAction::ACTION_TYPE;
                break;
            default:
                $actionType = "";
        }

        if (!isset($this->actionLogType[$actionType]) || !$this->actionLogType[$actionType]) {
            return;
        }

        $process = $this->objectManager->create($this->actionLogType[$actionType]);
        $process->saveRequest($request);
    }
}
