<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Staff\Data\Login;

use Magento\Framework\Model\AbstractModel;
use Magestore\Webpos\Api\Data\Staff\Login\PosResultInterface;

/**
 * Login - PosResult
 */
class PosResult extends AbstractModel implements PosResultInterface
{
    /**
     * Get pos id
     *
     * @api
     * @return int
     */
    public function getPosId()
    {
        return $this->getData(self::POS_ID);
    }

    /**
     * Set pos id
     *
     * @api
     * @param int $posId
     * @return $this
     */
    public function setPosId($posId)
    {
        return $this->setData(self::POS_ID, $posId);
    }

    /**
     * Get pos name
     *
     * @api
     * @return string
     */
    public function getPosName()
    {
        return $this->getData(self::POS_NAME);
    }

    /**
     * Set location name
     *
     * @api
     * @param string $posName
     * @return $this
     */
    public function setPosName($posName)
    {
        return $this->setData(self::POS_NAME, $posName);
    }

    /**
     * Get staff id
     *
     * @api
     * @return int
     */
    public function getStaffId()
    {
        return $this->getData(self::STAFF_ID);
    }

    /**
     * Set staff id
     *
     * @api
     * @param int $staffId
     * @return $this
     */
    public function setStaffId($staffId)
    {
        return $this->setData(self::STAFF_ID, $staffId);
    }

    /**
     * Get status
     *
     * @api
     * @return int
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set status
     *
     * @api
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get staff name
     *
     * @api
     * @return string
     */
    public function getStaffName()
    {
        return $this->getData(self::STAFF_NAME);
    }

    /**
     * Set staff name
     *
     * @api
     * @param int $staffName
     * @return $this
     */
    public function setStaffName($staffName)
    {
        return $this->setData(self::STAFF_NAME, $staffName);
    }
}
