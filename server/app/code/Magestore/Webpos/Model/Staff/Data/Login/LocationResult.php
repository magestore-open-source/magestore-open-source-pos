<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Staff\Data\Login;

use Magento\Directory\Model\Country;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Staff\Login\LocationResultInterface;

/**
 * Pos - LocationResult model
 */
class LocationResult extends DataObject implements LocationResultInterface
{
    /**
     * @var Country
     */
    protected $country;
    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * @param Country $country
     * @param RegionFactory $regionFactory
     * @param array $data
     */
    public function __construct(
        Country $country,
        RegionFactory $regionFactory,
        array $data = []
    ) {
        parent::__construct($data);
        $this->country = $country;
        $this->regionFactory = $regionFactory;
    }

    /**
     * @inheritDoc
     */
    public function getLocationId()
    {
        return $this->getData(self::LOCATION_ID);
    }

    /**
     * @inheritDoc
     */
    public function setLocationId($locationId)
    {
        return $this->setData(self::LOCATION_ID, $locationId);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData(self::LOCATION_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setName($locationName)
    {
        return $this->setData(self::LOCATION_NAME, $locationName);
    }
    /**
     * @inheritDoc
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * @inheritDoc
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }
    /**
     * @inheritDoc
     */
    public function getTelephone()
    {
        return $this->getData(self::TELEPHONE);
    }

    /**
     * @inheritDoc
     */
    public function setTelephone($telephone)
    {
        return $this->setData(self::TELEPHONE, $telephone);
    }

    /**
     * @inheritDoc
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getPos()
    {
        return $this->getData(self::POS);
    }

    /**
     * @inheritDoc
     */
    public function setPos($pos)
    {
        return $this->setData(self::POS, $pos);
    }
}
