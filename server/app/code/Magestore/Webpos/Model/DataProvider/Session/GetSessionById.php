<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

declare(strict_types = 1);

namespace Magestore\Webpos\Model\DataProvider\Session;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Webpos\Api\Data\Staff\SessionInterface;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;

/**
 * Class GetSessionById
 *
 * Data Provider to get session by id
 */
class GetSessionById extends DataObject implements GetSessionByIdInterface
{
    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;

    /**
     * GetSessionById constructor.
     *
     * @param SessionRepositoryInterface $sessionRepository
     * @param array $data
     */
    public function __construct(
        SessionRepositoryInterface $sessionRepository,
        $data = []
    ) {
        $this->sessionRepository = $sessionRepository;
        parent::__construct($data);
    }

    /**
     * Execute get by session id
     *
     * @param string|null $sessionId
     * @return SessionInterface|null
     * @throws LocalizedException
     */
    public function execute($sessionId)
    {
        $sessionData = $this->_getData(self::SESSION_DATA);
        if (!is_array($sessionData)) {
            $sessionData = [];
        }
        if (!isset($sessionData[$sessionId])) {
            $sessionData[$sessionId] = $this->sessionRepository->getBySessionId($sessionId);
            $this->setData(self::SESSION_DATA, $sessionData);
        }

        return $sessionData[$sessionId];
    }
}
