<?php
/**
 * Copyright © 2018 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types = 1);

namespace Magestore\Webpos\Model\DataProvider;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\DataProvider\IsSkipReindexPluginInterface;

/**
 * Class IsSkipReindexPlugin
 *
 * Data Provider to get is skip reindex plugin
 */
class IsSkipReindexPlugin extends DataObject implements IsSkipReindexPluginInterface
{
    /**
     * Set is skip reindex plugin for save multiple
     *
     * @param bool $isSkipReindexPlugin
     * @return void
     */
    public function setIsSkipReindexPlugin(bool $isSkipReindexPlugin)
    {
        $this->setData(self::IS_SKIP_REINDEX_PLUGIN, $isSkipReindexPlugin);
    }

    /**
     * Get is skip reindex plugin for save multiple
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsSkipReindexPlugin(): bool
    {
        if ($this->_getData(self::IS_SKIP_REINDEX_PLUGIN)) {
            return true;
        } else {
            return false;
        }
    }
}
