<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

declare(strict_types = 1);

namespace Magestore\Webpos\Model\DataProvider\Staff;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Webpos\Api\DataProvider\GetCurrentStaffIdBySessionInterface;
use Magestore\Webpos\Api\Staff\StaffManagementInterface;

/**
 * Class GetCurrentStaffIdBySession
 *
 * Data Provider to get staff by session
 */
class GetCurrentStaffIdBySession extends DataObject implements GetCurrentStaffIdBySessionInterface
{
    /**
     * @var StaffManagementInterface
     */
    protected $staffManagement;

    /**
     * GetCurrentStaffById constructor.
     *
     * @param StaffManagementInterface $staffManagement
     * @param array $data
     */
    public function __construct(
        StaffManagementInterface $staffManagement,
        $data = []
    ) {
        $this->staffManagement = $staffManagement;
        parent::__construct($data);
    }

    /**
     * Get staff by session
     *
     * @param string $session
     * @return int|null
     * @throws LocalizedException
     */
    public function execute($session)
    {
        $staffId = $this->_getData(self::STAFF_ID);
        if (!$staffId) {
            $staffId = $this->staffManagement->authorizeSession($session);
            $this->setData(self::STAFF_ID, $staffId);
        }
        return $staffId;
    }
}
