<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Shipping;

use Magento\Framework\Api\SearchResults;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Webpos\Api\Shipping\ShippingMethodRepositoryInterface;
use Magestore\Webpos\Model\Source\Adminhtml\Shipping;

/**
 * Shipping - ShippingMethodRepository
 */
class ShippingMethodRepository implements ShippingMethodRepositoryInterface
{
    /**
     * @var SearchResults
     */
    protected $shippingMethodSearchResults;

    /**
     * @var Shipping
     */
    protected $shippingMethodSource;

    /**
     * @param Shipping $shippingMethodSource
     * @param SearchResults $shippingMethodSearchResults
     */
    public function __construct(
        Shipping $shippingMethodSource,
        SearchResults $shippingMethodSearchResults
    ) {
        $this->shippingMethodSearchResults = $shippingMethodSearchResults;
        $this->shippingMethodSource = $shippingMethodSource;
    }

    /**
     * @inheritDoc
     */
    public function getList()
    {
        $shippingList = $this->shippingMethodSource->getPosShippingMethods();
        $shippings = $this->shippingMethodSearchResults;
        $shippings->setItems($shippingList);
        $shippings->setTotalCount(count($shippingList));
        return $shippings;
    }
}
