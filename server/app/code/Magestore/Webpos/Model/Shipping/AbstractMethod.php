<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Shipping;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\DataObject;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;
use Psr\Log\LoggerInterface;

/**
 * Web POS shipping abstract model
 */
abstract class AbstractMethod extends AbstractCarrier implements CarrierInterface
{

    /**
     * @var ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var MethodFactory
     */
    protected $rateMethodFactory;
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'webpos_shipping';

    /**
     * Method's code
     *
     * @var string
     */
    protected $method_code = '';

    /**
     * Request object
     *
     * @var Http
     */
    protected $request = '';

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param Http $request
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        Http $request,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->request = $request;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Collect Rates
     *
     * @param RateRequest $request
     * @return Result
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function collectRates(RateRequest $request)
    {
        /** @var Result $result */
        $result = $this->rateResultFactory->create();
        $result->append($this->_getRate());
        return $result;
    }

    /**
     * Get Rate
     *
     * @return MethodFactory
     */
    protected function _getRate()
    {
        $rate = $this->rateMethodFactory->create();
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod($this->method_code);
        $rate->setMethodTitle($this->getConfigData('name'));
        $rate->setPrice($this->getConfigData('price'));
        $rate->setCost($this->getConfigData('price'));

        return $rate;
    }

    /**
     * Is Tracking Available
     *
     * Enable for Web POS
     *
     * @return bool
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * Enable method for Web POS only
     *
     * @return bool
     */
    public function isActive()
    {
        return true;
    }

    /**
     * Enable method for Web POS only
     *
     * @param DataObject $request
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function checkAvailableShipCountries(DataObject $request)
    {
        $active = $this->getConfigData('active');
        $isWebpos = $this->request->getParam(RequestProcessor::SESSION_PARAM_KEY);
        if ($isWebpos && ($active == 1 || $active == 'true')) {
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getAllowedMethods()
    {
        return [];
    }
}
