<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Shipping;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Shipping\ShippingMethodInterface;

/**
 * Shipping Method Model
 */
class ShippingMethod extends DataObject implements ShippingMethodInterface
{
    /**
     * @inheritDoc
     */
    public function getCode()
    {
        return $this->getData(self::CODE);
    }

    /**
     * @inheritDoc
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @inheritDoc
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @inheritDoc
     */
    public function getErrorMessage()
    {
        return $this->getData(self::ERROR_MESSAGE);
    }

    /**
     * @inheritDoc
     */
    public function setErrorMessage($errorMessage)
    {
        return $this->setData(self::ERROR_MESSAGE, $errorMessage);
    }

    /**
     * @inheritDoc
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * @inheritDoc
     */
    public function getShipmentRequestType()
    {
        return $this->getData(self::SHIPMENT_REQUEST_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setShipmentRequestType($shipmentRequestType)
    {
        return $this->setData(self::SHIPMENT_REQUEST_TYPE, $shipmentRequestType);
    }

    /**
     * @inheritDoc
     */
    public function getConditionName()
    {
        return $this->getData(self::CONDITION_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setConditionName($conditionName)
    {
        return $this->setData(self::CONDITION_NAME, $conditionName);
    }

    /**
     * @inheritDoc
     */
    public function getPrice()
    {
        return $this->getData(self::PRICE);
    }

    /**
     * @inheritDoc
     */
    public function setPrice($price)
    {
        return $this->setData(self::PRICE, $price);
    }

    /**
     * @inheritDoc
     */
    public function getIsDefault()
    {
        return $this->getData(self::IS_DEFAULT);
    }

    /**
     * @inheritDoc
     */
    public function setIsDefault($isDefault)
    {
        return $this->setData(self::IS_DEFAULT, $isDefault);
    }

    /**
     * @inheritDoc
     */
    public function getSpecificCountriesAllow()
    {
        return $this->getData(self::SPECIFIC_COUNTRIES_ALLOW);
    }

    /**
     * @inheritDoc
     */
    public function setSpecificCountriesAllow($specificCountriesAllow)
    {
        return $this->setData(self::SPECIFIC_COUNTRIES_ALLOW, $specificCountriesAllow);
    }

    /**
     * @inheritDoc
     */
    public function getSpecificCountry()
    {
        return $this->getData(self::SPECIFIC_COUNTRY);
    }

    /**
     * @inheritDoc
     */
    public function setSpecificCountry($specificCountry)
    {
        return $this->setData(self::SPECIFIC_COUNTRY, $specificCountry);
    }

    /**
     * @inheritDoc
     */
    public function getHandlingFee()
    {
        return $this->getData(self::HANDLING_FEE);
    }

    /**
     * @inheritDoc
     */
    public function setHandlingFee($handlingFee)
    {
        return $this->setData(self::HANDLING_FEE, $handlingFee);
    }

    /**
     * @inheritDoc
     */
    public function getHandlingType()
    {
        return $this->getData(self::HANDLING_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setHandlingType($handlingType)
    {
        return $this->setData(self::HANDLING_TYPE, $handlingType);
    }

    /**
     * @inheritDoc
     */
    public function getHandlingAction()
    {
        return $this->getData(self::HANDLING_ACTION);
    }

    /**
     * @inheritDoc
     */
    public function setHandlingAction($handlingAction)
    {
        return $this->setData(self::HANDLING_ACTION, $handlingAction);
    }

    /**
     * @inheritDoc
     */
    public function getMaxPackageWeight()
    {
        return $this->getData(self::MAX_PACKAGE_WEIGHT);
    }

    /**
     * @inheritDoc
     */
    public function setMaxPackageWeight($maxPackageWeight)
    {
        return $this->setData(self::MAX_PACKAGE_WEIGHT, $maxPackageWeight);
    }

    /**
     * @inheritDoc
     */
    public function getIncludeVirtualPrice()
    {
        return $this->getData(self::INCLUDE_VIRTUAL_PRICE);
    }

    /**
     * @inheritDoc
     */
    public function setIncludeVirtualPrice($includeVirtualPrice)
    {
        return $this->setData(self::INCLUDE_VIRTUAL_PRICE, $includeVirtualPrice);
    }

    /**
     * @inheritDoc
     */
    public function getFreeShippingSubtotal()
    {
        return $this->getData(self::FREE_SHIPPING_SUBTOTAL);
    }

    /**
     * @inheritDoc
     */
    public function setFreeShippingSubtotal($freeShippingSubtotal)
    {
        return $this->setData(self::FREE_SHIPPING_SUBTOTAL, $freeShippingSubtotal);
    }

    /**
     * @inheritDoc
     */
    public function getRates()
    {
        return $this->getData(self::RATES);
    }

    /**
     * @inheritDoc
     */
    public function setRates($rates)
    {
        return $this->setData(self::RATES, $rates);
    }
}
