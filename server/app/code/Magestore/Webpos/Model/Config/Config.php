<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Config\ConfigInterface;

/**
 * Pos - Config Model
 *
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Config extends DataObject implements ConfigInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param ProductMetadataInterface $productMetadata
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ProductMetadataInterface $productMetadata,
        array $data = []
    ) {
        parent::__construct($data);
        $this->scopeConfig = $scopeConfig;
        $this->productMetadata = $productMetadata;
    }

    /**
     * @inheritDoc
     */
    public function getSettings()
    {
        return $this->getData(self::SETTINGS);
    }

    /**
     * @inheritDoc
     */
    public function setSettings($settings)
    {
        return $this->setData(self::SETTINGS, $settings);
    }

    /**
     * @inheritDoc
     */
    public function getCurrencies()
    {
        return $this->getData(self::CURRENCIES);
    }

    /**
     * @inheritDoc
     */
    public function setCurrencies($currencies)
    {
        return $this->setData(self::CURRENCIES, $currencies);
    }

    /**
     * @inheritDoc
     */
    public function getBaseCurrencyCode()
    {
        return $this->getData(self::BASE_CURRENCY_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setBaseCurrencyCode($baseCurrencyCode)
    {
        return $this->setData(self::BASE_CURRENCY_CODE, $baseCurrencyCode);
    }

    /**
     * @inheritDoc
     */
    public function getCurrentCurrencyCode()
    {
        return $this->getData(self::CURRENT_CURRENCY_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setCurrentCurrencyCode($currentCurrencyCode)
    {
        return $this->setData(self::CURRENT_CURRENCY_CODE, $currentCurrencyCode);
    }

    /**
     * @inheritDoc
     */
    public function getPriceFormats()
    {
        return $this->getData(self::PRICE_FORMATS);
    }

    /**
     * @inheritDoc
     */
    public function setPriceFormats($priceFormats)
    {
        return $this->setData(self::PRICE_FORMATS, $priceFormats);
    }

    /**
     * @inheritDoc
     */
    public function getGuestCustomer()
    {
        return $this->getData(self::GUEST_CUSTOMER);
    }

    /**
     * @inheritDoc
     */
    public function setGuestCustomer($guestInfo)
    {
        return $this->setData(self::GUEST_CUSTOMER, $guestInfo);
    }

    /**
     * @inheritDoc
     */
    public function getStoreName()
    {
        return $this->scopeConfig->getValue('general/store_information/name');
    }

    /**
     * @inheritDoc
     */
    public function setStoreName($storeName)
    {
        return $this->setData(self::STORE_NAME, $storeName);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerGroups()
    {
        return $this->getData(self::CUSTOMER_GROUPS);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerGroups($customerGroups)
    {
        return $this->setData(self::CUSTOMER_GROUPS, $customerGroups);
    }

    /**
     * @inheritDoc
     */
    public function getShipping()
    {
        return $this->getData(self::SHIPPING);
    }

    /**
     * @inheritDoc
     */
    public function setShipping($shipping)
    {
        return $this->setData(self::SHIPPING, $shipping);
    }

    /**
     * @inheritDoc
     */
    public function getPayment()
    {
        return $this->getData(self::PAYMENT);
    }

    /**
     * @inheritDoc
     */
    public function setPayment($payment)
    {
        return $this->setData(self::PAYMENT, $payment);
    }

    /**
     * @inheritDoc
     */
    public function getTaxRates()
    {
        return $this->getData(self::TAX_RATES);
    }

    /**
     * @inheritDoc
     */
    public function setTaxRates($taxRates)
    {
        return $this->setData(self::TAX_RATES, $taxRates);
    }

    /**
     * @inheritDoc
     */
    public function getTaxRules()
    {
        return $this->getData(self::TAX_RULES);
    }

    /**
     * @inheritDoc
     */
    public function setTaxRules($taxRules)
    {
        return $this->setData(self::TAX_RULES, $taxRules);
    }

    /**
     * @inheritDoc
     */
    public function getProductTaxClasses()
    {
        return $this->_getData(self::PRODUCT_TAX_CLASSES);
    }

    /**
     * @inheritDoc
     */
    public function setProductTaxClasses($taxClasses)
    {
        return $this->setData(self::PRODUCT_TAX_CLASSES, $taxClasses);
    }

    /**
     * @inheritDoc
     */
    public function getIsPrimaryLocation()
    {
        return $this->_getData(self::IS_PRIMARY_LOCATION);
    }

    /**
     * @inheritDoc
     */
    public function setIsPrimaryLocation($isPrimaryLocation)
    {
        return $this->setData(self::IS_PRIMARY_LOCATION, $isPrimaryLocation);
    }

    /**
     * @inheritDoc
     */
    public function getCustomSaleProductId()
    {
        return $this->getData(self::CUSTOM_SALE_PRODUCT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomSaleProductId($customSaleProductId)
    {
        return $this->setData(self::CUSTOM_SALE_PRODUCT_ID, $customSaleProductId);
    }

    /**
     * @inheritDoc
     */
    public function getEnableModules()
    {
        return $this->getData(self::ENABLE_MODULES);
    }

    /**
     * @inheritDoc
     */
    public function setEnableModules($enableModules)
    {
        return $this->setData(self::ENABLE_MODULES, $enableModules);
    }

    /**
     * @inheritDoc
     */
    public function getMaxDiscountPercent()
    {
        return $this->getData(self::MAX_DISCOUNT_PERCENT);
    }

    /**
     * @inheritDoc
     */
    public function setMaxDiscountPercent($maxDiscountPercent)
    {
        return $this->setData(self::MAX_DISCOUNT_PERCENT, $maxDiscountPercent);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerForm()
    {
        return $this->getData(self::CUSTOMER_FORM);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerForm($customerForm)
    {
        return $this->setData(self::CUSTOMER_FORM, $customerForm);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerAddressForm()
    {
        return $this->getData(self::CUSTOMER_ADDRESS_FORM);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerAddressForm($customerAddressForm)
    {
        return $this->setData(self::CUSTOMER_ADDRESS_FORM, $customerAddressForm);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerCustomAttributes()
    {
        return $this->getData(self::CUSTOMER_CUSTOM_ATTRIBUTES);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerCustomAttributes($customerCustomAttributes)
    {
        return $this->setData(self::CUSTOMER_CUSTOM_ATTRIBUTES, $customerCustomAttributes);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerAddressCustomAttributes()
    {
        return $this->getData(self::CUSTOMER_ADDRESS_CUSTOM_ATTRIBUTES);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerAddressCustomAttributes($customerAddressCustomAttributes)
    {
        return $this->setData(self::CUSTOMER_ADDRESS_CUSTOM_ATTRIBUTES, $customerAddressCustomAttributes);
    }

    /**
     * @inheritDoc
     */
    public function getRootCategoryId()
    {
        return $this->getData(self::ROOT_CATEGORY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setRootCategoryId($rootCategoryId)
    {
        return $this->setData(self::ROOT_CATEGORY_ID, $rootCategoryId);
    }

    /**
     * @inheritDoc
     */
    public function getCountriesWithOptionalZip()
    {
        return $this->getData(self::COUNTRIES_WITH_OPTIONAL_ZIP);
    }

    /**
     * @inheritDoc
     */
    public function setCountriesWithOptionalZip($countriesWithOptionalZip)
    {
        return $this->setData(self::COUNTRIES_WITH_OPTIONAL_ZIP, $countriesWithOptionalZip);
    }

    /**
     * @inheritDoc
     */
    public function getMagentoVersion()
    {
        if ($version = $this->getData(self::MAGENTO_VERSION)) {
            return $version;
        }
        return $this->productMetadata->getVersion();
    }

    /**
     * @inheritDoc
     */
    public function setMagentoVersion($magentoVersion)
    {
        return $this->setData(self::MAGENTO_VERSION, $magentoVersion);
    }

    /**
     * @inheritDoc
     */
    public function getServerTimezoneOffset()
    {
        return $this->getData(self::SERVER_TIMEZONE_OFFSET);
    }

    /**
     * @inheritDoc
     */
    public function setServerTimezoneOffset($serverTimezoneOffset)
    {
        return $this->setData(self::SERVER_TIMEZONE_OFFSET, $serverTimezoneOffset);
    }
}
