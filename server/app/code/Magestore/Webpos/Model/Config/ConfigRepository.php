<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Config;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Directory\Helper\Data;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Module\Manager;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Model\TaxClass\Source\Product;
use Magestore\Webpos\Api\Config\ConfigRepositoryInterface;
use Magestore\Webpos\Api\Data\Config\ConfigInterface;
use Magestore\Webpos\Api\Data\Config\CustomerGroupInterface;
use Magestore\Webpos\Api\Data\Config\GuestCustomerInterface;
use Magestore\Webpos\Api\Data\Config\PaymentInterface;
use Magestore\Webpos\Api\Data\Config\PriceFormatInterfaceFactory;
use Magestore\Webpos\Api\Data\Config\ProductTaxClassesInterface;
use Magestore\Webpos\Api\Data\Config\ProductTaxClassesInterfaceFactory;
use Magestore\Webpos\Api\Data\Config\ShippingInterface;
use Magestore\Webpos\Api\Data\Config\SystemConfigInterface;
use Magestore\Webpos\Api\Data\Config\SystemConfigInterfaceFactory;
use Magestore\Webpos\Api\DataProvider\GetCurrentStaffIdBySessionInterface;
use Magestore\Appadmin\Api\DataProvider\GetStaffByIdInterface;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Webpos\Helper\Product\CustomSale;
use Magestore\Webpos\Model\Customer\AddressMetadata;
use Magestore\Webpos\Model\Customer\CustomerMetadata;
use Magestore\Webpos\Model\ResourceModel\Directory\CurrencyRates;
use Magestore\Webpos\Model\ResourceModel\Sales\Order;
use Magestore\Webpos\Model\Tax\TaxRateRepository;
use Magestore\Webpos\Model\Tax\TaxRuleRepository;

/**
 * Config ConfigRepository
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ConfigRepository implements ConfigRepositoryInterface
{
    /**
     * @var SystemConfigInterfaceFactory
     */
    protected $systemConfigFactory;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var ConfigInterface
     */
    protected $config;
    /**
     * @var CurrencyFactory
     */
    protected $currencyFactory;
    /**
     * @var CurrencyInterface
     */
    protected $localeCurrency;
    /**
     * @var FormatInterface
     */
    protected $localeFormat;
    /**
     * @var PriceFormatInterfaceFactory
     */
    protected $priceFormatFactory;
    /**
     * @var GuestCustomerInterface
     */
    protected $guestCustomer;
    /**
     * @var ShippingInterface
     */
    protected $shipping;
    /**
     * @var PaymentInterface
     */
    protected $payment;
    /**
     * @var RegionFactory
     */
    protected $regionFactory;
    /**
     * @var GroupRepositoryInterface
     */
    protected $customerGroupRepository;
    /**
     * @var SearchCriteriaInterface
     */
    protected $searchCriteria;
    /**
     * @var \Magestore\Webpos\Helper\Data
     */
    protected $helper;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var GetCurrentStaffIdBySessionInterface
     */
    protected $getCurrentStaffIdBySession;

    /**
     * @var TaxRateRepository
     */
    protected $taxRateRepository;
    /**
     * @var TaxRuleRepository
     */
    protected $taxRuleRepository;

    /**
     * @var Product
     */
    protected $productTaxClass;
    /**
     * @var ProductTaxClassesInterfaceFactory
     */
    protected $productTaxClassFactory;

    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepository;

    /**
     * @var CustomSale $customSaleHelper
     */
    protected $customSaleHelper;
    /**
     * @var CustomerMetadata
     */
    protected $customerMetadataService;
    /**
     * @var
     */
    protected $addressMetadataService;

    /**
     * @var Data
     */
    protected $directoryHelper;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var Order
     */
    protected $orderResourceModel;

    /**
     * @var GetStaffByIdInterface
     */
    protected $getStaffById;

    protected $currencyRates;

    /**
     * ConfigRepository constructor.
     *
     * @param SystemConfigInterfaceFactory $systemConfigInterfaceFactory
     * @param ConfigInterface $config
     * @param PriceFormatInterfaceFactory $priceFormatFactory
     * @param GuestCustomerInterface $guestCustomer
     * @param ShippingInterface $shipping
     * @param PaymentInterface $payment
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param CurrencyInterface $localeCurrency
     * @param CurrencyFactory $currencyFactory
     * @param FormatInterface $localeFormat
     * @param RegionFactory $regionFactory
     * @param GroupRepositoryInterface $customerGroupRepository
     * @param SearchCriteriaInterface $searchCriteria
     * @param \Magestore\Webpos\Helper\Data $helper
     * @param Request $request
     * @param GetCurrentStaffIdBySessionInterface $getCurrentStaffIdBySession
     * @param TaxRateRepository $taxRateRepository
     * @param TaxRuleRepository $taxRuleRepository
     * @param Product $productTaxClass
     * @param ProductTaxClassesInterfaceFactory $productTaxClassFactory
     * @param Manager $moduleManager
     * @param Registry $coreRegistry
     * @param LocationRepositoryInterface $locationRepository
     * @param CustomSale $customSaleHelper
     * @param CustomerMetadata $customerMetadataService
     * @param AddressMetadata $addressMetadataService
     * @param Data $directoryHelper
     * @param DateTime $date
     * @param Order $orderResourceModel
     * @param GetStaffByIdInterface $getStaffById
     * @param CurrencyRates $currencyRates
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        SystemConfigInterfaceFactory $systemConfigInterfaceFactory,
        ConfigInterface $config,
        PriceFormatInterfaceFactory $priceFormatFactory,
        GuestCustomerInterface $guestCustomer,
        ShippingInterface $shipping,
        PaymentInterface $payment,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        CurrencyInterface $localeCurrency,
        CurrencyFactory $currencyFactory,
        FormatInterface $localeFormat,
        RegionFactory $regionFactory,
        GroupRepositoryInterface $customerGroupRepository,
        SearchCriteriaInterface $searchCriteria,
        \Magestore\Webpos\Helper\Data $helper,
        Request $request,
        GetCurrentStaffIdBySessionInterface $getCurrentStaffIdBySession,
        TaxRateRepository $taxRateRepository,
        TaxRuleRepository $taxRuleRepository,
        Product $productTaxClass,
        ProductTaxClassesInterfaceFactory $productTaxClassFactory,
        Manager $moduleManager,
        Registry $coreRegistry,
        LocationRepositoryInterface $locationRepository,
        CustomSale $customSaleHelper,
        CustomerMetadata $customerMetadataService,
        AddressMetadata $addressMetadataService,
        Data $directoryHelper,
        DateTime $date,
        Order $orderResourceModel,
        GetStaffByIdInterface $getStaffById,
        CurrencyRates $currencyRates
    ) {
        $this->systemConfigFactory = $systemConfigInterfaceFactory;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->currencyFactory = $currencyFactory;
        $this->localeCurrency = $localeCurrency;
        $this->localeFormat = $localeFormat;
        $this->priceFormatFactory = $priceFormatFactory;
        $this->guestCustomer = $guestCustomer;
        $this->shipping = $shipping;
        $this->payment = $payment;
        $this->regionFactory = $regionFactory;
        $this->customerGroupRepository = $customerGroupRepository;
        $this->searchCriteria = $searchCriteria;
        $this->helper = $helper;
        $this->request = $request;
        $this->getCurrentStaffIdBySession = $getCurrentStaffIdBySession;
        $this->taxRateRepository = $taxRateRepository;
        $this->taxRuleRepository = $taxRuleRepository;
        $this->productTaxClass = $productTaxClass;
        $this->productTaxClassFactory = $productTaxClassFactory;
        $this->moduleManager = $moduleManager;
        $this->coreRegistry = $coreRegistry;
        $this->locationRepository = $locationRepository;
        $this->customSaleHelper = $customSaleHelper;
        $this->customerMetadataService = $customerMetadataService;
        $this->addressMetadataService = $addressMetadataService;
        $this->directoryHelper = $directoryHelper;
        $this->date = $date;
        $this->orderResourceModel = $orderResourceModel;
        $this->getStaffById = $getStaffById;
        $this->currencyRates = $currencyRates;
    }

    /**
     * Get list location
     *
     * @return ConfigInterface
     * @throws LocalizedException
     */
    public function getAllConfig()
    {
        $configurations = [];
        $store = $this->helper->getCurrentStoreView();
        $storeId = $store->getId();
        $config = $this->getConfigPath();
        foreach ($config as $item) {
            $configModel = $this->systemConfigFactory->create();
            $configModel->setData(SystemConfigInterface::PATH, $item);
            $value = $this->scopeConfig->getValue($item, 'stores', $storeId);
            $value = $value == null ? '' : $value;
            $configModel->setData(SystemConfigInterface::VALUE, $value);
            $configurations[] = $configModel;
        }
        $this->config->setSettings($configurations);
        $this->config->setBaseCurrencyCode($store->getBaseCurrencyCode());
        $this->config->setCurrentCurrencyCode($store->getDefaultCurrencyCode());
        $this->config->setCustomerGroups($this->getCustomerGroups());
        $output = $this->getCurrencyList();
        $this->config->setCurrencies($output['currencies']);
        $this->config->setPriceFormats($output['price_formats']);
        $this->config->setGuestCustomer($this->getGuestCustomerInfo());
        $this->config->setShipping($this->getShippingInfo());
        $this->config->setPayment($this->getPaymentInfo());
        $this->config->setTaxRates($this->getTaxRates());
        $this->config->setTaxRules($this->getTaxRules());
        $this->config->setIsPrimaryLocation($this->isPrimaryLocation());
        $this->config->setEnableModules($this->getEnableModules());
        $this->config->setMaxDiscountPercent($this->getMaxDiscountPercent());
        $this->config->setCustomerForm($this->customerMetadataService->getAttributes('adminhtml_customer'));
        $this->config->setCustomerAddressForm(
            $this->addressMetadataService->getAttributes('adminhtml_customer_address')
        );
        $this->config->setCustomerCustomAttributes($this->customerMetadataService->getCustomAttributesMetadata());
        $this->config->setCustomerAddressCustomAttributes(
            $this->addressMetadataService->getCustomAttributesMetadata()
        );
        $this->config->setRootCategoryId($this->helper->getCurrentStoreView()->getRootCategoryId());
        $this->config->setCountriesWithOptionalZip($this->directoryHelper->getCountriesWithOptionalZip(true));
        $this->config->setServerTimezoneOffset($this->date->getGmtOffset());

        $taxClasses = [];
        foreach ($this->productTaxClass->getAllOptions() as $option) {
            /** @var ProductTaxClassesInterface $model */
            $model = $this->productTaxClassFactory->create();
            $model->setLabel($option['label']);
            $model->setValue($option['value']);
            $taxClasses[] = $model;
        }
        $this->config->setProductTaxClasses($taxClasses);
        $this->config->setCustomSaleProductId($this->customSaleHelper->getProductId());
        return $this->config;
    }

    /**
     * Get general config.
     *
     * @return array
     */
    public function getConfigPath()
    {
        $configurations = [
            'general/locale/code',
            'general/region/display_all',
            'cataloginventory/item_options/manage_stock',
            'cataloginventory/item_options/backorders',
            'cataloginventory/item_options/max_sale_qty',
            'cataloginventory/item_options/min_qty',
            'cataloginventory/item_options/min_sale_qty',
            'cataloginventory/item_options/notify_stock_qty',
            'cataloginventory/item_options/auto_return',
            'cataloginventory/item_options/enable_qty_increments',
            'cataloginventory/item_options/qty_increments',
            'cataloginventory/options/can_subtract',
            'cataloginventory/options/can_back_in_stock',
            'customer/create_account/default_group',
            'shipping/origin/country_id',
            'shipping/origin/region_id',
            'shipping/origin/city',
            'shipping/origin/postcode',
            'tax/classes/shipping_tax_class',
            'tax/calculation/price_includes_tax',
            'tax/calculation/shipping_includes_tax',
            'tax/calculation/based_on',
            'tax/calculation/apply_tax_on',
            'tax/calculation/apply_after_discount',
            'tax/calculation/discount_tax',
            'tax/calculation/algorithm',
            'tax/calculation/cross_border_trade_enabled',
            'tax/defaults/country',
            'tax/defaults/region',
            'tax/defaults/postcode',
            'tax/display/type',
            'tax/cart_display/price',
            'tax/weee/enable',
            'tax/weee/display',
            'tax/weee/display_sales',
            'tax/weee/display_email',
            'tax/weee/include_in_subtotal',
            'tax/weee/apply_vat',
            'webpos/general/session_timeout',
            'webpos/general/webpos_color',
            'webpos/general/google_api_key',
            'webpos/product_search/barcode',
            'webpos/product_search/additional_attributes_on_grid',
            'webpos/checkout/add_out_of_stock_product',
            'webpos/checkout/need_confirm',
            'webpos/checkout/automatically_send_mail',
            'webpos/checkout/use_custom_prefix',
            'webpos/checkout/custom_prefix',
            'webpos/tax_configuration/custom_sale_default_tax_class',
            'webpos/tax_configuration/price_display/product_list',
            'webpos/tax_configuration/price_display/shipping',
            'webpos/tax_configuration/shopping_cart_display/price',
            'webpos/tax_configuration/shopping_cart_display/subtotal',
            'webpos/tax_configuration/shopping_cart_display/shipping_amount',
            'webpos/tax_configuration/shopping_cart_display/full_tax_summary',
            'webpos/tax_configuration/shopping_cart_display/zero_tax_subtotal',
            'webpos/tax_configuration/tax_display/price',
            'webpos/tax_configuration/tax_display/subtotal',
            'webpos/tax_configuration/tax_display/shipping_amount',
            'webpos/tax_configuration/tax_display/full_tax_summary',
            'webpos/tax_configuration/tax_display/zero_tax_subtotal',
            'webpos/tax_configuration/fpt/product_price',
            'webpos/tax_configuration/fpt/include_in_subtotal',
            'webpos/offline/product_time',
            'webpos/offline/stock_item_time',
            'webpos/offline/customer_time',
            'webpos/offline/order_time',
            'webpos/offline/order_since',
            'webpos/custom_receipt/display_reason',
            'webpos/performance/pos_default_mode',
            'webpos/performance/pos_tablet_default_mode',
            /** Version Info */
            'about/product/line',
            'about/product/version',
            'customer/create_account/default_group',
        ];
        return $configurations;
    }

    /**
     * Get base currency code
     *
     * @return mixed
     */
    public function getBaseCurrencyCode()
    {
        return $this->helper->getCurrentStoreView()->getBaseCurrency()->getCode();
    }

    /**
     * Get Currency List
     *
     * @return array
     */
    public function getCurrencyList()
    {
        $store = $this->helper->getCurrentStoreView();
        $baseCurrency = $this->helper->getCurrentStoreView()->getBaseCurrency();
        $baseCurrencyCode = $baseCurrency->getData('currency_code');
        $currencyList = [];
        $priceFormats = [];
        $output = [];
        $collection = $store->getAvailableCurrencyCodes();
        /* One query to get all rate for base currency */
        $rates = $this->currencyRates->getCurrencyRates($baseCurrencyCode, $collection);
        if (count($collection) > 0) {
            foreach ($collection as $code) {
                $rate = '';
                /* The same currency with base currency => rate = 1 */
                if ($code == $baseCurrencyCode) {
                    $rate = 1;
                }
                if ($code != $baseCurrencyCode && isset($rates[$code])) {
                    $rate = $rates[$code];
                }
                $currency = $this->currencyFactory->create();
                $allowCurrency = $this->localeCurrency->getCurrency($code);
                $currencySymbol = $allowCurrency->getSymbol() ? $allowCurrency->getSymbol() : $code;
                $currencyName = $allowCurrency->getName();
                $isDefault = $code == $baseCurrencyCode ? 1 : 0;
                $currency->setCode($code);
                $currency->setCurrencyName($currencyName);
                $currency->setCurrencySymbol($currencySymbol);
                $currency->setIsDefault($isDefault);
                $currency->setCurrencyRate($rate);
                $currencyList[] = $currency->getData();

                /* Get price format when it has rate only - does not get format when no rate*/
                $priceFormatModel = $this->priceFormatFactory->create();
                $priceFormat = $this->localeFormat->getPriceFormat(null, $code);
                $priceFormatModel->setCurrencyCode($code);
                $priceFormatModel->setDecimalSymbol($priceFormat['decimalSymbol']);
                $priceFormatModel->setGroupSymbol($priceFormat['groupSymbol']);
                $priceFormatModel->setGroupLength($priceFormat['groupLength']);
                $priceFormatModel->setIntegerRequired($priceFormat['integerRequired']);
                $priceFormatModel->setPattern($priceFormat['pattern']);
                $priceFormatModel->setPrecision($priceFormat['precision']);
                $priceFormatModel->setRequiredPrecision($priceFormat['requiredPrecision']);
                $priceFormats[] = $priceFormatModel;
            }
        }
        $output['currencies'] = $currencyList;
        $output['price_formats'] = $priceFormats;
        return $output;
    }

    /**
     * Get Guest Customer Info
     *
     * @return GuestCustomerInterface
     */
    protected function getGuestCustomerInfo()
    {
        $guestInfo = [
            'webpos/guest_checkout/guest_status' => 'status',
            'webpos/guest_checkout/first_name' => 'first_name',
            'webpos/guest_checkout/last_name' => 'last_name',
            'webpos/guest_checkout/email' => 'email'
        ];

        $guestCustomer = $this->guestCustomer;

        foreach ($guestInfo as $key => $item) {
            $guestCustomer->setData($item, $this->scopeConfig->getValue($key));
        }

        $region = $this->regionFactory->create()->load($guestCustomer->getRegion());
        if ($region->getId() && $region->getCountryId() == $guestCustomer->getCountry()) {
            $guestCustomer->setRegion($region->getName());
            $guestCustomer->setRegionId($region->getId());
        }

        return $guestCustomer;
    }

    /**
     * Get Shipping Info
     *
     * @return ShippingInterface
     */
    protected function getShippingInfo()
    {
        $shippingInfo = [
            'webpos/shipping/method' => 'shipping_methods',
            'webpos/shipping/enable_delivery_date' => 'delivery_date',
            'webpos/shipping/default_shipping_title' => 'default_shipping_title'
        ];

        $shipping = $this->shipping;

        foreach ($shippingInfo as $key => $item) {
            $shipping->setData($item, $this->scopeConfig->getValue($key) ? $this->scopeConfig->getValue($key) : "");
        }

        return $shipping;
    }

    /**
     * Get Payment Info
     *
     * @return PaymentInterface
     */
    protected function getPaymentInfo()
    {
        $paymentInfo = [
            'webpos/payment/method' => 'payment_methods'
        ];

        $payment = $this->payment;

        foreach ($paymentInfo as $key => $item) {
            $payment->setData($item, $this->scopeConfig->getValue($key) ? $this->scopeConfig->getValue($key) : "");
        }

        return $payment;
    }

    /**
     * Get Customer Groups
     *
     * @return CustomerGroupInterface[]
     * @throws LocalizedException
     */
    protected function getCustomerGroups()
    {
        $customerGroups = [];

        $searchCriteria = $this->searchCriteria;
        $searchCriteria->setFilterGroups([]);
        $searchCriteria->setSortOrders([]);
        $searchCriteria->setCurrentPage('');
        $searchCriteria->setPageSize('');

        $groups = $this->customerGroupRepository->getList($searchCriteria)->getItems();
        foreach ($groups as $group) {
            $customerGroups[] = $group;
        }

        return $customerGroups;
    }

    /**
     * Get Tax Rates
     *
     * @return array
     */
    protected function getTaxRates()
    {
        return $this->taxRateRepository->getAllTaxRates();
    }

    /**
     * Get Tax Rules
     *
     * @return array
     */
    protected function getTaxRules()
    {
        $taxRules = [];

        $searchCriteria = $this->searchCriteria;
        $searchCriteria->setFilterGroups([]);
        $searchCriteria->setSortOrders([]);
        $searchCriteria->setCurrentPage('');
        $searchCriteria->setPageSize('');

        $rules = $this->taxRuleRepository->getList($searchCriteria)->getItems();
        foreach ($rules as $rule) {
            $taxRules[] = $rule;
        }

        return $taxRules;
    }

    /**
     * Is Primary Location
     *
     * @return bool
     */
    protected function isPrimaryLocation()
    {
        return true;
    }

    /**
     * Get Enable Modules
     *
     * @return array
     */
    protected function getEnableModules()
    {
        $enableModules = [];
        $modules = [];
        foreach ($modules as $module) {
            if ($this->moduleManager->isEnabled($module)) {
                $enableModules[] = $module;
            }
        }
        return $enableModules;
    }

    /**
     * Get Max Discount Percent
     *
     * @return float
     * @throws NoSuchEntityException
     */
    public function getMaxDiscountPercent()
    {
        /* Full discount percent */
        return 100;
    }
}
