<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Config\Data;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Config\CurrencyInterface;

/**
 * Config model - Currency
 */
class Currency extends DataObject implements CurrencyInterface
{
    /**
     * Get code
     *
     * @api
     * @return string
     */
    public function getCode()
    {
        return $this->getData(self::CODE);
    }

    /**
     * Set code
     *
     * @api
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }
    /**
     * Get currency name
     *
     * @api
     * @return string
     */
    public function getCurrencyName()
    {
        return $this->getData(self::CURRENCY_NAME);
    }

    /**
     * Set currency name
     *
     * @api
     * @param string $currencyName
     * @return $this
     */
    public function setCurrencyName($currencyName)
    {
        return $this->setData(self::CURRENCY_NAME, $currencyName);
    }
    /**
     * Get currency rate
     *
     * @api
     * @return float
     */
    public function getCurrencyRate()
    {
        return $this->getData(self::CURRENCY_RATE);
    }

    /**
     * Set currency rate
     *
     * @api
     * @param float $currencyRate
     * @return $this
     */
    public function setCurrencyRate($currencyRate)
    {
        return $this->setData(self::CURRENCY_RATE, $currencyRate);
    }
    /**
     * Get currency symbol
     *
     * @api
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->getData(self::CURRENCY_SYMBOL);
    }

    /**
     * Set currency symbol
     *
     * @api
     * @param string $currencySymbol
     * @return $this
     */
    public function setCurrencySymbol($currencySymbol)
    {
        return $this->setData(self::CURRENCY_SYMBOL, $currencySymbol);
    }
    /**
     * Get is default
     *
     * @api
     * @return int
     */
    public function getIsDefault()
    {
        return $this->getData(self::IS_DEFAULT);
    }

    /**
     * Set is default
     *
     * @api
     * @param int $isDefault
     * @return $this
     */
    public function setIsDefault($isDefault)
    {
        return $this->setData(self::IS_DEFAULT, $isDefault);
    }
}
