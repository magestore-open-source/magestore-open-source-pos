<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Config\Data;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Config\PriceFormatInterface;

/**
 * Config Model - PriceFormat
 */
class PriceFormat extends DataObject implements PriceFormatInterface
{

    /**
     * Get code
     *
     * @api
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->getData(self::CURRENCY_CODE);
    }

    /**
     * Set code
     *
     * @api
     * @param string $currencyCode
     * @return $this
     */
    public function setCurrencyCode($currencyCode)
    {
        return $this->setData(self::CURRENCY_CODE, $currencyCode);
    }
    /**
     * Get decimal symbol
     *
     * @api
     * @return string
     */
    public function getDecimalSymbol()
    {
        return $this->getData(self::DECIMAL_SYMBOL);
    }

    /**
     * Set decimal symbol
     *
     * @api
     * @param string $decimalSymbol
     * @return $this
     */
    public function setDecimalSymbol($decimalSymbol)
    {
        return $this->setData(self::DECIMAL_SYMBOL, $decimalSymbol);
    }
    /**
     * Get group symbol
     *
     * @api
     * @return string
     */
    public function getGroupSymbol()
    {
        return $this->getData(self::GROUP_SYMBOL);
    }

    /**
     * Set group symbol
     *
     * @api
     * @param string $groupSymbol
     * @return $this
     */
    public function setGroupSymbol($groupSymbol)
    {
        return $this->setData(self::GROUP_SYMBOL, $groupSymbol);
    }
    /**
     * Get group length
     *
     * @api
     * @return int
     */
    public function getGroupLength()
    {
        return $this->getData(self::GROUP_LENGTH);
    }

    /**
     * Set group length
     *
     * @api
     * @param int $groupLength
     * @return $this
     */
    public function setGroupLength($groupLength)
    {
        return $this->setData(self::GROUP_LENGTH, $groupLength);
    }
    /**
     * Get integer required
     *
     * @api
     * @return int
     */
    public function getIntegerRequired()
    {
        return $this->getData(self::INTEGER_REQUIRED);
    }

    /**
     * Set integer required
     *
     * @api
     * @param int $integerRequired
     * @return $this
     */
    public function setIntegerRequired($integerRequired)
    {
        return $this->setData(self::INTEGER_REQUIRED, $integerRequired);
    }
    /**
     * Get pattern
     *
     * @api
     * @return string
     */
    public function getPattern()
    {
        return $this->getData(self::PATTERN);
    }

    /**
     * Set pattern
     *
     * @api
     * @param string $pattern
     * @return $this
     */
    public function setPattern($pattern)
    {
        return $this->setData(self::PATTERN, $pattern);
    }
    /**
     * Get precision
     *
     * @api
     * @return int
     */
    public function getPrecision()
    {
        return $this->getData(self::PRECISION);
    }

    /**
     * Set precision
     *
     * @api
     * @param int $precision
     * @return $this
     */
    public function setPrecision($precision)
    {
        return $this->setData(self::PRECISION, $precision);
    }
    /**
     * Get required precision
     *
     * @api
     * @return int
     */
    public function getRequiredPrecision()
    {
        return $this->getData(self::REQUIRED_PRECISION);
    }

    /**
     * Set required precision
     *
     * @api
     * @param int $requiredPrecision
     * @return $this
     */
    public function setRequiredPrecision($requiredPrecision)
    {
        return $this->setData(self::REQUIRED_PRECISION, $requiredPrecision);
    }
}
