<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Config\Data;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Config\ShippingInterface;

/**
 * Config Model - Shipping
 */
class Shipping extends DataObject implements ShippingInterface
{
    /**
     * @inheritdoc
     */
    public function getShippingMethods()
    {
        return $this->getData(self::SHIPPING_METHODS);
    }

    /**
     * @inheritdoc
     */
    public function setShippingMethods($shippingMethod)
    {
        return $this->setData(self::SHIPPING_METHODS, $shippingMethod);
    }

    /**
     * @inheritdoc
     */
    public function getDeliveryDate()
    {
        return $this->getData(self::DELIVERY_DATE);
    }

    /**
     * @inheritdoc
     */
    public function setDeliveryDate($deliveryDate)
    {
        return $this->setData(self::DELIVERY_DATE, $deliveryDate);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultShippingTitle()
    {
        return $this->getData(self::DEFAULT_SHIPPING_TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setDefaultShippingTitle($defaultShippingTitle)
    {
        return $this->setData(self::DEFAULT_SHIPPING_TITLE, $defaultShippingTitle);
    }
}
