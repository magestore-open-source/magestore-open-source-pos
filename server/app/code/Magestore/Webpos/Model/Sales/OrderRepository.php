<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Sales;

use Exception;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\ServiceInputProcessor;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Webpos\Api\Data\Log\DataLogStringResultsInterface;
use Magestore\Webpos\Api\Data\Sales\OrderSearchResultInterfaceFactory;
use Magestore\Webpos\Api\Sales\OrderRepositoryInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Log\Logger;
use Magestore\Webpos\Model\Checkout\PosOrder;
use Magestore\Webpos\Model\Log\DataLogResults;
use Magestore\Webpos\Model\Request\ActionLog;
use Magestore\Webpos\Model\Request\ActionLogFactory;
use Magestore\Webpos\Model\Request\Actions\CancelAction;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\CollectionFactory;
use Magestore\Webpos\Model\Sales\Order\EmailSender;
use Magestore\Webpos\Model\Source\Adminhtml\Since;
use Magento\Framework\Api\SortOrder;
use Magestore\Webpos\Api\Data\Checkout\Order\CommentInterface;

/**
 * Class OrderRepository
 *
 * Used for order repository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OrderRepository implements OrderRepositoryInterface
{
    /**
     * @var OrderSearchResultInterfaceFactory
     */
    protected $searchResultFactory;
    /**
     * @var \Magestore\Webpos\Model\Checkout\OrderFactory
     */
    protected $orderFactory;
    /**
     * @var OrderFactory
     */
    protected $_orderFactory;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var EmailSender
     */
    protected $orderSender;
    /**
     * @var OrderManagementInterface
     */
    protected $orderManagement;
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var \Magestore\Webpos\Helper\Order
     */
    protected $orderHelper;
    /**
     * @var DataLogStringResultsInterface $dataLogResults
     */
    protected $dataLogResults;
    /**
     * @var ActionLogFactory
     */
    protected $actionLogFactory;
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var ServiceInputProcessor
     */
    protected $serviceInputProcessor;

    /**
     * OrderRepository constructor.
     *
     * @param OrderSearchResultInterfaceFactory $searchResultFactory
     * @param \Magestore\Webpos\Model\Checkout\OrderFactory $orderFactory
     * @param OrderFactory $_orderFactory
     * @param CollectionFactory $collectionFactory
     * @param Data $helper
     * @param Order\EmailSender $orderSender
     * @param OrderManagementInterface $orderManagement
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param ObjectManagerInterface $objectManager
     * @param \Magestore\Webpos\Helper\Order $orderHelper
     * @param DataLogResults $dataLogResults
     * @param ActionLogFactory $actionLogFactory
     * @param RequestInterface $request
     * @param Logger $logger
     * @param ServiceInputProcessor $serviceInputProcessor
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        OrderSearchResultInterfaceFactory $searchResultFactory,
        \Magestore\Webpos\Model\Checkout\OrderFactory $orderFactory,
        OrderFactory $_orderFactory,
        CollectionFactory $collectionFactory,
        Data $helper,
        EmailSender $orderSender,
        OrderManagementInterface $orderManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        ObjectManagerInterface $objectManager,
        \Magestore\Webpos\Helper\Order $orderHelper,
        DataLogResults $dataLogResults,
        ActionLogFactory $actionLogFactory,
        RequestInterface $request,
        Logger $logger,
        ServiceInputProcessor $serviceInputProcessor
    ) {
        $this->searchResultFactory = $searchResultFactory;
        $this->orderFactory = $orderFactory;
        $this->_orderFactory = $_orderFactory;
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->orderSender = $orderSender;
        $this->orderManagement = $orderManagement;
        $this->_orderRepository = $orderRepository;
        $this->_objectManager = $objectManager;
        $this->orderHelper = $orderHelper;
        $this->dataLogResults = $dataLogResults;
        $this->actionLogFactory = $actionLogFactory;
        $this->request = $request;
        $this->logger = $logger;
        $this->serviceInputProcessor = $serviceInputProcessor;
    }

    /**
     * Get order by id
     *
     * @param int $id
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws Exception
     */
    public function get($id)
    {
        if (!$id) {
            throw new LocalizedException(__('Id required'));
        }
        /** @var OrderInterface $entity */
        $order = $this->orderFactory->create()->load($id);
        if (!$order->getEntityId()) {
            throw new LocalizedException(__('Requested entity doesn\'t exist'));
        }
        return $order;
    }

    /**
     * Find entities by criteria
     *
     * @param SearchCriteria $searchCriteria
     * @return \Magestore\Webpos\Api\Data\Sales\OrderSearchResultInterface Order search result interface.
     * @throws Exception
     */
    public function sync(SearchCriteria $searchCriteria)
    {
        $collection = $this->getOrderCollection($searchCriteria);
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        return $searchResult;
    }

    /**
     * Check order permission
     *
     * @param SearchCriteria $searchCriteria
     * @return DataLogStringResultsInterface
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function outOfPermission(SearchCriteria $searchCriteria)
    {
        // another permission
        $collection = $this->collectionFactory->create();
        $lastTime = $this->getSearchDays();

        $updatedAt = $lastTime;

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() == 'updated_at') {
                    $updatedAt = $filter->getValue();
                }
            }
        }

        $collection->getSelect()->where(
            '(main_table.state != "' . Order::STATE_HOLDED . '" 
            AND main_table.created_at >= "' . $lastTime . '"
            AND main_table.updated_at >= "' . $updatedAt . '")'
        );

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $this->addFilterGroupToCollection($filterGroup, $collection);
        }
        $collection->ignoreByCurrentStockAndSource();
        $collection->getSelect()->group('main_table.entity_id');

        $orderIds = [];
        foreach ($collection as $order) {
            $orderIds[] = $order->getIncrementId();
        }
        $this->dataLogResults->setIds($orderIds);
        return $this->dataLogResults;
    }

    /**
     * Get order collection
     *
     * @param SearchCriteria $searchCriteria
     * @return mixed
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function getOrderCollection($searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $lastTime = $this->getSearchDays();

        $holdState = Order::STATE_HOLDED;

        $locationId = $this->helper->getCurrentLocationId();

        $collection->getSelect()->where(
            '(main_table.state = "' . $holdState . '" AND main_table.pos_location_id = "' . $locationId . '") OR 
            (main_table.state != "' . $holdState . '" AND main_table.created_at >= "' . $lastTime . '")'
        );

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $this->addFilterGroupToCollection($filterGroup, $collection);
        }

        if ($searchCriteria->getCurrentPage()) {
            $collection->setCurPage($searchCriteria->getCurrentPage());
        }
        if ($searchCriteria->getPageSize()) {
            $collection->setPageSize($searchCriteria->getPageSize());
        }

        if ($searchCriteria->getSortOrders()) {
            foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            $collection->addOrder(
                'created_at',
                'DESC'
            );
        }

        $collection->filterByStockAndSource();
        $collection->getSelect()->group('main_table.entity_id');

        return $collection;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param OrderSearchResultInterface $searchResult
     * @return void
     * @throws InputException
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        OrderSearchResultInterface $searchResult
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $conditions[] = [$condition => $filter->getValue()];
            $fields[] = $filter->getField();
            if ($filter->getConditionType() == 'like' && strpos($filter->getValue(), '@') === false) {
                $values = explode(' ', $filter->getValue());
                foreach ($values as $value) {
                    if (strlen($value) > 2) {
                        $conditions[] = [$condition => $value];
                        $fields[] = $filter->getField();
                    }
                }
            }
        }
        if ($fields) {
            $searchResult->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * Get period search from config
     *
     * @return string
     */
    public function getSearchDays()
    {
        $config = $this->helper->getStoreConfig('webpos/offline/order_since');
        $time = time();
        switch ($config) {
            case Since::SINCE_24H:
                $lastTime = $time - 60 * 60 * 24 * 1;
                return date('Y-m-d H:i:s', $lastTime);
            case Since::SINCE_7DAYS:
                $lastTime = $time - 60 * 60 * 24 * 7;
                return date('Y-m-d H:i:s', $lastTime);
            case Since::SINCE_MONTH:
                return date('Y-m-01 00:00:00');
            case Since::SINCE_YTD:
                return date('Y-01-01 00:00:00');
            case Since::SINCE_2YTD:
                $year = date("Y") - 1;
                return date($year . '-01-01 00:00:00');
            default:
                $lastTime = $time - 60 * 60 * 24 * 7;
                return date('Y-m-d H:i:s', $lastTime);
        }
    }

    /**
     * Get order by increment id
     *
     * @param string $incrementId
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws Exception
     */
    public function getWebposOrderByIncrementId($incrementId)
    {
        if (!$incrementId) {
            throw new LocalizedException(__('Id required'));
        }
        /** @var \Magestore\Webpos\Api\Data\Checkout\OrderInterface $entity */
        $order = $this->orderFactory->create()->load($incrementId, 'increment_id');
        if (!$order->getEntityId()) {
            throw new LocalizedException(__('Requested entity doesn\'t exist'));
        }
        return $order;
    }

    /**
     * Get webpos order by increment id
     *
     * @param string $incrementId
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws Exception
     */
    public function getByIncrementId($incrementId)
    {
        if (!$incrementId) {
            throw new LocalizedException(__('Id required'));
        }
        /** @var \Magestore\Webpos\Api\Data\Checkout\OrderInterface $order */
        $order = $this->getWebposOrderByIncrementId($incrementId);
        if (!$order->getEntityId()) {
            throw new LocalizedException(__('Requested entity doesn\'t exist'));
        }
        return $this->orderHelper->verifyOrderReturn($order);
    }

    /**
     * Get magento order by increment id
     *
     * @param string $incrementId
     * @return OrderInterface
     * @throws Exception
     */
    public function getMagentoOrderByIncrementId($incrementId)
    {
        if (!$incrementId) {
            throw new LocalizedException(__('Id required'));
        }
        /** @var \Magestore\Webpos\Api\Data\Checkout\OrderInterface $order */
        $order = $this->getWebposOrderByIncrementId($incrementId);
        /** @var OrderInterface $order */
        $order = $this->_orderRepository->get($order->getEntityId());
        if (!$order->getEntityId()) {
            throw new LocalizedException(__('Requested entity doesn\'t exist'));
        }
        return $order;
    }

    /**
     * Get order by id
     *
     * @param int $id
     * @return OrderInterface
     * @throws Exception
     */
    public function getById($id)
    {
        if (!$id) {
            throw new LocalizedException(__('Id required'));
        }
        /** @var OrderInterface $entity */
        $order = $this->_orderFactory->create()->load($id);
        if (!$order->getEntityId()) {
            throw new LocalizedException(__('Requested entity doesn\'t exist'));
        }
        return $order;
    }

    /**
     * Send order email
     *
     * @param string $incrementId
     * @param string $email
     * @return boolean
     * @throws Exception
     */
    public function sendEmail($incrementId, $email)
    {
        $order = $this->getMagentoOrderByIncrementId($incrementId);
        $order = $this->getById($order->getEntityId());
        if ($order) {
            $emailSender = $this->orderSender;
            $order->setCustomerEmail($email);
            try {
                $emailSender->send($order);
                return true;
            } catch (Exception $e) {
                throw new LocalizedException(__('Can not send email'));
            }
        }
        return true;
    }

    /**
     * Comment order
     *
     * @param string $incrementId
     * @param CommentInterface $comment
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function commentOrder($incrementId, $comment)
    {
        $order = $this->getMagentoOrderByIncrementId($incrementId);
        if ($order) {
            $newOrder = $this->comment($order, $comment);
            return $newOrder;
        } else {
            throw new LocalizedException(__('Cannot add order comment'));
        }
    }

    /**
     * Comment order
     *
     * @param OrderInterface $order
     * @param CommentInterface $comment
     * @return OrderInterface|\Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws Exception
     */
    public function comment($order, $comment)
    {
        $this->saveComment($order, $comment);
        $newOrder = $this->get($order->getId());
        return $newOrder;
    }

    /**
     * Save Comment order
     *
     * New function to save comment order without reload order (get($orderId)) (optimize query)
     *
     * @param OrderInterface $order
     * @param CommentInterface $comment
     * @return void
     * @throws Exception
     */
    public function saveComment($order, $comment)
    {
        $history = $order->addCommentToStatusHistory($comment->getComment(), $order->getStatus());
        $history->setIsVisibleOnFront($comment->getIsVisibleOnFront());
        $history->setCreatedAt($comment->getCreatedAt());
        $history->setIsCustomerNotified(0);
        try {
            $history->save();
        } catch (Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
    }

    /**
     * @inheritDoc
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function cancelOrder($incrementId, $comment, $requestIncrementId)
    {
        try {
            /** @var \Magestore\Webpos\Api\Data\Checkout\OrderInterface $existedOrder */
            $existedOrder = $this->getWebposOrderByIncrementId($incrementId);
        } catch (Exception $e) {
            $existedOrder = false;
        }
        if (!$existedOrder || !$existedOrder->getEntityId()) {
            throw new LocalizedException(
                __('The order that you want to cancel has not been converted successfully!'),
                new Exception(),
                DispatchServiceInterface::EXCEPTION_CODE_SAVED_REQUEST_TO_SERVER
            );
        }

        try {
            $result = $this->processCancelOrderActionLog($requestIncrementId);

            if (!$result) {
                throw new LocalizedException(
                    __('Some things went wrong when trying to process cancel request!'),
                    new Exception(),
                    DispatchServiceInterface::EXCEPTION_CODE_SAVED_REQUEST_TO_SERVER
                );
            }

            return $result;
        } catch (Exception $e) {
            throw new LocalizedException(
                __('Some things went wrong when trying to process cancel request!'),
                new Exception(),
                DispatchServiceInterface::EXCEPTION_CODE_SAVED_REQUEST_TO_SERVER
            );
        }
    }

    /**
     * Process cancel order
     *
     * @param \Magestore\Webpos\Api\Data\Checkout\OrderInterface $order
     * @param string $requestIncrementId
     * @param CommentInterface|null $comment
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @deprecated
     */
    public function cancel($order, $requestIncrementId, $comment = '')
    {
        if ($order) {
            if ($order->canCancel()) {
                try {
                    $order->cancel();
                    $this->_orderRepository->save($order);
                    if ($comment && isset($comment['comment'])) {
                        $this->saveComment($order, $comment);
                    }

                    return $this->get($order->getId());
                } catch (Exception $e) {
                    throw new LocalizedException(__($e->getMessage()));
                }
            }
        }
        throw new LocalizedException(__('Cannot cancel order'));
    }

    /**
     * @inheritDoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function processCancelOrderActionLog($requestIncrementId)
    {
        /** @var ActionLog $actionLog */
        $actionLog = $this->actionLogFactory->create();
        $actionLog->load($requestIncrementId, 'request_increment_id');
        if (!$actionLog->getId() ||
            $actionLog->getActionType() != CancelAction::ACTION_TYPE ||
            $actionLog->getStatus() == ActionLog::STATUS_COMPLETED) {
            return false;
        }

        // Modify request params
        $requestParams = $this->request->getParams();
        $requestLocationId = $actionLog->getLocationId();
        $requestParams[PosOrder::PARAM_ORDER_LOCATION_ID] = $requestLocationId;
        $this->request->setParams($requestParams);
        // End: Modify request params

        // Convert array to object parameter
        $params = json_decode($actionLog->getParams(), true);
        $params = $this->serviceInputProcessor->process(
            OrderRepositoryInterface::class,
            'cancelOrder',
            $params
        );
        $incrementId = $params[0];
        $comment = $params[1];
        // End: Convert array to object parameter

        ////////////////////////////////
        /// Process Cancel Order
        ////////////////////////////////
        try {
            $order = $this->getMagentoOrderByIncrementId($incrementId);

            if ($order->canCancel()) {
                $order->cancel();
                $this->_orderRepository->save($order);
                if ($comment && isset($comment['comment'])) {
                    $this->saveComment($order, $comment);
                }

                $newOrder = $this->get($order->getId());

                // Update action log
                $actionLog->setStatus(ActionLog::STATUS_COMPLETED)->save();

                return $newOrder;
            } else {
                // Update action log
                $actionLog->setStatus(ActionLog::STATUS_COMPLETED)->save();

                return $this->get($order->getId());
            }
        } catch (Exception $e) {
            $this->logger->info($incrementId);
            $this->logger->info($e->getMessage());
            $this->logger->info($e->getTraceAsString());
            $this->logger->info('___________________________________________');
            // Update action log
            $actionLog->setStatus(ActionLog::STATUS_FAILED)->save();
            return false;
        }
    }

    /**
     * Un-hold order
     *
     * @param string $incrementId
     * @param CommentInterface $comment
     * @return \Magestore\Webpos\Api\Data\Checkout\OrderInterface
     * @throws Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function unholdOrder($incrementId, $comment = '')
    {
        $order = $this->getMagentoOrderByIncrementId($incrementId);
        if ($order && $order->getStatus() == Order::STATE_HOLDED) {
            try {
                if ($order->canUnhold()) {
                    $order->unhold();
                    $this->_orderRepository->save($order);
                } else {
                    throw new LocalizedException(__('Can\'t unhold order.'));
                }

                if ($order->canCancel()) {
                    $order->cancel();
                    $this->_orderRepository->save($order);
                }

                if ($comment && isset($comment['comment'])) {
                    $this->saveComment($order, $comment);
                }

                $newOrder = $this->get($order->getId());
                return $newOrder;
            } catch (Exception $e) {
                throw new LocalizedException(__($e->getMessage()));
            }
        }
        throw new LocalizedException(__('Cannot cancel order'));
    }

    /**
     * Delete order
     *
     * @param string $incrementId
     * @return boolean
     * @throws Exception
     */
    public function deleteOrder($incrementId)
    {
        $order = $this->getMagentoOrderByIncrementId($incrementId);
        if ($order && $order->getStatus() == Order::STATE_HOLDED) {
            $this->_orderRepository->delete($order);
            return true;
        }
        throw new LocalizedException(__('Cannot delete order'));
    }
}
