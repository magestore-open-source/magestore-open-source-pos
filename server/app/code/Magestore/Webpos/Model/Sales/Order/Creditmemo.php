<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Sales\Order;

use Magento\Framework\App\ObjectManager;
use Magestore\Webpos\Api\Data\Sales\Order\Creditmemo\ItemInterface;
use Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\Creditmemo\Item\CollectionFactory;

/**
 * Pos - Creditmemo
 */
class Creditmemo extends \Magento\Sales\Model\Order\Creditmemo implements CreditmemoInterface
{

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(\Magestore\Webpos\Model\ResourceModel\Sales\Order\Creditmemo::class);
    }

    /**
     * @inheritDoc
     */
    public function getOrderIncrementId()
    {
        return $this->getData(self::ORDER_INCREMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderIncrementId($orderIncrementId)
    {
        return $this->setData(self::ORDER_INCREMENT_ID, $orderIncrementId);
    }

    /**
     * @inheritdoc
     */
    public function getPosLocationId()
    {
        return $this->getData(self::POS_LOCATION_ID);
    }

    /**
     * @inheritdoc
     */
    public function setPosLocationId($posLocationId)
    {
        return $this->setData(self::POS_LOCATION_ID, $posLocationId);
    }

    /**
     * Sets credit memo items.
     *
     * @param ItemInterface[] $items
     * @return $this
     */
    public function setItems($items)
    {
        return $this->setData(self::ITEMS, $items);
    }

    /**
     * @inheritDoc
     */
    public function getItemsCollection()
    {
        $objectManager = ObjectManager::getInstance();
        $itemCollection = $objectManager
            ->get(CollectionFactory::class)
            ->create();
        $collection = $itemCollection->setCreditmemoFilter($this->getId());

        if ($this->getId()) {
            foreach ($collection as $item) {
                $item->setCreditmemo($this);
            }
        }
        return $collection;
    }

    /**
     * @inheritDoc
     */
    public function getPayments()
    {
        return $this->getData(self::PAYMENTS);
    }

    /**
     * @inheritDoc
     */
    public function setPayments($payment)
    {
        return $this->setData(self::PAYMENTS, $payment);
    }
}
