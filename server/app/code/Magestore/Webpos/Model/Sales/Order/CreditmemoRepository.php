<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Sales\Order;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Framework\Webapi\ServiceInputProcessor;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\RefundAdapterInterface;
use Magento\Sales\Model\Service\CreditmemoService;
use Magento\Tax\Model\Config;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Webpos\Api\Data\Checkout\Order\PaymentInterface;
use Magestore\Webpos\Api\Data\Checkout\Order\PaymentInterfaceFactory;
use Magestore\Webpos\Api\Data\Checkout\OrderInterface;
use Magestore\Webpos\Api\Data\Checkout\OrderInterfaceFactory;
use Magestore\Webpos\Api\Data\Customer\CustomerInterface;
use Magestore\Webpos\Api\Sales\Order\CreditmemoRepositoryInterface;
use Magestore\Webpos\Api\Sales\OrderRepositoryInterface;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;
use Magestore\Webpos\Log\Logger;
use Magestore\Webpos\Model\Checkout\Order\Payment;
use Magestore\Webpos\Model\Checkout\PosOrder;
use Magestore\Webpos\Model\Customer\CustomerRepository;
use Magestore\Webpos\Model\Request\ActionLog;
use Magestore\Webpos\Model\Request\ActionLogFactory;
use Magestore\Webpos\Model\Request\Actions\RefundAction;
use Magestore\Webpos\Model\Sales\OrderRepository;

/**
 * Pos creditmemo repository
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class CreditmemoRepository implements CreditmemoRepositoryInterface
{
    /**
     * @var CreditmemoService
     */
    protected $creditmemoService;

    /**
     * @var \Magento\Sales\Api\CreditmemoRepositoryInterface
     */
    protected $creditmemoRepository;

    /**
     * @var CreditmemoLoader
     */
    protected $creditmemoLoader;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var CreditmemoManagementInterface
     */
    protected $creditmemoManagement;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var InvoiceRepositoryInterface
     */
    protected $invoiceRepositoryInterface;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var RefundAdapterInterface
     */
    private $refundAdapter;

    /**
     * @var \Magento\Sales\Model\Order\Creditmemo
     */
    private $creditmemo;

    /**
     * @var \Magestore\Webpos\Model\Sales\Order\Creditmemo\EmailSender
     */
    private $creditmemoSender;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepositoryInterface;

    /**
     * @var OrderInterfaceFactory
     */
    protected $posOrderInterfaceFactory;

    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var PaymentInterfaceFactory
     */
    protected $paymentInterfaceFactory;
    /**
     * @var ManagerInterface
     */
    protected $_eventManager;
    /**
     * @var OrderRepository
     */
    protected $posOrderRepository;
    /**
     * @var ServiceInputProcessor
     */
    protected $serviceInputProcessor;
    /**
     * @var ActionLogFactory
     */
    protected $actionLogFactory;
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * CreditmemoRepository constructor.
     *
     * @param CreditmemoService $creditmemoService
     * @param \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemoRepository
     * @param CreditmemoLoader $creditmemoLoader
     * @param RequestInterface $request
     * @param CreditmemoManagementInterface $creditmemoManagement
     * @param ResourceConnection $resource
     * @param InvoiceRepositoryInterface $invoiceRepositoryInterface
     * @param PriceCurrencyInterface $priceCurrency
     * @param RefundAdapterInterface $refundAdapter
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @param Creditmemo\EmailSender $creditmemoSender
     * @param CustomerRepository $customerRepository
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param OrderRepositoryInterface $orderRepositoryInterface
     * @param OrderInterfaceFactory $posOrderInterfaceFactory
     * @param SessionRepositoryInterface $sessionRepository
     * @param Registry $coreRegistry
     * @param ObjectManagerInterface $objectManager
     * @param PaymentInterfaceFactory $paymentInterfaceFactory
     * @param ManagerInterface $_eventManager
     * @param OrderRepository $posOrderRepository
     * @param ServiceInputProcessor $serviceInputProcessor
     * @param ActionLogFactory $actionLogFactory
     * @param Logger $logger
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        CreditmemoService $creditmemoService,
        \Magento\Sales\Api\CreditmemoRepositoryInterface $creditmemoRepository,
        CreditmemoLoader $creditmemoLoader,
        RequestInterface $request,
        CreditmemoManagementInterface $creditmemoManagement,
        ResourceConnection $resource,
        InvoiceRepositoryInterface $invoiceRepositoryInterface,
        PriceCurrencyInterface $priceCurrency,
        RefundAdapterInterface $refundAdapter,
        \Magento\Sales\Model\Order\Creditmemo $creditmemo,
        \Magestore\Webpos\Model\Sales\Order\Creditmemo\EmailSender $creditmemoSender,
        CustomerRepository $customerRepository,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        OrderRepositoryInterface $orderRepositoryInterface,
        OrderInterfaceFactory $posOrderInterfaceFactory,
        SessionRepositoryInterface $sessionRepository,
        Registry $coreRegistry,
        ObjectManagerInterface $objectManager,
        PaymentInterfaceFactory $paymentInterfaceFactory,
        ManagerInterface $_eventManager,
        OrderRepository $posOrderRepository,
        ServiceInputProcessor $serviceInputProcessor,
        ActionLogFactory $actionLogFactory,
        Logger $logger
    ) {
        $this->creditmemoService = $creditmemoService;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->creditmemoLoader = $creditmemoLoader;
        $this->request = $request;
        $this->creditmemoManagement = $creditmemoManagement;
        $this->resource = $resource;
        $this->invoiceRepositoryInterface = $invoiceRepositoryInterface;
        $this->priceCurrency = $priceCurrency;
        $this->refundAdapter = $refundAdapter;
        $this->creditmemo = $creditmemo;
        $this->creditmemoSender = $creditmemoSender;
        $this->customerRepository = $customerRepository;
        $this->orderRepository = $orderRepository;
        $this->orderRepositoryInterface = $orderRepositoryInterface;
        $this->posOrderInterfaceFactory = $posOrderInterfaceFactory;
        $this->sessionRepository = $sessionRepository;
        $this->coreRegistry = $coreRegistry;
        $this->objectManager = $objectManager;
        $this->paymentInterfaceFactory = $paymentInterfaceFactory;
        $this->_eventManager = $_eventManager;
        $this->posOrderRepository = $posOrderRepository;
        $this->serviceInputProcessor = $serviceInputProcessor;
        $this->actionLogFactory = $actionLogFactory;
        $this->logger = $logger;
    }

    /**
     * Prepare creditmemo to refund and save it.
     *
     * @param \Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface $creditmemo
     * @return OrderInterface
     * @throws LocalizedException
     */
    public function createCreditmemoByOrderId($creditmemo)
    {
        try {
            /** @var OrderInterface $existedOrder */
            $existedOrder = $this->posOrderRepository->getWebposOrderByIncrementId($creditmemo->getOrderIncrementId());
        } catch (Exception $e) {
            $existedOrder = false;
        }
        if (!$existedOrder || !$existedOrder->getEntityId()) {
            throw new LocalizedException(
                __('The order that you want to create credit memo has not been converted successfully!'),
                new Exception(),
                DispatchServiceInterface::EXCEPTION_CODE_SAVED_REQUEST_TO_SERVER
            );
        }

        $requestIncrementId = $creditmemo->getIncrementId();
        try {
            $result = $this->processCreditmemoRequest($requestIncrementId);

            if (!$result) {
                throw new LocalizedException(
                    __('Some things went wrong when trying to create new credit memo!'),
                    new Exception(),
                    DispatchServiceInterface::EXCEPTION_CODE_SAVED_REQUEST_TO_SERVER
                );
            }

            return $result;
        } catch (Exception $e) {
            throw new LocalizedException(
                __('Some things went wrong when trying to create new credit memo!'),
                new Exception(),
                DispatchServiceInterface::EXCEPTION_CODE_SAVED_REQUEST_TO_SERVER
            );
        }
    }

    /**
     * @inheritDoc
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function processCreditmemoRequest($requestIncrementId)
    {
        /** @var ActionLog $actionLog */
        $actionLog = $this->actionLogFactory->create();
        $actionLog->load($requestIncrementId, 'request_increment_id');
        if (!$actionLog->getId() ||
            $actionLog->getActionType() != RefundAction::ACTION_TYPE ||
            $actionLog->getStatus() == ActionLog::STATUS_COMPLETED) {
            return false;
        }

        // Modify request params
        $requestParams = $this->request->getParams();
        $requestLocationId = $actionLog->getLocationId();
        $requestParams[PosOrder::PARAM_ORDER_LOCATION_ID] = $requestLocationId;
        $this->request->setParams($requestParams);
        // End: Modify request params

        // Convert array to object parameter
        $params = json_decode($actionLog->getParams(), true);
        $params = $this->serviceInputProcessor->process(
            CreditmemoRepositoryInterface::class,
            'createCreditmemoByOrderId',
            $params
        );
        /** @var \Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface $creditmemo */
        $creditmemo = $params[0];
        // End: Convert array to object parameter

        ////////////////////////////////
        /// Process Refund
        ////////////////////////////////
        try {
            $creditmemo = $this->modifyCreditmemoConvertingData($creditmemo);

            $this->coreRegistry->unregister('create_creditmemo_webpos');
            $this->coreRegistry->register('create_creditmemo_webpos', true);
            $this->coreRegistry->unregister('current_location_id');
            $this->coreRegistry->register('current_location_id', $requestLocationId);

            $creditmemoOffline = $creditmemo;
            $orderId = $creditmemo->getOrderId();
            $payments = $creditmemo->getPayments();
            $this->validateForRefund($creditmemoOffline);
            $data = $this->prepareCreditmemo($creditmemoOffline);
            $this->creditmemoLoader->setOrderId($data['order_id']);
            $this->creditmemoLoader->setCreditmemo($data['creditmemo']);
            $this->request->setParams($data);
            $creditmemo = $this->creditmemoLoader->load();
            if ($creditmemo) {
                if (!$creditmemo->isValidGrandTotal()) {
                    throw new LocalizedException(
                        __('The credit memo\'s total must be positive.')
                    );
                }
                if (!empty($data['creditmemo']['comment_text'])) {
                    foreach ($data['creditmemo']['comment_text'] as $commentText) {
                        $creditmemo->addComment(
                            $commentText,
                            isset($data['creditmemo']['comment_customer_notify']),
                            true
                        );
                    }
                    if (isset($data['creditmemo']['comment_text'][0])) {
                        $creditmemo->setCustomerNote($data['creditmemo']['comment_text'][0]);
                    }
                    if (isset($data['creditmemo']['comment_customer_notify'])) {
                        $creditmemo->setCustomerNoteNotify(isset($data['comment_customer_notify']));
                    }
                }
                if ($creditmemoOffline->getIncrementId()) {
                    $creditmemo->setIncrementId($creditmemoOffline->getIncrementId());
                }
                $creditmemo->setPosLocationId($requestLocationId);
                $this->creditmemoManagement->refund(
                    $creditmemo,
                    true
                );

                $order = $creditmemo->getOrder();

                if ($payments && count($payments)) {
                    $this->createWebposOrderPayment($order, $payments);
                }

                if (!empty($data['creditmemo']['send_email'])) {
                    $this->creditmemoSender->send($creditmemo);
                }
            }

            $order = $this->orderRepositoryInterface->get($orderId);

            // Update action log
            $actionLog->setStatus(ActionLog::STATUS_COMPLETED)->save();

            return $this->verifyOrderReturn($order);
        } catch (Exception $e) {
            $this->logger->info($creditmemo->getOrderIncrementId());
            $this->logger->info($e->getMessage());
            $this->logger->info($e->getTraceAsString());
            $this->logger->info('___________________________________________');
            // Update action log
            $actionLog->setStatus(ActionLog::STATUS_FAILED)->save();
            return false;
        }
    }

    /**
     * Modify credit memo data
     *
     * @param \Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface $creditmemo
     * @return \Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface
     * @throws Exception
     */
    public function modifyCreditmemoConvertingData($creditmemo)
    {
        $orderId = $creditmemo->getOrderId();
        $existedOrder = $this->posOrderRepository->getWebposOrderByIncrementId($creditmemo->getOrderIncrementId());

        if ($existedOrder->getEntityId() !== $orderId) {
            // Have to correct order data in credit memo param
            $creditmemo->setOrderId($existedOrder->getEntityId());

            $orderItems = [];
            foreach ($existedOrder->getAllItems() as $item) {
                $orderItems[$item->getTmpItemId()] = $item->getItemId();
            }

            $creditmemoItems = [];
            foreach ($creditmemo->getItems() as $cmItem) {
                if ($cmItem->getOrderItemId() && isset($orderItems[$cmItem->getOrderItemId()])) {
                    $cmItem->setOrderItemId($orderItems[$cmItem->getOrderItemId()]);
                }
                $creditmemoItems[] = $cmItem;
            }
            $creditmemo->setItems($creditmemoItems);
        }

        return $creditmemo;
    }

    /**
     * Validate for refund
     *
     * @param \Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface $creditmemo
     * @throws LocalizedException
     */
    public function validateForRefund($creditmemo)
    {
        if ($creditmemo->getId()) {
            throw new LocalizedException(
                __('We cannot register an existing credit memo.')
            );
        }
    }

    /**
     * Prepare credit memo
     *
     * @param \Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface $creditmemo
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function prepareCreditmemo(\Magestore\Webpos\Api\Data\Sales\Order\CreditmemoInterface $creditmemo)
    {
        $data = [];
        $items = $creditmemo->getItems();
        $orderId = $creditmemo->getOrderId();
        $result = new DataObject();
        if (count($items) > 0 && $orderId) {
            $data['order_id'] = $orderId;
            $creditmemoData = [];
            foreach ($items as $item) {
                $creditmemoData['items'][$item->getOrderItemId()]['qty'] = $item->getQty();
                if ($item->getBackToStock()) {
                    $creditmemoData['items'][$item->getOrderItemId()]['back_to_stock'] = 1;
                }
            }
            $creditmemoData['send_email'] = $creditmemo->getEmailSent();
            $comments = $creditmemo->getComments();
            if (count($comments)) {
                foreach ($comments as $comment) {
                    if (!isset($creditmemoData['comment_text'])) {
                        $creditmemoData['comment_text'] = [$comment->getComment()];
                    } else {
                        $creditmemoData['comment_text'][] = $comment->getComment();
                    }
                }
            }
            if ($creditmemoData['send_email']) {
                $creditmemoData['comment_customer_notify'] = 1;
            }
            /*$creditmemoData['shipping_amount'] = $creditmemo->getBaseShippingAmount();*/
            /** @var ScopeConfigInterface $scopeConfig */
            $taxConfig = $this->objectManager->create(Config::class);
            if ($taxConfig->displaySalesShippingInclTax($creditmemo->getOrder()->getStoreId())) {
                $creditmemoData['shipping_amount'] = $creditmemo->getBaseShippingInclTax();
            } else {
                $creditmemoData['shipping_amount'] = $creditmemo->getBaseShippingAmount();
            }
            $creditmemoData['adjustment_positive'] = $creditmemo->getBaseAdjustmentPositive();
            $creditmemoData['adjustment_negative'] = $creditmemo->getBaseAdjustmentNegative();
            $data['creditmemo'] = $creditmemoData;

            $result->setData($data);
            $eventData = [
                'result' => $result,
                'creditmemo' => $creditmemo
            ];
            $this->_eventManager->dispatch('pos_prepare_creditmemo_data_after', $eventData);
        }
        return $result->getData();
    }

    /**
     * Send email
     *
     * @param int $creditmemoIncrementId
     * @param string $email
     * @param string|null $incrementId
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function sendEmail($creditmemoIncrementId, $email, $incrementId = '')
    {
        $creditmemo = $this->getByIncrementId($creditmemoIncrementId);
        if ($creditmemo) {
            $emailSender = $this->creditmemoSender;
            return $emailSender->sendCreditmemoToAnotherEmail($creditmemo, $email);
        }
        return false;
    }

    /**
     * Loads a specified credit memo.
     *
     * @param int $incrementId The credit memo Increment Id.
     * @return CreditmemoInterface|null Credit memo interface.
     */
    public function getByIncrementId($incrementId)
    {
        $creditmemo = $this->creditmemo->load($incrementId, 'increment_id');
        if ($creditmemo->getId()) {
            return $creditmemo;
        }
        return null;
    }

    /**
     * Create customer
     *
     * @param CustomerInterface $customer
     * @param string $incrementId
     * @return CustomerInterface
     * @throws Exception
     */
    public function createCustomer($customer, $incrementId)
    {
        if (!$customer->getId()) {
            $customer = $this->customerRepository->save($customer);
        }
        if ($customer->getId() && $incrementId) {
            $order = $this->orderRepositoryInterface->getMagentoOrderByIncrementId($incrementId);
            $order->setCustomerId($customer->getId());
            $order->setCustomerEmail($customer->getEmail());
            $order->setCustomerGroupId($customer->getGroupId());
            $order->setCustomerFirstname($customer->getFirstname());
            $order->setCustomerLastname($customer->getLastname());
            $order->setCustomerIsGuest(0);
            try {
                $this->orderRepository->save($order);
            } catch (Exception $e) {
                $this->logger->info($e->getMessage());
            }
            return $customer;
        } else {
            throw new LocalizedException(__('Can not create customer'));
        }
    }

    /**
     * Get correct status from order
     *
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function verifyOrderReturn($order)
    {
        $posOrder = $this->posOrderInterfaceFactory->create();
        $posOrder->setData($order->getData());
        $posOrder->setAddresses($order->getAddresses());
        $posOrder->setPayments($order->getPayments());
        $posOrder->setItems($order->getItems());
        $posOrder->setStatusHistories($order->getStatusHistories());
        return $posOrder;
    }

    /**
     * Create order payment
     *
     * @param Order $order
     * @param PaymentInterface[] $payments
     * @throws CouldNotSaveException
     */
    public function createWebposOrderPayment(&$order, $payments)
    {
        if (count($payments)) {
            /** @var PaymentInterface $payment */
            foreach ($payments as $payment) {
                $data = $payment->getData();
                $data['title'] = $payment->getTitle();
                $data['base_amount_paid'] = $payment->getBaseAmountPaid();
                $data['amount_paid'] = $payment->getAmountPaid();
                /** @var Payment $paymentModel */
                $paymentModel = $this->paymentInterfaceFactory->create();
                $paymentModel->setData($data);
                $paymentModel->setOrderId($order->getId());
                $this->_eventManager->dispatch(
                    'creditmemo_webpos_payment_save_before',
                    [
                        'webpos_payment' => $paymentModel,
                        'payment_data' => $payment
                    ]
                );
                try {
                    $paymentModel->getResource()->save($paymentModel);
                } catch (Exception $exception) {
                    throw new CouldNotSaveException(__($exception->getMessage()));
                }
            }
        }
    }
}
