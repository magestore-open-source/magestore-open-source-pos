<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Sales\Order\Creditmemo;

use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;
use Magestore\Webpos\Model\Checkout\Order;

/**
 * Class InvoiceService
 */
class EmailSender extends CreditmemoSender
{

    /**
     * Set the mail receptient
     *
     * @param string $email
     * @param string $name
     */
    public function setRecepient($email, $name)
    {
        $this->identityContainer->setCustomerEmail($email);
        $this->identityContainer->setCustomerName($name);
    }

    /**
     * Get the payment email HTML
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentEmailHtml($order)
    {
        return $this->getPaymentHtml($order);
    }

    /**
     * Send the creditmemo to another email
     *
     * @param CreditmemoInterface $creditmemo
     * @param string $email
     * @return bool
     */
    public function sendCreditmemoToAnotherEmail($creditmemo, $email)
    {
        $order = $creditmemo->getOrder();
        $order->setCustomerEmail($email);
        $creditmemo->setOrder($order);

        return parent::send($creditmemo, true);
    }
}
