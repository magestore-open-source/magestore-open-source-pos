<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Sales\Order;

use Magento\Framework\Api\SearchResults;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;
use Magestore\Webpos\Api\Sales\Order\StatusRepositoryInterface;

/**
 * Order - OrderRepository
 */
class StatusRepository implements StatusRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    protected $statusCollectionFactory;

    /**
     * @var SearchResults
     */
    protected $statusResultInterface;

    /**
     * OrderRepository constructor.
     * @param CollectionFactory $statusCollectionFactory
     * @param SearchResults $statusResultInterface
     */
    public function __construct(
        CollectionFactory $statusCollectionFactory,
        SearchResults $statusResultInterface
    ) {
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->statusResultInterface = $statusResultInterface;
    }

    /**
     * @inheritDoc
     */
    public function getStatuses()
    {
        $collection = $this->statusCollectionFactory->create();
        $collection->joinStates();
        $statuses = $this->statusResultInterface;
        $statuses->setItems($collection->getData());
        $statuses->setTotalCount($collection->getSize());
        return $statuses;
    }
}
