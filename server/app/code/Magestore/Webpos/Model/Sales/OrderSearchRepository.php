<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Sales;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;
use Magestore\Webpos\Api\Data\Sales\OrderSearchResultInterface;
use Magestore\Webpos\Api\SearchCriteriaInterface;
use Magestore\Webpos\Helper\Profiler;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\Collection;
use Magestore\Webpos\Model\Source\Adminhtml\Since;
use Magestore\Webpos\Api\Sales\OrderSearchRepositoryInterface;

/**
 * Model OrderSearchRepository
 */
class OrderSearchRepository extends OrderRepository implements OrderSearchRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function search(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->getOrderCollection($searchCriteria);
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setItems($collection->getItems());
        return $searchResult;
    }

    /**
     * Get Order Collection
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return Collection
     * @throws NoSuchEntityException
     */
    public function getOrderCollection($searchCriteria)
    {
        Profiler::start('search');
        $collection = $this->collectionFactory->create();
        /** @var OrderSearchResultInterface $searchResult */
        if ($searchCriteria->getCurrentPage()) {
            $collection->setCurPage($searchCriteria->getCurrentPage());
        }
        if ($searchCriteria->getPageSize()) {
            $collection->setPageSize($searchCriteria->getPageSize());
        }

        $isLimit = $searchCriteria->getIsLimit();
        if ($searchCriteria->getIsHold()) {
            $collection->addFieldToFilter('main_table.state', Order::STATE_HOLDED);
        } else {
            if ($isLimit) {
                $lastTime = $this->getSearchDays();
                $collection->addFieldToFilter('main_table.created_at', ['gteq' => $lastTime]);
            }
            $collection->addFieldToFilter('main_table.state', ['nin' => Order::STATE_HOLDED]);
        }
        if ($searchCriteria->getQueryString()) {
            $queryString = '%' . $searchCriteria->getQueryString() . '%';
            $collection->joinToGetSearchString($queryString);
        }
        $collection->addOrder('main_table.created_at', 'DESC');
        return $collection;
    }

    /**
     * Get period search from config
     *
     * @return string
     */
    public function getSearchDays()
    {
        $config = $this->helper->getStoreConfig('webpos/offline/order_since');
        $time = time();
        switch ($config) {
            case Since::SINCE_24H:
                $lastTime = $time - 60 * 60 * 24 * 1;
                return date('Y-m-d H:i:s', $lastTime);
            case Since::SINCE_7DAYS:
                $lastTime = $time - 60 * 60 * 24 * 7;
                return date('Y-m-d H:i:s', $lastTime);
            case Since::SINCE_MONTH:
                return date('Y-m-01 00:00:00');
            case Since::SINCE_YTD:
                return date('Y-01-01 00:00:00');
            case Since::SINCE_2YTD:
                $year = date("Y") - 1;
                return date($year . '-01-01 00:00:00');
            default:
                $lastTime = $time - 60 * 60 * 24 * 7;
                return date('Y-m-d H:i:s', $lastTime);
        }
    }
}
