<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Sales\ResourceModel\Order\Plugin;

use Closure;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use Magento\Sales\Model\Order;

/**
 * Plugin - Authorization Plugin
 */
class Authorization extends \Magento\Sales\Model\ResourceModel\Order\Plugin\Authorization
{
    /**
     * Checks if order is allowed
     *
     * @param \Magento\Sales\Model\ResourceModel\Order $subject
     * @param callable $proceed
     * @param AbstractModel $order
     * @param mixed $value
     * @param null|string $field
     * @return Order
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundLoad(
        \Magento\Sales\Model\ResourceModel\Order $subject,
        Closure $proceed,
        AbstractModel $order,
        $value,
        $field = null
    ) {
        if ($order instanceof Order) {
            $function = 'aroundLoad';
            foreach (class_parents($this) as $parent) {
                if (method_exists($parent, 'aroundLoad')) {
                    //do something, for instance:
                    return $parent::$function($subject, $proceed, $order, $value, $field);
                }
            }
        }
        return $proceed($order, $value, $field);
    }
}
