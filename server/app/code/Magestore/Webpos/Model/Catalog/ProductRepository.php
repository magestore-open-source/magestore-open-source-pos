<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Catalog;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductLinkInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\CatalogInventory\Model\Stock as Stock;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DB\Adapter\ConnectionException;
use Magento\Framework\DB\Adapter\DeadlockException;
use Magento\Framework\DB\Adapter\LockWaitException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Module\Manager;
use Magento\Framework\Registry;
use Magento\Framework\Webapi\ServiceOutputProcessor;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\InventoryIndexer\Model\StockIndexTableNameResolverInterface;
use Magento\Weee\Model\Tax;
use Magestore\Webpos\Api\Catalog\ProductRepositoryInterface;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Api\SyncInterface;
use Magestore\Webpos\Api\WebposManagementInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\Collection;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\CollectionFactory;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;
use Zend_Json;

/**
 * Catalog ProductRepository
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ProductRepository extends \Magento\Catalog\Model\ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var Manager
     */
    protected $_moduleManager;

    /**
     * @var ManagerInterface
     */
    protected $_eventManagerInterFace;

    /**
     * @var Product
     */
    protected $_webposProduct;

    /**
     * @var Collection
     */
    protected $_productCollection;
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Tax
     */
    protected $weeTax;

    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    protected $listAttributes = [
        'entity_id',
        'type_id',
        'category_ids',
        'description',
        'has_options',
        'image',
        'small_image',
        'name',
        'price',
        'sku',
        'special_from_date',
        'special_to_date',
        'special_price',
        'status',
        'tax_class_id',
        'tier_price',
        'updated_at',
        'weight'
    ];

    protected $listCondition = [
        'eq' => '=',
        'neq' => '!=',
        'like' => 'like',
        'gt' => '>',
        'gteq' => '>=',
        'lt' => '<',
        'lteq' => '<=',
        'in' => 'in'
    ];

    protected $collectionSize = 0;

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();
        $this->request = $objectManager->get(RequestInterface::class);
        $this->registry = $objectManager->get(Registry::class);
        $this->registry->register('webpos_get_product_list', true);
        $isShowStock = (boolean)$this->request->getParam('show_stock');
        $isShowOption = (boolean)$this->request->getParam('show_option');
        if ($isShowOption) {
            $this->registry->register('wp_is_show_options', true);
        } else {
            $this->registry->register('wp_is_show_options', false);
        }
        if ($isShowStock) {
            $this->registry->register('wp_is_show_stock', true);
        } else {
            $this->registry->register('wp_is_show_stock', false);
        }
        /** @var Data $helper */
        $helper = $objectManager->get(Data::class);
        $storeId = $helper->getCurrentStoreView()->getId();
        $this->prepareCollection($searchCriteria);
        $this->_productCollection->setStoreId($storeId);
        $this->_productCollection->addStoreFilter($storeId);
        $this->_productCollection->setCurPage($searchCriteria->getCurrentPage());
        $this->_productCollection->setPageSize($searchCriteria->getPageSize());
        $searchResult = $this->searchResultsFactory->create();
        $collectionSize = $this->_productCollection->getSize();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($this->_productCollection->getItems());
        $searchResult->setTotalCount($collectionSize);
        return $searchResult;
    }

    /**
     * @inheritDoc
     */
    public function sync(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();

        /** @var StateInterface $cacheState */
        $cacheState = $objectManager->get(StateInterface::class);
        if (!$cacheState->isEnabled(\Magestore\Webpos\Model\Cache\Type::TYPE_IDENTIFIER)
            || count($searchCriteria->getFilterGroups())) {
            return $this->getList($searchCriteria);
        }

        /** @var CacheInterface $cache */
        $cache = $objectManager->get(CacheInterface::class);

        $this->request = $objectManager->get(RequestInterface::class);

        $this->_moduleManager = $objectManager->get(Manager::class);
        $locationId = Stock::DEFAULT_STOCK_ID;
        /** @var WebposManagementInterface $webposManagement */
        $webposManagement = $objectManager->get(WebposManagementInterface::class);
        if ($webposManagement->isMSIEnable()) {
            /** @var StockManagementInterface $stockManagement */
            $stockManagement = $objectManager->get(
                StockManagementInterface::class
            );
            $locationId = $stockManagement->getStockId();
        }

        $key = 'syncProducts-' . $searchCriteria->getPageSize()
            . '-' . $searchCriteria->getCurrentPage() . '-' . $locationId;

        if ($response = $cache->load($key)) {
            return json_decode($response, true);
        }

        // Check async queue for product
        $flag = SyncInterface::QUEUE_NAME;
        if ($cachedAt = $cache->load($flag)) {
            return [
                'async_stage' => 2, // processing
                'cached_at' => $cachedAt, // process started time
            ];
        }

        // Block metaphor
        $cachedAt = date("Y-m-d H:i:s");
        $cache->save($cachedAt, $flag, [\Magestore\Webpos\Model\Cache\Type::CACHE_TAG], 300);

        /** @var ServiceOutputProcessor $processor */
        $processor = $objectManager->get(ServiceOutputProcessor::class);
        $outputData = $processor->process(
            $this->getList($searchCriteria),
            ProductRepositoryInterface::class,
            'sync'
        );
        $outputData['cached_at'] = $cachedAt;
        $cache->save(
            json_encode($outputData),
            $key,
            [
                \Magestore\Webpos\Model\Cache\Type::CACHE_TAG,
            ],
            86400   // Cache lifetime: 1 day
        );
        // Release metaphor
        $cache->remove($flag);
        return $outputData;
    }

    /**
     * @inheritdoc
     */
    public function getProductsWithoutOptions(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();
        $this->_moduleManager = $objectManager->get(Manager::class);
        $this->_productCollection = $objectManager->create(
            Collection::class
        );
        $this->prepareCollection($searchCriteria);
        /** @var Data $helper */
        $helper = $objectManager->get(Data::class);
        $storeId = $helper->getCurrentStoreView()->getId();
        $this->_productCollection->setStoreId($storeId);
        $this->_productCollection->setCurPage($searchCriteria->getCurrentPage());
        $this->_productCollection->setPageSize($searchCriteria->getPageSize());
        $this->_productCollection->addAttributeToSelect($this->listAttributes);
        $this->_productCollection->addStoreFilter($storeId);
        $this->_productCollection->getSelect()->joinLeft(
            ['stock_item' => $this->_productCollection->getTable('cataloginventory_stock_item')],
            'e.entity_id = stock_item.product_id AND stock_item.stock_id = "' . Stock::DEFAULT_STOCK_ID . '"',
            [
                'qty',
                'manage_stock',
                'backorders',
                'min_sale_qty',
                'max_sale_qty',
                'is_in_stock',
                'enable_qty_increments',
                'qty_increments',
                'is_qty_decimal'
            ]
        );
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setTotalCount($this->_productCollection->getSize());
        $searchResult->setItems($this->_productCollection->getItems());
        return $searchResult;
    }

    /**
     * @inheritdoc
     */
    public function prepareCollection($searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();
        /** @var GetSessionByIdInterface $getSessionById */
        $getSessionById = $objectManager->get(GetSessionByIdInterface::class);
        $session = $getSessionById->execute(
            $this->request->getParam(RequestProcessor::SESSION_PARAM_KEY)
        );

        $this->weeTax = $objectManager->create(Tax::class);
        if (empty($this->_productCollection)) {
            $this->_productCollection = $objectManager->create(
                Collection::class
            );
            $this->_eventManagerInterFace = $objectManager->get(ManagerInterface::class);
            $this->_eventManagerInterFace->dispatch(
                'webpos_catalog_product_getlist',
                ['collection' => $this->_productCollection, 'is_new' => true, 'location' => $session->getLocationId()]
            );
            /** End integrate webpos **/

            $this->extensionAttributesJoinProcessor->process($this->_productCollection);
            $this->_productCollection->addAttributeToSelect($this->listAttributes);
            $weeTax = $this->weeTax->getWeeeTaxAttributeCodes();
            if (count($weeTax)) {
                $this->_productCollection->addAttributeToSelect($weeTax);
            }
            $this->_productCollection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner'
            );
            $this->_productCollection->addAttributeToFilter(
                'status',
                Status::STATUS_ENABLED
            );
            $this->_productCollection->addVisibleFilter(); // filter visible on pos
            foreach ($searchCriteria->getFilterGroups() as $group) {
                $this->addFilterGroupToCollection($group, $this->_productCollection);
            }
            /** @var SortOrder $sortOrder */
            foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $this->_productCollection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
                );
            }
            /** @var Data $webposHelper */
            $webposHelper = ObjectManager::getInstance()
                ->get(Data::class);
            $productTypeIds = $webposHelper->getProductTypeIds();
            $this->_productCollection->addAttributeToFilter('type_id', ['in' => $productTypeIds]);

            $this->filterProductByStockAndSource($this->_productCollection);
        }
    }

    /**
     * Get product attributes to select
     *
     * @return array
     */
    public function getSelectProductAtrributes()
    {
        return [
            self::TYPE_ID,
            self::NAME,
            self::PRICE,
            self::SPECIAL_PRICE,
            self::SPECIAL_FROM_DATE,
            self::SPECIAL_TO_DATE,
            self::SKU,
            self::SHORT_DESCRIPTION,
            self::DESCRIPTION,
            self::IMAGE,
            self::FINAL_PRICE
        ];
    }

    /**
     * Get product type ids to support
     *
     * @return array
     * @deprecated Moved to \Magestore\Webpos\Helper\Data
     */
    public function getProductTypeIds()
    {
        $types = [
            Type::TYPE_VIRTUAL,
            Type::TYPE_SIMPLE,
            Grouped::TYPE_CODE,
            Type::TYPE_BUNDLE,
            Configurable::TYPE_CODE
        ];
        return $types;
    }

    /**
     * Get info about product by product SKU
     *
     * @param string $id
     * @param bool $editMode
     * @param int|null $storeId
     * @param bool $forceReload
     * @return \Magestore\Webpos\Api\Data\Catalog\ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProductById($id, $editMode = false, $storeId = null, $forceReload = false)
    {
        $objectManager = ObjectManager::getInstance();
        $this->_webposProduct = $objectManager->get(Product::class);
        $this->registry = $objectManager->get(Registry::class);
        $this->registry->register('webpos_get_product_by_id', true);
        $this->registry->register('wp_is_show_stock', true);
        $this->registry->register('wp_is_show_options', true);
        $cacheKey = $this->getCacheKey([$editMode, $storeId]);
        if (!isset($this->instancesById[$id][$cacheKey]) || $forceReload) {
            $product = $this->_webposProduct;
            if ($editMode) {
                $product->setData('_edit_mode', true);
            }
            if ($storeId !== null) {
                $product->setData('store_id', $storeId);
            }
            $product->load($id);
            if (!$product->getId()) {
                throw new NoSuchEntityException(__('Requested product doesn\'t exist'));
            }
            $this->instancesById[$id][$cacheKey] = $product;
            $this->instances[$product->getSku()][$cacheKey] = $product;
        }
        return $this->instancesById[$id][$cacheKey];
    }

    /**
     * Get product options
     *
     * @param string $id
     * @param bool $editMode
     * @param int|null $storeId
     * @return string
     * @throws NoSuchEntityException
     */
    public function getOptions($id, $editMode = false, $storeId = null)
    {
        $product = $this->getProductById($id, $editMode, $storeId);
        $data = [];
        $data['custom_options'] = $this->getCustomOptions($product);
        if ($product->getTypeId() == Type::TYPE_BUNDLE) {
            $data['bundle_options'] = $product->getBundleOptions();
        }
        if ($product->getTypeId() == Configurable::TYPE_CODE) {
            $data['configurable_options'] = $product->getConfigOptions();
            /*$data['json_config'] = $product->getJsonConfig();*/
            $data['price_config'] = $product->getPriceConfig();
        }
        return Zend_Json::encode($data);
    }

    /**
     * Get custom options
     *
     * @param \Magestore\Webpos\Api\Data\Catalog\ProductInterface $product
     * @return array
     */
    public function getCustomOptions($product)
    {
        $customOptions = $product->getOptions();
        $options = [];
        foreach ($customOptions as $child) {
            $values = [];
            if ($child->getValues()) {
                foreach ($child->getValues() as $value) {
                    $values[] = $value->getData();
                }
                $child['values'] = $values;
            }
            $options[] = $child->getData();
        }
        return $options;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return void
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    ) {
        $fields = [];
        $categoryFilter = [];
        $searchString = '';
        foreach ($filterGroup->getFilters() as $filter) {
            $value = $filter->getValue();
            $conditionType = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            if ($filter->getField() == 'category_id') {
                $categoryFilter['in'][] = str_replace("%", "", $value);
                continue;
            }
            $fields[] = ['attribute' => $filter->getField(), $conditionType => $value];
            $searchString = $value ? $value : $searchString;
        }
        if ($categoryFilter && empty($fields)) {
            $collection->addCategoriesFilter($categoryFilter);
        }
        if ($fields) {
            $collection->addAttributeToFilter($fields, '', 'left');
        }
    }

    /**
     * Process product links, creating new links, updating and deleting existing links
     *
     * @param ProductInterface $product
     * @param ProductLinkInterface[] $newLinks
     * @return $this
     * @throws NoSuchEntityException
     */
    private function processLinks(ProductInterface $product, $newLinks)
    {
        if ($newLinks === null) {
            // If product links were not specified, don't do anything
            return $this;
        }

        // Clear all existing product links and then set the ones we want
        $linkTypes = $this->linkTypeProvider->getLinkTypes();
        foreach (array_keys($linkTypes) as $typeName) {
            $this->linkInitializer->initializeLinks($product, [$typeName => []]);
        }

        // Set each linktype info
        if (!empty($newLinks)) {
            $productLinks = [];
            foreach ($newLinks as $link) {
                $productLinks[$link->getLinkType()][] = $link;
            }

            foreach ($productLinks as $type => $linksByType) {
                $assignedSkuList = [];
                /** @var ProductLinkInterface $link */
                foreach ($linksByType as $link) {
                    $assignedSkuList[] = $link->getLinkedProductSku();
                }
                $linkedProductIds = $this->resourceModel->getProductsIdsBySkus($assignedSkuList);

                $linksToInitialize = [];
                foreach ($linksByType as $link) {
                    $linkDataArray = $this->extensibleDataObjectConverter
                        ->toNestedArray($link, [], ProductLinkInterface::class);
                    $linkedSku = $link->getLinkedProductSku();
                    if (!isset($linkedProductIds[$linkedSku])) {
                        throw new NoSuchEntityException(
                            __('Product with SKU "%1" does not exist', $linkedSku)
                        );
                    }
                    $linkDataArray['product_id'] = $linkedProductIds[$linkedSku];
                    $linksToInitialize[$linkedProductIds[$linkedSku]] = $linkDataArray;
                }

                $this->linkInitializer->initializeLinks($product, [$type => $linksToInitialize]);
            }
        }

        $product->setProductLinks($newLinks);
        return $this;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function save(ProductInterface $product, $saveOptions = false)
    {
        $tierPrices = $product->getData('tier_price');

        try {
            $existingProduct = $this->get($product->getSku());

            $product->setData(
                $this->resourceModel->getLinkField(),
                $existingProduct->getData($this->resourceModel->getLinkField())
            );
            if (!$product->hasData(Product::STATUS)) {
                $product->setStatus($existingProduct->getStatus());
            }
        } catch (NoSuchEntityException $e) {
            $existingProduct = null;
        }

        $productDataArray = $this->extensibleDataObjectConverter
            ->toNestedArray($product, [], ProductInterface::class);
        $productDataArray = array_replace($productDataArray, $product->getData());
        $ignoreLinksFlag = $product->getData('ignore_links_flag');
        $productLinks = null;
        if (!$ignoreLinksFlag && $ignoreLinksFlag !== null) {
            $productLinks = $product->getProductLinks();
        }
        $productDataArray['store_id'] = (int)$this->storeManager->getStore()->getId();
        $product = $this->initializeProductData($productDataArray, empty($existingProduct));

        $this->processLinks($product, $productLinks);
        if (isset($productDataArray['media_gallery'])) {
            $this->processMediaGallery($product, $productDataArray['media_gallery']['images']);
        }

        if (!$product->getOptionsReadonly()) {
            $product->setCanSaveCustomOptions(true);
        }

        $validationResult = $this->resourceModel->validate($product);
        if (true !== $validationResult) {
            throw new CouldNotSaveException(
                __('Invalid product data: %1', implode(',', $validationResult))
            );
        }

        try {
            if ($tierPrices !== null) {
                $product->setData('tier_price', $tierPrices);
            }
            unset($this->instances[$product->getSku()]);
            unset($this->instancesById[$product->getId()]);
            $this->resourceModel->save($product);
        } catch (ConnectionException $exception) {
            throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                __('Database connection error'),
                $exception,
                $exception->getCode()
            );
        } catch (DeadlockException $exception) {
            throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                __('Database deadlock found when trying to get lock'),
                $exception,
                $exception->getCode()
            );
        } catch (LockWaitException $exception) {
            throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                __('Database lock wait timeout exceeded'),
                $exception,
                $exception->getCode()
            );
        } catch (\Magento\Eav\Model\Entity\Attribute\Exception $exception) {
            throw InputException::invalidFieldValue(
                $exception->getAttributeCode(),
                $product->getData($exception->getAttributeCode()),
                $exception
            );
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (LocalizedException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Unable to save product'), $e);
        }
        unset($this->instances[$product->getSku()]);
        unset($this->instancesById[$product->getId()]);
        return $this->get($product->getSku(), false, $product->getStoreId());
    }

    /**
     * Create
     *
     * @param ProductInterface $product
     * @param bool $saveOptions
     * @return ProductInterface|\Magento\Catalog\Model\Product
     * @throws CouldNotSaveException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws \Magento\Eav\Model\Entity\Attribute\Exception
     * @throws InputException
     * @throws StateException
     * @throws \Magento\Framework\Exception\TemporaryState\CouldNotSaveException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function create(ProductInterface $product, $saveOptions = false)
    {
        /** @var Product $product */
        $tierPrices = $product->getData('tier_price');
        $productDataArray = $this->extensibleDataObjectConverter
            ->toNestedArray($product, [], ProductInterface::class);
        $productDataArray = array_replace($productDataArray, $product->getData());
        $ignoreLinksFlag = $product->getData('ignore_links_flag');
        $productLinks = null;
        if (!$ignoreLinksFlag && $ignoreLinksFlag !== null) {
            $productLinks = $product->getProductLinks();
        }
        $productDataArray['store_id'] = (int)$this->storeManager->getStore()->getId();
        $product = $this->initializeProductData($productDataArray, true);

        $this->processLinks($product, $productLinks);
        if (isset($productDataArray['media_gallery'])) {
            $this->processMediaGallery($product, $productDataArray['media_gallery']['images']);
        }

        if (!$product->getOptionsReadonly()) {
            $product->setCanSaveCustomOptions(true);
        }

        $validationResult = $this->resourceModel->validate($product);
        if (true !== $validationResult) {
            throw new CouldNotSaveException(
                __('Invalid product data: %1', implode(',', $validationResult))
            );
        }

        try {
            if ($tierPrices !== null) {
                $product->setData('tier_price', $tierPrices);
            }
            unset($this->instances[$product->getSku()]);
            unset($this->instancesById[$product->getId()]);
            $this->resourceModel->save($product);
        } catch (ConnectionException $exception) {
            throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                __('Database connection error'),
                $exception,
                $exception->getCode()
            );
        } catch (DeadlockException $exception) {
            throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                __('Database deadlock found when trying to get lock'),
                $exception,
                $exception->getCode()
            );
        } catch (LockWaitException $exception) {
            throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                __('Database lock wait timeout exceeded'),
                $exception,
                $exception->getCode()
            );
        } catch (\Magento\Eav\Model\Entity\Attribute\Exception $exception) {
            throw InputException::invalidFieldValue(
                $exception->getAttributeCode(),
                $product->getData($exception->getAttributeCode()),
                $exception
            );
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (LocalizedException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Unable to save product'), $e);
        }
        unset($this->instances[$product->getSku()]);
        unset($this->instancesById[$product->getId()]);
        return $this->get($product->getSku(), false, $product->getStoreId());
    }

    /**
     * Filter Product By Stock And Source
     *
     * @param Collection $collection
     */
    public function filterProductByStockAndSource($collection)
    {
        $objectManager = ObjectManager::getInstance();
        /** @var StockManagementInterface $stockManagement */
        $stockManagement = $objectManager->get(
            StockManagementInterface::class
        );
        $stockId = $stockManagement->getStockId();
        if ($stockId) {
            $resource = $objectManager->create(ResourceConnection::class);
            $stockTable = $objectManager->get(
                StockIndexTableNameResolverInterface::class
            )->execute($stockId);
            $sourceItemTable = $resource->getTableName('inventory_source_item');
            $linkedSources = $stockManagement->getLinkedSourceCodesByStockId($stockId);
            $collection->getSelect()
                ->joinLeft(
                    ['stock_table' => $stockTable],
                    'e.sku = stock_table.sku',
                    ['is_salable']
                )
                ->joinLeft(
                    ['inventory_source_item' => $sourceItemTable],
                    "e.sku = inventory_source_item.sku
                    AND inventory_source_item.source_code IN ('" . implode("', '", $linkedSources) . "')",
                    ['source_code']
                )->group('e.entity_id')
                ->having('inventory_source_item.source_code IN (?)', $linkedSources)
                ->orHaving('stock_table.is_salable = ?', 1);
        }
    }
}
