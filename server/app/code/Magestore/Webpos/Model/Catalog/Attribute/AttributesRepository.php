<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Catalog\Attribute;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Eav\Model\Config;
use Magento\Framework\Exception\NoSuchEntityException;
use Magestore\Webpos\Api\Catalog\Attribute\AttributesRepositoryInterface;

/**
 * Attribute model - Attributes Repository
 */
class AttributesRepository implements AttributesRepositoryInterface
{
    protected $defaultCustomSaleAttributes = [
        'type_id',
        'store_id',
        'sku',
        'name',
        'weight',
        'status',
        'visibility',
        'price',
        'description',
        'short_description'
    ];
    /**
     * @var AttributeSetRepositoryInterface
     */
    protected $setRepository;

    /**
     * @var Config
     */
    protected $eavConfig;

    /**
     * @var CollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * AttributesRepository constructor.
     * @param AttributeSetRepositoryInterface $setRepository
     * @param Config $eavConfig
     * @param CollectionFactory $attributeCollectionFactory
     */
    public function __construct(
        AttributeSetRepositoryInterface $setRepository,
        Config $eavConfig,
        CollectionFactory $attributeCollectionFactory
    ) {
        $this->setRepository = $setRepository;
        $this->eavConfig = $eavConfig;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function getCustomSaleRequireAttributes($attributeSetId)
    {
        /** @var AttributeSetInterface $attributeSet */
        $attributeSet = $this->setRepository->get($attributeSetId);
        $requiredEntityTypeId = $this->eavConfig
            ->getEntityType(ProductAttributeInterface::ENTITY_TYPE_CODE)
            ->getId();
        if (!$attributeSet->getAttributeSetId() || $attributeSet->getEntityTypeId() != $requiredEntityTypeId) {
            throw NoSuchEntityException::singleField('attributeSetId', $attributeSetId);
        }
        /** @var Collection $attributeCollection */
        $attributeCollection = $this->attributeCollectionFactory->create();
        $attributeCollection->setAttributeSetFilter($attributeSet->getAttributeSetId());
        $attributeCollection->addFieldToFilter('is_required', 1);
        $attributeCollection->addFieldToFilter('attribute_code', ['nin' => $this->defaultCustomSaleAttributes]);
        $attributeCollection->load();

        return $attributeCollection->getItems();
    }
}
