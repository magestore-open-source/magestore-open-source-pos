<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Catalog;

use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Magento\Eav\Model\Entity\Attribute\Option;
use Magento\Framework\View\Result\PageFactory;
use Magento\Swatches\Helper\Data;
use Magento\Swatches\Helper\Media;
use Magento\Swatches\Model\Swatch;
use Magestore\Webpos\Api\Catalog\SwatchOptionRepositoryInterface;
use Magestore\Webpos\Api\Data\Catalog\Option\Swatch\SwatchInterface;
use Magestore\Webpos\Api\Data\Catalog\Option\Swatch\SwatchInterfaceFactory;
use Magestore\Webpos\Api\Data\Catalog\Option\SwatchOptionInterface;
use Magestore\Webpos\Api\Data\Catalog\Option\SwatchOptionInterfaceFactory;
use Magestore\Webpos\Api\Data\Catalog\Option\SwatchOptionSearchResultsInterface;
use Magestore\Webpos\Api\Data\Catalog\Option\SwatchOptionSearchResultsInterfaceFactory;

/**
 * Model catalog SwatchOptionRepository
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class SwatchOptionRepository implements SwatchOptionRepositoryInterface
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var Data
     */
    protected $swatchHelper;
    /**
     * @var CollectionFactory
     */
    protected $attributeCollection;
    /**
     * @var SwatchOptionSearchResultsInterfaceFactory
     */
    protected $swatchOptionSearchResultsFactory;
    /**
     * @var SwatchInterface
     */
    protected $swatchInterfaceFactory;
    /**
     * @var SwatchOptionInterfaceFactory
     */
    protected $swatchOptionInterfaceFactory;
    /**
     * @var Media
     */
    protected $swatchMediaHelper;

    /**
     * SwatchOptionRepository constructor.
     *
     * @param PageFactory $resultPageFactory
     * @param Data $swatchHelper
     * @param CollectionFactory $attributeCollection
     * @param SwatchOptionSearchResultsInterfaceFactory $swatchOptionSearchResultsFactory
     * @param SwatchInterfaceFactory $swatchInterfaceFactory
     * @param SwatchOptionInterfaceFactory $swatchOptionInterfaceFactory
     * @param Media $swatchMediaHelper
     */
    public function __construct(
        PageFactory $resultPageFactory,
        Data $swatchHelper,
        CollectionFactory $attributeCollection,
        SwatchOptionSearchResultsInterfaceFactory $swatchOptionSearchResultsFactory,
        SwatchInterfaceFactory $swatchInterfaceFactory,
        SwatchOptionInterfaceFactory $swatchOptionInterfaceFactory,
        Media $swatchMediaHelper
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->swatchHelper = $swatchHelper;
        $this->attributeCollection = $attributeCollection;
        $this->swatchOptionSearchResultsFactory = $swatchOptionSearchResultsFactory;
        $this->swatchInterfaceFactory = $swatchInterfaceFactory;
        $this->swatchOptionInterfaceFactory = $swatchOptionInterfaceFactory;
        $this->swatchMediaHelper = $swatchMediaHelper;
    }

    /**
     * @inheritdoc
     */
    public function getSwatchOptions()
    {
        $swatchAttributeArray = [];
        $swatchArray = [];
        $collection = $this->attributeCollection->create();
        foreach ($collection as $attributeModel) {
            $isSwatch = $this->swatchHelper->isSwatchAttribute($attributeModel);
            if ($isSwatch) {
                $swatchAttributeArray[] = $attributeModel->getId();
                $attributeOptions = [];
                foreach ($attributeModel->getOptions() as $option) {
                    $attributeOptions[$option->getValue()] = $this->getUnusedOption($option);
                }
                $attributeOptionIds = array_keys($attributeOptions);
                $swatches = $this->swatchHelper->getSwatchesByOptionsId($attributeOptionIds);
                $data = [
                    'attribute_id' => $attributeModel->getId(),
                    'attribute_code' => $attributeModel->getAttributeCode(),
                    'attribute_label' => $attributeModel->getStoreLabel(),
                    'swatches' => $this->getSwatchOptionData($swatches),
                ];
                /** @var SwatchOptionInterface $swatch */
                $swatch = $this->swatchOptionInterfaceFactory->create();
                $swatch->setData($data);
                $swatchArray[] = $swatch;
            }
        }
        /** @var SwatchOptionSearchResultsInterface $swatchInterface */
        $swatchInterface = $this->swatchOptionSearchResultsFactory->create();
        $swatchInterface->setItems($swatchArray);
        $swatchInterface->setTotalCount(count($swatchArray));
        return $swatchInterface;
    }

    /**
     * Get Swatch Option Data
     *
     * @param array $swatches
     * @return SwatchInterface[]
     */
    protected function getSwatchOptionData($swatches)
    {
        $data = [];
        foreach ($swatches as $swatch) {
            /** @var SwatchInterface $swt */
            $swt = $this->swatchInterfaceFactory->create();

            $swt->setSwatchId($swatch['swatch_id']);
            $swt->setOptionId($swatch['option_id']);
            $swt->setType($swatch['type']);
            if ($swatch['type'] == Swatch::SWATCH_TYPE_VISUAL_IMAGE) {
                $swt->setValue(
                    $this->swatchMediaHelper->getSwatchAttributeImage(
                        Swatch::SWATCH_IMAGE_NAME,
                        $swatch['value']
                    )
                );
            } else {
                $swt->setValue($swatch['value']);
            }

            $data[] = $swt;
        }

        return $data;
    }

    /**
     * Get Unused Option
     *
     * @param Option $swatchOption
     * @return array
     */
    protected function getUnusedOption(Option $swatchOption)
    {
        return [
            'label' => $swatchOption->getLabel(),
            'link' => 'javascript:void();',
            'custom_style' => 'disabled'
        ];
    }
}
