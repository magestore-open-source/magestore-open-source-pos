<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Catalog\Option;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Catalog\Option\ProductOptionsInterface;

/**
 * Model - Product options
 */
class ProductOptions extends DataObject implements ProductOptionsInterface
{
    /**
     * @inheritdoc
     */
    public function getConfigOption()
    {
        return $this->getData(self::CONFIG_OPTION);
    }
    /**
     * @inheritdoc
     */
    public function setConfigOption($configOption)
    {
        return $this->setData(self::CONFIG_OPTION, $configOption);
    }

    /**
     * @inheritdoc
     */
    public function getCustomOption()
    {
        return $this->getData(self::CUSTOM_OPTION);
    }
    /**
     * @inheritdoc
     */
    public function setCustomOption($customOption)
    {
        return $this->setData(self::CUSTOM_OPTION, $customOption);
    }

    /**
     * @inheritdoc
     */
    public function getBundleOption()
    {
        return $this->getData(self::BUNDLE_OPTION);
    }
    /**
     * @inheritdoc
     */
    public function setBundleOption($bundleOption)
    {
        return $this->setData(self::BUNDLE_OPTION, $bundleOption);
    }

    /**
     * @inheritdoc
     */
    public function getIsOptions()
    {
        return $this->getData(self::IS_OPTIONS);
    }
    /**
     * @inheritdoc
     */
    public function setIsOptions($isOptions)
    {
        return $this->setData(self::IS_OPTIONS, $isOptions);
    }
}
