<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Catalog\Option;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Catalog\Option\SwatchOptionInterface;

/**
 * Model - Swatch option
 */
class SwatchOption extends DataObject implements SwatchOptionInterface
{
    /**
     * @inheritdoc
     */
    public function getAttributeId()
    {
        return $this->getData(self::ATTRIBUTE_ID);
    }
    /**
     * @inheritdoc
     */
    public function setAttributeId($attributeId)
    {
        return $this->setData(self::ATTRIBUTE_ID, $attributeId);
    }

    /**
     * @inheritdoc
     */
    public function getAttributeCode()
    {
        return $this->getData(self::ATTRIBUTE_CODE);
    }
    /**
     * @inheritdoc
     */
    public function setAttributeCode($attributeCode)
    {
        return $this->setData(self::ATTRIBUTE_CODE, $attributeCode);
    }

    /**
     * @inheritdoc
     */
    public function getAttributeLabel()
    {
        return $this->getData(self::ATTRIBUTE_LABEL);
    }
    /**
     * @inheritdoc
     */
    public function setAttributeLabel($attributeLabel)
    {
        return $this->setData(self::ATTRIBUTE_LABEL, $attributeLabel);
    }

    /**
     * @inheritdoc
     */
    public function getSwatches()
    {
        return $this->getData(self::SWATCHES);
    }
    /**
     * @inheritdoc
     */
    public function setSwatches($swatches)
    {
        return $this->setData(self::SWATCHES, $swatches);
    }
}
