<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Catalog\Option\Config\Attribute\Option;

use Magento\Framework\DataObject;
use Magestore\Webpos\Api\Data\Catalog\Option\Config\Attribute\Option\ProductInterface;

/**
 * Product option model
 */
class Product extends DataObject implements ProductInterface
{

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }
    /**
     * @inheritdoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritdoc
     */
    public function getPrice()
    {
        return $this->getData(self::PRICE);
    }
    /**
     * @inheritdoc
     */
    public function setPrice($price)
    {
        return $this->setData(self::PRICE, round($price, 4));
    }

    /**
     * @inheritdoc
     */
    public function getBasePrice()
    {
        return $this->getData(self::BASE_PRICE);
    }
    /**
     * @inheritdoc
     */
    public function setBasePrice($basePrice)
    {
        return $this->setData(self::BASE_PRICE, round($basePrice, 4));
    }
}
