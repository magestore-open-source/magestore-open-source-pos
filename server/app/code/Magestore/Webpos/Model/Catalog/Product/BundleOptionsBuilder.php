<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Catalog\Product;

use Magento\Bundle\Model\Option;
use Magento\Bundle\Model\Product\Type;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Product BundleOptionsBuilder
 */
class BundleOptionsBuilder
{
    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var int|null
     */
    protected $productId = null;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Data
     */
    protected $_pricingHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var Option
     */
    protected $_bundleOption;

    /**
     * @var Type
     */
    protected $_bundleType;

    /**
     * BundleOptionsBuilder constructor.
     *
     * @param ProductFactory $productFactory
     * @param ObjectManagerInterface $objectManager
     * @param Data $pricingHelper
     * @param StoreManagerInterface $storeManager
     * @param Option $bundleOption
     * @param Type $bundleType
     */
    public function __construct(
        ProductFactory $productFactory,
        ObjectManagerInterface $objectManager,
        Data $pricingHelper,
        StoreManagerInterface $storeManager,
        Option $bundleOption,
        Type $bundleType
    ) {
        $this->productFactory = $productFactory;
        $this->_objectManager = $objectManager;
        $this->_pricingHelper = $pricingHelper;
        $this->_storeManager = $storeManager;
        $this->_bundleOption = $bundleOption;
        $this->_bundleType = $bundleType;
    }

    /**
     * Set Product Id
     *
     * @param int $productId
     * @return void
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * GetProductId
     *
     * @return int|null
     */
    protected function getProductId()
    {
        return $this->productId;
    }

    /**
     * Create
     *
     * @return array|null
     */
    public function create()
    {
        if ($this->getProductId()) {
            $product = $this->productFactory->create()->load($this->getProductId());
            $bundleChilds = [];
            $store_id = $this->_storeManager->getStore()->getId();
            $options = $this->_bundleOption->getResourceCollection()
                ->setProductIdFilter($product->getId())
                ->setPositionOrder();
            $options->joinValues($store_id);
            $typeInstance = $this->_bundleType;
            $selections = $typeInstance->getSelectionsCollection($typeInstance->getOptionsIds($product), $product);
            $price_type = $product->getData('price_type');
            foreach ($options->getItems() as $option) {
                $bundleChilds[$option->getId()]['title'] = $option->getTitle();
                $bundleChilds[$option->getId()]['required'] = $option->getRequired();
                $bundleChilds[$option->getId()]['type'] = $option->getType();
                $bundleChilds[$option->getId()]['id'] = $option->getId();
                $bundleChilds[$option->getId()]['product_id'] = $product->getId();
                foreach ($selections as $selection) {
                    $selection_price_type = $selection->getData('selection_price_type');
                    $selection_price_value = $selection->getData('selection_price_value');
                    $price = $selection->getData('price');
                    $selection_price = ($selection_price_type == 0)
                        ? $selection_price_value
                        : $price * $selection_price_value;

                    if ($price_type == 0) {
                        $selection_price = $price;
                    }
                    if ($option->getId() == $selection->getOptionId()) {
                        $bundleChilds[$option->getId()]['items'][$selection->getSelectionId()] = [];
                        $bundleChilds[$option->getId()]['items'][$selection->getSelectionId()]['name']
                            = $selection->getName();
                        $bundleChilds[$option->getId()]['items'][$selection->getSelectionId()]['qty']
                            = $selection->getSelectionQty();
                        $bundleChilds[$option->getId()]['items'][$selection->getSelectionId()]['price']
                            = $selection_price;
                        $bundleChilds[$option->getId()]['items'][$selection->getSelectionId()]['can_change_qty']
                            = $selection->getSelectionCanChangeQty();
                        $bundleChilds[$option->getId()]['items'][$selection->getSelectionId()]['is_default']
                            = $selection->getIsDefault();
                    }
                }
            }
            return $bundleChilds;
        }
        return null;
    }

    /**
     * FormatPrice
     *
     * @param string $price
     * @return string
     */
    public function formatPrice($price)
    {
        return $this->_pricingHelper->currency($price, true, false);
    }
}
