<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Catalog\Product;

use Magento\Catalog\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\Helper\Data;
use Zend_Json;

/**
 * Model ConfigOptionsBuilder
 */
class ConfigOptionsBuilder
{
    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var int|null
     */
    protected $productId = null;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Data
     */
    protected $_pricingHelper;

    /**
     * @var Configurable
     */
    protected $_configurable;

    /**
     * ConfigOptionsBuilder constructor.
     *
     * @param ProductFactory $productFactory
     * @param ObjectManagerInterface $objectManager
     * @param Data $pricingHelper
     * @param Configurable $configurable
     */
    public function __construct(
        ProductFactory $productFactory,
        ObjectManagerInterface $objectManager,
        Data $pricingHelper,
        Configurable $configurable
    ) {
        $this->productFactory = $productFactory;
        $this->_objectManager = $objectManager;
        $this->_pricingHelper = $pricingHelper;
        $this->_configurable = $configurable;
    }

    /**
     * Set Product Id
     *
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * Get Product Id
     *
     * @return int|null
     */
    protected function getProductId()
    {
        return $this->productId;
    }

    /**
     * Create
     *
     * @return array|null
     */
    public function create()
    {
        if ($this->getProductId()) {
            $product = $this->productFactory->create()->load($this->getProductId());
            $productAttributeOptions = $this->_configurable->getConfigurableAttributesAsArray($product);
            $options = $prices = [];
            $originalPrice = $product->getFinalPrice();
            $tempKey = 1;
            foreach ($productAttributeOptions as $productAttributeOption) {
                $values = $productAttributeOption['values'];
                $optionId = $productAttributeOption['attribute_id'];
                $code = $productAttributeOption['attribute_code'];
                $optionLabel = $productAttributeOption['label'];
                $options[$code]['optionId'] = $optionId;
                $options[$code]['optionLabel'] = $optionLabel;
                foreach ($values as $value) {
                    $optionValueId = $value['value_index'];
                    $pricing_value = (isset($value['pricing_value']) && $value['pricing_value'] != null)
                        ? $value['pricing_value']
                        : 0;
                    $val = $value['label'];
                    $is_percent = (isset($value['is_percent']) && $value['is_percent'] != null)
                        ? $value['is_percent']
                        : 0;
                    $options[$code][$optionValueId] = $val;
                    $childPrice = ($is_percent == 0) ? ($pricing_value) : ($pricing_value * $originalPrice / 100);
                    $prices[$code . $tempKey][$optionId] = $optionValueId;
                    $prices[$code . $tempKey]['isSaleable'] = 'true';
                    $prices[$code . $tempKey]['price'] = $this->formatPrice($childPrice);
                    $tempKey++;
                }
            }
            $options['price_condition'] = Zend_Json::encode(array_values($prices));
            return $options;
        }
        return null;
    }

    /**
     * Format price
     *
     * @param string $price
     * @return string
     */
    public function formatPrice($price)
    {
        return $this->_pricingHelper->currency($price, true, false);
    }
}
