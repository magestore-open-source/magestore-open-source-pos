<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Catalog;

use Exception;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magestore\Webpos\Api\Catalog\ProductSearchRepositoryInterface;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;
use Magestore\Webpos\Api\SearchCriteriaInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Helper\Profiler;
use Magestore\Webpos\Model\Indexer\Product\Processor;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\Collection;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;
use Zend_Db_Expr;

/**
 * Class ProductSearchRepository
 *
 * Used for search product
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ProductSearchRepository extends ProductRepository implements
    ProductSearchRepositoryInterface
{
    const SEARCH_ATTRIBUTES = [
        'name',
        'sku',
    ];

    /**
     * Get webpos search attributes
     *
     * @return array
     */
    public function getSearchAttributes()
    {
        $searchAttrs = self::SEARCH_ATTRIBUTES;

        $objectManager = ObjectManager::getInstance();
        $this->scopeConfig = $objectManager->get(ScopeConfigInterface::class);
        $barcodeAttr = $this->scopeConfig->getValue('webpos/product_search/barcode');

        if ($barcodeAttr && !in_array($barcodeAttr, $searchAttrs)) {
            $searchAttrs[] = $barcodeAttr;
        }

        return $searchAttrs;
    }

    /**
     * @inheritdoc
     */
    public function barcode(SearchCriteriaInterface $searchCriteria)
    {
        return $this->search($searchCriteria, true);
    }

    /**
     * Process Search In Elastic
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @param bool $barcode
     * @return ProductSearchResultsInterface
     * @throws NoSuchEntityException
     */
    public function processSearchInElasticSearch(
        SearchCriteriaInterface $searchCriteria,
        $barcode = false
    ) {
        if ($searchCriteria->getQueryString() && !$barcode) {
            return $this->searchByElasticSearch($searchCriteria);
        } elseif ($searchCriteria->getQueryString() && $barcode) {
            return $this->scanBarcodeInElasticSearch($searchCriteria);
        } else {
            return $this->getProductListByElasticSearch($searchCriteria);
        }
    }

    /**
     * @inheritdoc
     */
    public function search(SearchCriteriaInterface $searchCriteria, $barcode = false)
    {
        $objectManager = ObjectManager::getInstance();
        $this->registry = $objectManager->get(Registry::class);
        $this->registry->register('webpos_get_product_list', true);
        $this->request = $objectManager->get(RequestInterface::class);
        // Show stock by default
        $this->registry->register('wp_is_show_stock', true);
        $this->registry->register('wp_is_show_options', (boolean)$this->request->getParam('show_option'));
        $this->registry->register('wp_is_search_product', true);

        /* @var Data $webposHelper */
        $webposHelper = $objectManager->get(Data::class);
        if ($webposHelper->isEnableElasticSearch()) {
            return $this->processSearchInElasticSearch($searchCriteria, $barcode);
        } else {
            Profiler::start('search');

            $this->prepareCollection($searchCriteria, $barcode);
            $this->_productCollection->setCurPage($searchCriteria->getCurrentPage());
            $this->_productCollection->setPageSize($searchCriteria->getPageSize());
            $searchResult = $this->searchResultsFactory->create();
            $searchResult->setSearchCriteria($searchCriteria);

            Profiler::start('load');
            $totalCount = $this->_productCollection->getSize();
            $this->_productCollection->load();
            Profiler::stop('load');

            Profiler::start('items');
            $items = $this->_productCollection->getItems();

            if ($barcode) {
                foreach ($items as $product) {
                    Profiler::start('get_product_' . $product->getEntityId());
                    $product->getProduct();
                    Profiler::stop('get_product_' . $product->getEntityId());
                }
            } elseif (count($items)
                && $this->_productCollection instanceof \Magestore\Webpos\Model\ResourceModel\Catalog\Search\Collection
            ) {
                // Reinit collection from base data
                $collection = $objectManager
                    ->create(Collection::class);
                $collection->setStoreId($this->storeManager->getStore()->getId());
                $collection->addAttributeToSelect($this->listAttributes);
                $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
                $collection->addAttributeToSort('name', 'ASC');
                $productIds = [];
                foreach ($items as $product) {
                    $productIds[] = $product->getId();
                }
                $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
                Profiler::start('product_from_eav');
                $items = $collection->getItems();
                Profiler::stop('product_from_eav');
            }
            $searchResult->setItems($items);
            Profiler::stop('items');
            Profiler::start('get_size');
            $searchResult->setTotalCount($totalCount);
            Profiler::stop('get_size');

            Profiler::stop('search');

            return $searchResult;
        }
    }

    /**
     * Check can use indexed table for searching
     *
     * @return boolean
     */
    protected function useIndexedTable()
    {
        $objectManager = ObjectManager::getInstance();
        /** @var Processor $processor */
        $processor = $objectManager->get(Processor::class);
        return $processor->getIndexer()->isValid();
    }

    /**
     * Search by elastic search
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ProductSearchResultsInterface
     * @throws NoSuchEntityException
     */
    public function searchByElasticSearch(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();
        $searchResultSearchEngine = $objectManager
            ->create(\Magestore\Webpos\Model\ResourceModel\Catalog\Search\Collection::class)
            ->searchWebposProductInSearchEngine($searchCriteria);
        return $this->getListProductFromElasticSearchResult($searchResultSearchEngine, $searchCriteria);
    }

    /**
     * Search by elastic search
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ProductSearchResultsInterface
     * @throws NoSuchEntityException
     */
    public function getProductListByElasticSearch(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();
        $searchResultSearchEngine = $objectManager
            ->create(\Magestore\Webpos\Model\ResourceModel\Catalog\Search\Collection::class)
            ->getProductListInSearchEngine($searchCriteria);
        return $this->getListProductFromElasticSearchResult($searchResultSearchEngine, $searchCriteria);
    }

    /**
     * Get List Product From Elastic Search Result
     *
     * @param SearchResultInterface $searchResultSearchEngine
     * @param SearchCriteriaInterface $searchCriteria
     * @return ProductSearchResultsInterface
     * @throws NoSuchEntityException
     */
    public function getListProductFromElasticSearchResult(
        $searchResultSearchEngine,
        SearchCriteriaInterface $searchCriteria
    ) {
        $objectManager = ObjectManager::getInstance();
        $productIds = [];
        foreach ($searchResultSearchEngine->getItems() as $item) {
            $productIds[] = $item->getId();
        }
        if (count($productIds)) {
            // Reinit collection from base data
            $collection = $objectManager
                ->create(Collection::class);
            $collection->setStoreId($this->storeManager->getStore()->getId());
            $collection->addAttributeToSelect($this->listAttributes);
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $collection->addFieldToFilter('entity_id', ['in' => $productIds]);
            $collection->getSelect()
                ->order(new Zend_Db_Expr('FIELD(e.entity_id,' . implode(',', $productIds).')'));
            $items = $collection->getItems();
        } else {
            $items = [];
        }

        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($items);
        $searchResult->setTotalCount($searchResultSearchEngine->getTotalCount());
        return $searchResult;
    }

    /**
     * Scan barcode In Elastic
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ProductSearchResultsInterface
     * @throws NoSuchEntityException
     */
    public function scanBarcodeInElasticSearch(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();
        $searchResultSearchEngine = $objectManager
            ->create(\Magestore\Webpos\Model\ResourceModel\Catalog\Search\Collection::class)
            ->getProductIdByBarcodeInElasticSearch($searchCriteria);

        if (count($searchResultSearchEngine->getItems())) {
            $itemList = $searchResultSearchEngine->getItems();
            $itemSearchResult = $itemList[0];
            $productId = $itemSearchResult->getId();
        } else {
            $productId = 0;
        }

        $searchResult = $this->searchResultsFactory->create();
        $items = [];

        if ($productId) {
            $product = $objectManager->create(Product::class)
                ->load($productId);
            $barcodes = $product->getPosBarcode();
            if (in_array($searchCriteria->getQueryString(), explode(',', $barcodes))) {
                $items = [$product];
            }
        }
        $searchResult->setItems($items);
        $searchResult->setTotalCount(count($items));
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function prepareCollection($searchCriteria, $barcode = false)
    {
        if (!empty($this->_productCollection)) {
            return;
        }
        $objectManager = ObjectManager::getInstance();

        $categoryFilter = null;

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() == 'category_id') {
                    $categoryFilter = str_replace("%", "", $filter->getValue());
                }
            }
        }

        $useIndexedTable = $this->useIndexedTable() && $categoryFilter === null;
        /** @var Collection $collection */
        if ($useIndexedTable) {
            $collection = $objectManager
                ->create(\Magestore\Webpos\Model\ResourceModel\Catalog\Search\Collection::class);
            $collection->setItemObjectClass(Product::class);
        } else {
            $collection = $objectManager
                ->create(Collection::class);
        }
        $storeId = $this->storeManager->getStore()->getId();
        $collection->setStoreId($storeId);

        $this->_eventManagerInterFace = $objectManager->get(ManagerInterface::class);
        /** @var GetSessionByIdInterface $getSessionById */
        $getSessionById = $objectManager->get(GetSessionByIdInterface::class);
        $this->request = $objectManager->get(RequestInterface::class);
        $session = $getSessionById->execute(
            $this->request->getParam(
                RequestProcessor::SESSION_PARAM_KEY
            )
        );
        $this->_eventManagerInterFace->dispatch(
            'webpos_catalog_product_getlist',
            ['collection' => $collection, 'is_new' => true, 'location' => $session->getLocationId()]
        );
        /** End integrate webpos **/

        // Fix inventory
        if ($useIndexedTable) {
            $collection->setOrder('name', 'ASC');
            $collection->addFieldToFilter([
                'webpos_visible'
            ], [
                ['eq' => 1],
            ]);
        } else {
            $collection->addAttributeToSelect($this->listAttributes);
            $collection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner'
            );
            $collection->addAttributeToFilter(
                'status',
                Status::STATUS_ENABLED
            );
            $collection->addAttributeToSort('name', 'ASC');
            $collection->addVisibleFilter();

            if ($categoryFilter !== null) {
                $objectManager = ObjectManager::getInstance();
                /** @var CategoryRepositoryInterface $categoryRepository */
                $categoryRepository = $objectManager->create(
                    CategoryRepositoryInterface::class
                );
                try {
                    $category = $categoryRepository->get($categoryFilter);
                    $collection->addCategoryFilter($category);
                } catch (Exception $e) {
                    $categoryFilter = [['in' => [$categoryFilter]]];
                    $collection->addCategoriesFilter($categoryFilter);
                }
            }
        }

        /** @var Data $webposHelper */
        $webposHelper = ObjectManager::getInstance()
            ->get(Data::class);
        $productTypeIds = $webposHelper->getProductTypeIds();
        $collection->addFieldToFilter('type_id', ['in' => $productTypeIds]);

        // Search data
        if ($barcode) {
            $queryString = $searchCriteria->getQueryString();
        } else {
            $queryString = '%' . $searchCriteria->getQueryString() . '%';
        }

        /** Integrate Inventory Barcode **/
        $eventManage = ObjectManager::getInstance()->get(
            ManagerInterface::class
        );
        $array = [];
        $result = new DataObject();
        $result->setData($array);
        $eventManage->dispatch(
            'webpos_catalog_product_search_online',
            ['search_string' => $queryString, 'search_criteria' => $searchCriteria, 'result' => $result]
        );

        if ($barcode) {
            $this->scopeConfig = $objectManager->get(ScopeConfigInterface::class);
            $barcodeAttr = $this->scopeConfig->getValue('webpos/product_search/barcode');
            if ($useIndexedTable) {
                if ($barcodeAttr === 'sku') {
                    $barcodeAttr = 'e.sku';
                }
                $attributes = [$barcodeAttr];
                $conditions = [['eq' => $queryString]];
                if ($result->getData() && !empty($result->getData())) {
                    $attributes[] = 'e.sku';
                    $conditions[] = ['in' => $result->getData()];
                }
                $collection->addFieldToFilter($attributes, $conditions);
            } else {
                $attrConditions[] = [
                    'attribute' => $barcodeAttr,
                    'like' => $queryString,
                ];
                if ($result->getData() && !empty($result->getData())) {
                    $attrConditions[] = [
                        'attribute' => 'sku',
                        'in' => $result->getData(),
                    ];
                }
                $collection->addAttributeToFilter($attrConditions);
            }
        } else {
            $searchAttrs = $this->getSearchAttributes();
            $conditions = [];
            if ($useIndexedTable) {
                foreach ($searchAttrs as &$attribute) {
                    if ($attribute === 'sku') {
                        $attribute = 'e.sku';
                    }
                    $conditions[] = [
                        'like' => $queryString,
                    ];
                }
                if ($result->getData() && !empty($result->getData())) {
                    $searchAttrs[] = 'e.sku';
                    $conditions[] = ['in' => $result->getData()];
                }
                $collection->addFieldToFilter($searchAttrs, $conditions);
            } else {
                foreach ($searchAttrs as $attribute) {
                    $conditions[] = [
                        'attribute' => $attribute,
                        'like' => $queryString,
                    ];
                }
                if ($result->getData() && !empty($result->getData())) {
                    $conditions[] = [
                        'attribute' => 'sku',
                        'in' => $result->getData()
                    ];
                }
                $collection->addAttributeToFilter($conditions, null, "left");
            }
        }

        $this->filterProductByStockAndSource($collection);

        $this->_productCollection = $collection;
    }
}
