<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Catalog;

use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\App\Area;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Magestore\Webpos\Api\Data\Catalog\CategoryInterface;
use Magestore\Webpos\Block\AbstractBlock;

/**
 * Catalog Category model
 *
 * @method Collection getCollection()
 */
class Category extends \Magento\Catalog\Model\Category implements CategoryInterface
{

    /**
     * root categoty id
     */
    protected $rootCategory;

    /**
     * Get Root Category Id
     *
     * @return int
     * @throws NoSuchEntityException
     */
    public function getRootCategoryId()
    {
        if (!$this->rootCategory) {
            $storeManager = ObjectManager::getInstance()->get(
                StoreManagerInterface::class
            );
            $this->rootCategory = $storeManager->getStore()->getRootCategoryId();
        }
        return $this->rootCategory;
    }

    /**
     * Get category image
     *
     * @return string/null
     */
    public function getImage()
    {
        $storeManager = ObjectManager::getInstance()->get(
            StoreManagerInterface::class
        );
        $url = $storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        if ($this->getData('image', null)) {
            return $url. 'catalog/category/'. ltrim(str_replace('\\', '/', $this->getData('image')), '/');
        }
        $objectManager = ObjectManager::getInstance();
        $defaultStoreId = $this->_storeManager->getStore()->getId();
        $appEmulation = $objectManager->get(Emulation::class);
        $appEmulation->startEnvironmentEmulation($defaultStoreId, Area::AREA_FRONTEND, true);
        $block = $objectManager->get(AbstractBlock::class);
        $url = $block->getViewFileUrl('Magestore_Webpos::images/category/image.jpg');
        $appEmulation->stopEnvironmentEmulation();
        return $url;
    }

    /**
     * Retrieve children ids
     *
     * @return array
     */
    public function getChildrenIds()
    {
        return $this->getResource()->getChildren($this, false);
    }

    /**
     * @inheritdoc
     */
    public function getChildren($recursive = false, $isActive = true, $sortByPosition = false)
    {
        return $this->getData('children_object');
    }

    /**
     * @inheritdoc
     */
    public function setChildren($children)
    {
        return $this->setData('children_object', $children);
    }
    /**
     * Is first category
     *
     * @return int
     */
    public function isFirstCategory()
    {
        $rootCategoryId = $this->getRootCategoryId();
        if ($this->getParentId() == $rootCategoryId) {
            return 1;
        }
        return 0;
    }
}
