<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Inventory;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Model\Stock\Item;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\Search\SearchResultFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Module\Manager;
use Magento\Framework\ObjectManagerInterface as ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Webapi\ServiceOutputProcessor;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magestore\Webpos\Api\Data\Inventory\AvailableQtyInterface;
use Magestore\Webpos\Api\Data\Inventory\StockItemInterface;
use Magestore\Webpos\Api\Data\Inventory\StockSearchResultsInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;
use Magestore\Webpos\Api\SyncInterface;
use Magestore\Webpos\Api\WebposManagementInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Model\Location\LocationFactory;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\Collection;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\CollectionFactory;
use Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item as StockItemResource;
use Magento\CatalogInventory\Model\Stock\Item as StockItemModel;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface as CoreStockItemRepositoryInterface;
use Magento\Framework\Api\SortOrder;
use Magestore\Webpos\Api\Inventory\StockItemRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\CatalogInventory\Model\Stock as Stock;

/**
 * Class StockItemRepository
 *
 * Used for stock item repository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class StockItemRepository implements StockItemRepositoryInterface
{

    /**
     *
     * @var StockItemResource
     */
    protected $resource;

    /**
     *
     * @var Item
     */
    protected $stockItemModel;

    /**
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     *
     * @var CoreStockItemRepositoryInterface
     */
    protected $stockItemRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     *
     * @var StockRegistryProviderInterface
     */
    protected $stockRegistryProvider;
    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var array
     */
    protected $listCondition = [
        'eq' => '=',
        'neq' => '!=',
        'like' => 'like',
        'gt' => '>',
        'gteq' => '>=',
        'lt' => '<',
        'lteq' => '<=',
        'in' => 'in'
    ];

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var SearchResultFactory
     */
    protected $_searchResultFactory;

    /**
     * @var Manager
     */
    protected $_moduleManager;

    /**
     * @var AvailableQtyInterface
     */
    protected $availableQty;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var LocationFactory
     */
    protected $locationFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var WebposManagementInterface
     */
    protected $webposManagement;

    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * StockItemRepository constructor.
     *
     * @param StockItemResource $resource
     * @param Item $stockItemModel
     * @param CoreStockItemRepositoryInterface $stockItemRepository
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param StockRegistryProviderInterface $stockRegistryProvider
     * @param ManagerInterface $eventManager
     * @param CollectionFactory $collectionFactyory
     * @param SearchResultFactory $searchResultFactory
     * @param Manager $moduleManager
     * @param AvailableQtyInterface $availableQty
     * @param ProductFactory $productFactory
     * @param LocationFactory $locationFactory
     * @param Registry $coreRegistry
     * @param Data $helper
     * @param WebposManagementInterface $webposManagement
     * @param StockManagementInterface $stockManagement
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        StockItemResource $resource,
        StockItemModel $stockItemModel,
        CoreStockItemRepositoryInterface $stockItemRepository,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        StockRegistryProviderInterface $stockRegistryProvider,
        ManagerInterface $eventManager,
        CollectionFactory $collectionFactyory,
        SearchResultFactory $searchResultFactory,
        Manager $moduleManager,
        AvailableQtyInterface $availableQty,
        ProductFactory $productFactory,
        LocationFactory $locationFactory,
        Registry $coreRegistry,
        Data $helper,
        WebposManagementInterface $webposManagement,
        StockManagementInterface $stockManagement
    ) {
        $this->resource = $resource;
        $this->stockItemModel = $stockItemModel;
        $this->stockItemRepository = $stockItemRepository;
        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
        $this->stockRegistryProvider = $stockRegistryProvider;
        $this->eventManager = $eventManager;
        $this->_collectionFactory = $collectionFactyory;
        $this->_searchResultFactory = $searchResultFactory;
        $this->_moduleManager = $moduleManager;
        $this->availableQty = $availableQty;
        $this->productFactory = $productFactory;
        $this->coreRegistry = $coreRegistry;
        $this->locationFactory = $locationFactory;
        $this->helper = $helper;
        $this->webposManagement = $webposManagement;
        $this->stockManagement = $stockManagement;
    }

    /**
     * @inheritdoc
     */
    public function getStockItems(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->_collectionFactory->create();
        $storeId = $this->helper->getCurrentStoreView()->getId();
        $collection->addAttributeToSelect('name');
        $collection->getSelect()->group('e.entity_id');
        $collection->setStoreId($storeId);
        $collection->addStoreFilter($storeId);
        $collection->addAttributeToFilter('type_id', ['in' => $this->getProductTypeIds()]);
        $collection = $this->resource->addStockDataToCollection($collection);
        /** Integrate webpos **/
        $locationId = 0;
        $sessionModel = $this->coreRegistry->registry('currrent_session_model');
        if ($sessionModel && $sessionModel->getLocationId()) {
            $locationId = $sessionModel->getLocationId();
        }
        $this->eventManager->dispatch('webpos_inventory_stockitem_getstockitems', [
            'collection' => $collection,
            'location_id' => $locationId
        ]);
        /** End integrate webpos **/
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
            );
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collectionSize = $collection->getSize();
        $collection->load();
        $searchResult = $this->_searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collectionSize);
        return $searchResult;
    }

    /**
     * @inheritDoc
     */
    public function sync(SearchCriteriaInterface $searchCriteria)
    {
        $objectManager = ObjectManager::getInstance();

        /** @var StateInterface $cacheState */
        $cacheState = $objectManager->get(StateInterface::class);
        if (!$cacheState->isEnabled(\Magestore\Webpos\Model\Cache\Type::TYPE_IDENTIFIER)
            || count($searchCriteria->getFilterGroups())
        ) {
            return $this->getStockItems($searchCriteria);
        }

        /** @var CacheInterface $cache */
        $cache = $objectManager->get(CacheInterface::class);
        /** @var SerializerInterface $serializer */

        $locationId = Stock::DEFAULT_STOCK_ID;
        if ($this->webposManagement->isMSIEnable()) {
            $locationId = $this->stockManagement->getStockId();
        }

        $key = 'syncStockItems-' . $searchCriteria->getPageSize() . '-'
            . $searchCriteria->getCurrentPage() . '-' . $locationId;

        if ($response = $cache->load($key)) {
            // Reponse from cache
            return json_decode($response, true);
        }

        // Check async queue for product
        $flag = SyncInterface::QUEUE_NAME;
        if ($cachedAt = $cache->load($flag)) {
            return [
                'async_stage' => 2, // processing
                'cached_at' => $cachedAt, // process started time
            ];
        }

        // Block metaphor
        $cachedAt = date("Y-m-d H:i:s");
        $cache->save($cachedAt, $flag, [\Magestore\Webpos\Model\Cache\Type::CACHE_TAG], 300);

        /** @var ServiceOutputProcessor $processor */
        $processor = $objectManager->get(ServiceOutputProcessor::class);
        $outputData = $processor->process(
            $this->getStockItems($searchCriteria),
            StockItemRepositoryInterface::class,
            'sync'
        );
        $outputData['cached_at'] = $cachedAt;
        $cache->save(
            json_encode($outputData),
            $key,
            [
                \Magestore\Webpos\Model\Cache\Type::CACHE_TAG,
            ],
            3600    // Cache lifetime: 1 hour
        );
        // Release metaphor
        $cache->remove($flag);
        return $outputData;
    }

    /**
     * Get product type ids to support
     *
     * @return array
     */
    public function getProductTypeIds()
    {
        $types = [
            Type::TYPE_VIRTUAL,
            Type::TYPE_SIMPLE,
            Grouped::TYPE_CODE,
            Type::TYPE_BUNDLE,
            Configurable::TYPE_CODE
        ];
        return $types;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return void
     */
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    ) {
        $where = '(';
        $first = true;
        foreach ($filterGroup->getFilters() as $filter) {
            $conditionType = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $condition = $this->convertCondition($conditionType);
            $value = is_array($filter->getValue()) ? "('"
                . implode("','", $filter->getValue()) . "')" : $filter->getValue();

            if (in_array($condition, ['IN', 'NOT IN'])) {
                $value = '(' . $value . ')';
            } else {
                $value = "'" . $value . "'";
            }

            if (!$first) {
                $where .= ' OR ';
            }

            /*implement for API-test with SKU */
            if (in_array($condition, ['IN', 'NOT IN']) && $filter->getField() == "sku"
                && !is_array($filter->getValue())) {
                $valueArray = explode(',', $filter->getValue());
                $valueArray = "('" . implode("','", $valueArray) . "')";
                $columnFilter = "`e`.`sku`";
                $where .= $columnFilter . " " . $condition . ' ' . $valueArray;
            } else {
                $where .= $filter->getField() . " " . $condition . ' ' . $value;
            }

            $first = false;
        }

        $where .= ')';
        $collection->getSelect()->where($where);
    }

    /**
     * Convert sql condition from Magento to Zend Db Select
     *
     * @param string $type
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function convertCondition($type)
    {
        switch ($type) {
            case 'gt':
                return '>';
            case 'gteq':
                return '>=';
            case 'lt':
                return '<';
            case 'lteq':
                return '=<';
            case 'eq':
                return '=';
            case 'in':
                return 'IN';
            case 'nin':
                return 'NOT IN';
            case 'neq':
                return '!=';
            case 'like':
                return 'LIKE';
            default:
                return '=';
        }
    }

    /**
     * Mass update stock items
     *
     * @param array $stockItems
     * @return bool
     */
    public function massUpdateStockItems($stockItems)
    {
        if (count($stockItems)) {
            foreach ($stockItems as $stockItem) {
                if (!$stockItem->getItemId()) {
                    continue;
                }
                $this->updateStockItem($stockItem->getItemId(), $stockItem);
            }
        }
        return true;
    }

    /**
     * Update stock item
     *
     * @param string $itemId
     * @param StockItemInterface $stockItem
     * @return int
     */
    public function updateStockItem($itemId, StockItemInterface $stockItem)
    {
        $origStockItem = $this->stockItemModel->load($itemId);
        $changeQty = $stockItem->getQty() - $origStockItem->getQty();
        $data = $stockItem->getData();
        if ($origStockItem->getItemId()) {
            unset($data['item_id']);
        }
        $origStockItem->addData($data);

        $stockItem = $this->stockItemRepository->save($origStockItem);

        $this->eventManager->dispatch('webpos_inventory_stockitem_update', [
            'stock_item' => $stockItem,
            'change_qty' => $changeQty,
        ]);

        return $stockItem->getItemId();
    }

    /**
     * @inheritdoc
     */
    public function getAvailableQty($product_id)
    {
        /** @var Product $product */
        $product = $this->objectManager->get(ProductFactory::class)
            ->create()->load($product_id);
        if (!$product->getId()) {
            throw new NoSuchEntityException(__('Product does not exist!'));
        }
        $websiteId = 0;
        $qtys = $this->resource->getAvailableQty($product_id, $websiteId);

        $availableQty = $this->availableQty;

        if (!count($qtys)) {
            $availableQty->setAvailableQty(0);
        } else {
            $availableQty->setAvailableQty(round($qtys[0]['qty'], 4));
        }

        return $availableQty;
    }

    /**
     * Get Stock item to refund
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return StockSearchResultsInterface
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getStockItemsToRefund(
        SearchCriteriaInterface $searchCriteria
    ) {
        $response = $this->sync($searchCriteria);
        if (!$this->webposManagement->isMSIEnable()) {
            return $response;
        }
        $cloneSearchCriteria = clone $searchCriteria;
        $productIds = [];
        foreach ($cloneSearchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() == 'stock_item_index.product_id' &&
                    strtolower($filter->getConditionType()) == 'in'
                ) {
                    $productIds = explode(',', $filter->getValue());
                }
            }
        }
        $itemsResponse = $response->getItems();
        if (count($itemsResponse) === count($productIds)) {
            return $response;
        }
        $responseProductIds = [];
        foreach ($itemsResponse as $item) {
            $responseProductIds[] = $item->getProductId();
        }
        $needSyncFromOtherProductIds = array_diff($productIds, $responseProductIds);
        /** @var Collection $collection */
        $collection = $this->_collectionFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->getSelect()->group('e.entity_id');
        $collection = $this->resource->joinStockItemTable($collection);
        foreach ($cloneSearchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() == 'stock_item_index.product_id' &&
                    strtolower($filter->getConditionType()) == 'in'
                ) {
                    $filter->setValue(implode(',', $needSyncFromOtherProductIds));
                }
            }
            $this->addFilterGroupToCollection($filterGroup, $collection);
        }
        $collection->setCurPage($cloneSearchCriteria->getCurrentPage());
        $collection->setPageSize($cloneSearchCriteria->getPageSize());
        $collectionSize = $collection->getSize();
        $collection->load();
        $searchResult = $this->_searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $itemsResponse = array_merge($itemsResponse, $collection->getItems());
        $searchResult->setItems($itemsResponse);
        $searchResult->setTotalCount($response->getTotalCount() + $collectionSize);
        return $searchResult;
    }
}
