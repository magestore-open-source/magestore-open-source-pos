<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Inventory\Stock;

use Magestore\Webpos\Api\Data\Inventory\StockItemInterface;

/**
 * Class StockItemRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class Item extends \Magento\CatalogInventory\Model\Stock\Item implements
    StockItemInterface
{
    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * @inheritDoc
     */
    public function getSku()
    {
        return $this->getData('sku');
    }

    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        $this->setData('name', $name);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        $this->setData('sku', $sku);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedTime()
    {
        return $this->getData('updated_time');
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedTime($updatedTime)
    {
        $this->setData('updated_time', $updatedTime);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity()
    {
        return $this->getData('quantity');
    }

    /**
     * @inheritDoc
     */
    public function setQuantity($qtyInLocation)
    {
        return $this->setData('quantity', $qtyInLocation);
    }
}
