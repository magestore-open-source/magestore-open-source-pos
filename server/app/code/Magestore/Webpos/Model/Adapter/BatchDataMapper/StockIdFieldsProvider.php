<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Adapter\BatchDataMapper;

use Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item;
use Magento\AdvancedSearch\Model\Adapter\DataMapper\AdditionalFieldsProviderInterface;

/**
 * Provide data mapping for stock id
 */
class StockIdFieldsProvider implements AdditionalFieldsProviderInterface
{
    /**
     * @var Item
     */
    private $resourceStockItem;

    /**
     * StockIdFieldsProvider constructor.
     *
     * @param Item $resourceStockItem
     */
    public function __construct(
        Item $resourceStockItem
    ) {
        $this->resourceStockItem = $resourceStockItem;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getFields(array $productIds, $storeId)
    {
        $fields = $this->resourceStockItem->getAssignedStockIdByIds($productIds);

        return $fields;
    }
}
