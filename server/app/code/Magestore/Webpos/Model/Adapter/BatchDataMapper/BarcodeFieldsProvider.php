<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Adapter\BatchDataMapper;

use Magento\AdvancedSearch\Model\Adapter\DataMapper\AdditionalFieldsProviderInterface;
use Magento\CatalogSearch\Model\Indexer\Fulltext\Action\DataProvider;
use Magestore\Webpos\Helper\Data as PosHelper;
use Magestore\Webpos\Model\Indexer\IndexerHandler;

/**
 * Provide data mapping for price fields
 */
class BarcodeFieldsProvider implements AdditionalFieldsProviderInterface
{
    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var PosHelper
     */
    private $posHelper;

    /**
     * BarcodeFieldsProvider constructor.
     *
     * @param DataProvider $dataProvider
     * @param PosHelper $posHelper
     */
    public function __construct(
        DataProvider $dataProvider,
        PosHelper $posHelper
    ) {
        $this->dataProvider = $dataProvider;
        $this->posHelper = $posHelper;
    }

    /**
     * @inheritdoc
     */
    public function getFields(array $productIds, $storeId)
    {
        $fields = [];
        $barcodeAttributeCode = $this->posHelper->getStoreConfig('webpos/product_search/barcode');
        if ($barcodeAttributeCode
            && $barcodeAttribute = $this->dataProvider->getSearchableAttribute($barcodeAttributeCode)) {
            if ($barcodeAttribute->getBackendType() == 'static') {
                $products = $this->dataProvider->getSearchableProducts(
                    $storeId,
                    [$barcodeAttribute->getAttributeCode()],
                    $productIds,
                    0,
                    IndexerHandler::DEFAULT_BATCH_SIZE
                );
                foreach ($products as $productData) {
                    $fields[$productData['entity_id']]['barcode'][]
                        = $productData[$barcodeAttribute->getAttributeCode()];
                }
            } else {
                $attributeTypes[$barcodeAttribute->getBackendType()] = [$barcodeAttribute->getAttributeId()];
                $products = $this->dataProvider->getProductAttributes($storeId, $productIds, $attributeTypes);
                foreach ($products as $productId => $productData) {
                    $fields[$productId]['barcode'][]
                        = $productData[$barcodeAttribute->getAttributeId()];
                }
            }
        }
        return $fields;
    }
}
