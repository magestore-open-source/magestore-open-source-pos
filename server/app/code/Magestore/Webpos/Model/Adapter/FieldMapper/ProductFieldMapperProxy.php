<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Adapter\FieldMapper;

use Magento\AdvancedSearch\Model\Client\ClientResolver;
use Magento\Elasticsearch\Model\Adapter\FieldMapperInterface;

/**
 * Proxy for product fields mappers
 *
 * Follow Magento\Elasticsearch\Elasticsearch5\Model\Adapter\FieldMapper\ProductFieldMapperProxy
 */
class ProductFieldMapperProxy implements FieldMapperInterface
{
    /**
     * @var ClientResolver
     */
    private $clientResolver;

    /**
     * @var FieldMapperInterface[]
     */
    private $productFieldMappers;

    /**
     * CategoryFieldsProviderProxy constructor.
     *
     * @param ClientResolver $clientResolver
     * @param FieldMapperInterface[] $productFieldMappers
     */
    public function __construct(
        ClientResolver $clientResolver,
        array $productFieldMappers
    ) {
        $this->clientResolver = $clientResolver;
        $this->productFieldMappers = $productFieldMappers;
    }

    /**
     * Get product field mapper
     *
     * @return FieldMapperInterface
     */
    private function getProductFieldMapper()
    {
        return $this->productFieldMappers[$this->clientResolver->getCurrentEngine()];
    }

    /**
     * Get field name
     *
     * @param string $attributeCode
     * @param array $context
     * @return string
     */
    public function getFieldName($attributeCode, $context = [])
    {
        return $this->getProductFieldMapper()->getFieldName($attributeCode, $context);
    }

    /**
     * Get all entity attribute types
     *
     * @param array $context
     * @return array
     */
    public function getAllAttributesTypes($context = [])
    {
        return $this->getProductFieldMapper()->getAllAttributesTypes($context);
    }
}
