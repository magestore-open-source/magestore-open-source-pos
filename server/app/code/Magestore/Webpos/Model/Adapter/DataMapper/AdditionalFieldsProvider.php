<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Model\Adapter\DataMapper;

use Magento\AdvancedSearch\Model\Adapter\DataMapper\AdditionalFieldsProvider as DefaultAdditionalFieldsProvider;
use Magento\AdvancedSearch\Model\Adapter\DataMapper\AdditionalFieldsProviderInterface;

/**
 * Provide additional fields for data mapper during search indexer
 *
 * Must return array with the following format: [[product id] => [field name1 => value1, ...], ...]
 */
class AdditionalFieldsProvider extends DefaultAdditionalFieldsProvider implements AdditionalFieldsProviderInterface
{
}
