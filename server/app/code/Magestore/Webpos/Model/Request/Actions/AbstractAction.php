<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Model\Request\Actions;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Webapi\Rest\Request;
use Magestore\Webpos\Log\Logger;
use Magestore\Webpos\Model\Request\ActionLog;
use Magestore\Webpos\Model\Request\ActionLogFactory;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;

/**
 * Class AbstractAction
 *
 * Abstract action for the request
 */
class AbstractAction
{
    /**
     * @var string
     */
    const ACTION_TYPE = "";
    /**
     * @var ActionLogFactory
     */
    protected $actionLogFactory;
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var GetSessionByIdInterface
     */
    protected $getSessionById;

    /**
     * AbstractAction constructor.
     *
     * @param ActionLogFactory $actionLogFactory
     * @param Logger $logger
     * @param GetSessionByIdInterface $getSessionById
     */
    public function __construct(
        ActionLogFactory $actionLogFactory,
        Logger $logger,
        GetSessionByIdInterface $getSessionById
    ) {
        $this->actionLogFactory = $actionLogFactory;
        $this->logger = $logger;
        $this->getSessionById = $getSessionById;
    }

    /**
     * Save request place order
     *
     * @param Request $request
     * @throws LocalizedException
     */
    public function saveRequest(Request $request)
    {
        $params = $request->getBodyParams();

        $sessionId = $request->getParam(RequestProcessor::SESSION_PARAM_KEY);
        $session = $this->getSessionById->execute($sessionId);

        $data = [
            'action_type' => static::ACTION_TYPE,
            'status' => ActionLog::STATUS_PENDING,
            'location_id' => $session->getLocationId(),
            'pos_staff_id' => $session->getStaffId(),
            'pos_id' => $session->getPosId(),
            'params' => json_encode($params),
        ];

        $data = $this->prepareParams($data, $params);

        // Check request data before save
        $isValidate = $this->validate($data);

        if (!$isValidate) {
            return;
        }

        try {
            /** @var ActionLog $actionLog */
            $actionLog = $this->actionLogFactory->create();
            $actionLog->setData($data)->save();
        } catch (Exception $e) {
            $this->logger->info($e->getTraceAsString());
        }
    }

    /**
     * Validate data before save
     *
     * @param array $data
     * @return bool
     */
    public function validate($data)
    {
        $actionLog = $this->actionLogFactory->create();
        $actionLog->load($data['request_increment_id'], 'request_increment_id');

        if (!$actionLog->getId()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Prepare params
     *
     * @param array $data
     * @param array $params
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function prepareParams($data, $params)
    {
        return $data;
    }
}
