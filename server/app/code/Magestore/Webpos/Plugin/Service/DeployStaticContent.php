<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Service;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Webpos\Model\Deploy\DeployService;

/**
 * Plugin - DeployStaticContent
 */
class DeployStaticContent
{

    /**
     * @var DeployService
     */
    protected $deployService;

    /**
     * DeployStaticContent constructor.
     * @param DeployService $deployService
     */
    public function __construct(
        DeployService $deployService
    ) {
        $this->deployService = $deployService;
    }

    /**
     * After Deploy
     *
     * @param \Magento\Deploy\Service\DeployStaticContent $subject
     * @param void $result
     * @return mixed
     * @throws Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterDeploy(\Magento\Deploy\Service\DeployStaticContent $subject, $result)
    {
        /** deploy webapp */
        try {
            $this->deployService->execute();
        } catch (Exception $e) {
            throw new LocalizedException(__('can not deploy pos app, permission denied!'));
        }
        return $result;
    }
}
