<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\InventoryReservations\Model\ResourceModel;

use Magento\InventoryReservationsApi\Model\ReservationInterface;
use Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item;

/**
 * Plugin - SaveMultiple
 */
class SaveMultiple
{
    /**
     * @var Item
     */
    protected $stockItemResource;

    /**
     * SaveMultiple constructor.
     * @param Item $stockItemResource
     */
    public function __construct(
        Item $stockItemResource
    ) {
        $this->stockItemResource = $stockItemResource;
    }

    /**
     * After Execute
     *
     * @param \Magento\InventoryReservations\Model\ResourceModel\SaveMultiple $subject
     * @param void $result
     * @param ReservationInterface[] $reservations
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(
        $subject,
        $result,
        array $reservations
    ) {
        $skus = [];
        foreach ($reservations as $reservation) {
            $skus[] = $reservation->getSku();
        }
        if (!empty($skus)) {
            $this->stockItemResource->updateUpdatedTimeBySku($skus);
        }
    }
}
