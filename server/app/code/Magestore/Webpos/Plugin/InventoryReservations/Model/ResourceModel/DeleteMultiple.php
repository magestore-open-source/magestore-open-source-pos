<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\InventoryReservations\Model\ResourceModel;

use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item;

/**
 * Plugin - DeleteMultiple
 */
class DeleteMultiple
{
    /**
     * @var Item
     */
    protected $stockItemResource;

    /**
     * DeleteMultiple constructor.
     * @param Item $stockItemResource
     */
    public function __construct(
        Item $stockItemResource
    ) {
        $this->stockItemResource = $stockItemResource;
    }

    /**
     * After Execute
     *
     * @param \Magento\Inventory\Model\ResourceModel\SourceItem\DeleteMultiple $subject
     * @param void $result
     * @param SourceItemInterface[] $sourceItems
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(
        $subject,
        $result,
        array $sourceItems
    ) {
        $skus = [];
        foreach ($sourceItems as $sourceItem) {
            $skus[] = $sourceItem->getSku();
        }
        if (!empty($skus)) {
            $this->stockItemResource->updateUpdatedTimeBySku($skus);
        }
    }
}
