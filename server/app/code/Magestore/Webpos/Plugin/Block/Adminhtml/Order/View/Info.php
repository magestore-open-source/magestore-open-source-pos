<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Block\Adminhtml\Order\View;

use Magento\Framework\Exception\LocalizedException;

/**
 * Plugin Order View Info
 */
class Info extends \Magento\Sales\Block\Adminhtml\Order\View\Info
{
    /**
     * Before To Html
     *
     * @param \Magento\Sales\Block\Adminhtml\Order\View\Info $subject
     * @throws LocalizedException
     */
    public function beforeToHtml(\Magento\Sales\Block\Adminhtml\Order\View\Info $subject)
    {

        if (!$subject->getParentBlock()) {
            $order = $subject->getOrder();
        } else {
            $order = $subject->getParentBlock()->getOrder();
        }
        $posId = $order->getPosId();
        if ($posId) {
            $subject->setTemplate('Magestore_Webpos::sales/order/view/info.phtml');
        }
    }
}
