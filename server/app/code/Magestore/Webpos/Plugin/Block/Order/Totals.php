<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Block\Order;

/**
 * Plugin Block Order Totals
 */
class Totals extends \Magento\Sales\Block\Order\Totals
{

    const POS_CUSTOM_DISCOUNT = "POS_CUSTOM_DISCOUNT";
    const PERCENT_PRICE_TYPE = '%';

    /**
     * After Get Totals
     *
     * @param \Magento\Sales\Block\Order\Totals $subject
     * @param array $totals
     * @return array
     */
    public function afterGetTotals(\Magento\Sales\Block\Order\Totals $subject, array $totals)
    {
        foreach ($totals as $total) {
            if ($total->getCode() == 'discount') {
                if ($subject->getOrder()->getAppliedRuleIds() == self::POS_CUSTOM_DISCOUNT
                    && $subject->getOrder()->getOsPosCustomDiscountAmount()
                ) {
                    $label = "Custom Discount";
                    if ($subject->getOrder()->getOsPosCustomDiscountType() == self::PERCENT_PRICE_TYPE) {
                        $label .= sprintf(" (%.2f%%)", $subject->getOrder()->getOsPosCustomDiscountAmount());
                    }
                    $total->setLabel($label);
                }
            }
        }
        return $totals;
    }
}
