<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Customer\Ui\Component\Listing;

use Magento\Customer\Api\Data\AttributeMetadataInterface as AttributeMetadata;

/**
 * Class Columns
 *
 * Plugin Columns data to change data type
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class Columns
{
    /**
     * Used to plugin select data type
     *
     * @param \Magento\Customer\Ui\Component\Listing\Columns $columns
     * @param array $result
     * @param array $attributeData
     * @param string $newAttributeCode
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterUpdateColumn(
        \Magento\Customer\Ui\Component\Listing\Columns $columns,
        $result,
        array $attributeData,
        $newAttributeCode
    ) {
        if ($attributeData[AttributeMetadata::ATTRIBUTE_CODE] == 'created_location_id') {
            $component = $columns->getComponent('created_location_id');
            $component->setData(
                'config',
                array_merge(
                    $component->getData('config'),
                    ['dataType' => 'select']
                )
            );
        }
    }
}
