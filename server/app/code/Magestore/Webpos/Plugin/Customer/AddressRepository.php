<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Customer;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Model\ResourceModel\Address\Collection;
use Magento\Framework\App\ObjectManager;

/**
 * Plugin - AddressRepository
 */
class AddressRepository
{
    /**
     * Before Save
     *
     * @param \Magento\Customer\Model\ResourceModel\AddressRepository $subject
     * @param AddressInterface $address
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSave(
        \Magento\Customer\Model\ResourceModel\AddressRepository $subject,
        AddressInterface $address
    ) {
        if ($address->getCustomAttributes()) {
            foreach ($address->getCustomAttributes() as $customAttribute) {
                if ($customAttribute->getAttributeCode() != 'sub_id') {
                    continue;
                }
                $subId = $customAttribute->getValue();

                $currentData = $this->getCustomerAddressBySubId($subId);
                if ($currentData) {
                    $address->setId($currentData);
                }
            }
        }
    }

    /**
     * Get Customer Address By Sub Id
     *
     * @param int $subId
     * @return int|bool
     */
    public function getCustomerAddressBySubId($subId)
    {
        $objectManager = ObjectManager::getInstance();
        /** @var Collection $addressCollection */
        $addressCollection = $objectManager->create(\Magento\Customer\Model\ResourceModel\Address\Collection::class);
        $addressCollection->addAttributeToFilter('sub_id', $subId);
        if ($addressCollection->getSize()) {
            return $addressCollection->getFirstItem()->getId();
        }
        return false;
    }
}
