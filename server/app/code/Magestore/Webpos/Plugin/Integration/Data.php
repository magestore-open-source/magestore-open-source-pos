<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Integration;

use Magento\Framework\Webapi\Rest\Request;

/**
 * Plugin - Integration Request Data
 */
class Data
{
    const PATH = 'integration';

    /**
     * @var Request
     */
    protected $request;

    /**
     * Validator constructor.
     * @param Request $request
     */
    public function __construct(
        Request $request
    ) {
        $this->request = $request;
    }

    /**
     * Before Map Resources
     *
     * @param \Magento\Integration\Helper\Data $helper
     * @param array $resources
     * @return array[]
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeMapResources(\Magento\Integration\Helper\Data $helper, array $resources)
    {
        $restricted = $this->getRestrictedIds();
        foreach ($resources as $key => $resource) {
            if (in_array($resource['id'], $restricted)) {
                unset($resources[$key]);
            }
        }
        return [$resources];
    }

    /**
     * Get Restricted Ids
     *
     * @return array|string[]
     */
    protected function getRestrictedIds()
    {
        $pathInfo = $this->request->getPathInfo();
        if (strpos($pathInfo, self::PATH) === false) {
            return ['Magestore_Webpos::posclient'];
        }

        return [];
    }
}
