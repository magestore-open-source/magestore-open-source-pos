<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\InventoryShippingAdminUi\Block\Adminhtml\Shipment;

use Magento\Framework\Registry;
use Magento\InventoryApi\Api\GetSourcesAssignedToStockOrderedByPriorityInterface;
use Magento\InventoryApi\Api\SourceRepositoryInterface;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;

/**
 * Plugin - Inventory
 */
class Inventory
{
    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepository;
    /**
     * @var GetSourcesAssignedToStockOrderedByPriorityInterface
     */
    protected $getSourcesAssignedToStock;

    /**
     * @param StockManagementInterface $stockManagement
     * @param LocationRepositoryInterface $locationRepository
     * @param GetSourcesAssignedToStockOrderedByPriorityInterface $getSourcesAssignedToStock
     */
    public function __construct(
        StockManagementInterface $stockManagement,
        LocationRepositoryInterface $locationRepository,
        GetSourcesAssignedToStockOrderedByPriorityInterface $getSourcesAssignedToStock
    ) {
        $this->stockManagement = $stockManagement;
        $this->locationRepository = $locationRepository;
        $this->getSourcesAssignedToStock = $getSourcesAssignedToStock;
    }

    /**
     * Around Get Source Code
     *
     * @param \Magento\InventoryShippingAdminUi\Block\Adminhtml\Shipment\Inventory $subject
     * @param callable $proceed
     * @return mixed
     */
    public function aroundGetSourceCode(
        \Magento\InventoryShippingAdminUi\Block\Adminhtml\Shipment\Inventory $subject,
        callable $proceed
    ) {
        $shipment = $subject->getShipment();
        if ($shipment->getOrder()) {
            $stockId = $this->stockManagement->getStockIdFromOrder($shipment->getOrder());
            if ($stockId) {
                $sourceCode = $this->getSourceCode($stockId);
                if ($sourceCode) {
                    return $sourceCode;
                }
            }
        }
        return $proceed();
    }

    /**
     * Get Source Code
     *
     * @param int $stockId
     * @return string|bool
     */
    public function getSourceCode($stockId)
    {
        $sources = $this->getSourcesAssignedToStock->execute($stockId);
        if ($sources) {
            foreach ($sources as $source) {
                if ($source->getEnabled()) {
                    return $source->getSourceCode();
                }
            }
        }
        return false;
    }
}
