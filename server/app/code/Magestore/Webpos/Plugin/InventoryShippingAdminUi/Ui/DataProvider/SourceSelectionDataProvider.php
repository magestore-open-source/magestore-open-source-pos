<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\InventoryShippingAdminUi\Ui\DataProvider;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface;
use Magento\InventorySalesApi\Model\GetSkuFromOrderItemInterface;
use Magento\InventorySalesApi\Model\StockByWebsiteIdResolverInterface;
use Magento\InventoryShippingAdminUi\Ui\DataProvider\GetSourcesByStockIdSkuAndQty;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\OrderRepository;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;

/**
 * Plugin - SourceSelectionDataProvider
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SourceSelectionDataProvider
{
    protected $sources = [];
    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var StockByWebsiteIdResolverInterface
     */
    private $stockByWebsiteIdResolver;
    /**
     * @var LocationRepositoryInterface
     */
    private $locationRepository;
    /**
     * @var GetSkuFromOrderItemInterface
     */
    private $getSkuFromOrderItem;

    /**
     * @var GetSourcesByStockIdSkuAndQty
     */
    private $getSourcesByStockIdSkuAndQty;

    /**
     * @var GetStockItemConfigurationInterface
     */
    private $getStockItemConfiguration;

    /**
     * @param StockManagementInterface $stockManagement
     * @param RequestInterface $request
     * @param OrderRepository $orderRepository
     * @param StockByWebsiteIdResolverInterface $stockByWebsiteIdResolver
     * @param LocationRepositoryInterface $locationRepository
     * @param GetSkuFromOrderItemInterface $getSkuFromOrderItem
     * @param GetSourcesByStockIdSkuAndQty $getSourcesByStockIdSkuAndQty
     * @param GetStockItemConfigurationInterface $getStockItemConfiguration
     */
    public function __construct(
        StockManagementInterface $stockManagement,
        RequestInterface $request,
        OrderRepository $orderRepository,
        StockByWebsiteIdResolverInterface $stockByWebsiteIdResolver,
        LocationRepositoryInterface $locationRepository,
        GetSkuFromOrderItemInterface $getSkuFromOrderItem,
        GetSourcesByStockIdSkuAndQty $getSourcesByStockIdSkuAndQty,
        GetStockItemConfigurationInterface $getStockItemConfiguration
    ) {
        $this->stockManagement = $stockManagement;
        $this->request = $request;
        $this->orderRepository = $orderRepository;
        $this->stockByWebsiteIdResolver = $stockByWebsiteIdResolver;
        $this->locationRepository = $locationRepository;
        $this->getSkuFromOrderItem = $getSkuFromOrderItem;
        $this->getSourcesByStockIdSkuAndQty = $getSourcesByStockIdSkuAndQty;
        $this->getStockItemConfiguration = $getStockItemConfiguration;
    }

    /**
     * Around Get Data
     *
     * @param \Magento\InventoryShippingAdminUi\Ui\DataProvider\SourceSelectionDataProvider $subject
     * @param callable $proceed
     * @return array
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundGetData(
        \Magento\InventoryShippingAdminUi\Ui\DataProvider\SourceSelectionDataProvider $subject,
        callable $proceed
    ) {
        $data = [];
        $orderId = $this->request->getParam('order_id');
        /** @var Order $order */
        $order = $this->orderRepository->get($orderId);
        $websiteId = $order->getStore()->getWebsiteId();

        $stockId = $this->stockManagement->getStockIdFromOrder($order);
        if (!$stockId) {
            return $proceed();
        }

        foreach ($order->getAllItems() as $orderItem) {
            if ($orderItem->getIsVirtual()
                || $orderItem->getLockedDoShip()
                || $orderItem->getHasChildren()) {
                continue;
            }

            $item = $orderItem->isDummy(true) ? $orderItem->getParentItem() : $orderItem;
            $qty = $item->getSimpleQtyToShip();
            $qty = $this->castQty($item, $qty);
            $sku = $this->getSkuFromOrderItem->execute($item);
            $data[$orderId]['items'][] = [
                'orderItemId' => $item->getId(),
                'sku' => $sku,
                'product' => $this->getProductName($orderItem),
                'qtyToShip' => $qty,
                'sources' => $this->getSources($stockId, $sku, $qty),
                'isManageStock' => $this->isManageStock($sku, $stockId)
            ];
        }
        $data[$orderId]['websiteId'] = $websiteId;
        $data[$orderId]['order_id'] = $orderId;
        foreach ($this->sources as $code => $name) {
            $data[$orderId]['sourceCodes'][] = [
                'value' => $code,
                'label' => $name
            ];
        }

        return $data;
    }

    /**
     * Get Sources
     *
     * @param int $stockId
     * @param string $sku
     * @param float $qty
     * @return array
     * @throws NoSuchEntityException
     */
    private function getSources(int $stockId, string $sku, float $qty)
    {
        $sources = $this->getSourcesByStockIdSkuAndQty->execute($stockId, $sku, $qty);
        foreach ($sources as $source) {
            $this->sources[$source['sourceCode']] = $source['sourceName'];
        }
        return $sources;
    }

    /**
     * Is Manage Stock
     *
     * @param string $itemSku
     * @param int $stockId
     * @return bool
     * @throws LocalizedException
     */
    private function isManageStock($itemSku, $stockId)
    {
        $stockItemConfiguration = $this->getStockItemConfiguration->execute($itemSku, $stockId);

        return $stockItemConfiguration->isManageStock();
    }

    /**
     * Generate display product name
     *
     * @param Item $item
     * @return null|string
     */
    private function getProductName(Item $item)
    {
        //TODO: need to transfer this to html block and render on Ui
        $name = $item->getName();
        if ($parentItem = $item->getParentItem()) {
            $name = $parentItem->getName();
            $options = [];
            if ($productOptions = $parentItem->getProductOptions()) {
                if (isset($productOptions['options'])) {
                    $options = array_merge($options, $productOptions['options']);
                }
                if (isset($productOptions['additional_options'])) {
                    $options = array_merge($options, $productOptions['additional_options']);
                }
                if (isset($productOptions['attributes_info'])) {
                    $options = array_merge($options, $productOptions['attributes_info']);
                }
                if (count($options)) {
                    foreach ($options as $option) {
                        $name .= '<dd>' . $option['label'] . ': ' . $option['value'] . '</dd>';
                    }
                } else {
                    $name .= '<dd>' . $item->getName() . '</dd>';
                }
            }
        }

        return $name;
    }

    /**
     * Cast Qty
     *
     * @param Item $item
     * @param string|int|float $qty
     * @return float|int
     */
    private function castQty(Item $item, $qty)
    {
        if ($item->getIsQtyDecimal()) {
            $qty = (double)$qty;
        } else {
            $qty = (int)$qty;
        }

        return $qty > 0 ? $qty : 0;
    }
}
