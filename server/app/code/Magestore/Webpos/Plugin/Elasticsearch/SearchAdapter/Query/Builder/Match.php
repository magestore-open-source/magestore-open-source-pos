<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Plugin\Elasticsearch\SearchAdapter\Query\Builder;

use Magento\Elasticsearch\SearchAdapter\Query\Builder\Match as MatchCore;
use Magento\Framework\Search\Request\QueryInterface as RequestQueryInterface;

/**
 * Class Match
 *
 * Plugin match elastic search to check condition and add partial search
 */
class Match
{
    /**
     * After build to change query match
     *
     * @param MatchCore $subject
     * @param array $result
     * @param array $selectQuery
     * @param RequestQueryInterface $requestQuery
     * @param string $conditionType
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterBuild(
        MatchCore $subject,
        array $result,
        array $selectQuery,
        RequestQueryInterface $requestQuery,
        string $conditionType
    ) {
        if ($requestQuery->getName() !== 'webpos_search_key') {
            return $result;
        }

        if (isset($result['bool']['should'])) {
            $fieldArrayMatches = $result['bool']['should'];
            $fieldArrayMatchPrefix = [];
            foreach ($fieldArrayMatches as $fieldArrayMatch) {
                if (isset($fieldArrayMatch['match'])) {
                    $fieldArrayMatch['match_phrase_prefix'] = $fieldArrayMatch['match'];
                    unset($fieldArrayMatch['match']);
                }
                $fieldArrayMatchPrefix[] = $fieldArrayMatch;
            }
            $result['bool']['should'] = $fieldArrayMatchPrefix;
        }
        return $result;
    }
}
