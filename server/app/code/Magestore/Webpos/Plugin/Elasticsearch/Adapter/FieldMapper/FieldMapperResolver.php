<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Elasticsearch\Adapter\FieldMapper;

use Closure;
use Magento\Elasticsearch\Model\Config;
use Magestore\Webpos\Model\Adapter\FieldMapper\ProductFieldMapperProxy;

/**
 * Class FieldMapperResolver
 *
 * Plugin field map resolver to solve problem on magento 2.3.3
 */
class FieldMapperResolver
{
    /**
     * @var ProductFieldMapperProxy
     */
    private $productFieldMapper;

    /**
     * FieldMapperResolver constructor.
     *
     * @param ProductFieldMapperProxy $productFieldMapper
     */
    public function __construct(
        ProductFieldMapperProxy $productFieldMapper
    ) {
        $this->productFieldMapper = $productFieldMapper;
    }

    /**
     * Check if entity is webpos search, call webpos product field directly
     *
     * Resolve the problem can not reindex webpos search in magento 2.3.3
     *
     * @param \Magento\Elasticsearch\Model\Adapter\FieldMapper\FieldMapperResolver $subject
     * @param Closure $proceed
     * @param string $attributeCode
     * @param array $context
     * @return mixed|string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundGetFieldName(
        \Magento\Elasticsearch\Model\Adapter\FieldMapper\FieldMapperResolver $subject,
        Closure $proceed,
        $attributeCode,
        $context = []
    ) {
        $entityType = isset($context['entityType']) ? $context['entityType'] : Config::ELASTICSEARCH_TYPE_DEFAULT;
        if ($entityType == 'webpos_productsearch_fulltext') {
            return $this->productFieldMapper->getFieldName($attributeCode, $context);
        } else {
            return $proceed($attributeCode, $context);
        }
    }

    /**
     * Check if entity is webpos search, call webpos product field directly
     *
     * Resolve the problem can not reindex webpos search in magento 2.3.3
     *
     * @param \Magento\Elasticsearch\Model\Adapter\FieldMapper\FieldMapperResolver $subject
     * @param Closure $proceed
     * @param array $context
     * @return array|mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundGetAllAttributesTypes(
        \Magento\Elasticsearch\Model\Adapter\FieldMapper\FieldMapperResolver $subject,
        Closure $proceed,
        $context = []
    ) {
        $entityType = isset($context['entityType']) ? $context['entityType'] : Config::ELASTICSEARCH_TYPE_DEFAULT;
        if ($entityType == 'webpos_productsearch_fulltext') {
            return $this->productFieldMapper->getAllAttributesTypes($context);
        } else {
            return $proceed($context);
        }
    }
}
