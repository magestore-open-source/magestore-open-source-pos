<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Elasticsearch\Model\ResourceModel;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\Registry;

/**
 * Plugin resource model engine
 */
class Engine
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * Engine constructor.
     *
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * After Get Allowed Visibility
     *
     * @param \Magento\Elasticsearch\Model\ResourceModel\Engine $subject
     * @param int[] $result
     * @return int[]
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetAllowedVisibility(\Magento\Elasticsearch\Model\ResourceModel\Engine $subject, $result)
    {
        if ($this->registry->registry('webpos_productsearch_fulltext')) {
            $result = array_merge($result, [Visibility::VISIBILITY_NOT_VISIBLE]);
        }
        return $result;
    }
}
