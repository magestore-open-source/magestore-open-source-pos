<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Plugin\InventoryApi;

use Magento\Framework\Indexer\IndexerRegistry;
use Magento\InventoryApi\Api\Data\SourceInterface;
use Magento\InventoryApi\Api\SourceRepositoryInterface;
use Magento\InventoryIndexer\Model\ResourceModel\IsInvalidationRequiredForSource;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Model\Indexer\Product\Processor;

/**
 * Invalidate Inventory Indexer after Source was enabled or disabled.
 */
class InvalidateAfterEnablingOrDisablingSourcePlugin
{
    /**
     * @var IndexerRegistry
     */
    private $indexerRegistry;

    /**
     * @var IsInvalidationRequiredForSource
     */
    private $isInvalidationRequiredForSource;
    /**
     * @var Data
     */
    private $helper;

    /**
     * InvalidateAfterEnablingOrDisablingSourcePlugin constructor.
     *
     * @param IndexerRegistry $indexerRegistry
     * @param IsInvalidationRequiredForSource $isInvalidationRequiredForSource
     * @param Data $helper
     */
    public function __construct(
        IndexerRegistry $indexerRegistry,
        IsInvalidationRequiredForSource $isInvalidationRequiredForSource,
        Data $helper
    ) {
        $this->indexerRegistry = $indexerRegistry;
        $this->isInvalidationRequiredForSource = $isInvalidationRequiredForSource;
        $this->helper = $helper;
    }

    /**
     * Invalidate Inventory Indexer after Source was enabled or disabled.
     *
     * @param SourceRepositoryInterface $subject
     * @param callable $proceed
     * @param SourceInterface $source
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundSave(
        SourceRepositoryInterface $subject,
        callable $proceed,
        SourceInterface $source
    ) {
        $invalidationRequired = false;
        if ($source->getSourceCode()) {
            $invalidationRequired = $this->isInvalidationRequiredForSource->execute(
                $source->getSourceCode(),
                (bool)$source->isEnabled()
            );
        }

        $proceed($source);

        if ($this->helper->isEnableElasticSearch() && $invalidationRequired) {
            $indexer = $this->indexerRegistry->get(Processor::INDEXER_ID);
            if ($indexer->isValid()) {
                $indexer->invalidate();
            }
        }
    }
}
