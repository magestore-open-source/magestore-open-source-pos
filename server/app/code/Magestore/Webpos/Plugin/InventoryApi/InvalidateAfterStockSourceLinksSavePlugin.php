<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Plugin\InventoryApi;

use Magento\Framework\Indexer\IndexerRegistry;
use Magento\InventoryApi\Api\StockSourceLinksSaveInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Model\Indexer\Product\Processor;

/**
 * Invalidate index after source links have been saved.
 */
class InvalidateAfterStockSourceLinksSavePlugin
{
    /**
     * @var IndexerRegistry
     */
    private $indexerRegistry;
    /**
     * @var Data
     */
    private $helper;

    /**
     * InvalidateAfterStockSourceLinksSavePlugin constructor.
     *
     * @param IndexerRegistry $indexerRegistry
     * @param Data $helper
     */
    public function __construct(
        IndexerRegistry $indexerRegistry,
        Data $helper
    ) {
        $this->indexerRegistry = $indexerRegistry;
        $this->helper = $helper;
    }

    /**
     * Invalidate index after source links have been saved.
     *
     * @param StockSourceLinksSaveInterface $subject
     * @param void $result
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(
        StockSourceLinksSaveInterface $subject,
        $result
    ) {
        $indexer = $this->indexerRegistry->get(Processor::INDEXER_ID);
        if ($this->helper->isEnableElasticSearch() && $indexer->isValid()) {
            $indexer->invalidate();
        }
    }
}
