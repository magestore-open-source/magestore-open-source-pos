<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Order\Address;

use Closure;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Sales\Model\Order\Address;

/**
 * Plugin - Validator
 */
class Validator
{
    const WEBPOS_PATH = 'webpos';

    /**
     * @var Request
     */
    protected $request;

    /**
     * Validator constructor.
     * @param Request $request
     */
    public function __construct(
        Request $request
    ) {
        $this->request = $request;
    }

    /**
     * Around Validate
     *
     * @param Address\Validator $subject
     * @param Closure $proceed
     * @param Address $address
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundValidate(
        Address\Validator $subject,
        Closure $proceed,
        Address $address
    ) {
        $pathInfo = $this->request->getPathInfo();
        if (strpos($pathInfo, self::WEBPOS_PATH) !== false) {
            $isPosRequest = true;
        } else {
            $isPosRequest = false;
        }
        if ($isPosRequest && !$address->getCustomerId()) {
            return [];
        } else {
            return $proceed($address);
        }
    }
}
