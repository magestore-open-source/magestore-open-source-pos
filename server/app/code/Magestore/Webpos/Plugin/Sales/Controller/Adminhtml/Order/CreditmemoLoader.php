<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Sales\Controller\Adminhtml\Order;

use Magento\Framework\Registry;

/**
 * Plugin - CreditmemoLoader
 */
class CreditmemoLoader
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * CreditmemoLoader constructor.
     *
     * @param Registry $registry
     */
    public function __construct(
        Registry $registry
    ) {
        $this->registry = $registry;
    }

    /**
     * Before load
     *
     * @param \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $subject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeLoad(
        \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $subject
    ) {
        $this->registry->unregister('current_creditmemo');
    }
}
