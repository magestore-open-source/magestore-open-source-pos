<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Sales\Model\Order;

use Magento\Framework\App\ObjectManager;

/**
 * Plugin - Payment
 */
class Payment
{
    /**
     * After Prepend Message
     *
     * @param \Magento\Sales\Model\Order\Payment $subject
     * @param string|\Magento\Sales\Model\Order\Status\History $result
     * @param string|\Magento\Sales\Model\Order\Status\History $messagePrependTo
     * @return array|mixed|string|string[]
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterPrependMessage(
        \Magento\Sales\Model\Order\Payment $subject,
        $result,
        $messagePrependTo
    ) {
        $registry = ObjectManager::getInstance()
            ->get(\Magento\Framework\Registry::class);
        $newMessage = null;
        if ($registry->registry('create_creditmemo_webpos') && strpos($messagePrependTo, 'We refunded') === 0) {
            $newMessage = str_replace('offline.', 'on POS system.', $messagePrependTo);
        }
        if ($newMessage) {
            return $newMessage;
        } else {
            return $result;
        }
    }
}
