<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Plugin\FixtureGenerator;

use Magento\Setup\Model\FixtureGenerator\EntityGeneratorFactory;
use Magestore\Webpos\Api\DataProvider\IsSkipReindexPluginInterface;

/**
 * Class EntityGeneratorFactoryPlugin
 *
 * Plugin to ignore index when generate fixture
 */
class EntityGeneratorFactoryPlugin
{
    /**
     * @var IsSkipReindexPluginInterface
     */
    protected $isSkipReindexPlugin;

    /**
     * Constructor
     *
     * @param IsSkipReindexPluginInterface $isSkipReindexPlugin
     */
    public function __construct(
        IsSkipReindexPluginInterface $isSkipReindexPlugin
    ) {
        $this->isSkipReindexPlugin = $isSkipReindexPlugin;
    }

    /**
     * Skip reindex plugin when generate fixture
     *
     * @param EntityGeneratorFactory $subject
     * @param array $data
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeCreate(
        EntityGeneratorFactory $subject,
        array $data
    ): array {
        $this->isSkipReindexPlugin->setIsSkipReindexPlugin(true);
        return [$data];
    }
}
