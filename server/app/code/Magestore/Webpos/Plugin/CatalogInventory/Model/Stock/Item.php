<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\CatalogInventory\Model\Stock;

use Magento\CatalogInventory\Model\Stock;
use Magento\Framework\Registry;
use Magestore\Webpos\Helper\Data;
use Magento\CatalogInventory\Model\Stock\Item as CoreStockItem;

/**
 * Plugin function getBackOrders of stock item
 */
class Item
{
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * Item constructor.
     *
     * @param Registry $registry
     * @param Data $helper
     */
    public function __construct(
        Registry $registry,
        Data $helper
    ) {
        $this->registry = $registry;
        $this->helper = $helper;
    }

    /**
     * After get backorders
     *
     * @param CoreStockItem $stockItem
     * @param int $isBackOrdered
     * @return int
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetBackorders(
        CoreStockItem $stockItem,
        $isBackOrdered
    ) {
        $isEnableCheckoutWithOutOfStockproduct = (int) $this->helper->getStoreConfig(
            'webpos/checkout/add_out_of_stock_product'
        );
        if ($isEnableCheckoutWithOutOfStockproduct && $this->registry->registry('create_order_webpos')) {
            return Stock::BACKORDERS_YES_NONOTIFY;
        }
        return $isBackOrdered;
    }
}
