<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Catalog\Product;

use Magestore\Webpos\Model\Indexer\Product;

/**
 * Class \Magestore\Webpos\Plugin\Catalog\Product\Action
 */
class Action
{
    /**
     * @var Product
     */
    protected $webposIndexer;

    /**
     * Action constructor.
     *
     * @param Product $webposIndexer
     */
    public function __construct(
        Product $webposIndexer
    ) {
        $this->webposIndexer = $webposIndexer;
    }

    /**
     * After update attributes, reindex pos search
     *
     * @param \Magento\Catalog\Model\Product\Action $subject
     * @param \Magento\Catalog\Model\Product\Action $result
     * @param array $productIds
     * @param array $attrData
     * @param int $storeId
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterUpdateAttributes(
        \Magento\Catalog\Model\Product\Action $subject,
        $result,
        $productIds,
        $attrData,
        $storeId
    ) {
        $this->webposIndexer->executeList($productIds);
        return $result;
    }
}
