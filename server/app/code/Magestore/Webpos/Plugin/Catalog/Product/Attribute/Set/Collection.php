<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Catalog\Product\Attribute\Set;

use Magento\Framework\App\RequestInterface;

/**
 * Plugin Attribute Set Collection
 */
class Collection
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Collection constructor.
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * Before Set Entity Type Filter
     *
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $subject
     * @param int $typeId
     * @return array
     */
    public function beforeSetEntityTypeFilter(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $subject,
        $typeId
    ) {
        if ($this->request->getModuleName() == 'catalog') {
            $subject->addFieldToFilter('attribute_set_name', ['nin' => ['Custom_Sale_Attribute_Set']]);
        }
        return [$typeId];
    }
}
