<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Catalog\Product\ProductTypes;

use Magento\Framework\App\ObjectManager;
use Magestore\Webpos\Helper\Product\CustomSale;
use Magestore\Webpos\Model\Service\Catalog\IsHideCustomSaleFlag;

/**
 * Class Config
 *
 * Used to hide option custom sale
 */
class Config extends \Magento\Catalog\Model\ProductTypes\Config
{
    /**
     * After get all
     *
     * @param \Magento\Catalog\Model\ProductTypes\Config $subject
     * @param array $result
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetAll(
        \Magento\Catalog\Model\ProductTypes\Config $subject,
        $result
    ) {
        $objectManager = ObjectManager::getInstance();
        $hideCustomeSale = $objectManager->get(IsHideCustomSaleFlag::class);
        $isHideCustomSaleFlag = $hideCustomeSale->getHideCustomSaleFlag();
        if ($isHideCustomSaleFlag) {
            if (isset($result[CustomSale::TYPE])) {
                unset($result[CustomSale::TYPE]);
            }
            $hideCustomeSale->setHideCustomSaleFlag(0);
        }
        return $result;
    }
}
