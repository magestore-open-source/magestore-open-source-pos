<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\Ui\Component\Form\Element;

use Magestore\Webpos\Model\Service\Catalog\IsHideCustomSaleFlag;

/**
 * Class Select
 *
 * Plugin select option
 */
class Select
{
    /**
     * @var IsHideCustomSaleFlag
     */
    protected $isHideCustomSaleFlag;

    /**
     * Select constructor.
     *
     * @param IsHideCustomSaleFlag $isHideCustomSaleFlag
     */
    public function __construct(
        IsHideCustomSaleFlag $isHideCustomSaleFlag
    ) {
        $this->isHideCustomSaleFlag = $isHideCustomSaleFlag;
    }

    /**
     * Before prepare
     *
     * @param \Magento\Ui\Component\Form\Element\Select $select
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforePrepare(
        \Magento\Ui\Component\Form\Element\Select $select
    ) {
        $this->isHideCustomSaleFlag->setHideCustomSaleFlag(1);
    }
}
