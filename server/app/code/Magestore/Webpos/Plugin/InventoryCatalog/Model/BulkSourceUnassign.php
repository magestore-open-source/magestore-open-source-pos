<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\InventoryCatalog\Model;

use Magento\Catalog\Model\ResourceModel\Product;
use Magento\InventoryCatalog\Model\BulkSourceUnassign as CoreBulkSourceUnassign;
use Magestore\Webpos\Api\MultiSourceInventory\GetStockIdsBySourceCodesInterface;
use Magestore\Webpos\Api\Log\ProductDeletedRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item;

/**
 * After unassign products from sources
 */
class BulkSourceUnassign
{
    /**
     * @var Item
     */
    private $stockItemResource;
    /**
     * @var ProductDeletedRepositoryInterface
     */
    private $productDeletedRepository;
    /**
     * @var Product
     */
    private $productResource;
    /**
     * @var GetStockIdsBySourceCodesInterface
     */
    private $getStockIdsBySourceCodes;

    /**
     * BulkSourceUnassign constructor.
     * @param Item $stockItemResource
     * @param ProductDeletedRepositoryInterface $productDeletedRepository
     * @param Product $productResource
     * @param GetStockIdsBySourceCodesInterface $getStockIdsBySourceCodes
     */
    public function __construct(
        Item $stockItemResource,
        ProductDeletedRepositoryInterface $productDeletedRepository,
        Product $productResource,
        GetStockIdsBySourceCodesInterface $getStockIdsBySourceCodes
    ) {
        $this->stockItemResource = $stockItemResource;
        $this->productDeletedRepository = $productDeletedRepository;
        $this->productResource = $productResource;
        $this->getStockIdsBySourceCodes = $getStockIdsBySourceCodes;
    }

    /**
     * After execute
     *
     * @param CoreBulkSourceUnassign $subject
     * @param int $result
     * @param array $skus
     * @param array $sourceCodes
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(
        CoreBulkSourceUnassign $subject,
        int $result,
        array $skus,
        array $sourceCodes
    ) {
        if (count($skus)) {
            $this->stockItemResource->updateUpdatedTimeBySku($skus);
            $productIds = $this->productResource->getProductsIdsBySkus($skus);
            $stockIds = $this->getStockIdsBySourceCodes->execute($sourceCodes);

            if (count($stockIds)) {
                foreach (array_values($productIds) as $productId) {
                    $this->productDeletedRepository->insertByProductIdAndStock($productId, $stockIds);
                }
            }
        }
        return $result;
    }
}
