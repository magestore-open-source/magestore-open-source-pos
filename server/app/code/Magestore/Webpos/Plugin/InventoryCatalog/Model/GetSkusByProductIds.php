<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\InventoryCatalog\Model;

use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\InventoryCatalogApi\Model\GetSkusByProductIdsInterface;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;
use Magestore\Webpos\Model\Checkout\PosOrder;

/**
 * Plugin - GetSkusByProductIds
 */
class GetSkusByProductIds
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ProductResourceModel
     */
    private $productResource;

    /**
     * GetSkusByProductIds constructor.
     *
     * @param RequestInterface $request
     * @param ProductResourceModel $productResource
     */
    public function __construct(
        RequestInterface $request,
        ProductResourceModel $productResource
    ) {
        $this->request = $request;
        $this->productResource = $productResource;
    }

    /**
     * Before get sku by product id
     *
     * @param GetSkusByProductIdsInterface $subject
     * @param array $productIds
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeExecute(GetSkusByProductIdsInterface $subject, array $productIds)
    {
        if (!$this->request->getParam(RequestProcessor::SESSION_PARAM_KEY)
            || !$this->request->getParam(PosOrder::PARAM_ORDER_LOCATION_ID)) {
            return null;
        }
        // Only update stock for existing products (remove deleted products)
        $skuByIds = array_column(
            $this->productResource->getProductsSku($productIds),
            ProductInterface::SKU,
            'entity_id'
        );
        return [array_keys($skuByIds)];
    }
}
