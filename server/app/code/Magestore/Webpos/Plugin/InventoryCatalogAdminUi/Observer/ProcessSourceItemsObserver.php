<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\InventoryCatalogAdminUi\Observer;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Event\Observer as EventObserver;
use Magestore\Webpos\Api\Log\ProductDeletedRepositoryInterface;
use Magestore\Webpos\Model\ResourceModel\Catalog\Product\Collection;
use Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item;

/**
 * Plugin - ProcessSourceItemsObserver
 */
class ProcessSourceItemsObserver
{
    /**
     * @var ProductDeletedRepositoryInterface
     */
    protected $productDeletedRepository;

    /**
     * @param ProductDeletedRepositoryInterface $productDeletedRepository
     */
    public function __construct(
        ProductDeletedRepositoryInterface $productDeletedRepository
    ) {
        $this->productDeletedRepository = $productDeletedRepository;
    }

    /**
     * Arround Execute
     *
     * @param \Magento\InventoryCatalogAdminUi\Observer\ProcessSourceItemsObserver $subject
     * @param callable $proceed
     * @param EventObserver $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function arroundExecute(
        $subject,
        callable $proceed,
        EventObserver $observer
    ) {
        $proceed($observer);
        /** @var ProductInterface $product */
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        if ($product->getStatus() == Status::STATUS_DISABLED
            || $product->getData('webpos_visible') != Collection::VISIBLE_ON_WEBPOS
        ) {
            if ($productId) {
                $this->productDeletedRepository->insertByProductId($productId);
            }
        } elseif ($product->getStatus() == Status::STATUS_ENABLED
            && $product->getData('webpos_visible') == Collection::VISIBLE_ON_WEBPOS
        ) {
            $this->productDeletedRepository->deleteByProduct($product);
        }
    }
}
