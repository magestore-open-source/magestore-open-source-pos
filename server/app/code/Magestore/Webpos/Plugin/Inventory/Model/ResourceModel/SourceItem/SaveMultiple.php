<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\Inventory\Model\ResourceModel\SourceItem;

use Exception;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Inventory\Model\ResourceModel\SourceItem\SaveMultiple as CoreSaveMultiple;
use Magestore\Webpos\Api\DataProvider\IsSkipReindexPluginInterface;
use Magestore\Webpos\Helper\Data;
use Magestore\Webpos\Model\ResourceModel\Inventory\Stock\Item;
use Psr\Log\LoggerInterface;

/**
 * Reindex product search on elasticsearch server when source items have been updated
 */
class SaveMultiple
{
    /**
     * @var Item
     */
    protected $sourceItemResource;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var IsSkipReindexPluginInterface
     */
    protected $isSkipReindexPlugin;

    /**
     * SaveMultiple constructor.
     *
     * @param Item $sourceItemResource
     * @param Data $helper
     * @param LoggerInterface $logger
     * @param Registry $registry
     * @param IsSkipReindexPluginInterface|null $isSkipReindexPlugin
     */
    public function __construct(
        Item $sourceItemResource,
        Data $helper,
        LoggerInterface $logger,
        Registry $registry,
        $isSkipReindexPlugin = null
    ) {
        $this->sourceItemResource = $sourceItemResource;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->registry = $registry;
        $this->isSkipReindexPlugin = $isSkipReindexPlugin ?: ObjectManager::getInstance()
            ->get(IsSkipReindexPluginInterface::class);
    }

    /**
     * Reindex by rows
     *
     * @param CoreSaveMultiple $subject
     * @param array $sourceItems
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeExecute(
        CoreSaveMultiple $subject,
        array $sourceItems
    ) {
        if (!$this->helper->isEnableElasticSearch()) {
            return;
        }

        if ($this->isSkipReindexPlugin->getIsSkipReindexPlugin()) {
            return;
        }

        $ids = $this->sourceItemResource->reindexBySourceItem($sourceItems);

        try {
            $this->registry->unregister(
                'products_need_to_be_reindexed_pos_search'
            );
            $this->registry->register('products_need_to_be_reindexed_pos_search', $ids);
        } catch (Exception $e) {
            $this->logger->info($e->getMessage());
            $this->logger->info($e->getTraceAsString());
        }
    }
}
