<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\InventorySales\Model;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventorySalesApi\Model\GetSkuFromOrderItemInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\InventoryCatalogApi\Model\GetSkusByProductIdsInterface;
use Magento\InventoryConfigurationApi\Model\IsSourceItemManagementAllowedForProductTypeInterface;
use Magestore\Webpos\Model\Checkout\PosOrder;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;

/**
 * Plugin - GetSkuFromOrderItem
 */
class GetSkuFromOrderItem
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var GetSkusByProductIdsInterface
     */
    private $getSkusByProductIds;

    /**
     * @var IsSourceItemManagementAllowedForProductTypeInterface
     */
    private $isSourceItemManagementAllowedForProductType;

    /**
     * GetSkuFromOrderItem constructor.
     *
     * @param RequestInterface $request
     * @param GetSkusByProductIdsInterface $getSkusByProductIds
     * @param IsSourceItemManagementAllowedForProductTypeInterface $isSourceItemManagementAllowedForProductType
     */
    public function __construct(
        RequestInterface $request,
        GetSkusByProductIdsInterface $getSkusByProductIds,
        IsSourceItemManagementAllowedForProductTypeInterface $isSourceItemManagementAllowedForProductType
    ) {
        $this->request = $request;
        $this->getSkusByProductIds = $getSkusByProductIds;
        $this->isSourceItemManagementAllowedForProductType = $isSourceItemManagementAllowedForProductType;
    }

    /**
     * Change process get sku by order item
     *
     * @param GetSkuFromOrderItemInterface $subject
     * @param callable $proceed
     * @param OrderItemInterface $orderItem
     * @return mixed|string
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        GetSkuFromOrderItemInterface $subject,
        callable $proceed,
        OrderItemInterface $orderItem
    ) {
        if (!$this->request->getParam(RequestProcessor::SESSION_PARAM_KEY)
            || !$this->request->getParam(PosOrder::PARAM_ORDER_LOCATION_ID)) {
            return $proceed($orderItem);
        }

        // Allow create shipment for deleted products
        if ($this->isSourceItemManagementAllowedForProductType->execute($orderItem->getProductType())) {
            $itemSkus = $this->getSkusByProductIds->execute([$orderItem->getProductId()]);
            if (isset($itemSkus[$orderItem->getProductId()])) {
                return $itemSkus[$orderItem->getProductId()];
            }
        }
        return $orderItem->getSku();
    }
}
