<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\InventorySales\Model\ResourceModel;

use Magento\Framework\App\RequestInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;

/**
 * Plugin - StockIdResolver
 */
class StockIdResolver
{
    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /***
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * StockIdResolver constructor.
     * @param RequestInterface $request
     * @param OrderRepositoryInterface $orderRepository
     * @param StockManagementInterface $stockManagement
     */
    public function __construct(
        RequestInterface $request,
        OrderRepositoryInterface $orderRepository,
        StockManagementInterface $stockManagement
    ) {
        $this->request = $request;
        $this->stockManagement = $stockManagement;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Around Resolve
     *
     * @param \Magento\InventorySales\Model\ResourceModel\StockIdResolver $subject
     * @param callable $proceed
     * @param string $type
     * @param string $code
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundResolve(
        $subject,
        callable $proceed,
        string $type,
        string $code
    ) {
        if ($this->request->getControllerName().'_'.$this->request->getActionName() == 'order_invoice_save') {
            $orderId = $this->request->getParam('order_id');
            $order = $this->orderRepository->get($orderId);
            if ($order->getId()) {
                $stockId = $this->stockManagement->getStockIdFromOrder($order);
            }
        } else {
            $stockId = $this->stockManagement->getStockId();
        }

        return $stockId ? $stockId : $proceed($type, $code);
    }
}
