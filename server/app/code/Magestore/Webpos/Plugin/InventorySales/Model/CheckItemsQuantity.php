<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Plugin\InventorySales\Model;

use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;

/**
 * Plugin - CheckItemsQuantity
 */
class CheckItemsQuantity
{
    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;

    /**
     * StockIdResolver constructor.
     * @param StockManagementInterface $stockManagement
     */
    public function __construct(
        StockManagementInterface $stockManagement
    ) {
        $this->stockManagement = $stockManagement;
    }

    /**
     * Around Execute
     *
     * @param \Magento\InventorySales\Model\CheckItemsQuantity $subject
     * @param callable $proceed
     * @param array $items
     * @param int $stockId
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        $subject,
        callable $proceed,
        array $items,
        int $stockId
    ) {
        if ($this->stockManagement->getStockId()) {
            return true;
        }
        return $proceed($items, $stockId);
    }
}
