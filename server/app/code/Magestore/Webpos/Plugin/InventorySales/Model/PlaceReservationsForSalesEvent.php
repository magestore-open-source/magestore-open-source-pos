<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Plugin\InventorySales\Model;

use Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use Magento\InventorySalesApi\Api\Data\SalesEventInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\InventoryReservationsApi\Model\AppendReservationsInterface;
use Magento\InventoryReservationsApi\Model\ReservationBuilderInterface;
use Magento\InventorySalesApi\Api\Data\ItemToSellInterface;
use Magento\InventorySalesApi\Api\GetStockBySalesChannelInterface;
use Magento\InventoryConfigurationApi\Model\IsSourceItemManagementAllowedForProductTypeInterface;
use Magento\InventoryCatalogApi\Model\GetProductTypesBySkusInterface;
use Magento\InventorySales\Model\SalesEventToArrayConverter;
use Magento\Sales\Model\OrderRepository;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Webpos\Api\MultiSourceInventory\StockManagementInterface;

/**
 * Class PlaceReservationsForSalesEvent
 *
 * Place reservation for sale event
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PlaceReservationsForSalesEvent
{
    /**
     * @var StockManagementInterface
     */
    protected $stockManagement;
    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var ReservationBuilderInterface
     */
    private $reservationBuilder;

    /**
     * @var AppendReservationsInterface
     */
    private $appendReservations;

    /**
     * @var GetStockBySalesChannelInterface
     */
    private $getStockBySalesChannel;

    /**
     * @var GetProductTypesBySkusInterface
     */
    private $getProductTypesBySkus;

    /**
     * @var IsSourceItemManagementAllowedForProductTypeInterface
     */
    private $isSourceItemManagementAllowedForProductType;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var SalesEventToArrayConverter
     */
    private $salesEventToArrayConverter;

    /**
     * PlaceReservationsForSalesEvent constructor.
     *
     * @param StockManagementInterface $stockManagement
     * @param LocationRepositoryInterface $locationRepository
     * @param OrderRepository $orderRepository
     * @param ReservationBuilderInterface $reservationBuilder
     * @param AppendReservationsInterface $appendReservations
     * @param GetStockBySalesChannelInterface $getStockBySalesChannel
     * @param GetProductTypesBySkusInterface $getProductTypesBySkus
     * @param IsSourceItemManagementAllowedForProductTypeInterface $isSourceItemManagementAllowedForProductType
     * @param SerializerInterface $serializer
     * @param SalesEventToArrayConverter $salesEventToArrayConverter
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        StockManagementInterface $stockManagement,
        LocationRepositoryInterface $locationRepository,
        OrderRepository $orderRepository,
        ReservationBuilderInterface $reservationBuilder,
        AppendReservationsInterface $appendReservations,
        GetStockBySalesChannelInterface $getStockBySalesChannel,
        GetProductTypesBySkusInterface $getProductTypesBySkus,
        IsSourceItemManagementAllowedForProductTypeInterface $isSourceItemManagementAllowedForProductType,
        SerializerInterface $serializer,
        SalesEventToArrayConverter $salesEventToArrayConverter
    ) {
        $this->stockManagement = $stockManagement;
        $this->locationRepository = $locationRepository;
        $this->orderRepository = $orderRepository;
        $this->reservationBuilder = $reservationBuilder;
        $this->appendReservations = $appendReservations;
        $this->getStockBySalesChannel = $getStockBySalesChannel;
        $this->getProductTypesBySkus = $getProductTypesBySkus;
        $this->isSourceItemManagementAllowedForProductType = $isSourceItemManagementAllowedForProductType;
        $this->serializer = $serializer;
        $this->salesEventToArrayConverter = $salesEventToArrayConverter;
    }

    /**
     * Around execute
     *
     * @param \Magento\InventorySales\Model\PlaceReservationsForSalesEvent $subject
     * @param callable $proceed
     * @param array $items
     * @param SalesChannelInterface $salesChannel
     * @param SalesEventInterface $salesEvent
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(
        \Magento\InventorySales\Model\PlaceReservationsForSalesEvent $subject,
        callable $proceed,
        array $items,
        SalesChannelInterface $salesChannel,
        SalesEventInterface $salesEvent
    ) {
        if (empty($items)) {
            return $proceed($items, $salesChannel, $salesEvent);
        }

        $salesEventType = $salesEvent->getType();
        $salesEventObjectType = $salesEvent->getObjectType();
        if ((
                $salesEventType == SalesEventInterface::EVENT_ORDER_CANCELED ||
                $salesEventType == SalesEventInterface::EVENT_SHIPMENT_CREATED ||
                $salesEventType == SalesEventInterface::EVENT_ORDER_PLACED
            ) &&
            $salesEventObjectType == SalesEventInterface::OBJECT_TYPE_ORDER
        ) {
            $orderId = $salesEvent->getObjectId();
            if (!$orderId) {
                return $proceed($items, $salesChannel, $salesEvent);
            }

            $order = $this->orderRepository->get($orderId);
            $stockId = $this->stockManagement->getStockIdFromOrder($order);
            if (!$stockId) {
                return $proceed($items, $salesChannel, $salesEvent);
            }

            $skus = [];
            /** @var ItemToSellInterface $item */
            foreach ($items as $item) {
                $skus[] = $item->getSku();
            }
            $productTypes = $this->getProductTypesBySkus->execute($skus);
            $reservations = [];
            /** @var ItemToSellInterface $item */
            foreach ($items as $item) {
                $currentSku = $item->getSku();
                $skuNotExistInCatalog = !isset($productTypes[$currentSku]);
                if ($skuNotExistInCatalog ||
                    $this->isSourceItemManagementAllowedForProductType->execute($productTypes[$currentSku])) {
                    $reservations[] = $this->reservationBuilder
                        ->setSku($item->getSku())
                        ->setQuantity((float)$item->getQuantity())
                        ->setStockId($stockId)
                        ->setMetadata(
                            $this->serializer->serialize($this->salesEventToArrayConverter->execute($salesEvent))
                        )
                        ->build();
                }
            }
            return $this->appendReservations->execute($reservations);
        }
        return $proceed($items, $salesChannel, $salesEvent);
    }
}
