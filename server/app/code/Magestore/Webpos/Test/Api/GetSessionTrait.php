<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Test\Api;

use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Webapi\Rest\Request as RestRequest;
use Magento\Framework\Webapi\Exception;

use Magento\TestFramework\Helper\Bootstrap;
use Magento\Framework\App\ResourceConnection;

use Magento\Mtf\Fixture\FixtureInterface;
use Magento\Webpos\Api\Staff\LoginTest;

/**
 * Trait GetSessionTrait
 */
trait GetSessionTrait
{
    protected $testCaseId;
    protected $messageTxt = "Message";
    protected $statusCodeTxt = "StatusCode";
    protected $parametersTxt = "Parameters";

    /**
     * Login And Assign Pos
     *
     * @return mixed
     */
    public function loginAndAssignPos()
    {
        /**
         * @var LoginTest $staffLogin
         */
        $staffLogin = Bootstrap::getObjectManager()->get('\Magestore\Webpos\Test\Api\Staff\LoginTest');
        $loginPosResult = $staffLogin->getLoginResponse();
        $posAssign = Bootstrap::getObjectManager()->get('\Magestore\Webpos\Test\Api\Pos\AssignPosTest');
        $assignPosResult = $posAssign->getAssignPosResponse($loginPosResult['session_id']);
        $this->assertEquals("Enter to pos successfully", $assignPosResult['message']);
        $sessionId = $loginPosResult['session_id'];
        return $sessionId;
    }

    /**
     * Get response data from API test
     *
     * @param array $requestData
     * @param string $session_id
     * @return mixed
     */
    public function getResponseAPI($requestData = null, $session_id = false)
    {
        if (!$session_id) {
            $session_id = $this->posSession;
        }
        $query = ($requestData) ? '?'.http_build_query($requestData) : '';
        $serviceInfo = [
            'rest' => [
                'resourcePath' => self::RESOURCE_PATH . $query
                    .'&show_option=1'
                    .'&pos_session=' . $session_id,
                'httpMethod' => RestRequest::HTTP_METHOD_GET,
            ]
        ];

        if (!$query) {
            $serviceInfo = [
                'rest' => [
                    'resourcePath' => self::RESOURCE_PATH
                        .'?pos_session=' . $session_id,
                    'httpMethod' => RestRequest::HTTP_METHOD_GET,
                ]
            ];
        }

        if ($requestData) {
            $response = $this->_webApiCall($serviceInfo, $requestData);
        } else {
            $response = $this->_webApiCall($serviceInfo);
        }
        return $response;
    }

    /**
     * Get Response API without Session
     *
     * @param array $requestData
     * @return mixed
     */
    public function getResponseAPIwithoutSession($requestData)
    {
        $serviceInfo = [
            'rest' => [
                'resourcePath' => self::RESOURCE_PATH . '?'.http_build_query($requestData)
                    .'&show_option=1',
                'httpMethod' => RestRequest::HTTP_METHOD_GET,
            ]
        ];
        $response = $this->_webApiCall($serviceInfo, $requestData);
        return $response;
    }


    /**
     * Get Time Before Created
     *
     * @return mixed
     */
    public function getTimeBeforeCreated()
    {
        /** @var ResourceConnection $connection */
        $connection = Bootstrap::getObjectManager()->get(ResourceConnection::class);
        $lastTimeUPdated = $connection->getConnection()->fetchRow(
            $connection->getConnection()->select()
                ->from($connection->getTableName('catalog_product_entity'))
                ->where('sku = ?', 'SKU-1')
        )['updated_at'];
        return $lastTimeUPdated;
    }

    /**
     * Get Time After Created And Updated
     *
     * @return mixed
     */
    public function getTimeAfterCreatedAndUpdated()
    {
        /** @var ResourceConnection $connection */
        $connection = Bootstrap::getObjectManager()->get(ResourceConnection::class);
        $lastTimeUPdated = $connection->getConnection()->fetchRow(
            $connection->getConnection()->select()
                ->from($connection->getTableName('catalog_product_entity'))
                ->where('sku = ?', 'SKU-10')
        )['updated_at'];
        return $lastTimeUPdated;
    }

    /**
     * Get time after created all sample data to search
     * @return mixed
     */
    public function getTimeAfterCreated()
    {
        /** @var ResourceConnection $connection */
        $connection = Bootstrap::getObjectManager()->get(ResourceConnection::class);
        $lastTimeUPdated = $connection->getConnection()->fetchRow(
            $connection->getConnection()->select()
                ->from($connection->getTableName('catalog_product_entity'))
                ->where('sku = ?', 'SKU-15')
        )['updated_at'];
        return $lastTimeUPdated;
    }

    /**
     * Get time at first update sample data to search
     * @return mixed
     */
    public function getTimeAtFirstUpdated()
    {
        /** @var ResourceConnection $connection */
        $connection = Bootstrap::getObjectManager()->get(ResourceConnection::class);
        $lastTimeUPdated = $connection->getConnection()->fetchRow(
            $connection->getConnection()->select()
                ->from($connection->getTableName('catalog_product_entity'))
                ->where('sku = ?', 'SKU-8')
        )['updated_at'];
        return $lastTimeUPdated;
    }

    /**
     * Get Message
     *
     * @param int|string $testCaseId
     * @param string $message
     * @return string
     */
    public function getMessage($message)
    {
        return sprintf('API "%s" fail at testcase "%s" - "%s" is invalid', $this->apiName, $this->testCaseId, $message);
    }

    /**
     * Test Case - the pos_session is not valid
     */
    public function sessionCase1()
    {
        $requestData = [
            'searchCriteria' => [
                SearchCriteria::FILTER_GROUPS => [
                    [
                        'filters' => [
                            [
                                'field' => 'sku',
                                'value' => 'SKU-1',
                                'condition_type' => 'in',
                            ],
                        ],
                    ],
                ],
                SearchCriteria::PAGE_SIZE => 10,
                SearchCriteria::CURRENT_PAGE => 1
            ],
        ];
        $pos_session = 'hca98erfksahrfkj3hy4r89ashfkhsdf98';
        $expectedMessage = sprintf('Session with id "%s" does not exist.', $pos_session);
        try {
            $this->getResponseAPI($requestData, $pos_session);
            $this->fail('Expected throwing exception');
        } catch (\Exception $e) {
            $errorData = $this->processRestExceptionResult($e);

            /* check error message */
            $message = $this->getMessage($this->messageTxt);
            self::assertEquals($expectedMessage, $errorData['message'], $message);
            /* check error code */
            $message = $this->getMessage($this->statusCodeTxt);
            self::assertEquals(Exception::HTTP_UNAUTHORIZED, $e->getCode(), $message);
        }
    }

    /**
     * Test Case - the pos_session is missing
     */
    public function sessionCase2()
    {
        $requestData = [
            'searchCriteria' => [
                SearchCriteria::FILTER_GROUPS => [
                    [
                        'filters' => [
                            [
                                'field' => 'sku',
                                'value' => 'SKU-1',
                                'condition_type' => 'in',
                            ],
                        ],
                    ],
                ],
                SearchCriteria::PAGE_SIZE => 10,
                SearchCriteria::CURRENT_PAGE => 1
            ],
        ];
        $expectedMessage = 'Session with id "" does not exist.';
        try {
            $this->getResponseAPIwithoutSession($requestData);
            $this->fail('Expected throwing exception');
        } catch (\Exception $e) {
            $errorData = $this->processRestExceptionResult($e);
            /* check error message */
            $message = $this->getMessage($this->messageTxt);
            self::assertEquals($expectedMessage, $errorData['message'], $message);
            /* check error code */
            $message = $this->getMessage($this->statusCodeTxt);
            self::assertEquals(Exception::HTTP_UNAUTHORIZED, $e->getCode(), $message);
        }
    }

    /**
     * Test Case  - the searchCriteria is missing
     */
    public function sessionCase3()
    {
        $expectedMessage = '"%fieldName" is required. Enter and try again.';
        $parameters = 'searchCriteria';

        try {
            $this->getResponseAPI();
            $this->fail('Expected throwing exception');

        } catch (\Exception $e) {
            $errorData = $this->processRestExceptionResult($e);

            /* check error message */
            $message = $this->getMessage($this->messageTxt);
            self::assertEquals($expectedMessage, $errorData['message'], $message);
            /* check error code */
            $message = $this->getMessage($this->statusCodeTxt);
            self::assertEquals(Exception::HTTP_BAD_REQUEST, $e->getCode(), $message);
            /* check parameters */
            $message = $this->getMessage($this->parametersTxt);
            self::assertEquals($parameters, $errorData['parameters']['fieldName'], $message);
        }
    }
}
