<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Test\Api\Staff;

use Magento\Framework\Webapi\Rest\Request;
use Magento\TestFramework\TestCase\WebapiAbstract;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Webpos\Api\Staff\Constraint\Staff;

/**
 * Api Test LoginTest
 */
class LoginTest extends WebapiAbstract
{
    const RESOURCE_PATH = '/V1/webpos/staff/login';

    /**
     * @var $username
     */
    protected $username = 'admin';
    /**
     * @var $password
     */
    protected $password = 'admin123';

    /**
     * Get Login Response
     */
    public function getLoginResponse()
    {
        $serviceInfo = [
            'rest' => [
                'resourcePath' => self::RESOURCE_PATH,
                'httpMethod' => Request::HTTP_METHOD_POST,
            ],
        ];
        $requestData = [
            'staff' => [
                'username' => $this->username,
                'password' => $this->password
            ],
        ];
        return $this->_webApiCall($serviceInfo, $requestData);
    }

    /**
     * @var Staff
     */
    protected $staff;

    /**
     * @inheritDoc
     */
    protected function setUp() : void // phpcs:ignore
    {
        $this->staff = Bootstrap::getObjectManager()->get('\Magestore\Webpos\Test\Api\Staff\Expected\LoginResult');
    }

    /**
     * Test Staff Login
     *
     * @return void
     */
    public function testStaffLogin()
    {
        $result = $this->getLoginResponse();
        $this->assertNotNull($result);

        foreach ($this->staff->getSchema() as $key) {
            $this->assertArrayHasKey($key, $result);
        }
    }
}
