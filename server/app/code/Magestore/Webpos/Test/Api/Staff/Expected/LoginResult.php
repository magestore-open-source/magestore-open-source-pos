<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Test\Api\Staff\Expected;

use Magestore\Webpos\Test\Api\Staff\Expected\ConstraintAbstract as ConstraintAbstract;

/**
 * Expected - LoginResult
 */
class LoginResult extends ConstraintAbstract
{
    /**
     * @inheritDoc
     */
    public function getSchemaJson()
    {
        return '
            {
              "staff_id": 0,
              "timeout": 0,
              "session_id": "string",
              "message": "string",
              "staff_name": "string",
              "locations": [
                {
                  "location_id": 0,
                  "name": "string",
                  "location_code": "string",
                  "address": {
                    "street": "string",
                    "city": "string",
                    "region": {
                      "region_code": "string",
                      "region": "string",
                      "region_id": 0,
                      "extension_attributes": {}
                    },
                    "region_id": 0,
                    "country_id": "string",
                    "country": "string",
                    "postcode": "string"
                  },
                  "telephone": "string",
                  "store_id": 0,
                  "pos": [
                    {
                      "pos_id": 0,
                      "pos_name": "string",
                      "staff_id": 0,
                      "status": 0,
                      "staff_name": "string"
                    }
                  ]
                }
              ],
              "website_id": 0,
              "sharing_account": 0,
              "token": "string"
            }
        ';
    }
}
