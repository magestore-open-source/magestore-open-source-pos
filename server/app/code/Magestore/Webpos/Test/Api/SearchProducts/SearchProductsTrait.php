<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Test\Api\SearchProducts;

use Magento\Framework\Api\SearchCriteria;
use Magento\TestFramework\Assert\AssertArrayContains;
use Magento\Mtf\Fixture\FixtureInterface;
use Magestore\Webpos\Test\Constant\Product;

/**
 * Trait SearchProductsTrait
 */
trait SearchProductsTrait
{

    /**
     * @var $requestData
     */
    protected $requestData;

    /**
     * @var
     */
    protected $queryString;

    /**
     * Create Request Data
     *
     * @param null $queryString
     * @param null $pageSize
     * @param null $currentPage
     * @return array
     */
    public function createRequestData($queryString = null, $pageSize = null, $currentPage = null, $dir = null)
    {
        $this->queryString = $queryString;
        $requestData = [
            'searchCriteria' => [
                SearchCriteria::SORT_ORDERS => [
                    [
                        'field' => 'name',
                        'direction' => (!$dir || $dir=='ASC') ? 'ASC' : "DESC"
                    ],
                ],
                'queryString' => $queryString ? $queryString : '',
                SearchCriteria::PAGE_SIZE => $pageSize ? $pageSize : 100,
                SearchCriteria::CURRENT_PAGE => $currentPage ? $currentPage : 1
            ],
        ];
        return $this->requestData = $requestData;
    }

    /**
     * Expected Empty Data
     *
     * @param array $response
     */
    public function expectedEmptyData($response)
    {
        $expectedTotalCount = 0;

        $message = sprintf('Failed at Testcase  "%s" ', $this->testCaseId);

        $this->assertNotNull($response, $message);

        /* check search_criteria */
        unset($this->requestData['searchCriteria']['queryString']);
        AssertArrayContains::assert($this->requestData['searchCriteria'], $response['search_criteria']);

        /* check totalcount = 0 */
        self::assertEquals($expectedTotalCount, $response['total_count'], $message);

        /* check list_items is null or empty */
        self::assertEmpty($response['items'], $message);
    }

    /**
     * Expected Has One Item Data
     *
     * @param array $response
     * @param array|null $expectedItemsData
     * @param bool $gtCount
     */
    public function expectedHasOneItemData($response, $expectedItemsData = null, $gtCount = false)
    {
        $expectedTotalCount = 1;
        $message = sprintf('Failed at Testcase  "%s" ', $this->testCaseId);
        $this->assertNotNull($response, $message);

        /* check search_criteria */
        unset($this->requestData['searchCriteria']['queryString']);
        AssertArrayContains::assert($this->requestData['searchCriteria'], $response['search_criteria']);

        /* check totalcount != 0 */
        if (!$gtCount) {
            self::assertEquals($expectedTotalCount, $response['total_count'], $message);
        } else {
            self::assertGreaterThanOrEqual($expectedTotalCount, $response['total_count'], $message);
        }
        /* check list_items is null or empty */
        self::assertNotEmpty($response['items'], $message);

        /* chek items count is 2 or not */
        self::assertCount($expectedTotalCount, $response['items'], $message);

        if (!$expectedItemsData) {
            $expectedItemsData = [
                [
                    'sku' => Product::SKU_13,
                ],
            ];
        }

        /* check list_items is contains excepted data items */
        AssertArrayContains::assert($expectedItemsData, $response['items']);
    }

    /**
     * Expected Has Two Item Data
     *
     * @param array $response
     * @param array|null $expectedItemsData
     * @param bool $gtCount
     */
    public function expectedHasTwoItemData($response, $expectedItemsData = null, $gtCount = false)
    {
        $expectedTotalCount = 2;
        $message = sprintf('Failed at Testcase  "%s" ', $this->testCaseId);
        $this->assertNotNull($response, $message);

        /* check search_criteria */
        unset($this->requestData['searchCriteria']['queryString']);
        AssertArrayContains::assert($this->requestData['searchCriteria'], $response['search_criteria']);

        /* check totalcount  */
        if (!$gtCount) {
            self::assertEquals($expectedTotalCount, $response['total_count'], $message);
        } else {
            self::assertGreaterThanOrEqual($expectedTotalCount, $response['total_count'], $message);
        }
        /* check list_items is null or empty */
        self::assertNotEmpty($response['items'], $message);

        /* chek items count is 2 or not */
        self::assertCount($expectedTotalCount, $response['items'], $message);

        if (!$expectedItemsData) {
            $expectedItemsData = [
                [
                    'sku' => Product::SKU_13,
                ],
                [
                    'sku' => Product::SKU_14,
                ],
            ];
        }
        /* check list_items is contains excepted data items */
        AssertArrayContains::assert($expectedItemsData, $response['items']);
    }


    /**
     * Expected Has There Item Data
     *
     * @param array $response
     * @param array|null $expectedItemsData
     */
    public function expectedHasThereItemData($response, $expectedItemsData = null)
    {
        $expectedTotalCount = 3;
        $message = sprintf('Failed at Testcase  "%s" ', $this->testCaseId);
        $this->assertNotNull($response, $message);

        /* check search_criteria */
        unset($this->requestData['searchCriteria']['queryString']);
        AssertArrayContains::assert($this->requestData['searchCriteria'], $response['search_criteria']);

        /* check totalcount = 0 */
        self::assertEquals($expectedTotalCount, $response['total_count'], $message);

        /* check list_items is null or empty */
        self::assertNotEmpty($response['items'], $message);

        if (!$expectedItemsData) {
            $expectedItemsData = [
                [
                    'sku' => Product::SKU_13,
                ],
                [
                    'sku' => Product::SKU_14,
                ],
                [
                    'sku' => Product::SKU_15,
                ],
            ];
        }

        /* check list_items is contains excepted data items */
        AssertArrayContains::assert($expectedItemsData, $response['items']);
    }
}
