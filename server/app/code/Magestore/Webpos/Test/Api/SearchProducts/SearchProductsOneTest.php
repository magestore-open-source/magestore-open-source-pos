<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Webpos\Test\Api\SearchProducts;

use Magento\Framework\Api\SearchCriteria;
use Magento\InventoryApi\Api\Data\StockInterface;
use Magento\TestFramework\Assert\AssertArrayContains;
use Magento\TestFramework\TestCase\WebapiAbstract;
use Magento\Framework\Registry;
use Magento\Framework\Webapi\Exception;
use Magento\TestFramework\Helper\Bootstrap;

use Magestore\Webpos\Test\Constant\Product;
use Magestore\Webpos\Test\Api\GetSessionTrait;
use Magestore\Webpos\Test\Api\SearchProducts\SearchProductsTrait;

/**
 * Api Test SearchProductsOneTest
 */
class SearchProductsOneTest extends WebapiAbstract
{

    use GetSessionTrait;
    use SearchProductsTrait;

    /**#@+
     * Service constants
     */
    const RESOURCE_PATH = '/V1/webpos/products/search';
    const SERVICE_NAME = 'searchProductsRepositoryV1';

    protected $posSession;

    protected $apiName = "searchProducts";

    /**
     * Set Up
     *
     * @return void
     */
    protected function setUp() : void // phpcs:ignore
    {
        $this->posSession = $this->loginAndAssignPos();
    }

    /**
     * Initialize fixture namespaces.
     */
    public static function setUpBeforeClass() : void // phpcs:ignore
    {
        include __DIR__ . '/../../_files/delete_product.php';
        parent::setUpBeforeClass();
    }
    /**
     * Run garbage collector for cleaning memory
     *
     * @return void
     */
    public static function tearDownAfterClass() : void // phpcs:ignore
    {
        parent::tearDownAfterClass();
        include __DIR__ . '/../../_files/delete_product_rollback.php';
    }

    /**
     * Test Case 1 - No items return
     */
    public function testCase1()
    {
        // Disable test because elastic search still response data with this search key
        return true;
    }

    /**
     * Test Case 2 - No items return
     */
    public function testCase2()
    {
        // Disable test because elastic search still response data with this search key
        return true;
    }

    /**
     * Test Case 3 - No items return
     */
    public function testCase3()
    {

        $this->testCaseId = "3";
        $this->createRequestData(Product::OPERATOR_SKU);
        /* get Response from API test */
        $response = $this->getResponseAPI($this->requestData);

        /* expected Data is empty */
        $this->expectedEmptyData($response);
    }

    /**
     * Test Case 4 - No items return
     */
    public function testCase4()
    {

        $this->testCaseId = "4";
        $this->createRequestData(Product::OPERATOR_NAME);
        /* get Response from API test */
        $response = $this->getResponseAPI($this->requestData);

        /* expected Data is empty */
        $this->expectedEmptyData($response);
    }
}
