<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Webpos\Test\Api\DeletedProducts;

use Magento\Framework\Api\SearchCriteria;
use Magento\Mtf\Fixture\FixtureInterface;

use Magento\TestFramework\Helper\Bootstrap;
use Magento\Framework\App\ResourceConnection;

/**
 * Trait DeletedProductsTrait
 */
trait DeletedProductsTrait
{

    /**
     * @var $requestData
     */
    protected $requestData;

    /**
     * @var
     */
    protected $queryString;

    /**
     * @param null $queryString
     * @param null $pageSize
     * @param null $currentPage
     * @return array
     */
    public function createRequestData($time, $gt = false)
    {
        $requestData = [
            'searchCriteria' => [
                SearchCriteria::FILTER_GROUPS => [
                    [
                        'filters' => [
                            [
                                'field' => 'updated_at',
                                'value' => $time,
                                'condition_type' => !$gt ? 'gteq' : 'gt',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        return $this->requestData = $requestData;
    }

    /**
     * @return bool
     */
    public function deleteTable()
    {
        try {
            /** @var ResourceConnection $connection */
            $connection = Bootstrap::getObjectManager()->get(ResourceConnection::class);
            $connection->getConnection()->delete($connection->getTableName('webpos_product_deleted'));
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function updateTimeForNextTestCase()
    {
        /** @var ResourceConnection $connection */
        $connection = Bootstrap::getObjectManager()->get(ResourceConnection::class);
        $lastTimeUPdated = $connection->getConnection()->fetchRow(
            $connection->getConnection()->select()
                ->from($connection->getTableName('webpos_product_deleted'))
                ->order('deleted_at DESC')
                //->where('sku = ?', 'SKU-15')
                ->limit(1)
        )['deleted_at'];
        return $this->time = $lastTimeUPdated;
    }
}
