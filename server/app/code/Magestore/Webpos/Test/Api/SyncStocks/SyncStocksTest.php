<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Test\Api\SyncStocks;

use Magento\TestFramework\TestCase\WebapiAbstract;
use Magestore\Webpos\Test\Api\GetSessionTrait;

/**
 * Api test SyncStocksTest
 */
class SyncStocksTest extends WebapiAbstract
{

    use GetSessionTrait;

    /**#@+
     * Service constants
     */
    const RESOURCE_PATH = '/V1/webpos/stocks/sync';
    const SERVICE_NAME = 'stocksSyncRepositoryV1';

    protected $posSession;

    protected $apiName = "syncStocks";

    /**
     * Setup
     */
    protected function setUp() : void // phpcs:ignore
    {
        $this->posSession = $this->loginAndAssignPos();
    }

    /**#@-*/
    /**
     * Test Case SS1 - No items need to get from sample data
     */
    public function testCase6()
    {
        return 0;
    }

    /**
     * Test Case SS2 - has 3 items need to sync from sample data
     */
    public function testCase7()
    {
        return 3;
    }

    /**
     * Test Case SS3 - the pos_session is not valid
     */
    public function testCase8()
    {
        $this->testCaseId = "SS8";
        $this->sessionCase1();
    }

    /**
     * Test Case SS4 - the pos_session is missing
     */
    public function testCase9()
    {
        $this->testCaseId = "SS9";
        $this->sessionCase2();
    }

    /**
     * Test Case SS5 - the searchCriteria is missing
     */
    public function testCase10()
    {
        $this->testCaseId = "SS10";
        $this->sessionCase3();
    }
}
