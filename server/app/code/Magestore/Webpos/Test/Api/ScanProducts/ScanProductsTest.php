<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Test\Api\ScanProducts;

use Magento\TestFramework\TestCase\WebapiAbstract;
use Magestore\Webpos\Test\Constant\Product;
use Magestore\Webpos\Test\Api\GetSessionTrait;

/**
 * Api Test ScanProductsTest
 */
class ScanProductsTest extends WebapiAbstract
{

    use GetSessionTrait;
    use ScanProductsTrait;

    /**#@+
     * Service constants
     */
    const RESOURCE_PATH = '/V1/webpos/products/barcode';
    const SERVICE_NAME = 'scanProductsRepositoryV1';

    protected $posSession;

    protected $apiName = "scanProducts";

    /**
     * Set Up
     *
     * @return void
     */
    protected function setUp() : void // phpcs:ignore
    {
        $this->posSession = $this->loginAndAssignPos();
    }

    /**
     * Test Case 1 - No items return
     */
    public function testCase1()
    {

        $this->testCaseId = "SB1";
        $this->createRequestData(Product::SKU_1);
        /* get Response from API test */
        $response = $this->getResponseAPI($this->requestData);

        /* expected Data is empty */
        $this->expectedEmptyData($response);
    }

    /**
     * Test Case 2 - No items return
     */
    public function testCase2()
    {

        $this->testCaseId = "SB2";
        $this->createRequestData('SKU-m');
        /* get Response from API test */
        $response = $this->getResponseAPI($this->requestData);
        /* expected Data is empty */
        $this->expectedEmptyData($response);
    }

    /**
     * Test Case 3
     */
    public function testCase3()
    {
        return true;
    }

    /**
     * Test Case 4 - has no items return
     */
    public function testCase4()
    {

        $this->testCaseId = "SB4";
        $this->createRequestData('SKU-m');
        /* get Response from API test */
        $response = $this->getResponseAPI($this->requestData);

        /* expected Data is empty */
        $this->expectedEmptyData($response);
    }

    /**
     * Test Case 5 - the pos_session is not valid
     */
    public function testCase5()
    {
        $this->testCaseId = "SB5";
        $this->sessionCase1();
    }

    /**
     * Test Case 6 - the pos_session is missing
     */
    public function testCase6()
    {
        $this->testCaseId = "SB6";
        $this->sessionCase2();
    }

    /**
     * Test Case 7 - the searchCriteria is missing
     */
    public function testCase7()
    {
        $this->testCaseId = "SB7";
        $this->sessionCase3();
    }
}
