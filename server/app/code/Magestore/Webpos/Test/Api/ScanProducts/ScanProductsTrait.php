<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Test\Api\ScanProducts;

use Magento\Framework\Api\SearchCriteria;
use Magento\TestFramework\Assert\AssertArrayContains;
use Magento\Mtf\Fixture\FixtureInterface;
use Magestore\Webpos\Test\Constant\Product;

/**
 * Trait SearchProductsTrait
 */
trait ScanProductsTrait
{

    /**
     * @var $requestData
     */
    protected $requestData;

    /**
     * @var
     */
    protected $queryString;

    /**
     * Create Request Data
     *
     * @param string|null $queryString
     * @param int|null $pageSize
     * @param int|null $currentPage
     * @return array
     */
    public function createRequestData($queryString = null, $pageSize = null, $currentPage = null)
    {
        $this->queryString = $queryString;
        $requestData = [
            'searchCriteria' => [
                'queryString' => $queryString ? $queryString : '',
                SearchCriteria::PAGE_SIZE => $pageSize ? $pageSize : 100,
                SearchCriteria::CURRENT_PAGE => $currentPage ? $currentPage : 1
            ],
        ];
        return $this->requestData = $requestData;
    }

    /**
     * Expected Empty Data
     *
     * @param array $response
     */
    public function expectedEmptyData($response)
    {
        $expectedTotalCount = 0;

        $message = sprintf('Failed at Testcase  "%s" ', $this->testCaseId);

        $this->assertNotNull($response, $message);

        /* check search_criteria */
        unset($this->requestData['searchCriteria']['queryString']);
        AssertArrayContains::assert($this->requestData['searchCriteria'], $response['search_criteria']);

        /* check totalcount = 0 */
        self::assertEquals($expectedTotalCount, $response['total_count'], $message);

        /* check list_items is null or empty */
        self::assertEmpty($response['items'], $message);
    }

    /**
     * Expected Has One Item Data
     *
     * @param array $response
     */
    public function expectedHasOneItemData($response, $expectedItemsData = null, $gtCount = false)
    {
        $expectedTotalCount = 1;
        $message = sprintf('Failed at Testcase  "%s" ', $this->testCaseId);
        $this->assertNotNull($response, $message);

        /* check search_criteria */
        unset($this->requestData['searchCriteria']['queryString']);
        AssertArrayContains::assert($this->requestData['searchCriteria'], $response['search_criteria']);

        /* check totalcount != 0 */
        if (!$gtCount) {
            self::assertEquals($expectedTotalCount, $response['total_count'], $message);
        } else {
            self::assertGreaterThanOrEqual($expectedTotalCount, $response['total_count'], $message);
        }
        /* check list_items is null or empty */
        self::assertNotEmpty($response['items'], $message);

        /* check items count is 2 or not */
        self::assertCount($expectedTotalCount, $response['items'], $message);

        if (!$expectedItemsData) {
            $expectedItemsData = [
                [
                    'sku' => Product::SKU_13,
                ],
            ];
        }

        /* check list_items is contains excepted data items */
        AssertArrayContains::assert($expectedItemsData, $response['items']);
    }
}
