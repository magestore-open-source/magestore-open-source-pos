<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Test\Api\Pos;

use Magento\TestFramework\TestCase\WebapiAbstract;
use Magento\Framework\Webapi\Rest\Request as RestRequest;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Webpos\Api\Staff\StaffTest;

/**
 * Test - AssignPosTest
 */
class AssignPosTest extends WebapiAbstract
{
    const RESOURCE_PATH = '/V1/webpos/posassign';

    /**
     * @var $pos_id
     */
    protected $pos_id = 1;
    /**
     * @var $location_id
     */
    protected $location_id = 1;

    /**
     * Get Assign Pos Response
     *
     * @param string $session_id
     * @return array|bool|float|int|string
     */
    public function getAssignPosResponse($session_id)
    {
        $requestData = [
            'pos' => [
                'pos_id' => $this->pos_id,
                'location_id' => $this->location_id
            ],
        ];

        $serviceInfo = [
            'rest' => [
                'resourcePath' => self::RESOURCE_PATH
                    .'?pos_session=' . $session_id,
                'httpMethod' => RestRequest::HTTP_METHOD_POST,
            ]
        ];
        return $this->_webApiCall($serviceInfo, $requestData);
    }

    /**
     * Get Configuration
     */
    public function testPosAssign()
    {
        /**
         * @var StaffTest $staffLogin
         */
        $staffLogin = Bootstrap::getObjectManager()->get('\Magestore\Webpos\Test\Api\Staff\LoginTest');
        $loginPosResult = $staffLogin->getLoginResponse();
        $assignPosResult = $this->getAssignPosResponse($loginPosResult['session_id']);

        self::assertContains(
            'message',
            array_keys($assignPosResult),
            "message key is not in found in result's keys. Response: 
            ". json_encode($assignPosResult, JSON_PRETTY_PRINT)
        );

        $this->assertEquals("Enter to pos successfully", $assignPosResult['message']);
    }
}
