<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
/**
 * unlink stock-source
 */

use Magento\InventoryApi\Api\Data\StockSourceLinkInterface;
use Magento\InventoryApi\Api\Data\StockSourceLinkInterfaceFactory;
use Magento\InventoryApi\Api\StockSourceLinksDeleteInterface;
use Magento\TestFramework\Helper\Bootstrap;

/** @var StockSourceLinkInterfaceFactory $stockSourceLinkFactory */
$stockSourceLinkFactory = Bootstrap::getObjectManager()->get(StockSourceLinkInterfaceFactory::class);
/** @var StockSourceLinksDeleteInterface $stockSourceLinksDelete */
$stockSourceLinksDelete = Bootstrap::getObjectManager()->get(StockSourceLinksDeleteInterface::class);


use Magestore\Webpos\Test\Constant\Stock;
use Magestore\Webpos\Test\Constant\Source;

$linksData = [
    Stock::STOCK_ID => [Source::SOURCE_CODE]
];

$links = [];

foreach ($linksData as $stockID => $sourceCodes) {
    foreach ($sourceCodes as $sourceCode) {
        /** @var StockSourceLinkInterface $link */
        $link = $stockSourceLinkFactory->create();

        $link->setStockId($stockID);
        $link->setSourceCode($sourceCode);

        $links[] = $link;
    }
}

$stockSourceLinksDelete->execute($links);
