<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
/**
 * delete stock
 */

use Magento\InventoryApi\Api\StockRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Framework\Exception\NoSuchEntityException;

use Magento\Indexer\Model\Indexer;
use Magento\Indexer\Model\Indexer\Collection;
use Magestore\Webpos\Test\Constant\Stock;

/** @var StockRepositoryInterface $stockRepository */
$stockRepository = Bootstrap::getObjectManager()->get(StockRepositoryInterface::class);
try {
    $stockRepository->deleteById(Stock::STOCK_ID);

    /* reindex */
    $indexerCollection = Bootstrap::getObjectManager()->create(Collection::class);
    $ids = $indexerCollection->getAllIds();
    foreach ($ids as $id) {
        $indexerFactory = Bootstrap::getObjectManager()->create(Indexer::class);
        $idx = $indexerFactory->load($id);
        $idx->reindexAll($id); // this reindexes all
    }

} catch (NoSuchEntityException $e) {
    //Stock already removed
}
