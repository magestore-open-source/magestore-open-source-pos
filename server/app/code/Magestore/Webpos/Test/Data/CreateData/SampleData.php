<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Test\Data\CreateData;

use Magento\TestFramework\TestCase\WebapiAbstract;

/**
 * Api Test SampleData
 */
class SampleData extends WebapiAbstract
{
    /**
     * Initialize fixture namespaces.
     */
    public static function setUpBeforeClass() : void // phpcs:ignore
    {
        /* create sample Data2 */
        include __DIR__. '/../../_files/product.php';
        include __DIR__. '/../../_files/source.php';
        include __DIR__. '/../../_files/source_item.php';
        include __DIR__. '/../../_files/stock.php';
        include __DIR__. '/../../_files/stock_source_link.php';
        parent::setUpBeforeClass();
    }

    /**
     * Test Case
     */
    public function testCase()
    {
        $expected = true;
        self::assertEquals($expected, $expected);
    }
}
