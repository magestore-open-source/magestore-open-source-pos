<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Webpos\Console\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magestore\Webpos\Api\Console\WebposDeployInterface;

/**
 * Console - Webpos Command
 */
class WebposCommand extends Command
{

    /**
     * @var WebposDeployInterface
     */
    protected $webposDeployInterface;

    /**
     * WebposCommand constructor.
     *
     * @param WebposDeployInterface $webposDeployInterface
     */
    public function __construct(
        WebposDeployInterface $webposDeployInterface
    ) {
        parent::__construct();
        $this->webposDeployInterface = $webposDeployInterface;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('webpos:deploy');
        $this->setDescription('Deploy the pos app');
    }

    /**
     * Execute
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $deployCommand =  '';
            $this->webposDeployInterface->webposDeploy($deployCommand, $output);
        } catch (Exception $e) {
            $output->writeln(sprintf('<error>Error: %s</error>', $e->getMessage()));
        }
    }
}
