/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
define(
    [
        'jquery'
    ],
    function ($) {
        "use strict";

        return {
            dispatch: function (eventName, data, timeout) {
                $("body").eventName = '';
                if (timeout) {
                    setTimeout(function () {
                        $("body").trigger(eventName, data);
                    }, 100);
                } else $("body").trigger(eventName, data);
                return true;
            },
            observer: function (eventName, function_callback) {
                $("body").on(eventName, function_callback);
                return true;
            }
        };
    }
);