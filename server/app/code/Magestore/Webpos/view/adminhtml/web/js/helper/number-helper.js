/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

define(
    [],
    function (ko, priceUtils) {
        'use strict';

        var NumberHelper = {
            /**
             * Count the number of decimal
             *
             * @param {number} number
             * @return {number}
             */
            countDecimal: function (number) {
                if ((parseFloat(number) % 1) !== 0) {
                    if (number.toString().indexOf('e') !== -1) {
                        return Math.abs(number.toString().split('e')[1]);
                    }
                    return number.toString().split(".")[1].length;
                }
                return 0;
            },

            /**
             * sum an array number
             *
             * @param {Array.<number>} numbers
             * @return {*|any}
             */
            addNumber: function (a, b) {
                    if (!a) {
                        a = 0;
                    }
                    if (!b) {
                        b = 0;
                    }
                    let maxCountNumberDecimal = Math.max(this.countDecimal(a), this.countDecimal(b));
                    return parseFloat((+a + +b).toFixed(maxCountNumberDecimal));
            },

            /**
             * Minus 2 numbers
             * @param {number} a
             * @param {number} b
             * @return {number}
             */
            minusNumber: function (a, b) {
                if (!a) {
                    a = 0;
                }
                if (!b) {
                    b = 0;
                }
                let maxCountNumberDecimal = Math.max(this.countDecimal(a), this.countDecimal(b));
                return parseFloat((+a - +b).toFixed(maxCountNumberDecimal));
            },

            /**
             * Multiple 2 numbers
             * @param {Array.<number>} numbers
             * @return {number}
             */
            multipleNumber: function (...numbers) {
                return numbers.reduce(function (a, b) {
                    if (!a) {
                        a = 0;
                    }
                    if (!b) {
                        b = 0;
                    }
                    let maxCountNumberDecimal = this.countDecimal(a) + this.countDecimal(b);
                    return parseFloat((+a * +b).toFixed(maxCountNumberDecimal));
                }.bind(this));
            }
        };
        return NumberHelper;
    }
);
