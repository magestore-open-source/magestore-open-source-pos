/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

define(
    [
        'jquery',
        'Magestore_Webpos/js/model/storage'
    ],
    function ($, storage) {
        "use strict";
        var Request = {
            initialize: function () {
                return this;
            },
            send: function (url, method, params) {
                return new Promise((resolve, reject) => {
                    switch (method) {
                        case 'post':
                            storage.post(
                                url, JSON.stringify(params)
                            ).done(
                                function (response) {
                                    resolve(response);
                                }
                            ).fail(
                                function (response) {
                                    reject(response);
                                }
                            );
                            break;
                        case 'get':
                            storage.get(
                                url, JSON.stringify(params)
                            ).done(
                                function (response) {
                                    resolve(response);
                                }
                            ).fail(
                                function (response) {
                                    reject(response);
                                }
                            );
                            break;
                        case 'delete':
                            url = this.addParamsToUrl(url, params);
                            storage.delete(
                                url, JSON.stringify(params)
                            ).done(
                                function (response) {
                                    resolve(response);
                                }
                            ).fail(
                                function (response) {
                                    reject(response);
                                }
                            );
                            break;
                        default:
                            break;
                        }
                });
            },
            addParamsToUrl: function(url, params){
                $.each(params, function(key, value){
                    if(key){
                        if (url.indexOf("?") != -1) {
                            url = url + '&'+key+'=' + value;
                        }
                        else {
                            url = url + '?'+key+'=' + value;
                        }
                    }
                });
                return url;
            }
        };
        return Request.initialize();
    }
);
