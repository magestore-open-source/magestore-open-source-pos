<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Model\Staff;

use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magestore\Appadmin\Api\Data\Staff\StaffInterface;
use Magestore\Appadmin\Api\Data\Staff\StaffInterfaceFactory;
use Magestore\Appadmin\Api\Data\Staff\StaffSearchResultsInterface;
use Magestore\Appadmin\Api\Data\Staff\StaffSearchResultsInterfaceFactory;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;
use Magestore\Appadmin\Model\ResourceModel\Staff\Staff\Collection;
use Magestore\Appadmin\Model\ResourceModel\Staff\Staff\CollectionFactory;
use Magestore\Appadmin\Model\ResourceModel\Staff\Staff as StaffResourceModel;

/**
 * Class StaffManagement
 */
class StaffRepository implements StaffRepositoryInterface
{
    /**
     * @var StaffFactory
     */
    protected $staffFactory;
    /**
     * @var StaffResourceModel
     */
    protected $staffResource;
    /**
     * @var StaffSearchResultsInterface
     */
    protected $staffSearchResults;
    /**
     * @var CollectionFactory
     */
    protected $staffCollectionFactory;

    /**
     * @param StaffInterfaceFactory $staffFactory
     * @param StaffResourceModel $staffResource
     * @param CollectionFactory $staffCollectionFactory
     * @param StaffSearchResultsInterfaceFactory $staffSearchResultsInterfaceFactory
     */
    public function __construct(
        StaffInterfaceFactory $staffFactory,
        StaffResourceModel $staffResource,
        CollectionFactory $staffCollectionFactory,
        StaffSearchResultsInterfaceFactory $staffSearchResultsInterfaceFactory
    ) {
        $this->staffFactory = $staffFactory;
        $this->staffResource = $staffResource;
        $this->staffSearchResults = $staffSearchResultsInterfaceFactory;
        $this->staffCollectionFactory = $staffCollectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function save(StaffInterface $staff)
    {
        try {
            /* @var Staff $staff */
            /* @var Staff $staffModel */
            $staffModel = $this->staffResource->save($staff);
            return $staffModel;
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Could not save staff.'), $e);
        }
    }

    /**
     * @inheritDoc
     */
    public function getById($staffId)
    {
        $staff = $this->staffFactory->create();
        $this->staffResource->load($staff, $staffId);
        if (!$staff->getId()) {
            throw new NoSuchEntityException(__('Staff with id "%1" does not exist.', $staffId));
        } else {
            return $staff;
        }
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->staffCollectionFactory->create();
        //Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders === null) {
            $sortOrders = [];
        }
        /** @var SortOrder $sortOrder */
        foreach ($sortOrders as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == SortOrder::SORT_ASC)
                    ? SortOrder::SORT_ASC : SortOrder::SORT_DESC
            );
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->load();
        $searchResults = $this->staffSearchResults->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(StaffInterface $staff)
    {
        return $this->deleteById($staff->getId());
    }

    /**
     * @inheritDoc
     */
    public function deleteById($staffId)
    {
        /* @var Staff $staff */
        $staff = $this->getById($staffId);
        if ($staff->getId()) {
            $this->staffResource->delete($staff);
            return true;
        } else {
            throw new NoSuchEntityException(__('Staff with id "%1" does not exist.', $staffId));
        }
    }

    /**
     * @inheritDoc
     */
    public function getAllStaff()
    {
        /** @var Collection $collection */
        $collection = $this->staffCollectionFactory->create();
        return $collection;
    }
}
