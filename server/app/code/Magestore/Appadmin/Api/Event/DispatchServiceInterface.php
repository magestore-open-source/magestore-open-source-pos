<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Api\Event;

/**
 * Interface DispatchServiceInterface
 */
interface DispatchServiceInterface
{
    const EVENT_NAME_FORCE_SIGN_OUT = 'magestore_webpos_force_sign_out';
    const EVENT_NAME_FORCE_CHANGE_POS = 'magestore_webpos_force_change_pos';

    const EXCEPTION_CODE_FORCE_SIGN_OUT = 900;
    const EXCEPTION_CODE_FORCE_CHANGE_POS = 901;
    const EXCEPTION_CODE_CANNOT_LOGIN = 903;
    const EXCEPTION_CODE_SAVED_REQUEST_TO_SERVER = 904;

    const EXCEPTION_MESSAGE = 'Opps! Access denied. Recent action has not been saved yet.';

    /**
     * Dispatch event to force sign out
     *
     * @param int $staffId
     * @param int $posId
     * @return bool
     */
    public function dispatchEventForceSignOut($staffId, $posId = null);

    /**
     * Dispatch event to force change pos
     *
     * @param int $staffId
     * @param int $posId
     * @return bool
     */
    public function dispatchEventForceChangePos($staffId, $posId);
}
