<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types = 1);

namespace Magestore\Appadmin\Api\DataProvider;

interface GetStaffByIdInterface
{
    const STAFF_DATA = 'staff_data';

    /**
     * Get staff by id
     *
     * @param int $id
     * @return \Magestore\Appadmin\Api\Data\Staff\StaffInterface|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(int $id);
}
