<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Api\Staff;

interface StaffRepositoryInterface
{
    /**
     * Save staff.
     *
     * @param \Magestore\Appadmin\Api\Data\Staff\StaffInterface $staff
     * @return \Magestore\Appadmin\Api\Data\Staff\StaffInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magestore\Appadmin\Api\Data\Staff\StaffInterface $staff);

    /**
     * Retrieve staff.
     *
     * @param int $staffId
     * @return \Magestore\Appadmin\Api\Data\Staff\StaffInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($staffId);

    /**
     * Retrieve staff matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Appadmin\Api\Data\Staff\StaffSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete staff.
     *
     * @param \Magestore\Appadmin\Api\Data\Staff\StaffInterface $staff
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magestore\Appadmin\Api\Data\Staff\StaffInterface $staff);

    /**
     * Delete staff by ID.
     *
     * @param int $staffId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($staffId);

    /**
     * Get location collection.
     *
     * @return \Magestore\Appadmin\Api\Data\Staff\StaffInterface[]
     */
    public function getAllStaff();
}
