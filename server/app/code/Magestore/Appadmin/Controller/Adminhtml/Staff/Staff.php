<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Appadmin\Controller\Adminhtml\Staff;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magestore\Appadmin\Api\Data\Staff\StaffInterfaceFactory;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;

/**
 * Abstract Staff action class
 */
abstract class Staff extends Action
{
    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;
    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var StaffRepositoryInterface
     */
    protected $staffRepository;
    /**
     * @var StaffInterfaceFactory
     */
    protected $staffInterfaceFactory;
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param ForwardFactory $resultForwardFactory
     * @param Registry $registry
     * @param StaffRepositoryInterface $staffRepository
     * @param StaffInterfaceFactory $staffInterfaceFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LayoutFactory $resultLayoutFactory,
        ForwardFactory $resultForwardFactory,
        Registry $registry,
        StaffRepositoryInterface $staffRepository,
        StaffInterfaceFactory $staffInterfaceFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->staffRepository = $staffRepository;
        $this->staffInterfaceFactory = $staffInterfaceFactory;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magestore_Appadmin::manageStaffs');
    }
}
