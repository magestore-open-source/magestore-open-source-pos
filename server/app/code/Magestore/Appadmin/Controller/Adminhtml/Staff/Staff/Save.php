<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Controller\Adminhtml\Staff\Staff;

use Exception;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magestore\Appadmin\Controller\Adminhtml\Staff\Staff;

/**
 * Controller Save staff
 */
class Save extends Staff implements HttpPostActionInterface
{
    /**
     * Execute
     *
     * @return $this|ResponseInterface|ResultInterface
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $modelId = (int)$this->getRequest()->getParam('staff_id');
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            return $resultRedirect->setPath('*/*/');
        }
        $email = $data['email'];
        if ($email) {
            $findEmailExist = $this->staffInterfaceFactory->create()->load($email, 'email');
            if ($findEmailExist->getId() && $findEmailExist->getId() != $modelId) {
                $this->messageManager->addErrorMessage(__('Email %1 already exists.', $email));
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('staff_id')]);
            }
        }
        $username = $data['username'];
        if ($username) {
            $findUsernameExist = $this->staffInterfaceFactory->create()->load($username, 'username');
            if ($findUsernameExist->getId() && $findUsernameExist->getId() != $modelId) {
                $this->messageManager->addErrorMessage(__('Username already exists.', $username));
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('staff_id')]);
            }
        }

        if ($modelId) {
            $model = $this->staffRepository->getById($modelId);
        } else {
            $model = $this->staffInterfaceFactory->create();
        }

        if (isset($data['password']) && $data['password'] == '') {
            unset($data['password']);
        } else {
            $data['new_password'] = $data['password'];
        }

        if ($data['location_ids']) {
            $data['location_ids'] = implode(',', $data['location_ids']);
        } else {
            $data['location_ids'] = '';
        }

        $model->setData($data);
        if ($model->hasNewPassword() && $model->getNewPassword() === '') {
            $model->unsNewPassword();
        }
        if ($model->hasPasswordConfirmation() && $model->getPasswordConfirmation() === '') {
            $model->unsPasswordConfirmation();
        }
        $result = $model->validate(); /* validate data */
        if (is_array($result)) {
            foreach ($result as $message) {
                $this->messageManager->addErrorMessage($message);
            }
            $this->_redirect('*/*/edit', ['_current' => true]);
            return $resultRedirect->setPath('*/*/');
        }
        try {
            $this->staffRepository->save($model);
            $this->messageManager->addSuccessMessage(__('Staff was successfully saved'));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('staff_id')]);
        }
        if ($this->getRequest()->getParam('back') == 'edit') {
            return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
