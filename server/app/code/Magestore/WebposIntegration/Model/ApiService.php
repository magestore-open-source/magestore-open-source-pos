<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\WebposIntegration\Model;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Api\IntegrationServiceInterface;
use Magento\Integration\Api\OauthServiceInterface;
use Magento\Integration\Model\ConfigBasedIntegrationManager;
use Magento\Integration\Model\Integration;
use Magestore\WebposIntegration\Api\ApiServiceInterface;

/**
 * Integration Api Service
 */
class ApiService implements ApiServiceInterface
{

    /**
     * @var IntegrationServiceInterface
     */
    protected $integrationService;

    /**
     * @var OauthServiceInterface
     */
    protected $oauthService;

    /**
     * @var ConfigBasedIntegrationManager
     */
    protected $integrationManager;

    /**
     * ApiService constructor.
     * @param IntegrationServiceInterface $integrationService
     * @param OauthServiceInterface $oauthService
     * @param ConfigBasedIntegrationManager $integrationManager
     */
    public function __construct(
        IntegrationServiceInterface $integrationService,
        OauthServiceInterface $oauthService,
        ConfigBasedIntegrationManager $integrationManager
    ) {
        $this->integrationService = $integrationService;
        $this->oauthService = $oauthService;
        $this->integrationManager = $integrationManager;
    }

    /**
     * @inheritDoc
     */
    public function getToken()
    {
        try {
            $integration = $this->getIntegration();
        } catch (Exception $e) {
            $this->setupIntegration();
            $integration = $this->getIntegration();
        }
        $token = $this->getIntegrationAccessToken($integration);
        if (!$token) {
            $integration = $this->createAccessToken($integration);
            $token = $this->getIntegrationAccessToken($integration);
        }
        return $token;
    }

    /**
     * @inheritDoc
     */
    public function getIntegrationAccessToken(Integration $integration)
    {
        $token = '';
        if (($integration->getStatus() == Integration::STATUS_ACTIVE) && $integration->getConsumerId()) {
            $accessToken = $this->oauthService->getAccessToken($integration->getConsumerId());
            if ($accessToken && $accessToken->getToken()) {
                $token = $accessToken->getToken();
            }
        }
        return $token;
    }

    /**
     * @inheritDoc
     */
    public function createAccessToken(Integration $integration)
    {
        if ($this->oauthService->createAccessToken($integration->getConsumerId(), true)) {
            $integration->setStatus(Integration::STATUS_ACTIVE)->save();
        }
        return $integration;
    }

    /**
     * @inheritDoc
     */
    public function setupIntegration()
    {
        $this->integrationManager->processIntegrationConfig([ApiServiceInterface::API_INTEGRATION_NAME]);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getIntegration()
    {
        $integration = $this->integrationService->findByName(self::API_INTEGRATION_NAME);
        if ($integration && $integration->getIntegrationId()) {
            return $integration;
        }
        throw new LocalizedException(__('POS Integration has not been setup correctly yet'));
    }
}
