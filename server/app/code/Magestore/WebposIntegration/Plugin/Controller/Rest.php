<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\WebposIntegration\Plugin\Controller;

use Exception;
use Magento\Framework\App\Area;
use Magento\Framework\App\AreaList;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\State;
use Magento\Framework\Webapi\ErrorProcessor;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\Webapi\Rest\Response;
use Magento\Webapi\Controller\PathProcessor;
use Magestore\WebposIntegration\Controller\Rest\RequestProcessor;

/**
 * Plugin for Rest Controller
 */
class Rest
{
    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var RequestProcessor
     */
    protected $requestProcessor;

    /**
     * @var Request
     */
    protected $restRequest;

    /**
     * @var PathProcessor
     */
    protected $pathProcessor;
    
    /**
     * @var Response
     */
    protected $response;
    
    /**
     * @var ErrorProcessor
     */
    protected $errorProcessor;

    /**
     * @var AreaList
     */
    protected $areaList;

    /**
     * @var State
     */
    protected $appState;

    /**
     * Rest constructor.
     * @param ProductMetadataInterface $productMetadata
     * @param RequestProcessor $requestProcessor
     * @param Request $restRequest
     * @param PathProcessor $pathProcessor
     * @param Response $response
     * @param ErrorProcessor $errorProcessor
     * @param AreaList $areaList
     * @param State $appState
     */
    public function __construct(
        ProductMetadataInterface $productMetadata,
        RequestProcessor $requestProcessor,
        Request  $restRequest,
        PathProcessor $pathProcessor,
        Response $response,
        ErrorProcessor $errorProcessor,
        AreaList $areaList,
        State $appState
    ) {
        $this->productMetadata = $productMetadata;
        $this->requestProcessor = $requestProcessor;
        $this->restRequest = $restRequest;
        $this->pathProcessor = $pathProcessor;
        $this->response = $response;
        $this->errorProcessor = $errorProcessor;
        $this->areaList = $areaList;
        $this->appState = $appState;
    }

    /**
     * Around Dispatch
     *
     * @param \Magento\Webapi\Controller\Rest $subject
     * @param callable $proceed
     * @param RequestInterface $request
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundDispatch(
        \Magento\Webapi\Controller\Rest $subject,
        callable $proceed,
        RequestInterface $request
    ) {
        if (version_compare($this->productMetadata->getVersion(), '2.2.6', '<')) {
            $path = $this->pathProcessor->process($request->getPathInfo());
            $this->restRequest->setPathInfo($path);
            if ($this->requestProcessor->canProcess($this->restRequest)) {
                $this->areaList->getArea($this->appState->getAreaCode())
                    ->load(Area::PART_TRANSLATE);
                try {
                    $this->response = $this->requestProcessor->process($this->restRequest);
                } catch (Exception $e) {
                    $maskedException = $this->errorProcessor->maskException($e);
                    $this->response->setException($maskedException);
                }
                return $this->response;
            }
        }
        return $proceed($request);
    }
}
