<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\WebposIntegration\Api;

/**
 * @api
 */
interface ApiServiceInterface
{
    const API_INTEGRATION_NAME = "Magestore POS";

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken();

    /**
     * Get Integration Access Token
     *
     * @param \Magento\Integration\Model\Integration $integration
     * @return string
     */
    public function getIntegrationAccessToken(\Magento\Integration\Model\Integration $integration);

    /**
     * Create Access Token
     *
     * @param \Magento\Integration\Model\Integration  $integration
     * @return $this|ApiServiceInterface
     * @throws \Exception
     */
    public function createAccessToken(\Magento\Integration\Model\Integration $integration);

    /**
     * Setup Integration
     *
     * @return $this
     */
    public function setupIntegration();

    /**
     * Get Integration
     *
     * @return \Magento\Integration\Model\Integration
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getIntegration();
}
