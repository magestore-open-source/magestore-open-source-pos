<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\WebposIntegration\Controller\Rest;

use Exception;
use Magento\Customer\Model\Session;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Config\ConfigOptionsListConstants;
use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\Webapi\Rest\Response;
use Magento\Framework\Webapi\ServiceOutputProcessor;
use Magento\Webapi\Controller\Rest\InputParamsResolver;
use Magestore\Webpos\Api\Staff\SessionRepositoryInterface;
use Magestore\Webpos\Helper\Profiler;
use Magento\Framework\Webapi\Rest\Response\FieldsFilter;
use Magestore\Webpos\Api\Staff\StaffManagementInterface;
use Magestore\Webpos\Api\Pos\PosRepositoryInterface;
use Magestore\Webpos\Api\Location\LocationRepositoryInterface;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;
use Magestore\Webpos\Api\DataProvider\Session\GetSessionByIdInterface;
use Magestore\Webpos\Api\DataProvider\GetCurrentStaffIdBySessionInterface;
use Magestore\Webpos\Model\Service\Request\SaveActionLog;

/**
 * Class RequestProcessor
 *
 * Request processor for rest api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RequestProcessor
{
    /**
     * Session param
     */
    const SESSION_PARAM_KEY = 'pos_session';

    /**
     * Pos Resource
     */
    const MANAGE_POS_RESOURCE = 'Magestore_Webpos::manage_pos';

    /**
     * @var InputParamsResolver
     */
    protected $inputParamsResolver;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var ServiceOutputProcessor
     */
    protected $serviceOutputProcessor;

    /**
     * @var DeploymentConfig
     */
    protected $deploymentConfig;

    /**
     * @var FieldsFilter
     */
    protected $fieldsFilter;

    /**
     * @var ObjectManagerInterface|ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var StaffManagementInterface
     */
    protected $staffManagement;

    /**
     * @var SessionRepositoryInterface
     */
    protected $sessionRepository;

    /**
     * @var LocationRepositoryInterface
     */
    protected $locationRepository;

    /**
     * @var SaveActionLog
     */
    protected $saveActionLog;

    /**
     * @var GetSessionByIdInterface
     */
    protected $getSessionById;

    /**
     * @var GetCurrentStaffIdBySessionInterface
     */
    protected $getCurrentStaffIdBySession;

    /**
     * RequestProcessor constructor.
     *
     * @param Response $response
     * @param InputParamsResolver $inputParamsResolver
     * @param ServiceOutputProcessor $serviceOutputProcessor
     * @param FieldsFilter $fieldsFilter
     * @param DeploymentConfig $deploymentConfig
     * @param ObjectManagerInterface $objectManager
     * @param StaffManagementInterface $staffManagement
     * @param SessionRepositoryInterface $sessionRepository
     * @param LocationRepositoryInterface $locationRepository
     * @param SaveActionLog $saveActionLog
     * @param GetSessionByIdInterface $getSessionById
     * @param GetCurrentStaffIdBySessionInterface $getCurrentStaffIdBySession
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Response $response,
        InputParamsResolver $inputParamsResolver,
        ServiceOutputProcessor $serviceOutputProcessor,
        FieldsFilter $fieldsFilter,
        DeploymentConfig $deploymentConfig,
        ObjectManagerInterface $objectManager,
        StaffManagementInterface $staffManagement,
        SessionRepositoryInterface $sessionRepository,
        LocationRepositoryInterface $locationRepository,
        SaveActionLog $saveActionLog,
        GetSessionByIdInterface $getSessionById,
        GetCurrentStaffIdBySessionInterface $getCurrentStaffIdBySession
    ) {
        $this->response = $response;
        $this->inputParamsResolver = $inputParamsResolver;
        $this->serviceOutputProcessor = $serviceOutputProcessor;
        $this->deploymentConfig = $deploymentConfig;
        $this->fieldsFilter = $fieldsFilter;
        $this->objectManager = $objectManager;
        $this->staffManagement = $staffManagement;
        $this->sessionRepository = $sessionRepository;
        $this->locationRepository = $locationRepository;
        $this->saveActionLog = $saveActionLog;
        $this->getSessionById = $getSessionById;
        $this->getCurrentStaffIdBySession = $getCurrentStaffIdBySession;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function process(Request $request)
    {
        $sessionId = $request->getParam(self::SESSION_PARAM_KEY);
        $route = $this->inputParamsResolver->getRoute();
        if ($route->getServiceMethod() != 'logout') {
            try {
                $session = $this->getSessionById->execute($sessionId);
            } catch (Exception $e) {
                throw new AuthorizationException(
                    __($e->getMessage())
                );
            }
            if ($route->getServiceClass() != StaffManagementInterface::class
                && $route->getServiceMethod() != 'logout') {
                if ($session->getHasException() == DispatchServiceInterface::EXCEPTION_CODE_FORCE_SIGN_OUT) {
                    throw new AuthorizationException(
                        __('Opps! Access denied. Recent action has not been saved yet.'),
                        new Exception(),
                        DispatchServiceInterface::EXCEPTION_CODE_FORCE_SIGN_OUT
                    );
                } elseif ($session->getHasException() == DispatchServiceInterface::EXCEPTION_CODE_FORCE_CHANGE_POS) {
                    $locations = $this->locationRepository->getListAvailable($session->getStaffId());
                    if (!$locations) {
                        throw new AuthorizationException(
                            __('Opps! Access denied. Recent action has not been saved yet.'),
                            new Exception(),
                            DispatchServiceInterface::EXCEPTION_CODE_FORCE_SIGN_OUT
                        );
                    }
                    if (($route->getServiceClass() != PosRepositoryInterface::class
                            && $route->getServiceMethod() != 'assignPos')
                        && ($route->getServiceClass() != LocationRepositoryInterface::class
                            && $route->getServiceMethod() != 'getList')) {
                        throw new AuthorizationException(
                            __('Opps! Access denied. Recent action has not been saved yet.'),
                            new Exception(),
                            DispatchServiceInterface::EXCEPTION_CODE_FORCE_CHANGE_POS
                        );
                    }
                }
            }
            $this->logoutCustomer();
        }

        // Save action log
        $this->saveActionLog->execute($request, $this->inputParamsResolver->getRoute());

        return $this->doProcess($request);
    }

    /**
     * Execute api process
     *
     * @param Request $request
     * @return Response
     * @throws InputException
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function doProcess(Request $request)
    {
        $route = $this->inputParamsResolver->getRoute();
        $serviceMethodName = $route->getServiceMethod();
        $serviceClassName = $route->getServiceClass();
        $service = $this->objectManager->get($serviceClassName);
        $inputParams = $this->inputParamsResolver->resolve();

        // Enable Profiler
        if ($request->getParam('webposProfiler')) {
            Profiler::$enable = true;
        }

        Profiler::start('api_service');
        // @codingStandardsIgnoreStart
        $outputData = call_user_func_array([$service, $serviceMethodName], $inputParams);
        // @codingStandardsIgnoreEnd
        Profiler::stop('api_service');

        Profiler::start('process_output');
        if (!is_array($outputData) || !isset($outputData['cached_at'])) {
            $outputData = $this->serviceOutputProcessor->process(
                $outputData,
                $serviceClassName,
                $serviceMethodName
            );
        }
        Profiler::stop('process_output');

        if ($request->getParam(FieldsFilter::FILTER_PARAMETER) && is_array($outputData)) {
            $outputData = $this->fieldsFilter->filter($outputData);
        }

        // Output profiler
        if (Profiler::$enable) {
            return $this->response->prepareResponse(Profiler::$profiles);
        }

        $header = $this->deploymentConfig->get(
            ConfigOptionsListConstants::CONFIG_PATH_X_FRAME_OPT
        );
        if ($header) {
            $this->response->setHeader('X-Frame-Options', $header);
        }
        return $this->response->prepareResponse($outputData);
    }

    /**
     * @inheritdoc
     */
    public function canProcess(Request $request)
    {
        try {
            $route = $this->inputParamsResolver->getRoute();
            $aclResource = $route->getAclResources();
            if (in_array(self::MANAGE_POS_RESOURCE, $aclResource)) {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * Logout customer
     */
    public function logoutCustomer()
    {
        $isLoggedInCustomer = $this->objectManager->get(Session::class)
            ->isLoggedIn();
        if ($isLoggedInCustomer) {
            $this->objectManager->get(Session::class)->logout();
        }
    }
}
