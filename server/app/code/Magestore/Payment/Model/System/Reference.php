<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Payment\Model\System;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

/**
 * Reference Field
 */
class Reference extends Field
{
    /**
     * Getting back the reference text
     *
     * @param AbstractElement $element
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function render(AbstractElement $element)
    {
        return __(
            '<p>Upgrade to our <a href="%1">Commerce plan</a> to use other payment methods like <a href="%2">'
            .'Authorize.net</a>, Stripe, Square, SmartPay, Zip, Tyro etc.</p>',
            $this->getUpsellPaymentPage(),
            $this->getAuthorizeNetUrl()
        );
    }

    /**
     * Get upsell payment page url
     *
     * @return string
     */
    private function getUpsellPaymentPage()
    {
        return 'https://www.magestore.com/features/pos-terminal/?utm_source=pos-open-backend&utm_medium=product'
            .'&utm_campaign=upsell_commerce_2021&utm_content=pos-settings';
    }

    /**
     * Get authorize net url
     *
     * @return string
     */
    private function getAuthorizeNetUrl()
    {
        return 'https://www.authorize.net/';
    }
}
