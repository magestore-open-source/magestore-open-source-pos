<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Payment\Model\Payment\Method;

use Magestore\Payment\Model\Payment\AbstractMethod;

/**
 * Web POS Cash on delivery (Pay later) payment method model
 */
class Cod extends AbstractMethod
{
    /**
     * Payment method code
     * @var string
     */
    protected $_code = 'codforpos';
    /**
     * @var string
     */
    protected $enabledPath = 'payment/codforpos/active';
    /**
     * Class of form block
     * @var string
     */
    protected $_formBlockType = \Magestore\Payment\Block\Payment\Method\Cod::class;
}
