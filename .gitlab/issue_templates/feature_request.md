---
name: Feature request
about: Request new feature for POS core components

---

<!---
Please review our guidelines before adding a new issue: [Issue-reporting-guidelines]
Fields marked with (*) are required. Please don't remove the template.
-->

### Description (*)
<!--- Describe the feature you would like to add. -->

### Expected behavior (*)
<!--- What is the expected behavior of this feature? How is it going to work? -->

### Benefits
<!--- How do you think this feature would improve POS? -->

### Additional information
<!--- What other information can you provide about the desired feature? -->
