/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import i18n from 'i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import {reactI18nextModule} from 'react-i18next';
import Config from "./Config"

let path = window.location.pathname;
path = path.split(Config.basename);
path = path[0] ? path[0] : '/';

let locale = window.navigator.language.toLowerCase();
locale = locale.replace('-', '_');

i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(reactI18nextModule)
    .init({
        fallbackLng: false,
        lng: locale,

        // have a common namespace used around the full app
        ns: ['translations'],
        defaultNS: 'translations',
        nsSeparator: false,
        keySeparator: false,

        debug: false,

        interpolation: {
            escapeValue: false, // not needed for react!!
        },

        backend: {
            loadPath: path + Config.basename + '/locales/{{lng}}/{{ns}}.json'
        },

        react: {
            wait: true,
            withRef: false,
            bindI18n: 'languageChanged loaded',
            bindStore: 'added removed',
            nsMode: 'default'
        }
    });


export default i18n;
