/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
/**
 * Init GLOBAL_VARIABLES to store pos config
 */
if (typeof window.pos === 'undefined') {
    window.pos = {config: {}};
}
const GLOBAL_VARIABLES = window.pos.config;

/**
 * get allow permission of current staff
 *
 * @return {Array}
 *
 * */
export function getPermission() {
    if ( !GLOBAL_VARIABLES.config) return [];
    if ( !GLOBAL_VARIABLES.config.permissions) {
        return [];
    }
    let permissions = GLOBAL_VARIABLES.config.permissions;
    if ( permissions.length) {
        return permissions;
    }
    return [];
}

export default GLOBAL_VARIABLES;
