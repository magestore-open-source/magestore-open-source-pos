/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import GLOBAL_VARIABLES, {getPermission} from '../Config'

it('Get global variables', () => {
    expect(GLOBAL_VARIABLES).not.toBeNull();
    expect(GLOBAL_VARIABLES).not.toBeUndefined();
});

it('Check get permission function', () => {
    // Prepare environment
    let oldConfig = GLOBAL_VARIABLES.config;

    // Run test cases
    delete GLOBAL_VARIABLES.config;
    expect(getPermission()).toEqual([]);
    GLOBAL_VARIABLES.config = {};
    expect(getPermission()).toEqual([]);
    GLOBAL_VARIABLES.config.permissions = [];
    expect(getPermission()).toEqual([]);
    GLOBAL_VARIABLES.config.permissions = [''];
    expect(getPermission()).toEqual(['']);

    // Rollback environment
    GLOBAL_VARIABLES.config = oldConfig;
});
