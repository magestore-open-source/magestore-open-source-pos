/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractFactory from "../framework/factory/AbstractFactory";
import PaymentConstant from "../view/constant/PaymentConstant";
import {Cash} from "../service/payment/type/CashService";
import {PaymentAbstract} from "../service/payment/type/PaymentAbstract";
import {fire} from "../event-bus";

class PaymentFactory extends AbstractFactory {
    map = {
        [PaymentConstant.CASH]                             : Cash,
    };

    /**
     *  Constructor with event, allow extended
     */
    constructor() {
        super();
        fire('factory_payment_factory_init_after', { factory: this});
    }

    /**
     *
     * @param code
     * @return {PaymentAbstract}
     */
    getByCode(code) {
        return this.map[code] || PaymentAbstract;
    }

    /**
     *
     * @param code
     * @return {PaymentAbstract}
     */
    createByCode(code) {
        return this.create(this.getObject(`Service`, this.getByCode(code))).setCode(code);
    }
}

/**
 *
 * @type {PaymentFactory}
 */
const paymentFactory = (new PaymentFactory());

export default paymentFactory;