/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import React from 'react';
import './view/style/css/bootstrap.min.css'
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {HashRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import AppStore from './view/store/store';
import Login from './view/component/user/Login';
import Index from './view/Index';
import Loading from './view/component/loading/Loading';
import registerServiceWorker from './registerServiceWorker';
import SelectLocationPos from "./view/component/user/SelectLocationPos";
import App from "./App";
import i18n from './config/i18n';
import { I18nextProvider } from 'react-i18next';
import UserService from "./service/user/UserService";
import Events from './view/component/events'

let isLogin = (pathname) => {
    let session = UserService.getSession();
    if (pathname !== '/login' && !session) {
        return false;
    } else if (pathname !== '/login' && session) {
        return true;
    }
    return true;
}

/* eslint-disable react/prop-types */
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            isLogin(props.location.pathname) ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/login"
                    }}
                />
            )
        }
    />
);
/* eslint-enable react/prop-types */

ReactDOM.render(
    <I18nextProvider i18n={ i18n }>
    <Provider store={AppStore}>
        <Router>
            <div>
                <PrivateRoute component={App}/>
                <Switch>
                    <PrivateRoute path="/login" exact component={Login}/>
                    <PrivateRoute path="/loading" exact component={Loading}/>
                    <PrivateRoute path="/locations" exact component={SelectLocationPos}/>
                    <PrivateRoute path="/" component={Index}/>
                </Switch>
                <Events/>
            </div>
        </Router>
    </Provider>
    </I18nextProvider>,
    document.getElementById('root'));
registerServiceWorker();
