/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import React from 'react';
import CoreComponent from '../CoreComponent';
import Item from './item/AbstractListItem';

export default class AbstractList extends CoreComponent {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            columns: [],
            searchKey: '',
            isSearching: false
        }
    }

    /**
     * Set loading flag is true
     */
    startLoading() {
        this.setState({loading: true});
    }

    /**
     * Set loading flag is false
     */
    stopLoading() {
        this.setState({loading: false});
    }

    /**
     * Check list can load
     *
     * @return boolean
     */
    canLoad() {
        return !this.isLoading();
    }

    /**
     * Return loading flag value
     *
     * @return boolean
     */
    isLoading() {
        return this.state.loading;
    }

    /**
     * add items to list
     *
     * @param {Array} items
     */
    addItems(items = []) {
        this.setState({items: items || []});
    }

    /**
     * Push more items to list
     *
     * @param {Array} items
     */
    pushItems(items = []) {
        if (items.length > 0) {
            this.setState({items: [...this.state.items, ...items]});
        }
    }

    /**
     * Remove all items of list
     */
    clearItems() {
        this.setState({items: []})
    }

    /**
     * Load more products when you scroll product list
     *
     * @param {event} event
     */
    lazyload(event) {
        let scrollHeight = event.target.scrollHeight,
            clientHeight = event.target.clientHeight,
            scrollTop = event.target.scrollTop;
        if ((scrollHeight - (clientHeight + scrollTop) <= 0) && (this.canLoad() === true)) {
            this.startLoading();
        }
    }

    /**
     * Start searching list
     */
    startSearching() {
        this.setState({isSearching: true});
    }

    /**
     * Stop searching list
     */
    stopSearching() {
        this.setState({isSearching: false});
    }

    /**
     * List is searching
     *
     * @return {boolean}
     */
    isSearching() {
        return this.state.isSearching;
    }

    template() {
        return (
            <div>
                <table>
                    <thead>
                    <tr>
                        {
                            this.state.columns.map(
                                (column, index) => <th key={index} name={column.name}>{column.label}</th>
                            )
                        }
                    </tr>
                    </thead>
                    <tbody onScroll={event => this.lazyload(event)}>
                    {
                        this.state.items.map((item, index) => <Item key={index} columns={this.state.columns}
                                                                    data={item}/>)
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}