/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import React, {Component, Fragment} from 'react'

export default class CoreComponent extends Component {
    static acl = false;
    beforeHTML;
    afterHTML;
    showOnPages = [];

    constructor(props) {
        super(props);
        this.fireEvent();
    }

    /**
     *
     * @return {boolean}
     */
    canShow() {
        if (!this.props) return true;
        if (!this.props.hasOwnProperty('currentPage')) return true;
        return this.showOnPages.indexOf(this.props.currentPage) !== -1; // eslint-disable-line
    }

    /**
     *  abstract render method
     * @return {string}
     */
    template() {
        return '';
    }

    /**
     *  dispatch event to add render before or after template
     *  @return {void}
     */
    fireEvent() {
        let payload = {
            name: this.constructor.className,
            beforeHTML: this.beforeHTML,
            component: this
        };

        // AppStore.dispatch({
        //     type: `${payload.name}BeforeHtml`,
        //     payload
        // });

        this.beforeHTML = payload.beforeHTML;

        payload = {
            name: this.constructor.className,
            afterHTML: this.afterHTML,
            component: this
        };

        // AppStore.dispatch({
        //     type: `${payload.name}AfterHtml`,
        //     payload
        // });

        this.afterHTML = payload.afterHTML
    }

    /**
     *  render component
     *
     * @return {*}
     */
    render() {
        return (
            <Fragment>
                {this.beforeHTML}
                {this.template()}
                {this.afterHTML}
            </Fragment>
        )
    }
}