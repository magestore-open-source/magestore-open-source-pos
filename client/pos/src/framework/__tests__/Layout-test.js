/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import layout from "../Layout";
import {updateConfig} from "../../extension/config"

describe('Test layout mechanism', () => {
  it('Get layout function with dot pattern', () => {
    updateConfig({});
    expect(layout('catalog')()(1)).toBeUndefined();
    expect(layout('catalog.product.category')()).toBe(layout('catalog')('product')('category')());
    updateConfig({
      layout: {}
    });
    expect(layout('catalog.product.category')()).toBe(layout('catalog')('product')('category')());
    updateConfig({
      layout: {
        catalog: {
          product: {
            category: () => 10
          }
        }
      }
    });
    expect(layout('catalog.product')()).toBe(layout('catalog')('product')());
    expect(layout('catalog.product.category')()()).toBe(10);
    expect(layout('catalog.product.category')()).toBe(layout('catalog')('product')('category')());
  });

  it('Execute layout by plugin config', () => {
    updateConfig({
      layout: {
        catalog: {
          product: {
            category: (x) => 10 + x,
            page_size: 16,
            test: [
              (x) => x + 1,
              (x) => x + 2,
              13,
            ]
          }
        }
      }
    });
    expect(layout('catalog.product.category')()(11)).toBe(21);
    expect(layout('catalog')('product')('category')()(10)).toBe(20);
    expect(layout('catalog')('product')('page_size')()).toBe(16);
    expect(layout('catalog')('product')('test')()(10)).toEqual([11, 12, 13]);
  });
});
