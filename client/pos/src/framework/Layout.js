/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import config from '../extension/config'

/**
 * Empty function pointer
 */
function empty() {}

/**
 * Compile config with function call
 *
 * @param {any} config
 * @returns {any}
 */
function compile(config) {
  if (!Array.isArray(config)
    || undefined === config.find(e => 'function' === typeof e)
  ) {
    return config;
  }
  return function(...args) {
    return config.map(e => ('function' === typeof e) ? e.apply(null, args) : e);
  };
}

/**
 * Get layout function by config
 *
 * @param {Object} config
 * @returns {Function}
 */
function getLayout(config) {
  return function (name) {
    if (!name) {
      return (undefined === config) ? empty : compile(config);
    } else {
      return getLayout((!config || !config[name]) ? undefined : config[name]);
    }
  }
}

let cache = {};

/**
 * Get cache object
 *
 * @returns {Object}
 */
function getCache() {
  if (cache.__config !== config()) {
    cache = {__config: config()};
  }
  return cache;
}

/**
 * Get layout function config by name
 *
 * Name of layout can be dot(.) pattern:
 *  layout('product.category.list') = layout('product')('category')('list')
 *
 * @param {String} name
 * @returns {Function}
 */
export default function layout(name) {
  let cache = getCache();
  if (!cache.hasOwnProperty(name)) {
    cache[name] = getLayout(name.split('.').reduce(function(config, name) {
      return (!config || !config[name]) ? undefined : config[name];
    }, config().layout));
  }
  return cache[name];
}
