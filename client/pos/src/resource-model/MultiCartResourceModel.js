/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "./AbstractResourceModel";

export default class MultiCartResourceModel extends AbstractResourceModel {
    static className = 'MultiCartResourceModel';

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {resourceName : 'Cart'};
    }

    /**
     * Search by staff Id
     * @param posId
     * @returns {*|{type: string, code: *}}
     */
    searchByPosId(posId) {
        return this.getResourceOffline().searchByPosId(posId);
    }

    /**
     * add new cart
     * @param cart
     * @returns {*|{type: string, code: *}}
     */
    add(cart) {
        return this.getResourceOffline().add(cart);
    }

    /**
     * update cart
     * @param cart
     * @returns {*|{type: string, code: *}}
     */
    update(cart) {
        return this.getResourceOffline().save(cart);
    }

    /**
     * delete cart
     * @param cart
     * @returns {*|{type: string, code: *}}
     */
    delete(cart) {
        return this.getResourceOffline().delete(cart.id);
    }
}