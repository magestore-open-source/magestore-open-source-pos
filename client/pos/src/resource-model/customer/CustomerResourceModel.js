/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "../AbstractResourceModel";
import SyncConstant from "../../view/constant/SyncConstant";

export default class CustomerResourceModel extends AbstractResourceModel {
    static className = 'CustomerResourceModel';

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            resourceName : 'Customer',
            dataType: SyncConstant.TYPE_CUSTOMER
        };
    }

    /**
     * check email
     * @param email
     * @returns {*|{type: string, email: *}|Dexie.Promise.<any|T>|Dexie.Promise.<any>|Promise.<any>}
     */
    checkEmail(email) {
        return this.getResource().checkEmail(email);
    }

    /**
     * create customer
     * @param customer
     * @returns {Promise.<TResult>}
     */
    createCustomer(customer) {
        return this.saveToDb([customer], true).then(() => customer);
    }

    /**
     * edit customer
     * @param customer
     */
    editCustomer(customer) {
        return this.getResourceOffline().updateCustomer(customer);
    }

    /**
     * update customer
     * @param customer
     * @returns {*|Promise}
     */
    updateCustomer(customer) {
        return this.getResourceOffline().updateCustomer(customer);
    }

    /**
     * get customer by id
     * @param id
     * @param field
     */
    get(id, field = null) {
        return this.getResourceOffline().get(id, field);
    }
}
