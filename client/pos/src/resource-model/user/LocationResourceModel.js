/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "../AbstractResourceModel";

export default class LocationResourceModel extends AbstractResourceModel {
    static className = 'LocationResourceModel';

    /**
     * constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {resourceName : 'Location'};
    }

    /**
     * assign pos to session
     *
     * @param posId
     * @param locationId
     * @param currentStaffId
     * @returns {*|promise|{type, posId, locationId, currentStaffId}}
     */
    assignPos(posId, locationId, currentStaffId){
        return this.getResourceOnline().assignPos(posId, locationId, currentStaffId);
    }
}

