/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "../AbstractResourceModel";

export default class UserResourceModel extends AbstractResourceModel {
    static className = 'UserResourceModel';
    /**
     * constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {resourceName : 'User'};
    }

    /**
     * login by username and password
     *
     * @return: promise
     */
    login(username, password) {
        return this.getResourceOnline().login(username, password);
    }

    /**
     * Change Password by username, old_password and new_password
     *
     * @return: promise
     */
    changePassword(user_name, old_password, new_password, confirmation_password) {
        return this.getResourceOnline().changePassword(user_name, old_password, new_password, confirmation_password);
    }

    /**
     *
     * @returns {*|promise|{type}}
     */
    continueLogin(){
        return this.getResourceOnline().continueLogin();
    }
    /* eslint-disable no-unused-vars */
    /**
     * user change information
     * @returns {Promise}
     */
    changeInformation() {
        return new Promise((resolve, reject) => {
            resolve('12131');
        });
    }
    /* eslint-enable no-unused-vars */
    /**
     * logout by session
     *
     * @return: Promise
     */
    logout() {
        return this.getResourceOnline().logout();
    }

    /**
     * get logo
     */
    getLogo() {
        return this.getResourceOnline().getLogo();
    }

    /**
     * get countries
     */
    getCountries() {
        return this.getResourceOnline().getCountries();
    }
}

