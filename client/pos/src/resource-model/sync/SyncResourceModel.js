/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "../AbstractResourceModel";

export default class SyncResourceModel extends AbstractResourceModel {
    static className = 'SyncResourceModel';
    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {resourceName : 'Sync'};
    }

    /**
     * Call get all sync items
     *
     * @returns {Object|*|FormDataEntryValue[]|string[]}
     */
    getAll() {
        return this.getResourceOffline().getAll();
    }

    /**
     * Call Set default data of Sync Table
     * @returns {*|void|Promise<*|null>|Promise<*|null>}
     */
    setDefaultData() {
        return this.getResourceOffline().setDefaultData();
    }

    /**
     * Reset items's data of sync table in indexedDb
     * @param items
     * @returns {*}
     */
    resetData(items) {
        return this.getResourceOffline().resetData(items);
    }

    /**
     * Get default data type mode
     * @returns {*|{}}
     */
    getDefaultDataTypeMode() {
        return this.getResourceOffline().getDefaultDataTypeMode();
    }

    /**
     * Get default sync data
     */
    getDefaultData() {
        return this.getResourceOffline().getDefaultData();
    }
}
