/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "../AbstractResourceModel";
import QueryService from "../../service/QueryService";
import SyncConstant from "../../view/constant/SyncConstant";

export default class ErrorLogResourceModel extends AbstractResourceModel {
    static className = 'ErrorLogResourceModel';

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {resourceName : 'ErrorLog'};
    }

    /**
     * Get all data in table error log
     * @returns {Promise<any>}
     */
    getAllDataErrorLog() {
        // filter error log request place order
        let queryService = QueryService.reset();
        queryService.addFieldToFilter('action_type', SyncConstant.REQUEST_PLACE_ORDER, 'eq');
        return this.getResourceOffline().getList(queryService);
    }
}