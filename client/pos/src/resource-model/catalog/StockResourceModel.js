/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "../AbstractResourceModel";
import SyncConstant from "../../view/constant/SyncConstant";

export default class StockResourceModel extends AbstractResourceModel {
    static className = 'StockResourceModel';

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            resourceName : 'Stock',
            dataType: SyncConstant.TYPE_STOCK
        };
    }

    /**
     * get available qty
     * @param productId
     */
    getAvailableQty(productId) {
        return this.getResourceOnline().getQty(productId);
    }

    /**
     * Get stock from product ids
     *
     * @param productIds
     * @return {*|Promise<any>}
     */
    getStockProducts(productIds) {
        return this.getResource().getStockProducts(productIds);
    }
}
