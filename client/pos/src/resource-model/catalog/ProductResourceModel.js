/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import AbstractResourceModel from "../AbstractResourceModel";
import SyncConstant from "../../view/constant/SyncConstant";

export default class ProductResourceModel extends AbstractResourceModel {
    static className = 'ProductResourceModel';

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            resourceName: 'Product',
            dataType: SyncConstant.TYPE_PRODUCT
        };
    }


    /**
     * Get options product
     *
     * @param productId
     * @return {*}
     */
    getOptions(productId) {
        return this.getResource().getOptions(productId);
    }

    /**
     * Get options product and stock of children
     *
     * @param productId
     * @return {*}
     */
    getOptionsAndStockChildrens(productId) {
        return this.getResourceOnline().getOptionsAndStockChildrens(productId);
    }

    /**
     * Get list product ids from response get list product
     * @param response
     * @return {*|Array}
     */
    getProductIdsFromResponse(response) {
        return this.getResourceOffline().getProductIdsFromResponse(response);
    }

    /**
     * Add stock for product
     *
     * @param response
     * @param stocks
     */
    addStockProducts(response, stocks) {
        return this.getResourceOffline().addStockProducts(response, stocks);
    }

    /**
     * Add catalog rule prices for product
     * @param response
     * @param catalogRulePrices
     * @return {*}
     */
    addCatalogRuleProductPrices(response, catalogRulePrices) {
        return this.getResourceOffline().addCatalogRuleProductPrices(response, catalogRulePrices);
    }

    /**
     * Search by barcode
     * @param code
     * @returns {*|{type: string, code: *}}
     */
    searchByBarcode(code) {
        return this.getResource().searchByBarcode(code);
    }
}
