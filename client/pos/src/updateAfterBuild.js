/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
/* eslint-disable no-console */
function updateServiceWorker() {
    console.log('Updating service worker...')
    var fs = require('fs');
    fs.readFile('public/service-worker.js', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        let timestamp = new Date().getTime();
        var result = data.replace(/static-v2/g, 'static-v' + timestamp).replace('dynamic-v2', 'dynamic-v' + timestamp);

        fs.writeFile('build/service-worker.js', result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
    });
}

updateServiceWorker();
/* eslint-enable no-console */
