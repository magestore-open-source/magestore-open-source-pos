/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Abstract from './IndexedDbAbstract';

export default class IndexedDbStock extends Abstract {
    static className = 'IndexedDbStock';

    main_table = 'stock';
    primary_key = 'item_id';
    offline_id_prefix = '';

    /**
     * Get stock by product id
     *
     * @param productId
     * @return {Promise<any>}
     */
    getStock(productId) {
        return this.get(productId, 'product_id');
    }

    /**
     * Get list stock by product ids
     *
     * @param productIds
     * @return {Promise<any>}
     */
    getListByProductIds(productIds) {
        return new Promise((resolve, reject)=> {
            this.db[this.main_table].where('product_id').anyOf(productIds).toArray()
                .then(items => resolve(items))
                .catch(exception => reject(exception));
        });
    }

    /**
     * Get stock products from product ids
     *
     * @param productIds
     */
    getStockProducts(productIds) {
        return new Promise((resolve, reject) => {
            this.getListByProductIds(productIds).then(items => {
                let stocks = {};
                if (items && items.length > 0) {
                    items.map(stock => {
                        stock.is_offline_data = true;
                        if (!stocks[stock.product_id]) {
                            stocks[stock.product_id] = [];
                        }
                        stock.need_calculate_salable = true;
                        stocks[stock.product_id].push(stock);
                        return stock;
                    });
                }
                resolve(stocks);
            }).catch(error => {
                reject(error);
            })
        })
    }
}
