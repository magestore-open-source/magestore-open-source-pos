/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Dexie from 'dexie';

export {default as IndexedDbSync} from './IndexedDbSync';
export {default as IndexedDbProduct} from './IndexedDbProduct';
export {default as IndexedDbActionLog} from './IndexedDbActionLog';
export {default as IndexedDbPayment} from './IndexedDbPayment';
export {default as IndexedDbShipping} from './IndexedDbShipping';
export {default as IndexedDbOrder} from './IndexedDbOrder';
export {default as IndexedDbStock} from './IndexedDbStock';
export {default as IndexedDbErrorLog} from './IndexedDbErrorLog';
export {default as IndexedDbCustomer} from './IndexedDbCustomer';
export {default as IndexedDbCategory} from './IndexedDbCategory';
export {default as IndexedDbTaxRate} from './IndexedDbTaxRate';
export {default as IndexedDbTaxRule} from './IndexedDbTaxRule';
export {default as IndexedDbCatalogRuleProductPrice} from './IndexedDbCatalogRuleProductPrice';

let db = new Dexie("magestore_pos_open");

db.version(1).stores({
    product: 'id, sku, name, pos_barcode',
    sync: 'type',
    action_log: 'action_id, uuid, order, staff_id, created_at',
    payment: 'code, sort_order',
    shipping: 'code',
    order: 'increment_id, created_at, state',
    stock: 'item_id, product_id',
    error_log: 'action_id, uuid, order, staff_id, created_at',
    customer: 'id, email, full_name, tmp_customer_id',
    category: 'id, name, level, parent_id, path',
    cart: 'id, pos_id, count',
    catalogrule_product_price: 'rule_product_price_id, product_id',

    product_index: 'id',
    customer_index: 'id',
    order_index: 'id',
});

db.open();

export default db;
