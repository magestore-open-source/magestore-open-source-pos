/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Abstract from './IndexedDbAbstract';

export default class IndexedDbCatalogRuleProductPrice extends Abstract {
    static className = 'IndexedDbCatalogRuleProductPrice';

    main_table = 'catalogrule_product_price';
    primary_key = 'rule_product_price_id';
    index_table = '';
    index_table_fields = [];
    index_extension_attribute_table_fields = [];
    default_order_by = 'rule_product_price_id';

    /**
     * get not existed ids
     * @param ruleProductPriceIds
     * @return {Promise<Array>}
     */
    async getNotExistedIds(ruleProductPriceIds) {
        let allIds = await this.db[this.main_table].toCollection().primaryKeys();
        let index = 0, len = ruleProductPriceIds.length;
        if (index === len) {
            return allIds;
        }
        let result = allIds.filter(id => {
            if (index === len) {
                return true;
            }
            while (ruleProductPriceIds[index] < id) {
                index++;
                if (index === len) {
                    return true;
                }
            }
            if (ruleProductPriceIds[index] === id) {
                index++;
                return false;
            }
            return true;
        });
        return result;
    }

    /**
     * get data by product ids
     * @param productIds
     * @returns {Promise<*>}
     */
    getByProductIds(productIds) {
        return this.db[this.main_table].where('product_id').anyOf(productIds).toArray();
    }

    /**
     * get catalog rule product price for products
     * @param productIds
     * @returns {Promise<void>}
     */
    async getCatalogRulePriceProducts(productIds) {
        let data = await this.getByProductIds(productIds);
        let catalogRulePrices = {};
        data.forEach(rulePrice => {
            let productId = rulePrice.product_id;
            if (!catalogRulePrices[productId]) {
                catalogRulePrices[productId] = [rulePrice];
            } else {
                catalogRulePrices[productId].push(rulePrice);
            }
        });
        return catalogRulePrices;
    }
}
