/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Abstract from './IndexedDbAbstract';

export default class IndexedDbOrder extends Abstract {
    static className = 'IndexedDbOrder';

    main_table = 'order';
    primary_key = 'increment_id';
    index_table = 'order_index';
    index_table_fields = [
        'increment_id',
        'search_string',
        'created_at',
        'state'
    ];
    index_fields = [];
    index_extension_attribute_table_fields = [];
    offline_id_prefix = 'order';
    default_order_by = 'created_at';
    default_order_direction = 'DESC';

    /**
     * get order by increment ids
     * @param ids
     * @returns {Promise}
     */
    getOrderByIncrementIds(ids) {
        return new Promise((resolve, reject) => {
            let self = this;
            this.db[self.main_table].where(self.primary_key).anyOf(ids).toArray(items => {
                if (!items) {
                    return;
                }
                let orderParams = [{field: "created_at", direction: "DESC"}];
                self.sort(items, orderParams);
                resolve(items);
            })
                .catch(function (err) {
                    return reject(err);
                });
        });
    }
}
