/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Abstract from './IndexedDbAbstract';

export default class IndexedDbActionLog extends Abstract {
    static className = 'IndexedDbActionLog';

    main_table = 'action_log';
    primary_key = 'action_id';
    offline_id_prefix = 'action';

    // /**
    //  * Constructor
    //  * @param props
    //  */
    // constructor(props) {
    //     super(props);
    // }

    /**
     * get last order of uuid
     * @param uuid
     * @returns {Promise<any>}
     */
    getLastOrder(uuid) {
        return new Promise((resolve, reject) => {
            this.db[this.main_table].where('uuid').equals(uuid).reverse().sortBy('order').then(results => {
                if (results[0]) {
                    resolve(results[0].order);
                } else {
                    resolve(0);
                }
            }).catch(exception => {
                reject(exception);
            });
        })
    }

    /**
     * check dependent request action log
     * @param uuid
     * @param order
     * @returns {Promise}
     */
    checkDependent(uuid, order) {
        return new Promise((resolve, reject) => {
            this.db[this.main_table].where('uuid').equals(uuid).and(function(item) {
                return item.order < order;
            }).toArray().then(results => {
                if (results.length > 0) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            }).catch(exception => {
                reject(exception);
            });
        })
    }

    /**
     * Get all data of table
     * @returns {Promise<any>}
     */
    getAll() {
        return this.db[this.main_table].toCollection().sortBy('created_at');
    }
}
