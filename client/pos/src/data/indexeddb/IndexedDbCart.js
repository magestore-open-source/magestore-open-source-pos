/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Abstract from './IndexedDbAbstract';

export default class IndexedDbCart extends Abstract {
    static className = 'IndexedDbCart';
    main_table = 'cart';
    primary_key = 'id';


    /**
     * Search by pos Id
     *
     * @param posId
     * @returns {Promise<any>}
     */
    searchByPosId(posId) {
        return new Promise((resolve, reject) => {
            this.db[this.main_table]
                .where('pos_id')
                .equalsIgnoreCase(posId)
                .reverse()
                .sortBy('id')
                .then(items => resolve(items))
                .catch(() => reject([]));
        });
    }

    /**
     * add new record
     * @param data
     * @param requestTime
     */
    add(data, requestTime = 1) {
        return new Promise((resolve, reject) => {
            if (requestTime > 10) {
                resolve(0);
            }
            this.db[this.main_table].put(data).then(response => {
                resolve(response);
            }).catch('AbortError', error => {
                this.add(data, requestTime++)
                    .then(response => resolve(response))
                    .catch(error => resolve(error));
            }).catch('TimeoutError', error => {
                this.bulkPut(data, requestTime++)
                    .then(response => resolve(response))
                    .catch(error => resolve(error));
            }).catch(error => {
                this.add(data, requestTime++)
                    .then(response => resolve(response))
                    .catch(error => resolve(error));
            })
        });
    }
}
