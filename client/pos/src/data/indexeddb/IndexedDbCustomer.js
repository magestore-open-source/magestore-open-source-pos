/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Abstract from './IndexedDbAbstract';

export default class IndexedDbCustomer extends Abstract {
    static className = 'IndexedDbCustomer';

    main_table = 'customer';
    primary_key = 'id';
    offline_id_prefix = 'customer';
    index_table = 'customer_index';
    index_table_fields= [
        'id',
        'full_name',
        'search_string',
    ];
    index_extension_attribute_table_fields = [];
    default_order_by = 'full_name';

    /**
     * check email
     * @param email
     * @returns {Dexie.Promise<any | T> | Dexie.Promise<any>}
     */
    checkEmail(email) {
        return this.db[this.main_table].where('email').equalsIgnoreCase(email).first(item => !item);
    }

    /**
     * update customer
     * @param data
     * @returns {Promise}
     */
    updateCustomer(data) {
        // eslint-disable-next-line no-unused-vars
        return new Promise((resolve, reject) => {
            this.db[this.main_table].where('id').equals(data.id).or('email')
                .equalsIgnoreCase(data.email).delete().then(() => {
                this.bulkPut([data]).then(
                    // eslint-disable-next-line no-unused-vars
                    item => {
                        let new_search_string = data.email + data.full_name;
                        if (new_search_string !== data.search_string) {
                            this.reindexTable();
                        }
                        resolve(data);
                    }
                );
            });
        })
    }
}
