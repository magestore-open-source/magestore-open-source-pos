/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Abstract from './IndexedDbAbstract';

export default class IndexedDbPayment extends Abstract {
    static className = 'IndexedDbPayment';

    main_table = 'payment';
    primary_key = 'code';
    offline_id_prefix = '';

    /**
     * Get all data of table
     * @returns {Promise<any>}
     */
    getAll() {
        return this.db[this.main_table].toCollection().sortBy('sort_order');
    }
}
