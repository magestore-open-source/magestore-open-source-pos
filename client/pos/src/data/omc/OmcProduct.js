/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import OmcAbstract from "./OmcAbstract";
import ApiResourceConstant from "../../view/constant/data-resource/ApiResourceConstant";
import RestProduct from "./rest/RestProduct";
import GraphqlProduct from "./graphql/GraphqlProduct";

if (!window.Promise) {
    window.Promise = Promise;
}

export default class OmcProduct extends OmcAbstract {
    static className = 'OmcProduct';

    apiResources = {
        [ApiResourceConstant.REST]: RestProduct,
        [ApiResourceConstant.GRAPHQL]: GraphqlProduct,
    };


    /**
     * Request api get option product
     *
     * @param productId
     * @return {Promise<any>}
     */
    getOptions(productId) {
        return this.getRestResource().getOptions(productId);
    }

    /**
     * Request api get option product
     *
     * @param productId
     * @return {Promise<any>}
     */
    getOptionsAndStockChildrens(productId) {
        return this.getRestResource().getOptionsAndStockChildrens(productId);
    }

    /**
     * get product ids from option list
     *
     * @param response
     * @returns {Array}
     */
    getProductIdsFromGetOptionsResponse(response) {
        return this.getRestResource().getProductIdsFromGetOptionsResponse(response);
    }

    /**
     * Get stock products from product ids
     *
     * @param productIds
     */
    getStockItemsToRefund(productIds) {
        return this.getRestResource().getStockItemsToRefund(productIds);
    }

    /**
     * Search product by barcode
     * @param code
     * @returns {Promise<any>}
     */
    searchByBarcode(code) {
        return this.getRestResource().searchByBarcode(code);
    }
}

