/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import OmcAbstract from "./OmcAbstract";
import ApiResourceConstant from "../../view/constant/data-resource/ApiResourceConstant";
import RestOrder from "./rest/RestOrder";
import GraphqlOrder from "./graphql/GraphqlOrder";

export default class OmcOrder extends OmcAbstract {
    static className = 'OmcOrder';

    apiResources = {
        [ApiResourceConstant.REST]: RestOrder,
        [ApiResourceConstant.GRAPHQL]: GraphqlOrder,
    };

    /**
     * get path api place order
     * @return {string}
     */
    getPathPlaceOrder() {
        return this.getRestResource().getPathPlaceOrder();
    }

    /**
     * get path take payment
     * return string
     */
    getPathTakePayment() {
        return this.getRestResource().getPathTakePayment();
    }

    /**
     * Get Path create creditmemo
     */
    getPathCreateCreditmemo() {
        return this.getRestResource().getPathCreateCreditmemo();
    }

    /**
     * get path api hold order
     * @return {string}
     */
    getPathHoldOrder() {
        return this.getRestResource().getPathHoldOrder();
    }

    /**
     * get path api hold order
     * @return {string}
     */
    getPathUnHoldOrder() {
        return this.getRestResource().getPathUnHoldOrder();
    }

    /**
     * get path api delete order
     * @return {string}
     */
    getPathDeleteOrder() {
        return this.getRestResource().getPathDeleteOrder();
    }

	/**
     * get path api send email
     * @return {string}
     */
    getPathSendEmailOrder() {
        return this.getRestResource().getPathSendEmailOrder();
    }

    /**
     * get path api add comment
     * @return {string}
     */
    getPathAddComentOrder() {
        return this.getRestResource().getPathAddComentOrder();
    }

    /**
     * get path api cancel order
     * @return {string}
     */
    getPathCancelOrder() {
        return this.getRestResource().getPathCancelOrder();
    }

    /**
     * get path api send email credit memo
     * @return {string}
     */
    getPathSendEmailCreditmemo() {
        return this.getRestResource().getPathSendEmailCreditmemo();
    }

    /**
     * get path api credit memo create customer
     * @return {string}
     */
    getPathCreditmemoCreateCustomer() {
        return this.getRestResource().getPathCreditmemoCreateCustomer();
    }

    /**
     * get path api load order
     * @return {string}
     */
    getPathLoadOrder() {
        return this.getRestResource().getPathLoadOrder();
    }

    /**
     * get list order statuses
     * @return {Promise<any>}
     */
    getListOrderStatuses() {
        return this.getRestResource().getListOrderStatuses();
    }

    /**
     * get out of permission orders
     * @param queryService
     * @param isSync
     * @returns {Promise<any>}
     */
    getOutOfPermissionOrders(queryService = {}, isSync = false) {
        return this.getRestResource().getOutOfPermissionOrders(queryService, isSync);
    }

    /**
     * reload order
     * @param orderIncrement
     * @return {Promise<any>}
     */
    loadOrderByIncrement(orderIncrement) {
        return this.getRestResource().loadOrderByIncrement(orderIncrement);
    }
}
