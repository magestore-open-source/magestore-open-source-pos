/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import { ApolloClient, InMemoryCache } from '@apollo/client';
import Config from "../../../config/Config";

/**
 * get base url
 * @returns {string}
 */
function getBaseUrl() {
    if (process.env.NODE_ENV !== 'production') {
        return process.env.REACT_APP_POS_URL;
    }
    let url = window.location.href;
    url = url.split(Config.basename)[0];
    return url;
}

function getUri() {
    return getBaseUrl() + 'graphql';
}

const client = new ApolloClient({
    uri: getUri(),
    cache: new InMemoryCache()
});

export default client;