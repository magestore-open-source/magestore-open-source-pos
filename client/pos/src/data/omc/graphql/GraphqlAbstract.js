/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import LocalStorageHelper from '../../../helper/LocalStorageHelper'
import {gql} from "@apollo/client";

export default class GraphqlAbstract {
    // Use for graphql
    apolloClient = require('./ApolloClient').default;

    /**
     * GraphQL Query
     *
     * @param {string} query
     * @param {object} variables
     * @returns {Promise<ApolloQueryResult<any>>}
     */
    query(query, variables = {}) {
        let token = LocalStorageHelper.getToken();
        return this.apolloClient.query({
            context: {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : '',
                    'pos-session': LocalStorageHelper.getSession()
                }
            },
            query: gql(query),
            variables: variables
        });
    }

    /**
     * GraphQL Mutate
     *
     * @param {string} mutation
     * @param {object} variables
     * @returns {Promise<FetchResult<any>>}
     */
    mutate(mutation, variables = {}) {
        let token = LocalStorageHelper.getToken();
        return this.apolloClient.mutate({
            context: {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : '',
                    'pos-session': LocalStorageHelper.getSession()
                }
            },
            mutation: gql(mutation),
            variables: variables
        });
    }

    /**
     * GraphQL subscribe
     *
     * @param {string} query
     * @param {object} variables
     * @returns {Observable<FetchResult<any>>}
     */
    subscribe(query, variables = {}) {
        let token = LocalStorageHelper.getToken();
        return this.apolloClient.subscribe({
            context: {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : '',
                    'pos-session': LocalStorageHelper.getSession()
                }
            },
            query: gql(query),
            variables: variables
        });
    }
}

