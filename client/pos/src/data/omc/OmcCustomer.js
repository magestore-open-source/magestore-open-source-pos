/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import OmcAbstract from "./OmcAbstract";
import ApiResourceConstant from "../../view/constant/data-resource/ApiResourceConstant";
import RestCustomer from "./rest/RestCustomer";
import GraphqlCustomer from "./graphql/GraphqlCustomer";

export default class OmcCustomer extends OmcAbstract {
    static className = 'OmcCustomer';

    apiResources = {
        [ApiResourceConstant.REST]: RestCustomer,
        [ApiResourceConstant.GRAPHQL]: GraphqlCustomer
    };

    /**
     * check email
     * @param email
     * @returns {Promise.<any>}
     */
    checkEmail(email) {
        return this.getRestResource().checkEmail(email);
    }

    /**
     * get path save customer
     * return string
     */
    getPathSaveCustomer() {
        return this.getRestResource().getPathSaveCustomer();
    }

    /**
     * get path edit customer
     * return string
     */
    getPathEditCustomer() {
        return this.getRestResource().getPathEditCustomer();
    }
}
