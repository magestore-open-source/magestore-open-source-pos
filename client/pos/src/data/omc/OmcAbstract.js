/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import ApiResourceConstant from "../../view/constant/data-resource/ApiResourceConstant";
import ObjectManager from "../../framework/ObjectManager";

export default class OmcAbstract {
    /**
     * Api resources
     *
     * Format: {
     *   'rest' => RestResource,
     *   'graphql' => GraphqlResource,
     *   ...
     * }
     * @type {{}}
     */
    apiResources = {};

    /**
     * Default resource type
     * @type {string}
     */
    defaultResource = ApiResourceConstant.REST;

    /**
     * Get api resources
     *
     * @returns {{}}
     */
    getApiResources() {
        return this.apiResources;
    }

    /**
     * Get Rest Resource
     *
     * @returns {*}
     */
    getRestResource() {
        return this.getApiResource(ApiResourceConstant.REST);
    }

    /**
     * Get Graphql Resource
     *
     * @returns {*}
     */
    getGraphqlResource() {
        return this.getApiResource(ApiResourceConstant.GRAPHQL);
    }

    /**
     * Get Api Resource
     *
     * @param resourceType
     * @returns {*}
     */
    getApiResource(resourceType = this.defaultResource) {
        return ObjectManager.get(this.getApiResources()[resourceType]);
    }

    /**
     * Get list via api
     * @param queryService
     * @param isSync
     * @returns {Promise<any>}
     */
    getList(queryService = {}, isSync = false) {
        return this.getApiResource().getList(queryService, isSync);
    }

    /**
     * Get deleted items via api
     * @param queryService
     * @param isSync
     * @returns {Promise<any>}
     */
    getDeleted(queryService = {}, isSync = false) {
        return this.getApiResource().getDeleted(queryService, isSync);
    }

    /**
     * Get data by id
     * @param id
     * @return {Promise<any>}
     */
    getById(id) {
        return this.getApiResource().getById(id);
    }

    /**
     * Get update data via api
     *
     * @param queryService
     * @param isSync
     * @returns {Promise<any>}
     */
    getUpdateData(queryService = {}, isSync = false) {
        return this.getApiResource().getUpdateData(queryService, isSync);
    }
}

