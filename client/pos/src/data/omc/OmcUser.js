/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import OmcAbstract from "./OmcAbstract";
import ApiResourceConstant from "../../view/constant/data-resource/ApiResourceConstant";
import RestUser from "./rest/RestUser";
import GraphqlUser from "./graphql/GraphqlUser";

export default class OmcUser extends OmcAbstract {
    static className = 'OmcUser';

    apiResources = {
        [ApiResourceConstant.REST]: RestUser,
        [ApiResourceConstant.GRAPHQL]: GraphqlUser,
    };

    /**
     * login with username and password
     *
     * @param string
     * @param string
     *
     * @return promise
     */
    login(username, password) {
        return this.getRestResource().login(username, password);
    }

    /**
     * Change Password with username, new_password and old_password
     *
     * @return promise
     * @param user_name
     * @param old_password
     * @param new_password
     * @param confirmation_password
     */
    changePassword(user_name, old_password, new_password, confirmation_password) {
        return this.getRestResource().changePassword(user_name, old_password, new_password, confirmation_password);
    }

    continueLogin(){
        return this.getRestResource().continueLogin();
    }

    /**
     * get logo form server
     */
    getLogo() {
        return this.getRestResource().getLogo();
    }

    /**
     * get countries form server
     */
    getCountries() {
        return this.getRestResource().getCountries();
    }

    /**
     * logout with session
     * @return {Promise}
     */
    logout() {
        return this.getRestResource().logout();
    }

    /**
     * get path api change information
     * @return {string}
     */
    getPathChangeInfomation() {
        return this.getRestResource().getPathChangeInfomation();
    }
}

