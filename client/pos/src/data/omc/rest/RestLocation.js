/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";

export class RestLocation extends RestAbstract {
    static className = 'RestLocation';

    assign_pos_api = "/V1/webpos/posassign";

    /**
     * assign pos
     * @returns {promise}
     */
    assignPos(posId, locationId, currentStaffId) {
        let params = {
            pos: {
                pos_id: posId,
                location_id: locationId
            }
        };
        let url = this.getBaseUrl() + this.assign_pos_api;
        return this.post(url, params);
    }
}

/**
 * @type {RestLocation}
 */
export default DataResourceFactory.get(RestLocation);

