/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";

export class RestCustomer extends RestAbstract {
    static className = 'RestCustomer';

    get_list_api = "/V1/webpos/customers/list";
    search_api = "/V1/webpos/customers/search";
    get_deleted_api = "/V1/webpos/customers/deleted";
    get_by_id_api = "/V1/webpos/customers";
    get_update_data_api = "/V1/webpos/customers/updateLoyalty";

    save_customer_api = "/V1/webpos/customers";
    edit_customer_api = "/V1/webpos/customers/";
    check_email_api = "/V1/customers/isEmailAvailable";

    /**
     * check email
     * @param email
     * @returns {Promise.<any>}
     */
    checkEmail(email) {
        let url = this.getBaseUrl() + this.check_email_api;
        let param = {
            customerEmail: email
        };
        return this.post(url, param);
    }

    /**
     * get path save customer
     * return string
     */
    getPathSaveCustomer() {
        return this.save_customer_api;
    }

    /**
     * get path edit customer
     * return string
     */
    getPathEditCustomer() {
        return this.edit_customer_api;
    }
}

/**
 * @type {RestCustomer}
 */
export default DataResourceFactory.get(RestCustomer);
