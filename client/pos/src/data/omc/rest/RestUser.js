/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";

export class RestUser extends RestAbstract {
    static className = 'RestUser';

    login_api = "/V1/webpos/staff/login";
    continue_login_api = "/V1/webpos/staff/continueLogin";
    assign_pos_api = "/V1/webpos/posassign";
    change_information_api = "/V1/webpos/staff/changepassword";
    logout_api = "/V1/webpos/staff/logout";
    logo_api = "/V1/webpos/website/information?";
    countries_api = "/V1/webpos/country/list?";

    /**
     * login with username and password
     *
     * @param string
     * @param string
     *
     * @return promise
     */
    login(username, password) {
        let params = {
            staff: {"username": username, "password": password},
            app: "pos"
        };

        let url = this.getBaseUrl() + this.login_api;
        return this.post(url, params);
    }

    /**
     * Change Password with username, new_password and old_password
     *
     * @return promise
     * @param user_name
     * @param old_password
     * @param new_password
     * @param confirmation_password
     */
    changePassword(user_name, old_password, new_password, confirmation_password) {
        let params = {"username": user_name, "oldPassword": old_password, "newPassword": new_password, "confirmationPassword": confirmation_password};

        let url = this.getBaseUrl() + this.change_information_api;
        return this.post(url, params);
    }

    continueLogin(){
        let url = this.getBaseUrl() + this.continue_login_api;
        return this.get(url);
    }

    /**
     * get logo form server
     */
    getLogo() {
        let url = this.getBaseUrl() + this.logo_api;
        return this.get(url);
    }

    /**
     * get countries form server
     */
    getCountries() {
        let url = this.getBaseUrl() + this.countries_api;
        return this.get(url);
    }

    /**
     * logout with session
     * @return {Promise}
     */
    logout() {
        return this.get(this.getBaseUrl() + this.logout_api);
    }

    /**
     * get path api change information
     * @return {string}
     */
    getPathChangeInfomation() {
        return this.change_information_api;
    }
}

/**
 * @type {RestUser}
 */
export default DataResourceFactory.get(RestUser);
