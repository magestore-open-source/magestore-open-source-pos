/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";
import QueryService from "../../../service/QueryService";

export class RestCatalogRuleProductPrice extends RestAbstract {
    static className = 'RestCatalogRuleProductPrice';

    get_list_api = "/V1/webpos/catalogrule/product_price/sync";
    get_catalog_rule_product_price_ids_api = "/V1/webpos/catalogrule/product_price/get_all_ids";

    /**
     * get catalog rule product price ids
     * @param queryService
     * @param isSync
     * @returns {Promise<any>}
     */
    getIds(queryService = {}, isSync = false) {
        let queryParams = this.getQueryParams(Object.assign({}, queryService));
        let opts = {};
        if (isSync) {
            opts.credentials = 'omit';
        }
        return this.get(this.getBaseUrl()
            + this.get_catalog_rule_product_price_ids_api
            + '?' + encodeURI(queryParams.join('&')), opts);
    }

    /**
     * get catalog rule product price for products
     * @param productIds
     * @returns {Promise<void>}
     */
    getCatalogRulePriceProducts(productIds) {
        let queryService = Object.assign({}, QueryService);
        queryService.reset();
        queryService.addFieldToFilter('product_id', productIds, 'in');
        return new Promise((resolve, reject) => {
            this.getList(queryService).then(response => {
                let catalogRulePrices = {};
                if (response.items && response.items.length > 0) {
                    response.items.map(item => {
                        if (!catalogRulePrices[item.product_id]) {
                            catalogRulePrices[item.product_id] = [];
                        }
                        catalogRulePrices[item.product_id].push(item);
                        return item;
                    });
                }
                resolve(catalogRulePrices);
            }).catch(error => {
                resolve({});
            })
        })
    }
}

/**
 * @type {RestCatalogRuleProductPrice}
 */
export default DataResourceFactory.get(RestCatalogRuleProductPrice);
