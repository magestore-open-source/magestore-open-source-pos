/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import QueryService from "../../../service/QueryService";
import Singleton from "../../../resource-model/Singleton";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";

export class RestProduct extends RestAbstract {
    static className = 'RestProduct';
    get_list_api = "/V1/webpos/products/sync";
    search_api = "/V1/webpos/products/search";
    get_deleted_api = "/V1/webpos/products/deleted";
    get_by_id_api = "/V1/webpos/products";

    get_product_option = "/V1/webpos/options";
    search_barcode_product_api = "/V1/webpos/products/barcode";

    OmcStock = Singleton.getOnline('Stock');

    /**
     * Request api get option product
     *
     * @param productId
     * @return {Promise<any>}
     */
    getOptions(productId) {
        return this.get(this.getBaseUrl() + this.get_product_option + '?product_id=' + productId);
    }

    /**
     * Request api get option product
     *
     * @param productId
     * @return {Promise<any>}
     */
    getOptionsAndStockChildrens(productId) {
        return new Promise((resolve, reject) => {
            this.get(this.getBaseUrl() + this.get_product_option + '?product_id=' + productId).then(response => {
                let productIds = this.getProductIdsFromGetOptionsResponse(response);
                if (productIds.length > 0) {
                    this.OmcStock.getStockProducts(productIds).then(stocks => {
                        response.stocks = stocks;
                        resolve(response);
                    })
                } else {
                    resolve(response);
                }
            });
        });
    }

    /**
     * get product ids from option list
     *
     * @param response
     * @returns {Array}
     */
    getProductIdsFromGetOptionsResponse(response) {
        let productIds = [];
        if (response.config_option && response.config_option.length > 0) {
            response.config_option.map(option => {
                if (option.options && option.options.length > 0) {
                    option.options.map(item => {
                        if (item.products && item.products.length > 0) {
                            item.products.map(product => productIds.push(product.id));
                        }
                        return item;
                    });
                }
                return option;
            })
        }
        return productIds;
    }

    /**
     * Get stock products from product ids
     *
     * @param productIds
     */
    getStockItemsToRefund(productIds) {
        let queryService = Object.assign({}, QueryService);
        queryService.reset();
        queryService.addFieldToFilter('stock_item_index.product_id', productIds, 'in');
        return new Promise((resolve, reject) => {
            this.OmcStock.getStockItemsToRefund(queryService).then(response => {
                let stocks = {};
                if (response.items && response.items.length > 0) {
                    response.items.map(stock => {
                        if (!stocks[stock.product_id]) {
                            stocks[stock.product_id] = [];
                        }
                        stocks[stock.product_id].push(stock);
                        return stock;
                    });
                }
                resolve(stocks);
            }).catch(error => {
                resolve({});
            })
        })
    }

    /**
     * Search product by barcode
     * @param code
     * @returns {Promise<any>}
     */
    searchByBarcode(code) {
        let queryService = Object.assign({}, QueryService);
        queryService.reset();
        queryService.addQueryString(code);
        queryService.setPageSize(16);
        queryService.setCurrentPage(1);
        let queryParams = this.getQueryParams(queryService);
        return this.get(this.getBaseUrl() + this.search_barcode_product_api + '?' + encodeURI(queryParams.join('&')));
    }
}

/**
 * @type {RestProduct}
 */
export default DataResourceFactory.get(RestProduct);

