/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import SyncConstant from "../../../view/constant/SyncConstant";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";

export class RestActionLog extends RestAbstract {
    static className = 'RestActionLog';

    /**
     * request action log with type
     * @param api_url
     * @param method
     * @param params
     * @returns {Promise.<any>}
     */
    requestActionLog(api_url, method, params) {
        let url = this.getBaseUrl() + api_url;
        if (method === SyncConstant.METHOD_POST) {
            return this.post(url, params);
        } else if(method === SyncConstant.METHOD_PUT) {
            return this.put(url, params);
        } else if (method === SyncConstant.METHOD_DELETE) {
            return this.delete(url, params);
        }
    }
}

/**
 * @type {RestActionLog}
 */
export default DataResourceFactory.get(RestActionLog);