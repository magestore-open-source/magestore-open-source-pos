/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";

export class RestOrder extends RestAbstract {
    static className = 'RestOrder';

    get_list_api = "/V1/webpos/orders/sync";
    search_api = "/V1/webpos/orders/search";
    get_deleted_api = "/V1/webpos/orders/deleted";
    load_order_by_increment_api = "/V1/webpos/load-order-by-increment-id/";
    place_order_api = "/V1/webpos/checkout/placeOrder";
    order_take_payment_api = "/V1/webpos/order/takePayment";
    order_create_creditmemo_api = "/V1/webpos/creditmemos/create";
    hold_order_api = "/V1/webpos/order/hold";
    unhold_order_api = "/V1/webpos/order/unhold";
    delete_order_api = "/V1/webpos/order/delete";
    get_deleted_order_api = "/V1/webpos/orders/deleted";
    send_email_order_api = "/V1/webpos/order/sendEmail";
    add_comment_order_api = "/V1/webpos/order/comment";
    cancel_order_api = "/V1/webpos/order/cancel";
    send_email_creditmemo_api = "/V1/webpos/creditmemos/sendEmail";
    creditmemo_create_customer = "/V1/webpos/creditmemos/createCustomer";
    get_list_order_status_api = "/V1/webpos/orders/statuses";
    get_out_of_permission_orders_api = "/V1/webpos/orders/out-of-permission";

    /**
     * get path api place order
     * @return {string}
     */
    getPathPlaceOrder() {
        return this.place_order_api;
    }

    /**
     * get path take payment
     * return string
     */
    getPathTakePayment() {
        return this.order_take_payment_api;
    }

    /**
     * Get Path create creditmemo
     */
    getPathCreateCreditmemo() {
        return this.order_create_creditmemo_api;
    }

    /**
     * get path api hold order
     * @return {string}
     */
    getPathHoldOrder() {
        return this.hold_order_api;
    }

    /**
     * get path api hold order
     * @return {string}
     */
    getPathUnHoldOrder() {
        return this.unhold_order_api;
    }

    /**
     * get path api delete order
     * @return {string}
     */
    getPathDeleteOrder() {
        return this.delete_order_api;
    }

    /**
     * get path api send email
     * @return {string}
     */
    getPathSendEmailOrder() {
        return this.send_email_order_api;
    }

    /**
     * get path api add comment
     * @return {string}
     */
    getPathAddComentOrder() {
        return this.add_comment_order_api;
    }

    /**
     * get path api cancel order
     * @return {string}
     */
    getPathCancelOrder() {
        return this.cancel_order_api;
    }

    /**
     * get path api send email credit memo
     * @return {string}
     */
    getPathSendEmailCreditmemo() {
        return this.send_email_creditmemo_api;
    }

    /**
     * get path api credit memo create customer
     * @return {string}
     */
    getPathCreditmemoCreateCustomer() {
        return this.creditmemo_create_customer;
    }

    /**
     * get path api load order
     * @return {string}
     */
    getPathLoadOrder() {
        return this.getBaseUrl() + this.load_order_by_increment_api;
    }

    /**
     * get list order statuses
     * @return {Promise<any>}
     */
    getListOrderStatuses() {
        return this.get(this.getBaseUrl() + this.get_list_order_status_api);
    }

    /**
     * get out of permission orders
     * @param queryService
     * @param isSync
     * @returns {Promise<any>}
     */
    getOutOfPermissionOrders(queryService = {}, isSync = false) {
        let queryParams = this.getQueryParams(Object.assign({}, queryService));
        let opts = {};
        if (isSync) {
            opts.credentials = 'omit';
        }
        return this.get(this.getBaseUrl()
            + this.get_out_of_permission_orders_api
            + '?' + encodeURI(queryParams.join('&')), opts);
    }

    /**
     * reload order
     * @param orderIncrement
     * @return {Promise<any>}
     */
    loadOrderByIncrement(orderIncrement) {
        return this.get(this.getPathLoadOrder()
            + '?increment_id=' + orderIncrement);
    }
}

/**
 * @type {RestOrder}
 */
export default DataResourceFactory.get(RestOrder);
