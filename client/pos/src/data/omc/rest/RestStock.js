/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import RestAbstract from "./RestAbstract";
import QueryService from "../../../service/QueryService";
import DataResourceFactory from "../../../framework/factory/DataResourceFactory";

export class RestStock extends RestAbstract {
    static className = 'RestStock';
    get_list_api = "/V1/webpos/stocks/sync";
    get_available_qty_api = "/V1/webpos/availableQty";
    get_stock_items_to_refund_api = "/V1/webpos/stocks/getStockItemsToRefund";

    /**
     * get available qty from server
     * @param productId
     * @returns {Promise<any>}
     */
    getQty(productId) {
        let url = this.getBaseUrl() + this.get_available_qty_api;
        if (productId) {
            url += '?product_id=' + productId;
        }
        return this.get(url);
    }

    /**
     * Get list via api
     *
     * @param {object} queryService
     * @return {Promise<any>}
     */
    getStockItemsToRefund(queryService = {}) {
        let query = Object.assign({}, queryService);
        let queryParams = this.getQueryParams(query);
        return this.get(this.getBaseUrl() + this.get_stock_items_to_refund_api
            + '?' + encodeURI(queryParams.join('&')));
    }

    /**
     * Get stock products from product ids
     *
     * @param productIds
     */
    getStockProducts(productIds) {
        let queryService = Object.assign({}, QueryService);
        queryService.reset();
        queryService.addFieldToFilter('stock_item_index.product_id', productIds, 'in');
        return new Promise((resolve, reject) => {
            this.getList(queryService).then(response => {
                let stocks = {};
                if (response.items && response.items.length > 0) {
                    response.items.map(stock => {
                        if (!stocks[stock.product_id]) {
                            stocks[stock.product_id] = [];
                        }
                        stocks[stock.product_id].push(stock);
                        return stock;
                    });
                }
                resolve(stocks);
            }).catch(error => {
                resolve({});
            })
        })
    }
}

/**
 * @type {RestStock}
 */
export default DataResourceFactory.get(RestStock);
