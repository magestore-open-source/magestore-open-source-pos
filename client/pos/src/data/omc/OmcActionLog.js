/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import OmcAbstract from "./OmcAbstract";
import ApiResourceConstant from "../../view/constant/data-resource/ApiResourceConstant";
import RestActionLog from "./rest/RestActionLog";
import GraphqlActionLog from "./graphql/GraphqlActionLog";

export default class OmcActionLog extends OmcAbstract {
    static className = 'OmcActionLog';

    apiResources = {
        [ApiResourceConstant.REST]: RestActionLog,
        [ApiResourceConstant.GRAPHQL]: GraphqlActionLog,
    };

    /**
     * request action log with type
     * @param api_url
     * @param method
     * @param params
     * @returns {Promise.<any>}
     */
    requestActionLog(api_url, method, params) {
        return this.getApiResource().requestActionLog(api_url, method, params);
    }
}