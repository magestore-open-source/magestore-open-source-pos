/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import OmcAbstract from "./OmcAbstract";
import ApiResourceConstant from "../../view/constant/data-resource/ApiResourceConstant";
import RestCatalogRuleProductPrice from "./rest/RestCatalogRuleProductPrice";
import GraphqlCatalogRuleProductPrice from "./graphql/GraphqlCatalogRuleProductPrice";

if (!window.Promise) {
    window.Promise = Promise;
}

export default class OmcCatalogRuleProductPrice extends OmcAbstract {
    static className = 'OmcCatalogRuleProductPrice';

    apiResources = {
        [ApiResourceConstant.REST]: RestCatalogRuleProductPrice,
        [ApiResourceConstant.GRAPHQL]: GraphqlCatalogRuleProductPrice
    };

    /**
     * get catalog rule product price ids
     * @param queryService
     * @param isSync
     * @returns {Promise<any>}
     */
    getIds(queryService = {}, isSync = false) {
        return this.getRestResource().getIds(queryService, isSync);
    }

    /**
     * get catalog rule product price for products
     * @param productIds
     * @returns {Promise<void>}
     */
    getCatalogRulePriceProducts(productIds) {
        return this.getRestResource().getCatalogRulePriceProducts(productIds);
    }
}

