/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {combineEpics} from 'redux-observable';
import imports from './imports.js'
import config from './config'

const list = [];

const Config = config();
Config.module && Config.module.forEach(module => {
  try {
    list.push(require(`./${module}/view/epic`))
  } catch (e) {
    return null;
  }
});

/**
 *   add async into root epic
 */
export default class ExtensionEpic {
  static loadExtensionEpic(rootEpic$) {
    if (!list.length) return;
    return new Promise(() => {
      imports(...list).then((result) => {
        rootEpic$.next(combineEpics(...result))
      }).catch(() => {
      });

    })
  }
}
