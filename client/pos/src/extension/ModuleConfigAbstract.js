/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import PropTypes from 'prop-types'
import CoreContainer from "../framework/container/CoreContainer";
import CoreComponent from "../framework/component/CoreComponent";

class Container extends CoreContainer{}

class RewriteClass {}

function reducer(state = {}) { return state }

function anotherReducer(state = {}) { return state }

export default class ModuleConfigAbstract {
    module = ['name_module'];
    menu = {
        id_module: {
            "id": "id_module",
            "title": "Title in menu",
            "path": "unique key in here ...",
            "component": Container.withRouter(CoreComponent),
            "isEmbedded": false,
            "className": "icon class name",
            "sortOrder": 0
        }
    };
    reducer = { reducer , anotherReducer};
    rewrite = {
        service: {
            "NeededRewriteService": RewriteClass,
        },
        container: {
            "NeededRewriteContainer": RewriteClass,
        },
        component: {
            "NeededRewriteComponent": RewriteClass,
        }
    };
}

ModuleConfigAbstract.propTypes = {
    module: PropTypes.array.isRequired,
    menu: PropTypes.object,
    reducer: PropTypes.object,
    rewrite: PropTypes.shape({
        component: PropTypes.object,
        container: PropTypes.object,
        service: PropTypes.object,
    }),
};