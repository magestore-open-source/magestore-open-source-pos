/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import deepmerge from "../framework/Merge";
// IMPORT_LINES
// IMPORT_LINES

/**
 * Collect all config.js each extension module
 *
 * @return {*}
 */
function getConfig() {
    return deepmerge.all([
        {},
        {},
        // MODULE_LINES
        // MODULE_LINES
    ])
}

let cachedConfig = getConfig();

/**
 *
 * cache config
 *
 * @return {*}
 */
export default () => {
    if (!cachedConfig) {
        cachedConfig = getConfig()
    }

    return cachedConfig
}

export function updateConfig(newConfig) {
    cachedConfig = newConfig;
}
