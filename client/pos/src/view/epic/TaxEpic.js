/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import {combineEpics} from 'redux-observable';
import ConfigConstant from "../constant/tax/ConfigConstant";
import SyncService from "../../service/sync/SyncService";
import TaxService from "../../service/tax/TaxService";
import TaxAction from "../action/TaxAction";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * get tax rate action
 * @param action$
 * @returns {Observable<any>}
 */
function GetTaxRateEpic(action$) {
    return action$.ofType(ConfigConstant.GET_TAX_RATE_ONLINE)
        .mergeMap(() => Observable.from(SyncService.getTaxRate())
            .mergeMap((response) => {
                TaxService.saveTaxRateToConfig(response.items);
                TaxService.saveTaxRateToDb(response.items);
                return [
                    TaxAction.getTaxRateResult(response.items)
                ];
            }).catch(error => {
                return Observable.of(TaxAction.getTaxRateError(error));
            })
        );
}

GetTaxRateEpic.className = 'GetTaxRateEpic';

/**
 * get tax rate action
 * @param action$
 * @returns {Observable<any>}
 */
function GetTaxRuleEpic(action$) {
    return action$.ofType(ConfigConstant.GET_TAX_RULE_ONLINE)
        .mergeMap(() => Observable.from(SyncService.getTaxRule())
            .mergeMap((response) => {
                TaxService.saveTaxRuleToConfig(response.items);
                TaxService.saveTaxRuleToDb(response.items);
                return [
                    TaxAction.getTaxRuleResult(response.items)
                ];
            }).catch(error => {
                return Observable.of(TaxAction.getTaxRuleError(error));
            })
        );
}

GetTaxRuleEpic.className = 'GetTaxRuleEpic';

/**
 * Export combine epics
 *
 * @type {Epic<Action, any, any, Action> | (function(*): Observable<any>)}
 */
export const taxEpic = combineEpics(
    EpicFactory.get(GetTaxRateEpic),
    EpicFactory.get(GetTaxRuleEpic)
);

export default taxEpic;



