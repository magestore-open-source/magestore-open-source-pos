/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import ActionLogAction from "../../action/ActionLogAction";
import OnHoldOrderService from "../../../service/sales/OnHoldOrderService";
import OnHoldOrderConstant from "../../constant/OnHoldOrderConstant";
import EpicFactory from "../../../framework/factory/EpicFactory";

function DeleteHoldOrderEpic(action$) {
    return action$.ofType(OnHoldOrderConstant.DELETE_ON_HOLD_ORDER)
        .mergeMap(action => Observable.from(OnHoldOrderService.deleteOrder(action.order))
            .mergeMap(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            }).catch(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            })
        );
}

DeleteHoldOrderEpic.className = 'DeleteHoldOrderEpic';

export default EpicFactory.get(DeleteHoldOrderEpic);
