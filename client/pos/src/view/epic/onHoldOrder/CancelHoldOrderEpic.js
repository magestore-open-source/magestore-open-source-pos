/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import ActionLogAction from "../../action/ActionLogAction";
import OnHoldOrderService from "../../../service/sales/OnHoldOrderService";
import OnHoldOrderConstant from "../../constant/OnHoldOrderConstant";
import OnHoldOrderAction from "../../action/OnHoldOrderAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

function CancelHoldOrderEpic(action$) {
    return action$.ofType(OnHoldOrderConstant.CANCEL_ON_HOLD_ORDER)
        .mergeMap(action => Observable.from(OnHoldOrderService.cancelOrder(action.order))
            .mergeMap((response) => {
                return [
                    OnHoldOrderAction.syncDeletedHoldOrderFinish([action.order.increment_id]),
                    OnHoldOrderAction.cancelOrderAfter(response, action.history),
                    ActionLogAction.syncActionLog()
                ];
            }).catch(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            })
        );
}

CancelHoldOrderEpic.className = 'CancelHoldOrderEpic';

export default EpicFactory.get(CancelHoldOrderEpic);
