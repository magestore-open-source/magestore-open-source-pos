/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ColorSwatchAction from "../action/ColorSwatchAction";
import LoadingAction from "../action/LoadingAction";
import {Observable} from 'rxjs';
import ColorSwatchService from "../../service/config/ColorSwatchService";
import SyncService from "../../service/sync/SyncService";
import ColorSwatchConstant from "../constant/ColorSwatchConstant";
import AppStore from "../store/store";
import ErrorLogService from "../../service/sync/ErrorLogService";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * Receive action type(GET_COLOR_SWATCH) and request, response data color swatch
 * @param action$
 * @returns {Observable<* | type | configs | {type: string, configs: (module.exports.configs|{recommended, all})}>}
 */
function GetColorSwatchEpic(action$) {
    let requestTime = 0;
    let loadingErrorLogs = {};
    return action$.ofType(ColorSwatchConstant.GET_COLOR_SWATCH)
        .mergeMap((action) => {
            requestTime++;
            return Observable.from(SyncService.getColorSwatch())
                .mergeMap((response) => {
                    ColorSwatchService.saveToLocalStorage(response.items);

                    requestTime = 0;
                    if (action.atLoadingPage) {
                        AppStore.dispatch(LoadingAction.updateFinishedList(ColorSwatchConstant.TYPE_GET_COLOR_SWATCH));
                    }
                    return [
                        ColorSwatchAction.getColorSwatchResult(response.items)
                    ];
                }).catch(error => {
                    let message = "Failed to get color swatch data. Please contact technical support.";
                    ErrorLogService.handleLoadingPageErrors(
                        error,
                        ColorSwatchConstant.TYPE_GET_COLOR_SWATCH,
                        loadingErrorLogs,
                        requestTime,
                        action,
                        message
                    );
                    return Observable.of(ColorSwatchAction.getColorSwatchError(error));
                })
        });
}

GetColorSwatchEpic.className = 'GetColorSwatchEpic';

export default EpicFactory.get(GetColorSwatchEpic);
