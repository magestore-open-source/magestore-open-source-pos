/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ExportDataPopupAction from "../action/ExportDataPopupAction";
import ErrorLogService from "../../service/sync/ErrorLogService";
import MenuConstant from "../constant/MenuConstant";
import ActionLogService from "../../service/sync/ActionLogService";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * Send request export whenever user click yes on export data confirmation
 *
 * @param action$
 * @return {Observable<any>}
 * @constructor
 */
function ExportDataPopupEpic(action$) {
    return action$.ofType(MenuConstant.CLICK_EXPORT_ITEM)
        .mergeMap(async () => {
            try {
                let error_log = await ErrorLogService.getAllDataErrorLog();
                let action_log = await ActionLogService.getListRequestPlaceOrder();
                let all_request = {error_log: error_log.items, action_log: action_log.items};
                return ExportDataPopupAction.finishExportDataRequesting(all_request);
            } catch (e) {
                return ExportDataPopupAction.finishExportDataRequesting({ok: true});
            }
        });
}

ExportDataPopupEpic.className = 'ExportDataPopupEpic';

export default EpicFactory.get(ExportDataPopupEpic);