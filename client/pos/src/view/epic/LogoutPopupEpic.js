/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from "rxjs/Rx";
import LogoutPopupAction from "../action/LogoutPopupAction";
import LogoutPopupConstant from "../constant/LogoutPopupConstant";
import UserService from "../../service/user/UserService";
import {combineEpics} from 'redux-observable';
import CurrencyHelper from "../../helper/CurrencyHelper";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * send request logout whenever user click yes on logout confirmation
 *
 * @param action$
 * @return {Observable<any>}
 * @constructor
 */
function ProcessLogoutEpic(action$) {
    return action$.ofType(LogoutPopupConstant.CLICK_MODAL_YES)
        .mergeMap(() => Observable.from(UserService.logout())
            .map((response) => {
                /* remove sharing account */
                UserService.setSharingAccount('');
                /* remove currency data */
                CurrencyHelper.reset();
                return LogoutPopupAction.finishLogoutRequesting(response)
            }).catch(() => {
                return Observable.of(LogoutPopupAction.finishLogoutRequesting({ ok: true }));
            })
        );
}

ProcessLogoutEpic.className = 'ProcessLogoutEpic';

/**
 * send request logout whenever user was forced sign out
 *
 * @param action$
 * @return {Observable<any>}
 * @constructor
 */
function ForceSignOutEpic(action$) {
    return action$.ofType(LogoutPopupConstant.FORCE_SIGN_OUT)
        .mergeMap(() => {
                UserService.logout();
                return Observable.empty();
            }
        );
}

ForceSignOutEpic.className = 'ForceSignOutEpic';

/**
 * export combine epics
 *
 * @type {Epic<Action, any, any, Action> | (function(*): Observable<any>)}
 */
export const LogoutPopupEpic = combineEpics(
    EpicFactory.get(ProcessLogoutEpic),
    EpicFactory.get(ForceSignOutEpic)
);

export default LogoutPopupEpic;