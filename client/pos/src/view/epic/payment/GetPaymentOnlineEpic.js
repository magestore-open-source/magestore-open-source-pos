/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import PaymentConstant from '../../constant/PaymentConstant';
import LoadingAction from "../../action/LoadingAction";
import {Observable} from 'rxjs';
import PaymentService from "../../../service/payment/PaymentService";
import SyncService from "../../../service/sync/SyncService";
import AppStore from "../../store/store";
import ErrorLogService from "../../../service/sync/ErrorLogService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Get payment online epic
 * @param action$
 * @returns {Observable<any>}
 */
function GetPaymentOnlineEpic(action$) {
    let requestTime = 0;
    let loadingErrorLogs = {};
    return action$.ofType(PaymentConstant.GET_PAYMENT_ONLINE)
        .mergeMap((action) => {
            requestTime++;
            return Observable.from(SyncService.getPayment())
                .mergeMap((response) => {
                    PaymentService.clear().then(() => {
                        PaymentService.saveToDb(response.items);
                    });

                    requestTime = 0;
                    if (action.atLoadingPage) {
                        AppStore.dispatch(LoadingAction.updateFinishedList(PaymentConstant.TYPE_GET_PAYMENT));
                    }
                    return [];
                }).catch(error => {
                    let message = "Failed to get payment data. Please contact technical support.";
                    ErrorLogService.handleLoadingPageErrors(
                        error,
                        PaymentConstant.TYPE_GET_PAYMENT,
                        loadingErrorLogs,
                        requestTime,
                        action,
                        message
                    );
                    return Observable.empty();
                })
        });
}

GetPaymentOnlineEpic.className = 'GetPaymentOnlineEpic';

export default EpicFactory.get(GetPaymentOnlineEpic);
