/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import PaymentAction from "../../action/PaymentAction";
import PaymentConstant from '../../constant/PaymentConstant';
import {Observable} from 'rxjs';
import PaymentService from "../../../service/payment/PaymentService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Get payment list 
 * 
 * @param action$
 * @returns {Observable<any>}
 */
function GetListPaymentEpic(action$) {
    return action$.ofType(PaymentConstant.GET_LIST_PAYMENT)
        .mergeMap(() => Observable.from(PaymentService.getAll())
            .mergeMap((response) => {
                return Observable.of(PaymentAction.getListPaymentResult(response));
            }).catch(() => {
                return Observable.empty();
            })
        );
}

GetListPaymentEpic.className = 'GetListPaymentEpic';

export default EpicFactory.get(GetListPaymentEpic);
