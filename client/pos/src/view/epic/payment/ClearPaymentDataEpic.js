/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import PaymentConstant from '../../constant/PaymentConstant';
import {Observable} from 'rxjs';
import PaymentService from "../../../service/payment/PaymentService";
import EpicFactory from "../../../framework/factory/EpicFactory";

function ClearPaymentDataEpic(action$) {
    return action$.ofType(PaymentConstant.CLEAR_DATA)
        .mergeMap(() => Observable.from(PaymentService.clear())
            .mergeMap(() => {
                return Observable.empty();
            }).catch(() => {
                return Observable.empty();
            })
        );
}

ClearPaymentDataEpic.className = 'ClearPaymentDataEpic';

export default EpicFactory.get(ClearPaymentDataEpic);
