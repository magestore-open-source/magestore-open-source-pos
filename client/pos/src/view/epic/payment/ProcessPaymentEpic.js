/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import PaymentConstant from "../../constant/PaymentConstant";
import CheckoutConstant from "../../constant/CheckoutConstant";
import OrderConstant from "../../constant/OrderConstant";
import {Observable} from 'rxjs';
import EpicFactory from "../../../framework/factory/EpicFactory";

import {combineEpics} from "redux-observable";
import CashEpic from "./type/CashEpic";
import CreditCardEpic from "./type/CreditCardEpic";
import PaymentHelper from "../../../helper/PaymentHelper";
import AppStore from "../../../view/store/store";
import PaymentAction from "../../action/PaymentAction";
import CreditmemoConstant from "../../constant/order/CreditmemoConstant";

/**
 * checkout place order epic
 * @param action$
 * @returns {*}
 */
function ProcessPaymentEpic(action$) {
    return action$.ofType(
        CheckoutConstant.CHECKOUT_PROCESS_PAYMENT,
        CreditmemoConstant.CREDITMEMO_PROCESS_PAYMENT,
        OrderConstant.TAKE_PAYMENT_PROCESS_PAYMENT
    )
        .mergeMap(action => Observable.from(Promise.resolve())
            .mergeMap(() => {
                const object    = action.quote || action.order || action.creditmemo;
                let listEpic    = [];
                object.payments = object.payments.map((payment, index) => {
                    if (!payment.status) {
                        return payment;
                    }

                    if (PaymentHelper.PROCESSED_STATUS.indexOf(payment.status) !== -1) {
                        return payment;
                    }

                    if (payment.status === PaymentConstant.PROCESS_PAYMENT_PROCESSING) {
                        return payment;
                    }

                    let existedPending = listEpic.find(
                        epic => PaymentHelper.hasUsingTerminal(epic.payment.method)
                            || PaymentHelper.hasUsingEWallet(epic.payment.method)
                    );
                    if (existedPending) return payment;

                    if (payment.status === PaymentConstant.PROCESS_PAYMENT_PENDING) {
                        delete payment['errorMessage'];
                        AppStore.dispatch(PaymentAction.paymentStatusToProcessing(payment, index));
                    }

                    payment.status = PaymentConstant.PROCESS_PAYMENT_PROCESSING;
                    listEpic.push(PaymentAction.startProcessSinglePayment(payment, index, object));
                    return payment;
                });

                return [
                    ...listEpic
                ];
            })
        );
}

ProcessPaymentEpic.className = 'ProcessPaymentEpic';


/**
 * Combine all payment epic
 * @type {Epic<Action, any, any, T> | any}
 */
const paymentEpic = combineEpics(
    CashEpic,
    CreditCardEpic,
    EpicFactory.get(ProcessPaymentEpic)
);

export default paymentEpic;