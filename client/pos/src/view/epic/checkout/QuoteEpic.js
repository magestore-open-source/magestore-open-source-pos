/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {combineEpics} from 'redux-observable';
import AddProductEpic from "./quote/AddProductEpic";
import RemoveCartItemEpic from "./quote/RemoveCartItemEpic";
import UpdateQtyCartItemEpic from "./quote/UpdateQtyCartItemEpic";
import UpdateCustomPriceCartItemEpic from "./quote/UpdateCustomPriceCartItemEpic";
import ChangeCustomerEpic from "./quote/ChangeCustomerEpic";
import SaveCustomerAfterEpic from "./quote/SaveCustomerAfterEpic";
import CreateCustomerAfterEpic from "./quote/CreateCustomerAfterEpic";
import RemoveCouponCodeEpic from "./quote/RemoveCouponCodeEpic";
import ValidateQuoteSalesRuleEpic from "./quote/ValidateQuoteSalesRuleEpic";
import ResetQuoteEpic from "./quote/ResetQuoteEpic";
import CheckoutToCatalogEpic from "./quote/CheckoutToCatalogEpic";
import SetCartCustomDiscountEpic from "./quote/SetCartCustomDiscountEpic";
import RemoveCartCustomDiscountEpic from "./quote/RemoveCartCustomDiscountEpic";

/**
 * Combine all product epic
 * @type {Epic<Action, any, any, T> | any}
 */
const quoteEpic = combineEpics(
    AddProductEpic,
    RemoveCartItemEpic,
    UpdateQtyCartItemEpic,
    UpdateCustomPriceCartItemEpic,
    ChangeCustomerEpic,
    SaveCustomerAfterEpic,
    CreateCustomerAfterEpic,
    RemoveCouponCodeEpic,
    ValidateQuoteSalesRuleEpic,
    ResetQuoteEpic,
    CheckoutToCatalogEpic,
    SetCartCustomDiscountEpic,
    RemoveCartCustomDiscountEpic
);

export default quoteEpic;


