/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import CheckoutConstant from "../../constant/CheckoutConstant";
import CheckoutService from "../../../service/checkout/CheckoutService";
import CheckoutAction from "../../action/CheckoutAction";
import ActionLogAction from "../../action/ActionLogAction";
import OrderAction from "../../action/OrderAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Checkout place order epic
 *
 * @param action$
 * @returns {*}
 */
function PlaceOrderEpic(action$) {
    return action$.ofType(CheckoutConstant.CHECK_OUT_PLACE_ORDER)
        .mergeMap(action => Observable.from(CheckoutService.placeOrder(action.quote, action.additionalData))
            .mergeMap((response) => {
                return [
                    CheckoutAction.placeOrderResult(response),
                    OrderAction.placeOrderAfter(response, action.quote),
                    ActionLogAction.syncActionLog()
                ];
            }).catch(error => {
                return [
                    CheckoutAction.placeOrderError(error),
                    ActionLogAction.syncActionLog()
                ];
            })
        );
}

PlaceOrderEpic.className = 'PlaceOrderEpic';

export default EpicFactory.get(PlaceOrderEpic);
