/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteService from '../../../../service/checkout/QuoteService';
import CheckoutConstant from "../../../constant/CheckoutConstant";
import QuoteAction from '../../../action/checkout/QuoteAction';
import {Observable} from 'rxjs';
import EpicFactory from "../../../../framework/factory/EpicFactory";
import {fire} from "../../../../event-bus";

function CheckoutToCatalogEpic(action$, store) {
    return action$.ofType(CheckoutConstant.CHECKOUT_TO_CATALOG)
        .mergeMap(() => {
                try {
                    let quote = {
                        ...store.getState().core.checkout.quote,
                        valid_salesrule: null,
                        coupon_code: null
                    };

                    fire('epic_checkout_to_catalog_collect_quote_totals_before', {quote});

                    quote = QuoteService.collectTotals(quote);
                    return Observable.of(QuoteAction.setQuote(quote));
                } catch (e) {
                    return Observable.empty();
                }
            }
        );
}

CheckoutToCatalogEpic.className = "CheckoutToCatalogEpic";

export default EpicFactory.get(CheckoutToCatalogEpic);
