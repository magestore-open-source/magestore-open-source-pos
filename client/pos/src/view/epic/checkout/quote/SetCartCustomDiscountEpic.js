/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import QuoteCustomDiscountService from '../../../../service/checkout/quote/CustomDiscountService';
import QuoteAction from '../../../action/checkout/QuoteAction';
import {Observable} from 'rxjs';
import CheckoutAction from "../../../action/CheckoutAction";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 *
 * @param action$
 * @param store
 * @return {Observable<any>}
 * @constructor
 */
function SetCartCustomDiscountEpic(action$, store) {
    return action$.ofType(QuoteConstant.SET_CUSTOM_DISCOUNT)
        .mergeMap(action => {
            return Observable.from(QuoteCustomDiscountService.applyCustomRule(
                store.getState().core.checkout.quote,
                action.discountType,
                action.discountAmount,
                action.discountReason)
            ).mergeMap((response) => {
                let quote = response.quote;
                let actions = [QuoteAction.setQuote(quote)];
                if (!quote.grand_total) {
                    actions.push(CheckoutAction.checkoutToSelectPayments(quote));
                }
                return actions;
            });
        });
}

SetCartCustomDiscountEpic.className = 'SetCartCustomDiscountEpic';

export default EpicFactory.get(SetCartCustomDiscountEpic);
