/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import {Observable} from "rxjs/Rx";
import QuoteService from "../../../../service/checkout/QuoteService";
import QuoteAction from "../../../action/checkout/QuoteAction";
import CheckoutAction from "../../../action/CheckoutAction";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 *
 * @param action$
 * @param store
 * @returns {Observable<any>}
 * @constructor
 */
function ValidateQuoteSalesRuleEpic(action$, store) {
    return action$.ofType(QuoteConstant.VALIDATE_QUOTE_SALES_RULE)
        .mergeMap(action => {
            let quote = action.quote;
            return Observable.fromPromise(
                QuoteService.submitCouponCode(quote, quote.coupon_code || "")
            ).pipe(response => {
                return Observable.fromPromise(response.toPromise())
                    .mergeMap(rules => {
                        quote = store.getState().core.checkout.quote;
                        quote.valid_salesrule = rules;
                        quote = QuoteService.collectTotals(quote);
                        let actions = [QuoteAction.setQuote(quote)];
                        if (!quote.grand_total) {
                            actions.push(CheckoutAction.checkoutToSelectPayments(quote));
                        }
                        return actions;
                    }).catch(() => {
                        quote = store.getState().core.checkout.quote;
                        quote.coupon_code = null;
                        return Observable.fromPromise(
                            QuoteService.submitCouponCode(quote, quote.coupon_code || "")
                        ).pipe(response => {
                            return Observable.fromPromise(response.toPromise())
                                .mergeMap(rules => {
                                    quote = store.getState().core.checkout.quote;
                                    quote.valid_salesrule = rules;
                                    quote = QuoteService.collectTotals(quote);
                                    return Observable.of(QuoteAction.setQuote(quote));
                                }).catch(() => {
                                    quote = store.getState().core.checkout.quote;
                                    quote.valid_salesrule = [];
                                    quote = QuoteService.collectTotals(quote);
                                    let actions = [QuoteAction.setQuote(quote)];
                                    if (!quote.grand_total) {
                                        actions.push(CheckoutAction.checkoutToSelectPayments(quote));
                                    }
                                    return actions;
                                });
                        });
                    });
            });
        }).catch(() => {
            return Observable.empty();
        });
}

ValidateQuoteSalesRuleEpic.className = 'ValidateQuoteSalesRuleEpic';

export default EpicFactory.get(ValidateQuoteSalesRuleEpic);
