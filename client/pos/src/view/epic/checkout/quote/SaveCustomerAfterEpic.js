/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import CustomerConstant from "../../../constant/CustomerConstant";
import QuoteService from "../../../../service/checkout/QuoteService";
import QuoteAction from "../../../action/checkout/QuoteAction";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 * Receive action type(CREATE_CUSTOMER_SUCCESS) and request
 *
 * @param action$
 * @param store
 * @return {Observable<any>}
 * @constructor
 */
function QuoteSaveCustomerAfterEpic(action$, store) {
    return action$.ofType(CustomerConstant.SAVE_CUSTOMER)
        .mergeMap(action => {
                let quote = store.getState().core.checkout.quote;
                let customers = action.customers;
                if (quote.customer && quote.customer.id) {
                    let customer = customers.find(customer => customer.email === quote.customer.email);
                    if (customer && customer.email) {
                        quote = QuoteService.changeCustomer(quote, customer);
                        quote = QuoteService.collectTotals(quote);
                        return Observable.of(QuoteAction.setQuote(quote));
                    }
                }
                return Observable.empty();
            }
        );
}

QuoteSaveCustomerAfterEpic.className = "QuoteSaveCustomerAfterEpic";

export default EpicFactory.get(QuoteSaveCustomerAfterEpic);
