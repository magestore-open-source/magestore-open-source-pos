/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import QuoteService from '../../../../service/checkout/QuoteService';
import QuoteAction from '../../../action/checkout/QuoteAction';
import { toast } from 'react-toastify'
import i18n from '../../../../config/i18n'
import EpicFactory from "../../../../framework/factory/EpicFactory";
import AppStore from "../../../store/store";

/**
 *
 * @param action$
 * @param store
 * @return {Observable<any>}
 * @constructor
 */
function UpdateCustomPriceCartItemEpic(action$, store) {
    return action$.ofType(QuoteConstant.UPDATE_CUSTOM_PRICE_CART_ITEM)
        .mergeMap(action => {
                return QuoteService.updateCustomPriceCartItem(
                    store.getState().core.checkout.quote, action.item, action.customPrice, action.reason)
                    .map(response => {
                        if (response.success) {
                            AppStore.dispatch(QuoteAction.updatedItemIdInQuote(action.item.item_id));
                            return QuoteAction.setQuote(response.quote);
                        } else {
                            toast.error(
                                i18n.translator.translate(response.message),
                                {
                                    className: 'wrapper-messages messages-warning'
                                }
                            );
                          return QuoteAction.updateCustomPriceCartItemFail(response)
                        }
                    });

            }
        );
}

UpdateCustomPriceCartItemEpic.className = 'UpdateCustomPriceCartItemEpic';

export default EpicFactory.get(UpdateCustomPriceCartItemEpic);


