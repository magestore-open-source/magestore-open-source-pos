/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import QuoteService from '../../../../service/checkout/QuoteService';
import QuoteAction from '../../../action/checkout/QuoteAction';
import MultiCheckoutAction from "../../../action/MultiCheckoutAction";
import MultiCartService from "../../../../service/MultiCartService";
import AppStore from "../../../../view/store/store";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 *
 * @param action$
 * @param store
 * @return {Observable<any>}
 */
function AddProductEpic(action$, store) {
    return action$.ofType(QuoteConstant.ADD_PRODUCT)
        .mergeMap(async action => {
            try {
                let response =
                    await QuoteService.addProduct(store.getState().core.checkout.quote, action.data).toPromise();
                if (!response.success) {
                    return {
                        type: '',
                        quote: response
                    }
                }

                /** auto create cart if empty */
                const {activeCart} = store.getState().core.multiCheckout;
                if (!activeCart) {
                    await QuoteAction.setQuote(response.quote);
                    let newCartId = await MultiCartService.addCartFromStore(store);
                    return MultiCheckoutAction.getListCart(newCartId);
                }
                AppStore.dispatch(QuoteAction.addedItemIdInQuote(response.added_item_id));
                return QuoteAction.setQuote(response.quote);
            } catch (e) {
                return {
                    type: '',
                    quote: store.getState().core.checkout.quote
                }
            }
        });
}

AddProductEpic.className = "AddProductEpic";

export default EpicFactory.get(AddProductEpic);