/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import QuoteService from '../../../../service/checkout/QuoteService';
import QuoteAction from "../../../action/checkout/QuoteAction";
import MultiCartService from "../../../../service/MultiCartService";
import MultiCheckoutAction from "../../../action/MultiCheckoutAction";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 *
 * @param action$
 * @param store
 * @return {Observable<any>}
 * @constructor
 */
function ChangeCustomerEpic(action$, store) {
    return action$.ofType(QuoteConstant.SET_CUSTOMER)
        .mergeMap(async action => {
            const oldQuote  = store.getState().core.checkout.quote;
            let quote = QuoteService.changeCustomer(oldQuote, action.customer);
            quote = QuoteService.collectTotals(quote);
            /** auto create cart if empty */
            const {activeCart} = store.getState().core.multiCheckout;
            if (!activeCart) {
                await QuoteAction.setQuote(quote);
                let newCartId = await MultiCartService.addCartByQuote(quote);
                return MultiCheckoutAction.getListCart(newCartId);
            }
            return QuoteAction.setQuote(quote);
        });
}

ChangeCustomerEpic.className = "ChangeCustomerEpic";

export default EpicFactory.get(ChangeCustomerEpic);
