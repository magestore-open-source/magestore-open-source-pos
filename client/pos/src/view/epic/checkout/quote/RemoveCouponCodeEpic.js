/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import {Observable} from "rxjs/Rx";
import QuoteService from "../../../../service/checkout/QuoteService";
import QuoteAction from "../../../action/checkout/QuoteAction";
import CouponTypeConstant from "../../../constant/salesrule/CouponTypeConstant";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 *
 * @param action$
 * @return {Observable<any>}
 */
function RemoveCouponCode(action$) {
    return action$.ofType(QuoteConstant.REMOVE_COUPON_CODE)
        .mergeMap(action => {
            let quote = action.quote;
            quote.coupon_code = null;
            if (quote.valid_salesrule && quote.valid_salesrule.length) {
                quote.valid_salesrule = quote.valid_salesrule.filter(
                    rule => rule.coupon_type === CouponTypeConstant.COUPON_TYPE_NO_COUPON
                );
                QuoteService.collectTotals(quote);
            }
            return Observable.of(QuoteAction.setQuote(quote));
        }).catch(() => {
            return Observable.empty();
        });
}

RemoveCouponCode.className = "RemoveCouponCode";

export default EpicFactory.get(RemoveCouponCode);
