/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import QuoteService from '../../../../service/checkout/QuoteService';
import QuoteAction from '../../../action/checkout/QuoteAction';
import MultiCheckoutAction from "../../../action/MultiCheckoutAction";
import MultiCartService from "../../../../service/MultiCartService";
import EpicFactory from "../../../../framework/factory/EpicFactory";

function RemoveCartItemEpic(action$, store) {
    return action$.ofType(QuoteConstant.REMOVE_CART_ITEM)
        .mergeMap(async action => {
            try {
                let response =
                    await QuoteService.removeItem(store.getState().core.checkout.quote, action.item).toPromise();
                if (response.success) {
                    const {activeCart} = store.getState().core.multiCheckout;
                    if (!activeCart) {
                        return QuoteAction.setQuote(response.quote);
                    }
                    /** update active cart */
                    await MultiCartService.updateActiveCartFromStore(store);
                    return MultiCheckoutAction.selectCart(response.quote);
                } else {
                    return {
                        type: '',
                        quote: response
                    }
                }
            } catch (e) {
                return {
                    type: '',
                    quote: e
                }
            }
        });
}

RemoveCartItemEpic.className = "RemoveCartItemEpic";

export default EpicFactory.get(RemoveCartItemEpic);
