/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import QuoteConstant from '../../../constant/checkout/QuoteConstant';
import QuoteCustomDiscountService from '../../../../service/checkout/quote/CustomDiscountService';
import QuoteAction from '../../../action/checkout/QuoteAction';
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 *
 * @param action$
 * @param store
 * @return {Observable<any>}
 * @constructor
 */
function RemoveCartCustomDiscountEpic(action$, store) {
    return action$.ofType(QuoteConstant.REMOVE_CUSTOM_DISCOUNT)
        .mergeMap(() => {
            return QuoteCustomDiscountService.removeCustomRule(store.getState().core.checkout.quote)
                .map(response => {
                    return QuoteAction.setQuote(response.quote);
                });
        });
}

RemoveCartCustomDiscountEpic.className = "RemoveCartCustomDiscountEpic";

export default EpicFactory.get(RemoveCartCustomDiscountEpic);
