/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {combineEpics} from "redux-observable";
import LoadingConstant from '../constant/LoadingConstant';
import ProductService from "../../service/catalog/ProductService";
import LoadingAction from "../action/LoadingAction";
import SyncService from "../../service/sync/SyncService";
import SyncConstant from "../constant/SyncConstant";
import StockService from "../../service/catalog/StockService";
import OrderService from "../../service/sales/OrderService";
import CustomerService from "../../service/customer/CustomerService";
import SyncAction from "../action/SyncAction";
import Appstore from "../store/store";
import {fire} from "../../event-bus";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * Clear data of table in indexedDb
 * @param action$
 * @returns {Observable<any>}
 */
function ClearDataEpic(action$) {
    return action$.ofType(LoadingConstant.CLEAR_DATA)
        .mergeMap(async function () {
            let needSync = SyncService.getNeedSync();
            let promises = [];
            let changeDataTypeMode = {};
            if (needSync === '1') {
                // Change mode to online
                changeDataTypeMode = {
                    [SyncConstant.TYPE_PRODUCT]: SyncConstant.ONLINE_MODE,
                    [SyncConstant.TYPE_STOCK]: SyncConstant.ONLINE_MODE,
                    [SyncConstant.TYPE_ORDER]: SyncConstant.ONLINE_MODE,
                    [SyncConstant.TYPE_CATEGORY]: SyncConstant.ONLINE_MODE,
                    [SyncConstant.TYPE_CUSTOMER]: SyncConstant.ONLINE_MODE,
                };
                promises.push(
                    ProductService.clear(),
                    StockService.clear(),
                    OrderService.clear(),
                    CustomerService.clear()
                );
            }

            /* Event clear data before */
            let eventDataBefore = {
                changeDataTypeMode: changeDataTypeMode,
                promises: promises
            };
            fire('epic_loading_clear_data_before', eventDataBefore);
            changeDataTypeMode = eventDataBefore.changeDataTypeMode;
            promises = eventDataBefore.promises;

            promises.push(
                SyncService.resetData(Object.keys(changeDataTypeMode)),
            );

            if (changeDataTypeMode && Object.keys(changeDataTypeMode).length) {
                Appstore.dispatch(SyncAction.changeDataTypeMode(changeDataTypeMode));
            }

            try {
                await Promise.all(promises);
                SyncService.saveNeedSync(0);
                return LoadingAction.clearDataSuccess();
            } catch (e) {
                return LoadingAction.clearDataError(e);
            }
        });
}

ClearDataEpic.className = 'ClearDataEpic';

export const loadingEpic = combineEpics(
    EpicFactory.get(ClearDataEpic)
);

export default loadingEpic;
