/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ConfigAction from "../action/ConfigAction";
import ConfigConstant from '../constant/ConfigConstant';
import LoadingAction from "../action/LoadingAction";
import {Observable} from 'rxjs';
import ConfigService from "../../service/config/ConfigService";
import SyncService from "../../service/sync/SyncService";
import ConfigHelper from "../../helper/ConfigHelper";
import OrderConstant from "../constant/OrderConstant";
import LocalStorageHelper from "../../helper/LocalStorageHelper";
import Config from "../../config/Config";
import SearchConstant from "../constant/SearchConstant";
import SyncAction from "../action/SyncAction";
import SyncConstant from "../constant/SyncConstant";
import AppStore from "../../view/store/store";
import {toast} from "react-toastify";
import i18n from "../../config/i18n";
import CategoryService from "../../service/catalog/CategoryService";
import {fire} from "../../event-bus";
import ErrorLogService from "../../service/sync/ErrorLogService";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * Receive action type(GET_CONFIG) and request, response data config
 * @param action$
 * @returns {Observable<any>}
 */
function ConfigEpic(action$) {
    let requestTime = 0;
    let loadingErrorLogs = {};
    return action$.ofType(ConfigConstant.GET_CONFIG)
        .mergeMap(action => {
            requestTime++;
            return Observable.from(SyncService.getConfig())
                .mergeMap(async (response) => {
                    if (response.current_currency_code && response.currencies) {
                        let currentCurrency = response.currencies.find(item =>
                            item.code === response.current_currency_code
                        );
                        if (!currentCurrency) {
                            toast.error(
                                i18n.translator.translate(
                                    'Undefined currency rate from {{base_currency_code}} to {{current_currency_code}}',
                                    {
                                        base_currency_code: response.base_currency_code,
                                        current_currency_code: response.current_currency_code
                                    }
                                ),
                                {
                                    className: 'wrapper-messages messages-warning'
                                }
                            );
                            response.current_currency_code = response.base_currency_code;
                        }
                    }


                    ConfigService.saveToLocalStorage(response);

                    if (response && Array.isArray(response.settings)) {
                        let locale = response.settings.find(item =>
                            item.path === ConfigConstant.CONFIG_XML_PATH_GENERAL_LOCALE_CODE
                        );
                        if (locale) {
                            ConfigService.changeLanguage(locale.value);
                        }
                    }

                    if (Config.config) {
                        let needSyncData = false;
                        // check config sync order
                        checkSyncOrderConfig(response);

                        // check config barcode
                        let isChangeBarcodeConfig = await checkBarcodeConfig(response);
                        if (isChangeBarcodeConfig) needSyncData = true;

                        // check root category
                        let isChangeRootCategory = await checkRootCategory(response);
                        if (isChangeRootCategory) needSyncData = true;

                        if (needSyncData) {
                            AppStore.dispatch(SyncAction.syncData());
                        }
                    }

                    fire('epic_config_get_config_after', { config: response });
                    requestTime = 0;
                    if (action.atLoadingPage) {
                        AppStore.dispatch(LoadingAction.updateFinishedList(ConfigConstant.TYPE_GET_CONFIG));
                    }
                    return ConfigAction.getConfigResult(response);
                }).catch(error => {
                    let message = "Failed to get configurable data. Please contact technical support.";
                    ErrorLogService.handleLoadingPageErrors(
                        error,
                        ConfigConstant.TYPE_GET_CONFIG,
                        loadingErrorLogs,
                        requestTime,
                        action,
                        message
                    );
                    return Observable.of(ConfigAction.getConfigError(error));
                })
        });
}

ConfigEpic.className = 'GetConfigEpic';

export default EpicFactory.get(ConfigEpic);

/**
 * check sync order config
 * @param newConfig
 */
function checkSyncOrderConfig(newConfig) {
    let oldConfigOrderSince = ConfigHelper.getConfig(OrderConstant.XML_PATH_CONFIG_SYNC_ORDER_SINCE);
    let newConfigOrderSince = newConfig.settings.find(
        item => item.path === OrderConstant.XML_PATH_CONFIG_SYNC_ORDER_SINCE
    );
    if (typeof newConfigOrderSince !== "undefined") {
        newConfigOrderSince = newConfigOrderSince.value;
        if (oldConfigOrderSince !== newConfigOrderSince) {
            LocalStorageHelper.set(LocalStorageHelper.NEED_SYNC_ORDER, 1);
        }
    }
}

/**
 * check barcode config
 * @param newConfig
 * @return {Promise<boolean>}
 */
async function checkBarcodeConfig(newConfig) {
    let oldConfigBarcode = ConfigHelper.getConfig(SearchConstant.BARCODE_CONFIG);
    let newConfigBarcode = newConfig.settings.find(
        item => item.path === SearchConstant.BARCODE_CONFIG
    );
    if (typeof newConfigBarcode !== "undefined") {
        newConfigBarcode = newConfigBarcode.value;
        if (oldConfigBarcode !== newConfigBarcode) {
            await SyncService.resetData([SyncConstant.TYPE_PRODUCT]);
            return true;
        }
    }
    return false;
}

/**
 * check root category
 * @param newConfig
 * @returns {Promise<boolean>}
 */
async function checkRootCategory(newConfig) {
    if (Config.config.root_category_id !== newConfig.root_category_id) {
        await CategoryService.clear();
        let dataTypeMode = {
            [SyncConstant.TYPE_CATEGORY]: SyncConstant.ONLINE_MODE
        };
        await AppStore.dispatch(SyncAction.changeDataTypeMode(dataTypeMode));
        await SyncService.resetData([SyncConstant.TYPE_CATEGORY]);
        return true;
    }
    return false;
}
