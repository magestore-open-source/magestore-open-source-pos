/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from "rxjs";
import CategoryConstant from "../../constant/CategoryConstant";
import CategoryAction from '../../action/CategoryAction';
import CategoryService from "../../../service/catalog/CategoryService";
import Config from "../../../config/Config";
import SyncConstant from "../../constant/SyncConstant";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(GET_LIST_CATEGORY) and request, response list category
 * @param action$
 */
function GetListCategoryEpic(action$) {
    return action$.ofType(CategoryConstant.GET_LIST_CATEGORY)
        .mergeMap((action) => {
            let requestMode = Config.dataTypeMode[SyncConstant.TYPE_CATEGORY];
            return Observable.from(CategoryService.getListCategory(action.parent_id))
                .mergeMap((response) => {
                    return Observable.of(CategoryAction.getListCategoryResult(
                        response.parentCategory,
                        response.items,
                        response.search_criteria,
                        response.total_count,
                        action.parent_id,
                        requestMode
                    ));
                }).catch(() => Observable.of(CategoryAction.getListCategoryResult([])))
        });
}

GetListCategoryEpic.className = "GetListCategoryEpic";

export default EpicFactory.get(GetListCategoryEpic);
