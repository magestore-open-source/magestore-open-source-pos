/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {combineEpics} from 'redux-observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import extensionEpic from '../../extension/epics';
import configEpic from './ConfigEpic';
import categoryEpic from './CategoryEpic';
import productEpic from './ProductEpic';
import customerEpic from './CustomerEpic';
import syncEpic from './SyncEpic';
import userEpic from "./UserEpic";
import locationEpic from "./LocationEpic";
import LogoutPopupEpic from "./LogoutPopupEpic";
import exportDataPopupEpic from "./ExportDataPopupEpic";
import loadingEpic from "./LoadingEpic";
import menuEpic from "./MenuEpic";
import paymentEpic from "./PaymentEpic";
import shippingEpic from "./ShippingEpic";
import checkoutEpic from "./CheckoutEpic";
import colorSwatchEpic from "./ColorSwatchEpic";
import signoutEpic from "./SignoutEpic";
import observerEpic from "./ObserverEpic";
import orderEpic from "./OrderEpic";
import taxEpic from "./TaxEpic";
import multiCheckoutEpic from "./MultiCheckoutEpic";
import onHoldOrderEpic from "./OnHoldOrderEpic";

export default () => {
    const epic$ = new BehaviorSubject(combineEpics(
        userEpic,
        locationEpic,
        configEpic,
        categoryEpic,
        productEpic,
        customerEpic,
        syncEpic,
        LogoutPopupEpic,
        exportDataPopupEpic,
        loadingEpic,
        menuEpic,
        paymentEpic,
        shippingEpic,
        checkoutEpic,
        colorSwatchEpic,
        signoutEpic,
        observerEpic,
        orderEpic,
        taxEpic,
        multiCheckoutEpic,
        onHoldOrderEpic,
    ));

    const rootEpic = (action$, store) =>
        epic$.mergeMap(epic =>
            epic(action$, store)
        );

    extensionEpic.loadExtensionEpic(epic$);
    return rootEpic
}
