/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {combineEpics} from 'redux-observable';
import SyncDataEpic from "./sync/SyncDataEpic";
import SetDefaultSyncDBEpic from "./sync/SetDefaultSyncDBEpic";
import SyncDataWithTypeEpic from "./sync/SyncDataWithTypeEpic";
import InternetEpic from "./sync/InternetEpic";
import SyncActionLogEpic from "./sync/SyncActionLogEpic";
import ScheduleUpdateDataEpic from "./sync/ScheduleUpdateDataEpic";
import UpdateDataEpic from "./sync/UpdateDataEpic";
import ExecuteUpdateDataEpic from "./sync/ExecuteUpdateDataEpic";
import UpdateDataWithTypeEpic from "./sync/UpdateDataWithTypeEpic";
import UpdateProductLocationEpic from "./sync/UpdateProductLocationEpic";
import ReloadPageEpic from "./sync/ReloadPageEpic";

/**
 * Combine all epic sync
 * @type {Epic<Action, any, any, T> | any}
 */
const syncEpic = combineEpics(
    SetDefaultSyncDBEpic,
    SyncDataEpic,
    SyncDataWithTypeEpic,
    InternetEpic,
    SyncActionLogEpic,
    ScheduleUpdateDataEpic,
    UpdateDataEpic,
    ExecuteUpdateDataEpic,
    UpdateDataWithTypeEpic,
    UpdateProductLocationEpic,
    ReloadPageEpic
);

export default syncEpic;
