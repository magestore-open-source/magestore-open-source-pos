/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import LoadingAction from "../../action/LoadingAction";
import {Observable} from 'rxjs';
import Config from "../../../config/Config";
import OrderConstant from "../../constant/OrderConstant";
import OrderService from "../../../service/sales/OrderService";
import AppStore from "../../store/store";
import ErrorLogService from "../../../service/sync/ErrorLogService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(GET_LIST_ORDER_STATUSES) and request, response data statuses
 * @param action$
 * @returns {Observable<any>}
 */
function GetListOrderStatusesEpic(action$) {
    let requestTime = 0;
    let loadingErrorLogs = {};
    return action$.ofType(OrderConstant.GET_LIST_ORDER_STATUSES)
        .mergeMap(action => {
            requestTime++;
            return Observable.from(OrderService.getListOrderStatuses())
                .mergeMap((response) => {
                    OrderService.saveOrderStatus(response.items);
                    Config.orderStatus = response.items;

                    requestTime = 0;
                    if (action.atLoadingPage) {
                        AppStore.dispatch(LoadingAction.updateFinishedList(OrderConstant.TYPE_GET_LIST_ORDER_STATUSES));
                    }
                    return [];
                }).catch(error => {
                    let message = "Failed to get order status data. Please contact technical support.";
                    ErrorLogService.handleLoadingPageErrors(
                        error,
                        OrderConstant.TYPE_GET_LIST_ORDER_STATUSES,
                        loadingErrorLogs,
                        requestTime,
                        action,
                        message
                    );
                    return Observable.empty();
                })
        });
}

GetListOrderStatusesEpic.className = 'GetListOrderStatusesEpic';

export default EpicFactory.get(GetListOrderStatusesEpic);
