/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import ActionLogAction from "../../action/ActionLogAction";
import OrderAction from "../../action/OrderAction";
import OrderConstant from "../../constant/OrderConstant";
import TakePaymentService from "../../../service/sales/order/TakePaymentService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * take payment epic
 * @param action$
 * @returns {*}
 */
function TakePaymentEpic(action$) {
    return action$.ofType(OrderConstant.TAKE_PAYMENT)
        .mergeMap(action => Observable.from(TakePaymentService.takePayment(action.order))
            .mergeMap((response) => {
                return [
                    OrderAction.takePaymentResult(response.order, response.createInvoice),
                    OrderAction.syncActionUpdateDataFinish([response.order]),
                    ActionLogAction.syncActionLog()
                ];
            }).catch(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            })
        );
}

TakePaymentEpic.className = 'TakePaymentEpic';

export default EpicFactory.get(TakePaymentEpic);
