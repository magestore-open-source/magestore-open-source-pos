/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import CreditmemoConstant from "../../constant/order/CreditmemoConstant";
import ActionLogAction from "../../action/ActionLogAction";
import CreditmemoSuccessService from "../../../service/sales/order/creditmemo/CreditmemoSuccessService";
import OrderAction from "../../action/OrderAction";
import CreditmemoAction from "../../action/order/CreditmemoAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

function CreditmemoCreateCustomerEpic(action$) {
    return action$.ofType(CreditmemoConstant.CREDITMEMO_CREATE_CUSTOMER)
        .mergeMap(action => Observable.from(
                CreditmemoSuccessService.creditmemoCreateCustomer(
                    action.order, action.email, action.isNewAccount, action.newCustomer
                )
            )
                .mergeMap((response) => {
                    return [
                        CreditmemoAction.creditmemoCreateCustomerResult(response),
                        ActionLogAction.syncActionLog(),
                        OrderAction.syncActionUpdateDataFinish([response])
                    ];
                }).catch(() => {
                    return Observable.empty();
                })
        );
}

CreditmemoCreateCustomerEpic.className = 'CreditmemoCreateCustomerEpic';

export default EpicFactory.get(CreditmemoCreateCustomerEpic);
