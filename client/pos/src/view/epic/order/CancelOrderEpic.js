/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import cloneDeep from 'lodash/cloneDeep';
import ActionLogAction from "../../action/ActionLogAction";
import OrderAction from "../../action/OrderAction";
import OrderConstant from "../../constant/OrderConstant";
import OrderService from "../../../service/sales/OrderService";
import EpicFactory from "../../../framework/factory/EpicFactory";

function CancelOrderEpic(action$) {
    return action$.ofType(OrderConstant.CANCEL)
        .mergeMap(action => {
            let orderToCancel = cloneDeep(action.order);
            return Observable.from(
                OrderService.cancel(action.order, action.comment, action.notify, action.visibleOnFront)
            ).mergeMap((response) => {
                return [
                    OrderAction.cancelResult(response),
                    OrderAction.cancelOrderAfter(response, orderToCancel),
                    OrderAction.syncActionUpdateDataFinish([response]),
                    ActionLogAction.syncActionLog()
                ];
            }).catch(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            })
        });
}

CancelOrderEpic.className = 'CancelOrderEpic';

export default EpicFactory.get(CancelOrderEpic);
