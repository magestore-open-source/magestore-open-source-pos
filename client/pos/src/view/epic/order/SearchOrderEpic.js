/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import OrderConstant from "../../constant/OrderConstant";
import OrderService from "../../../service/sales/OrderService";
import OrderAction from "../../action/OrderAction";
import Config from "../../../config/Config";
import SyncConstant from "../../constant/SyncConstant";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * search order epic
 *
 * @param action$
 * @returns {Observable<any>}
 */
function SearchOrderEpic(action$) {
    return action$.ofType(OrderConstant.SEARCH_ORDER)
        .mergeMap(action => {
            let requestMode = Config.dataTypeMode[SyncConstant.TYPE_ORDER];
            return Observable.from(
                    OrderService.getListOrder(action.queryService, action.searchAllTime)
                ).map(response => {
                    return OrderAction.searchOrderResult(
                        response.items,
                        response.search_criteria,
                        response.total_count,
                        action.search_key,
                        action.searchAllTime,
                        requestMode
                    )
                }).catch(() => Observable.of(OrderAction.searchOrderResult([])))
            }
        );
}

SearchOrderEpic.className = 'SearchOrderEpic';

export default EpicFactory.get(SearchOrderEpic);
