/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import ActionLogAction from "../../action/ActionLogAction";
import OrderAction from "../../action/OrderAction";
import OrderConstant from "../../constant/OrderConstant";
import OrderService from "../../../service/sales/OrderService";
import SyncConstant from "../../constant/SyncConstant";
import ActionLogService from "../../../service/sync/ActionLogService";
import DateTimeHelper from "../../../helper/DateTimeHelper";
import EpicFactory from "../../../framework/factory/EpicFactory";

function AddCommentOrderEpic(action$) {
    return action$.ofType(OrderConstant.ADD_COMMENT)
        .mergeMap(action => {
            let order = action.order;
            let comment = action.comment;
            let notify = action.notify;
            let visibleOnFront = action.visibleOnFront;
            let createAt = DateTimeHelper.getDatabaseDateTime();
            order = OrderService.addComment(order, comment, notify, visibleOnFront);
            let orderResource = OrderService.getResourceModel();
            return Observable.fromPromise(
                orderResource.saveToDb([order])
            ).mergeMap(() => {
                let url_api = orderResource.getResourceOnline().getPathAddComentOrder();
                let params = {
                    increment_id: order.increment_id,
                    comment:
                        {
                            comment: comment,
                            created_at: createAt,
                            is_visible_on_front: +visibleOnFront
                        }
                };
                return Observable.fromPromise(
                    ActionLogService.createDataActionLog(
                        SyncConstant.REQUEST_ADD_COMMENT_ORDER, url_api, SyncConstant.METHOD_POST, params
                    ).mergeMap(() => {
                        return [
                            OrderAction.addCommentResult(order),
                            OrderAction.syncActionUpdateDataFinish([order]),
                            ActionLogAction.syncActionLog()
                        ];
                    })
                )
            }).catch(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            })
        });
}

AddCommentOrderEpic.className = 'AddCommentOrderEpic';

export default EpicFactory.get(AddCommentOrderEpic);
