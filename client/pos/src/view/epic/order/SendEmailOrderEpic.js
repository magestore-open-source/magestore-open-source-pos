/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import ActionLogAction from "../../action/ActionLogAction";
import OrderConstant from "../../constant/OrderConstant";
import OrderService from "../../../service/sales/OrderService";
import EpicFactory from "../../../framework/factory/EpicFactory";

function SendEmailOrderEpic(action$) {
    return action$.ofType(OrderConstant.SEND_EMAIL)
        .mergeMap(action => Observable.from(OrderService.sendEmail(action.order, action.email))
            .mergeMap(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            }).catch(() => {
                return [
                    ActionLogAction.syncActionLog()
                ];
            })
        );
}

SendEmailOrderEpic.className = 'SendEmailOrderEpic';

export default EpicFactory.get(SendEmailOrderEpic);
