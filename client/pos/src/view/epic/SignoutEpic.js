/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import {combineEpics} from 'redux-observable';
import SignoutConstant from "../constant/SignoutConstant";
import LogoutPopupAction from "../action/LogoutPopupAction";
import LocationService from "../../service/LocationService";
import UserService from "../../service/user/UserService";
import ApiResponseConstant from "../constant/ApiResponseConstant";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * click login action
 * @param action$
 * @returns {Observable<any>}
 */
function SignOutGetLocationsEpic(action$) {
    return action$.ofType(SignoutConstant.GET_NEW_LOCATION_LIST)
        .mergeMap(action => Observable.from(
            LocationService.getNewLocations(action.queryService))
            .map((response) => {
                if (response.locations !== undefined) {
                    UserService.saveLocations(JSON.stringify(response.locations));
                }
                let data = {
                    code: ApiResponseConstant.EXCEPTION_CODE_FORCE_CHANGE_POS,
                    message: 'Opps! Access denied. Recent action has not been saved yet.'
                };
                return LogoutPopupAction.finishLogoutRequesting(data);
            }).catch((error) => {
                let data = {
                    code: ApiResponseConstant.EXCEPTION_CODE_FORCE_CHANGE_POS,
                    message: error.message
                };
                return Observable.of(LogoutPopupAction.finishLogoutRequesting(data));
            })
        );
}

SignOutGetLocationsEpic.className = 'SignOutGetLocationsEpic';

/**
 * export combine epics
 *
 * @type {Epic<Action, any, any, Action> | (function(*): Observable<any>)}
 */
export const signoutEpic = combineEpics(
    EpicFactory.get(SignOutGetLocationsEpic)
);

export default signoutEpic;



