/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ProductConstant from '../../constant/ProductConstant';
import {Observable} from 'rxjs';
import ProductService from "../../../service/catalog/ProductService";
import QuoteService from "../../../service/checkout/QuoteService";
import {fire} from "../../../event-bus";
import EpicFactory from "../../../framework/factory/EpicFactory";
import { toast } from 'react-toastify';
import i18n from '../../../config/i18n';

/**
 * Receive action type(SEARCH_BY_BARCODE) and request, response list product
 * @param action$
 * @param store
 * @returns {Observable<Observable<any> | * | type | products>}
 * @constructor
 */
function SearchProductByBarcodeEpic(action$, store) {
    return action$.ofType(ProductConstant.SEARCH_BY_BARCODE)
        .mergeMap(action => {
            return Observable.from(
                    ProductService.processBarcode(action.code, store)
                ).map(response => {
                    fire('search_barcode_result_after', {
                        products: response
                    });
                    if (response && response.items && response.items.length === 1) {
                        let product = response.items[0];
                        QuoteService.addProductToCurrentQuote(store, product);
                    } else {
                        toast.error(
                            i18n.translator.translate("Barcode {{code}} does not exist.", {code: action.code}),
                            {
                                className: 'wrapper-messages messages-warning'
                            }
                        );
                    }
                    // require to return an object action (have key "type")
                    return {type: ""};
                }).catch(() => Observable.empty())
            }
        );
}

SearchProductByBarcodeEpic.className = 'SearchProductByBarcodeEpic';

export default EpicFactory.get(SearchProductByBarcodeEpic);
