/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from "rxjs";
import ProductConstant from "../../constant/ProductConstant";
import ProductService from "../../../service/catalog/ProductService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(GET_LIST_PRODUCT) and request, response list product
 * @param action$
 */
function GetListProductEpic(action$) {
    return action$.ofType(ProductConstant.GET_LIST_PRODUCT)
        .mergeMap(action =>
            Observable.from(ProductService.getProductList(action.search_key, action.pageSize, action.currentPage))
                .map(response => {
                    let data  = response.items;
                    // call service save data to database
                    ProductService.saveToDb(data);
                    return ProductConstant.getListProductResult(data)})
                .catch(() => Observable.of(ProductConstant.getListProductResult([])))
        );
}

GetListProductEpic.className = 'GetListProductEpic';

export default EpicFactory.get(GetListProductEpic);
