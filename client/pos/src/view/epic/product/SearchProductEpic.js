/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ProductConstant from '../../constant/ProductConstant';
import ProductAction from '../../action/ProductAction';
import {Observable} from 'rxjs';
import ProductService from "../../../service/catalog/ProductService";
import Config from "../../../config/Config";
import SyncConstant from "../../constant/SyncConstant";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(SEARCH_PRODUCT) and request, response list product
 * @param action$
 */
function SearchProductEpic(action$) {
    return action$.ofType(ProductConstant.SEARCH_PRODUCT)
        .mergeMap(action => {
            let requestMode = Config.dataTypeMode[SyncConstant.TYPE_PRODUCT];
            return Observable.from(
                ProductService.getProductList(action.queryService)
            ).map(response => {
                return ProductAction.searchProductResult(
                    response.items,
                    response.search_criteria,
                    response.total_count,
                    action.search_key,
                    requestMode
                )
            }).catch(() => Observable.of(ProductAction.searchProductResult([])))
        });
}

SearchProductEpic.className = 'SearchProductEpic';

export default EpicFactory.get(SearchProductEpic);
