/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import CustomerConstant from "../../constant/CustomerConstant";
import CustomerService from "../../../service/customer/CustomerService";
import CustomerAction from "../../action/CustomerAction";
import QuoteAction from "../../action/checkout/QuoteAction";
import ActionLogAction from "../../action/ActionLogAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Edit customer epic
 * 
 * @param action$
 * @return {Observable<any>}
 * @constructor
 */
function EditCustomerEpic(action$) {
    return action$.ofType(CustomerConstant.EDIT_CUSTOMER)
        .mergeMap(action => Observable.from(
            CustomerService.editCustomer(action.customer)
            ).mergeMap(response => {
                return [
                    CustomerAction.saveCustomer([response], action.isShipping),
                    QuoteAction.setCustomer(action.customer),
                    CustomerAction.syncActionUpdateDataFinish([response]),
                    ActionLogAction.syncActionLog()
                ]
            }).catch(error => Observable.of(CustomerAction.editCustomerError(error)))
        );
}

EditCustomerEpic.className = 'EditCustomerEpic';

export default EpicFactory.get(EditCustomerEpic);
