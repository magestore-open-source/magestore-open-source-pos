/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CustomerConstant from '../../constant/CustomerConstant';
import CustomerAction from '../../action/CustomerAction';
import {Observable} from 'rxjs';
import CustomerService from "../../../service/customer/CustomerService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(SEARCH_CUSTOMER) and request, response list customer
 * @param action$
 */
function SearchCustomerEpic(action$) {
    return action$.ofType(CustomerConstant.SEARCH_CUSTOMER)
        .mergeMap(action => Observable.from(
            CustomerService.getList(action.queryService)
            ).map(response => {
                return CustomerAction.searchCustomerResult(
                    response.items,
                    response.search_criteria,
                    response.total_count,
                    action.search_key
                )
            }).catch(() => Observable.of(CustomerAction.searchCustomerResult([])))
        );
}

SearchCustomerEpic.className = "SearchCustomerEpic";

export default EpicFactory.get(SearchCustomerEpic);
