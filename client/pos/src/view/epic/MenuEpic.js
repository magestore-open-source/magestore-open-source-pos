/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import MenuConstant from "../constant/MenuConstant";
import {combineEpics} from 'redux-observable';
import SyncService from "../../service/sync/SyncService";
import LogoutPopupAction from "../action/LogoutPopupAction";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * click login action
 * @param action$
 * @returns {Observable<any>}
 */
function MenuUserClickLogoutEpic(action$) {
    return action$.ofType(MenuConstant.CLICK_LOGOUT_ITEM)
        .mergeMap(() => Observable.from(SyncService.hasSyncPending())
            .map(hasPending => {
                if(hasPending) {
                    return LogoutPopupAction.logoutRequestingError({
                        type: 'Network Error',
                        message: 'You shouldn\'t logout now because data haven\'t been synchronized yet'
                    });
                }

                return LogoutPopupAction.toggle()
            })
        );
}

MenuUserClickLogoutEpic.className = 'MenuUserClickLogoutEpic';

/**
 * export combine epics
 *
 * @type {Epic<Action, any, any, Action> | (function(*): Observable<any>)}
 */
export const menuEpic = combineEpics(
    EpicFactory.get(MenuUserClickLogoutEpic)
);

export default menuEpic;



