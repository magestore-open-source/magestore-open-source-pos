/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {combineEpics} from 'redux-observable';
import GetPaymentOnlineEpic from "./payment/GetPaymentOnlineEpic";
import GetListPaymentEpic from "./payment/GetListPaymentEpic";
import ClearPaymentDataEpic from "./payment/ClearPaymentDataEpic";
import ProcessPaymentEpic from "./payment/ProcessPaymentEpic";

/**
 * Combine all payment epic
 * @type {Epic<Action, any, any, T> | any}
 */
const paymentEpic = combineEpics(
    GetPaymentOnlineEpic,
    GetListPaymentEpic,
    ClearPaymentDataEpic,
    ProcessPaymentEpic
);

export default paymentEpic;