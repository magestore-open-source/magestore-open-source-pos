/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import UserService from "../../service/user/UserService";
import UserConstant from "../constant/UserConstant";
import UserAction from "../action/UserAction";
import {combineEpics} from 'redux-observable';
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 * click login action
 * @param action$
 * @returns {Observable<any>}
 */
function UserClickLoginEpic(action$) {
    return action$.ofType(UserConstant.USER_CLICK_LOGIN)
        .mergeMap(action => Observable.from(
            UserService.login(action.username, action.password))
            .map((response) => {
                if (response.token !== undefined) {
                    UserService.saveToken(response.token);
                }
                if (response.session_id !== undefined) {
                    UserService.saveSession(response.session_id, response.timeout);
                }
                if (response.staff_id !== undefined) {
                    UserService.saveStaff(
                        response.staff_id,
                        response.staff_name,
                        response.staff_email,
                        action.username
                    );
                }
                /* save sharing account */
                if (response.sharing_account !== undefined) {
                    UserService.setSharingAccount(response.sharing_account);
                }
                if (response.locations !== undefined) {
                    UserService.saveLocations(JSON.stringify(response.locations));
                }
                if (response.website_id !== undefined) {
                    UserService.saveWebsiteId(JSON.stringify(response.website_id));
                }
                return UserAction.loginSuccess(response);
            }).catch(error => Observable.of(UserAction.loginError(error.message)))
        );
}

UserClickLoginEpic.className = 'UserClickLoginEpic';

/**
 * continue login will remove all current session
 * @param action$
 * @returns {Observable<any>}
 */
function UserContinueLoginEpic(action$) {
    return action$.ofType(UserConstant.USER_CONTINUE_LOGIN)
        .mergeMap(() => Observable.from(
            UserService.continueLogin())
            .mergeMap((response) => {
                if (response.locations) {
                    UserService.saveLocations(JSON.stringify(response.locations));
                }
                /* save sharing account */
                if (response.sharing_account) {
                    UserService.setSharingAccount(response.sharing_account);
                }
                return [UserAction.loginSuccess(response),
                    UserAction.afterContinueLogin(true)];
            }).catch(error => Observable.of(UserAction.loginError(error.message)))
        );
}

UserContinueLoginEpic.className = 'UserContinueLoginEpic';

/**
 * user change information
 * @param action$
 * @returns {Observable<any>}
 */
function UserChangeInformationEpic(action$) {
    return action$.ofType(UserConstant.USER_CHANGE_INFORMATION)
        .mergeMap(action => Observable.from(
            UserService.changeInformation(
                action.user_name,
                action.old_password,
                action.new_password,
                action.confirmation_password
            )).map((response) => {
                let result = JSON.parse(response);

                if (result.success) {
                    return UserAction.changeInformationSuccess(result);
                } else {
                    return UserAction.changeInformationError(result);
                }
            })
                .catch((error) => {
                    return UserAction.changeInformationError(JSON.parse(error));
                })
        );
}

UserChangeInformationEpic.className = 'UserChangeInformationEpic';

/**
 * user get logo
 * @param action$
 * @returns {Observable<any>}
 */
function GetLogoEpic(action$) {
    return action$.ofType(UserConstant.USER_GET_LOGO)
        .mergeMap(() => Observable.from(
            UserService.getLogo())
            .map((response) => {
                return UserAction.getLogoSuccess(response);
            })
            .catch(error => {
                return Observable.of(UserAction.getLogoError(error.message))
            })
        );
}

GetLogoEpic.className = 'GetLogoEpic';

/**
 * user get logo
 * @param action$
 * @returns {Observable<any>}
 */
function GetCountriesEpic(action$) {
    return action$.ofType(UserConstant.USER_GET_COUNTRIES)
        .mergeMap(() => {
            UserService.getCountries()
                .then(response => {
                    UserService.saveLocalCountries(response);
                });
            return Observable.empty();
        });
}

GetCountriesEpic.className = 'GetCountriesEpic';

/**
 * export combine epics
 *
 * @type {Epic<Action, any, any, Action> | (function(*): Observable<any>)}
 */
export const userEpic = combineEpics(
    EpicFactory.get(UserClickLoginEpic),
    EpicFactory.get(UserContinueLoginEpic),
    EpicFactory.get(UserChangeInformationEpic),
    EpicFactory.get(GetLogoEpic),
    EpicFactory.get(GetCountriesEpic),
);

export default userEpic;



