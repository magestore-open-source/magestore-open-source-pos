/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {combineEpics} from 'redux-observable';
import SearchOrderEpic from "./order/SearchOrderEpic";
import SyncOrderUpdateDataFinishEpic from "./order/SyncOrderUpdateDataFinishEpic";
import TakePaymentEpic from "./order/TakePaymentEpic";
import CreditmemoEpic from "./order/CreditmemoEpic";
import SendEmailOrderEpic from "./order/SendEmailOrderEpic";
import AddCommentOrderEpic from "./order/AddCommentOrderEpic";
import CancelOrderEpic from "./order/CancelOrderEpic";
import SendEmailCreditmemoEpic from "./order/SendEmailCreditmemoEpic";
import CreditmemoCreateCustomerEpic from "./order/CreditmemoCreateCustomerEpic";
import GetListOrderStatusesEpic from "./order/GetListOrderStatusesEpic";
import SyncDeletedOrderFinishEpic from "./order/SyncDeletedOrderFinishEpic";

/**
 * Combine all product epic
 * @type {Epic<Action, any, any, T> | any}
 */
const orderEpic = combineEpics(
    SearchOrderEpic,
    SyncOrderUpdateDataFinishEpic,
    TakePaymentEpic,
    CreditmemoEpic,
    SendEmailOrderEpic,
    AddCommentOrderEpic,
    CancelOrderEpic,
    SendEmailCreditmemoEpic,
    CreditmemoCreateCustomerEpic,
    GetListOrderStatusesEpic,
    SyncDeletedOrderFinishEpic,
);

export default orderEpic;


