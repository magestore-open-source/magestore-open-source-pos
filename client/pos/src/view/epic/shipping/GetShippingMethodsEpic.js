/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ShippingConstant from "../../constant/ShippingConstant";
import ShippingAction from "../../action/ShippingAction";
import {Observable} from 'rxjs';
import QuoteAddressService from "../../../service/checkout/quote/AddressService";
import QuoteService from "../../../service/checkout/QuoteService";
import AddressConstant from "../../constant/checkout/quote/AddressConstant";
import ShippingService from "../../../service/shipping/ShippingService";
import TaxHelper from "../../../helper/TaxHelper";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Get shipping method by address
 *
 * @param action$
 * @returns {Observable<any>}
 */
function GetShippingMethodsEpic(action$) {
    return action$.ofType(ShippingConstant.GET_SHIPPING_METHODS)
        .mergeMap(action => Observable.of(ShippingService.getAllowShippingMethods())
            .mergeMap((allShippingMethods) => {
                let quote = JSON.parse(JSON.stringify(action.quote));
                let address = JSON.parse(JSON.stringify(action.address));
                if (!quote || !address) {
                    return Observable.empty();
                }
                address.address_type = AddressConstant.SHIPPING_ADDRESS_TYPE;
                let shippingAddress = quote.addresses.find(address =>
                    address.address_type === AddressConstant.SHIPPING_ADDRESS_TYPE
                );
                QuoteAddressService.updateAddress(shippingAddress, address, quote.customer);
                QuoteService.collectTotals(quote);

                let shippingMethods = QuoteAddressService.requestShippingRates(
                    quote, shippingAddress, allShippingMethods
                );
                let displayInclTax = TaxHelper.shippingPriceDisplayIncludeTax();
                shippingMethods.forEach(shipping => {
                    shippingAddress.shipping_method = shipping.code;
                    shippingAddress.current_shipping_method = shipping;
                    QuoteService.collectTotals(quote);
                    shipping.display_amount = displayInclTax ?
                        shippingAddress.shipping_incl_tax : shippingAddress.shipping_amount;
                });

                return Observable.of(
                    ShippingAction.getListShippingResult(shippingMethods)
                );
            }).catch(() => {
                return Observable.of(ShippingAction.getListShippingResult([]));
            })
        );
}

GetShippingMethodsEpic.className = 'GetShippingMethodsEpic';

export default EpicFactory.get(GetShippingMethodsEpic);
