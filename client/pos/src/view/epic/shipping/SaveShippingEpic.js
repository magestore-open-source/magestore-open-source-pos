/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ShippingConstant from "../../constant/ShippingConstant";
import {Observable} from 'rxjs';
import QuoteAddressService from "../../../service/checkout/quote/AddressService";
import AddressConstant from "../../constant/checkout/quote/AddressConstant";
import QuoteService from "../../../service/checkout/QuoteService";
import QuoteAction from "../../action/checkout/QuoteAction";
import AppStore from "../../../view/store/store";
import ShippingAction from "../../action/ShippingAction";
import CheckoutAction from "../../action/CheckoutAction";
import {fire} from "../../../event-bus";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Save shipping to quote
 *
 * @param action$
 * @returns {Observable<any>}
 */
function SaveShippingEpic(action$) {
    return action$.ofType(ShippingConstant.SAVE_SHIPPING)
        .mergeMap((action) => {
            let quote = action.quote,
                address = action.address,
                shippingMethod = action.shipping_method,
                deliveryDate = action.delivery_date;
            let customer = quote.customer;
            let shippingAddress = QuoteAddressService.getShippingAddress(quote);
            if (!shippingAddress) {
                shippingAddress = QuoteAddressService.createAddress(
                    AddressConstant.SHIPPING_ADDRESS_TYPE, address, customer
                );
                shippingAddress.quote_id = quote.id;
                quote.addresses.push(shippingAddress);
            } else {
                QuoteAddressService.updateAddress(shippingAddress, address, customer);
            }
            shippingAddress.shipping_method = shippingMethod ? shippingMethod.code : "";
            shippingAddress.current_shipping_method = shippingMethod;
            quote.pos_delivery_date = deliveryDate;

            AppStore.dispatch(ShippingAction.saveShippingAfter(quote));

            quote = QuoteService.collectTotals(quote);
            let shippingArray = quote.applied_rule_ids && quote.applied_rule_ids.includes('POS_CUSTOM_DISCOUNT') ?
                [
                    QuoteAction.setQuote(quote)
                ] : [
                    QuoteAction.setQuote(quote),
                    QuoteAction.validateQuoteSalesRule(quote)
                ];
            if (!quote.grand_total) {
                shippingArray.push(CheckoutAction.checkoutToSelectPayments(quote));
            }
            fire('epic_save_shipping_execute_after', { quote });
            return Observable.from(shippingArray);
        });
}

SaveShippingEpic.className = 'SaveShippingEpic';

export default EpicFactory.get(SaveShippingEpic);
