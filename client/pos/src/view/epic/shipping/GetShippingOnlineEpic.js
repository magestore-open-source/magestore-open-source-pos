/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ShippingConstant from '../../constant/ShippingConstant';
import LoadingAction from "../../action/LoadingAction";
import {Observable} from 'rxjs';
import ShippingService from "../../../service/shipping/ShippingService";
import SyncService from "../../../service/sync/SyncService";
import Config from "../../../config/Config";
import AppStore from "../../store/store";
import ErrorLogService from "../../../service/sync/ErrorLogService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Get shipping online epic
 * @param action$
 * @returns {Observable<any>}
 */
function GetShippingOnlineEpic(action$) {
    let requestTime = 0;
    let loadingErrorLogs = {};
    return action$.ofType(ShippingConstant.GET_SHIPPING_ONLINE)
        .mergeMap((action) => {
            requestTime++;
            return Observable.from(SyncService.getShipping())
                .mergeMap((response) => {
                    ShippingService.clear().then(() => {
                        ShippingService.saveToDb(response.items);
                        Config.shipping_methods = response.items;
                    });
                    requestTime = 0;
                    if (action.atLoadingPage) {
                        AppStore.dispatch(LoadingAction.updateFinishedList(ShippingConstant.TYPE_GET_SHIPPING));
                    }
                    return [];
                }).catch(error => {
                    let message = "Failed to get shipping data. Please contact technical support.";
                    ErrorLogService.handleLoadingPageErrors(
                        error,
                        ShippingConstant.TYPE_GET_SHIPPING,
                        loadingErrorLogs,
                        requestTime,
                        action,
                        message
                    );
                    return Observable.empty();
                })
        });
}

GetShippingOnlineEpic.className = 'GetShippingOnlineEpic';

export default EpicFactory.get(GetShippingOnlineEpic);
