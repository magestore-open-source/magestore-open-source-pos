/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ShippingConstant from "../../constant/ShippingConstant";
import ShippingService from "../../../service/shipping/ShippingService";
import Config from "../../../config/Config";
import {Observable} from 'rxjs';
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Get shipping list
 *
 * @param action$
 * @returns {Observable<any>}
 */
function GetListShippingEpic(action$) {
    return action$.ofType(ShippingConstant.GET_LIST_SHIPPING)
        .mergeMap(() => Observable.from(ShippingService.getAll())
            .mergeMap((response) => {
                Config.shipping_methods = response;
                return Observable.empty();
            }).catch(() => {
                return Observable.empty();
            })
        );
}

GetListShippingEpic.className = 'GetListShippingEpic';

export default EpicFactory.get(GetListShippingEpic);
