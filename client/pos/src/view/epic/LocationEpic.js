/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from "rxjs/Rx";
import UserConstant from "../constant/UserConstant";
import LocationAction from "../action/LocationAction";
import LocationService from "../../service/LocationService";
import EpicFactory from "../../framework/factory/EpicFactory";

/**
 *
 * @param action$
 * @returns {Observable<{type: string} | {type: string, error: *}>}
 */
function LocationEpic(action$) {
    return action$.ofType(UserConstant.USER_ASSIGN_POS)
        .mergeMap(action => Observable.from(
            LocationService.assignPos(
                action.posId, action.locationId, action.currentStaffId))
            .map(() => {
                return LocationAction.assignPosResponse();
            })
            .catch((error) =>{
                    return Observable.of(LocationAction.assignPosError(error.message));
                }
            )
        );
}

LocationEpic.className = 'LocationEpic';

export default EpicFactory.get(LocationEpic);
