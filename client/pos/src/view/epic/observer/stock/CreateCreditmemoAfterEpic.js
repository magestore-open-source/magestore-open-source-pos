/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CreditmemoConstant from '../../../constant/order/CreditmemoConstant';
import {Observable} from 'rxjs';
import ReturnProcessorService from "../../../../service/sales/inventory/order/ReturnProcessorService";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 * Receive action type(PLACE_ORDER_AFTER) and request, response list product
 * @param action$
 */
function StockCreateCreditmemoAfterEpic(action$) {
    return action$.ofType(CreditmemoConstant.CREATE_CREDITMEMO_AFTER)
        .mergeMap(action => {
            let creditmemo = action.creditmemo;
            let order = creditmemo.order;
            let returnToStockItems = [];
            creditmemo.items.forEach(item => {
                if (item.back_to_stock) {
                    returnToStockItems.push(item.order_item_id);
                }
            });
            if (returnToStockItems.length) {
                ReturnProcessorService.execute(creditmemo, order, returnToStockItems);
            }

            return Observable.empty();
        });
}

StockCreateCreditmemoAfterEpic.className = 'StockCreateCreditmemoAfterEpic';

export default EpicFactory.get(StockCreateCreditmemoAfterEpic);
