/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import OrderConstant from '../../../constant/OrderConstant';
import {Observable} from 'rxjs';
import NumberHelper from "../../../../helper/NumberHelper";
import LocationHelper from "../../../../helper/LocationHelper";
import ProductAction from "../../../action/ProductAction";
import StockService from "../../../../service/catalog/StockService";
import OrderItemService from "../../../../service/sales/order/OrderItemService";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 * Receive action type(PLACE_ORDER_AFTER) and request, response list product
 * @param action$
 */
function StockCancelOrderAfterEpic(action$) {
    return action$.ofType(OrderConstant.CANCEL_ORDER_AFTER)
        .mergeMap(action => {
            let orderToCancel = action.orderToCancel;
            let productIdsQty = getOrderProductIdsQty(orderToCancel);
            if (LocationHelper.isPrimaryLocation()) {
                return Observable.from(
                    StockService.getListByProductIds(Object.keys(productIdsQty).map(Number))
                ).mergeMap(stocks => {
                    stocks.map(stock => {
                        if (productIdsQty[stock.product_id]) {
                            stock.qty = parseFloat(stock.qty) + parseFloat(productIdsQty[stock.product_id])
                        }
                        return stock;
                    });
                    StockService.saveToDb(stocks);
                    return Observable.of(ProductAction.syncActionUpdateStockDataFinish(stocks));
                }).catch(() => {
                    return Observable.empty();
                })
            }
            return Observable.empty();
        });
}

StockCancelOrderAfterEpic.className = 'StockCancelOrderAfterEpic';

export default EpicFactory.get(StockCancelOrderAfterEpic);

/**
 * Get order product ids qty
 *
 * @param order
 * @return {{}}
 */
function getOrderProductIdsQty(order) {
    let productIds = {};
    order.items.forEach(item => {
        if (item.product_id) {
            if (!productIds[item.product_id]) {
                productIds[item.product_id] = 0;
            }
            let qtyToReturn = OrderItemService.getQtyToReturnCancel(item, order);
            productIds[item.product_id] = NumberHelper.addNumber(productIds[item.product_id], qtyToReturn);
        }
    });
    return productIds;
}
