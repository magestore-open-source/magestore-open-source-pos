/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import OrderConstant from '../../../constant/OrderConstant';
import {Observable} from 'rxjs';
import LocationHelper from "../../../../helper/LocationHelper";
import StockService from "../../../../service/catalog/StockService";
import QuoteService from "../../../../service/checkout/QuoteService";
import NumberHelper from "../../../../helper/NumberHelper";
import OnHoldOrderConstant from "../../../constant/OnHoldOrderConstant";
import ProductAction from "../../../action/ProductAction";
import EpicFactory from "../../../../framework/factory/EpicFactory";

/**
 * Deduce
 *
 * Receive action type(PLACE_ORDER_AFTER) and request, response list product
 * @param action$
 */
function StockPlaceOrderAfterEpic(action$) {
    return action$.ofType(OrderConstant.PLACE_ORDER_AFTER, OnHoldOrderConstant.HOLD_ORDER_AFTER)
        .mergeMap(action => {
            if (LocationHelper.isPrimaryLocation()) {
                let productIdsQty = getOrderProductIdsQty(action.quote);
                let createShipment = action.quote.create_shipment;
                return Observable.from(
                    StockService.getListByProductIds(Object.keys(productIdsQty).map(Number))
                ).mergeMap(stocks => {
                    stocks.map(stock => {
                        if (productIdsQty[stock.product_id]) {
                            stock.qty = parseFloat(stock.qty) - parseFloat(productIdsQty[stock.product_id]);
                            if (createShipment) {
                                stock.quantity = NumberHelper.minusNumber(
                                    stock.quantity,
                                    productIdsQty[stock.product_id]
                                );
                            }
                        }
                        return stock;
                    });
                    StockService.saveToDb(stocks);
                    return Observable.of(ProductAction.syncActionUpdateStockDataFinish(stocks));
                }).catch(() => {
                    return Observable.empty();
                })
            }
            return Observable.empty()
        });
}

StockPlaceOrderAfterEpic.className = 'StockPlaceOrderAfterEpic';

export default EpicFactory.get(StockPlaceOrderAfterEpic);

/**
 * Get order product ids qty from quote
 *
 * @param quote
 * @return {{}}
 */
function getOrderProductIdsQty(quote) {
    let productIds = {};
    let parentItems = {};
    quote.items.map(item => {
        if (item.product && item.product.id) {
            if (StockService.getProductStockService(item.product).isManageStock(item.product)) {
                if (!productIds[item.product.id]) {
                    productIds[item.product.id] = 0;
                }
                let qty = item.qty;
                if (item.parent_item_id) {
                    let parentItemId = item.parent_item_id;
                    if (!parentItems[parentItemId]) {
                        let parentItem = QuoteService.getParentItem(quote, item);
                        if (parentItem) {
                            parentItems[parentItemId] = parentItem;
                        }
                    }
                    if (parentItems[parentItemId] && parentItems[parentItemId].item_id) {
                        qty = NumberHelper.multipleNumber(qty, parentItems[parentItemId].qty);
                    }
                }
                productIds[item.product.id] = NumberHelper.addNumber(productIds[item.product.id], qty);
            }
        }
        return item;
    });
    return productIds;
}
