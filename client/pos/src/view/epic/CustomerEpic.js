/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import SearchCustomerEpic from './customer/SearchCustomerEpic';
import {combineEpics} from 'redux-observable';
import CreateCustomerEpic from "./customer/CreateCustomerEpic";
import EditCustomerEpic from "./customer/EditCustomerEpic";
import SyncCustomerUpdateDataFinishEpic from "./customer/SyncCustomerUpdateDataFinishEpic";
import SyncDeletedCustomerEpic from "./customer/SyncDeletedCustomerEpic";

/**
 * Combine all customer epic
 * @type {Epic<Action, any, any, T> | any}
 */
const customerEpic = combineEpics(
    SearchCustomerEpic,
    CreateCustomerEpic,
    EditCustomerEpic,
    SyncCustomerUpdateDataFinishEpic,
    SyncDeletedCustomerEpic,
);

export default customerEpic;