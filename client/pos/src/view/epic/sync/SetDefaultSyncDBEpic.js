/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import SyncService from "../../../service/sync/SyncService";
import SyncConstant from "../../constant/SyncConstant";
import SyncAction from "../../action/SyncAction";
import AppStore from "../../store/store";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Set default data of syncDb
 * @param action$
 * @returns {Observable<any>}
 */
function SetDefaultSyncDBEpic(action$) {
    return action$.ofType(SyncConstant.SET_DEFAULT_SYNC_DB)
        .mergeMap(() => {
                SyncService.setDefaultData().then((result) => {
                    if (result) {
                        AppStore.dispatch(SyncAction.setDataTypeMode(SyncService.getDefaultDataTypeMode()));
                    }
                });
                return Observable.of(SyncAction.setDefaultSyncDBSuccess());
            }
        );
}

SetDefaultSyncDBEpic.className = 'SetDefaultSyncDBEpic';

export default EpicFactory.get(SetDefaultSyncDBEpic);
