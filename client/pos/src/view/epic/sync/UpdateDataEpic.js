/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import Config from "../../../config/Config";
import SyncService from "../../../service/sync/SyncService";
import SyncConstant from "../../constant/SyncConstant";
import SyncAction from "../../action/SyncAction";
import {fire} from "../../../event-bus";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(UPDATE_DATA) and update data from server
 * @param action$
 */
function UpdateDataEpic(action$) {
    let counter = 0;
    return action$.ofType(SyncConstant.UPDATE_DATA)
        .mergeMap(() => {
            if (!window.navigator.onLine) {
                return Observable.empty();
            }
            return Observable.from(SyncService.getAll())
                .mergeMap(result => {
                    let actions = [];
                    let data = result.filter(dataType => dataType.total >= 0 && dataType.count >= dataType.total);

                    for (let syncData of data) {
                        if (syncData.updating && !Config.updateDataFirstLoad) {
                            continue;
                        }

                        // Read config
                        let path = 'webpos/offline/' + syncData.type;
                        if (syncData.type === SyncConstant.TYPE_STOCK) {
                            path += '_item';
                        }
                        path += '_time';

                        let time = 10;
                        let configTime = Config.config.settings.filter(x => x.path === path)[0];
                        if (configTime) {
                            time = parseInt(configTime.value, 10);
                        }
                        if (syncData.type === SyncConstant.TYPE_CATALOG_RULE_PRODUCT_PRICE) {
                            time = 60;
                        } else if (syncData.type === SyncConstant.TYPE_CATEGORY) {
                            time = 60;
                        }

                        let eventDataBefore = {
                            time: time,
                            syncData: syncData
                        };
                        /* Event update data before */
                        fire('epic_update_data_before', eventDataBefore);
                        time = eventDataBefore.time;

                        if (0 === counter % time) {
                            // Update Data
                            syncData.updating = true;
                            actions.push(syncData);
                        }
                    }
                    counter++; // increase counter every minute
                    Config.updateDataFirstLoad = false;

                    // Save updating to database
                    actions.length && SyncService.saveToDb(actions);

                    // Dispatch executeUpdateData action to start update data
                    return Observable.of(SyncAction.executeUpdateData(actions));
                })
                .catch(() => Observable.empty());
        });
}

UpdateDataEpic.className = 'UpdateDataEpic';

export default EpicFactory.get(UpdateDataEpic);
