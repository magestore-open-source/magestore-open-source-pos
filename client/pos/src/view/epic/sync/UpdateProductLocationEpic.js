/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import QueryService from "../../../service/QueryService";
import SyncConstant from "../../constant/SyncConstant";
import SyncAction from "../../action/SyncAction";
import ProductService from "../../../service/catalog/ProductService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(UPDATE_DATA_FINISH, UPDATE_DATA_FINISH_RESULT) and check for add new product #locationfix
 * @param action$
 */
function UpdateProductLocationEpic(action$) {
    let ids = [], isCompletedProduct = false, isCompletedStock = false;
    return action$.ofType(SyncConstant.UPDATE_DATA_FINISH, SyncConstant.UPDATE_DATA_FINISH_RESULT)
        .mergeMap(action => {
            if (action.type === SyncConstant.UPDATE_DATA_FINISH) {
                if (action.data.type === SyncConstant.TYPE_STOCK) {
                    if(Array.isArray(action.items)) {
                        ids = ids.concat(action.items.map(item => item.product_id));
                    }
                }
            } else if (action.data.type === SyncConstant.TYPE_PRODUCT
                || action.data.type === SyncConstant.TYPE_STOCK
            ) { // UPDATE_DATA_FINISH_RESULT
                if (action.data.type === SyncConstant.TYPE_PRODUCT) {
                    isCompletedProduct = true;
                } else {
                    isCompletedStock = true;
                }
                // Check finished to update
                if (isCompletedProduct && isCompletedStock) {
                    // Update Data
                    if (ids.length) {
                        let pipe = Observable.fromPromise(ProductService.getNotExistedIds(ids))
                            .mergeMap(ids => {
                                if (!ids.length) {
                                    return Observable.empty();
                                }
                                return Observable.fromPromise(addProducts(ids))
                                    .mergeMap(result => {
                                        if (!result.total) {
                                            return Observable.empty();
                                        }
                                        return Observable.of(SyncAction.updateDataFinish(
                                            {type: SyncConstant.TYPE_PRODUCT}, result.items
                                        ));
                                    }).catch(() => Observable.empty());
                            }).catch(() => Observable.empty());
                        ids = [];
                        return pipe;
                    }
                    isCompletedProduct = false;
                    isCompletedProduct = false;
                }
            }
            return Observable.empty();
        });
}

UpdateProductLocationEpic.className = 'UpdateProductLocationEpic';

export default EpicFactory.get(UpdateProductLocationEpic);

/**
 * add product by ids
 *
 * @param {Array} ids
 * @return {object}
 */
async function addProducts(ids) {
    try {
        let queryService = QueryService.reset();
        queryService.addParams('show_option', '1');
        queryService.addFieldToFilter(
            'entity_id',
            ids,
            'in'
        );
        let response = await ProductService.getDataOnline(queryService, true);

        // Save items
        await ProductService.saveToDb(response.items, true);

        return {
            items: response.items,
            total: response.total_count
        };
    } catch (error) {
        return {error: true};
    }
}
