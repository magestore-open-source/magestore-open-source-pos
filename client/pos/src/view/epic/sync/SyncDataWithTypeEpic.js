/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import SyncConstant from "../../constant/SyncConstant";
import SyncAction from "../../action/SyncAction";
import ProductService from "../../../service/catalog/ProductService";
import SyncService from "../../../service/sync/SyncService";
import QueryService from "../../../service/QueryService";
import StockService from "../../../service/catalog/StockService";
import CustomerService from "../../../service/customer/CustomerService";
import Config from "../../../config/Config";
import OrderService from "../../../service/sales/OrderService";
import CatalogRuleProductPriceService from "../../../service/catalog/rule/CatalogRuleProductPriceService";
import CategoryService from "../../../service/catalog/CategoryService";
import GeneralService from "../../../service/settings/GeneralService";
import {fire} from "../../../event-bus";
import EpicFactory from "../../../framework/factory/EpicFactory";
/**
 * Request Data with type
 * @param action$
 * @returns {Observable<any>}
 */
function SyncDataWithTypeEpic(action$) {
    return action$.ofType(SyncConstant.SYNC_DATA_WITH_TYPE)
        .mergeMap(async function (action) {
            if(!GeneralService.isUseOfflineData()) {
                return SyncAction.syncData();
            }

            let data = action.data;
            let pageSize = 50;
            let currentPage;
            let result;
            let service;

            if (data.type === SyncConstant.TYPE_PRODUCT) {
                service = ProductService;
            } else if (data.type === SyncConstant.TYPE_STOCK) {
                service = StockService;
            } else if (data.type === SyncConstant.TYPE_CUSTOMER) {
                service = CustomerService;
            } else if (data.type === SyncConstant.TYPE_ORDER) {
                service = OrderService;
            } else if (data.type === SyncConstant.TYPE_CATALOG_RULE_PRODUCT_PRICE) {
                service = CatalogRuleProductPriceService;
            } else if (data.type === SyncConstant.TYPE_CATEGORY) {
                service = CategoryService;
            }

            if (data.type === SyncConstant.TYPE_CATALOG_RULE_PRODUCT_PRICE) {
                pageSize = 300;
            }

            let eventDataBefore = {
                data: data,
                service: service,
                pageSize: pageSize
            };
            /* Event update data with type before */
            fire('epic_sync_data_with_type_before', eventDataBefore);
            service = eventDataBefore.service;
            pageSize = eventDataBefore.pageSize;

            // Request each page of data
            if (data.total === SyncConstant.DEFAULT_TOTAL && Config.session) {
                currentPage = 1;
                result = await requestData(data, pageSize, currentPage, service);
                data = result.data;
                if (result.error) {
                    // If request is failed, recall syncData action to sync next data type
                    return SyncAction.syncData();
                }

            }
            if (data.count < data.total) {
                currentPage = Number((data.count / pageSize).toFixed()) + 1;
                let totalPage = Number(Math.ceil(data.total / pageSize));
                for (let i = currentPage; i <= totalPage; i++) {
                    if (Config.session) {
                        if(!GeneralService.isUseOfflineData()) {
                            return SyncAction.syncData();
                        }

                        result = await requestData(data, pageSize, i, service);
                        data = result.data;
                        if (result.error) {
                            // If request is failed, recall syncData action to sync next data type
                            return SyncAction.syncData();
                        }
                    }
                }
            }

            // After finished syncing this data type, reindex table and recall syncData action to sync next data type
            if (service.reindexTable) {
                service.reindexTable();
            }
            return SyncAction.syncData();
        });
}

SyncDataWithTypeEpic.className = 'SyncDataWithTypeEpic';

export default EpicFactory.get(SyncDataWithTypeEpic);

/**
 * Request data from server
 * @param data
 * @param pageSize
 * @param page
 * @param service
 * @return {Promise<{data: *, error: boolean}>}
 */
async function requestData(data, pageSize, page, service) {
    try {
        let queryService = QueryService.reset();
        queryService.setPageSize(pageSize).setCurrentPage(page);
        queryService.addParams('show_option', '1');

        let response = await service.getDataOnline(queryService, true);
        data.count += response.items.length;
        data.total = response.total_count;

        // Process updated_time
        !data.updated_time && (data.updated_time = Date.now());
        if (response.hasOwnProperty('cached_at') && response.cached_at < data.updated_time) {
            data.updated_time = response.cached_at;
        }

        // Process updated_data_time
        !data.updated_data_time && (data.updated_data_time = Date.now());
        if (response.hasOwnProperty('cached_at') && response.cached_at < data.updated_data_time) {
            data.updated_data_time = response.cached_at;
        }

        service.saveToDb(response.items);

        delete data.isFailed;
        await SyncService.saveToDb([data]);
        return {
            data: data,
            error: false
        }
    }
    catch (error) {
        // If request is failed, set data's isFailed attribute as true and save to indexedDb
        data.isFailed = true;
        await SyncService.saveToDb([data]);
        return {
            data: data,
            error: true
        }
    }
}
