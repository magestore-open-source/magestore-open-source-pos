/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import SyncConstant from "../../constant/SyncConstant";
import SyncAction from "../../action/SyncAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

function ExecuteUpdateDataEpic(action$) {
    return action$.ofType(SyncConstant.EXECUTE_UPDATE_DATA)
        .mergeMap(action => {
            if (!window.navigator.onLine || !action.actions.length) {
                return Observable.empty();
            }
            // get first data that need to be update
            let currentAction = action.actions.shift();
            // dispatch updateDataWithType action to update that data
            // with params are that data and list of next data type that need to update
            return Observable.of(SyncAction.updateDataWithType(currentAction, action.actions));
        });
}

ExecuteUpdateDataEpic.className = 'ExecuteUpdateDataEpic';

export default EpicFactory.get(ExecuteUpdateDataEpic);
