/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import SyncConstant from "../../constant/SyncConstant";
import ActionLogService from "../../../service/sync/ActionLogService";
import CheckoutConstant from "../../constant/CheckoutConstant";
import Action from "../../action/index";
import AppStore from "../../store/store";
import SyncAction from "../../action/SyncAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Reload after sync action log
 *
 * @param action$
 * @param store
 * @returns {Observable<any>}
 */
function ReloadPageEpic(action$, store) {
    let countOrder = 0;
    let timeout;
    return action$.ofType(
        SyncConstant.RELOAD_PAGE,
        SyncConstant.SYNC_ACTION_LOG_SUCCESS,
        CheckoutConstant.CHECK_OUT_PLACE_ORDER_RESULT
    ).mergeMap(async function(action) {
        if (action.type === CheckoutConstant.CHECK_OUT_PLACE_ORDER_RESULT) {
            countOrder++;
            return Action.empty();
        }
        if (timeout) {
            clearTimeout(timeout);
        }
        let canReload = await ActionLogService.checkCanReload(countOrder, store.getState());
        if (canReload === SyncConstant.CAN_RELOAD) {
            window.location.reload();
        } else if (canReload === SyncConstant.NEED_TO_RELOAD_BUT_USER_IS_DOING_SOMETHING) {
            // wait until user doesn't do any thing on pos
            timeout = setTimeout(() => AppStore.dispatch(SyncAction.reloadPage()), 1000);
        }
        return Action.empty();
    });
}

ReloadPageEpic.className = 'ReloadPageEpic';

export default EpicFactory.get(ReloadPageEpic);
