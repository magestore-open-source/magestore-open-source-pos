/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ActionTypes from 'react-redux-internet-connection/lib/react-redux-internet-connection/redux/actionTypes'
import SyncAction from "../../action/SyncAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Check Internet Online
 * @param action$
 * @returns {Observable<*|{type: string}>}
 */
function InternetOnlineEpic(action$) {
    return action$.ofType(ActionTypes.ON_LINE)
        .mergeMap(() =>
            {
                return [
                    SyncAction.syncData(),
                    SyncAction.syncActionLog()
                ];
            }
        );
}

InternetOnlineEpic.className = 'InternetOnlineEpic';

export default EpicFactory.get(InternetOnlineEpic);
