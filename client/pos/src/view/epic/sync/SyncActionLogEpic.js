/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import SyncConstant from "../../constant/SyncConstant";
import ActionLogService from "../../../service/sync/ActionLogService";
import Config from "../../../config/Config";
import ActionLogAction from "../../action/ActionLogAction";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Sync data action log
 * @param action$
 * @returns {*}
 */
function SyncActionLogEpic(action$) {
    return action$.ofType(SyncConstant.SYNC_ACTION_LOG)
        .switchMap(async function() {
            if (window.navigator.onLine) {
                if (Config.isSyncingActionLog === SyncConstant.NOT_SYNCING_ACTION_LOG) {
                    try {
                        if (Config.syncActionLogFirstLoad) {
                            await ActionLogService.resetActionsStatus();
                            Config.syncActionLogFirstLoad = false;
                        }

                        ActionLogService.saveIsSyncingActionLog(SyncConstant.SYNCING_ACTION_LOG);
                        await ActionLogService.syncActionLog();
                        let data = await ActionLogService.getAllDataActionLog();
                        if (data.some(item => item.status === SyncConstant.STATUS_REQUESTING)) {
                            return {type: "[SYNC] EMPTY"};
                        }

                        if (data.some(item => item.status === SyncConstant.STATUS_PENDING)) {
                            ActionLogService.saveIsSyncingActionLog(SyncConstant.NOT_SYNCING_ACTION_LOG);
                            return ActionLogAction.syncActionLog();
                        }
                    } catch (e) {
                        return ActionLogAction.syncActionLogSuccess();
                    }
                }
            }
            return ActionLogAction.syncActionLogSuccess();
        });
}

SyncActionLogEpic.className = 'SyncActionLogEpic';

export default EpicFactory.get(SyncActionLogEpic);
