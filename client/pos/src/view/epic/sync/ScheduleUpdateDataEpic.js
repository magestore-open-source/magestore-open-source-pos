/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {Observable} from 'rxjs';
import SyncConstant from "../../constant/SyncConstant";
import SyncAction from "../../action/SyncAction";
import SyncService from "../../../service/sync/SyncService";
import UserService from "../../../service/user/UserService";
import EpicFactory from "../../../framework/factory/EpicFactory";

/**
 * Receive action type(CHECK_SYNC_DATA_FINISH_RESULT) and send action to update data every 3 seconds
 * @param action$
 */
function ScheduleUpdateDataEpic(action$) {
    let isRunning = false;
    return action$.ofType(SyncConstant.CHECK_SYNC_DATA_FINISH_RESULT)
        .mergeMap(() => {
            if (isRunning) {
                return Observable.empty();
            } else {
                isRunning = true;
                // Reset all error updating
                SyncService.getAll().then(data => {
                    SyncService.saveToDb(data.map(syncData => {
                        syncData.updating = false;
                        return syncData;
                    }));
                });
                return Observable.concat(
                    Observable.of(SyncAction.updateData()),
                    Observable.interval(60000)
                      .takeWhile(() => {
                          let session = UserService.getSession();
                          if (!session) {
                              isRunning = false;
                              return false;
                          }
                          return true;
                      })
                      .map(() => SyncAction.updateData())
                );
            }
        });
}

ScheduleUpdateDataEpic.className = 'ScheduleUpdateDataEpic';

export default EpicFactory.get(ScheduleUpdateDataEpic);
