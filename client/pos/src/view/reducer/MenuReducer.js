/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import MenuConfig from "../../config/MenuConfig";
import LogoutPopupConstant from "../constant/LogoutPopupConstant";
import MenuConstant from "../constant/MenuConstant";


/**
 *  init state for reducer
 *
 * @type {{active: *, items: *}}
 */
const initialState = {
    items: MenuConfig.getMenuItem(),
    isOpen: false
};

/**
 * receive action from Menu Action
 *
 * @param state
 * @param action
 * @return {{active: *, items: *}}
 * @constructor
 */
export default function MenuReducer(state = initialState, action) {
    switch (action.type) {
        case MenuConstant.TOGGLE:
            return {...state,  isOpen: !state.isOpen};
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return {...initialState};
        case MenuConstant.TRIGGER_REFRESH_MENU_ITEMS: {
            return {...state, items: MenuConfig.getMenuItem()}
        }
        default:
            return state
    }
}
