/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import SyncConstant from '../constant/SyncConstant';
import LocalStorageHelper from '../../helper/LocalStorageHelper';
import SyncService from "../../service/sync/SyncService";
import ActionLogService from "../../service/sync/ActionLogService";

let mode = LocalStorageHelper.get(LocalStorageHelper.MODE);
if (!mode) {
    mode = SyncConstant.ONLINE_MODE;
}

let dataTypeMode = SyncService.getDataTypeMode();
if (!dataTypeMode) {
    dataTypeMode = SyncService.getDefaultDataTypeMode();
}

const initialState = {
    mode: mode,
    dataTypeMode: dataTypeMode
};
/**
 * receive action from Sync Action
 *
 * @param state = {sync: []}
 * @param action
 * @returns {*}
 */
const syncReducer = function (state = initialState, action) {
    switch (action.type) {
        case SyncConstant.CHANGE_MODE:
            //save mode
            SyncService.saveMode(action.mode);
            return {...state, mode: action.mode};
        case SyncConstant.CHECK_SYNC_DATA_FINISH_RESULT:
            //save mode
            SyncService.saveMode(action.isSync);
            return {...state, mode: action.isSync};
        case SyncConstant.CHANGE_DATA_TYPE_MODE:
            var dataTypeMode = {...state.dataTypeMode, ...action.dataTypeMode};
            //save data type's mode
            SyncService.saveDataTypeMode(dataTypeMode);
            return {...state, dataTypeMode: dataTypeMode};
        case SyncConstant.SET_DATA_TYPE_MODE:
            //save data type's mode
            SyncService.saveDataTypeMode(action.dataTypeMode);
            return {...state, dataTypeMode: action.dataTypeMode};
        case SyncConstant.SYNC_ACTION_LOG_SUCCESS:
            ActionLogService.saveIsSyncingActionLog(SyncConstant.NOT_SYNCING_ACTION_LOG);
            return state;
        default:
            return state;
    }
};

export default syncReducer;
