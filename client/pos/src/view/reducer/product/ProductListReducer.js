/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ProductConstant from '../../constant/ProductConstant';
import LogoutPopupConstant from "../../constant/LogoutPopupConstant";

const initialState = {products: []};

/**
 * Receive action from Product Action
 *
 * @param state = {products: []}
 * @param action
 * @returns {*}
 */
const productListReducer = function (state = initialState, action) {
    switch (action.type) {
        case ProductConstant.SEARCH_PRODUCT_RESULT: {
            const {products, search_criteria, total_count, search_key, request_mode} = action;
            return {...state, products, search_criteria, total_count, search_key, request_mode};
        }
        case ProductConstant.SYNC_ACTION_UPDATE_PRODUCT_DATA_FINISH:
            return {...state, updated_products: action.products};
        case ProductConstant.SYNC_ACTION_DELETE_PRODUCT_DATA_FINISH:
            return {...state, deleted_products: action.ids};
        case ProductConstant.SYNC_ACTION_UPDATE_STOCK_DATA_FINISH:
            return {...state, updated_stocks: action.stocks};
        case ProductConstant.SYNC_ACTION_UPDATE_CATALOG_RULE_PRICE_DATA_FINISH:
            return {...state, updated_catalogrule_prices: action.catalogrule_prices};
        case ProductConstant.SYNC_ACTION_DELETED_CATALOG_RULE_PRICE_DATA_FINISH:
            return {...state, deleted_catalogrule_prices: action.ids};
        case ProductConstant.RESET_PRODUCT_LIST:
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return {...state, products: []};
        default:
            return state
    }
};

export default productListReducer;
