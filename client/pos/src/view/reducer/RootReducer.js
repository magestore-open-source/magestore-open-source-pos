/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {combineReducers} from 'redux'
import menu from './MenuReducer';
import logout from './LogoutPopupReducer';
import exportData from './ExportDataPopupReducer';
import user from './UserReducer';
import product from './ProductReducer';
import category from './CategoryReducer';
import customer from './CustomerReducer';
import loading from './LoadingReducer';
import sync from './SyncReducer';
import config from './ConfigReducer';
import checkout from './CheckoutReducer';
import signout from './SignoutReducer';
import Extension from "../../framework/Extension";
import {reducer as internet} from 'react-redux-internet-connection';
import print from "./PrintReducer";
import order from "./OrderReducer";
import multiCheckout from "./MultiCheckoutReducer";
import onHoldOrder from "./OnHoldOrderReducer";
import scan from "./ScanReducer";

/**
 * Init core reducer
 *
 * @returns {Reducer<any>}
 */
function coreReducer() {
    return combineReducers({
        internet,
        menu,
        logout,
        exportData,
        user,
        product,
        category,
        customer,
        sync,
        loading,
        config,
        checkout,
        print,
        signout,
        order,
        multiCheckout,
        onHoldOrder,
        scan
    })
}

/**
 * Check extension reducer
 *
 * @returns {*}
 */
function extensionReducer() {
    if (Extension.ExtensionConfig.reducer && Object.keys(Extension.ExtensionConfig.reducer).length) {
        return combineReducers({...Extension.ExtensionConfig.reducer})
    }

    return false
}

/**
 * Root reducer
 *
 * @returns {*}
 */
function rootReducer() {
    let extension = extensionReducer();
    if (!extension) {
        return {
            core: coreReducer(),
        }
    }
    return {
        core: coreReducer(),
        extension: extensionReducer()
    }
}

export default combineReducers(rootReducer())

