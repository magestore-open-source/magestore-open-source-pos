/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import constant from '../constant/MultiCheckoutConstant';
import LogoutPopupConstant from '../constant/LogoutPopupConstant';
import _ from 'lodash';

const initialState = {
    carts: [],
    activeCart: false
};

/**
 * receive action from Multiple Checkout Action
 *
 * @param state
 * @param action
 * @return {{carts: Array, activeCart: boolean}}
 */
export default (state = initialState, action) => {
    switch (action.type) {
        case constant.GOT_LIST_CART:
            return {...state, carts: action.carts };
        case constant.ADDED_CART:
            return {...state, carts: state.carts.concat([action.cart])};
        case constant.SELECTED_CART:
            return {...state, activeCart: action.cart};
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return {...state, ...initialState};
        case constant.DELETED_CART:
            return {...state,
                activeCart: false,
                carts: _.remove(state.carts, cart => (_.isEqual(cart, action.cart)))
            };
        default:
            return state
    }
}
