/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ScanConstant from '../constant/ScanConstant';

const initialState = {barcodeString: "", scanPage: ""};

/**
 * Receive action from Scan Action
 *
 * @param state = {barcodeString: ""}
 * @param action
 * @returns {*}
 */
const configReducer = function (state = initialState, action) {
    switch (action.type) {
        case ScanConstant.SET_BARCODE_STRING:
            return {...state, barcodeString: action.barcodeString};
        case ScanConstant.SET_SCAN_PAGE:
            return {...state, scanPage: action.scanPage};
        default:
            return state
    }
};

export default configReducer;