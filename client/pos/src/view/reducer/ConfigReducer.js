/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ConfigConstant from '../constant/ConfigConstant';
import Config from '../../config/Config';

const initialState = {configs: []};

/**
 * receive action from Config Action
 *
 * @param state = {configs: []}
 * @param action
 * @returns {*}
 */
const configReducer = function (state = initialState, action) {
    switch (action.type) {
        case ConfigConstant.GET_CONFIG_RESULT: {
            const {configs} = action;
            Config.config = configs;
            return {...state, configs: configs};
        }
        case ConfigConstant.GET_CONFIG_ERROR:
            return state;
        // case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
        //     return initialState;
        default:
            return state
    }
};

export default configReducer;