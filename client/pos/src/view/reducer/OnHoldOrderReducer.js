/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import LogoutPopupConstant from "../constant/LogoutPopupConstant";
import { combineReducers } from 'redux';
import holdOrderList from "./onHoldOrder/HoldOrderListReducer";
import OnHoldOrderConstant from "../constant/OnHoldOrderConstant";
import {toast} from "react-toastify";
import i18n from "../../config/i18n";

const initialState = {};

const index = function (state = initialState, action) {
    switch (action.type) {
        case OnHoldOrderConstant.HOLD_ORDER_RESULT:
            toast.success(
                i18n.translator.translate(
                    'Order #{{id}} has been held successfully!',
                    {id: action.order.increment_id}
                ),
                {
                    position: toast.POSITION.BOTTOM_CENTER,
                    className: 'wrapper-messages messages-success'
                }
            );
            return {state};
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return initialState;
        default:
            return state
    }
};

export default combineReducers({
    index,
    holdOrderList
});