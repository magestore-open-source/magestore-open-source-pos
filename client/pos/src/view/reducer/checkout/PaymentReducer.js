/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import PaymentConstant from "../../constant/PaymentConstant";
import LogoutPopupConstant from "../../constant/LogoutPopupConstant";
import CheckoutConstant from "../../constant/CheckoutConstant";
import QuoteConstant from "../../constant/checkout/QuoteConstant";

const initialState = {
    payments: [],
    paymentPage: PaymentConstant.PAYMENT_PAGE_SELECT_PAYMENT,
    payment: {},
    amountPaid: 0,
    customer_id: null,
    isUpdate: false
};

/**
 * Receive action from Payment Action
 * @param state
 * @param action
 * @returns {*}
 */
const paymentReducer =  function (state = initialState, action) {
    switch (action.type) {
        case PaymentConstant.GET_LIST_PAYMENT_RESULT:
            return {...state, payments: action.payments};
        case PaymentConstant.SELECT_PAYMENT:
            return {
                ...state,
                remain: action.remain,
                payment: action.payment,
                paymentPage: PaymentConstant.PAYMENT_PAGE_EDIT_PAYMENT,
            };
        case PaymentConstant.CANCEL_PAYMENT:
            return {
                ...state,
                payment: '',
                paymentPage: PaymentConstant.PAYMENT_PAGE_SELECT_PAYMENT
            };
        case PaymentConstant.SWITCH_PAGE:
            return {
                ...state,
                paymentPage: action.paymentPage
            };
        case CheckoutConstant.CHECKOUT_TO_SELECT_PAYMENTS: {
            let {quote} = action;
            let {payments} = quote;

            if (
                !quote.grand_total
                || (!action.initPayments && quote.payments.length)
            ) {
                return {
                    ...state,
                    paymentPage: PaymentConstant.PAYMENT_PAGE_COMPLETE_ORDER,
                    customer_id: quote.customer_id
                };
            }

            if (action.initPayments || payments.length <= 0) {
                return {...initialState, payments: []};
            }
            return {...state, customer_id: quote.customer_id};
        }
        case QuoteConstant.REMOVE_CART:
            return initialState;
        case CheckoutConstant.CHECK_OUT_PLACE_ORDER_RESULT:
            return initialState;
        case PaymentConstant.RESET_STATE:
            return initialState;
        case PaymentConstant.ADD_PAYMENT:
            return {
                ...state,
                remain: action.remain,
                paymentPage: PaymentConstant.PAYMENT_PAGE_SELECT_PAYMENT
            };
        case PaymentConstant.UPDATE_PAYMENT_LIST:
            if (action.payments) {
                return {...state, isUpdate: action.isUpdate, payments: action.payments};
            }
            return {...state, isUpdate: action.isUpdate};
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return initialState;
        default:
            return state;
    }
};

export default paymentReducer;