/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ShippingConstant from "../../constant/ShippingConstant";

const initialState = {
    shipping_methods: []
};

/**
 * Receive action from Shipping Action
 * @param state
 * @param action
 * @returns {*}
 */
const shippingReducer =  function (state = initialState, action) {
    switch (action.type) {
        case ShippingConstant.GET_LIST_SHIPPING_RESULT: {
            const {shipping_methods} = action;
            return {...state, shipping_methods: shipping_methods};
        }
        default:
            return state;
    }
};

export default shippingReducer;