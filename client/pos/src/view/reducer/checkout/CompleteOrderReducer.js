/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import LogoutPopupConstant from "../../constant/LogoutPopupConstant";
import CheckoutConstant from "../../constant/CheckoutConstant";
import PaymentConstant from "../../../view/constant/PaymentConstant";

const initialState = {
    error: false,
    response: false,
    additionalData: false,
};

/**
 * Receive action from Checkout Action
 * @param state
 * @param action
 * @returns {{}}
 */
const completeOrderReducer = function (state = initialState, action) {
    switch (action.type) {
        case CheckoutConstant.CHECKOUT_TO_SELECT_PAYMENTS: {
            if(action.initPayments) {
                return {...initialState};
            }
            return state;
        }
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return {...initialState};
        case CheckoutConstant.CHECKOUT_PROCESS_SINGLE_PAYMENT_RESULT:{
            const { result } = action;
            return {
                ...state,
                response: result.response,
                additionalData: result.response && result.response.additionalData,
                error: false
            };
        }
        case CheckoutConstant.CHECKOUT_PROCESS_SINGLE_PAYMENT_ERROR: {
            const {result} = action;
            return {
                ...state,
                response: false,
                error: result.message,
            };
        }

        case PaymentConstant.PAYMENTS_STATUS_TO_PENDING: {
            return {...state, error: false};
        }
        default:
            return state;
    }
};

export default completeOrderReducer;