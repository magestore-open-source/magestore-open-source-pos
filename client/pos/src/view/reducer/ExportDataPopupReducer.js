/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ExportDataPopupConstant from "../constant/ExportDataPopupConstant";

/**
 * initial State for reducer
 *
 * @type {{isOpen: boolean, success: {}, error: {}}}
 */
const initState = {
    isOpen: false,
    success: {},
    error: {}
};

/**
 * receive action from Export Data Popup Action
 *
 * @param state
 * @param action
 * @returns {{isOpen: boolean, success: {}, error: {}}}
 * @constructor
 */
export default function ExportDataPopupReducer(state = initState, action) {
    switch (action.type) {
        case ExportDataPopupConstant.CLICK_MODAL_BACKDROP:
            return {...state , isOpen: !state.isOpen };
        case ExportDataPopupConstant.FINISH_EXPORT_REQUESTING:
            return {...state , ...{ success: action.response, isOpen: !state.isOpen }};
        default: return state
    }
}