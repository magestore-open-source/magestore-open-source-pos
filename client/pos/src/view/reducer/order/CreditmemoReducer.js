/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CreditmemoConstant from '../../constant/order/CreditmemoConstant';
import LogoutPopupConstant from "../../constant/LogoutPopupConstant";

const initialState = {
    creditmemo: {items: []},
    order_creditmemo_create_account: {},
    error: false,
    response: false
};

/**
 * receive action from Credimemo Action
 *
 * @param state = {configs: []}
 * @param action
 * @returns {*}
 */
const orderReducer = function (state = JSON.parse(JSON.stringify(initialState)), action) {
    switch (action.type) {
        case CreditmemoConstant.SET_CREDITMEMO:
            return {...state, creditmemo: action.creditmemo};
        case CreditmemoConstant.RESET_CREDITMEMO:
            return {...state, creditmemo: {}, error: false, response: false};
        case CreditmemoConstant.CREDITMEMO_CREATE_CUSTOMER_RESULT:
            return {...state, order_creditmemo_create_account: action.order};
        case CreditmemoConstant.CREDITMEMO_PROCESS_SINGLE_PAYMENT_RESULT: {
            const {result, index} = action;
            return {
                ...state,
                response: {...result.response, index},
                error: false
            };
        }
        case CreditmemoConstant.CREDITMEMO_PROCESS_SINGLE_PAYMENT_ERROR: {
            const { result, index } = action;
            return {
                ...state,
                response: false,
                error: { message: result.message, index },
            };
        }
        case CreditmemoConstant.CREDITMEMO_PROCESS_PAYMENT: {
            return { ...state, error: false, response: false };
        }
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return JSON.parse(JSON.stringify(initialState));
        default:
            return state
    }
};

export default orderReducer;