/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import LogoutPopupConstant from "../../constant/LogoutPopupConstant";
import OrderConstant from "../../constant/OrderConstant";

const initialState = {orders: []};

/**
 * receive action from Config Action
 *
 * @param state = {configs: []}
 * @param action
 * @returns {*}
 */
const orderListReducer = function (state = initialState, action) {
    switch (action.type) {
        case OrderConstant.SEARCH_ORDER_RESULT:
            return {
                ...state,
                orders: action.orders,
                search_criteria: action.search_criteria,
                total_count: action.total_count,
                search_key: action.search_key,
                searchAllTime: action.searchAllTime,
                request_mode: action.request_mode
            };
        case OrderConstant.SYNC_ACTION_UPDATE_DATA_FINISH:
            return {...state, updated_orders: action.orders};
        case OrderConstant.RESET_UPDATED_ORDERS_LIST:
            return {...state, updated_orders: []};
        case OrderConstant.SYNC_DELETED_ORDER_FINISH:
            return {...state, deleted_order_ids: action.ids};
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return initialState;
        default:
            return state
    }
};

export default orderListReducer;
