/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ProductConstant from '../constant/ProductConstant';
import ProductListReducer from './product/ProductListReducer';
import ViewProductReducer from './product/ViewProductReducer';
import { combineReducers } from 'redux'
import LogoutPopupConstant from "../constant/LogoutPopupConstant";

const initialState = {products: []};

/**
 * receive action from Product Action
 *
 * @param state = {products: []}
 * @param action
 * @returns {*}
 */
const productReducer = function (state = initialState, action) {
    switch (action.type) {
        case ProductConstant.GET_LIST_PRODUCT_RESULT: {
            const {products} = action;
            return {...state, products: products};
        }
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return initialState;
        default:
            return state
    }
};

export default combineReducers({
    productReducer,
    productList: ProductListReducer,
    viewProduct: ViewProductReducer
});
