/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import * as PrintConstant from "../constant/PrintConstant";
import OrderConstant from "../constant/OrderConstant";

/**
 *  initial State for reducer
 *
 * @type {{isOpen: boolean}}
 */
const initState = {
    showLoading: false
};

/**
 * receive action from Checkout Action
 * 
 * @param state
 * @param action
 * @returns {*}
 */
export default function PrintReducer(state = initState, action) {
    switch (action.type) {
        case OrderConstant.PLACE_ORDER_AFTER:
            return {
                ...state,
                orderData: action.order,
                isReprint: false,
                quote: action.quote,
                showLoading: true
            };
        case PrintConstant.FINISH_PRINT:
            return {
                ...state,
                orderData: null,
                isReprint: false,
                quote: null,
                creditmemo: null,
                showLoading: false
            };
        case OrderConstant.REPRINT_ORDER:
            return {
                ...state,
                orderData: action.order,
                isReprint: true,
                showLoading: true
            };
        case  OrderConstant.PRINT_CREDITMEMO:
            return {
                ...state,
                creditmemo: action.creditmemo,
                showLoading: true
            };
        default: return state
    }
}