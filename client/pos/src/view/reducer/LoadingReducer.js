/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import LoadingConstant from '../constant/LoadingConstant';
import LogoutPopupConstant from "../constant/LogoutPopupConstant";
import ConfigConstant from "../constant/ConfigConstant";
import {fire} from "../../event-bus";
import ColorSwatchConstant from "../constant/ColorSwatchConstant";
import PaymentConstant from "../constant/PaymentConstant";
import ShippingConstant from "../constant/ShippingConstant";
import OrderConstant from "../constant/OrderConstant";

const initialState = {
    finishedList: [],
    count: 0,
    total: 6,
    dataList: [
        ConfigConstant.TYPE_GET_CONFIG,
        ColorSwatchConstant.TYPE_GET_COLOR_SWATCH,
        PaymentConstant.TYPE_GET_PAYMENT,
        ShippingConstant.TYPE_GET_SHIPPING,
        OrderConstant.TYPE_GET_LIST_ORDER_STATUSES,
    ],
};
// event to modify initial state
fire('reducer_loading_define_initial_state_after', {initialState});
initialState.total = initialState.dataList.length;

/**
 * receive action from Loading Action
 *
 * @param state = initialState
 * @param action
 * @returns {*}
 */
const loadingReducer =  function (state = initialState, action) {
    switch (action.type) {
        case LoadingConstant.UPDATE_FINISHED_LIST: {
            let finishedList = state.finishedList;
            if (finishedList.indexOf(action.dataType) < 0) {
                finishedList.push(action.dataType);
            }
            return {...state, finishedList: finishedList, count: finishedList.length};
        }
        case LoadingConstant.RESET_STATE: {
            return initialState;
        }
        case LoadingConstant.CLEAR_DATA_ERROR: {
            return state;
        }
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING: {
            return initialState;
        }
        default: {
            return state;
        }
    }
};

export default loadingReducer;
