/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CategoryConstant from '../constant/CategoryConstant';
import LogoutPopupConstant from "../constant/LogoutPopupConstant";

const initialState = {
    categories: [],
    parentCategory: null,
};

/**
 * receive action from Category Action
 * @param state = {categories: []}
 * @param action
 * @returns {*}
 */
const categoryReducer = function (state = initialState, action) {
    switch (action.type) {
        case CategoryConstant.GET_LIST_CATEGORY_RESULT: {
            const {
                parentCategory,
                categories,
                search_criteria,
                total_count,
                parent_id,
                requestMode
            } = action;
            return {
                ...state,
                parentCategory: parentCategory,
                categories: categories,
                search_criteria: search_criteria,
                total_count: total_count,
                parent_id: parent_id,
                requestMode: requestMode
            };
        }
        case LogoutPopupConstant.FINISH_LOGOUT_REQUESTING:
            return initialState;
        default:
            return state
    }
};

export default categoryReducer;
