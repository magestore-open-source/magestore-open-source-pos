/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Component, Fragment} from 'react';
import Menu from './component/menu';
import {Route, Switch} from 'react-router-dom'
import MenuConfig from "../config/MenuConfig";
import ComponentFactory from "../framework/factory/ComponentFactory";
import ContainerFactory from "../framework/factory/ContainerFactory";
import CoreContainer from "../framework/container/CoreContainer";
import i18n from "../config/i18n";
import {toast} from "react-toastify";
import SignoutAction from "./action/SignoutAction";
import PrintContainer from "./component/print/PrintContainer";
import layout from "../framework/Layout";
import PropTypes from "prop-types";

class Index extends Component {
    static className = 'Index';

    /**
     * Component Will Receive Props
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        if(nextProps.signout !== undefined && nextProps.signout.message) {
            this.props.forceSignOutSuccess();
            toast.error(
                i18n.translator.translate(nextProps.signout.message),
                {
                    className: 'wrapper-messages messages-warning',
                    autoClose: 5000
                }
            );
        }
        if(nextProps.signout.page) {
            this.props.history.replace(nextProps.signout.page);
        }
    }

    /**
     * Render root template
     * @returns {*}
     */
    render() {

        const embedeeds =  MenuConfig.getMenuItem().filter(menu => {
            return menu.isEmbedded;
        });

        const singlePages =  MenuConfig.getMenuItem().filter(menu => {
            return !menu.isEmbedded;
        });

        return (
            <Fragment>
                <Menu/>
                {
                    embedeeds.map(item => {
                        let TargetComponent = item.component;
                        return <TargetComponent key={item.id}/>
                    })
                }
                <Switch>
                    {
                        singlePages.map(item => {
                            return <Route key={item.id}
                                          path={item.path} component={item.component}/>
                        })
                    }
                </Switch>
                <PrintContainer/>
                {layout('view')('index')('print_after')()(this)}
            </Fragment>
        )
    }
}

Index.propTypes = {
    history: PropTypes.any,
    signout: PropTypes.any,
    forceSignOutSuccess: PropTypes.any
};

export class IndexContainer extends CoreContainer {
    static className = 'IndexContainer';

    /**
     * Map states
     *
     * @param state
     * @returns {{user: {connection, error, logoUrl: *|string}}}
     */
    static mapState(state) {
        const {signout} = state.core;
        return {signout};
    }

    /**
     * Map dispatch to props
     * @param dispatch
     * @returns {{forceSignOutSuccess: function(): *}}
     */
    static mapDispatch(dispatch) {
        return {
            forceSignOutSuccess: () => dispatch(SignoutAction.forceSignOutSuccess())
        };
    }
}

export default ContainerFactory.get(IndexContainer).withRouter(
    ComponentFactory.get(Index)
)
