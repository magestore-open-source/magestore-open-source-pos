/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreComponent from '../../../framework/component/CoreComponent';
import ComponentFactory from '../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../framework/container/CoreContainer';
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import MenuConfig from "../../../config/MenuConfig";

export class SessionManagement extends CoreComponent {
    static className = 'SessionManagement';

    /**
     * template
     * @returns {*}
     */
    template() {
        let win = window.open(
            'https://www.magestore.com/manage-sessions/?utm_source=pos-open-frontend&utm_medium=product&utm_campaign=upsell_commerce_2021&utm_content=pos-menu',
            '_blank'
        );
        win.focus();
        this.props.history.replace(MenuConfig.defaultItem().path);
        return null;
    }
}

class SessionManagementContainer extends CoreContainer {
    static className = 'SessionManagementContainer';
}

/**
 * @type {SessionManagement}
 */
export default ContainerFactory.get(SessionManagementContainer).withRouter(
    ComponentFactory.get(SessionManagement)
);