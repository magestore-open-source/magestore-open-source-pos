/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from 'react';
import CoreComponent from '../../../../framework/component/CoreComponent';
import ComponentFactory from '../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../framework/factory/ContainerFactory";
import ProductAction from "../../../action/ProductAction";
import '../../../style/css/Option.css'
import ConfigurablePopup from './view/Configurable';
import BundlePopup from './view/Bundle';
import GroupedPopup from './view/Grouped';
import CustomProduct from './view/CustomProduct';
import cloneDeep from 'lodash/cloneDeep';
import ProductTypeConstant from "../../../constant/ProductTypeConstant";
import $ from "jquery";

export class ProductViewComponent extends CoreComponent {
    static className = 'ProductViewComponent';
    modal_body = null;

    setModalAddToCartElement = element => this.modal_add_to_cart = element;
    setModalDialogElement = element => this.modal_dialog = element;

    /**
     * constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            configOptions: []
        }
    }

    componentDidUpdate(prevProps) {
        if (this.modal_add_to_cart && this.modal_dialog && this.getProduct() &&
            (!prevProps.product || !prevProps.product.id || prevProps.product.id !== this.getProduct().id)
        ) {
            let height = $( window ).height();
            this.modal_dialog.style.height = height + 'px';
            /*$(elm).css('height', height + 'px');
            let modalHeight = this.modal_add_to_cart.getBoundingClientRect().height;
            this.modal_dialog.style.height = modalHeight + 'px';*/
        }
    }

    /**
     * Get product
     *
     * @returns {string}
     */
    getProduct() {
        return this.props.product;
    }

    /**
     * Get Product type
     *
     * @returns {string}
     */
    getProductType() {
        return this.props.product && this.props.product.type_id ? this.props.product.type_id : '';
    }

    /**
     * set config options
     * @param configOptions
     */
    setConfigOptions(configOptions) {
        if (
            JSON.stringify(this.state.configOptions) !== JSON.stringify(configOptions)
        ) {
            this.setState({
                configOptions: cloneDeep(configOptions)
            });
        }
    }

    /**
     * Render template
     *
     * @returns {*}
     */
    template() {
        let popupId = 'popup-'
                        + (this.getProductType() === ProductTypeConstant.SIMPLE ? 'custom' : this.getProductType())
                        + '-product';
        let modalClass = 'modal fade popup-addtocart ' + popupId;
        let fadeClass = 'popup-catalog modal-backdrop fade';
        if (this.getProduct() && this.getProduct().id) {
            modalClass += ' in';
            fadeClass += ' in';
        }
        return (
            <Fragment>
                <div className={modalClass} data-backdrop="static" id={popupId} tabIndex="-1"
                     ref={this.setModalAddToCartElement}
                     role="dialog">
                    <div className="modal-dialog" role="document" ref={this.setModalDialogElement}>
                        <div style={{height: '100%'}}>
                            <ConfigurablePopup product={this.props.product}
                                               closePopup={() => this.props.actions.closePopup()}
                                               setConfigOptions={this.setConfigOptions.bind(this)}
                            />
                            <BundlePopup product={this.props.product}
                                         closePopup={() => this.props.actions.closePopup()}
                            />
                            <GroupedPopup product={this.props.product}
                                          closePopup={() => this.props.actions.closePopup()}
                            />
                            <CustomProduct product={this.props.product}
                                           closePopup={() => this.props.actions.closePopup()}/>
                        </div>
                    </div>
                </div>
                <div className={fadeClass}/>
            </Fragment>
        );
    }
}

class ProductViewContainer extends CoreContainer {
    static className = 'ProductViewContainer';

    /**
     * This maps the state to the property of the component
     *
     * @param state
     * @returns {{product}}
     */
    static mapState(state) {
        let {product} = state.core.product.viewProduct;
        return {product: product};
    }

    /**
     * This maps the state to the property of the component
     *
     * @param dispatch
     * @returns {{product}}
     */
    static mapDispatch(dispatch) {
        return {
            actions: {
                closePopup: () => dispatch(ProductAction.viewProduct()),
            }
        }
    }
}

export default ContainerFactory.get(ProductViewContainer).withRouter(
    ComponentFactory.get(ProductViewComponent)
);

