/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreComponent from '../../../../../framework/component/CoreComponent';
import ComponentFactory from '../../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import OptionConstant from "../../../../constant/catalog/OptionConstant";
import CurrencyHelper from "../../../../../helper/CurrencyHelper";

export class ProductAbstractOptionComponent extends CoreComponent {
    static className = 'ProductAbstractOptionComponent';

    /**
     * get display price of option
     * @param price
     * @param priceType
     * @param productPrice
     * @return {string}
     */
    getOptionDisplayPrice(price, priceType, productPrice) {
        let realPrice = price;
        if (priceType === OptionConstant.PRICE_TYPE_PERCENT) {
            realPrice = (productPrice * price) / 100;
        }
        return realPrice ? ('+ ' + CurrencyHelper.convertAndFormat(realPrice) ) : '';
    }
}

class ProductAbstractOptionsContainer extends CoreContainer {
    static className = 'ProductAbstractOptionsContainer';
}

export default ContainerFactory.get(ProductAbstractOptionsContainer).withRouter(
    ComponentFactory.get(ProductAbstractOptionComponent)
);

