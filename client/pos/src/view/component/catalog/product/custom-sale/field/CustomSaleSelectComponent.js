/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from 'react';
import PropTypes from 'prop-types';
import CoreComponent from '../../../../../../framework/component/CoreComponent'
import ComponentFactory from "../../../../../../framework/factory/ComponentFactory";

export class CustomSaleSelectComponent extends CoreComponent {
    static className = 'CustomSaleSelectComponent';
    select;

    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            options: props.Options,
            value: props.DefaultValue
        }
    }

    /**
     * Component will receive props
     *
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        if(nextProps.isNew){
            this.setState({
                value: this.props.DefaultValue
            });
            this.props.onSelect(this.props.Code, this.props.DefaultValue);
        }
    }

    /**
     * Set select
     *
     * @param select
     */
    setSelect(select) {
        this.select = select;
    }

    /**
     * On change
     *
     * @param event
     */
    onChange(event) {
        this.setState({
            value: event.target.value
        });
        if (this.props.onSelect) {
            this.props.onSelect(this.props.Code, event.target.value);
        }
    }

    template() {
        let { Code, Label, OneRow, KeyValue, KeyTitle } = this.props;
        let id = "custom-sale-" + Code;
        return (
            <div className={OneRow ? "col-sm-12" : "col-sm-6"}>
                <label htmlFor={id}> { Label } </label>
                <select
                    id={id}
                    className="form-control"
                    ref={this.setSelect.bind(this)}
                    value={ this.state.value }
                    onChange={this.onChange.bind(this)}>
                    {
                        this.state.options ? this.state.options.map((option, index) => {
                            return (<option key={index} value={option[KeyValue]}> {option[KeyTitle]} </option>)
                        }) : null
                    }
                </select>
            </div>
        )
    }
}

CustomSaleSelectComponent.propTypes = {
    Label: PropTypes.string,
    Options: PropTypes.array,
    KeyValue: PropTypes.string,
    KeyTitle: PropTypes.string,
    OneRow: PropTypes.bool,
    Code: PropTypes.string,
};

/**
 * CustomSaleSelectComponent
 */
export default ComponentFactory.get(CustomSaleSelectComponent);
