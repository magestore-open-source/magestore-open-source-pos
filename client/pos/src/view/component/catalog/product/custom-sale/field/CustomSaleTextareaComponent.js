/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from 'react';
import CoreComponent from '../../../../../../framework/component/CoreComponent'
import Textarea from "react-textarea-autosize";
import PropTypes from "prop-types"
import ComponentFactory from "../../../../../../framework/factory/ComponentFactory";

export class CustomSaleTextareaComponent extends CoreComponent {
    static className = 'CustomSaleTextareaComponent';
    input;

    /**
     * Component will receive props
     *
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        if(nextProps.isNew){
            if (this.input) {
                this.input.value = "";
                this.props.inputFieldOnChange(this.props.Code, null);
            }
        }
    }

    /**
     * Set text area
     *
     * @param input
     */
    setInput(input) {
        this.input = input;
    }

    /**
     * On change text area
     */
    onChange() {
        if (this.props.inputFieldOnChange) {
            this.props.inputFieldOnChange(this.props.Code, this.input.value);
        }
    }

    /**
     * Clear input
     */
    clear() {
        if (this.input) {
            this.input.focus();
            this.input.value = "";
            this.props.inputFieldOnChange(this.props.Code, null);
        }
    }

    template() {
        let { Code, Label , OneRow} = this.props;
        let id = "custom-sale-" + Code;
        let style = {
            resize: "none",
            padding: "0px",
        };
        return (
            <div className={OneRow ? "col-sm-12" : "col-sm-6"}>
                <label htmlFor={id}>{ Label }</label>
                <Textarea
                    id={id}
                    className="form-control"
                    inputRef={this.setInput.bind(this)}
                    onChange={this.onChange.bind(this)}
                    maxRows={3}
                    style={style}
                />
            </div>
        );
    }
}

CustomSaleTextareaComponent.propTypes = {
    Label: PropTypes.string,
    OneRow: PropTypes.bool,
    Code: PropTypes.string,
};

/**
 * CustomSaleTextareaComponent
 */
export default ComponentFactory.get(CustomSaleTextareaComponent);
