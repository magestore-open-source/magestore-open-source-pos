/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from 'react';
import CoreComponent from '../../../../../../framework/component/CoreComponent'
import NumPad from "../../../../lib/react-numpad";
import CurrencyHelper from "../../../../../../helper/CurrencyHelper";
import NumberHelper from "../../../../../../helper/NumberHelper";
import ComponentFactory from "../../../../../../framework/factory/ComponentFactory";

export class CustomSaleNumberComponent extends CoreComponent {
    static className = 'CustomSaleNumberComponent';


    /**
     * Constructor
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            input_value: props.DefaultValue,
            canClear: false,
            decimal_symbol: CurrencyHelper.getCurrencyFormat().decimal_symbol
        };
        this.getContainer = this.getContainer.bind(this);
        this.isTouch = false
    }

    /**
     * get container
     * @param ref
     */
    getContainer(ref) {
        this.container = ref
    }

    /**
     * Component will receive props
     *
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        if (nextProps.isNew) {
            this.setState({
                input_value: this.props.DefaultValue,
                canClear: false
            });
            if (this.props.inputFieldOnChange) {
                this.props.inputFieldOnChange(this.props.Code, this.props.DefaultValue);
            }
        }
    }
    /**
     * Change input value
     *
     * @param value
     */
    changeInputValue(value) {
        this.setState({
            input_value: value,
            canClear: value !== this.props.DefaultValue
        });
        if (this.props.inputFieldOnChange) {
            this.props.inputFieldOnChange(this.props.Code, value);
        }
    }

    /**
     * Render template
     *
     * @return {*}
     */
    template() {
        let {Code, Label, DefaultValue, OneRow} = this.props;
        let id = "custom-sale-" + Code;
        return (
            <div ref={this.getContainer} className={OneRow ? "col-sm-12" : "col-sm-6"}>
                <label htmlFor={id}>{Label}</label>
                <NumPad.CustomNumber
                    key={id}
                    onChange={this.changeInputValue.bind(this)}
                    position="centerLeft"
                    value={DefaultValue}
                    isDecimal={true}
                    decimalSeparator={this.state.decimal_symbol}
                    min={0}
                    isShowAction={true}
                    arrow="left">
                        <span className="form-control">
                            {NumberHelper.formatDisplayGroupAndDecimalSeparator(this.state.input_value)}
                        </span>
                </NumPad.CustomNumber>
            </div>
        );
    }
}

/**
 * CustomSaleNumberComponent
 */
export default ComponentFactory.get(CustomSaleNumberComponent);
