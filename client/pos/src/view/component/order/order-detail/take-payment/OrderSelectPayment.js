/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {CoreComponent} from "../../../../../framework/component/index";
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import CoreContainer from "../../../../../framework/container/CoreContainer";
import ComponentFactory from "../../../../../framework/factory/ComponentFactory";
import TakePaymentItem from "./TakePaymentItem";
import '../../../../style/css/SelectPayment.css';
import SmoothScrollbar from "smooth-scrollbar";
import $ from "jquery";
import TakePaymentConstant from "../../../../constant/order/TakePaymentConstant";
import PaymentAction from "../../../../action/PaymentAction";
import OrderHelper from "../../../../../helper/OrderHelper";

export class OrderSelectPayment extends CoreComponent {
    static className = 'OrderSelectPayment';
    setPopupPaymentElement = element => {
        this.popup_payment = element;
        if (!this.scrollbar && this.popup_payment) {
            this.scrollbar = SmoothScrollbar.init(this.popup_payment);
            this.heightPopup('.wrapper-payment');
        }
    };

    /**
     * constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            customer: props.customer,
            isLoading: false
        }
    }

    /**
     * componentWillMount: load list payment when start
     */
    componentWillMount() {
        this.props.actions.getListPayment();
    }

    /**
     * This function after mapStateToProps then set list payment to state
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        this.setState({
            items: nextProps.payments
        }, async () => {
            this.setState({isLoading: false});
        });
    }

    /**
     * select payment
     * @param payment
     */
    selectPayment(payment) {
        this.props.selectPayment(payment, this.props.remain);
        this.props.setCustomer(this.state.customer);
    }

    /**
     * back to complete payment page
     */
    handleBack() {
        this.props.switchPage(TakePaymentConstant.PAYMENT_PAGE_COMPLETE_PAYMENT);
    }

    /**
     *  setWrapperPayment
     * @param content
     */
    setWrapperPayment(content) {
        if (content && !this.wrapperPayment) {
            this.wrapperPayment = content;
        }
    }

    /**
     * get height popup
     * @param elm
     */
    heightPopup(elm) {
        let height = $(window).height();
        $(elm).css('height', height + 'px');
    }


    template() {
        let {order, remain} = this.props;
        let {isLoading} = this.state;
        let isRemain = (remain < order.grand_total);
        let addedNewPayment = Array.isArray(order.payments) ? order.payments.filter(payment => !payment.is_paid) : [];
        let classContentRemain = addedNewPayment.length ?
            "block-content block-content1" : "block-content block-content1 no-remain";
        let classContent = isLoading ? "hidden" : classContentRemain;
        return (
            <Fragment>
                <div className="wrapper-payment full-width active" id="wrapper-payment1"
                     ref={this.setWrapperPayment.bind(this)}>
                    <div className="block-title">
                        <button className="btn-cannel" onClick={() => this.props.cancelTakePayment()}>
                            {this.props.t('Cancel')}
                        </button>
                        <strong className="title">
                            {this.props.t('Take Payment Order #{{orderId}}', {orderId: order.increment_id})}
                        </strong>
                    </div>
                    <div className={classContent} data-scrollbar
                         ref={this.setPopupPaymentElement} tabIndex={1}>
                        <ul className="payment-total">
                            <li>
                                <span className="label">{this.props.t((isRemain ? 'Remaining' : 'Total'))}</span>
                                <span className="value">{OrderHelper.formatPrice(remain, order)}</span>
                            </li>
                        </ul>
                        <ul className="payment-list">
                            {
                                this.state.items.map((payment) => {
                                    return <TakePaymentItem key={payment.code}
                                                        remain={this.props.remain}
                                                        order={order}
                                                        payment={payment}
                                                        selectPayment={(payment) => this.selectPayment(payment)}/>;
                                })
                            }
                        </ul>
                    </div>
                    {
                        addedNewPayment.length ?
                            <div className="block-bottom">
                                <div className="actions-accept">
                                    <button className="btn btn-default btn-cannel" type="button"
                                            onClick={() => this.handleBack()}>
                                        {this.props.t("Back")}
                                    </button>
                                </div>
                            </div>
                            :
                            ''
                    }
                    {
                        <div className="loader-product"
                             style={{display: isLoading ? 'block' : 'none'}}>
                        </div>
                    }
                </div>
            </Fragment>
        )
    }
}

OrderSelectPayment.propTypes = {
    selectPayment: PropTypes.func
};

class OrderSelectPaymentContainer extends CoreContainer {
    static className = 'OrderSelectPaymentContainer';

    /**
     * map state to props
     * @param state
     * @return {{payments: *}}
     */
    static mapState(state) {
        let {payments} = state.core.order.takePayment;
        return {payments: payments};
    }

    /**
     * map dispatch to props
     * @param dispatch
     * @return {{actions: {getListPayment: function(): *}}}
     */
    static mapDispatch(dispatch) {
        return {
            actions: {
                getListPayment: () => dispatch(PaymentAction.getListPayment()),
            }
        }
    }
}

export default ContainerFactory.get(OrderSelectPaymentContainer).withRouter(
    ComponentFactory.get(OrderSelectPayment)
);