/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from 'react';
import PropTypes from 'prop-types';
import {CoreComponent} from "../../../../../framework/component/index";
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import CoreContainer from "../../../../../framework/container/CoreContainer";
import ComponentFactory from "../../../../../framework/factory/ComponentFactory";

export class TakePaymentItem extends CoreComponent {
    static className = 'TakePaymentItem';

    /**
     * select payment
     * @param payment
     */
    selectPayment(payment) {
        this.props.selectPayment(payment);
    }

    /**
     * Render template
     *
     * @return {*}
     */
    template() {
        let {payment} = this.props;
        let code = payment.code;
        let payment_title = payment.title;
        let payment_subtitle = payment.sub_title;
        return (
            <li onClick={() => this.selectPayment(payment)}>
                <span className={"img image-default image-" + code }/>
                <span className="text">
                    {
                        this.props.t(payment_title) + (payment_subtitle ? ' ' + payment_subtitle : '')
                    }
                </span>
            </li>
        )
    }
}

TakePaymentItem.propTypes = {
    selectPayment: PropTypes.func
};

class TakePaymentItemContainer extends CoreContainer {
    static className = 'TakePaymentItemContainer';
}

export default ContainerFactory.get(TakePaymentItemContainer).withRouter(
    ComponentFactory.get(TakePaymentItem)
)