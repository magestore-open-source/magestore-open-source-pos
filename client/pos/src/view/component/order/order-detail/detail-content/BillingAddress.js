/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from "react";
import ComponentFactory from '../../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import CoreComponent from "../../../../../framework/component/CoreComponent";
import AddressConstant from "../../../../constant/checkout/quote/AddressConstant";
import OrderService from "../../../../../service/sales/OrderService";

export class BillingAddress extends CoreComponent {
    static className = 'BillingAddress';

    /**
     * template
     * @returns {*}
     */
    template() {
        let {order} = this.props;
        let address = order.addresses.find(item => item.address_type === AddressConstant.BILLING_ADDRESS_TYPE);
        if (!address) {
            return null;
        }
        return (
            <ul className="billing-address">
                <li>
                    <div className="title">{address.firstname + " " + address.lastname}</div>
                </li>
                <li>
                    <div className="title">{OrderService.getFullAddress(address)}</div>
                </li>
            </ul>
        );
    }
}

class BillingAddressContainer extends CoreContainer {
    static className = 'BillingAddressContainer';
}

/**
 * @type {BillingAddress}
 */
export default ContainerFactory.get(BillingAddressContainer).withRouter(
    ComponentFactory.get(BillingAddress)
);