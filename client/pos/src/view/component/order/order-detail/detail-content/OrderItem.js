/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from "react";
import ComponentFactory from '../../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import CoreComponent from "../../../../../framework/component/CoreComponent";
import OrderService from "../../../../../service/sales/OrderService";
import ProductTypeConstant from "../../../../constant/ProductTypeConstant";
import OrderHelper from "../../../../../helper/OrderHelper";
import StatusConstant from "../../../../constant/order/StatusConstant";
import OrderItemService from "../../../../../service/sales/order/OrderItemService";
import NumberHelper from "../../../../../helper/NumberHelper";
import layout from "../../../../../framework/Layout";

export class OrderItem extends CoreComponent {
    static className = 'OrderItem';

    /**
     * Get display price
     * @param price
     * @param order
     * @return {*}
     */
    getDisplayPrice(price, order) {
        return OrderHelper.formatPrice(price, order);
    }

    /**
     * get option detail like as custom, bundle, configurable option
     * @param item
     * @return {*}
     */
    displayOption(item) {
        let productOptions = (item.product_options && !Array.isArray(item.product_options)) ?
            JSON.parse(item.product_options) : null;
        if(!productOptions) {
            return '';
        }
        let result = [];
        if (
            item.product_type === ProductTypeConstant.SIMPLE
            || item.product_type === ProductTypeConstant.VIRTUAL
        ) {
            result.push(...this.displayCustomOption(productOptions));
        }
        if (item.product_type === ProductTypeConstant.CONFIGURABLE) {
            result.push(...this.displayConfigurableOption(productOptions));
            result.push(...this.displayCustomOption(productOptions));
        }

        return result.join(', ');
    }

    /**
     * display custom options
     * @param productOptions
     * @return {Array}
     */
    displayCustomOption(productOptions) {
        return OrderItemService.getCustomOption(productOptions);
    }

    /**
     * Display option of configurable item
     * @param productOptions
     * @return {*}
     */
    displayConfigurableOption(productOptions) {
        return OrderItemService.getConfigurableOption(productOptions);
    }

    isShowShipped(){
        let {order, item} = this.props;
        if (!item.qty_shipped){
            return false;
        }
        if (item.product_type === ProductTypeConstant.BUNDLE){
            return !OrderItemService.isShipSeparately(item, order);
        } else if (OrderItemService.getParentItem(item, order)) {
            let parent = OrderItemService.getParentItem(item, order);
            return (parent.product_type === ProductTypeConstant.BUNDLE && OrderItemService.isShipSeparately(parent, order));
        }
        return true;
    }

    canShowPriceInfo() {
        let {order, item} = this.props;
        return ((OrderItemService.getParentItem(item, order) && OrderItemService.isChildrenCalculated(item, order)) || (!OrderItemService.getParentItem(item, order) && !OrderItemService.isChildrenCalculated(item, order)));
    }

    canShowInvoiceRefundCancel() {
        let {order, item} = this.props;
        if (item.product_type === ProductTypeConstant.BUNDLE){
            return !OrderItemService.isChildrenCalculated(item, order);
        } else if (OrderItemService.getParentItem(item, order)) {
            let parent = OrderItemService.getParentItem(item, order);
            return (parent.product_type === ProductTypeConstant.BUNDLE && OrderItemService.isChildrenCalculated(parent, order));
        }
        return true;
    }

    /**
     * check display original price
     * @param item
     * @returns {*|boolean}
     */
    displayOriginPrice(item) {
        return item && typeof item.pos_base_original_price_incl_tax !== 'undefined' && item.pos_base_original_price_incl_tax != null
            && typeof item.pos_original_price_excl_tax !== 'undefined' && item.pos_original_price_excl_tax != null;
    }

    /**
     * template
     * @returns {*}
     */
    template() {
        let {order, item} = this.props;
        let isHolded = (order.status === StatusConstant.STATUS_HOLDED);
        let canShowInvoiceRefundCancel = this.canShowInvoiceRefundCancel();

        if (item.parent_item_id) {
            let parent = order.items.find(x => Number(x.item_id) === Number(item.parent_item_id));
            if (parent && parent.product_type !== ProductTypeConstant.BUNDLE) {
                return null;
            }
        }

        return (
            <div className={item.parent_item_id?"item-ordered has-parent-order-item":'item-ordered'}>
                <div className="item-detail">
                    <div className="name">{item.name}</div>
                    <div className="sku">{'[' + item.sku + ']'}</div>
                    <div className="option">
                        {
                            this.displayOption(item)
                        }
                    </div>
                    {layout('order')('order_detail')('detail_content')('order_item')('order_item_name_option_after')()(this, item)}
                    <div className="item-status">
                        <span>{this.props.t("Ordered: {{qty}}",{qty: NumberHelper.formatDisplayGroupAndDecimalSeparator(item.qty_ordered)})}</span>
                        {
                            !isHolded ?
                                <Fragment>
                                    {
                                        canShowInvoiceRefundCancel && item.qty_invoiced ?
                                            <span>
                                                {
                                                    this.props.t(
                                                        "Invoiced: {{qty}}",
                                                        {qty: (item.qty_invoiced ? NumberHelper.formatDisplayGroupAndDecimalSeparator(item.qty_invoiced) : 0)}
                                                    )
                                                }
                                            </span>
                                            : null
                                    }
                                    {
                                        this.isShowShipped() ?
                                            <span>
                                                {
                                                    this.props.t(
                                                        "Shipped: {{qty}}",
                                                        {qty: (item.qty_shipped ? NumberHelper.formatDisplayGroupAndDecimalSeparator(item.qty_shipped) : 0)}
                                                    )
                                                }
                                            </span>
                                            : null
                                    }
                                    {
                                        canShowInvoiceRefundCancel && item.qty_refunded ?
                                            <span>
                                                {
                                                    this.props.t(
                                                        "Refunded: {{qty}}",
                                                        {qty: (item.qty_refunded ? NumberHelper.formatDisplayGroupAndDecimalSeparator(item.qty_refunded) : 0)}
                                                    )
                                                }
                                            </span>
                                            : null
                                    }
                                    {
                                        canShowInvoiceRefundCancel && item.qty_canceled ?
                                            <span>
                                                {
                                                    this.props.t(
                                                        "Canceled: {{qty}}",
                                                        {qty: (item.qty_canceled ? NumberHelper.formatDisplayGroupAndDecimalSeparator(item.qty_canceled) : 0)}
                                                    )
                                                }
                                            </span>
                                            : null
                                    }
                                </Fragment>
                                : null
                        }
                    </div>
                    {
                        item.os_pos_custom_price_reason ?
                            <div className="custom-reason">
                                <span>{item.os_pos_custom_price_reason}</span>
                            </div>
                            : ''
                    }
                    <div className={"order-item-detail-after"}>
                        {layout('order')('order_detail')('detail_content')('order_item')('order_item_itemdetail_after')()(this, item)}
                    </div>
                </div>
                {
                    this.canShowPriceInfo() ?
                        <div className="item-order">
                            <div><b>{OrderService.getRowTotal(item, order)}</b></div>
                            {
                                this.displayOriginPrice(item) ?
                                    <div className="origin-price">
                                        {this.props.t("Origin Price: {{original_price}}", {original_price: OrderService.getItemDisplayOriginalPrice(item, order)})}
                                    </div> : ''
                            }
                            <div>
                                {this.props.t("Price: {{price}}", {price: OrderService.getItemDisplayPrice(item, order)})}
                            </div>
                            <div>
                                {this.props.t("Tax: {{tax}}", {tax: this.getDisplayPrice(item.tax_amount, order)})}
                            </div>
                            {
                                item.weee_tax_applied_row_amount ?
                                    <div>
                                        {
                                            this.props.t(
                                                "FPT: {{fpt}}",
                                                {fpt: this.getDisplayPrice(item.weee_tax_applied_row_amount, order)}
                                            )
                                        }
                                    </div>
                                    :
                                    null
                            }
                            {
                                !isHolded ?
                                    <div>
                                        {
                                            this.props.t(
                                                "Discount: {{discount}}",
                                                {discount: this.getDisplayPrice(item.discount_amount, order)}
                                            )
                                        }
                                    </div>
                                    :
                                    null
                            }
                        </div>
                        : null
                }
            </div>
        );
    }
}

class OrderItemContainer extends CoreContainer {
    static className = 'OrderItemContainer';
}

/**
 * @type {OrderItem}
 */
export default ContainerFactory.get(OrderItemContainer).withRouter(
    ComponentFactory.get(OrderItem)
);


// WEBPACK FOOTER //
// ./src/view/component/order/order-detail/detail-content/OrderItem.js