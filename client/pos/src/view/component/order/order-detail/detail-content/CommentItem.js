/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from "react";
import ComponentFactory from '../../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import CoreComponent from "../../../../../framework/component/CoreComponent";
import moment from "moment/moment";
import DateTimeHelper from "../../../../../helper/DateTimeHelper";

export class CommentItem extends CoreComponent {
    static className = 'CommentItem';

    /**
     * template
     * @returns {*}
     */
    template() {
        let {comment} = this.props;
        let date = moment(
            DateTimeHelper.convertDatabaseDateTimeToLocalDate(comment.created_at)
        ).format('L LT');
        return (
            <li>
                <div className="date">{date}</div>
                <div className="comment">
                    {
                        comment.comment
                    }
                </div>
            </li>
        );
    }
}

class CommentItemContainer extends CoreContainer {
    static className = 'CommentItemContainer';
}

/**
 * @type {CommentItem}
 */
export default ContainerFactory.get(CommentItemContainer).withRouter(
    ComponentFactory.get(CommentItem)
);