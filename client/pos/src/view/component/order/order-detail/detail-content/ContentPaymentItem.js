/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from "react";
import ComponentFactory from '../../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import CoreComponent from "../../../../../framework/component/CoreComponent";
import DateTimeHelper from "../../../../../helper/DateTimeHelper";
import moment from 'moment';
import OrderHelper from "../../../../../helper/OrderHelper";

export class ContentPaymentItem extends CoreComponent {
    static className = 'ContentPaymentItem';

    /**
     * template
     * @returns {*}
     */
    template() {
        let {payment, order} = this.props;
        let date = payment.payment_date ?
            '(' +
            moment(DateTimeHelper.convertDatabaseDateTimeToLocalDate(payment.payment_date)).format('L')
            + ')'
            : '';
        return (
            <Fragment>
                <li>
                    <div className="title">{payment.title} <span>{date}</span></div>
                    <div className="value">{OrderHelper.formatPrice(payment.amount_paid, order)}</div>
                </li>
                {
                    (payment.reference_number && (
                        <li>
                            <div className="value">
                                {
                                    payment.reference_number
                                }
                                {
                                    payment.card_type && ` - ${payment.card_type.toUpperCase()}`
                                }
                            </div>
                        </li>
                    )) || (payment.card_type && <li>
                        <div className="value">
                            {
                                `${payment.card_type.toUpperCase()}`
                            }
                        </div>
                    </li>)
                }
            </Fragment>
        );
    }
}

class ContentPaymentItemContainer extends CoreContainer {
    static className = 'ContentPaymentItemContainer';
}

/**
 * @type {ContentPaymentItem}
 */
export default ContainerFactory.get(ContentPaymentItemContainer).withRouter(
    ComponentFactory.get(ContentPaymentItem)
);