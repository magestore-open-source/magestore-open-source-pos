/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from 'react';
import {CoreComponent} from "../../../../../../framework/component/index";
import CoreContainer from "../../../../../../framework/container/CoreContainer";
import ComponentFactory from "../../../../../../framework/factory/ComponentFactory";
import ContainerFactory from "../../../../../../framework/factory/ContainerFactory";
import OrderHelper from "../../../../../../helper/OrderHelper";
import CreateCreditmemoConstant from "../../../../../constant/order/creditmemo/CreateCreditmemoConstant";

class CreateCreditmemoStepPaymentConfirmationComponent extends CoreComponent {
    static className = 'CreateCreditmemoStepPaymentConfirmationComponent';

    /**
     * Set comment text
     * @param event
     */
    setCommentText(event) {
        let value = event.target.value;
        let realValue = OrderHelper.stripHtmlTags(value);
        if (value !== realValue) {
            event.target.value = realValue;
        }
        this.props.setCommentText(realValue)
    }

    /**
     * template to render
     * @returns {*}
     */
    template() {
        let payment_confirm_step = this.props.payment_confirm_step,
            isConfirmationStep = payment_confirm_step === CreateCreditmemoConstant.PAYMENT_CONFIRM_STEP_CONFIRMATION;
        return (
            <Fragment>
                <div className={"modal fade in popup-messages" + (isConfirmationStep ? " popup-messages-refund" : "")}
                     style={{display: 'block'}}>
                    <div className={"modal-dialog " + (isConfirmationStep ? "modal-md" : "modal-sm")}>
                        <div className="modal-content">
                            {
                                isConfirmationStep ?
                                    <div className="modal-header">
                                        <h3 className="title">{this.props.t('Refund Confirmation')}</h3>
                                    </div> :
                                    null
                            }
                            {
                                isConfirmationStep ?
                                    <div className="modal-body">
                                        <p>{this.props.t('Are you sure you want to process this refund?')}</p>
                                        <textarea className="form-control refund-confirmation-text"
                                                  placeholder={this.props.t('Reason to refund (Optional)')}
                                                  defaultValue={this.props.comment_text}
                                                  onChange={event => this.setCommentText(event)}>
                                        </textarea>
                                    </div> :
                                    <div className="modal-body">
                                        <h3 className="title">{this.props.t('Refund Warning')}</h3>
                                        <p>
                                            {
                                                this.props.t(
                                                    'You must fully allocate refund amount to payment methods!'
                                                )
                                            }
                                        </p>
                                    </div>
                            }
                            {
                                isConfirmationStep ?
                                    <div className="modal-footer actions-2column">
                                        <div className="modal-footer actions-2column">
                                            <a className="close-modal"
                                               onClick={() => this.props.cancelPopup()}>{this.props.t("No")}</a>
                                            <a className="close-modal"
                                               onClick={() => this.props.nextStep()}>{this.props.t("Yes")}</a>
                                        </div>
                                    </div> :
                                    <div className="modal-footer actions-2column actions-1column">
                                        <a className="close-modal"
                                           onClick={() => this.props.cancelPopup()}>{this.props.t("OK")}</a>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop fade in popover-backdrop"/>
            </Fragment>
        );
    }
}

class CreateCreditmemoStepPaymentConfirmationContainer extends CoreContainer {
    static className = 'CreateCreditmemoStepPaymentConfirmationContainer';
}

/**
 * @type {CreateCreditmemoStepPaymentConfirmationContainer}
 */
export default ContainerFactory.get(CreateCreditmemoStepPaymentConfirmationContainer).withRouter(
    ComponentFactory.get(CreateCreditmemoStepPaymentConfirmationComponent)
)