/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {CoreComponent} from "../../../../../../framework/component/index";
import NumberHelper from "../../../../../../helper/NumberHelper";

export default class CreateCreditmemoStepItemItemAbstractComponent extends CoreComponent {

    /**
     * Decrease qty
     *
     * @param creditmemoItemParam
     */
    decreaseQty(creditmemoItemParam) {
        let qty = NumberHelper.minusNumber(creditmemoItemParam.qty, 1);
        this.props.updateCreditmemoItemParam(creditmemoItemParam, {qty: qty}, true);
    }

    /**
     * Increase qty
     *
     * @param creditmemoItemParam
     */
    increaseQty(creditmemoItemParam) {
        let qty = NumberHelper.addNumber(creditmemoItemParam.qty, 1);
        this.props.updateCreditmemoItemParam(creditmemoItemParam, {qty: qty}, true);
    }

    /**
     * Change back to stock of item
     *
     * @param creditmemoItemParam
     * @param isBackToStock
     */
    changeBackToStock(creditmemoItemParam, isBackToStock = true) {
        this.props.updateCreditmemoItemParam(creditmemoItemParam, {back_to_stock: isBackToStock});
    }
}