/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from 'react';
import PropTypes from 'prop-types';
import {Modal} from "react-bootstrap";
import {CoreComponent} from "../../../../framework/component/index";
import CoreContainer from "../../../../framework/container/CoreContainer";
import ComponentFactory from "../../../../framework/factory/ComponentFactory";
import ContainerFactory from "../../../../framework/factory/ContainerFactory";

class OrderAlertPopup extends CoreComponent {
    static className = 'OrderAlertPopup';
    /**
     * template to render
     * @returns {*}
     */
    template() {
        return (
            <Modal
                className={"popup-messages"}
                show={this.props.showOrderAlertPopup}
                onHide={() => this.props.closeOrderAlertPopup()}
            >
                <Modal.Body>
                    <p>
                        {this.props.t(this.props.orderAlertPopupData.message)}
                    </p>
                </Modal.Body>
                <Modal.Footer className={"close-modal"}>
                    <button onClick={() => this.props.closeOrderAlertPopup()}>{this.props.t('CLOSE')}</button>
                </Modal.Footer>
            </Modal>
        );
    }
}

OrderAlertPopup.propTypes = {
    showOrderAlertPopup: PropTypes.bool,
    orderAlertPopupData: PropTypes.object,
    closeOrderAlertPopup: PropTypes.func,
};

class OrderAlertPopupContainer extends CoreContainer {
    static className = 'OrderAlertPopupContainer';
}

/**
 * @type {OrderAlertPopupContainer}
 */
export default ContainerFactory.get(OrderAlertPopupContainer).withRouter(
    ComponentFactory.get(OrderAlertPopup)
)