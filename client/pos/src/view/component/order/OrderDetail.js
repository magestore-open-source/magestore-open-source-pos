/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from "react";
import * as _ from "lodash";
import CoreComponent from '../../../framework/component/CoreComponent';
import ComponentFactory from '../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../framework/container/CoreContainer';
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import SmoothScrollbar from "smooth-scrollbar";
import DateTimeHelper from "../../../helper/DateTimeHelper";
import OrderService from "../../../service/sales/OrderService";
import OrderItem from "./order-detail/detail-content/OrderItem";
import {Panel} from "react-bootstrap";
import moment from "moment/moment";
import ContentPaymentItem from "./order-detail/detail-content/ContentPaymentItem";
import ShippingMethod from "./order-detail/detail-content/ShippingMethod";
import ShippingAddress from './order-detail/detail-content/ShippingAddress';
import BillingAddress from './order-detail/detail-content/BillingAddress';
import OrderAction from "../../action/OrderAction";
import StatusConstant from "../../constant/order/StatusConstant";
import CommentItem from "./order-detail/detail-content/CommentItem";
import CreditmemoAction from "../../action/order/CreditmemoAction";
import OrderHelper from "../../../helper/OrderHelper";
import TaxHelper from "../../../helper/TaxHelper";
import OrderWeeeDataService from "../../../service/weee/OrderWeeeDataService";
import PaymentConstant from "../../constant/PaymentConstant";
import LocationService from "../../../service/LocationService";
import NumberHelper from "../../../helper/NumberHelper";
import layout from "../../../framework/Layout";
import {fire} from "../../../event-bus";

export class OrderDetail extends CoreComponent {
    static className = 'OrderDetail';

    setBlockContentElement = element => {
        this.block_content = element;
        if (!this.scrollbarOrderDetail) {
            this.scrollbarOrderDetail = SmoothScrollbar.init(this.block_content);
        }
    };

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            canCancel: false,
            canTakePayment: false,
            canRefund: false,
            canReorder: true,
            printBtnClassName: 'btn btn-default',
            isRefunding: false
        }
    }

    /**
     * Component will receive props
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        if (this.props.order !== nextProps.order) {
            if (nextProps.order && !nextProps.order.items) {
                return;
            }
            if (this.scrollbarOrderDetail) {
                this.scrollbarOrderDetail.scrollTo(0, 0);
            }

            let paymentStatus = OrderService.getPaymentStatus(nextProps.order);
            let abilities = {
                canCancel: (
                    OrderService.canCancel(nextProps.order)
                ),
                canTakePayment: (
                    paymentStatus.realValue === StatusConstant.PAYMENT_STATUS_PARTIAL_PAID
                    || paymentStatus.realValue === StatusConstant.PAYMENT_STATUS_UNPAID
                ),
                canRefund: (
                    OrderService.canCreditmemo(nextProps.order)
                )
            };

            fire('component_order_detail_component_will_receive_props_calculate_order_abilities_after', { abilities, nextProps });
            this.setState(abilities);

            if (this.state.isRefunding) {
                this.setState({isRefunding: false});
                this.props.openCreditmemoOrder();
            }
        }
    }

    /**
     * component will unmount
     */
    componentWillUnmount() {
        if (this.scrollbarOrderDetail) {
            SmoothScrollbar.destroy(this.scrollbarOrderDetail);
        }
    }

    /**
     * refund
     */
    async refund() {
        if (!this.state.canRefund) {
            return;
        }
        return this.props.openCreditmemoOrder();
    }

    /**
     * handle click take payment
     */
    handleClickTakePayment() {
        if (this.state.canTakePayment) {
            this.props.openTakePayment();
        }
    }

    /**
     * handle click cancel order
     */
    handleClickCancelOrder() {
        let deepCanCancel = OrderService.deepCanCancel(this.props.order);
        if (deepCanCancel.canCancel) {
            return this.props.openCancelOrder();
        }

        if (!deepCanCancel.message) {
            return;
        }

        return this.props.openOrderAlertPopup(deepCanCancel)
    }


    /**
     * handle click add note
     */
    handleClickAddComment() {
        this.props.openAddComment();
    }

    /**
     * handle click send email
     */
    handleClickSendEmail() {
        this.props.openSendEmail();
    }

    /**
     * handle click reorder
     */
    async handleClickReorder() {
        if (!this.state.canReorder) {
            return;
        }
        this.props.startReorder();
    }

    /**
     * Print order by order data
     * @param order
     * @returns {Promise.<void>}
     */
    async handleClickPrint(order) {
        this.props.actions.reprintOrder(order);
    }

    /**
     * get display location
     * @param order
     * @return {*}
     */
    getDisplayLocation(order) {
        let locationId = order.pos_location_id;
        if (locationId) {
            let location = LocationService.getLocationById(locationId);
            if (location) {
                return this.props.t("Location: {{name}}", {name: location.name})
            }
        } else {
            return '';
        }
    }

    /**
     * Return loading flag value
     *
     * @return boolean
     */
    isLoading() {
        return this.props.isDetailLoading;
    }

    /**
     * template
     * @returns {*}
     */
    template() {
        let {order, isLoading, t} = this.props;
        if (order && !order.items) {
            order = null;
        }
        let billingAddress = OrderService.getBillingAddress(order);
        let created_at = order ?
            moment(DateTimeHelper.convertDatabaseDateTimeToLocalDate(order.created_at)).format('L LT') : "";
        let due = (
            order
            && order.total_due > 0
            && order.state !== StatusConstant.STATE_CANCELED
            && order.state !== StatusConstant.STATE_CLOSED
        )
            ? this.props.t("Due: {{total_due}}", {total_due: OrderHelper.formatPrice(order.total_due, order)})
            : "";

        let discount = order ? OrderHelper.formatPrice(0, order) : '';
        let pointDiscount = 0;
        if (order && order.discount_amount) {
            let discount_amount = Math.abs(order.discount_amount) - pointDiscount;
            discount = OrderHelper.formatPrice(-discount_amount, order);
        }

        let weeeTotal = order && order.items ? OrderWeeeDataService.getTotalAmounts(order.items, order) : "";

        let paymentStatus = OrderService.getPaymentStatus(order);

        let statusHistory = (order && order.status_histories) ? order.status_histories : [];
        statusHistory = _.orderBy(statusHistory, 'created_at', 'desc');

        let customer_name = (order && order.customer_firstname && order.customer_lastname && !order.customer_is_guest) ?
            (order.customer_firstname + ' ' + order.customer_lastname) :
            this.props.t('Guest');
        let telephone = order && !order.customer_is_guest && billingAddress ?
            (billingAddress.telephone || t('N/A')) :
            null;
        let other_payment;
        if (order && order.payments) {
            let actutal_total_paid = 0;
            order.payments.forEach((payment) => {
                if (payment.type !== PaymentConstant.TYPE_REFUND) {
                    actutal_total_paid = NumberHelper.addNumber(payment.amount_paid, actutal_total_paid);
                }
            });
            if (actutal_total_paid < order.total_paid) {
                let amount = order.total_paid - actutal_total_paid;
                other_payment = {
                    amount_paid: amount,
                    base_amount_paid: amount,
                    card_type: null,
                    is_paid: 1,
                    method: "otherpayment",
                    payment_date: null,
                    receipt: null,
                    reference_number: null,
                    title: "Other",
                    type: 0
                };
            }
        }
        return (
            <div className="wrapper-order-right">
                <div className="block-title">{order ? order.increment_id : ''}</div>

                <div className="loader-product"
                     style={{display: (this.isLoading() ? 'block' : 'none'), marginTop: '300px', marginBottom: '500px'}}>
                </div>

                <div data-scrollbar ref={this.setBlockContentElement} className="block-content">

                    {
                        !order ?
                            (
                                isLoading || this.isLoading() ?
                                    <div/>
                                    :
                                    <div className="page-notfound">
                                        <div className="icon"></div>
                                        {
                                            this.props.showNoInternet ?
                                                this.props.t('Please connect to Internet to find orders')
                                                :
                                                this.props.t('No order is found.')
                                        }
                                    </div>
                            )
                            :
                            <div>
                                <div className="order-info">
                                    <div className="order-price">
                                        <div className="price">{OrderHelper.formatPrice(order.grand_total, order)}</div>
                                        <div>
                                            <span className="price-label due">{due}</span>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className="order-detail">
                                                <div>
                                                    {
                                                        this.props.t(
                                                            "Order Date: {{created_at}}",
                                                            {created_at: created_at}
                                                        )
                                                    }
                                                </div>
                                                <div>{this.getDisplayLocation(order)}</div>
                                                <div>
                                                    {
                                                        this.props.t(
                                                            "Customer: {{name}}",
                                                            {name: customer_name}
                                                        )
                                                    }
                                                </div>
                                                {
                                                    telephone ?
                                                        <div>
                                                            {
                                                                this.props.t(
                                                                    "Customer Phone: {{telephone}}",
                                                                    {telephone: telephone}
                                                                )
                                                            }
                                                        </div> :
                                                        telephone
                                                }
                                                <div>
                                                    {
                                                        order.pos_staff_name ?
                                                            this.props.t("Staff: {{name}}", {name: order.pos_staff_name})
                                                            :
                                                            ''
                                                    }
                                                </div>
                                                <div className="order-status">
                                                    <span className={
                                                        "status " + order.status.toLowerCase().replace(' ', '-')
                                                    }>
                                                        {
                                                            this.props.t(
                                                                OrderService.getDisplayStatus(order.state, order.status)
                                                            )
                                                        }
                                                    </span>
                                                    <span className={"status " + paymentStatus.className}>
                                                        {paymentStatus.value}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="order-total">
                                                <ul>
                                                    <li>
                                                        <span className="title">{this.props.t('Subtotal')}</span>
                                                        <span className="value">
                                                            {OrderService.getDisplaySubtotal(order)}
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span className="title">
                                                            {OrderHelper.getDiscountDisplay(order)}
                                                        </span>
                                                        <span className="value">{discount}</span>
                                                    </li>
                                                    {
                                                        !order.is_virtual ?
                                                            <li>
                                                                <span className="title">
                                                                    {this.props.t('Shipping')}
                                                                </span>
                                                                <span className="value">
                                                                    {OrderService.getDisplayShippingAmount(order)}
                                                                </span>
                                                            </li>
                                                            : null
                                                    }
                                                    <li>
                                                        <span className="title">{this.props.t('FPT')}</span>
                                                        <span className="value">
                                                            {OrderHelper.formatPrice(weeeTotal, order)}
                                                        </span>
                                                    </li>
                                                    {
                                                        !TaxHelper.orderDisplayZeroTaxSubTotal() && order.tax_amount === 0
                                                            ?
                                                            null
                                                            :
                                                            <li>
                                                                <span className="title">{this.props.t('Tax')}</span>
                                                                <span className="value">
                                                                    {OrderHelper.formatPrice(order.tax_amount, order)}
                                                                </span>
                                                            </li>
                                                    }

                                                    {layout('order')('order_detail_layout')('total_tax_after')()(this)}

                                                    <li>
                                                        <span className="title">{this.props.t('Grand Total')}</span>
                                                        <span className="value">
                                                            {OrderHelper.formatPrice(order.grand_total, order)}
                                                        </span>
                                                    </li>
                                                    <li>
                                                        <span className="title">{this.props.t('Total Paid')}</span>
                                                        <span className="value">
                                                            {OrderHelper.formatPrice(order.total_paid, order)}
                                                        </span>
                                                    </li>
                                                    {
                                                        order.pos_change ?
                                                            <li>
                                                                <span className="title">
                                                                    {this.props.t('Total Change')}
                                                                </span>
                                                                <span className="value">
                                                                    {OrderHelper.formatPrice(order.pos_change, order)}
                                                                </span>
                                                            </li>
                                                            :
                                                            null
                                                    }
                                                    {
                                                        (order.total_refunded !== undefined)  ?
                                                            <li>
                                                                <span className="title">
                                                                    {this.props.t('Total Refunded')}
                                                                </span>
                                                                <span className="value">
                                                                    {OrderHelper.formatPrice(order.total_refunded, order)}
                                                                </span>
                                                            </li>
                                                            :
                                                            null
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="accordion" className="panel-group" role="tablist" aria-multiselectable="true">
                                    <Panel eventKey="1" defaultExpanded key={Math.random()}>
                                        <Panel.Heading>
                                            <Panel.Title toggle>{this.props.t('Items Ordered')}</Panel.Title>
                                        </Panel.Heading>
                                        <Panel.Body collapsible>
                                            {
                                                order.items.map(item => {
                                                    return <OrderItem key={'' + item.item_id + Math.random()}
                                                                      item={item} order={order}/>
                                                })
                                            }
                                        </Panel.Body>
                                    </Panel>
                                    <div className="panel-flex">
                                        <Panel eventKey="2" key={Math.random()}>
                                            <Panel.Heading>
                                                <Panel.Title toggle>{this.props.t('Payment Method')}</Panel.Title>
                                            </Panel.Heading>
                                            <Panel.Body collapsible>
                                                <div className="text-right">
                                                    <span className={"status " + paymentStatus.className}>
                                                        {paymentStatus.value}
                                                    </span>
                                                </div>
                                                <ul className="payment-method">
                                                    {
                                                        order.payments ?
                                                            order.payments.map((payment, index) =>
                                                                payment.type !== PaymentConstant.TYPE_REFUND
                                                                && payment.amount_paid > 0 ?
                                                                    <ContentPaymentItem key={index} payment={payment}
                                                                                 order={order}/> :
                                                                    null
                                                            )
                                                            :
                                                            null
                                                    }
                                                    {
                                                        other_payment ?
                                                            <ContentPaymentItem payment={other_payment} order={order}/> : null
                                                    }
                                                </ul>
                                            </Panel.Body>
                                        </Panel>
                                        {
                                            !order.is_virtual ?
                                                <Panel eventKey="3" key={Math.random()}>
                                                    <Panel.Heading>
                                                        <Panel.Title toggle>
                                                            {this.props.t('Shipping Method')}
                                                        </Panel.Title>
                                                    </Panel.Heading>
                                                    <Panel.Body collapsible>
                                                        <ShippingMethod order={order}/>
                                                    </Panel.Body>
                                                </Panel>
                                                : null
                                        }
                                        {
                                            !order.is_virtual ?
                                                <Panel eventKey="4" key={Math.random()}>
                                                    <Panel.Heading>
                                                        <Panel.Title toggle>
                                                            {this.props.t('Shipping Address')}
                                                        </Panel.Title>
                                                    </Panel.Heading>
                                                    <Panel.Body collapsible>
                                                        <ShippingAddress order={order}/>
                                                    </Panel.Body>
                                                </Panel>
                                                : null
                                        }
                                        <Panel eventKey="5" key={Math.random()}>
                                            <Panel.Heading>
                                                <Panel.Title toggle>{this.props.t('Billing Address')}</Panel.Title>
                                            </Panel.Heading>
                                            <Panel.Body collapsible>
                                                <BillingAddress order={order}/>
                                            </Panel.Body>
                                        </Panel>
                                    </div>
                                    {
                                        statusHistory && statusHistory.length ?
                                            <Panel eventKey="6" key={Math.random()}>
                                                <Panel.Heading>
                                                    <Panel.Title toggle>{this.props.t('Comment History')}</Panel.Title>
                                                </Panel.Heading>
                                                <Panel.Body collapsible>
                                                    <ul className="comment-history">
                                                        {
                                                            statusHistory.map((comment, index) =>
                                                                <CommentItem key={comment.entity_id + '_' + index}
                                                                             comment={comment}/>
                                                            )
                                                        }
                                                    </ul>
                                                </Panel.Body>
                                            </Panel>
                                            :
                                            null
                                    }
                                </div>
                            </div>
                    }
                </div>
                {
                    order ?
                        <div className="block-actions">
                            <ul className={"actions"}>
                                {layout('order')('order_detail_layout')('actions')('take_payment_before')()(this)}
                                <li>
                                    <button
                                        className={"btn btn-default take-payment-button " + (!this.state.canTakePayment ? 'disabled' : '')}
                                        onClick={() => this.handleClickTakePayment()}>
                                        {
                                            this.props.t('Take Payment')
                                        }
                                    </button>
                                </li>
                                {layout('order')('order_detail_layout')('actions')('refund_before')()(this)}
                                <li>
                                    <button
                                        className={"btn btn-default refund-button " + (!this.state.canRefund ? 'disabled' : '')}
                                        onClick={() => this.refund()}
                                    >{this.props.t('Refund')}</button>
                                </li>
                                {layout('order')('order_detail_layout')('actions')('cancel_before')()(this)}
                                <li>
                                    <button className={"btn btn-default cancel-button " + (!this.state.canCancel ? 'disabled' : '')}
                                            onClick={() => this.handleClickCancelOrder()}>
                                        {
                                            this.props.t('Cancel')
                                        }
                                    </button>
                                </li>
                                {layout('order')('order_detail_layout')('actions')('print_before')()(this)}
                                <li>
                                    <button
                                        className={this.state.printBtnClassName}
                                        onClick={() => this.handleClickPrint(order)}>
                                        {
                                            this.props.t('Print')
                                        }
                                    </button>
                                </li>
                                {layout('order')('order_detail_layout')('actions')('email_before')()(this)}
                                <li>
                                    <button className="btn btn-default email-button"
                                            onClick={() => this.handleClickSendEmail()}>
                                        {this.props.t('Email')}</button>
                                </li>
                                {layout('order')('order_detail_layout')('actions')('add_note_before')()(this)}
                                <li>
                                    <button className="btn btn-default add-note-button"
                                            onClick={() => this.handleClickAddComment()}>
                                        {this.props.t('Add Note')}
                                    </button>
                                </li>
                                {layout('order')('order_detail_layout')('actions')('reorder_before')()(this)}
                                <li>
                                    <button className={"btn btn-default reorder-button " + (!this.state.canReorder ? 'disabled' : '')}
                                            onClick={() => this.handleClickReorder()}>
                                        {this.props.t('Reorder')}
                                    </button>
                                </li>
                            </ul>
                        </div>
                        :
                        null
                }

            </div>
        )
    }
}

class OrderDetailContainer extends CoreContainer {
    static className = 'OrderDetailContainer';

    static mapDispatch(dispatch) {
        return {
            actions: {
                reprintOrder: (order) => dispatch(OrderAction.reprintOrder(order)),
                createCreditmemo: creditmemo => dispatch(CreditmemoAction.createCreditmemo(creditmemo)),
                updateOrder: orderData => dispatch(OrderAction.syncActionUpdateDataFinish([orderData]))
            }
        }
    }
}

export default ContainerFactory.get(OrderDetailContainer).withRouter(
    ComponentFactory.get(OrderDetail)
);
