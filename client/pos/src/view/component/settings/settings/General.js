/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from "react";
import ComponentFactory from '../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../framework/factory/ContainerFactory";
import AbstractGrid from "../../../../framework/component/grid/AbstractGrid";
import GeneralService from "../../../../service/settings/GeneralService";
import '../../../style/css/Setting.css';

export class General extends AbstractGrid {
    static className = 'General';

    // /**
    //  *
    //  * @param props
    //  */
    // constructor(props) {
    //     super(props);
    // }

    /**
     * template
     * @returns {*}
     */
    template() {
        return (
            <Fragment>
                <div className="settings-right">
                    <div className="block-title">
                        <strong className="title"></strong>
                    </div>
                    <div className="block-content">
                        <ul className="list-lv1">
                            <li>
                                <span className="title">
                                    {this.props.t('Synchronize data')}
                                </span>
                                <span className="value">
                                    <label className="checkbox">
                                        <input type="checkbox"
                                               defaultChecked={GeneralService.isUseOfflineData()}
                                               onChange={(event) =>
                                                   GeneralService.setUseOfflineData(event.target.checked)}
                                        />
                                        <span></span>
                                    </label>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </Fragment>
        )
    }
}

class GeneralContainer extends CoreContainer {
    static className = 'GeneralContainer';
}

/**
 * @type {GeneralContainer}
 */
export default ContainerFactory.get(GeneralContainer).withRouter(
    ComponentFactory.get(General)
);