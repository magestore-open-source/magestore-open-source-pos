/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from "react";
import ComponentFactory from '../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../framework/factory/ContainerFactory";
import AbstractGrid from "../../../../framework/component/grid/AbstractGrid";
import '../../../style/css/Setting.css';
import UserService from "../../../../service/user/UserService";
import UserAction from "../../../action/UserAction";
import {Modal} from "react-bootstrap";
import {toast} from "react-toastify";
import i18n from "../../../../config/i18n";
import {isArray} from "rxjs/util/isArray";

export class AccountInformation extends AbstractGrid {
    static className = 'AccountInformation';

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            buttonTitle: this.props.t('Change Password'),
            error: '',
            staffName: "",
            staffEmail: "",
            staffUsername: null,
            clickedButton: false,
            modalTitle: '',
            modalContent: '',
            isOpenModal: false,
            showToast: false
        };
    }

    /**
     * component will mount
     */
    componentWillMount() {
        const staffName = UserService.getStaffName();
        const staffEmail = UserService.getStaffEmail();
        const staffUsername = UserService.getStaffUsername();

        if (staffName) {
            this.setState({staffName: staffName})
        }

        if (staffEmail) {
            this.setState({staffEmail: staffEmail || ""})
        }

        if (staffUsername) {
            this.setState({staffUsername: staffUsername})
        }
    }

    /**
     * component will mount
     */
    componentWillReceiveProps(nextProps) {
        if (this.state.isOpenModal) {
            this.setState({
                isOpenModal: false
            });
        }
        if (this.state.clickedButton) {
            this.setState({
                clickedButton: false
            });
        }
        if (!nextProps.user.connection) {
            this.showInternetWarning();
        }

        const resultChangePass = nextProps.user.resultChangePass;
        if (resultChangePass) {
            if (!resultChangePass.success && this.state.showToast) {
                this.resetForm();
                this.setState({
                    buttonTitle: this.props.t('Change Password'),
                    showToast: false
                });
                isArray(resultChangePass.message) &&
                resultChangePass.message.map(error =>
                    toast.error(
                        i18n.translator.translate(error),
                        {
                            className: 'wrapper-messages messages-warning',
                            autoClose: 3000
                        }
                    ));
            } else if (resultChangePass.success) {
                toast.success(
                    i18n.translator.translate("Your password has been changed successfully!"),
                    {
                        position: toast.POSITION.TOP_CENTER,
                        className: 'wrapper-messages messages-success',
                        autoClose: 3000
                    }
                );
                return this.props.history.push({
                    pathname: '/checkout',
                });
            }
        }
    }

    /**
     * open internet warning popup
     */
    showInternetWarning() {
        let title = this.props.t('Network error');
        let content = this.props.t('You must connect to a Wi-Fi or cellular data network to access the POS');
        this.setState({
            isOpenModal: true,
            modalTitle: title,
            modalContent: content
        });
    }

    /**
     * open internet warning popup
     */
    showErrorChangePassword(content) {
        let title = this.props.t('Change Password Error');
        this.setState({
            isOpenModal: true,
            modalTitle: title,
            modalContent: content,
        });
    }

    /**
     * Reset form after submit if error
     */
    resetForm() {
        this.currentPasswordRef.value = null;
        this.newPasswordRef.value = null;
        this.confirmationRef.value = null;
    }

    /**
     * confirm modal
     */
    confirm() {
        this.setState({
            isOpenModal: false,
            clickedButton: false,
            buttonTitle: this.props.t('Change Password'),
            active: false
        });
    }

    /**
     * Change Pass handle
     */
    changePass() {
        if (this.state.clickedButton) {
            return;
        }
        if (this.props.user.connection) {
            this.setState({
                buttonTitle: this.props.t('Please wait') + '...',
                clickedButton: true,
                active: false,
                showToast: true
            });

            const newPassword = this.newPasswordRef.value,
                oldPassword = this.currentPasswordRef.value,
                confirmationPassword = this.confirmationRef.value,
                username = this.state.staffUsername;

            if (newPassword !== confirmationPassword) {
                const content = 'Password confirmation must be same as new password.';
                this.showErrorChangePassword(content);
                this.resetForm();
                return
            }
            if (!oldPassword) {
                const content = 'You must enter a current password.';
                this.showErrorChangePassword(content);
                this.resetForm();
                return;
            }
            this.props.clickChangePassword(username, oldPassword, newPassword, confirmationPassword);
        } else {
            this.showInternetWarning();
        }
    }

    /**
     * check active login button
     */
    checkActiveChangePass() {
        if (this.newPasswordRef.value && this.confirmationRef.value && this.currentPasswordRef.value) {
            this.setState({active: true});
        } else {
            this.setState({active: false});
        }
    }

    /**
     * template
     * @returns {*}
     */
    template() {
        let changePassButton;
        if (this.state.active) {
            changePassButton = <button type="button"
                                       className="btn btn-default"
                                       ref={(c) => {
                                           this.changePassButton = c;
                                       }}
                                       onClick={() => this.changePass()}>{this.state.buttonTitle}</button>
        } else {
            changePassButton = <button type="button"
                                       disabled
                                       ref={(c) => {
                                           this.changePassButton = c;
                                       }}
                                       className="btn btn-default disabled"
                                       onClick={() => this.changePass()}>{this.state.buttonTitle}</button>
        }

        return (
            <Fragment>
                <div className="settings-right">
                    <div className="block-title">
                        <strong className="title"></strong>
                    </div>
                    <div className="block-content">
                        <ul className="list-lv1">
                            <li>
                                <span className="title">
                                    {this.props.t('Name')}
                                </span>
                                <span className="value">{this.state.staffName}</span>
                            </li>
                            <li>
                                <span className="title">
                                    {this.props.t('Email')}
                                </span>
                                <span className="value">{this.state.staffEmail}</span>
                            </li>
                        </ul>
                        <form className="wrapper-form-change-pass" onSubmit={e => e.preventDefault()}>
                            <div className="row-form-change-pass">
                                <label>{this.props.t('New Password')}</label>
                                <input id="new-password" name="newPassword" type="password"
                                       className="form-control"
                                       ref={(c) => {
                                           this.newPasswordRef = c;
                                       }}
                                       onChange={() => this.checkActiveChangePass()}
                                       autoComplete="off"
                                />
                            </div>
                            <div className="row-form-change-pass">
                                <label>{this.props.t('Password Confirmation')}</label>
                                <input id="confirmation" name="confirmation" type="password"
                                       className="form-control"
                                       ref={(c) => {
                                           this.confirmationRef = c;
                                       }}
                                       onChange={() => this.checkActiveChangePass()}
                                       autoComplete="off"
                                />
                            </div>
                            <div className="row-form-change-pass">
                                <label>{this.props.t('Current Password')}</label>
                                <input id="current-password" name="newPassword" type="password"
                                       className="form-control"
                                       ref={(c) => {
                                           this.currentPasswordRef = c;
                                       }}
                                       onChange={() => this.checkActiveChangePass()}
                                       autoComplete="off"
                                />
                            </div>
                            <div className="button-change-pass">
                                {changePassButton}
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <Modal
                        bsSize={"small"}
                        className={"popup-messages"}
                        show={this.state.isOpenModal} onHide={() => this.confirm()}>
                        <Modal.Body>
                            <h3 className="title">{this.state.modalTitle}</h3>
                            <p>{this.props.t(this.state.modalContent)}</p>
                        </Modal.Body>
                        <Modal.Footer className={"close-modal"}>
                            <button onClick={() => this.confirm()}>OK</button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </Fragment>
        )
    }
}

class AccountInformationContainer extends CoreContainer {
    static className = 'AccountInformationContainer';

    /**
     * Map states
     *
     * @param state
     * @returns {{user: {connection, error}}}
     */
    static mapState(state) {
        const {user} = state.core;
        let connection = state.core.internet.connection;
        let error = user.error;
        if (!connection) {
            error = '';
        }
        let userProps = {...user, connection: connection, error: error};
        return {user: userProps};
    }

    /**
     * Map actions
     *
     * @param dispatch
     * @returns {{clickChangePassword: function(*=, *=, *-): *}}
     */
    static mapDispatch(dispatch) {
        return {
            clickChangePassword: (username, old_password, new_password, confirmation_password) =>
                dispatch(
                    UserAction.changeInformation(username, old_password, new_password, confirmation_password)
                ),
        }
    }
}

/**
 * @type {GeneralContainer}
 */
export default ContainerFactory.get(AccountInformationContainer).withRouter(
    ComponentFactory.get(AccountInformation)
);
