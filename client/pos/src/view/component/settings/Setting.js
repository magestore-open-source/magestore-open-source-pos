/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from "react";
import ComponentFactory from '../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../framework/container/CoreContainer';
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import SmoothScrollbar from "smooth-scrollbar";
import Config from "../../../config/Config";
import AbstractGrid from "../../../framework/component/grid/AbstractGrid";
import '../../style/css/Setting.css';
import SettingListConstant from "../../constant/settings/SettingListConstant";
import General from "./settings/General";
import AccountInformation from "./settings/AccountInformation";
import ScanAction from "../../action/ScanAction";

export class Setting extends AbstractGrid {
    static className = 'Setting';

    setBlockSettingListElement = element => this.setting_list = element;
    items = [
        {
            "id": "My Account",
            "title": SettingListConstant.GET_SETTING_ACCOUNT,
            "name": "My Account",
            "component": AccountInformation,
            "className": "li-account",
        },
        {
            "id": "General",
            "title": SettingListConstant.GET_SETTING_GENERAL,
            "name" : "General",
            "component": General,
            "className": "li-general",

        }
    ];
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            currentItem: SettingListConstant.GET_SETTING_ACCOUNT,
        };
    }

    /**
     * Component will mount
     */
    componentWillMount() {
        /* Set default state mode for component from Config */
        if (Config.mode) {
            this.setState({mode: Config.mode});
        }
        if (!this.scrollbar && this.setting_list) {
            this.scrollbar = SmoothScrollbar.init(this.setting_list);
        }

        if (this.items.length === 1) {
            this.setCurrentSetting(this.items[0].title);
        }
        this.props.actions.setScanPage();
    }

    /**
     * Init smooth scrollbar
     */
    componentDidMount() {
        if (!this.scrollbar && this.setting_list) {
            this.scrollbar = SmoothScrollbar.init(this.setting_list);
        }
    }

    /**
     * set selected current item
     * @param itemName
     */
    setCurrentSetting(itemName) {
        this.setState({currentItem: itemName});
    }

    /**
     * template
     * @returns {*}
     */
    template() {
        return (
            <div className="wrapper-settings">
                <div className="settings-left">
                    <div className="block-title">
                        <strong className="title">Settings</strong>
                    </div>
                    <div className="block-content" ref={this.setBlockSettingListElement}>
                        <ul className="list">
                            {
                                this.items.map(item => {
                                    return (
                                        <li key={item.id}
                                            className={
                                                (this.state.currentItem === item.title) ?
                                                    item.className + ' selected' :
                                                    item.className
                                            }
                                            onClick={() => this.setCurrentSetting(item.title)}>
                                            <a>{item.name}</a>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
                {
                    this.items.map(item => {
                        let Element = item.component;
                        return (this.state.currentItem === item.title) &&
                            (Element !== "") && <Element key={item.id}/>
                    })
                }
            </div>
        )
    }
}

class SettingContainer extends CoreContainer {
    static className = 'SettingContainer';
    /* eslint-disable no-unused-vars */
    /**
     * map state to component's props
     * @param state
     * @return {{}}
     */
    static mapState(state) {
        return {};
    }
    /* eslint-enable no-unused-vars */
    /**
     * map actions to component's props
     * @param dispatch
     * @return {{actions: }}
     */
    static mapDispatch(dispatch) {
        return {
            actions: {
                setScanPage: scanPage => dispatch(ScanAction.setScanPage(scanPage))
            }
        }
    }
}

/**
 * @type {Setting}
 */
export default ContainerFactory.get(SettingContainer).withRouter(
    ComponentFactory.get(Setting)
);
