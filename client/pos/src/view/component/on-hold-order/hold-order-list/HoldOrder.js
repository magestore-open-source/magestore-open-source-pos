/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from "react";
import ComponentFactory from '../../../../framework/factory/ComponentFactory';
import CoreContainer from '../../../../framework/container/CoreContainer';
import ContainerFactory from "../../../../framework/factory/ContainerFactory";
import CoreComponent from "../../../../framework/component/CoreComponent";
import OrderHelper from "../../../../helper/OrderHelper";

export class HoldOrder extends CoreComponent {
    static className = 'HoldOrder';

    /**
     * handle select order
     * @param order
     */
    selectOrder(order) {
        this.props.selectOrder(order);
    }

    /**
     * template
     * @returns {*}
     */
    template() {
        let {order, isActive} = this.props;
        return (
            <li className={"item " + (isActive ? 'active' : '')}
                onClick={() => this.selectOrder(order)}>
                <div className="item-info">
                    <div className="name">
                        <span className="value">{order.increment_id}</span>
                    </div>
                    <div className="price">
                        <span className="value">{OrderHelper.formatPrice(order.grand_total, order)}</span>
                    </div>
                </div>
            </li>
        );
    }
}

class HoldOrderContainer extends CoreContainer {
    static className = 'HoldOrderContainer';
}

/**
 * @type {HoldOrder}
 */
export default ContainerFactory.get(HoldOrderContainer).withRouter(
    ComponentFactory.get(HoldOrder)
);