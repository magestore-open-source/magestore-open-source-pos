/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreContainer from "../../../framework/container/CoreContainer";
import ComponentFactory from "../../../framework/factory/ComponentFactory";
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import PrintComponent from "./PrintComponent";
import PrintAction from "../../action/PrintAction";

class PrintContainer extends CoreContainer {
    static className = 'PrintContainer';

    /**
     *  map state to props of component
     *
     * @param {Object} state
     * @return {{orderData, order, isReprint, creditBalance, pointBalance, quote, creditmemo}}
     */
    static mapState(state) {
        const {
            orderData,
            order,
            isReprint,
            creditBalance,
            pointBalance,
            quote,
            creditmemo,
            showLoading
        } = state.core.print;
        return {
            orderData,
            order,
            isReprint,
            creditBalance,
            quote,
            creditmemo,
            pointBalance,
            showLoading
        }
    }

    /**
     * Map actions
     *
     * @param dispatch
     * @returns {{finishPrint: function(): *}}
     */
    static mapDispatch(dispatch) {
        return {
            finishPrint: () => dispatch(PrintAction.finishPrint())
        }
    }
}

const container = ContainerFactory.get(PrintContainer);
export default container.withRouter(ComponentFactory.get(PrintComponent))



