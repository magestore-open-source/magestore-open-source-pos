/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreContainer from "../../../framework/container/CoreContainer";
import CoreComponent from "../../../framework/component/CoreComponent";
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import ComponentFactory from "../../../framework/factory/ComponentFactory";
import {listen} from "../../../event-bus";
import TotalsAction from "../../action/checkout/quote/TotalsAction";
import QuoteAction from "../../action/checkout/QuoteAction";

export class QuoteComponent extends CoreComponent {
    static className = 'QuoteComponent';

    constructor(props) {
        super(props);
        listen('service_quote_init_total_collectors', eventData => {
            this.props.initTotalCollectors(eventData.service);
        }, 'QuoteComponent');
        listen('service_quote_collect_totals_before', eventData => {
            this.props.collectTotalsBefore(eventData.quote);
        }, 'QuoteComponent');
        listen('service_quote_collect_totals_after', eventData => {
            this.props.collectTotalsAfter(eventData.quote);
        }, 'QuoteComponent');
        listen('service_quote_change_customer_after', eventData => {
            this.props.changeCustomerAfter(eventData.quote);
        }, 'QuoteComponent');
        listen('service_quote_add_product_after', eventData => {
            this.props.addProductAfter(eventData.quote);
        }, 'QuoteComponent');
        listen('service_quote_update_qty_cart_item_after', eventData => {
            this.props.updateQtyCartItemAfter(eventData.quote);
        }, 'QuoteComponent');
        listen('service_quote_remove_cart_item_after', eventData => {
            this.props.removeCartItemAfter(eventData.quote);
        }, 'QuoteComponent');
        listen('service_quote_place_order_before', eventData => {
            this.props.placeOrderBefore(eventData.quote);
        }, 'QuoteComponent');
    }

    render() {
        return (null)
    }
}

class QuoteContainer extends CoreContainer {
    static className = 'QuoteContainer';

    static mapDispatch(dispatch) {
        return {
            initTotalCollectors: (service) => dispatch(
                TotalsAction.salesQuoteInitTotalCollectors(service)
            ),
            collectTotalsBefore: (quote) => dispatch(
                TotalsAction.salesQuoteCollectTotalsBefore(quote)
            ),
            collectTotalsAfter: (quote) => dispatch(
                TotalsAction.salesQuoteCollectTotalsAfter(quote)
            ),
            changeCustomerAfter: (quote) => dispatch(
                QuoteAction.changeCustomerAfter(quote)
            ),
            addProductAfter: (quote) => dispatch(
                QuoteAction.addProductAfter(quote)
            ),
            updateQtyCartItemAfter: (quote) => dispatch(
                QuoteAction.updateQtyCartItemAfter(quote)
            ),
            removeCartItemAfter: (quote) => dispatch(
                QuoteAction.removeCartItemAfter(quote)
            ),
            placeOrderBefore: (quote) => dispatch(
                QuoteAction.placeOrderBefore(quote)
            ),
        }
    }
}

export default ContainerFactory.get(QuoteContainer).withRouter(
    ComponentFactory.get(QuoteComponent)
);
