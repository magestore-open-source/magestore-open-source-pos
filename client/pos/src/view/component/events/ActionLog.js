/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreContainer from "../../../framework/container/CoreContainer";
import CoreComponent from "../../../framework/component/CoreComponent";
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import ComponentFactory from "../../../framework/factory/ComponentFactory";
import {listen} from "../../../event-bus";
import OrderAction from "../../../view/action/OrderAction";
import CustomerAction from "../../../view/action/CustomerAction";
import OnHoldOrderAction from "../../../view/action/OnHoldOrderAction";

export class ActionLogComponent extends CoreComponent {
    static className = 'ActionLogComponent';

    constructor(props) {
        super(props);
        listen('service_sync_log_order_update_data_finish', eventData => {
            this.props.orderUpdateDataFinish(eventData.result);
        }, 'ActionLogComponent');
        listen('service_sync_save_customer', eventData => {
            this.props.saveCustomer(eventData.result);
        }, 'ActionLogComponent');
        listen('service_sync_update_on_hold_order_finish', eventData => {
            this.props.updateOnHoldOrderFinish(eventData.result);
        }, 'ActionLogComponent');
    }

    render() {
        return (null)
    }
}

class ActionLogContainer extends CoreContainer {
    static className = 'ActionLogContainer';

    static mapDispatch(dispatch) {
        return {
            orderUpdateDataFinish: (result) => dispatch(
                OrderAction.syncActionUpdateDataFinish([result])
            ),
            saveCustomer: (result) => dispatch(
                CustomerAction.saveCustomer([result])
            ),
            updateOnHoldOrderFinish: (result) => dispatch(
                OnHoldOrderAction.syncActionUpdateOnHoldOrderFinish([result])
            ),
        }
    }
}

export default ContainerFactory.get(ActionLogContainer).withRouter(
    ComponentFactory.get(ActionLogComponent)
);
