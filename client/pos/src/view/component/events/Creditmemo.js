/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreContainer from "../../../framework/container/CoreContainer";
import CoreComponent from "../../../framework/component/CoreComponent";
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import ComponentFactory from "../../../framework/factory/ComponentFactory";
import {listen} from "../../../event-bus";
import CreditmemoAction from "../../action/order/CreditmemoAction";

export class CreditmemoComponent extends CoreComponent {
    static className = 'CreditmemoComponent';

    constructor(props) {
        super(props);
        listen('resource_model_order_refund', eventData => {
            this.props.createCreditmemo(eventData.creditmemo);
        }, 'CreditmemoComponent');
        listen('service_creditmemo_refund_operation_refund_after', eventData => {
            this.props.refundOperationRefundAfter(eventData.creditmemo, eventData.order);
        }, 'CreditmemoComponent');
        listen('service_creditmemo_init_total_collectors', eventData => {
            this.props.creditmemoInitTotalCollectors(eventData.service);
        }, 'CreditmemoComponent');
    }

    render() {
        return (null)
    }
}

class CreditmemoContainer extends CoreContainer {
    static className = 'CreditmemoContainer';

    static mapDispatch(dispatch) {
        return {
            createCreditmemo: (creditmemo) => dispatch(
                CreditmemoAction.creditmemoCreateActionLogBefore(creditmemo)
            ),
            refundOperationRefundAfter: (creditmemo, order) => dispatch(
                CreditmemoAction.refundOperationRefundAfter(creditmemo, order)
            ),
            creditmemoInitTotalCollectors: (service) => dispatch(
                CreditmemoAction.salesOrderCreditmemoInitTotalCollectors(service)
            ),
        }
    }
}

export default ContainerFactory.get(CreditmemoContainer).withRouter(
    ComponentFactory.get(CreditmemoComponent)
);
