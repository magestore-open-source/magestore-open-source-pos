/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from 'react';
import SmoothScrollbar from "smooth-scrollbar";
import CartItem from './CartItem';
import CoreComponent from "../../../../framework/component/CoreComponent";
import ComponentFactory from "../../../../framework/factory/ComponentFactory";
import CoreContainer from "../../../../framework/container/CoreContainer";
import ContainerFactory from "../../../../framework/factory/ContainerFactory";
import $ from "jquery";
import _ from 'lodash';
import Swiper from 'swiper/dist/js/swiper';
import QuoteAction from "../../../action/checkout/QuoteAction";
import layout from "../../../../framework/Layout";
import DeviceHelper from "../../../../helper/DeviceHelper";

export class CartItemsComponent extends CoreComponent {
    static className = 'CartItemsComponent';

    /**
     * Init smooth scrollbar for content modal
     */
    initScrollbar(minicart) {
        if (!minicart) return;

        if (this.scrollbar) {
            this.scrollbar.destroy();
        }
        this.scrollbar = SmoothScrollbar.init(minicart);
        if (DeviceHelper.isMobile()) {
            let miniCartEl = $('.minicart');
            if (!miniCartEl.hasClass('minicart-mobile')) {
                miniCartEl.addClass('minicart-mobile');
            }
            new Swiper('.swiper-container', {
                slidesPerView: 'auto',
                grabCursor: true,
                autoHeight: false,
            });
        }

        // scroll to new item add to cart
        if (this.scrollbar.containerEl) {
            if (this.props.added_item_id) {
                this.scrollToItem(this.props.added_item_id);
                this.props.addedItemIdInQuote(null);
            }  else if (this.props.updated_item_id) {
                this.scrollToItem(this.props.updated_item_id);
                this.props.updatedItemIdInQuote(null);
            }
        }
    }

    /**
     * Scroll to the item id
     *
     * @param itemId
     */
    scrollToItem(itemId) {
        let elements = this.scrollbar.containerEl
            .getElementsByClassName('item ' + itemId);
        if (elements && elements.length) {
            setTimeout(() => this.scrollbar.scrollTo(0, elements[0].offsetTop));
        } else {
            setTimeout(() => this.scrollbar.scrollTo(0, 0));
        }
    }

    /**
     * render template
     * @return {*}
     */
    template() {
        let items = _.orderBy(this.props.quote.items, 'item_id', 'desc');
        return (
            <ul className="minicart" ref={this.initScrollbar.bind(this)}>
                <div className={"minicart-content"}>
                    {layout('checkout')('cart')('cart_items')('cart_items_before')()(this)}
                    {
                        items.map((item, index = 0) => {
                            return !item.parent_item_id &&
                                <CartItem item={item} key={item.item_id}/>
                        })
                    }
                </div>
            </ul>
        );
    }
}

/**
 *
 * @type {CartItemsComponent}
 */
const component = ComponentFactory.get(CartItemsComponent);

export class CartItemsContainer extends CoreContainer {
    static className = 'CartItemsContainer';

    /**
     *
     * @param state
     * @return {{quote: *}}
     */
    static mapState(state) {
        const {quote} = state.core.checkout;
        const {added_item_id} = state.core.checkout.addedItemIdInQuote;
        const {updated_item_id} = state.core.checkout.updatedItemIdInQuote;
        return {
            quote,
            added_item_id,
            updated_item_id
        }
    }

    static mapDispatch(dispatch) {
        return {
            addedItemIdInQuote: (product) => dispatch(QuoteAction.addedItemIdInQuote(product)),
            updatedItemIdInQuote: (product) => dispatch(QuoteAction.updatedItemIdInQuote(product))
        }
    }
}

/**
 *
 * @type {CartItemsContainer} container
 */
const container = ContainerFactory.get(CartItemsContainer);

export default container.getConnect(component);
