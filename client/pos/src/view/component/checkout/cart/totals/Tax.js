/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from 'react';
import CoreComponent from "../../../../../framework/component/CoreComponent";
import ComponentFactory from "../../../../../framework/factory/ComponentFactory";
import ContainerFactory from "../../../../../framework/factory/ContainerFactory";
import CoreContainer from "../../../../../framework/container/CoreContainer";
import CurrencyHelper from "../../../../../helper/CurrencyHelper";
import {OverlayTrigger, Popover} from "react-bootstrap";
import TaxHelper from "../../../../../helper/TaxHelper";
import NumberHelper from "../../../../../helper/NumberHelper";
import {fire} from "../../../../../event-bus";

export class CartTotalsTaxComponent extends CoreComponent {
    static className = 'CartTotalsTaxComponent';

    /**
     * Get tax list
     *
     * @return {{}}
     */
    getListTax() {
        let quote = this.props.quote;
        if (!quote) {
            return {};
        }
        let listTax = {};
        let addresses = quote.addresses;
        if (addresses && addresses.length) {
            addresses.forEach(address => {
                let appliedTaxes = address.applied_taxes
                if (appliedTaxes) {
                    let keys = Object.keys(appliedTaxes);
                    if (keys && keys.length) {
                        keys.forEach(key => {
                            if (appliedTaxes[key].rates && appliedTaxes[key].rates.length) {
                                appliedTaxes[key].rates.forEach(rate => {
                                    if (!listTax[rate.code]) {
                                        listTax[rate.code] = rate;
                                    }
                                });
                            }
                        })
                    }
                }
            });
        }
        return listTax;
    }

    /**
     * Render tax total
     *
     * @return {*}
     */
    template() {
        let displayFullTaxSummary = TaxHelper.displayFullTaxSummary();
        let displayValue = CurrencyHelper.format(Math.abs(this.props.total.value));
        let listTax = this.getListTax();
        let popOver = "";
        if (displayFullTaxSummary) {
            popOver = (
                <Popover id="tax popover">
                    <div className="popup-totals-tax">
                        <div className="tax-title">{this.props.t('Tax')} ({displayValue})</div>
                        <div className="tax-content">
                            <div className="img-tax"/>
                            <ul className="list-tax">
                                {
                                    Object.keys(listTax).map(code => {
                                        return <li key={code}>
                                            <span className="mark">{listTax[code].code}</span>
                                            <span className="amount">{NumberHelper.formatDisplayGroupAndDecimalSeparator(listTax[code].percent)}%</span>
                                        </li>;
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </Popover>
            );
        }

        let eventData = {
            component: this,
            popOver: popOver,
            displayFullTaxSummary: displayFullTaxSummary,
            displayValue: displayValue,
            listTax: listTax
        };
        fire('view_cart_total_tax_template_create_tax_detail_popover_template_after', eventData);
        popOver = eventData.popOver;

        return (
            <Fragment key={this.props.total.code}>
                <OverlayTrigger trigger={displayFullTaxSummary ? "click" : null}
                                rootClose placement="right"
                                overlay={popOver}
                                onClick={() => displayFullTaxSummary ? this.props.showBackDrop() : false}
                >
                    <li className={"totals-tax" + (displayFullTaxSummary ? " totals-action" : "")}>
                        <span className="mark">{this.props.total.title}</span>
                        <span className="amount">{displayValue}</span>
                    </li>
                </OverlayTrigger>
            </Fragment>
        )
    }
}

export class CartTotalsTaxContainer extends CoreContainer {
    static className = 'CartTotalsTaxContainer';
}

/**
 *
 * @type {CartTotalsTaxContainer}
 */
const container = ContainerFactory.get(CartTotalsTaxContainer);
export default container.getConnect(ComponentFactory.get(CartTotalsTaxComponent));
