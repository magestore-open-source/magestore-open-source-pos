/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from 'react';
import CoreComponent from "../../../../framework/component/CoreComponent";
import ComponentFactory from "../../../../framework/factory/ComponentFactory";
import ContainerFactory from "../../../../framework/factory/ContainerFactory";
import CoreContainer from "../../../../framework/container/CoreContainer";
import CurrencyHelper from "../../../../helper/CurrencyHelper";
import QuoteAction from "../../../action/checkout/QuoteAction";
import TaxHelper from "../../../../helper/TaxHelper";
import WeeeDataService from "../../../../service/weee/WeeeDataService";
import DiscountComponent from "./totals/Discount";
import ShippingComponent from "../shipping/Shipping";
import TaxComponent from "./totals/Tax";
import {Payment} from "../Payment";

export class CartTotalsComponent extends CoreComponent {
    static className = 'CartTotalsComponent';

    /**
     *   initial state
     *z
     */
    constructor(props) {
        super(props);
        this.showOnPages = [Payment.className];
        this.state = {};
        this.totals = [];
        document.addEventListener('keydown', (event) => {
            const keyName = event.key;
            if (keyName === "Escape") {
                this.hideBackDrop();
            }
        });
    }

    /**
     *  did mount add backdrop
     *
     */
    componentDidMount() {
        this.backDrop = document.createElement("div");
        this.backDrop.className = "modal-backdrop  in popover-backdrop";
        this.backDrop.style.display = "none";
        this.backDrop.onclick = () => this.hideBackDrop();
        document.body.appendChild(this.backDrop);
    }

    /**
     *  Show back drop when display coupon code popover
     *
     * @return
     */
    showBackDrop() {
        this.backDrop.style.display = "block";
    }

    /**
     *  Hidden back drop when dismiss coupon code popover
     *
     * @return
     */
    hideBackDrop() {
        this.backDrop.click();
        this.backDrop.style.display = "none";
    }


    /**
     *  Prepare totals data for template
     *
     * @return rules
     */
    prepareTotals() {
        let {t, quote} = this.props;
        let items = quote.items;
        this.totals = [];
        let subTotal = quote.subtotal;

        let pointDiscount = 0;

        if (TaxHelper.shoppingCartDisplaySubtotalIncludeTax()) {
            subTotal = quote.subtotal_incl_tax;
        }
        if (subTotal !== undefined) {
            this.addToTotals("subtotal", t('Subtotal'), subTotal, "");
        }
        let discount = quote.discount_amount - pointDiscount;

        if (quote.coupon_code) {
            this.addToTotals(
                "discount", t('Discount') + " (" + quote.coupon_code + ")", discount, ""
            );
        } else {
            this.addToTotals("discount", t('Discount'), discount, "");
        }

        if (!quote.is_virtual) {
            let shipping_amount = TaxHelper.shoppingCartDisplayShippingIncludeTax() ?
                quote.shipping_incl_tax : quote.shipping_amount;
            this.addToTotals(
                "shipping",
                t(quote.shipping_method ? 'Shipping' : "Add Shipping"),
                shipping_amount, ""
            );
        }

        let weeeTotal = WeeeDataService.getTotalAmounts(items, quote);
        if (weeeTotal) {
            this.addToTotals("weee", t('FPT'), weeeTotal || 0, "");
        }

        let taxAmount = this.props.quote.tax_amount;
        if (taxAmount || TaxHelper.displayZeroSubtotal()) {
            this.addToTotals("tax", t('Tax'), taxAmount || 0, "");
        }

        if (this.canShow()) {
            this.addToTotals("grand_total", t('Total'), quote.grand_total || 0, "");
        }
    }

    /**
     *  Add data to totals
     *
     * @param code
     * @param title
     * @param value
     * @param unit
     * @return rules
     */
    addToTotals(code, title, value, unit) {
        this.totals.push({
            code: code,
            title: title,
            value: value,
            unit: unit
        })
    }

    /**
     *  Make tempate total by total data
     *
     * @param total
     * @return template total
     */
    getTemplateTotal(total) {
        if (total.code === "discount") {
            return <DiscountComponent key={total.code}
                                      quote={this.props.quote}
                                      total={total}
                                      showBackDrop={() => this.showBackDrop()}
                                      hideBackDrop={() => this.hideBackDrop()}
            />
        } else if (total.code === "shipping") {
            return <ShippingComponent key={total.code}
                                      quote={this.props.quote}
                                      total={total}
            />
        } else if (total.code === "tax") {
            return <TaxComponent key={total.code}
                                 quote={this.props.quote}
                                 total={total}
                                 showBackDrop={() => this.showBackDrop()}
            />
        } else {
            let displayValue = CurrencyHelper.format(Math.abs(total.value), null, null);
            return (
                <Fragment key={total.code}>
                    <li className={total.code}>
                        <span className="mark">{total.title}</span>
                        <span className="amount">{displayValue}</span>
                    </li>
                </Fragment>
            )
        }
    }

    /**
     *  Make tempate totals
     *
     * @return template totals
     */
    getTemplateTotals() {
        let templateTotals = this.totals.map(total => this.getTemplateTotal(total));
        return templateTotals;
    }

    /**
     *  render totls cart
     *
     * @return {*}
     */
    template() {
        this.prepareTotals();
        return (
            <div>
                <div className="cart-totals"
                     style={{display: this.canShow() ? "block" : "none"}}>
                    <ul>
                        {this.getTemplateTotals()}
                    </ul>
                </div>
            </div>
        );
    }
}

/**
 *
 * @type {CartTotalsComponent}
 */
const component = ComponentFactory.get(CartTotalsComponent);

export class CartTotalsContainer extends CoreContainer {
    static className = 'CartTotalsContainer';

    /**
     *
     * @param state
     * @return {{quote: *}}
     */
    static mapState(state) {
        const {quote} = state.core.checkout;
        const {currentPage} = state.core.checkout.index;
        return {
            quote,
            currentPage,
        }
    }

    /**
     *
     * @param dispatch
     * @return {{actions: {placeOrder, placeOrderResult, placeOrderError,
     *     checkoutToSelectPayments}|ActionCreator<any>|ActionCreatorsMapObject}}
     */
    static mapDispatch(dispatch) {
        return {
            actions: {
                setQuote: (quote) => dispatch(QuoteAction.setQuote(quote))
            }
        }
    }
}

/**
 *
 * @type {CartTotalsContainer}
 */
const container = ContainerFactory.get(CartTotalsContainer);
export default container.getConnect(component);