/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React, {Fragment} from 'react';
import CoreComponent from "../../../framework/component/CoreComponent";
import CartItems from "./cart/CartItems";
import CartFooter from "./cart/CartFooter";
import ComponentFactory from "../../../framework/factory/ComponentFactory";
import CoreContainer from "../../../framework/container/CoreContainer";
import ContainerFactory from "../../../framework/factory/ContainerFactory";

import '../../style/css/Cart.css';
import '../../style/css/Customer.css';
import CustomerButton from "../customer/CustomerButton";
import CartTotals from "./cart/CartTotals";
import layout from "../../../framework/Layout";

export class CartComponent extends CoreComponent {
    static className = 'CartComponent';

    /**
     * render template
     *
     * @returns {*}
     */
    template() {
        return (
            <Fragment>
                <div className="wrapper-content-customer">
                    <CustomerButton/>
                    {layout('checkout')('cart')('cart_items_before')()(this)}
                    <CartItems/>
                    <CartTotals/>
                    <CartFooter/>
                </div>
            </Fragment>
        );
    }
}

/**
 *
 * @type {CartComponent}
 */
const component = ComponentFactory.get(CartComponent);

export class CartContainer extends CoreContainer{
    static className = 'CartContainer';
}

/**
 *
 * @type {CartContainer}
 */
const container = ContainerFactory.get(CartContainer);
export default container.getConnect(component);
