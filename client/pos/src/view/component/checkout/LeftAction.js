/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import React from 'react';
import CoreComponent from "../../../framework/component/CoreComponent";
import ComponentFactory from "../../../framework/factory/ComponentFactory";
import AddComment from "./left-action/AddComment";
import RemoveCart from "./left-action/RemoveCart";
import CoreContainer from "../../../framework/container/CoreContainer";
import ContainerFactory from "../../../framework/factory/ContainerFactory";
import MultiCheckout from "./MultiCheckout";
import '../../style/css/CartLeftAction.css';

export class LeftActionComponent extends CoreComponent {
    static className = 'LeftActionComponent';
    template() {
        return (
            <div className="wrapper-action-left">
                <AddComment/>
                <RemoveCart/>
                <MultiCheckout/>
            </div>
        );
    }
}

/**
 *
 * @type {LeftActionComponent}
 */
const component = ComponentFactory.get(LeftActionComponent);

export class LeftActionContainer extends CoreContainer{
    static className = 'LeftActionContainer';
}

/**
 *
 * @type {LeftActionContainer}
 */
const container = ContainerFactory.get(LeftActionContainer);
export default container.getConnect(component)
