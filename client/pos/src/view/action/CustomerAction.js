/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CustomerConstant from "../../view/constant/CustomerConstant";

export default {
    /**
     * search customer
     *
     * @param {object} queryService
     * @param {string} searchKey
     * @returns {{type: string, queryService: *}}
     */
    searchCustomer: (queryService, searchKey = '') => {
        return {
            type: CustomerConstant.SEARCH_CUSTOMER,
            queryService: queryService,
            search_key: searchKey
        }
    },

    /**
     * Action dispatch result of search customer action
     *
     * @param {object[]} customers
     * @param {object} search_criteria
     * @param {number} totalCount
     * @param {string} searchKey
     * @return {object}
     */
    searchCustomerResult: (customers = [], search_criteria = {}, totalCount = 0, searchKey = '') => {
        return {
            type: CustomerConstant.SEARCH_CUSTOMER_RESULT,
            customers: customers,
            search_criteria: search_criteria,
            total_count: totalCount,
            search_key: searchKey
        }
    },

    /**
     * Action dispatch reset customer list
     *
     * @return {object}
     */
    resetCustomerList: () => {
        return {
            type: CustomerConstant.RESET_CUSTOMER_LIST
        }
    },

    /**
     * create customer
     * @param customer
     * @returns {{type: string, customer: *}}
     */
    createCustomer: (customer) => {
        return {
            type: CustomerConstant.CREATE_CUSTOMER,
            customer: customer
        }
    },

    /**
     * create customer success
     * @param customer
     * @returns {{type: string, customer: *}}
     */
    createCustomerSuccess: (customer) => {
        return {
            type: CustomerConstant.CREATE_CUSTOMER_SUCCESS,
            customer: customer
        }
    },

    /**
     * create customer
     * @param error
     * @returns {{type: string, customer: *}}
     */
    createCustomerError: (error) => {
        return {
            type: CustomerConstant.CREATE_CUSTOMER_ERROR,
            error: error
        }
    },

    /**
     * edit customer
     * @param customer
     * @param isShipping
     * @returns {{type: string, customer: *, isShipping: boolean}}
     */
    editCustomer: (customer, isShipping = false) => {
        return {
            type: CustomerConstant.EDIT_CUSTOMER,
            customer: customer,
            isShipping: isShipping
        }
    },

    /**
     * edit customer error
     * @param error
     * @returns {{type: string, error: *}}
     */
    editCustomerError: (error) => {
        return {
            type: CustomerConstant.EDIT_CUSTOMER_ERROR,
            error: error
        }
    },

    /**
     * save customer
     * @param customers
     * @param isShipping
     * @returns {{type: string, customers: *, isShipping: boolean}}
     */
    saveCustomer(customers, isShipping = false) {
        return {
            type: CustomerConstant.SAVE_CUSTOMER,
            customers: customers,
            isShipping: isShipping
        }
    },

    /**
     * reset isShipping
     * @returns {{type: string}}
     */
    resetIsShipping() {
        return {
            type: CustomerConstant.RESET_IS_SHIPPING,
        }
    },

    /**
     * Sync action update customer data finish
     * @param items
     * @return {{type: string, orders: Array}}
     */
    syncActionUpdateDataFinish(items = []) {
        return {
            type: CustomerConstant.SYNC_ACTION_UPDATE_DATA_FINISH,
            items: items
        }
    },

    /**
     * Sync deleted customer finish
     * @param ids
     * @return {{type: string, ids: Array}}
     */
    syncDeletedCustomerFinish(ids = []) {
        return {
            type: CustomerConstant.SYNC_DELETED_CUSTOMER_FINISH,
            ids: ids
        }
    },
}