/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import MenuConstant from "../constant/MenuConstant";

/**
 *  action emit whenever user click MenuItem
 *
 * @param {Object} active
 * @return {{type, active: *}}
 */
export const clickMenuItem = (active) => {
    return {
        type: MenuConstant.CLICK_MENU_ITEM,
        active
    }
};

/**
 * action emit whenever user click export data button
 *
 * @return {{type}}
 */
export const clickExportItem = () => {
    return {
        type: MenuConstant.CLICK_EXPORT_ITEM,
    }
};

/**
 * action emit whenever user click Logout button
 *
 * @return {{type}}
 */
export const clickLogoutItem = () => {
    return {
        type: MenuConstant.CLICK_LOGOUT_ITEM,
    }
};

/**
 * toggle menu
 *
 * @return {{type}}
 */
export const toggle= () => {
    return {
        type: MenuConstant.TOGGLE,
    }
};


/**
 * combine actions to export
 * @type {{clickMenuItem: function(Object), clickLogoutItem: function()}}
 */
const MenuAction = {
    clickMenuItem,
    clickExportItem,
    clickLogoutItem,
    toggle
};

export default MenuAction