/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ProductConstant from '../constant/ProductConstant';

export default {

    /**
     * search product
     *
     * @param queryService
     * @param {string} searchKey
     * @returns {{type: string, queryService: *}}
     */
    searchProduct: (queryService, searchKey = '') => {
        return {
            type: ProductConstant.SEARCH_PRODUCT,
            queryService: queryService,
            search_key: searchKey
        }
    },

    /**
     * Action dispatch result of search product action
     *
     * @param {object[]} products
     * @param {object} search_criteria
     * @param {number} total_count
     * @param {string} search_key
     * @param {string} requestMode
     * @return type, products
     */
    searchProductResult: (products = [], search_criteria = {}, total_count = 0, search_key = '', requestMode) => {
        return {
            type: ProductConstant.SEARCH_PRODUCT_RESULT,
            products: products,
            search_criteria: search_criteria,
            total_count: total_count,
            search_key: search_key,
            request_mode: requestMode
        }
    },

    /**
     * action get list product by keyword
     * @param searchKey
     * @return type, search_key
     */
    getListProduct: (searchKey = '', currentPage = 1, pageSize = 16) => {
        return {
            type: ProductConstant.GET_LIST_PRODUCT,
            search_key: searchKey,
            currentPage: currentPage,
            pageSize: pageSize
        }
    },

    /**
     * action result list product
     * @param products
     * @return type, products
     */
    getListProductResult: (products = []) => {
        return {
            type: ProductConstant.GET_LIST_PRODUCT_RESULT,
            products: products
        }
    },

    /**
     * View product action
     *
     * @param product
     * @returns {{product: null, type: string}}
     */
    viewProduct: (product = null) => {
        return {
            type: ProductConstant.VIEW_PRODUCT,
            product: product
        }
    },

    /**
     * Search by barcode
     * @param code
     * @returns {{type: string, code: *}}
     */
    searchByBarcode: (code) => {
        return {
            type: ProductConstant.SEARCH_BY_BARCODE,
            code: code
        }
    },

    /**
     * Sync action update product data finish
     *
     * @param items
     * @return {{type: string, products: *}}
     */
    syncActionUpdateProductDataFinish(items = []) {
        return {
            type: ProductConstant.SYNC_ACTION_UPDATE_PRODUCT_DATA_FINISH,
            products: items
        }
    },

    /**
     * Sync action delete product data finish
     *
     * @param ids
     * @return {{type: string, ids: *}}
     */
    syncActionDeleteProductDataFinish(ids = []) {
        return {
            type: ProductConstant.SYNC_ACTION_DELETE_PRODUCT_DATA_FINISH,
            ids: ids
        }
    },

    /**
     * Sync action update stock data finish
     *
     * @param items
     * @return {{type: string, stocks: *}}
     */
    syncActionUpdateStockDataFinish(items = []) {
        return {
            type: ProductConstant.SYNC_ACTION_UPDATE_STOCK_DATA_FINISH,
            stocks: items
        }
    },

    /**
     * Sync action update catalog rule price data finish
     * @param items
     * @return {{type: string, catalogrule_prices: Array}}
     */
    syncActionUpdateCatalogRulePriceDataFinish(items = []) {
        return {
            type: ProductConstant.SYNC_ACTION_UPDATE_CATALOG_RULE_PRICE_DATA_FINISH,
            catalogrule_prices: items
        }
    },

    /**
     * Sync action deleted catalog rule price data finish
     * @param ids
     * @return {{type: string, ids: Array}}
     */
    syncActionDeletedCatalogRulePriceDataFinish(ids = []) {
        return {
            type: ProductConstant.SYNC_ACTION_DELETED_CATALOG_RULE_PRICE_DATA_FINISH,
            ids: ids
        }
    }
}
