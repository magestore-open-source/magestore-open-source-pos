/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import SignoutConstant from "../constant/SignoutConstant";

export default {
    /**
     * Get new location list
     *
     * @returns {{type: string, queryService: *}}
     */
     getNewLocations: (queryService) => {
        return {
            type: SignoutConstant.GET_NEW_LOCATION_LIST,
            queryService: queryService
        }
    },

    /**
     * Force sign out successfully
     *
     * @returns {{type: string, queryService: *}}
     */
     forceSignOutSuccess: () => {
        return {
            type: SignoutConstant.FORCE_SIGN_OUT_SUCCESS
        }
    }

}
