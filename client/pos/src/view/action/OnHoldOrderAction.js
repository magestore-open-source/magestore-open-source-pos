/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import OnHoldOrderConstant from '../constant/OnHoldOrderConstant';

export default {
    /**
     * action hold order
     * @param quote
     * @return {{type: string, quote: *}}
     */
    holdOrder: (quote) => {
        return {
            type: OnHoldOrderConstant.HOLD_ORDER,
            quote: quote
        }
    },

    /**
     * hold order result
     * @param order
     * @return {{type: string, order: *}}
     */
    holdOrderResult: (order) => {
        return {
            type: OnHoldOrderConstant.HOLD_ORDER_RESULT,
            order: order
        }
    },

    /**
     * action holder order after
     * @param {object} order
     * @param {object} quote
     * @returns {{type: string, order: *}}
     */
    holderOrderAfter: (order, quote) => {
        return {
            type: OnHoldOrderConstant.HOLD_ORDER_AFTER,
            order: order,
            quote: quote
        }
    },

    /**
     * Sync action update on hold order data finish
     * @param items
     * @return {{type: string, orders: Array}}
     */
    syncActionUpdateOnHoldOrderFinish(items = []) {
        return {
            type: OnHoldOrderConstant.SYNC_ACTION_UPDATE_ON_HOLD_ORDER_FINISH,
            orders: items
        }
    },

    /**
     * Sync deleted hold order finish
     * @param ids
     * @return {{type: string, ids: Array}}
     */
    syncDeletedHoldOrderFinish(ids = []) {
        return {
            type: OnHoldOrderConstant.SYNC_DELETED_HOLD_ORDER_FINISH,
            ids: ids
        }
    },

    /**
     * action search hold order
     * @param queryService
     * @param searchKey
     * @return {{type: string, queryService: *, search_key: string}}
     */
    searchOrder: (queryService, searchKey = '') => {
        return {
            type: OnHoldOrderConstant.SEARCH_ORDER,
            queryService: queryService,
            search_key: searchKey,
        }
    },

    /**
     * action search hold order result
     * @param orders
     * @param search_criteria
     * @param total_count
     * @param search_key
     * @param request_mode
     * @return {{type: string, orders: Array, search_criteria, total_count: number, search_key: string, request_mode: *}}
     */
    searchOrderResult: (
        orders = [],
        search_criteria = {},
        total_count = 0,
        search_key = '',
        request_mode
    ) => {
        return {
            type: OnHoldOrderConstant.SEARCH_ORDER_RESULT,
            orders: orders,
            search_criteria: search_criteria,
            total_count: total_count,
            search_key: search_key,
            request_mode: request_mode
        }
    },

    /**
     * Delete on hold order
     * @param order
     * @return {{type: string, order: *}}
     */
    deleteOrder: (order) => {
        return {
            type: OnHoldOrderConstant.DELETE_ON_HOLD_ORDER,
            order: order
        }
    },

    /**
     * Cancel on hold order
     * @param order
     * @param history
     * @returns {{history: *, type: string, order: *}}
     */
    cancelOrder: (order, history = null) => {
        return {
            type: OnHoldOrderConstant.CANCEL_ON_HOLD_ORDER,
            order: order,
            history: history
        }
    },

    /**
     * action cancel order after
     * @param order
     * @param history
     * @returns {{history: *, type: string, order: *}}
     */
    cancelOrderAfter: (order, history = null) => {
        return {
            type: OnHoldOrderConstant.CANCEL_ON_HOLD_ORDER_AFTER,
            order: order,
            history: history
        }
    }
}