/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ConfigConstant from '../constant/ConfigConstant';

export default {
    /**
     * action get config
     * @param atLoadingPage
     * @returns {{type: string, atLoadingPage: boolean}}
     */
    getConfig: (atLoadingPage = false) => {
        return {
            type: ConfigConstant.GET_CONFIG,
            atLoadingPage: atLoadingPage
        }
    },

    /**
     * action result get config
     * @param configs
     * @return type, configs
     */
    getConfigResult: (configs = []) => {
        return {
            type: ConfigConstant.GET_CONFIG_RESULT,
            configs: configs
        }
    },

    /**
     * get config error
     * @param error
     * @returns {{type: string, configs: module.exports.configs|{recommended, all}}}
     */
    getConfigError: (error) => {
        return {
            type: ConfigConstant.GET_CONFIG_ERROR,
            error: error
        }
    }
}
