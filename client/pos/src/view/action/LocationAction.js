/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import UserConstant from "../constant/UserConstant";

export default {
    /**
     * action user assign pos
     * @param posId
     * @param locationId
     * @param currentStaffId
     * @returns {{type: string, posId: string, locationId: string, currentStaffId: string}}
     */
    assignPos: (posId = '', locationId = '', currentStaffId = '') => {
        return {
            type: UserConstant.USER_ASSIGN_POS,
            posId,
            locationId,
            currentStaffId
        }
    },
    
    /**
     * action user assign pos response
     * @returns {{type: string}}
     */
    assignPosResponse: () => {
        return {
            type: UserConstant.USER_ASSIGN_POS_RESPONSE,
            assignPos: true
        }
    },
    
    /**
     * action user assign pos error
     * @param error
     * @returns {{type: string, error: *}}
     */
    assignPosError: (error) => {
        return {
            type: UserConstant.USER_ASSIGN_POS_ERROR,
            error
        }
    }

}