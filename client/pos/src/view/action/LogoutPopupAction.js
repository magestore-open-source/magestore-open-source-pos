/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import LogoutPopupConstant from "../constant/LogoutPopupConstant";

/**
 * action emit whenever user click outside modal logout
 *
 * @return {{type}}
 */
export const toggle = () => {
    return {
        type: LogoutPopupConstant.TOGGLE_LOGOUT_ALERT,
    }
};

/**
 * action emit whenever user click outside modal logout
 *
 * @return {{type}}
 */
export const clickBackDrop = () => {
    return {
        type: LogoutPopupConstant.CLICK_MODAL_BACKDROP,
    }
};

/**
 * action emit whenever user click "Yes" option modal logout
 *
 * @return {{type}}
 */
export const clickLogOut = () => {
    return {
        type: LogoutPopupConstant.CLICK_MODAL_YES
    }
};

/**
 * action emit when logout request done
 *
 * @param response
 * @return {{type, response: *}}
 */
export const finishLogoutRequesting = (response) => {
    return {
        type: LogoutPopupConstant.FINISH_LOGOUT_REQUESTING,
        response
    }
};

/**
 * action emit when logout request has error
 *
 * @param reason
 * @return {{type, reason: *}}
 */
export const logoutRequestingError = (reason) => {
    return {
        type: LogoutPopupConstant.LOGOUT_REQUESTING_ERROR,
        reason
    }
};

/**
 * action force signout
 *
 * @return {{type}}
 */
export const forceSignOut = () => {
    return {
        type: LogoutPopupConstant.FORCE_SIGN_OUT
    }
};

/**
 * action force authorize
 * @returns {{type: string}}
 */
export const authorize = () => {
    return {
        type: LogoutPopupConstant.LOGOUT_RE_AUTHORIZE
    }
};

/**
 * action emit when error alert dismiss
 *
 * @return {{type}}
 */
export const afterErrorAlertDismiss = () => {
    return {
        type: LogoutPopupConstant.AFTER_ERROR_ALERT_DISMISS,
    }
};

export const reInit = () => {
    return {
        type: LogoutPopupConstant.LOGOUT_RE_INIT,
    }
};

/**
 * Combine actions
 *
 * @type {{toggle: function(), clickBackDrop: function(), clickLogOut: function(), finishLogoutRequesting: function(*), forceSignOut: function(), logoutRequestingError: function(*), afterErrorAlertDismiss: function(), reInit: function()}}
 */
export default {
    toggle,
    clickBackDrop,
    clickLogOut,
    finishLogoutRequesting,
    forceSignOut,
    logoutRequestingError,
    afterErrorAlertDismiss,
    reInit,
    authorize
};