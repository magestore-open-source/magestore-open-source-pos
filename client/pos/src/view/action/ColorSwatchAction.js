/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ColorSwatchConstant from '../constant/ColorSwatchConstant';

export default {
    /**
     * action get color swatch
     * @param atLoadingPage
     * @returns {{type: string, atLoadingPage: boolean}}
     */
    getColorSwatch: (atLoadingPage = false) => {
        return {
            type: ColorSwatchConstant.GET_COLOR_SWATCH,
            atLoadingPage: atLoadingPage
        }
    },

    /**
     * action result get color swatch
     * @param configs
     * @return type, configs
     */
    getColorSwatchResult: (configs = []) => {
        return {
            type: ColorSwatchConstant.GET_COLOR_SWATCH_RESULT,
            configs: configs
        }
    },

    /**
     * get color swatch error
     * @param error
     * @returns {{type: string, configs: module.exports.configs|{recommended, all}}}
     */
    getColorSwatchError: (error) => {
        return {
            type: ColorSwatchConstant.GET_COLOR_SWATCH_ERROR,
            error: error
        }
    }
}
