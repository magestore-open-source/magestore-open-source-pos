/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ConfigConstant from "../constant/tax/ConfigConstant";

export default {

    /**
     * Get tax rate
     *
     * @return {{type}}
     */
    getTaxRate: () => {
        return {
            type: ConfigConstant.GET_TAX_RATE_ONLINE
        }
    },

    /**
     * Action get tax rates result
     *
     * @param taxRates
     * @returns {{type: string, taxRules: *}}
     */
    getTaxRateResult: (taxRates) => {
        return {
            type: ConfigConstant.GET_TAX_RATE_ONLINE_RESULT,
            taxRates: taxRates
        }
    },

    /**
     * Aaction get tax rate error
     *
     * @param error
     * @returns {{type: string, error: *}}
     */
    getTaxRateError: (error) => {
        return {
            type: ConfigConstant.GET_TAX_RATE_ONLINE_ERROR,
            error: error
        }
    },

    /**
     * Get tax rule
     *
     * @return {{type}}
     */
    getTaxRule: () => {
        return {
            type: ConfigConstant.GET_TAX_RULE_ONLINE
        }
    },

    /**
     * Action get tax rules result
     *
     * @param taxRules
     * @returns {{type: string, taxRules: *}}
     */
    getTaxRuleResult: (taxRules) => {
        return {
            type: ConfigConstant.GET_TAX_RULE_ONLINE_RESULT,
            taxRules: taxRules
        }
    },

    /**
     * Action get tax rule error
     *
     * @param error
     * @returns {{type: string, error: *}}
     */
    getTaxRuleError: (error) => {
        return {
            type: ConfigConstant.GET_TAX_RULE_ONLINE_ERROR,
            error: error
        }
    }
}
