/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import MultiCheckoutConstant from '../constant/MultiCheckoutConstant';

export default {
    /**
     *
     * @param activeCartId
     * @param isActiveLatest
     * @return {*}
     */
    getListCart: (activeCartId, isActiveLatest) => {
        isActiveLatest = isActiveLatest || false;
        return {
            activeCartId,
            isActiveLatest,
            type: MultiCheckoutConstant.GET_LIST_CART,
        }
    },

    /**
     * action add cart
     * @returns {{type: string, cart: *}}
     */
    addCart: () => {
        return {
            type: MultiCheckoutConstant.ADD_CART,
        }
    },

    /**
     * action select cart
     * @param cart
     * @returns {{type: string, cart: *}}
     */
    selectCart: (cart) => {
        return {
            type: MultiCheckoutConstant.SELECT_CART,
            cart
        }
    },

    /**
     * action delete cart
     * @param cart
     * @returns {{type: string, cart: *}}
     */
    deleteCart: (cart) => {
        return {
            type: MultiCheckoutConstant.DELETE_CART,
            cart
        }
    },
    /**
     * action get carts in indexed db
     * @param carts
     * @returns {{type: string, carts: *}}
     */
    getListCartResult: (carts) => {
        return {
            type: MultiCheckoutConstant.GOT_LIST_CART,
            carts
        }
    },

    /**
     * action add cart into indexed db
     * @param cart
     * @returns {{type: string, cart: *}}
     */
    addCartResult: (cart) => {
        return {
            type: MultiCheckoutConstant.ADDED_CART,
            cart
        }
    },

    /**
     * action delete cart in indexed db
     * @param cart
     * @returns {{type: string, cart: *}}
     */
    deleteCartResult: (cart) => {
        return {
            type: MultiCheckoutConstant.DELETED_CART,
            cart
        }
    },

    /**
     * action after select cart from epic
     * @param cart
     * @returns {{type: string, cart: *}}
     */
    selectCartResult: (cart) => {
        return {
            type: MultiCheckoutConstant.SELECTED_CART,
            cart
        }
    },

    /**
     * action update cart
     * @param cart
     * @param callback
     * @returns {{type: string, cart: *}}
     */
    updateCart: (cart, callback) => {
        callback = callback || function () {};
        return {
            type: MultiCheckoutConstant.UPDATE_CART,
            cart,
            callback
        }
    },

    /**
     * action after updated cart
     * @param cart
     * @returns {{type: string, cart: *}}
     */
    updateCartResult: (cart) => {
        return {
            type: MultiCheckoutConstant.UPDATED_CART,
            cart
        }
    },
}
