/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import LoadingConstant from '../constant/LoadingConstant';

export default {
    /**
     * action update finished list
     *
     * @param dataType
     * @returns {{dataType: *, type: string}}
     */
    updateFinishedList: (dataType) => {
        return {
            type: LoadingConstant.UPDATE_FINISHED_LIST,
            dataType: dataType
        }
    },

    /**
     * Reset State
     * @returns {{type: string}}
     */
    resetState: () => {
        return {
            type: LoadingConstant.RESET_STATE
        }
    },

    /**
     * Clear Data
     * @returns {{type: string}}
     */
    clearData: () => {
        return {
            type: LoadingConstant.CLEAR_DATA
        }
    },

    /**
     * Clear data success
     * @returns {{type: string}}
     */
    clearDataSuccess: () => {
        return {
            type: LoadingConstant.CLEAR_DATA_SUCCESS
        }
    },

    /**
     * Clear data error
     * @returns {{type: string}}
     */
    clearDataError: (error) => {
        return {
            type: LoadingConstant.CLEAR_DATA_ERROR,
            error
        }
    }
}
