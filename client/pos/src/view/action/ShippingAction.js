/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ShippingConstant from '../constant/ShippingConstant';

export default {
    /**
     * action get shipping online
     * @param atLoadingPage
     * @returns {{type: string, atLoadingPage: boolean}}
     */
    getShippingOnline: (atLoadingPage = false) => {
        return {
            type: ShippingConstant.GET_SHIPPING_ONLINE,
            atLoadingPage: atLoadingPage
        }
    },

    /**
     * action get list Shipping
     * @returns {{type: string}}
     */
    getListShipping: () => {
        return {
            type: ShippingConstant.GET_LIST_SHIPPING
        }
    },

    /**
     * action result get list Shipping
     * @param shipping_methods
     * @returns {{type: string, shipping_methods: *}}
     */
    getListShippingResult: (shipping_methods) => {
        return {
            type: ShippingConstant.GET_LIST_SHIPPING_RESULT,
            shipping_methods: shipping_methods
        }
    },

    /**
     * action result get list Shipping
     * @param quote
     * @param address
     * @returns {{type: string, shipping_methods: *}}
     */
    getShippingMethods: (quote = null, address = null) => {
        return {
            type: ShippingConstant.GET_SHIPPING_METHODS,
            quote: quote,
            address: address
        }
    },

    /**
     * Action save shipping
     *
     * @param quote
     * @param address
     * @param shippingMethod
     * @param deliveryDate
     * @return {object}
     */
    saveShipping(quote, address, shippingMethod, deliveryDate) {
        return {
            type: ShippingConstant.SAVE_SHIPPING,
            quote: quote,
            address: address,
            shipping_method: shippingMethod,
            delivery_date: deliveryDate
        }
    },

    /**
     * Action save shipping
     *
     * @param quote
     * @return {object}
     */
    saveShippingAfter(quote) {
        return {
            type: ShippingConstant.SAVE_SHIPPING_AFTER,
            quote: quote
        }
    }


}
