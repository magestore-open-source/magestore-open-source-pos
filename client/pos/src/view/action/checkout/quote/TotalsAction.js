/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import TotalsConstant from '../../../constant/checkout/quote/TotalsConstant';

export default {

    /**
     * Init quote total
     *
     * @param totalService
     * @returns {{type: string, service: *}}
     */
    salesQuoteInitTotalCollectors: (totalService) => {
        return {
            type: TotalsConstant.SALES_QUOTE_INIT_TOTAL_COLLECTORS,
            service: totalService
        }
    },

    /**
     * Collect quote total before
     *
     * @param quote
     * @returns {{type: string, quote: *}}
     */
    salesQuoteCollectTotalsBefore: (quote) => {
        return {
            type: TotalsConstant.SALES_QUOTE_COLLECT_TOTALS_BEFORE,
            quote: quote
        }
    },

    /**
     * Collect quote total after
     *
     * @param quote
     * @returns {{type: string, quote: *}}
     */
    salesQuoteCollectTotalsAfter: (quote) => {
        return {
            type: TotalsConstant.SALES_QUOTE_COLLECT_TOTALS_AFTER,
            quote: quote
        }
    },
}
