/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CheckoutConstant from '../constant/CheckoutConstant';

export default {
    /**
     * action place order
     * @param quote
     * @param additionalData
     * @return {{type: string, quote: *, additionalData: *}}
     */
    placeOrder: (quote, additionalData) => {
        return {
            type: CheckoutConstant.CHECK_OUT_PLACE_ORDER,
            quote: quote,
            additionalData
        }
    },

    /**
     * action result order after place order
     * @param order
     * @returns {{type: string, order: *}}
     */
    placeOrderResult: (order) => {
        return {
            type: CheckoutConstant.CHECK_OUT_PLACE_ORDER_RESULT,
            order: order
        }
    },

    /**
     * action result error after place order
     * @param error
     * @returns {{type: string, error: *}}
     */
    placeOrderError: (error) => {
        return {
            type: CheckoutConstant.CHECK_OUT_PLACE_ORDER_ERROR,
            error: error
        }
    },

    /**
     *  whenever user click grand total, select payment show
     * @param quote
     * @return {{type: string, quote: *}}
     */
    checkoutToSelectPayments: (quote, initPayments) => {
        return {
            type: CheckoutConstant.CHECKOUT_TO_SELECT_PAYMENTS,
            quote,
            initPayments
        }
    },

    /**
     *  before user click grand total, init payment
     * @param quote
     * @return {{type: string, quote: *}}
     */
    initPayments: (quote) => {
        return {
            type: CheckoutConstant.CHECKOUT_INIT_PAYMENTS,
            quote
        }
    },

    /**
     *  whenever user click back on select payment, product list show
     * @return {{type: string, quote: *}}
     */
    checkoutToCatalog: () => {
        return {
            type: CheckoutConstant.CHECKOUT_TO_CATALOG,
        }
    },
    switchPage: (page) => {
        return {
            type: CheckoutConstant.CHECKOUT_SWITCH_PAGE,
            page
        }
    },

    /**
     * action process payments
     * @param quote
     * @returns {{type: string, quote: *}}
     */
    processPayment: (quote) => {
        return {
            type: CheckoutConstant.CHECKOUT_PROCESS_PAYMENT,
            quote: quote
        }
    },

    /**
     * action process payments result
     * @param result
     * @param payment
     * @param index
     * @return {{type: string, result: *, payment: *, index: *}}
     */
    processSinglePaymentResult: (result, payment, index) => {
        return {
            type: CheckoutConstant.CHECKOUT_PROCESS_SINGLE_PAYMENT_RESULT,
            result,
            payment,
            index
        }
    },

    /**
     * action process payments error
     * @param result
     * @param payment
     * @param index
     * @return {{type: string, result: *, payment: *, index: *}}
     */
    processSinglePaymentError: (result, payment, index) => {
        return {
            type: CheckoutConstant.CHECKOUT_PROCESS_SINGLE_PAYMENT_ERROR,
            result,
            payment,
            index
        }
    },

}
