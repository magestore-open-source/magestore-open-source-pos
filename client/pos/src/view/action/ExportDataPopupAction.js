/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ExportDataPopupConstant from "../constant/ExportDataPopupConstant";

/**
 * action emit whenever user click outside modal export
 *
 * @return {{type}}
 */
export const clickBackDrop = () => {
    return {
        type: ExportDataPopupConstant.CLICK_MODAL_BACKDROP,
    }
};

/**
 * action emit when export request done
 *
 * @param response
 * @return {{type, response: *}}
 */
export const finishExportDataRequesting = (response) => {
    return {
        type: ExportDataPopupConstant.FINISH_EXPORT_REQUESTING,
        response
    }
};

/**
 * Combine actions
 */
export default {
    clickBackDrop,
    finishExportDataRequesting
};