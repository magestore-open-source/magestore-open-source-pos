/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    XML_PATH_MANAGE_STOCK: 'cataloginventory/item_options/manage_stock',
    XML_PATH_BACKORDERS: 'cataloginventory/item_options/backorders',
    XML_PATH_MAX_SALE_QTY: 'cataloginventory/item_options/max_sale_qty',
    XML_PATH_MIN_SALE_QTY: 'cataloginventory/item_options/min_sale_qty',
    XML_PATH_MIN_QTY: 'cataloginventory/item_options/min_qty',
    XML_PATH_ENABLE_QTY_INCREMENTS: 'cataloginventory/item_options/enable_qty_increments',
    XML_PATH_QTY_INCREMENTS: 'cataloginventory/item_options/qty_increments',
    XML_PATH_CAN_SUBTRACT: 'cataloginventory/options/can_subtract',
    XML_PATH_CAN_BACK_IN_STOCK: 'cataloginventory/options/can_back_in_stock'
}
