/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export const TOGGLE_LOGOUT_ALERT = '[LOGOUT] TOGGLE_LOGOUT_ALERT';
export const CLICK_MODAL_BACKDROP = '[LOGOUT] CLICK_MODAL_BACKDROP';
export const CLICK_MODAL_YES = '[LOGOUT] CLICK_MODAL_YES';
export const FORCE_SIGN_OUT = '[LOGOUT] FORCE_SIGN_OUT';
export const BEGIN_LOGOUT_REQUESTING = '[LOGOUT] BEGIN_LOGOUT_REQUESTING';
export const FINISH_LOGOUT_REQUESTING = '[LOGOUT] FINISH_LOGOUT_REQUESTING';
export const LOGOUT_REQUESTING_ERROR= '[LOGOUT] LOGOUT_REQUESTING_ERROR';
export const AFTER_ERROR_ALERT_DISMISS= '[LOGOUT] AFTER_ERROR_ALERT_DISMISS';
export const LOGOUT_RE_INIT= '[LOGOUT] LOGOUT_RE_INIT';
export const LOGOUT_RE_AUTHORIZE= '[LOGOUT] LOGOUT_RE_AUTHORIZE';

export default {
    TOGGLE_LOGOUT_ALERT,
    CLICK_MODAL_BACKDROP,
    CLICK_MODAL_YES,
    FORCE_SIGN_OUT,
    BEGIN_LOGOUT_REQUESTING,
    FINISH_LOGOUT_REQUESTING,
    LOGOUT_REQUESTING_ERROR,
    AFTER_ERROR_ALERT_DISMISS,
    LOGOUT_RE_INIT,
    LOGOUT_RE_AUTHORIZE
}