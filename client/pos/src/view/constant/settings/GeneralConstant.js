/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    GET_IS_SYNC_DATA_TO_BROWSER_STORAGE : '[SETTING] GET_IS_SYNC_DATA_TO_BROWSER_STORAGE',
    // Config
    CONFIG_XML_PATH_PERFORMANCE_POS_DEFAULT_MODE: 'webpos/performance/pos_default_mode',
    CONFIG_XML_PATH_PERFORMANCE_POS_TABLET_DEFAULT_MODE: 'webpos/performance/pos_tablet_default_mode',
}
