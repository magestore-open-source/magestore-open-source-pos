/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    USER_CLICK_LOGIN: '[USER] CLICK_LOGIN',
    USER_LOGIN_RESULT: '[USER] LOGIN_RESULT',
    USER_LOGIN_ERROR: '[USER] LOGIN_ERROR',
    USER_ASSIGN_POS: '[USER] ASSIGN_POS',
    USER_ASSIGN_POS_RESPONSE: '[USER] ASSIGN_POS_RESPONSE',
    USER_ASSIGN_POS_ERROR: '[USER] ASSIGN_POS_ERROR',
    USER_CLOSE_POPUP: '[USER] CLOSE_POPUP',
    USER_GET_LOGO: '[USER] GET_LOLO',
    USER_GET_LOGO_SUCCESS: '[USER] GET_LOGO_SUCCESS',
    USER_GET_LOGO_ERROR: '[USER] GET_LOGO_ERROR',
    USER_GET_COUNTRIES: '[USER] GET_COUNTRIES',
    USER_CHANGE_INFORMATION: '[USER] CHANGE_INFORMATION',
    USER_CHANGE_INFORMATION_SUCCESS: '[USER] CHANGE_INFORMATION_SUCCESS',
    USER_CHANGE_INFORMATION_ERROR: '[USER] CHANGE_ERROR',
    USER_CONTINUE_LOGIN : '[USER] CONTINUE_LOGIN',
    USER_AFTER_CONTINUE_LOGIN : '[USER] AFTER_CONTINUE_LOGIN'
};