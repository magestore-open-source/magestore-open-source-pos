/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    MAX_DIFF_TIME_WITH_SCAN_BARCODE: 100,
    SET_BARCODE_STRING: "[SCAN] SET BARCODE STRING",
    SET_SCAN_PAGE: "[SCAN] SET SCAN PAGE",
    SCAN_PAGES: {
        PRODUCT: "PRODUCT",
        CUSTOMER: "CUSTOMER",
        ORDER: "ORDER",
        ONHOLD_ORDER: "ONHOLD_ORDER",
        LOGIN: "LOGIN"
    }
}
