/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    CLEAR_DATA        : '[PAYMENT] CLEAR_DATA',
    GET_PAYMENT_ONLINE: '[PAYMENT] GET_PAYMENT_ONLINE',

    TYPE_GET_PAYMENT: 'get_payment',

    GET_LIST_PAYMENT       : '[PAYMENT] GET_LIST_PAYMENT',
    GET_LIST_PAYMENT_RESULT: '[PAYMENT] GET_LIST_PAYMENT_RESULT',

    SELECT_PAYMENT                                 : '[PAYMENT] SELECT_PAYMENT',
    CANCEL_PAYMENT                                 : '[PAYMENT] CANCEL_PAYMENT',
    BACK_TO_ADD_PAYMENT                            : '[PAYMENT] BACK_TO_ADD_PAYMENT',
    BACK_PAYMENT                                   : '[PAYMENT] BACK_PAYMENT',
    SWITCH_PAGE                                    : '[PAYMENT] SWITCH_PAGE',
    RESET_STATE                                    : '[PAYMENT] RESET_STATE',
    ADD_PAYMENT                                    : '[PAYMENT] ADD_PAYMENT',
    UPDATE_PAYMENT_LIST                            : '[PAYMENT] UPDATE_PAYMENT_LIST',

    PAYMENT_PAGE_SELECT_PAYMENT: '[PAYMENT] PAYMENT_PAGE_SELECT_PAYMENT',
    PAYMENT_PAGE_EDIT_PAYMENT  : '[PAYMENT] PAYMENT_PAGE_EDIT_PAYMENT',
    PAYMENT_PAGE_COMPLETE_ORDER: '[PAYMENT] PAYMENT_PAGE_COMPLETE_ORDER',

    PAYMENT_TYPE_OFFLINE        : '0', // origin offline
    PAYMENT_TYPE_CREDITCARD_FORM: '1', // credit card form
    PAYMENT_TYPE_REDIRECT       : '2', // redirect
    PAYMENT_TYPE_TERMINAL       : '3', // terminal
    PAYMENT_TYPE_EWALLET        : '4', // eWallet

    PAYMENT_IS_REFERENCE_NUMBER: 1,
    PAYMENT_CAN_DUE            : 1,

    CASH                             : 'cashforpos',
    CREDIT_CARD                      : 'ccforpos',
    PROCESS_PAYMENT_NEW              : 'PROCESS_PAYMENT_NEW',
    PROCESS_PAYMENT_PENDING          : 'PROCESS_PAYMENT_PENDING',
    PROCESS_PAYMENT_PROCESSING       : 'PROCESS_PAYMENT_PROCESSING',
    PROCESS_PAYMENT_SUCCESS          : 'PROCESS_PAYMENT_SUCCESS',
    PROCESS_PAYMENT_ERROR            : 'PROCESS_PAYMENT_ERROR',
    PROCESSED_PAYMENT                : 'PROCESSED_PAYMENT',

    PAYMENT_STATUS_TO_PROCESSING: '[PAYMENT] PAYMENT_STATUS_TO_PROCESSING',
    PAYMENTS_STATUS_TO_PENDING  : '[PAYMENT] PAYMENT_STATUS_TO_PENDING',
    START_PROCESS_SINGLE_PAYMENT: '[PAYMENT] START_PROCESS_SINGLE_PAYMENT',

    TYPE_CHECKOUT: 0,
    TYPE_REFUND  : 1,

    LOST_INTERNET_STATUS            : 'LOST_INTERNET',
    TIMEOUT_STATUS                  : 'TIMEOUT',
    LOST_INTERNET_CONNECTION_MESSAGE: 'Connection failed. ' +
        'You must connect to a Wi-Fi or cellular data network to use this payment method',
    TIME_OUT_EXCEPTION_MESSAGE      : 'The order has timed out. Please start the process again.',
    UNKNOWN_EXCEPTION_MESSAGE       : 'Connection failed. Please contact admin to check the configuration of API.'
}
