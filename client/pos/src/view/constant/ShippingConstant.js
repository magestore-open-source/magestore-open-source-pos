/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    GET_SHIPPING_ONLINE: '[SHIPPING] GET_SHIPPING_ONLINE',

    TYPE_GET_SHIPPING: 'get_shipping',

    GET_LIST_SHIPPING: '[SHIPPING] GET_LIST_SHIPPING',
    GET_LIST_SHIPPING_RESULT: '[SHIPPING] GET_LIST_SHIPPING_RESULT',
    POPUP_TYPE_SHIPPING: '[SHIPPING] POPUP',
    POPUP_TYPE_SHIPPING_ADDRESS: '[SHIPPING] POPUP_ADDRESS',
    GET_SHIPPING_METHODS: '[SHIPPING] GET_SHIPPING_METHODS',
    SAVE_SHIPPING: "[SHIPPING] SAVE_SHIPPING",
    SAVE_SHIPPING_AFTER: "[SHIPPING] SAVE_SHIPPING_AFTER",
    STORE_PICKUP_SHIPPING_METHOD_CODE: "webpos_shipping_storepickup",
    STORE_PICKUP_SHIPPING_METHOD_DESCRIPTION: "Pickup-at-store",
}
