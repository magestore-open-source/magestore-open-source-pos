/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    GET_CONFIG: '[CONFIG] GET_CONFIG',
    GET_CONFIG_RESULT: '[CONFIG] GET_CONFIG_RESULT',
    GET_CONFIG_ERROR: '[CONFIG] GET_CONFIG_ERROR',

    TYPE_GET_CONFIG: 'get_config',

    /**
     * custom prefix order
     */
    CONFIG_XML_PATH_USE_CUSTOM_PREFIX: 'webpos/checkout/use_custom_prefix',
    CONFIG_XML_PATH_CUSTOM_PREFIX: 'webpos/checkout/custom_prefix',
    CONFIG_XML_PATH_GENERAL_LOCALE_CODE: 'general/locale/code',
    CONFIG_XML_PATH_MAGENTO_VERSION: 'magento_version'
}
