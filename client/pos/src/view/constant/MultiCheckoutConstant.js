/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    GET_LIST_CART: '[MULTI_CHECKOUT] GET_LIST_CART',
    GOT_LIST_CART: '[MULTI_CHECKOUT] GOT_LIST_CART',
    ADD_CART: '[MULTI_CHECKOUT] ADD_CART',
    ADDED_CART: '[MULTI_CHECKOUT] ADDED_CART',
    SELECT_CART: '[MULTI_CHECKOUT] SELECT_CART',
    SELECTED_CART: '[MULTI_CHECKOUT] SELECTED_CART',
    DELETE_CART: '[MULTI_CHECKOUT] DELETE_CART',
    DELETED_CART: '[MULTI_CHECKOUT] DELETED_CART',
    UPDATE_CART: '[MULTI_CHECKOUT] UPDATE_CART',
    UPDATED_CART: '[MULTI_CHECKOUT] UPDATED_CART',
}
