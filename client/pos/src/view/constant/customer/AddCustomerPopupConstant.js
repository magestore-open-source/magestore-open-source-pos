/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    POPUP_TYPE_CUSTOMER_LIST: "customer_list",
    POPUP_TYPE_CUSTOMER: "customer",
    POPUP_TYPE_ADDRESS: "address",
    TYPE_FIELD_INPUT: 'input',
    TYPE_FIELD_GROUP: 'group',
    TYPE_FIELD_CHECKBOX: 'checkbox',
    TYPE_FIELD_STATE: 'state',
    TYPE_FIELD_DATE: 'date',

    FRONTEND_INPUT_TYPE_SELECT: 'select',
    FRONTEND_INPUT_TYPE_TEXT: 'text',
    FRONTEND_INPUT_TYPE_MULTILINE: 'multiline',
    FRONTEND_INPUT_TYPE_HIDDEN: 'hidden',
    FRONTEND_INPUT_TYPE_BOOLEAN: 'boolean',
    FRONTEND_INPUT_TYPE_DATE: 'date',

    VALIDATION_RULE_MAX_LENGTH: 'max_text_length',
    VALIDATION_RULE_MIN_LENGTH: 'min_text_length',

    ATTRIBUTE_CODE_POSTCODE: 'postcode',
    ATTRIBUTE_CODE_STREET: 'street',
    ATTRIBUTE_CODE_VAT: 'vat_id',
    ATTRIBUTE_CODE_COUNTRY: 'country_id',
    ATTRIBUTE_CODE_REGION: 'region',
    ATTRIBUTE_CODE_REGION_ID: 'region_id',
    ATTRIBUTE_CODE_EMAIL: 'email',
    ATTRIBUTE_CODE_GROUP_ID: 'group_id',
}