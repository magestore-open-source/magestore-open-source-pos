/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    STATE_NEW: 'new',
    STATE_PROCESSING: 'processing',
    STATE_COMPLETE: 'complete',
    STATE_CLOSED: 'closed',
    STATE_CANCELED: 'canceled',
    STATE_HOLDED: 'holded',
    STATE_PAYMENT_REVIEW: 'payment_review',

    STATUS_NOT_SYNC: 'not_sync',
    STATUS_CANCELED: 'canceled',
    STATUS_CLOSED: 'closed',
    STATUS_COMPLETE: 'complete',
    STATUS_FRAUD: 'fraud',
    STATUS_HOLDED: 'holded',
    STATUS_PAYMENT_REVIEW: 'payment_review',
    STATUS_PENDING: 'pending',
    STATUS_PENDING_PAYMENT: 'pending_payment',
    STATUS_PROCESSING: 'processing',

    /**
     * Payment status
     */
    PAYMENT_STATUS_PAID: 'Paid',
    PAYMENT_STATUS_UNPAID: 'Unpaid',
    PAYMENT_STATUS_PARTIAL_PAID: 'Partially Paid',
    PAYMENT_STATUS_VOID: 'Void',
}
