/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    /**
     * Item status
     */
    STATUS_PENDING : 1,

    // No items shipped, invoiced, canceled, refunded nor backordered
    STATUS_SHIPPED : 2,

    // When qty ordered - [qty canceled + qty returned] = qty shipped
    STATUS_INVOICED : 9,

    // When qty ordered - [qty canceled + qty returned] = qty invoiced
    STATUS_BACKORDERED : 3,

    // When qty ordered - [qty canceled + qty returned] = qty backordered
    STATUS_CANCELED : 5,

    // When qty ordered = qty canceled
    STATUS_PARTIAL : 6,

    // If [qty shipped or(max of two) qty invoiced + qty canceled + qty returned]
    // < qty ordered
    STATUS_MIXED : 7,

    // All other combinations
    STATUS_REFUNDED : 8,

    // When qty ordered = qty refunded
    STATUS_RETURNED : 4
}
