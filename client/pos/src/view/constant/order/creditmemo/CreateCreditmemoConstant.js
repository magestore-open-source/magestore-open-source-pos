/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    STEP_ITEM: 1,
    STEP_ADJUSTMENT: 2,
    STEP_PAYMENT: 3,
    /* STEP_CONFIRMATION: 4,*/
    STEP_SUCCESS: 4,

    PAYMENT_CONFIRM_STEP_NONE: 0,
    PAYMENT_CONFIRM_STEP_REMAINING: 1,
    PAYMENT_CONFIRM_STEP_CONFIRMATION: 2,
    PAYMENT_CONFIRM_STEP_PROCESS_PAYMENT: 3,

    ADJUSTMENT_SHIPPING_KEY: 'adjustment_shipping_value',
    ADJUSTMENT_POSITIVE_KEY: 'adjustment_positive_value',
    ADJUSTMENT_NEGATIVE_KEY: 'adjustment_negative_value'
}
