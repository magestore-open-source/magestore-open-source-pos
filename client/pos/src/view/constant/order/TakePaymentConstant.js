/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    SELECT_PAYMENT: '[TAKE PAYMENT] SELECT_PAYMENT',
    CANCEL_PAYMENT: '[TAKE PAYMENT] CANCEL_PAYMENT',
    BACK_TO_ADD_PAYMENT: '[TAKE PAYMENT] BACK_TO_ADD_PAYMENT',
    BACK_PAYMENT: '[TAKE PAYMENT] BACK_PAYMENT',
    SWITCH_PAGE: '[TAKE PAYMENT] SWITCH_PAGE',
    RESET_STATE: '[TAKE PAYMENT] RESET_STATE',
    ADD_PAYMENT: '[TAKE PAYMENT] ADD_PAYMENT',

    PAYMENT_PAGE_SELECT_PAYMENT: '[TAKE PAYMENT] PAYMENT_PAGE_SELECT_PAYMENT',
    PAYMENT_PAGE_EDIT_PAYMENT: '[TAKE PAYMENT] PAYMENT_PAGE_EDIT_PAYMENT',
    PAYMENT_PAGE_COMPLETE_PAYMENT: '[TAKE PAYMENT] PAYMENT_PAGE_COMPLETE_PAYMENT',

    PAYMENT_TYPE_OFFLINE: '0',
    PAYMENT_TYPE_CREDITCARD_FORM: '1',
    PAYMENT_TYPE_REDIRECT: '2',

    PAYMENT_IS_REFERENCE_NUMBER: 1,
    PAYMENT_CAN_DUE: 1,
}