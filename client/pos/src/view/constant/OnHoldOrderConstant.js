/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    HOLD_ORDER: '[ON HOLD ORDER] HOLD_ORDER',
    HOLD_ORDER_RESULT: '[ON HOLD ORDER] HOLD_ORDER_RESULT',
    HOLD_ORDER_AFTER: '[ON HOLD ORDER] HOLD_ORDER_AFTER',

    DELETE_ON_HOLD_ORDER: '[ON HOLD ORDER] DELETE_ON_HOLD_ORDER',

    CANCEL_ON_HOLD_ORDER: '[ON HOLD ORDER] CANCEL_ON_HOLD_ORDER',
    CANCEL_ON_HOLD_ORDER_AFTER: '[ON HOLD ORDER] CANCEL_ON_HOLD_ORDER_AFTER',

    SEARCH_ORDER: '[ON HOLD ORDER] SEARCH_ORDER',
    SEARCH_ORDER_RESULT: '[ON HOLD ORDER] SEARCH_ORDER_RESULT',
    SEARCH_ORDER_ERROR: '[ON HOLD ORDER] SEARCH_ORDER_ERROR',

    SYNC_ACTION_UPDATE_ON_HOLD_ORDER_FINISH: '[ON HOLD ORDER] SYNC_ACTION_UPDATE_ON_HOLD_ORDER_FINISH',
    SYNC_DELETED_HOLD_ORDER_FINISH: '[ON HOLD ORDER] SYNC_DELETED_HOLD_ORDER_FINISH',

    PAGE_SIZE: 10
}
