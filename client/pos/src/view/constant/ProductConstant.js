/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    SEARCH_PRODUCT: '[PRODUCT] SEARCH_PRODUCT',
    SEARCH_PRODUCT_RESULT: '[PRODUCT] SEARCH_PRODUCT_RESULT',
    RESET_PRODUCT_LIST: '[PRODUCT] RESET_PRODUCT_LIST',
    GET_LIST_PRODUCT: '[PRODUCT] GET_LIST_PRODUCT',
    GET_LIST_PRODUCT_RESULT: '[PRODUCT] GET_LIST_PRODUCT_RESULT',
    VIEW_PRODUCT: '[PRODUCT] VIEW_PRODUCT',
    SEARCH_BY_BARCODE: '[PRODUCT] SEARCH_BY_BARCODE',
    SYNC_ACTION_UPDATE_PRODUCT_DATA_FINISH: '[PRODUCT] SYNC_ACTION_UPDATE_PRODUCT_DATA_FINISH',
    SYNC_ACTION_DELETE_PRODUCT_DATA_FINISH: '[PRODUCT] SYNC_ACTION_DELETE_PRODUCT_DATA_FINISH',
    SYNC_ACTION_UPDATE_STOCK_DATA_FINISH: '[PRODUCT] SYNC_ACTION_UPDATE_STOCK_DATA_FINISH',
    SYNC_ACTION_UPDATE_CATALOG_RULE_PRICE_DATA_FINISH: '[PRODUCT] SYNC_ACTION_UPDATE_CATALOG_RULE_PRICE_DATA_FINISH',

    SYNC_ACTION_DELETED_CATALOG_RULE_PRICE_DATA_FINISH: '[PRODUCT] SYNC_ACTION_DELETED_CATALOG_RULE_PRICE_DATA_FINISH',

    CALCULATE_CHILD: 0,
    CALCULATE_PARENT: 1,
    SHIPMENT_SEPARATELY: 1,
    SHIPMENT_TOGETHER: 0,
}
