/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    /**
     * Enabled config path
     */
    XML_PATH_FPT_ENABLED: 'tax/weee/enable',

    /*display settings*/
    XML_PATH_FPT_DISPLAY_PRODUCT_VIEW: 'tax/weee/display',

    XML_PATH_FPT_DISPLAY_PRODUCT_LIST: 'tax/weee/display_list',

    XML_PATH_FPT_DISPLAY_SALES: 'tax/weee/display_sales',

    XML_PATH_FPT_DISPLAY_EMAIL: 'tax/weee/display_email',

    XML_PATH_FPT_INCLUDE_IN_SUBTOTAL: 'tax/weee/include_in_subtotal',

    XML_PATH_FPT_TAXABLE: 'tax/weee/apply_vat',

    XML_PATH_POS_FPT_DISPLAY_PRODUCT_PRICE: 'webpos/tax_configuration/fpt/product_price',

    XML_PATH_POS_FPT_INCLUDE_IN_SUBTOTAL: 'webpos/tax_configuration/fpt/include_in_subtotal',
}