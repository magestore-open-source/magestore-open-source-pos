/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    SET_DEFAULT_SYNC_DB : '[SYNC] SET_DEFAULT_SYNC_DB',
    SET_DEFAULT_SYNC_DB_SUCCESS : '[SYNC] SET_DEFAULT_SYNC_DB_SUCCESS',
    SYNC_DATA: '[SYNC] SYNC_DATA',
    SYNC_DATA_WITH_TYPE: '[SYNC] SYNC_DATA_WITH_TYPE',
    CHECK_SYNC_DATA_FINISH: '[SYNC] CHECK_SYNC_DATA_FINISH',
    CHECK_SYNC_DATA_FINISH_RESULT: '[SYNC] CHECK_SYNC_DATA_FINISH_RESULT',
    DEFAULT_TOTAL: -1,

    CHANGE_MODE: '[SYNC] CHANGE_MODE',
    CHANGE_DATA_TYPE_MODE: '[SYNC] CHANGE_DATA_TYPE_MODE',
    SET_DATA_TYPE_MODE: '[SYNC] SET_DATA_TYPE_MODE',

    SYNC_ACTION_LOG: '[action_log] SYNC_ACTION_LOG',
    SYNC_ACTION_LOG_SUCCESS: '[action_log] SYNC_ACTION_LOG_SUCCESS',
    SYNCING_ACTION_LOG: 'SYNCing',
    NOT_SYNCING_ACTION_LOG: 'not_SYNCing',
    STATUS_PENDING: 'pending',
    STATUS_REQUESTING: 'requesting',
    STATUS_ERROR: 'error',

    UPDATE_DATA: '[SYNC] UPDATE_DATA',
    EXECUTE_UPDATE_DATA: '[SYNC] EXECUTE_UPDATE_DATA',
    UPDATE_DATA_WITH_TYPE: '[SYNC] UPDATE_DATA_WITH_TYPE',
    UPDATE_DATA_FINISH: '[SYNC] UPDATE_DATA_FINISH',
    DELETE_DATA_FINISH: '[SYNC] DELETE_DATA_FINISH',
    UPDATE_DATA_FINISH_RESULT: '[SYNC] UPDATE_DATA_FINISH_RESULT',
    ONLINE_MODE: 'online',
    OFFLINE_MODE: 'offline',
    TYPE_USER: 'user',
    TYPE_PRODUCT: 'product',
    TYPE_CUSTOMER: 'customer',
    TYPE_STOCK: 'stock',
    TYPE_ORDER: 'order',
    TYPE_CATALOG_RULE_PRODUCT_PRICE: 'catalog_rule_product_price',
    TYPE_CATEGORY: 'category',

    TYPE_REQUEST_GET: 'request_get',

    REQUEST_PLACE_ORDER: 'request_place_order',
    REQUEST_CREATE_CUSTOMER: 'request_create_customer',
    REQUEST_EDIT_CUSTOMER: 'request_edit_customer',
    REQUEST_TAKE_PAYMENT_ORDER: 'request_take_payment_order',
    REQUEST_CREATE_CREDITMEMO_ORDER: 'request_create_creditmemo_order',
    REQUEST_HOLD_ORDER: 'request_hold_order',
    REQUEST_DELETE_ORDER: 'request_delete_order',
    REQUEST_SEND_EMAIL_ORDER: 'request_send_email_order',
    REQUEST_ADD_COMMENT_ORDER: 'request_add_comment_order',
    REQUEST_CANCEL_ORDER: 'request_cancel_order',
    REQUEST_SEND_EMAIL_CREDITMEMO_ORDER: 'request_send_email_creditmemo_order',
    REQUEST_CREDITMEMO_CREATE_CUSTOMER: 'request_creditmemo_create_customer',

    METHOD_POST: "POST",
    METHOD_PUT: "PUT",
    METHOD_DELETE: "DELETE",
    METHOD_GET: "GET",
    MAX_COUNT_ERROR_REQUEST: 20,

    NUMBER_OF_ORDERS_FOR_RELOADING_PAGE: 50,
    RELOAD_PAGE: '[SYNC] RELOAD PAGE',
    CAN_NOT_RELOAD: 0,
    CAN_RELOAD: 1,
    NEED_TO_RELOAD_BUT_USER_IS_DOING_SOMETHING: 2
}
