/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    SEARCH_CUSTOMER: '[CUSTOMER] SEARCH_CUSTOMER',
    SEARCH_CUSTOMER_RESULT: '[CUSTOMER] SEARCH_CUSTOMER_RESULT',
    RESET_CUSTOMER_LIST: '[CUSTOMER] RESET_CUSTOMER_LIST',
    CREATE_CUSTOMER: '[CUSTOMER] CREATE_CUSTOMER',
    CREATE_CUSTOMER_SUCCESS: '[CUSTOMER] CREATE_CUSTOMER_SUCCESS',
    CREATE_CUSTOMER_ERROR: '[CUSTOMER] CREATE_CUSTOMER_ERROR',
    EDIT_CUSTOMER: '[CUSTOMER] EDIT_CUSTOMER',
    EDIT_CUSTOMER_ERROR: '[CUSTOMER] EDIT_CUSTOMER_ERROR',
    SAVE_CUSTOMER: '[CUSTOMER] SAVE_CUSTOMER',
    RESET_IS_SHIPPING: '[CUSTOMER] RESET_IS_SHIPPING',
    GOOGLE_API_PATH: "webpos/general/google_api_key",

    SYNC_ACTION_UPDATE_DATA_FINISH: '[CUSTOMER] SYNC_ACTION_UPDATE_DATA_FINISH',
    SYNC_DELETED_CUSTOMER_FINISH: '[CUSTOMER] SYNC_DELETED_CUSTOMER_FINISH',

    TMP_CUSTOMER_ID_PREFIX: "pos_",

    NEW_CUSTOMER_DATA: {
        id: null,
        addresses: []
    }
}
