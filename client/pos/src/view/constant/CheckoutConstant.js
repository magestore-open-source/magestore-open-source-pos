/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    CHECK_OUT_PLACE_ORDER: '[CHECKOUT] CHECK_OUT_PLACE_ORDER',
    CHECK_OUT_PLACE_ORDER_RESULT: '[CHECKOUT] CHECK_OUT_PLACE_ORDER_RESULT',
    CHECK_OUT_PLACE_ORDER_ERROR: '[CHECKOUT] CHECK_OUT_PLACE_ORDER_ERROR',
    CHECKOUT_TO_SELECT_PAYMENTS: '[QUOTE] CHECKOUT_TO_SELECT_PAYMENTS',
    CHECKOUT_TO_CATALOG: '[QUOTE] CHECKOUT_TO_CATALOG',
    CHECKOUT_SWITCH_PAGE: '[CHECKOUT] CHECKOUT_SWITCH_PAGE',
    CHECKOUT_INIT_PAYMENTS: '[QUOTE] INIT_PAYMENTS',
    CHECKOUT_PROCESS_PAYMENT: '[CHECKOUT] CHECKOUT_PROCESS_PAYMENT',
    CHECKOUT_PROCESS_SINGLE_PAYMENT_RESULT: '[CHECKOUT] CHECKOUT_PROCESS_SINGLE_PAYMENT_RESULT',
    CHECKOUT_PROCESS_SINGLE_PAYMENT_ERROR: '[CHECKOUT] CHECKOUT_PROCESS_SINGLE_PAYMENT_ERROR',
}
