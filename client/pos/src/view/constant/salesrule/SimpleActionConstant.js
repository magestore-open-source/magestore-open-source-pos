/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
export default {
    TO_PERCENT_ACTION: 'to_percent',
    BY_PERCENT_ACTION: 'by_percent',
    TO_FIXED_ACTION: 'to_fixed',
    BY_FIXED_ACTION: 'by_fixed',
    CART_FIXED_ACTION: 'cart_fixed',
    BUY_X_GET_Y_ACTION: 'buy_x_get_y',
}
