/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Config from '../config/Config'
import ConfigHelper from './ConfigHelper'
import {fire} from "../event-bus";

export default {
    /**
     * check is show shipping method
     * @returns {*|string|Array}
     */
    isShowShippingMethod() {
        return Config.config && Config.config.shipping && Config.config.shipping.shipping_methods;
    },

    /**
     * Check is show delivery date
     *
     * @return {boolean}
     */
    isShowDeliveryDate() {
        return Config.config && Config.config.shipping && Config.config.shipping.delivery_date;
    },

    /**
     * Check is show delivery date
     *
     * @return {boolean}
     */
    isAllowToAddOutOfStockProduct() {
        let isAllowToAddOutOfStockProduct = !!+ConfigHelper.getConfig(
            'webpos/checkout/add_out_of_stock_product'
        );
        const eventData = {
            isAllowToAddOutOfStockProduct: isAllowToAddOutOfStockProduct
        };
        fire('helper_checkout_is_allow_add_out_of_stock_product_after', eventData);

        return eventData.isAllowToAddOutOfStockProduct;
    },

    /**
     * Check need confirm before deleting cart
     *
     * @return {boolean}
     */
    needConfirmDeleteCart() {
        return !!+ConfigHelper.getConfig('webpos/checkout/need_confirm');
    },
}