/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import ConfigHelper from "./ConfigHelper";
import ConfigConstant from "../view/constant/weee/ConfigConstant";

export default {
    /**
     * Check if weee tax amount should be taxable
     *
     * @return {boolean}
     */
    isTaxable() {
        return !!+ConfigHelper.getConfig(ConfigConstant.XML_PATH_FPT_TAXABLE);
    },

    /**
     * Check if weee tax amount should be included to subtotal
     *
     * @return {boolean}
     */
    includeInSubtotal() {
        return !!+ConfigHelper.getConfig(ConfigConstant.XML_PATH_FPT_INCLUDE_IN_SUBTOTAL);
    },

    /**
     * Check if fixed taxes are used in system
     *
     * @return {*|null}
     */
    isFixedTaxEnabled() {
        return !!+ConfigHelper.getConfig(ConfigConstant.XML_PATH_FPT_ENABLED);
    },

    /**
     * Check if weee tax amount should be included to subtotal
     *
     * @return {boolean}
     */
    priceDisplayTypeIncludeFPT() {
        return !!+ConfigHelper.getConfig(ConfigConstant.XML_PATH_POS_FPT_DISPLAY_PRODUCT_PRICE);
    },

    /**
     * Check if weee tax amount should be included to subtotal
     *
     * @return {boolean}
     */
    includeInSubtotalInPOS() {
        return !!+ConfigHelper.getConfig(ConfigConstant.XML_PATH_POS_FPT_INCLUDE_IN_SUBTOTAL);
    },
}