/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Config from '../config/Config'
import ConfigConstant from "../view/constant/ConfigConstant";

export default {
    // eslint-disable-next-line
    regexEmail: /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
    
    /**
     * Get config by path
     *
     * @param path
     * @returns {null}
     */
    getConfig(path) {
        let config = Config.config.settings.find(item => item.path === path);
        return config ? config.value : null;
    },

    /**
     * Sort array object by array condition fields
     *
     * @param arrayObject
     * @param arraySortFiels
     * @param dirrection
     * @return {*}
     */
    sortArrayObjectsByArrayFields(arrayObject, arraySortFiels, dirrection = 'ASC') {
        return arrayObject.sort((a, b) => this.sortByArrayFields(a, b, arraySortFiels, dirrection));
    },

    /**
     * Sort array object by array condition fields
     *
     * @param a
     * @param b
     * @param arraySortFiels
     * @param index
     * @param direction
     * @return {number}
     */
    sortByArrayFields(a, b, arraySortFiels, direction = 'ASC', index = 0) {
        if (a[arraySortFiels[index]] === b[arraySortFiels[index]]) {
            return this.sortByArrayFields(a, b, arraySortFiels, direction, index + 1);
        } else {
            if (a[arraySortFiels[index]] > b[arraySortFiels[index]]) {
                return direction === "ASC" ? 1 : -1;
            }
            return direction === "ASC" ? -1 : 1;
        }
    },

    isShowReasonOnReceipt() {
        let isEnable = this.getConfig('webpos/custom_receipt/display_reason');
        return !!(isEnable && isEnable === '1');
    },

    /**
     * get locale code
     * @returns {*|null}
     */
    getLocaleCode() {
        let localeCode = this.getConfig(ConfigConstant.CONFIG_XML_PATH_GENERAL_LOCALE_CODE);
        if (!localeCode)
            localeCode = "en_US";
        return localeCode
    },
    /**
     *  get version of magento
     * @returns {*|null}
     */
    getMagentoVersion() {
        return Config.config.magento_version;
    },
    /**
     *
     * @param version
     * @param operator
     * @returns {boolean}
     */
    compareMagentoVersion(version, operator) {
        operator = operator || '===';

        if (!version) {
            return false;
        }

        let magentoVersion = this.getMagentoVersion();

        if (!magentoVersion) {
            return false
        }

        let magentoVersionPath = magentoVersion.split('.');
        if (magentoVersionPath.length < 3) {
            return false;
        }
        let result = String(version).split('.').map((number, index) => {
            if (operator === '===') {
                return Number(magentoVersionPath[index]) === number;
            }
            if (operator === '>=') {
                return Number(magentoVersionPath[index]) >= number;
            }
            if (operator === '>') {
                return Number(magentoVersionPath[index]) > number;
            }
            if (operator === '<=') {
                return Number(magentoVersionPath[index]) <= number;
            }
            return Number(magentoVersionPath[index]) < number;
        });

        return result.reduce((current, next) => current && next, true);
    }
}
