/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import Config from "../config/Config";

export default {
    /**
     * Get current location name
     * @return {string}
     */
    getId() {
        return Config.location_id;
    },
    /**
     * Get current location name
     * @return {string}
     */
    getName() {
        return Config.location_name;
    },
    /**
     * Get current location address
     * @return {object}
     */
    getAddress() {
        return Config.location_address;
    },
    /**
     * Get current location street
     * @return {string}
     */
    getStreet() {
        return this.getAddress().street;
    },
    /**
     * Get current location city
     * @return {string}
     */
    getCity() {
        return this.getAddress().city;
    },
    /**
     * Get current location country id
     * @return {string}
     */
    getCountryId() {
        return this.getAddress().country_id;
    },
    /**
     * Get current location region
     * @return {string}
     */
    getRegion() {
        return this.getAddress().region;
    },
    /**
     * Get current location region id
     * @return {number}
     */
    getRegionId() {
        return this.getAddress().region_id;
    },
    /**
     * Get current location postcode
     * @return {string}
     */
    getPostcode() {
        return this.getAddress().postcode;
    },

    /**
     * Get current location telephone
     * @return {string}
     */
    getTelephone() {
        return this.getAddress().telephone;
    },

    /**
     * Get current location fax
     * @return {string}
     */
    getFax() {
        return this.getAddress().fax;
    },

    /**
     * Check current location is primary location
     *
     * @return {*}
     */
    isPrimaryLocation() {
        return Config.config.is_primary_location;
    }
}