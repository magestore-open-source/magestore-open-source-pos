/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import moment from "moment/moment";
import Config from "../config/Config";

export default {
    /**
     * Month name string array
     *
     * @type {string[]}
     */
    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],

    /**
     * Full month name string array
     * @type {string[]}
     */
    fullMonthName: [
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
        "November", "December"
    ],

    /**
     * Get database format full year
     *
     * @param {Date} date
     * @returns {string}
     */
    getFullYear(date) {
        return this.getStringDateTime(date.getFullYear());
    },

    /**
     * Get database format month
     *
     * @param {Date} date
     * @returns {string}
     */
    getMonth(date) {
        return this.getStringDateTime(date.getMonth() + 1);
    },

    /**
     * Get database format date
     *
     * @param {Date} date
     * @returns {string}
     */
    getDate(date) {
        return this.getStringDateTime(date.getDate());
    },

    /**
     * Get database format hour
     *
     * @param {Date} date
     * @returns {string}
     */
    getHours(date) {
        return this.getStringDateTime(date.getHours());
    },

    /**
     * Get database format minus
     *
     * @param {Date} date
     * @returns {string}
     */
    getMinutes(date) {
        return this.getStringDateTime(date.getMinutes());
    },

    /**
     * Get database format seconds
     *
     * @param {Date} date
     * @returns {string}
     */
    getSeconds(date) {
        return this.getStringDateTime(date.getSeconds());
    },

    /**
     * Get database format date or time
     *
     * @param {number} number
     * @returns {string}
     */
    getStringDateTime(number) {
        if (number < 10) {
            return "0" + number;
        }
        return String(number);
    },

    /**
     * Get database format date time from current timestamp
     *
     * @param currentTimestamp
     * @returns {string}
     */
    getDatabaseDateTime(currentTimestamp) {
        let dateTime = new Date(new Date().getTime() + this.getTimezoneOffsetTimeStamp());
        if (currentTimestamp) {
            dateTime = new Date(currentTimestamp + this.getTimezoneOffsetTimeStamp());
        }
        return this.getFullYear(dateTime) + "-" +
            this.getMonth(dateTime) + "-" +
            this.getDate(dateTime) + " " +
            this.getHours(dateTime) + ":" +
            this.getMinutes(dateTime) + ":" +
            this.getSeconds(dateTime);
    },



    getCurrentDateTime() {
        return moment().format('lll');
    },

    getDayOfWeekDateTime() {
        return moment().format('llll');
    },




    /**
     * Get time zone offset to timestamp
     *
     * @returns {number}
     */
    getTimezoneOffsetTimeStamp() {
        return new Date().getTimezoneOffset() * 60 * 1000;
    },

    /**
     * Get time zone offset to timestamp
     *
     * @returns {number}
     */
    getServerTimezoneOffsetTimeStamp() {
        // Cách tính timezone offset của server và client khác nhau nên cần đảo dấu lại cho đồng bộ với bên client
        return Config.config && Config.config.server_timezone_offset ? -Config.config.server_timezone_offset * 1000 : 0;
    },

    /**
     * Get database format date time with server timezone from current timestamp
     *
     * @param currentTimestamp
     * @returns {string}
     */
    getServerDateTime(currentTimestamp) {
        let timeStamp = new Date().getTime();
        if (currentTimestamp) {
            timeStamp = currentTimestamp;
        }
        let dateTime = new Date(
            timeStamp + this.getTimezoneOffsetTimeStamp() - this.getServerTimezoneOffsetTimeStamp()
        );

        return this.getFullYear(dateTime) + "-" +
            this.getMonth(dateTime) + "-" +
            this.getDate(dateTime) + " " +
            this.getHours(dateTime) + ":" +
            this.getMinutes(dateTime) + ":" +
            this.getSeconds(dateTime);
    },

    /**
     *
     *
     * @param databaseDateTime
     * @returns {Date}
     */
    convertDatabaseDateTimeToLocalDate(databaseDateTime) {
        let timeMSecond = moment(databaseDateTime).unix() * 1000;
        return new Date(timeMSecond - this.getTimezoneOffsetTimeStamp());
    },

    /**
     * Get date time string to receipt
     * @param databaseDateTime {string}
     * @param getTime {boolean}
     * @returns {string}
     */
    getReceiptDateTime(databaseDateTime, getTime = true) {
        let dateTime = this.convertDatabaseDateTimeToLocalDate(databaseDateTime);
        let time = this.getDate(dateTime) + "/" +
            this.getMonth(dateTime) + "/" +
            this.getFullYear(dateTime);
        if (getTime) {
            time += " " + this.getHours(dateTime) + ":" +
                this.getMinutes(dateTime);
        }
        return time;
    },

    /**
     * get date object from timestamp
     *
     * @param timestamp
     * @returns {Date}
     */
    getDateObjectFromTimestamp(timestamp) {
        return new Date(timestamp);
    },

    /**
     * get time from timestamp
     *
     * @param timestamp
     * @returns {string}
     */
    getTimeFromTimestamp(timestamp) {
        let dateTime = this.getDateObjectFromTimestamp(timestamp);
        let hour = dateTime.getHours();
        let min = dateTime.getMinutes();
        hour = hour > 9 ? hour : `0${hour}`;
        min = min > 9 ? min : `0${min}`;
        return `${hour}:${min}`;
    },

    /**
     * Check current date in date range
     *
     * @param dateFrom
     * @param dateTo
     * @return {boolean}
     */
    isCurrentDateInInterval(dateFrom = null, dateTo = null) {
        let currentTime = new Date();
        let timezoneOffset = currentTime.getTimezoneOffset();
        let currentTimeStamp = new Date(currentTime.getTime() + timezoneOffset * 60 * 1000).getTime();
        let fromTimeStamp = new Date(dateFrom).getTime();
        let toTimeStamp = new Date(dateTo).getTime();
        let toNextDateTimeStamp = new Date(toTimeStamp + 86400 * 1000).getTime();
        if (dateFrom && currentTimeStamp < fromTimeStamp) {
            return false;
        }
        if (dateTo && currentTimeStamp > toNextDateTimeStamp) {
            return false;
        }
        return true;
    }
}
