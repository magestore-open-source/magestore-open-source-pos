/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import PaymentConstant from "../view/constant/PaymentConstant";
import CustomPrefixHelper from "./CustomPrefixHelper";
import Config from "../config/Config";


const FLAT_PAYMENTS = [
  PaymentConstant.CASH,
  PaymentConstant.CREDIT_CARD
];

const PROCESSED_STATUS = [
  PaymentConstant.PROCESSED_PAYMENT,
  PaymentConstant.PROCESS_PAYMENT_SUCCESS,
  PaymentConstant.PROCESS_PAYMENT_ERROR
];

const CREDITCARD_FORM_PAYMENTS = [];

const TERMINAL_PAYMENTS = [];

const INTERNET_TERMINAL_PAYMENTS = [];

const EWALLET_PAYMENTS = [];

const PREVENT_CANCEL_ORDER_RULE_PAYMENTS = [];

export default {
    PROCESSED_STATUS: PROCESSED_STATUS,
    EWALLET_PAYMENTS: EWALLET_PAYMENTS,
    /**
     *
     * @param method
     * @return {boolean}
     */
    isFlatPayment(method) {
        return FLAT_PAYMENTS.indexOf(method) !== -1
    },

    /**
     *
     * @param object
     * @return {boolean}
     */
    hasUsingGatewayPayment(object) {
        return object.payments.find(payment => {
            return this.hasUsingCreditCardForm(payment.method) && !payment.type
        });
    },
    /**
     *
     * @param method
     * @return {boolean}
     */
    hasUsingCreditCardForm(method) {
        return CREDITCARD_FORM_PAYMENTS.indexOf(method) !== -1;
    },

    /**
     *
     * @param method
     * @return {boolean}
     */
    hasUsingTerminal(method) {
        return TERMINAL_PAYMENTS.indexOf(method) !== -1;
    },

    /**
     *
     * @param method
     * @return {boolean}
     */
    hasUsingEWallet(method) {
        return EWALLET_PAYMENTS.indexOf(method) !== -1;
    },

    /**
     *
     * @param method
     * @returns {boolean}
     */
    hasPreventCancelOrderRule(method) {
        return PREVENT_CANCEL_ORDER_RULE_PAYMENTS.indexOf(method) !== -1;
    },

    /**
     * remove dump data
     * @param order
     * @return {{payments}}
     */
    filterOrderData(order) {
        if (!order.payments.length) {
            let newOrder = {...order};
            delete newOrder['payments'];
            return newOrder;
        }
        return {...order, payments: this.filterPaymentData(order.payments)};
    },
    /**
     * remove dump data
     * @param payments
     * @return {{payments}}
     */
    filterPaymentData(payments) {
        return payments.map(payment => {
            /**  remove cc information */
            delete payment['cc_owner'];
            delete payment['cc_number'];
            delete payment['cc_type'];
            delete payment['cc_exp_month'];
            delete payment['cc_exp_year'];
            delete payment['cc_cid'];
            delete payment['email'];
            delete payment['isCardMode'];
            delete payment['status'];
            delete payment['errorMessage'];
            delete payment['last4Digit'];
            delete payment['orderPayment'];
            delete payment['amount_refunded'];
            // delete payment.is_pay_later;
            return payment;
        });
    },

    /**
     *
     * @param processPayments
     * @param removedIndex
     * @return {*}
     */
    resortPayment(processPayments, removedIndex) {
        let newProcessPayments = {};
        Object.keys(processPayments).forEach(key => {
            let method   = key.substr(0, key.length - 1);
            let keyIndex = key.substr(key.length - 1) * 1;

            if (keyIndex < removedIndex) {
                newProcessPayments[key] = processPayments[key];
            }

            if (keyIndex > removedIndex) {
                newProcessPayments[method + (keyIndex - 1)] = processPayments[key];
            }
        });

        return newProcessPayments;
    },
    /**
     *
     * @param processPayments
     * @return {boolean}
     */
    hasPaidViaGatewaySuccess(processPayments) {
        let isCustomerPaidViaGateway = false;
        Object.values(processPayments).forEach(payment => {
            isCustomerPaidViaGateway |= payment.status === PaymentConstant.PROCESS_PAYMENT_SUCCESS;
        });

        return isCustomerPaidViaGateway;
    },
    /**
     *
     * @param processPayments
     * @return {boolean}
     */
    isWaitingProcessPaymentComplete(processPayments) {
        let isWaitingProcessPaymentComplete = false;
        Object.values(processPayments).forEach(payment => {
            isWaitingProcessPaymentComplete |= [
                PaymentConstant.PROCESS_PAYMENT_PENDING,
                PaymentConstant.PROCESS_PAYMENT_PROCESSING
            ].indexOf(payment.status) !== -1;
        });
        return isWaitingProcessPaymentComplete;
    },
    /**
     *
     * @param processPayments
     * @return {*|boolean}
     */
    hasPaidOrWaitingGatewayPayment(processPayments) {
        return this.hasPaidViaGatewaySuccess(processPayments) || this.isWaitingProcessPaymentComplete(processPayments);
    },
    /**
     *
     * @param processPayments
     * @return {boolean}
     */
    isSuccessAll(processPayments) {
        let isProcessedAllPayment = true;
        Object.values(processPayments).forEach(payment => {
            isProcessedAllPayment &= [
                PaymentConstant.PROCESSED_PAYMENT,
                PaymentConstant.PROCESS_PAYMENT_SUCCESS
            ].indexOf(payment.status) !== -1;
        });

        return isProcessedAllPayment
    },
    /**
     *
     * @param processPayments
     * @return {boolean}
     */
    hasErrorProcessPayment(processPayments) {
        let hasErrorProcessPayment = false;
        Object.values(processPayments).forEach(payment => {
            hasErrorProcessPayment |= [
                PaymentConstant.PROCESS_PAYMENT_ERROR
            ].indexOf(payment.status) !== -1;
        });
        return hasErrorProcessPayment;
    },

    /**
     *
     * @param object
     * @param refCode
     * @return {boolean | {}}
     */
    getPaymentByRefCode(object, refCode) {
        if (!object.payments) return false;
        if (!object.payments.length) return false;
        return object.payments.find(payment => payment.reference_number === refCode);
    },

    /**
     *
     * @param index
     * @return {string}
     */
    generateIncrement(index) {
        let incrementId = [];
        if (CustomPrefixHelper.getUseCustomPrefix()) {
            incrementId.push(CustomPrefixHelper.getCustomPrefix());
        }
        if (index) {
            incrementId.push(index);
        }

        let currentTimestamp = new Date().getTime();
        incrementId.push(Config.pos_id);
        incrementId.push(parseInt(currentTimestamp / 1000, 10));
        return incrementId.join('-');
    },

    /**
     *
     * @param object
     * @param seeder
     * @param payment
     * @return {*}
     */
    generateRefCode(object, seeder, payment) {
        let refCode = seeder;
        let counter = 1;
        let subFix = '';
        object.payments.forEach((objectPayment, index) => {
            if (objectPayment.method !== payment.method) {
                return;
            }

            if (objectPayment.increment_id === payment.increment_id) {
                subFix = `-${counter}`;
            }

            counter++;
        });

        return refCode + subFix;
    },

    /**
     *
     * @param refCode
     * @return {string}
     */
    convertRefCodeToIncrementOrderId(refCode) {
        if (!refCode || !refCode.length) return '';

        let refCodeArray = refCode.split('-');

        if (refCodeArray.length === 1) {
            //online order
            return refCodeArray[0];
        }

        if (CustomPrefixHelper.getUseCustomPrefix()) {
            refCode = refCode.replace(`${CustomPrefixHelper.getUseCustomPrefix()}-`, '');
            // custom prefix + pos id + time spam
            return [refCodeArray[0], refCodeArray[1], refCodeArray[2]].join('-');
        }

        //pos id + time spam
        return [refCodeArray[0], refCodeArray[1]].join('-');
    },

    /**
     *
     * @param object
     * @param {boolean} skipCheckPaymentType
     */
    isRequireInternet(object, skipCheckPaymentType) {
        return object.payments.find(payment => {
            let isEWallet = this.hasUsingEWallet(payment.method);
            let requireInternet = (
                    this.hasUsingCreditCardForm(payment.method)
                    || isEWallet
                    || INTERNET_TERMINAL_PAYMENTS.includes(payment.method)
                )
                && (
                    !payment.reference_number
                    || (
                        payment.reference_number
                        && isEWallet
                    )
                );

            if (skipCheckPaymentType) {
                return requireInternet;
            }

            return payment.type !== PaymentConstant.TYPE_REFUND && requireInternet
        });
    }
}
