/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import ConfigHelper from "./ConfigHelper";
import ConfigConstant from "../view/constant/catalog/stock/ConfigConstant";

export class StockHelper {
    /**
     * Get settings is manage stock
     *
     * @return {boolean}
     */
    static isManageStock() {
        return ConfigHelper.getConfig(ConfigConstant.XML_PATH_MANAGE_STOCK) === '1';
    }

    /**
     * Get settings is back order stock
     *
     * @return {boolean}
     */
    static isBackOrder() {
        return ConfigHelper.getConfig(ConfigConstant.XML_PATH_BACKORDERS) === '1';
    }

    /**
     * Get settings max sale qty stock
     *
     * @return {number}
     */
    static getMaxSaleQty() {
        let maxSaleQty = ConfigHelper.getConfig(ConfigConstant.XML_PATH_MAX_SALE_QTY);
        return parseFloat(maxSaleQty) || 100000000000000000;
    }

    /**
     * Get settings min sale qty stock
     *
     * @return {number}
     */
    static getMinSaleQty() {
        return ConfigHelper.getConfig(ConfigConstant.XML_PATH_MIN_SALE_QTY);
    }

    /**
     * Get settings out of stock threshold qty stock
     *
     * @return {number}
     */
    static getOutOfStockThreshold() {
        let minQty = ConfigHelper.getConfig(ConfigConstant.XML_PATH_MIN_QTY);
        return (minQty !== null && minQty !== '') ? parseFloat(minQty) : 0;
    }

    /**
     * Get settings enable qty increment stock
     *
     * @return {boolean}
     */
    static isEnableQtyIncrements() {
        return ConfigHelper.getConfig(ConfigConstant.XML_PATH_ENABLE_QTY_INCREMENTS) === '1';
    }

    /**
     * Get settings qty increment stock
     *
     * @return {number}
     */
    static getQtyIncrement() {
        return parseFloat(ConfigHelper.getConfig(ConfigConstant.XML_PATH_QTY_INCREMENTS)) || 1;
    }

    /**
     * Check if is possible subtract value from item qty
     * @return {boolean}
     */
    static canSubtractQty() {
        return ConfigHelper.getConfig(ConfigConstant.XML_PATH_CAN_SUBTRACT) === '1';
    }

    /**
     * Retrieve can Back in stock
     *
     * @return {boolean}
     */
    static getCanBackInStock() {
        return ConfigHelper.getConfig(ConfigConstant.XML_PATH_CAN_BACK_IN_STOCK) === '1';
    }

    /**
     * @return {*|null}
     */
    static getMinQty() {
        return +ConfigHelper.getConfig(ConfigConstant.XML_PATH_MIN_QTY);
    }
}

export default StockHelper;