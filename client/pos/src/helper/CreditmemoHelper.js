/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {StockHelper} from "./StockHelper";

export class CreditmemoHelper extends StockHelper{
    /**
     * Get Stock item
     *
     * @param item
     * @return {*}
     */
    static getStockItem(item) {
        if (Array.isArray(item.product_stocks) && item.product_stocks.length > 0) {
            return item.product_stocks[0];
        }
        return null;
    }

    /**
     * Check product is manage stock
     *
     * @param item
     * @returns {boolean}
     */
    static itemIsManageStock(item) {
        let stockItem = this.getStockItem(item);
        if (stockItem) {
            return (stockItem.use_config_manage_stock && this.isManageStock()) ||
                stockItem.manage_stock;
        }
        return false;
    }
}
