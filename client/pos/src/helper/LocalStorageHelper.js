/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
export default {
    /**
     * Used to avoid multiple app on same website use same local storage items
     */
    APP_NAME: 'magestore_pos',

    TOKEN: 'token',
    SESSION: 'session',
    TIMEOUT: 'timeout',
    STAFF_ID: 'staff_id',
    OLD_STAFF_ID: 'old_staff_id',
    STAFF_NAME: 'staff_name',
    STAFF_EMAIL: 'staff_email',
    STAFF_USERNAME: 'staff_username',
    MODE: 'mode',
    DATA_TYPE_MODE: 'data_type_mode',
    LOCATIONS: 'locations',
    LOCATION_ID: 'location_id',
    LOCATION_NAME: 'location_name',
    LOCATION_ADDRESS: 'location_address',
    LOCATION_TELEPHONE: 'location_telephone',
    POS_ID: 'pos_id',
    OLD_POS_ID: 'old_pos_id',
    POS_NAME: 'pos_name',
    CONFIG: 'config',
    COUNTRIES: 'countries',
    NEED_SYNC: 'need_sync',
    LOGO_URL: 'logo_url',
    COLOR_SWATCH: 'color_swatch',
    IS_SYNCING_ACTION_LOG: 'is_syncing_action_log',
    IS_FORCE_SIGNOUT: 'is_force_signout',
    DB_VERSION: 'db_version',
    NEED_SYNC_ORDER: 'need_sync_order',
    ORDER_STATUS: 'order_status',
    WEBSITE_ID: 'website_id',
    SHARING_ACCOUNT : 'sharing_account',

    /**
     * Get key saved in local storage
     * @param key
     * @returns {string}
     */
    getKey(key) {
        return this.APP_NAME + '_' + key;
    },

    /**
     * get value from local storage
     * @param key
     * @return string|Array|Object
     */
    get(key) {
        return localStorage.getItem(this.getKey(key));
    },

    /**
     * set data to local storage
     * @param key
     * @param value
     */
    set(key, value) {
        return localStorage.setItem(this.getKey(key), value);
    },

    /**
     * remove data to local storage
     * @param key
     * @return void
     */
    remove(key) {
        return localStorage.removeItem(this.getKey(key));
    },

    /**
     * set session to local storage
     * @param value
     */
    setSession(value){
        this.set(this.SESSION, value);
    },

    /**
     * get session to local storage
     * @returns {string | null}
     */
    getSession(){
        return this.get(this.SESSION);
    },

    /**
     * remove session
     */
    removeSession(){
        this.remove(this.SESSION);
    },

    /**
     * set token to local storage
     * @param token
     */
    setToken(token){
        this.set(this.TOKEN, token);
    },

    /**
     * get token to local storage
     * @returns {string | null}
     */
    getToken(){
        return this.get(this.TOKEN);
    },

    /**
     * token session
     */
    removeToken(){
        this.remove(this.TOKEN);
    }
}
