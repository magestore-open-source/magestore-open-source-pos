/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../../CoreService";
import ServiceFactory from "../../../framework/factory/ServiceFactory";
import CatalogRuleProductPriceResourceModel from "../../../resource-model/catalog/CatalogRuleProductPriceResourceModel";
import QueryService from "../../QueryService";

export class CatalogRuleProductPriceService extends CoreService {
    static className = 'CatalogRuleProductPriceService';
    resourceModel = CatalogRuleProductPriceResourceModel;

    /**
     * get not existed ids
     * @param ruleProductPriceIds
     * @return {*|Promise<Array>|Promise}
     */
    getNotExistedIds(ruleProductPriceIds) {
        return this.getResourceModel().getNotExistedIds(ruleProductPriceIds);
    }

    /**
     * get catalog rule product price ids
     * @param queryService
     * @param isSync
     * @returns {*|Promise<any>}
     */
    getIds(queryService = {}, isSync = false) {
        return this.getResourceModel().getIds(queryService, isSync);
    }

    /**
     * get all catalog rule product price ids
     * @param isSync
     * @returns {Promise<*>}
     */
    async getAllIds(isSync = false) {
        let pageSize = 300;
        let queryService = QueryService.reset();
        queryService.setPageSize(pageSize).setCurrentPage(1);
        let response = await this.getIds(queryService, isSync);
        let ids = response.items;
        if (ids.length === response.total_count) {
            return ids;
        }

        let totalPage = Number(Math.ceil(response.total_count / pageSize));
        for (let i = 2; i <= totalPage; i++) {
            queryService = QueryService.reset();
            queryService.setPageSize(pageSize).setCurrentPage(i);
            response = await this.getIds(queryService, isSync);
            ids = ids.concat(response.items);
        }
        return ids;
    }

    /**
     * get catalog rule product price for products
     * @param productIds
     * @returns {*|Promise<void>}
     */
    getCatalogRulePriceProducts(productIds) {
        return this.getResourceModel().getCatalogRulePriceProducts(productIds);
    }
}

/** @type CatalogRuleProductPriceService */
let catalogRuleProductPriceService = ServiceFactory.get(CatalogRuleProductPriceService);

export default catalogRuleProductPriceService;
