/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../../CoreService";
import SimplePriceService from './price/SimplePriceService';
import ConfigurablePriceService from './price/ConfigurablePriceService';
import BundlePriceService from './price/BundlePriceService';
import ServiceFactory from "../../../framework/factory/ServiceFactory";

export class PriceService extends CoreService {
    static className = 'ProductPriceService';
    priceServices = {
        simple: SimplePriceService,
        configurable: ConfigurablePriceService,
        bundle: BundlePriceService,
    };

    /**
     *  Get product price service by product type id
     *
     * @param product
     * @return {AbstractPriceService}
     */
    getPriceService(product) {
        if (typeof product === 'string' && this.priceServices[product]) {
            return this.priceServices[product];
        }
        if (product.type_id && this.priceServices[product.type_id]) {
            return this.priceServices[product.type_id];
        }
        return this.priceServices.simple;
    }
}

/** @type PriceService */
let priceService = ServiceFactory.get(PriceService);

export default priceService;