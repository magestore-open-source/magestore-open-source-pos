/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../../../CoreService";
import ServiceFactory from "../../../../framework/factory/ServiceFactory"

export class AbstractProductTypeService extends CoreService {
    static className = 'AbstractProductTypeService';

    static CALCULATE_CHILD = 0;

    static CALCULATE_PARENT = 1;

    /**#@+
     * values for shipment type (invoice etc)
     */
    static SHIPMENT_SEPARATELY = 1;

    static SHIPMENT_TOGETHER = 0;
}


let abstractProductTypeService = ServiceFactory.get(AbstractProductTypeService);

export default abstractProductTypeService;
