/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {AbstractPriceService} from "./AbstractPriceService";
import ServiceFactory from "../../../../framework/factory/ServiceFactory";

export class ConfigurablePriceService extends AbstractPriceService {
    static className = 'ConfigurableProductPriceService';

    /**
     * Get
     *
     * @param qty
     * @param product
     * @param quote
     * @param item
     */
    getFinalPrice(qty, product, quote, item) {
        let finalPrice = 0;
        if (product.custom_option && product.custom_option.simple_product && product.custom_option.simple_product.id) {
            finalPrice = super.getFinalPrice(qty, product.custom_option.simple_product, quote, item);
        }
        finalPrice = this._applyOptionsPrice(product, qty, finalPrice);
        finalPrice = Math.max(0, finalPrice);
        finalPrice = this._applyCustomPrice(item, finalPrice);
        product.final_price = finalPrice;
        return finalPrice;
    }

    /**
     * Get
     *
     * @param qty
     * @param product
     * @param quote
     * @param item
     */
    getOriginalFinalPrice(qty, product, quote, item) {
        let finalPrice = 0;
        if (product.custom_option && product.custom_option.simple_product && product.custom_option.simple_product.id) {
            finalPrice = super.getOriginalFinalPrice(qty, product.custom_option.simple_product, quote, item);
        }
        finalPrice = this._applyOptionsPrice(product, qty, finalPrice);
        finalPrice = Math.max(0, finalPrice);
        product.final_price = finalPrice;
        return finalPrice;
    }
}

let configurablePriceService = ServiceFactory.get(ConfigurablePriceService);

export default configurablePriceService;