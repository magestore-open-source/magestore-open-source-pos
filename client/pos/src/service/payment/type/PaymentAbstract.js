/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import PaymentService from "../PaymentService";
import PaymentConstant from "../../../view/constant/PaymentConstant";

export class PaymentAbstract {
    code = '';
    quote;
    order;
    creditmemo;
    payment;

    /**
     *
     * @param code
     * @return {PaymentAbstract}
     */
    setCode(code) {
        this.code = code;
        return this;
    }

    /**
     *
     * @param quote
     * @return {PaymentAbstract}
     */
    setQuote(quote) {
        this.quote      = quote;
        this.order      = null;
        this.creditmemo = null;
        return this;
    }

    /**
     *
     * @param order
     * @return {PaymentAbstract}
     */
    setOrder(order) {
        this.order      = order;
        this.quote      = null;
        this.creditmemo = null;
        return this;
    }

    /**
     *
     * @param creditmemo
     * @return {PaymentAbstract}
     */
    setCreditmemo(creditmemo) {
        this.creditmemo = creditmemo;
        this.order      = null;
        this.quote      = null;
        return this;
    }

    /**
     * set payment
     * @param payment
     * @returns {PaymentAbstract}
     */
    setPayment(payment) {
        this.payment = {...payment};
        return this;
    }

    /**
     *
     * @return {string}
     */
    getCode() {
        return this.code;
    }

    /**
     *
     * @return {*}
     */
    getQuote() {
        return this.quote;
    }

    /**
     *
     * @return {*}
     */
    getOrder() {
        return this.order;
    }

    /**
     *
     * @return {*}
     */
    getCreditmemo() {
        return this.creditmemo;
    }

    /**
     *
     * @return {*}
     */
    getPayment() {
        return this.payment;
    }


    /**
     *  prepare payload
     *
     * @return {PaymentAbstract}
     */
    prepare() {
        return this
    }

    /**
     *
     * @return {Promise<{}>}
     */
    async execute() {
        return {}
    }

    /**
     *
     * @return {PaymentAbstract}
     */
    clear() {
        this.payment = null;
        this.order = null;
        this.creditmemo = null;
        this.quote = null;
        return this;
    }

    /**
     *
     * @param code
     * @return {Promise<*>}
     */
    getPaymentMethodByCode(code) {
        return PaymentService.getByCode(code)
    }

    /**
     * Make Error Message
     *
     * @param response
     * @return {string}
     */
    makeError(response) {
        response = response || {};
        let error = response.error || {};
        let message = response.errorMessage
            || error.message
            || response.message
            || PaymentConstant.UNKNOWN_EXCEPTION_MESSAGE;

        if (!error.items || !error.items.length) {
            return message;
        }

        message = [];
        error.items.forEach(item => {
            if (!message.includes(item.message)) {
                message.push(item.message);
            }
        });

        return message.join(', ');
    }

    /**
     * Show Error
     *
     * @param {*} popup
     * @param {*} response
     * @returns
     * @memberof PaymentAbstract
     */
    showError(popup, response) {
        return popup.showError(popup, this.makeError(response));
    }
}