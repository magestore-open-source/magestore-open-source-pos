/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {SalesRuleByPercentService} from "./ByPercentService";
import ServiceFactory from "../../../../../framework/factory/ServiceFactory";

export class SalesRuleToPercentService extends SalesRuleByPercentService {
    static className = 'SalesRuleToPercentService';

    /**
     * caculate discount data
     * @param {object} quote
     * @param {object} address
     * @param {object} rule
     * @param {object} item
     * @param {object} qty
     */
    calculate(quote, address, rule, item, qty) {
        /* Get discount percent from rule's discount amount */
        let rulePercent = Math.max(0, 100 - rule.discount_amount);
        /* Return discount data got from _caculate function */
        return this._calculate(quote, rule, item, qty, rulePercent);
    }
}


/** @type SalesRuleToPercentService */
let salesRuleToPercentService = ServiceFactory.get(SalesRuleToPercentService);

export default salesRuleToPercentService;
