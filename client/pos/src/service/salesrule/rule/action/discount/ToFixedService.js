/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {SalesRuleAbstractDiscountService} from "./AbstractDiscountService";
import ServiceFactory from "../../../../../framework/factory/ServiceFactory";
import ValidatorService from "../../../ValidatorService";
import CurrencyHelper from "../../../../../helper/CurrencyHelper";

export class SalesRuleToFixedService extends SalesRuleAbstractDiscountService {
    static className = 'SalesRuleToFixedService';

    /**
     * Calculate discount for "ToFixed" type, mean discount from price to discount amount
     * The value of discount is equal price - discount_amount
     *
     * @param {object} quote
     * @param {object} address
     * @param {object} rule
     * @param {object} item
     * @param {object} qty
     */
    calculate(quote, address, rule, item, qty) {
        let discountData = {};

        let itemPrice = ValidatorService.getItemPrice(item);
        let baseItemPrice = ValidatorService.getItemBasePrice(item);
        let itemOriginalPrice = ValidatorService.getItemOriginalPrice(item, quote);
        let baseItemOriginalPrice = ValidatorService.getItemBaseOriginalPrice(item, quote);

        let quoteAmount = CurrencyHelper.convert(rule.discount_amount);
        discountData.amount = qty * (itemPrice - quoteAmount);
        discountData.base_amount = qty * (baseItemPrice - rule.discount_amount);
        discountData.original_amount = qty * (itemOriginalPrice - quoteAmount);
        discountData.base_original_amount = qty * (baseItemOriginalPrice - rule.discount_amount);

        return discountData;
    }
}


/** @type SalesRuleToFixedService */
let salesRuleToFixedService = ServiceFactory.get(SalesRuleToFixedService);

export default salesRuleToFixedService;
