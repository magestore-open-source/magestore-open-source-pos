/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {SalesRuleAbstractDiscountService} from "./AbstractDiscountService";
import ServiceFactory from "../../../../../framework/factory/ServiceFactory";
import ValidatorService from "../../../ValidatorService";
import NumberHelper from "../../../../../helper/NumberHelper";

export class SalesRuleBuyXGetYService extends SalesRuleAbstractDiscountService {
    static className = 'SalesRuleBuyXGetYService';

    /**
     * calculate discount value for item match with the rule which has action Buy X Get Y
     *
     * @param {object} quote
     * @param {object} address
     * @param {object} rule
     * @param {object} item
     * @param {number} qty
     */
    calculate(quote, address, rule, item, qty) {
        let discountData = {};
        let itemPrice = ValidatorService.getItemPrice(item);
        let baseItemPrice = ValidatorService.getItemBasePrice(item);
        let itemOriginalPrice = ValidatorService.getItemOriginalPrice(item, quote);
        let baseItemOriginalPrice = ValidatorService.getItemBaseOriginalPrice(item, quote);

        let x = rule.discount_step;
        let y = rule.discount_amount;

        if (!x || y > x) {
            return discountData;
        }

        let buyAndDiscountQty = NumberHelper.addNumber(x, y);

        let fullRuleQtyPeriod = Math.floor(parseFloat(qty / buyAndDiscountQty).toFixed(1));
        let freeQty = NumberHelper.minusNumber(qty, NumberHelper.multipleNumber(fullRuleQtyPeriod, buyAndDiscountQty));

        let discountQty = NumberHelper.multipleNumber(fullRuleQtyPeriod, y);

        if (freeQty > x) {
            discountQty = NumberHelper.addNumber(discountQty, freeQty, -x);
        }

        discountData.amount = discountQty * itemPrice;
        discountData.base_amount = discountQty * baseItemPrice;
        discountData.original_amount = discountQty * itemOriginalPrice;
        discountData.base_original_amount = discountQty * baseItemOriginalPrice;

        return discountData;
    }
}


/** @type SalesRuleBuyXGetYService */
let salesRuleBuyXGetYService = ServiceFactory.get(SalesRuleBuyXGetYService);

export default salesRuleBuyXGetYService;
