/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CalculatorFactoryService from '../CalculatorFactoryService';
import ByFixedService from '../ByFixedService';

describe('Test calculatior factory service', () => {

  it('[DIS-CFS-01] Create calculator for empty type', () => {
    expect(CalculatorFactoryService.create('')).toEqual(undefined);
  });

  it('[DIS-CFS-02] Create calculator for by_fixed type', () => {
    expect(CalculatorFactoryService.create('by_fixed')).toEqual(ByFixedService);
  });

  it('[DIS-CFS-03] Create calculator for none type', () => {
    expect(CalculatorFactoryService.create('none')).toEqual(undefined);
  });
});
