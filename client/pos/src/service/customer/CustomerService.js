/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../CoreService";
import ServiceFactory from "../../framework/factory/ServiceFactory";
import CustomerResourceModel from "../../resource-model/customer/CustomerResourceModel";
import ActionLogService from "../../service/sync/ActionLogService";
import SyncConstant from "../../view/constant/SyncConstant";
import DateTimeHelper from "../../helper/DateTimeHelper";
import {cloneDeep} from 'lodash';
import LocationHelper from "../../helper/LocationHelper";
import GuestCustomerHelper from "../../helper/GuestCustomerHelper";
import ConfigHelper from "../../helper/ConfigHelper";
import CustomerConstant from "../../view/constant/CustomerConstant";
import punycode from "punycode";

export class CustomerService extends CoreService {
    static className = 'CustomerService';
    resourceModel = CustomerResourceModel;

    /**
     * check email
     * @param email
     * @returns {*|{type: string, email: *}|Dexie.Promise.<any|T>|Dexie.Promise.<any>|Promise.<any>}
     */
    checkEmail(email) {
        let customerResource = this.getResourceModel(CustomerResourceModel);
        return customerResource.checkEmail(email);
    }

    /**
     * Validate Email
     *
     * @param {*} email
     * @returns {boolean}
     */
    validateEmail(email) {
        if (!email || typeof email !== "string" || !ConfigHelper.regexEmail.test(email)) {
            return false;
        }

        // Split email address up
        let matches = /^(.+)@([^@]+)$/.exec(email);

        // Disallow '..'
        if (email.includes('..') || !matches) {
            return false;
        }
        
        let localPart = matches[1];
        let hostname = matches[2];

        if (localPart.length > 64 || hostname.length > 255) {
            return false;
        }

        if (!this.validateEmailLocalPart(localPart)) {
            return false;
        }
        return true;
    }

    /**
     * Validate Email Local Part
     * 
     * @param {string} localPart
     * @returns {boolean}
     */
    validateEmailLocalPart(localPart) {
        // eslint-disable-next-line
        let regex = /^[a-zA-Z0-9\x21\x23\x24\x25\x26\x27\x2a\x2b\x2d\x2f\x3d\x3f\x5e\x5f\x60\x7b\x7c\x7d\x7e]+(\x2e+[a-zA-Z0-9\x21\x23\x24\x25\x26\x27\x2a\x2b\x2d\x2f\x3d\x3f\x5e\x5f\x60\x7b\x7c\x7d\x7e]+)*$/;

        if (regex.test(localPart)) {
            return true;
        } else {
            regex = /^"([\x20-\x21\x23-\x5b\x5d-\x7e']|\x5c[\x20-\x7e])*"$/;
            if (regex.test(localPart)) {
                return true;
            }
        }
        return false;
    }

    /**
     * create customer
     * @param customer
     * @returns {Promise.<*>}
     */
    async createCustomer(pCustomer) {
        pCustomer.created_location_id = LocationHelper.getId();

        let nameFields = [
            pCustomer.prefix,
            pCustomer.firstname,
            pCustomer.middlename,
            pCustomer.lastname,
            pCustomer.suffix,
        ];
        nameFields = nameFields.filter(field => field);
        pCustomer.full_name = nameFields.join(' ');
        pCustomer.search_string = pCustomer.email
            + ' ' + punycode.toUnicode(pCustomer.email)
            + ' ' + pCustomer.telephone
            + ' ' + pCustomer.full_name;
        pCustomer.is_creating = true;
        pCustomer.tmp_customer_id = CustomerConstant.TMP_CUSTOMER_ID_PREFIX + pCustomer.id;
        let customerResource = this.getResourceModel(CustomerResourceModel);
        let newCustomer = await customerResource.createCustomer(pCustomer);
        let customer = this.convertParamsCustomer(pCustomer);
        let params = {
            customer
        };
        let url_api = customerResource.getResourceOnline().getPathSaveCustomer();
        await ActionLogService.createDataActionLog(
            SyncConstant.REQUEST_CREATE_CUSTOMER, url_api, SyncConstant.METHOD_POST, params
        );
        return newCustomer;
    }

    /**
     * convert params customer
     * @param customer
     * @returns {*}
     */
    convertParamsCustomer(customer) {
        let new_customer = cloneDeep(customer);
        delete new_customer.full_name;
        delete new_customer.search_string;
        delete new_customer.is_creating;
        if (new_customer.point_balance) {
            delete new_customer.point_balance
        }
        if (new_customer.addresses) {
            for (let address of new_customer.addresses) {
                delete address.id;
                delete address.sub_id;
                delete address.is_new_address;
            }
        }
        return new_customer;
    }

    /**
     * edit customer
     * @param pCustomer
     * @returns {Promise.<*>}
     */
    async editCustomer(pCustomer) {
        let customerResource = this.getResourceModel(CustomerResourceModel);
        let newCustomer = await customerResource.editCustomer(pCustomer);
        let customer = this.convertParamsEditCustomer(pCustomer);
        let params = {
            customer
        };
        let url_api = customerResource.getResourceOnline().getPathSaveCustomer() + "/" + customer.id;
        await ActionLogService.createDataActionLog(
            SyncConstant.REQUEST_EDIT_CUSTOMER, url_api, SyncConstant.METHOD_PUT, params
        );
        return newCustomer;
    }

    /**
     * convert params edit customer
     * @param customer
     * @returns {*}
     */
    convertParamsEditCustomer(customer) {
        let new_customer = cloneDeep(customer);
        let currentTimestamp = new Date().getTime();
        let databaseCurrentTime = DateTimeHelper.getDatabaseDateTime(currentTimestamp);
        new_customer.full_name = new_customer.firstname + " " + new_customer.lastname;
        new_customer.updated_at = databaseCurrentTime;
        if (new_customer.point_balance) {
            delete new_customer.point_balance
        }
        delete new_customer.search_string;
        if (new_customer.addresses) {
            for (let address of new_customer.addresses) {
                if (address.is_new_address) {
                    delete address.id;
                }
                delete address.is_new_address;
                delete address.sub_id;
            }
        }
        return new_customer;
    }

    /**
     * get path customer
     * @returns {*}
     */
    getPathSaveCustomer() {
        return this.getResourceModel().getResourceOnline().getPathSaveCustomer();
    }

    /**
     * get customer by id
     * @param id
     * @param field
     */
    get(id, field = null) {
        return this.getResourceModel().get(id, field);
    }

    /**
     * Get default address by customer
     *
     * @param customer
     * @return {{firstname: *|string, lastname: *|string, country_id: *|string, region_id: *|number, region: *|string,
     *     postcode: *|string, street: *|string, telephone: *|string}}
     */
    getDefaultAddressByCustomer(customer) {
        return {
            firstname: customer && customer.id ? customer.firstname : GuestCustomerHelper.getFirstname(),
            lastname: customer && customer.id ? customer.lastname : GuestCustomerHelper.getLastname(),
            country_id: LocationHelper.getCountryId(),
            region_id: LocationHelper.getRegionId(),
            region: LocationHelper.getRegion(),
            postcode: LocationHelper.getPostcode(),
            street: LocationHelper.getStreet(),
            telephone: LocationHelper.getTelephone()
        };
    }

    /**
     * Get default billing address of customer
     *
     * @param customer
     * @return {*}
     */
    getDefaultBillingAddress(customer) {
        let address = null;
        if (customer && customer.addresses && customer.addresses.length) {
            address = customer.addresses.find(address => address.default_billing);
        }
        if (!address) {
            address = this.getDefaultAddressByCustomer(customer);
        }
        return address;
    }

    /**
     * Get default shipping address of customer
     *
     * @param customer
     * @return {*}
     */
    getDefaultShippingAddress(customer) {
        let address = null;
        if (customer && customer.addresses && customer.addresses.length) {
            address = customer.addresses.find(address => address.default_shipping);
        }
        if (!address) {
            address = this.getDefaultAddressByCustomer(customer);
        }
        return address;
    }

    /**
     * check need update data
     * @return {*|boolean|number}
     */
    needUpdateData() {
        return false;
    }
}

/** @type CustomerService */
let customerService = ServiceFactory.get(CustomerService);

export default customerService;
