/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import React from 'react';
import ServiceFactory from "../framework/factory/ServiceFactory";

import StylePrintComponent, {
    BARCODE_FONT_SIZE, BARCODE_HEIGHT, BARCODE_WIDTH
} from "../view/component/print/StylePrintComponent";
import {toast} from "react-toastify";
import i18n from "../config/i18n";

export class PrinterService {
    static className = 'PrinterService';

    /**
     *
     * @param target
     */
    removeWindow = (target) => {
        setTimeout(() => {
            target.parentNode.removeChild(target);
        }, 500);
    };

    /**
     *
     * @returns {{component: *, BARCODE_WIDTH: number, BARCODE_FONT_SIZE: number, BARCODE_HEIGHT: number}}
     */
    getStyleForPrint() {
        return {
            BARCODE_FONT_SIZE,
            BARCODE_HEIGHT,
            BARCODE_WIDTH,
            StylePrintComponent: <StylePrintComponent/>
        }
    }

    /**
     *
     * @param reason
     */
    showError(reason) {
        toast.error(
            i18n.translator.translate(reason),
            {
                className: 'wrapper-messages messages-warning',
                autoClose: 5000
            }
        );
    }

    /**
     *
     * @param content
     * @param title
     * @param option
     */
    print(content, title, option) {
        let print_window = window.open('', title, option);
        if (print_window) {
            print_window.document.open();
            print_window.document.write(content);
            print_window.print();
            print_window.close();
        } else {
            window.alert("Your browser has blocked the automatic popup, " +
                "please change your browser setting or print the receipt manually");
        }
    }

    /**
     * Print window target
     *
     * @param target
     */
    printTarget(target) {
        if (this.isChromeOnIOS()) {
            /* Chrome on IOS has problem on print iframe popup*/
            this.print(
                target.contentWindow.document.documentElement.innerHTML,
                '',
                'width=1,height=1'
            );
        } else {
            target.contentWindow.focus();
            target.contentWindow.print();
        }
    }

    /**
     * Is chrome ios or not
     *
     * @returns {boolean}
     */
    isChromeOnIOS() {
        return /CriOS/i.test(navigator.userAgent);
    }
}

/**
 * @type {PrinterService}
 */
let printerService = ServiceFactory.get(PrinterService);

export default printerService;
