/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "./CoreService";
import ServiceFactory from "../framework/factory/ServiceFactory";
import MultiCartResourceModel from "../resource-model/MultiCartResourceModel";
import PosService from "./PosService";
import {fire} from "../event-bus";


export let count = 0;

export class MultiCartService extends CoreService {
    static className = 'MultiCartService';
    resourceModel = MultiCartResourceModel;


    /**
     * Call MultiCartResourceModel search by cashier Id
     * @param posId
     * @return {Promise<*>}
     */
    async searchByPosId(posId) {
        let resourceModel = this.getResourceModel();
        let list = await resourceModel.searchByPosId(posId);
        count = list.length ? list[0].count : 0;
        return list;
    }

    /**
     * Call MultiCartResourceModel search by cashier Id
     * @return {Promise<*>}
     */
    async searchByCurrentPos() {
        return this.searchByPosId(PosService.getCurrentPosId());
    }

    /**
     * Call MultiCartResourceModel add cart
     * @param cart
     * @returns {*|{type: string, code: *}}
     */
    add(cart) {
        cart = {...cart, payments: [], valid_salesrule: null, coupon_code: null, count: ++count};
        let resourceModel = this.getResourceModel();
        return resourceModel.add(cart);
    }

    /**
     * add new cart from Store
     * @param store
     */
    addCartFromStore(store) {
        let quote = store.getState().core.checkout.quote;
        let newCart = {
            pos_id: PosService.getCurrentPosId(),
            ...quote,
            payments: [],
            valid_salesrule: [],
        };
        return this.add(newCart);
    }

    /**
     * add new cart from Store
     * @param quote
     */
    addCartByQuote(quote) {
        let newCart = {
            pos_id: PosService.getCurrentPosId(),
            ...quote,
            payments: [],
            valid_salesrule: [],
        };
        return this.add(newCart);
    }

    /**
     * reset count and add new cart from Store
     * @param store
     */
    resetCountAndAddCartFromStore(store) {
        count = 0;
        return this.addCartFromStore(store);
    }

    /**
     * Call MultiCartResourceModel update cart
     * @param cart
     * @returns {*|{type: string, code: *}}
     */
    update(cart) {
        let resourceModel = this.getResourceModel();
        return resourceModel.update(cart);
    }

    /**
     * Call MultiCartResourceModel update cart
     * @param store
     * @return {*|{type: string, code: *}}
     */
    updateActiveCartFromStore(store) {
        let quote = store.getState().core.checkout.quote;
        let neededUpdateCart = { ...quote, valid_salesrule: [] };

        fire('service_multi_cart_update_active_cart_from_store_update_before', {neededUpdateCart});

        let resourceModel = this.getResourceModel();
        return resourceModel.update(neededUpdateCart);
    }

    /**
     * Call MultiCartResourceModel delete cart
     * @param cart
     * @returns {*|{type: string, code: *}}
     */
    delete(cart) {
        let resourceModel = this.getResourceModel();
        return resourceModel.delete(cart);
    }
}

/** @type MultiCartService */
let multiCartService = ServiceFactory.get(MultiCartService);

export default multiCartService;
