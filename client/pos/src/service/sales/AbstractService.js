/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../CoreService";
import ServiceFactory from "../../framework/factory/ServiceFactory";
import ProductTypeConstant from "../../view/constant/ProductTypeConstant";

export class AbstractOrderService extends CoreService {
    static className = 'AbstractOrderService';

    /**
     * Get order item from product id
     *
     * @param order
     * @param productId
     */
    getItemsByProductId(order, productId) {
        return order.items.filter(item => item.product_id === productId);
    }

    /**
     * Get children items from order item parent id
     *
     * @param parentItem
     * @param order
     * @return {mixed[]|boolean}
     */
    getChildrenItems(parentItem, order) {
        if (parentItem.type_id === ProductTypeConstant.SIMPLE ||
            parentItem.product_type === ProductTypeConstant.SIMPLE) {
            return false;
        }
        return order.items.filter(item => +item.parent_item_id === +parentItem.item_id);
    }

    /**
     * Get parent item id from order item id
     *
     * @param childItem
     * @param order
     */
    getParentItem(childItem, order) {
        if (!childItem.parent_item_id) {
            return false;
        }
        return order.items.find(item => (+item.item_id === +childItem.parent_item_id));
    }
}

/**
 *
 * @type {AbstractOrderService}
 */
let abstractOrderService = ServiceFactory.get(AbstractOrderService);

export default abstractOrderService;