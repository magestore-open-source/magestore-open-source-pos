/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../../../CoreService";
import ServiceFactory from "../../../../framework/factory/ServiceFactory";
import NumberHelper from "../../../../helper/NumberHelper";
import StockManagementService from "../../../catalog/StockManagementService";
import OrderItemService from "../../../sales/order/OrderItemService";

export class ReturnProcessorService extends CoreService {
    static className = 'ReturnProcessorService';

    execute(creditmemo, order, returnToStockItems = [], isAutoReturn = false) {
        let itemsToUpdate = {};

        if (creditmemo && creditmemo.items && creditmemo.items.length) {
            creditmemo.items.forEach(item => {
                let productId = item.product_id;
                let orderItem = item.order_item;
                let parentItemId = orderItem.parent_item_id;
                let qty = item.qty;
                if (isAutoReturn || this.canReturnItem(item, qty, parentItemId, returnToStockItems)) {
                    if (itemsToUpdate[productId]) {
                        itemsToUpdate[productId] = NumberHelper.addNumber(itemsToUpdate[productId], qty);
                    } else {
                        itemsToUpdate[productId] = qty;
                    }
                    if (!OrderItemService.isDummy(orderItem, order)) {
                        let childrenItems = OrderItemService.getChildrenItems(orderItem, order);
                        if (childrenItems && childrenItems.length) {
                            childrenItems.forEach(children => {
                                let parentItemId = children.parent_item_id;
                                if (isAutoReturn || this.canReturnItem(children, qty, parentItemId, returnToStockItems)) {
                                    let qtyIncrement = children.qty_ordered / orderItem.qty_ordered;
                                    let qtyToReturn = NumberHelper.multipleNumber(qtyIncrement, qty);
                                    let productId = children.product_id;
                                    if (itemsToUpdate[productId]) {
                                        itemsToUpdate[productId] = NumberHelper.addNumber(itemsToUpdate[productId], qtyToReturn);
                                    } else {
                                        itemsToUpdate[productId] = qtyToReturn;
                                    }
                                }
                            });
                        }
                    }
                }
            })
        }
        if (!Object.keys(itemsToUpdate).length) {
            return this;
        }
        StockManagementService.backItemQty(itemsToUpdate);
    }

    /**
     * @param item
     * @param qty
     * @param parentItemId
     * @param returnToStockItems
     * @return {boolean | *}
     */
    canReturnItem(item, qty, parentItemId, returnToStockItems = []) {
        return (returnToStockItems.includes(item.order_item_id) || returnToStockItems.includes(parentItemId)) && qty;
    }
}

/**
 *
 * @type {ReturnProcessorService}
 */
let returnProcessorService = ServiceFactory.get(ReturnProcessorService);

export default returnProcessorService;