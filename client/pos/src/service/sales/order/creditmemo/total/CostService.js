/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {CreditmemoAbstractTotalService} from "./AbstractTotalService";
import ServiceFactory from "../../../../../framework/factory/ServiceFactory";
import NumberHelper from "../../../../../helper/NumberHelper";

export class CreditmemoCostService extends CreditmemoAbstractTotalService {
    static className = 'CreditmemoCostService';

    /**
     * Collect creditmemo cost
     *
     * @param creditmemo
     * @return {CreditmemoCostService}
     */
    collect(creditmemo) {
        let baseRefundTotalCost = 0;
        creditmemo.items.forEach(item => {
            baseRefundTotalCost = NumberHelper.addNumber(
                baseRefundTotalCost, NumberHelper.multipleNumber(item.base_cost, item.qty)
            );
        });
        creditmemo.base_cost = baseRefundTotalCost;
        return this;
    }
}

/** @type CreditmemoCostService */
let creditmemoCostService = ServiceFactory.get(CreditmemoCostService);

export default creditmemoCostService;