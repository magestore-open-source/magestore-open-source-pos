/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {AbstractOrderService} from "../../AbstractService";
import ServiceFactory from "../../../../framework/factory/ServiceFactory";
import CalculatorService from "../../../framework/math/CalculatorService";

export class CreditmemoPriceService extends AbstractOrderService {
    static className = 'CreditmemoPriceService';

    _calculators = [];

    /**
     * Reset calculators
     */
    resetCalculators() {
        this._calculators = [];
    }

    /**
     * Round price considering delta
     *
     * @param price
     * @param type
     * @param negative
     * @return {*}
     */
    roundPrice(price, type = 'regular', negative = false) {
        if (price) {
            if (!this._calculators[type]) {
                this._calculators[type] = ServiceFactory.create(CalculatorService);
            }
            price = this._calculators[type].deltaRound(price, negative);
        }
        return price;
    }
}

/** @type CreditmemoPriceService */
let creditmemoPriceService = ServiceFactory.get(CreditmemoPriceService);

export default creditmemoPriceService;