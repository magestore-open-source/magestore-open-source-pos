/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {InvoiceAbstractTotalService} from "./AbstractTotalService";
import ServiceFactory from "../../../../../framework/factory/ServiceFactory";
import NumberHelper from "../../../../../helper/NumberHelper";

export class InvoiceShippingService extends InvoiceAbstractTotalService {
    static className = 'InvoiceShippingService';

    /**
     * Collect invoice shipping
     *
     * @param invoice
     * @return {InvoiceShippingService}
     */
    collect(invoice) {
        invoice.shipping_amount = 0;
        invoice.base_shipping_amount = 0;
        let order = invoice.order;

        let orderShippingAmount = order.shipping_amount,
            baseOrderShippingAmount = order.base_shipping_amount,
            shippingInclTax = order.shipping_incl_tax,
            baseShippingInclTax = order.base_shipping_incl_tax;
        if (orderShippingAmount) {
            if (order.shipping_invoiced) {
                return this;
            }
            invoice.shipping_amount = orderShippingAmount;
            invoice.base_shipping_amount = baseOrderShippingAmount;
            invoice.shipping_incl_tax = shippingInclTax;
            invoice.base_shipping_incl_tax = baseShippingInclTax;

            invoice.grand_total = NumberHelper.addNumber(invoice.grand_total, orderShippingAmount);
            invoice.base_grand_total = NumberHelper.addNumber(invoice.base_grand_total, baseOrderShippingAmount);
        }
        return this;
    }
}

/** @type InvoiceShippingService */
let invoiceShippingService = ServiceFactory.get(InvoiceShippingService);

export default invoiceShippingService;