/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../../../../CoreService";
import ServiceFactory from "../../../../../framework/factory/ServiceFactory";
import InvoiceItemService from "../InvoiceItemService";

export class InvoiceAbstractTotalService extends CoreService {
    static className = 'InvoiceAbstractTotalService';

    /**
     * Collect invoice total
     *
     * @param invoice
     * @return {InvoiceAbstractTotalService}
     */
    collect(invoice) {
        return this;
    }

    /**
     * Checking if the invoice is last
     *
     * @param invoice
     * @return {boolean}
     */
    isLast(invoice) {
        let result = true;
        if (invoice && invoice.items && invoice.items.length) {
            invoice.items.forEach(item => {
                if (!result) {
                    return false;
                }
                if (!InvoiceItemService.isLast(item, invoice)) {
                    result = false;
                }
            })
        }
        return result;
    }
}

/** @type InvoiceAbstractTotalService */
let invoiceAbstractTotalService = ServiceFactory.get(InvoiceAbstractTotalService);

export default invoiceAbstractTotalService;