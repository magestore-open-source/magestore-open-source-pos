/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {AbstractOrderService} from "../AbstractService";
import ServiceFactory from "../../../framework/factory/ServiceFactory";
import OrderResourceModel from "../../../resource-model/order/OrderResourceModel";
import OrderService from "../OrderService";
import ActionLogService from "../../sync/ActionLogService";
import SyncConstant from "../../../view/constant/SyncConstant";
import cloneDeep from 'lodash/cloneDeep';
import CurrencyHelper from "../../../helper/CurrencyHelper";
import StatusConstant from "../../../view/constant/order/StatusConstant";
import {AbstractProductTypeService} from "../../catalog/product/type/AbstractTypeService";
import PaymentHelper from "../../../helper/PaymentHelper";
import InvoiceService from "./InvoiceService";
import PaymentService from "../../payment/PaymentService";

export class TakePaymentService extends AbstractOrderService {
    static className = 'TakePaymentService';

    /**
     * take payment
     * @param order
     * @return {Promise<{order: *, createInvoice: number}>}
     */
    async takePayment(order) {
        let orderResource = this.getResourceModel(OrderResourceModel);
        let newOrder = {...order};
        PaymentService.recalculatePaymentDataInOrder(order);

        order.base_total_paid = CurrencyHelper.roundToFloat(OrderService.getBaseTotalPaid(newOrder));
        order.total_paid = CurrencyHelper.roundToFloat(OrderService.getTotalPaid(newOrder));
        order.base_total_due = CurrencyHelper.roundToFloat(OrderService.getBaseTotalDue(newOrder));
        order.total_due = CurrencyHelper.roundToFloat(OrderService.getTotalDue(newOrder));
        order.base_pos_change = CurrencyHelper.roundToFloat(OrderService.getBasePosChange(newOrder));
        order.pos_change = CurrencyHelper.roundToFloat(OrderService.getPosChange(newOrder));
        let create_invoice = (order.base_total_due === 0 ? 1 : 0);

        let params = {
            request_increment_id: PaymentHelper.generateIncrement(),
            increment_id: order.increment_id,
            payments: cloneDeep(PaymentHelper.filterPaymentData(order.payments)),
            create_invoice: create_invoice
        };

        if (create_invoice) {
            order = InvoiceService.createInvoiceAfterPlaceOrder(order);
            order.status = StatusConstant.STATUS_PROCESSING;
            order.state = StatusConstant.STATE_PROCESSING;
            if (this.isShipped(order.items)) {
                order.status = StatusConstant.STATUS_COMPLETE;
                order.state = StatusConstant.STATE_COMPLETE;
            }
        }

        order.payments.map(payment => payment.is_paid = 1);
        order = await orderResource.takePayment(order);

        let url_api = orderResource.getResourceOnline().getPathTakePayment();
        await ActionLogService.createDataActionLog(
            SyncConstant.REQUEST_TAKE_PAYMENT_ORDER, url_api, SyncConstant.METHOD_POST, params
        );

        return {
            order: order,
            createInvoice: create_invoice
        };
    }
    /**
     * check order is shipped
     * @param items
     * @return {boolean}
     */
    isShipped(items) {
        for (let i = 0; i < items.length; i++) {
            if (items[i].parent_item_id) {
                let parent = items.find(x => x.item_id === items[i].parent_item_id);
                if (parent && parent.product_options) {
                    let productOptions = JSON.parse(parent.product_options);
                    if (productOptions.shipment_type === AbstractProductTypeService.SHIPMENT_TOGETHER) {
                        continue;
                    }
                }
            }
            if (!items[i].qty_shipped || items[i].qty_shipped < items[i].qty_ordered) {
                return false;
            }
        }

        return true;
    }

    /**
     * add and check payments
     * @param customer
     * @param payments
     * @param payments_selected
     * @returns {*}
     */
    addAndCheckPayments(customer, payments, payments_selected) {
        return payments;
    }
}

/** @type TakePaymentService */
let takePaymentService = ServiceFactory.get(TakePaymentService);

export default takePaymentService;
