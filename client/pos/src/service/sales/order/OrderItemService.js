/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import {AbstractOrderService} from "../AbstractService";
import ServiceFactory from "../../../framework/factory/ServiceFactory";
import {AbstractProductTypeService} from "../../catalog/product/type/AbstractTypeService";
import NumberHelper from "../../../helper/NumberHelper";
import StatusItemConstant from "../../../view/constant/order/StatusItemConstant";
import ProductTypeConstant from "../../../view/constant/ProductTypeConstant";

export class OrderItemService extends AbstractOrderService {
    static className = 'OrderItemService';

    /**
     * Check item can creditmemo
     *
     * @param item
     * @param order
     * @return {boolean}
     */
    canCreditmemo(item, order) {
        if (item.parent_item_id) {
            return false
        }
        if ([ProductTypeConstant.BUNDLE, ProductTypeConstant.CONFIGURABLE].includes(item.product_type)) {
            if (this.getQtyToRefund(item, order)) {
                return true;
            }
            return !!this.getChildrenItems(item, order).find(
                child => this.getQtyToRefund(child, order) > 0
            );
        }
        return !!this.getQtyToRefund(item, order) > 0;
    }

    /**
     * Retrieve item qty available for refund
     *
     * @param item
     * @param order
     * @return {number}
     */
    getQtyToRefund(item, order) {
        if (this.isDummy(item, order)) {
            return 0;
        }

        return Math.max(NumberHelper.minusNumber(item.qty_invoiced, item.qty_refunded), 0);
    }

    /**
     * Retrieve item qty available for ship
     *
     * @return {number}
     */
    getQtyToShip(item, order) {
        if (this.isDummy(item, order, true)) {
            return 0;
        }

        return this.getSimpleQtyToShip(item);
    }

    /**
     * Retrieve item qty available for ship
     *
     * @return {number}
     */
    getSimpleQtyToShip(item) {
        let qty = NumberHelper.addNumber(item.qty_ordered, -item.qty_shipped, -item.qty_refunded, -item.qty_canceled);
        return Math.max(qty, 0);
    }


    /**
     * Retrieve item qty available for invoice
     *
     * @return {number}
     */
    getQtyToInvoice(item, order) {
        if (this.isDummy(item, order)) {
            return 0;
        }
        let qty = NumberHelper.addNumber(item.qty_ordered, -item.qty_invoiced, -item.qty_canceled);
        return Math.max(qty, 0);
    }


    /**
     * Retrieve item qty available for cancel
     *
     * @return {number}
     */
    getQtyToCancel(item, order) {
        let qtyToCancel = Math.min(this.getQtyToInvoice(item, order), this.getQtyToShip(item, order));
        return Math.max(qtyToCancel, 0);
    }


    /**
     * Return checking of what calculation
     * type was for this product
     *
     * @param item
     * @param order
     * @return {boolean}
     */
    isChildrenCalculated(item, order) {
        let parentItem = this.getParentItem(item, order);
        let options = null;
        if (parentItem) {
            options = parentItem.product_options;
        } else {
            options = item.product_options;
        }
        options = options ? JSON.parse(options) : {};
        if (options && typeof options.product_calculations !== 'undefined' &&
            +options.product_calculations === AbstractProductTypeService.CALCULATE_CHILD) {
            return true;
        }
        return false;
    }

    /**
     * Return checking of what shipment
     * type was for this product
     *
     * @param item
     * @param order
     * @return {boolean}
     */
    isShipSeparately(item, order) {
        let parentItem = this.getParentItem(item, order);
        let options = null;
        if (parentItem) {
            options = parentItem.product_options;
        } else {
            options = item.product_options;
        }
        options = options ? JSON.parse(options) : {};
        if (options && typeof options.shipment_type !== 'undefined' &&
            +options.shipment_type === AbstractProductTypeService.SHIPMENT_SEPARATELY
        ) {
            return true;
        }
        return false;
    }

    /**
     * This is Dummy item or not
     * if $shipment is true then we checking this for shipping situation if not
     * then we checking this for calculation
     *
     * @param item
     * @param order
     * @param shipment
     * @return {boolean}
     */
    isDummy(item, order, shipment = false) {
        if (shipment) {
            if (this.getHasChildren(item, order) && this.isShipSeparately(item, order)) {
                return true;
            }
            if (this.getHasChildren(item, order) && !this.isShipSeparately(item, order)) {
                return false;
            }
            if (this.getParentItem(item, order) && this.isShipSeparately(item, order)) {
                return false;
            }
            if (this.getParentItem(item, order) && !this.isShipSeparately(item, order)) {
                return true;
            }
        } else {
            if (this.getHasChildren(item, order) && this.isChildrenCalculated(item, order)) {
                return true;
            }
            if (this.getHasChildren(item, order) && !this.isChildrenCalculated(item, order)) {
                return false;
            }
            if (this.getParentItem(item, order) && this.isChildrenCalculated(item, order)) {
                return false;
            }
            if (this.getParentItem(item, order) && !this.isChildrenCalculated(item, order)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check order item has children
     *
     * @param item
     * @param order
     * @return {boolean|*}
     */
    getHasChildren(item, order) {
        if (typeof item.has_children === 'undefined') {
            if (item.parent_item_id) {
                item.has_children = false;
            } else {
                let childrenItems = this.getChildrenItems(item, order);
                item.has_children = childrenItems && childrenItems.length;
            }
        }
        return item.has_children;
    }


    isProcessingAvailable(item, order) {
        return this.getQtyToShip(item, order) - this.getQtyToCancel(item, order);
    }

    _getQtyChildrenBackordered(item, order) {
        let backordered = null;
        let childrenItems = this.getChildrenItems(item, order);
        childrenItems.forEach(childrenItem => {
            backordered += childrenItem.qty_backordered;
        });
        return backordered;
    }


    getStatusId(item, order) {

        let backordered = item.qty_backordered;
        if (!backordered && this.getHasChildren(item, order)) {
            backordered = this._getQtyChildrenBackordered(item, order);
        }

        let canceled = item.qty_canceled;
        let invoiced = item.qty_invoiced;
        let ordered = item.qty_ordered;
        let refunded = item.qty_refunded;
        let shipped = item.qty_shipped;

        let actuallyOrdered = ordered - canceled - refunded;

        if (!invoiced && !shipped && !refunded && !canceled && !backordered) {
            return StatusItemConstant.STATUS_PENDING;
        }
        if (shipped && invoiced && actuallyOrdered === shipped) {
            return StatusItemConstant.STATUS_SHIPPED;
        }

        if (invoiced && !shipped && actuallyOrdered === invoiced) {
            return StatusItemConstant.STATUS_INVOICED;
        }

        if (backordered && actuallyOrdered === backordered) {
            return StatusItemConstant.STATUS_BACKORDERED;
        }

        if (refunded && ordered === refunded) {
            return StatusItemConstant.STATUS_REFUNDED;
        }

        if (canceled && ordered === canceled) {
            return StatusItemConstant.STATUS_CANCELED;
        }

        if (Math.max(shipped, invoiced) < actuallyOrdered) {
            return StatusItemConstant.STATUS_PARTIAL;
        }

        return StatusItemConstant.STATUS_MIXED;
    }

    cancel(item, order) {
        if (this.getStatusId(item, order) !== StatusItemConstant.STATUS_CANCELED) {
            item.qty_canceled = this.getQtyToCancel(item, order);
            item.tax_canceld = item.tax_canceld + item.base_tax_amount * (item.qty_canceled / item.qty_ordered);
            item.discount_tax_compensation_canceled = item.discount_tax_compensation_canceled + item.discount_tax_compensation_amount * (item.qty_canceled / item.qty_ordered);
        }
        return item;
    }


    /**
     * Get Order Item option label as array
     * @param orderItem
     * @param order
     * @return {Array}
     */
    getOrderItemOptionLabelsAsArray(orderItem, order) {
        let productOptions = (orderItem.product_options && !Array.isArray(orderItem.product_options)) ?
            JSON.parse(orderItem.product_options) : null;
        if (!productOptions) {
            return [];
        }
        if (
            ![
                ProductTypeConstant.CONFIGURABLE,
                ProductTypeConstant.SIMPLE,
                ProductTypeConstant.VIRTUAL,
            ].includes(orderItem.product_type)
        ) {
            return [];
        }
        let result = [];
        result.push(...this.getConfigurableOption(productOptions));
        result.push(...this.getCustomOption(productOptions));

        return result;
    }

    /**
     * get option of configurable item
     * @param productOptions
     * @return {*}
     */
    getConfigurableOption(productOptions) {
        if (!productOptions) return [];
        const {attributes_info} = productOptions;
        if (!attributes_info) return [];
        return attributes_info.map(attribute_info => {
            return `${attribute_info.value}`;
        });
    }

    /**
     * display custom options
     * @param productOptions
     * @return {Array}
     */
    getCustomOption(productOptions) {
        let result = [];
        let customOptions = productOptions && productOptions.options ?
            productOptions.options : [];
        customOptions.map(option => {
            return result.push(option.value);
        });
        return result;
    }

    /**
     * Get Qty to return when cancel order
     * 
     * @param item
     * @param order
     * @returns {number}
     */
    getQtyToReturnCancel(item, order) {
        let childrenItems = this.getChildrenItems(item, order);
        if (childrenItems && childrenItems.length) {
            return 0;
        }
        let qtyToInvoice = NumberHelper.addNumber(item.qty_ordered, -item.qty_invoiced, -item.qty_canceled);
        qtyToInvoice = Math.max(qtyToInvoice, 0);
        let qtyToShip = NumberHelper.addNumber(item.qty_ordered, -item.qty_shipped, -item.qty_canceled);
        let parentItem = this.getParentItem(item, order);
        if (parentItem && !this.isShipSeparately(parentItem, order)) {
            let qtyIncrement = item.qty_ordered / parentItem.qty_ordered;
            let qtyShipped = NumberHelper.multipleNumber(qtyIncrement, parentItem.qty_shipped);
            qtyToShip = NumberHelper.addNumber(item.qty_ordered, -qtyShipped, -item.qty_canceled);
        }
        qtyToShip = Math.max(qtyToShip, 0);
        return Math.min(qtyToInvoice, qtyToShip);
    }

    /**
     * Get non-shipped qty to return (qty invoiced but non shipped when refund)
     *
     * @param item
     * @param order
     * @returns {number}
     */
    getNonShippedQtyToReturn(item , order) {
        let qtyShipped = item.qty_shipped || 0;
        let parentItem = this.getParentItem(item, order);
        if (parentItem && !this.isShipSeparately(parentItem, order)) {
            let qtyIncrement = item.qty_ordered / parentItem.qty_ordered;
            qtyShipped = NumberHelper.multipleNumber(qtyIncrement, parentItem.qty_shipped);
        }
        return Math.max(
            NumberHelper.addNumber(item.qty_invoiced, -qtyShipped, -item.qty_refunded),
            0
        );
    }
}

/** @type OrderItemService */
let orderItemService = ServiceFactory.get(OrderItemService);

export default orderItemService;