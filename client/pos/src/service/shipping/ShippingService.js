/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreService from "../CoreService";
import ShippingResourceModel from "../../resource-model/shipping/ShippingResourceModel";
import ServiceFactory from "../../framework/factory/ServiceFactory";
import ResultService from "./rate/ResultService";
import FlatRateService from "./methods/FlatRateService";
import TableRateService from "./methods/TableRateService";
import FreeShippingService from "./methods/FreeShippingService";
import Config from "../../config/Config";

export class ShippingService extends CoreService {
    static className = 'ShippingService';
    resourceModel = ShippingResourceModel;

    allowShippingMethods = [
        FlatRateService,
        TableRateService,
        FreeShippingService
    ];

    /**
     * Call ShippingResourceModel get all
     *
     * @returns {Object|*|FormDataEntryValue[]|string[]}
     */
    getAll() {
        let shippingResourceModel = this.getResourceModel();
        return shippingResourceModel.getAll();
    }

    /**
     * Get Allow shipping methods
     *
     * @return {T[]}
     */
    getAllowShippingMethods() {
        let allShippingMethods = Config.shipping_methods || [];
        let allowShippingMethods = [];
        if (Config.config && Config.config.shipping && Config.config.shipping.shipping_methods) {
            allowShippingMethods = Config.config.shipping.shipping_methods.split(',');
        }
        return allShippingMethods.filter(shippingMethod => {
            return allowShippingMethods.find(allowShippingMethod => {
                return shippingMethod.code.substring(0, allowShippingMethod.length) === allowShippingMethod;
            });
        });
    }

    /**
     * Collect all shipping rates for address
     *
     * @param request
     * @param allShippingMethods
     * @param quote
     */
    collectRates(request, allShippingMethods = [], quote) {
        ResultService.reset();
        this.allowShippingMethods.forEach(shippingMethodService => {
            shippingMethodService.setMethod(allShippingMethods);
            if (shippingMethodService.isActive()) {
                this.collectMethodRates(shippingMethodService, request, quote);
            }
        });
        return ResultService;
    }

    /**
     * Collect method rates of shipping method service
     *
     * @param shippingMethodService
     * @param request
     * @param quote
     */
    collectMethodRates(shippingMethodService, request, quote) {
        let result = shippingMethodService.checkAvailableShipCountries(request);
        if (!result) {
            return false;
        }
        if (!shippingMethodService.getConfigData('shipment_request_type')) {
            result = shippingMethodService.collectRates(request, quote);
        }

        if (!result) {
            return this;
        }
        ResultService.append(result);
        return this;
    }
}

/** @type ShippingService */
let shippingService = ServiceFactory.get(ShippingService);

export default shippingService;
