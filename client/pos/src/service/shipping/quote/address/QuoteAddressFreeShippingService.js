/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import CoreService from "../../../CoreService";
import ServiceFactory from "../../../../framework/factory/ServiceFactory";
import QuoteItemService from "../../../checkout/quote/ItemService";

export class QuoteAddressFreeShippingService extends CoreService {
    static className = 'QuoteAddressFreeShippingService';

    FREE_SHIPPING_ITEM = '1';

    /**
     * Free Shipping option "For shipment with matching items"
     */
    FREE_SHIPPING_ADDRESS = '2';

    /**
     *
     * @param quote
     * @param address
     * @param items
     * @return {boolean}
     */
    isFreeShipping(quote, address, items) {
        let addressFreeShipping = true;
        items.forEach(item => {
            if (item.no_discount) {
                addressFreeShipping = false;
                item.free_shipping = false;
                return false;
            }
            if (item.parent_item_id) {
                return false;
            }
            this.processFreeShipping(quote, address, item);

            let itemFreeShipping = !!item.free_shipping;
            addressFreeShipping = addressFreeShipping && itemFreeShipping;

            if (addressFreeShipping && !address.free_shipping) {
                address.free_shipping = true;
            }

            this.applyToChildren(quote, address, item, itemFreeShipping);
        });
        return !!address.free_shipping;
    }

    /**
     *
     * @param quote
     * @param address
     * @param item
     */
    processFreeShipping(quote, address, item) {
        item.free_shipping = false;
        if (quote.valid_salesrule && quote.valid_salesrule.length) {
            quote.valid_salesrule.forEach(rule => {
                if (rule.valid_item_ids.includes(+item.item_id)) {
                    switch (rule.simple_free_shipping) {
                        case this.FREE_SHIPPING_ITEM:
                            item.free_shipping = rule.discount_qty ? rule.discount_qty : true;
                            break;
                        case this.FREE_SHIPPING_ADDRESS:
                            address.free_shipping = true;
                            break;
                        default:
                            break;
                    }
                }
            });
        }
    }

    /**
     *
     * @param quote
     * @param address
     * @param item
     * @param isFreeShipping
     */
    applyToChildren(quote, address, item, isFreeShipping) {
        if (item.has_children && QuoteItemService.isChildrenCalculated(item, quote)) {
            QuoteItemService.getChildrenItems(quote, item).forEach(child => {
                this.processFreeShipping(quote, address, child);
                if (isFreeShipping) {
                    child.free_shipping = isFreeShipping;
                }
            });
        }
    }
}

/** @type QuoteAddressFreeShippingService */
let quoteAddressFreeShippingService = ServiceFactory.get(QuoteAddressFreeShippingService);

export default quoteAddressFreeShippingService;
