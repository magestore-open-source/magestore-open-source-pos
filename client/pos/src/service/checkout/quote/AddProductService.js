/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import ServiceFactory from "../../../framework/factory/ServiceFactory";
import {AbstractQuoteService} from "./AbstractService";
import AddSimpleProductService from './add-product/SimpleService';
import AddConfigurableProductService from './add-product/ConfigurableService';
import AddBundleProductService from './add-product/BundleService';
import AddGroupedProductService from './add-product/GroupedService';

export class AddProductService extends AbstractQuoteService {
    static className = 'AddProductService';

    addProductServices = {
        simple: AddSimpleProductService,
        configurable: AddConfigurableProductService,
        bundle: AddBundleProductService,
        grouped: AddGroupedProductService,
    };

    /**
     * Get Add product service per product type
     *
     * @param product
     * @return {*}
     */
    getAddProductService(product) {
        if (typeof product === 'string' && this.addProductServices[product]) {
            return this.addProductServices[product];
        }
        if (product.type_id && this.addProductServices[product.type_id]) {
            return this.addProductServices[product.type_id];
        }
        return this.addProductServices.simple;
    }

    /**
     * Add product to cart
     *
     * @param quote
     * @param data
     * @return {*|{type, data}|void}
     */
    addProduct(quote, data) {
        return this.getAddProductService(data.product).addProduct(quote, data);
    }
}

/** @type AddProductService */
let addProductService = ServiceFactory.get(AddProductService);

export default addProductService;