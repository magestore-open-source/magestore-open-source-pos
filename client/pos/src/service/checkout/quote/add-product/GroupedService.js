/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import ServiceFactory from "../../../../framework/factory/ServiceFactory";
import {AbstractAddProductService} from "./AbstractService";
import ItemService from "../ItemService";
import i18n from "../../../../config/i18n";
import NumberHelper from "../../../../helper/NumberHelper";
import CheckoutHelper from "../../../../helper/CheckoutHelper";

export class AddGroupedProductService extends AbstractAddProductService {
    static className = 'AddGroupedProductService';

    /**
     * Add configurable product
     *
     * @param quote
     * @param data
     * @return {*}
     */
    addProduct(quote, data) {
        let addedItemIds = [];
        let index = 0;
        let childrenItems = [];
        data.childrens.map(children => {
            let existedItem = this.getExistedItem(quote, children);
            if (existedItem) {
                existedItem.qty = NumberHelper.addNumber(existedItem.qty, children.qty);
                addedItemIds.push(existedItem.item_id);
            } else {
                let childrenItem = {
                    ...ItemService.createItem(children.product, children.qty, quote),
                    quote_id: quote.id
                };
                childrenItem.item_id = childrenItem.item_id + 10000000000 * Math.floor(index/10) + index % 10;
                childrenItem.product_options = children.product_options;
                index++;
                childrenItems.push(childrenItem);
                addedItemIds.push(childrenItem.item_id);
            }
            return children;
        });

        quote.items.push(...childrenItems);

        return {
            success: true,
            quote: quote,
            added_item_id: (addedItemIds.length ? Math.max(...addedItemIds) : null)
        };
    }

    /**
     * Get existed item in cart
     *
     * @param quote
     * @param children
     * @return {*}
     */
    getExistedItem(quote, children) {
        let existedItem = null;
        let items = this.getItemsByProductId(quote, children.product.id);
        if (items && items.length) {
            existedItem = items.find(item => {
                if (typeof item.custom_price !== 'undefined' && item.custom_price !== null) return false;
                let cartItemCustomOptions = item.product_options;
                let addItemCustomOptions = children.product_options;
                if (!cartItemCustomOptions) {
                    return false;
                }
                if (!cartItemCustomOptions.super_product_config) {
                    return false;
                }
                let cartItemSuperProductConfig = cartItemCustomOptions.super_product_config;
                let addItemSuperProductConfig = addItemCustomOptions.super_product_config;
                if (!cartItemSuperProductConfig.product_code || !cartItemSuperProductConfig.product_type ||
                    !cartItemSuperProductConfig.product_id) {
                    return false;
                }
                if (cartItemSuperProductConfig.product_type !== addItemSuperProductConfig.product_type) {
                    return false;
                }
                if (cartItemSuperProductConfig.product_id !== addItemSuperProductConfig.product_id) {
                    return false;
                }
                return true;
            });
        }
        return existedItem;
    }

    /**
     *
     * @param product
     * @param qty
     * @param totalQty
     * @return {*}
     */
    validateChildrenQty(product, qty, totalQty) {
        let stock = product.stocks && product.stocks.length ? product.stocks[0] : null;
        if (!stock) {
            return {
                success: false,
                message: i18n.translator.translate("You cannot add this product to cart")
            };
        }
        let productStockService = this.getProductStockService(product);
        let maxSaleQty = productStockService.getMaxSaleQty(product);
        if (CheckoutHelper.isAllowToAddOutOfStockProduct() || !productStockService.isManageStock(product)) {
            maxSaleQty = Math.max(maxSaleQty, 0);
            let minQty = productStockService.getOutOfStockThreshold(product);
            let productQty = productStockService.getProductQty(product);
            maxSaleQty = Math.min(maxSaleQty, productQty - minQty);
        } else {
            let backOrder = productStockService.getBackorders(product);
            let minQty = productStockService.getOutOfStockThreshold(product);
            /**
             * Fix issue can add product out of threshold
             *
             * In online mode, if load 1 product to check stock => function magento get salable qty
             * is (Location + reservation - min_qty) (salable qty)
             *
             * But in offline mode, because used SQL query to sync bulk
             * => the formula is only calculate base on Quantity and Reservation (Location + reservation) by SUM
             *
             * So should check condition mode to get correct salable qty
             * */
            let productQty = productStockService.getProductQty(product);
            /* Is offline mode or need calculate */
            if (product.stocks && product.stocks.length && product.stocks[0].need_calculate_salable) {
                productQty = NumberHelper.minusNumber(productQty, minQty);
            }
            if (!backOrder) {
                maxSaleQty = Math.min(maxSaleQty, productQty);
            } else {
                if (minQty < 0) {
                    maxSaleQty = Math.max(maxSaleQty, 0);
                    maxSaleQty = Math.min(maxSaleQty, productQty);
                } else {
                    maxSaleQty = Math.min(maxSaleQty, productQty);
                }
            }
        }
        maxSaleQty = Math.max(maxSaleQty, 0);
        if (totalQty > maxSaleQty) {
            return {
                success: false,
                message: i18n.translator.translate("The maximum quantity of {{name}} to sell is {{qty}}", {
                    name: product.name,
                    qty: maxSaleQty
                })
            }
        }
        let isEnableQtyIncrement = productStockService.isEnableQtyIncrements(product);
        if (isEnableQtyIncrement) {
            let qtyIncrement = productStockService.getAddQtyIncrement(product);
            if (NumberHelper.remainderNumber(qty, qtyIncrement) !== 0) {
                return {
                    success: false,
                    message: i18n.translator.translate("Please enter multiple of {{qty}}", {qty: qtyIncrement})
                }
            }
        }
        return {success: true};
    }
}

/** @type AddGroupedProductService */
let addGroupedProductService = ServiceFactory.get(AddGroupedProductService);

export default addGroupedProductService;