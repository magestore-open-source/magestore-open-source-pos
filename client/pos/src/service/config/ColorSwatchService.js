/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import ColorSwatchResourceModel from "../../resource-model/catalog/ColorSwatchResourceModel";
import LocalStorageHelper from "../../helper/LocalStorageHelper";
import CoreService from "../CoreService";
import ServiceFactory from "../../framework/factory/ServiceFactory"
import Config from "../../config/Config";

export class ColorSwatchService extends CoreService {
    static className = 'ColorSwatchService';
    resourceModel = ColorSwatchResourceModel;

    /**
     * Save all ColorSwatch to Local storage
     * @param data
     */
    saveToLocalStorage(data) {
        LocalStorageHelper.set(LocalStorageHelper.COLOR_SWATCH, JSON.stringify(data));
        Config.swatch_config = data;
    }

    /**
     * get ColorSwatch from local storage
     * @returns {*|string}
     */
    getColorSwatchFromLocalStorage() {
        return LocalStorageHelper.get(LocalStorageHelper.COLOR_SWATCH);
    }
}

let colorSwatchService = ServiceFactory.get(ColorSwatchService);

export default colorSwatchService;