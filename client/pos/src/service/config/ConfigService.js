/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import ConfigResourceModel from "../../resource-model/config/ConfigResourceModel";
import LocalStorageHelper from "../../helper/LocalStorageHelper";
import CoreService from "../CoreService";
import ServiceFactory from "../../framework/factory/ServiceFactory";
import i18n from "../../config/i18n";
import ConfigHelper from "../../helper/ConfigHelper";
import Config from "../../config/Config";

export class ConfigService extends CoreService {
    static className = 'ConfigService';
    resourceModel = ConfigResourceModel;

    /**
     * Save all config to Local storage
     * @param data
     */
    saveToLocalStorage(data) {
        LocalStorageHelper.set(LocalStorageHelper.CONFIG, JSON.stringify(data));
    }

    /**
     * get config from local storage
     * @returns {*|string}
     */
    getConfigFromLocalStorage() {
        return LocalStorageHelper.get(LocalStorageHelper.CONFIG);
    }

    /**
     * Change language
     * @param locale
     */
    changeLanguage(locale) {
        let path = window.location.pathname;
        path = path.split(Config.basename);
        path = path[0] ? path[0] : '/';

        if(!locale) {
            locale = ConfigHelper.getLocaleCode();
        }
        i18n.init({
            fallbackLng: false,
            lng: locale.toLowerCase(),

            // have a common namespace used around the full app
            ns: ['translations'],
            defaultNS: 'translations',
            nsSeparator: false,
            keySeparator: false,

            debug: false,

            interpolation: {
                escapeValue: false, // not needed for react!!
            },

            backend: {
                loadPath: path + Config.basename + '/locales/{{lng}}/{{ns}}.json'
            },

            react: {
                wait: true,
                withRef: false,
                bindI18n: 'languageChanged loaded',
                bindStore: 'added removed',
                nsMode: 'default'
            }
        });
    }
}
/** @type ConfigService */
let configService = ServiceFactory.get(ConfigService);

export default configService;

