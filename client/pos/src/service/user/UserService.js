/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import UserResourceModel from '../../resource-model/user/UserResourceModel'
import LocalStorageHelper from "../../helper/LocalStorageHelper";
import Config from '../../config/Config';
import PosService from "../PosService";
import CoreService from "../CoreService";
import ServiceFactory from "../../framework/factory/ServiceFactory";

/**
 * export UserService
 * @type {{prepareLogin: prepareLogin, login: login, handleAssignPos: assignPos}}
 */
export class UserService extends CoreService{
    static className = 'UserService';
    resourceModel = UserResourceModel;

    /**
     * login by username and password
     * @param username
     * @param password
     * @returns {promise}
     */
    login(username, password) {
        return this.getResourceModel().login(username, password);
    }

    /**
     *
     * @returns {promise}
     */
    continueLogin(){
        return this.getResourceModel().continueLogin();
    }

    /**
     * get logo
     */
    getLogo() {
        return this.getResourceModel().getLogo();
    }

    /**
     * get logo
     */
    getCountries() {
        return this.getResourceModel().getCountries();
    }

    /**
     * user change information
     * @param user_name
     * @param old_password
     * @param new_password
     * @param confirmation_password
     */
    changeInformation(user_name, old_password, new_password, confirmation_password) {
        return this.getResourceModel().changePassword(user_name, old_password, new_password, confirmation_password);
    }

    /**
     * check has pending sync request yet
     * call api log out and remove session from storage
     *
     * @return {Promise<{ok: boolean}>}
     */
     async logout() {
        try {
            await this.getResourceModel().logout();
        } catch (error) {
            // handle error
        } finally {
            this.resetAllData();
        }

        return { ok: true };
    }

    /**
     * Reset all data
     * @returns {UserService}
     */
    resetAllData(){
        this.removeStaff();
        this.removeSession();
        this.removeToken();
        PosService.removeCurrentPos();
        this.removeWebsiteId();
        return this;
    }

    /**
     * save session to local storage
     *
     * @param session
     */
    saveSession(session, timeout) {
        LocalStorageHelper.setSession(session);
        LocalStorageHelper.set(LocalStorageHelper.TIMEOUT, timeout);
        Config.session = session;
        Config.timeout = timeout;
    }

    /**
     * save staff id and staff name local storage
     *
     * @param staffId
     * @param staffName
     * @param staffEmail
     * @param staffUsername
     */
    saveStaff(staffId, staffName, staffEmail, staffUsername) {
        LocalStorageHelper.set(LocalStorageHelper.STAFF_ID, staffId);
        LocalStorageHelper.set(LocalStorageHelper.OLD_STAFF_ID, staffId);
        LocalStorageHelper.set(LocalStorageHelper.STAFF_NAME, staffName);
        LocalStorageHelper.set(LocalStorageHelper.STAFF_EMAIL, staffEmail);
        LocalStorageHelper.set(LocalStorageHelper.STAFF_USERNAME, staffUsername);
        Config.staff_id = staffId;
        Config.staff_name = staffName;
        Config.staff_email = staffEmail;
        Config.staff_username = staffUsername;
    }

    /**
     * save location and pos to local storage
     *
     * @param locations
     */
    saveLocations(locations) {
        LocalStorageHelper.set(LocalStorageHelper.LOCATIONS, locations);
    }

    /**
     * get session from local storage
     *
     * @param string
     */
    getSession() {
        return LocalStorageHelper.getSession();
    }

    /**
     * remove session from localStorage
     */
    removeSession(){
        LocalStorageHelper.removeSession();
    }

    /**
     * get old staff id from local storage
     * @return {*|string}
     */
    getOldStaffId() {
        return LocalStorageHelper.get(LocalStorageHelper.OLD_STAFF_ID);
    }

    /**
     * get staff id from local storage
     *
     * @param string
     */
    getStaffId() {
        return LocalStorageHelper.get(LocalStorageHelper.STAFF_ID);
    }

    /**
     * get staff name local storage
     *
     * @return {string}
     */
    getStaffName() {
        return LocalStorageHelper.get(LocalStorageHelper.STAFF_NAME);
    }

    /**
     * get staff username local storage
     *
     * @return {string}
     */
    getStaffUsername() {
        return LocalStorageHelper.get(LocalStorageHelper.STAFF_USERNAME);
    }

    /**
     * get staff email local storage
     *
     * @return {string}
     */
    getStaffEmail() {
        return LocalStorageHelper.get(LocalStorageHelper.STAFF_EMAIL);
    }

    /**
     * remove staff id and staff name local storage
     *
     * @return void
     */
    removeStaff() {
        LocalStorageHelper.remove(LocalStorageHelper.STAFF_ID);
        LocalStorageHelper.remove(LocalStorageHelper.STAFF_NAME);
    }

    /**
     * clear location pos from localStorage
     */
    clearLocationPosInLocalStorage() {
        LocalStorageHelper.remove(LocalStorageHelper.LOCATION_ID);
        LocalStorageHelper.remove(LocalStorageHelper.LOCATION_NAME);
        LocalStorageHelper.remove(LocalStorageHelper.POS_ID);
        LocalStorageHelper.remove(LocalStorageHelper.POS_NAME);
    }

    /**
     * get logo from local storage
     */
    getLocalLogo () {
        return LocalStorageHelper.get(LocalStorageHelper.LOGO_URL);
    }

    /**
     * save logo to local storage
     * @param logoUrl
     */
    saveLocalLogo (logoUrl) {
        LocalStorageHelper.set(LocalStorageHelper.LOGO_URL, logoUrl);
    }

    /**
     * get countries from local storage
     */
    getLocalCountries () {
        let localConfig =  LocalStorageHelper.get(LocalStorageHelper.COUNTRIES);
        return localConfig;
    }

    /**
     * save countries to local storage
     * @param countries
     */
    saveLocalCountries (countries) {
        let localConfig = LocalStorageHelper.get(LocalStorageHelper.COUNTRIES);
        localConfig = JSON.stringify(countries);
        LocalStorageHelper.set(LocalStorageHelper.COUNTRIES, localConfig);
        Config.countries = countries;
    }
    /**
     * get website id local storage
     *
     * @return {string}
     */
    getWebsiteId() {
        return LocalStorageHelper.get(LocalStorageHelper.WEBSITE_ID);
    }

    /**
     * save website id to local storage
     * @param websiteId
     * @return {UserService}
     */
    saveWebsiteId (websiteId) {
        LocalStorageHelper.set(LocalStorageHelper.WEBSITE_ID, websiteId);
        return this;
    }
    /**
     * remove website id to local storage
     * @return {UserService}
     */
    removeWebsiteId () {
        LocalStorageHelper.remove(LocalStorageHelper.WEBSITE_ID);
        return this;
    }

    /**
     *
     * @param sharingAccount
     */
    setSharingAccount(sharingAccount){
        LocalStorageHelper.set(LocalStorageHelper.SHARING_ACCOUNT, sharingAccount);
        return this;
    }

    /**
     * get sharing account local storage
     *
     * @return {string}
     */
    getSharingAccount() {
        return LocalStorageHelper.get(LocalStorageHelper.SHARING_ACCOUNT);
    }

    /**
     * save token to local storage
     *
     * @param token
     */
    saveToken(token) {
        LocalStorageHelper.setToken(token);
        Config.token = token;
    }

    /**
     * get token from local storage
     *
     * @param string
     */
    getToken() {
        return LocalStorageHelper.getToken();
    }

    /**
     * remove token from localStorage
     */
    removeToken(){
        LocalStorageHelper.removeToken();
    }
}


/**
 * @type {UserService}
 */
const userService = ServiceFactory.get(UserService);

export default userService;
