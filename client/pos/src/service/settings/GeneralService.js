/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ServiceFactory from "../../framework/factory/ServiceFactory";
import LocalStorageHelper from "../../helper/LocalStorageHelper";
import GeneralConstant from "../../view/constant/settings/GeneralConstant";
import toNumber from "lodash/toNumber";
import ConfigHelper from "../../helper/ConfigHelper";
import DeviceHelper from "../../helper/DeviceHelper";

export class GeneralService {
    static className="GeneralService";

    /**
     * check is use offline data
     * @returns {boolean}
     */
    isUseOfflineData() {
        if(LocalStorageHelper.get(GeneralConstant.GET_IS_SYNC_DATA_TO_BROWSER_STORAGE) === null) {
            this.resetPosMode();
        }

        return !!toNumber(LocalStorageHelper.get(GeneralConstant.GET_IS_SYNC_DATA_TO_BROWSER_STORAGE));
    }

    /**
     * @param value
     * @returns {*}
     */
    setUseOfflineData(value) {
        return LocalStorageHelper.set(GeneralConstant.GET_IS_SYNC_DATA_TO_BROWSER_STORAGE, value ? 1 : 0);
    }

    /**
     * Reset pos mode
     */
    resetPosMode() {
        let mode = ConfigHelper.getConfig(GeneralConstant.CONFIG_XML_PATH_PERFORMANCE_POS_DEFAULT_MODE);
        if (DeviceHelper.isMobile()) {
            mode = ConfigHelper.getConfig(GeneralConstant.CONFIG_XML_PATH_PERFORMANCE_POS_TABLET_DEFAULT_MODE);
        }
        mode = !!(mode && mode === '1');
        this.setUseOfflineData(mode);
    }
}

/**
 * @type {GeneralService}
 */
let settingsGeneralService = ServiceFactory.get(GeneralService);

export default settingsGeneralService;
