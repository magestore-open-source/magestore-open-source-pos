/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {QuoteTotalCommonTaxCollectorService} from "./CommonTaxCollectorService";
import ServiceFactory from "../../../../framework/factory/ServiceFactory";
import AddressService from "../../../checkout/quote/AddressService";
import TaxCalculationService from "../../TaxCalculationService";

export class QuoteTotalTaxShippingService extends QuoteTotalCommonTaxCollectorService {
    static className = 'QuoteTotalTaxShippingService';

    code = 'tax_shipping';

    /**
     * Collect address tax subtotal
     *
     * @param {object} quote
     * @param {object} address
     * @param {object} total
     * @return {QuoteTotalSubtotalService}
     */
    collect(quote, address, total) {
        super.collect(quote, address, total);
        let isVirtual = this.isVirtual(quote);
        if (!quote.items || !quote.items.length) {
            return this;
        }
        if ((isVirtual && AddressService.isBillingAddress(address)) ||
            (!isVirtual && AddressService.isShippingAddress(address))
        ) {
            let shippingDataObject = this.getShippingDataObject(quote, address, total, false);
            let baseShippingDataObject = this.getShippingDataObject(quote, address, total, true);
            if (shippingDataObject === null || baseShippingDataObject === null) {
                return this;
            }
            let quoteDetais = this.prepareQuoteDetails(quote, address, [shippingDataObject]);
            let taxDetails = TaxCalculationService.calculateTax(quoteDetais);
            let baseQuoteDetails = this.prepareQuoteDetails(quote, address, [baseShippingDataObject]);
            let baseTaxDetails = TaxCalculationService.calculateTax(baseQuoteDetails);

            this.processShippingTaxInfo(
                quote,
                address,
                total,
                taxDetails.items[this.ITEM_CODE_SHIPPING],
                baseTaxDetails.items[this.ITEM_CODE_SHIPPING]
            );


        }
    }
}

/** @type QuoteTotalTaxShippingService */
let quoteTotalTaxShippingService = ServiceFactory.get(QuoteTotalTaxShippingService);

export default quoteTotalTaxShippingService;