/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import {AbstractAggregateCalculatorService} from "./AbstractAggregateCalculatorService";
import ServiceFactory from "../../../framework/factory/ServiceFactory";

export class RowBaseCalculatorService extends AbstractAggregateCalculatorService {
    static className = 'RowBaseCalculatorService';

    /**
     * {@inheritdoc}
     */
    roundAmount(
        amount,
        rate = null,
        direction = null,
        type = this.KEY_REGULAR_DELTA_ROUNDING,
        round = true,
        item = null
    ) {
        if (item.associated_item_code) {
            // Use delta rounding of the product's instead of the weee's
            type = type + item.associated_item_code;
        } else {
            type = type + item.code;
        }

        return this.deltaRound(amount, rate, direction, type, round);
    }

}

/** @type RowBaseCalculatorService */
let rowBaseCalculatorService = ServiceFactory.get(RowBaseCalculatorService);

export default rowBaseCalculatorService;