/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */ 
import ResourceModelFactory from "../../framework/factory/ResourceModelFactory";
import CoreService from "../CoreService";
import ServiceFactory from "../../framework/factory/ServiceFactory";
import TaxRateResourceModel from "../../resource-model/tax/TaxRateResourceModel";
import TaxRuleResourceModel from "../../resource-model/tax/TaxRuleResourceModel";
import Config from '../../config/Config';

export class TaxService extends CoreService{
    static className = 'TaxService';
    resourceModel = TaxRateResourceModel;
    ruleResourceModel = TaxRuleResourceModel;

    /**
     * get target Resource Model
     *
     * @return {class}
     */
    getRuleResourceModel() {
        return new (ResourceModelFactory.get(this.ruleResourceModel))();
    }

    /**
     * Call TaxRateResourceModel save to indexedDb
     *
     * @param data
     * @returns {Promise|*|void}
     */
    saveTaxRateToDb(data) {
        return this.getResourceModel().saveToDb(data);
    }

    /**
     * Call TaxRateResourceModel save to indexedDb
     *
     * @param data
     * @returns {Promise|*|void}
     */
    saveTaxRuleToDb(data) {
        return this.getRuleResourceModel().saveToDb(data);
    }

    /**
     * Save tax rate to config
     * 
     * @param data
     */
    saveTaxRateToConfig(data) {
        if(data) {
            Config.tax_rate = data;
        }
    }

    /**
     * Save tax rule to config
     *
     * @param data
     */
    saveTaxRuleToConfig(data) {
        if(data) {
            Config.tax_rule = data;
        }
    }
}

/**
 * @type {TaxService}
 */
let taxService = ServiceFactory.get(TaxService);

export default taxService;