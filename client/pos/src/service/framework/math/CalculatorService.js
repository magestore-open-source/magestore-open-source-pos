/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
import CoreService from "../../CoreService";
import NumberHelper from "../../../helper/NumberHelper";
import CurrencyHelper from "../../../helper/CurrencyHelper";

export default class MathCalculatorService extends CoreService {
    static className = 'MathCalculatorService';

    _delta = 0;

    /**
     * Round price considering delta
     *
     * @param price
     * @param negative
     * @return {*}
     */
    deltaRound(price, negative = false) {
        let roundedPrice = price;
        if (roundedPrice) {
            if (negative) {
                this._delta = -this._delta;
            }
            price = NumberHelper.addNumber(price, this._delta);
            roundedPrice = CurrencyHelper.roundToFloat(price);
            this._delta = NumberHelper.minusNumber(price, roundedPrice);
            if (negative) {
                this._delta = -this._delta;
            }
        }
        return roundedPrice;
    }
}