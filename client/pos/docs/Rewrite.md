<!--
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
-->
# Rewrite Class

A plugin can rewrite a class from `core` or `other plugin`.

## Config Rewrite
In the plugin, we register rewrite in file `etc/config.js`.

For example: We rewrite MenuComponent
```js
import rewriteMenu from "../view/menu";
import ModuleConfigAbstract from "../../ModuleConfigAbstract";

class HelloWorldConfig extends ModuleConfigAbstract{
    module = ['helloworld'];
    rewrite = {
        service: {
            // "UserService": BlaService
        },
        container: {
            // "LoginContainer": HelloWorldContainer
        },
        component: {
            MenuComponent: rewriteMenu
        },
    };
}

export default (new HelloWorldConfig());
```

We have 7 type of class can be rewrite:

1. service
2. resource_model
3. repository
4. epic
5. container
6. component
7. data_resource

To determine which type of class, we can find `Factory` type in source code. Example:
```js
/**
 *
 * @type {MenuComponent}
 */
const component = ComponentFactory.get(MenuComponent);
```

## Rewrite Implementation
To implement rewrite, we need using javascript function. For example, we implement rewrite for MenuComponent as:

```js
import React, {Fragment} from "react";

/**
 * Rewrite MenuComponent
 *
 * @param {MenuComponent} MenuComponent
 * @returns {RewriteClass}
 */
export default function(MenuComponent) {
    return class Rewrite extends MenuComponent {
        template() {
            let template = super.template();
            return (
              <Fragment>
                Hello World!
                {template}
              </Fragment>
            )
        }
    }
}
```

### Rewrite Epic
Epic is a **Function**, not a **Class**. So rewriting epic has a little difference. For example, we implement rewrite for LocationEpic as:

```js
import {Observable} from "rxjs/Rx";
import UserConstant from "../../../view/constant/UserConstant";
import LocationAction from "../../../view/action/LocationAction";

export default function () {
    let LocationEpic = function (action$) {
        let LocationService = require("../../../service/LocationService").default;
        return action$.ofType(UserConstant.USER_ASSIGN_POS)
            .mergeMap(action => Observable.from(
                LocationService.assignPos(
                    action.posId, action.locationId, action.currentStaffId))
                .map((response) => {
                    console.log(response);
                    return LocationAction.assignPosResponse();
                })
                .catch((error) =>{
                        return Observable.of(LocationAction.assignPosError(error.message));
                    }
                )
            );
    };
    LocationEpic.className = "LocationEpic";
    return LocationEpic;
}
```
