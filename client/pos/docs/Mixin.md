<!--
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
-->
# Mixin Mechanism

A module can add a method to a class from `core` or `other module`.

## Config Mixin
In the module, we register a mixin in file `etc/config.js`.

For example: We add mixin for MenuComponent
```js
import ModuleConfigAbstract from "../../ModuleConfigAbstract";

class HelloWorldConfig extends ModuleConfigAbstract{
    module = ['helloworld'];
    mixin = {
        component: {
            MenuComponent: {
                // new method name: mixin method implementation
                setOrder: function(order) {
                    this.order = order;
                    return this;
                },
                getOrder: function() {
                    return this.order;
                },
                // Static methods
                static: {
                    plus: function(a, b) {
                        return b + a;
                    }
                }
            }
        }
    };
}

export default (new HelloWorldConfig());
```

We have 7 type of class can be plugged in:

1. service
2. resource_model
3. repository
4. container
5. component
6. data_resource

To determine which type of class, we can find `Factory` type in source code. Example:
```js
/**
 *
 * @type {MenuComponent}
 */
const component = ComponentFactory.get(MenuComponent);
```
